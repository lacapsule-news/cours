# Titre RNCP

https://www.francecompetences.fr/recherche/rncp/36061/

- Titre reconnu en France délivré par le ministère du Travail
- Titre de niveau 6(bac +3/4)
- Une certification professionnelle qui permet de travailler en tant qu'ingénieur DevOps ou ingénieur cloud

L'épreuve se déroule 3 semaines après la formation et se décompose en deux parties:
- Un dossier projet à faire, basé sur votre projet de fin de batch
- une soutenance de 2h05 devant un jury d'experts indépendants constitué de deux personnes

> L'épreuve sera détaillée d'avantage lors d'une seconde réunion d'information lors de la 5eme semaine de formation.

Le passage du titre est conditionné par une évaluation continue basée sur le référentiel d'évaluation des certificats de compétences professionnelles(CCP).
- 6 QCM à faire directement sur ariane : dès la fin de la 2eme semaine
- 12 cas pratique(challenges) évalués par votre teacher

> Un rattrapage est possible si au moins un des 3 blocs est validé ou en cas d'absence par un cas de force majeur

```
Contact pou le titre

Alienor ROUSSEL
Office Manager

alienor.rousel@lacapsule.academy

0644640366
```