# START WITH DJANGO

## GÉNÉRATION DU BACKEND

Installez les dépendances systèmes nécessaires grâce à la commande suivante.


> sudo apt update && sudo apt install -y python3-venv python3-pip


-  Configurez l’environnement virtuel Python.


-  Installez Django via la commande pip3.


-  Initialisez un projet Django nommé "my_first_backend".


- Démarrez le projet puis testez-le afin vérifier qu’il est bien fonctionnel.

```
➜  06-Start_with_django python -m venv start_with_django
➜  06-Start_with_django cd start_with_django 
➜  start_with_django source bin/activate
(start_with_django) ➜  start_with_django pip install django
Collecting django
[notice] To update, run: pip install --upgrade pip
(start_with_django) ➜  start_with_django pip install --upgrade pip
(start_with_django) ➜  start_with_django django-admin startproject my_first_backend
(start_with_django) ➜  my_first_backend python3 manage.py startapp webservice
```


## PREMIÈRE ROUTE

- Créez une nouvelle route "/test" chargée de renvoyer le résultat suivant.

```
{

  "answer": "Hello, world!"

}
```

- Crée le fichier urls dans start_with_django/my_first_backend/webservices/routes.py

```
from django.http import JsonResponse

# Create your routes here

def first_route(request):
    return JsonResponse({"answer": "Hello, world!"})
```

- Edité le fichier start_with_django/my_first_backend/my_first_backend/urls.py

```
from django.contrib import admin
from django.urls import path

from webservice import routes

urlpatterns = [
    path('admin/', admin.site.urls),
    path('test/', routes.first_route)
]
```