# File api

## ÉCRITURE DANS UN FICHIER

> A partir du backend généré dans le challenge précédent, initialisez une nouvelle route "/fill" récupérant une chaîne de caractères en body et qui ajoute le contenu de cette chaîne de caractères dans un fichier data.txt à l’intérieur du projet.


> Pour vous aider dans l’ouverture de fichiers en Python, regardez du côté de la documentation Python.


## ENVOI DE FICHIERS

> A l’aide de la documentation Django, mettez en place une route "/upload" capable de recevoir un fichier. Une fois le fichier correctement reçu, cette route renverra comme réponse un objet JSON contenant le nom, le type et la taille du fichier téléchargé.

> routes.py
```
from django.http import JsonResponse
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from .forms import UploadFileForm
import json
import uuid
import os

# Create your routes here

def square(request,num):
    num *= num
    return JsonResponse({"answer": num})

def concat(request,str1,str2):
    return JsonResponse({"answer": f"{str1}{str2}"})

@csrf_exempt
def jsonin(request):
    if request.method == 'POST':
        lstStudent = []
        for employee in json.loads(request.body)["employees"]:
            if employee["status"].lower() == "sTudent".lower():
                lstStudent.append(employee)
        return JsonResponse({'success':lstStudent})
    else:
        return JsonResponse({'success':False})

@csrf_exempt
def fill(request):
    if request.method == 'POST':
        file = open("data.txt","a")
        file.write(request.body.decode('utf-8')+"\n")
        file.close()

        file = open("data.txt","r")
        return  JsonResponse({'success':True})
    else:
        return JsonResponse({'success':False})

@csrf_exempt
def upload(request,filename):
    if request.method == 'POST':
        name = f'uploads/{filename}-{uuid.uuid4()}'
        with open(f'{name}', 'wb+') as file:
            file.write(request.body)
        file_stats = os.stat(name)
        return JsonResponse({'name':name,
        'file size in Bytes':file_stats.st_size,
        'file size in MegaBytes':file_stats.st_size / (1024 * 1024)})
    else:
        return JsonResponse({'success':False})
```