# Api, webservices et django

## Théorique

### Api et web services

> Une api (interface de programation) donne acces a des méthode et fonctionalité dans des programme

- Stripe: Api de paiments
- Maps: Api de cartes interactives
- Facebook: Api pour accéder au information de facebook

1. api: sur le meme environnement
2. webservice: Environnement différents

> Comment fonctionne les échange.
1. Requête
2. Webservice
3. Réponse

- On y accède grace a des applications

### Django

- Pourquoi créer son propre webservice ?

> La dernière étape pour passer d'une application web statique à une applicatopn web dynamique.

- Page personalisée pour chaque utilisateur

#### Fonctionnement

- Le framework django facilite la création d'un serveur backend qui jouera le rôle d'un webservice.

- Un framework apporte des fonctionnalités clé en main et impose une structure de projet

- Le protocol http nous sert de socle, les réponse on des format, a noter qu'une requête traité aura une seul réponse.

- Les routes permmettent au backend d'associer une requête à une traitement pour que celui-ci puisse renvoyer une réponse.

- afin de pouvoir écrire du code pour le framework django il nous faut plusieur dépendance.
> python3
> pyhton-venv
> python3-pip

- Une fois les dépendance installé, nous allons pouvoir crée notre projet

> python -m venv [project name]

- Une fois crée nous devons executé l'environement virtuel

> source env/bin/activate

- Installer le framework django

> pip3 install django

- Genérer le projet avec django dans l'environement virtuel

> django-admin startproject my_backend

- Démarer le serveur (port par defaut : 8000)

> python3 manage.py runserver

- Création d'une application webservice

> python3 manage.py startapp webservice

- Création d'une route

> nano webservice/routes.py
```
from django.http import JsonResponse

# Create your routes here

def first_route(request):
    return JsonResponse({"successful": True})
```

> nano my_backend/urls.py

```
from django.contrib import admin
from django.urls import path

from webservice import routes

urlpatterns = [
    path('admin/', admin.site.urls),
    path('test/', routes.first_route),
    path('test2/', routes.first_route)
]
```

## Pratique

### 05 File api

#### ÉCRITURE DANS UN FICHIER

> A partir du backend généré dans le challenge précédent, initialisez une nouvelle route "/fill" récupérant une chaîne de caractères en body et qui ajoute le contenu de cette chaîne de caractères dans un fichier data.txt à l’intérieur du projet.


> Pour vous aider dans l’ouverture de fichiers en Python, regardez du côté de la documentation Python.


#### ENVOI DE FICHIERS

> A l’aide de la documentation Django, mettez en place une route "/upload" capable de recevoir un fichier. Une fois le fichier correctement reçu, cette route renverra comme réponse un objet JSON contenant le nom, le type et la taille du fichier téléchargé.

> routes.py
```
from django.http import JsonResponse
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from .forms import UploadFileForm
import json
import uuid
import os

# Create your routes here

def square(request,num):
    num *= num
    return JsonResponse({"answer": num})

def concat(request,str1,str2):
    return JsonResponse({"answer": f"{str1}{str2}"})

@csrf_exempt
def jsonin(request):
    if request.method == 'POST':
        lstStudent = []
        for employee in json.loads(request.body)["employees"]:
            if employee["status"].lower() == "sTudent".lower():
                lstStudent.append(employee)
        return JsonResponse({'success':lstStudent})
    else:
        return JsonResponse({'success':False})

@csrf_exempt
def fill(request):
    if request.method == 'POST':
        file = open("data.txt","a")
        file.write(request.body.decode('utf-8')+"\n")
        file.close()

        file = open("data.txt","r")
        return  JsonResponse({'success':True})
    else:
        return JsonResponse({'success':False})

@csrf_exempt
def upload(request,filename):
    if request.method == 'POST':
        name = f'uploads/{filename}-{uuid.uuid4()}'
        with open(f'{name}', 'wb+') as file:
            file.write(request.body)
        file_stats = os.stat(name)
        return JsonResponse({'name':name,
        'file size in Bytes':file_stats.st_size,
        'file size in MegaBytes':file_stats.st_size / (1024 * 1024)})
    else:
        return JsonResponse({'success':False})
```

### 04 BASIC OPERATIONS

#### CARRÉ

> Créez un nouveau backend et initialisez une route "/square/:number" récupérant comme paramètre un entier et qui retourne le carré du nombre fourni en paramètre.

#### CONCATÉNATION

> Initialisez une route "/concat/:str1/:str2" récupérant comme paramètre deux chaînes de caractères et qui retourne la concaténation de ces deux chaînes.

#### OBJETS JSON

> Initialisez une route "/json" capable de réceptionner des requêtes de type POST.
Cette route devra récupérer l’objet JSON ci-dessous (envoyé en "body" via Thunder Client) et retourner sous forme de liste les noms des individus ayant le statut "student".

```
{

  "employees": [

    {

      "name": "Shyam",

      "email": "shyamjaiswal@gmail.com",

      "status": "admin",

      "age": 25

    },

    {

      "name": "Bob",

      "email": "bob32@gmail.com",

      "status": "student",

      "age": 45

    },

    {

      "name": "Samantha",

      "email": "samantha92@gmail.com",

      "status": "student",

      "age": 32

    },

    {

      "name": "Jai",

      "email": "jai87@gmail.com",

      "status": "teacher",

      "age": 29

    }

  ]

}
```

Notes :


- Django utilise le token csrf (Cross Site Request Forgery) pour protéger le backend de certaines attaques. Cela pose problème lors de l’envoi de requêtes de type POST.
Pour désactiver l’utilisation de ce token sur les routes en POST, il suffit d’importer le module "csrf_exempt" du package "django.views.decorators.csrf" en en-tête du fichier routes.py puis d’utiliser la mention "@csrf_exempt" juste avant de définir la fonction associée à la route :


##### routes.py
from django.http import JsonResponse

from django.views.decorators.csrf import csrf_exempt

@csrf_exempt  
def my_route_post(request):                        # ROUTE SANS CSRF

  if request.method == 'POST':
   return JsonResponse({'success': True})


def my_route_post2(request):                       # ROUTE AVEC CSRF
 if request.method == 'POST':
 return JsonResponse({'success': True})


- Lorsqu’on transmet des informations au backend, celles-ci sont transmises au format chaîne de caractère. Il faut donc les convertir en élément interprétable par Python (dictionnaire ou même liste). Pour cela, il faut utiliser le module json.


> routes.py
```
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
import json

# Create your routes here

def square(request,num):
    num *= num
    return JsonResponse({"answer": num})

def concat(request,str1,str2):
    return JsonResponse({"answer": f"{str1}{str2}"})

@csrf_exempt
def jsonin(request):
    if request.method == 'POST':
        lstStudent = []
        for employee in json.loads(request.POST["employees"]):
            if employee["status"].lower() == "sTudent".lower():
                lstStudent.append(employee)
        return JsonResponse({'success':lstStudent})
    else:
        return JsonResponse({'success':False})
```

> urls.py
```
from django.contrib import admin
from django.urls import path

from webservice import routes

urlpatterns = [
    path('admin/', admin.site.urls),
    path('square/<int:num>/', routes.square),
    path('concat/<str:str1>/<str:str2>/', routes.concat)
]
```

### 03 START WITH DJANGO

#### GÉNÉRATION DU BACKEND

Installez les dépendances systèmes nécessaires grâce à la commande suivante.


> sudo apt update && sudo apt install -y python3-venv python3-pip


-  Configurez l’environnement virtuel Python.


-  Installez Django via la commande pip3.


-  Initialisez un projet Django nommé "my_first_backend".


- Démarrez le projet puis testez-le afin vérifier qu’il est bien fonctionnel.

```
➜  06-Start_with_django python -m venv start_with_django
➜  06-Start_with_django cd start_with_django 
➜  start_with_django source bin/activate
(start_with_django) ➜  start_with_django pip install django
Collecting django
[notice] To update, run: pip install --upgrade pip
(start_with_django) ➜  start_with_django pip install --upgrade pip
(start_with_django) ➜  start_with_django django-admin startproject my_first_backend
(start_with_django) ➜  my_first_backend python3 manage.py startapp webservice
```


#### PREMIÈRE ROUTE

- Créez une nouvelle route "/test" chargée de renvoyer le résultat suivant.

```
{

  "answer": "Hello, world!"

}
```

- Crée le fichier urls dans start_with_django/my_first_backend/webservices/routes.py

```
from django.http import JsonResponse

# Create your routes here

def first_route(request):
    return JsonResponse({"answer": "Hello, world!"})
```

- Edité le fichier start_with_django/my_first_backend/my_first_backend/urls.py

```
from django.contrib import admin
from django.urls import path

from webservice import routes

urlpatterns = [
    path('admin/', admin.site.urls),
    path('test/', routes.first_route)
]
```

### 02 Fetch Quiz

En choisissant le bon webservice dans la liste ci-dessous et en utilisant Thunder Client pour récupérer des informations, répondez aux différentes questions.

Fruityvice : https://www.fruityvice.com/
Harry Potter API : https://hp-api.herokuapp.com/
Pokemon TCG API : https://pokemontcg.io/
Dictionary API :  https://dictionaryapi.dev/
Exchangerate : https://exchangerate.host/#/

1. Quel est le synonyme du mot anglais programmer ?
1. Combien de calories y a-t-il dans une fraise ?
1. A quelle famille appartient la banane ?
1. Quel est le type du pokémon Gardevoir ? (Nul besoin de vous créer un compte)
1. Quelle est l’attaque la plus puissante du pokémon Gardevoir ?
1. Combien valait 1 euro en dollars le 01/01/2019 ?
1. Combien valait 1 euro en livres sterling le 01/01/2020 ?
1. Quelle est l’année de naissance du professeur Severus Snape dans Harry Potter ?
1. Quel est le nom de l’actrice jouant le rôle de Ginny Weasley dans Harry Potter ?

```
➜  ~ curl https://api.dictionaryapi.dev/api/v2/entries/en/programmer
[{"word":"programmer","phonetic":"/ˈpɹəʊɡɹæmə/","phonetics":[{"text":"/ˈpɹəʊɡɹæmə/","audio":""},{"text":"/ˈpɹoʊɡɹæmɚ/","audio":""}],"meanings":[{"partOfSpeech":"noun","definitions":[{"definition":"One who writes computer programs; a software developer.","synonyms":["coder"],"antonyms":[]},{"definition":"One who decides which programs will be shown on a television station, or which songs will be played on a radio station.","synonyms":[],"antonyms":[]},{"definition":"A device that installs or controls a software program in some other machine.","synonyms":[],"antonyms":[]},{"definition":"A short film feature as part of a longer film program.","synonyms":[],"antonyms":[]}],"synonyms":["coder"],"antonyms":[]}],"license":{"name":"CC BY-SA 3.0","url":"https://creativecommons.org/licenses/by-sa/3.0"},"sourceUrls":["https://en.wiktionary.org/wiki/programmer"]}]
```

```
➜  ~ curl https://fruityvice.com/api/fruit/strawberry
{
    "genus": "Fragaria",
    "name": "Strawberry",
    "id": 3,
    "family": "Rosaceae",
    "order": "Rosales",
    "nutritions": {
        "carbohydrates": 5.5,
        "protein": 0.8,
        "fat": 0.4,
        "calories": 29,
        "sugar": 5.4
    }
}
```

```
➜  ~ curl 'https://api.pokemontcg.io/v2/cards?q=name:gardevoir'
{"data":[{"id":"ex9-4","name":"Gardevoir","supertype":"Pokémon","subtypes":["Stage 2"],"hp":"100","types":["Psychic"],"evolvesFrom":"Kirlia","abilities":[{"name":"Heal Dance","text":"Once during your turn (before your attack), you may remove 2 damage counter from 1 of your Pokémon. You can't use more than 1 Heal Dance Poké-Power each turn. This power can't be used if Gardevoir is affected by a Special Condition.","type":"Poké-Power"}]
```

```
"attacks": [
                {
                    "name": "Psypunch",
                    "cost": ["Psychic", "Colorless"],
                    "convertedEnergyCost": 2,
                    "damage": "30",
                    "text": "",
                },
                {
                    "name": "Mind Shock",
                    "cost": ["Psychic", "Colorless", "Colorless", "Colorless"],
                    "convertedEnergyCost": 4,
                    "damage": "60",
                    "text": "This attack's damage isn't affected by Weakness or Resistance.",
```

```
➜  ~ curl 'https://api.exchangerate.host/convert?from=USD&to=EUR&date=2019-01-01'
{"motd":{"msg":"If you or your company use this project or like what we doing, please consider backing us so we can continue maintaining and evolving this project.","url":"https://exchangerate.host/#/donate"},"success":true,"query":{"from":"USD","to":"EUR","amount":1},"info":{"rate":0.870095},"historical":true,"date":"2019-01-01","result":0.870095}
```

```
➜  ~ curl 'https://api.exchangerate.host/convert?from=EUR&to=GBP&date=2020-01-01'
{"motd":{"msg":"If you or your company use this project or like what we doing, please consider backing us so we can continue maintaining and evolving this project.","url":"https://exchangerate.host/#/donate"},"success":true,"query":{"from":"GBP","to":"EUR","amount":1},"info":{"rate":1.181754},"historical":true,"date":"2020-01-01","result":1.181754}
```

```
response1 = "coder"
response2 = "29"
response3 = "Musaceae"
response4 = "Psychic"
response5 = "Mind Shock"
response6 = "1.1493"
response7 = "0.8462"
response8 = "1960"
response9 = "Bonnie Wright"
```

### 01 Secret webservice

> Le principe de ce challenge est de s’entraîner à manipuler Thunder Client en faisant une requête en GET auprès de l’API suivante : https://secret-webservice.vercel.app/code afin de récupérer un code secret.

- A partir de Thunder Client, décortiquez la réponse renvoyée par l’API et conservez uniquement la valeur de la propriété "realSecretCode". Assignez cette valeur à la variable "code".

`curl https://secret-webservice.vercel.app/code`

`"realSecretCode":4321`