# Django

- Pourquoi créer son propre webservice ?

> La dernière étape pour passer d'une application web statique à une applicatopn web dynamique.

- Page personalisée pour chaque utilisateur

## Fonctionnement

- Le framework django facilite la création d'un serveur backend qui jouera le rôle d'un webservice.

- Un framework apporte des fonctionnalités clé en main et impose une structure de projet

- Le protocol http nous sert de socle, les réponse on des format, a noter qu'une requête traité aura une seul réponse.

- Les routes permmettent au backend d'associer une requête à une traitement pour que celui-ci puisse renvoyer une réponse.

- afin de pouvoir écrire du code pour le framework django il nous faut plusieur dépendance.
> python3
> pyhton-venv
> python3-pip

- Une fois les dépendance installé, nous allons pouvoir crée notre projet

> python -m venv [project name]

- Une fois crée nous devons executé l'environement virtuel

> source env/bin/activate

- Installer le framework django

> pip3 install django

- Genérer le projet avec django dans l'environement virtuel

> django-admin startproject my_backend

- Démarer le serveur (port par defaut : 8000)

> python3 manage.py runserver

- Création d'une application webservice

> python3 manage.py startapp webservice

- Création d'une route

> nano webservice/routes.py
```
from django.http import JsonResponse

# Create your routes here

def first_route(request):
    return JsonResponse({"successful": True})
```

> nano my_backend/urls.py

```
from django.contrib import admin
from django.urls import path

from webservice import routes

urlpatterns = [
    path('admin/', admin.site.urls),
    path('test/', routes.first_route),
    path('test2/', routes.first_route)
]
```