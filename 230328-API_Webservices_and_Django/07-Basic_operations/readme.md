# BASIC OPERATIONS

## CARRÉ

> Créez un nouveau backend et initialisez une route "/square/:number" récupérant comme paramètre un entier et qui retourne le carré du nombre fourni en paramètre.

## CONCATÉNATION

> Initialisez une route "/concat/:str1/:str2" récupérant comme paramètre deux chaînes de caractères et qui retourne la concaténation de ces deux chaînes.

## OBJETS JSON

> Initialisez une route "/json" capable de réceptionner des requêtes de type POST.
Cette route devra récupérer l’objet JSON ci-dessous (envoyé en "body" via Thunder Client) et retourner sous forme de liste les noms des individus ayant le statut "student".

```
{

  "employees": [

    {

      "name": "Shyam",

      "email": "shyamjaiswal@gmail.com",

      "status": "admin",

      "age": 25

    },

    {

      "name": "Bob",

      "email": "bob32@gmail.com",

      "status": "student",

      "age": 45

    },

    {

      "name": "Samantha",

      "email": "samantha92@gmail.com",

      "status": "student",

      "age": 32

    },

    {

      "name": "Jai",

      "email": "jai87@gmail.com",

      "status": "teacher",

      "age": 29

    }

  ]

}
```

Notes :


- Django utilise le token csrf (Cross Site Request Forgery) pour protéger le backend de certaines attaques. Cela pose problème lors de l’envoi de requêtes de type POST.
Pour désactiver l’utilisation de ce token sur les routes en POST, il suffit d’importer le module "csrf_exempt" du package "django.views.decorators.csrf" en en-tête du fichier routes.py puis d’utiliser la mention "@csrf_exempt" juste avant de définir la fonction associée à la route :


# routes.py
from django.http import JsonResponse

from django.views.decorators.csrf import csrf_exempt

@csrf_exempt  
def my_route_post(request):                        # ROUTE SANS CSRF

  if request.method == 'POST':
   return JsonResponse({'success': True})


def my_route_post2(request):                       # ROUTE AVEC CSRF
 if request.method == 'POST':
 return JsonResponse({'success': True})


- Lorsqu’on transmet des informations au backend, celles-ci sont transmises au format chaîne de caractère. Il faut donc les convertir en élément interprétable par Python (dictionnaire ou même liste). Pour cela, il faut utiliser le module json.


> routes.py
```
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
import json

# Create your routes here

def square(request,num):
    num *= num
    return JsonResponse({"answer": num})

def concat(request,str1,str2):
    return JsonResponse({"answer": f"{str1}{str2}"})

@csrf_exempt
def jsonin(request):
    if request.method == 'POST':
        lstStudent = []
        for employee in json.loads(request.POST["employees"]):
            if employee["status"].lower() == "sTudent".lower():
                lstStudent.append(employee)
        return JsonResponse({'success':lstStudent})
    else:
        return JsonResponse({'success':False})
```

> urls.py
```
from django.contrib import admin
from django.urls import path

from webservice import routes

urlpatterns = [
    path('admin/', admin.site.urls),
    path('square/<int:num>/', routes.square),
    path('concat/<str:str1>/<str:str2>/', routes.concat)
]
```