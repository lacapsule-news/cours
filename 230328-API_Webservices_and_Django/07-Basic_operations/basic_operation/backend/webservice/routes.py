from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
import json

# Create your routes here

def square(request,num):
    num *= num
    return JsonResponse({"answer": num})

def concat(request,str1,str2):
    return JsonResponse({"answer": f"{str1}{str2}"})

@csrf_exempt
def jsonin(request):
    if request.method == 'POST':
        lstStudent = []
        for employee in json.loads(request.POST["employees"]):
            if employee["status"].lower() == "sTudent".lower():
                lstStudent.append(employee)
        return JsonResponse({'success':lstStudent})
    else:
        return JsonResponse({'success':False})