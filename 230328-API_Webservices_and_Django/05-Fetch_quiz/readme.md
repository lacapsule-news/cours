# Fetch Quiz

En choisissant le bon webservice dans la liste ci-dessous et en utilisant Thunder Client pour récupérer des informations, répondez aux différentes questions.

Fruityvice : https://www.fruityvice.com/
Harry Potter API : https://hp-api.herokuapp.com/
Pokemon TCG API : https://pokemontcg.io/
Dictionary API :  https://dictionaryapi.dev/
Exchangerate : https://exchangerate.host/#/

1. Quel est le synonyme du mot anglais programmer ?
1. Combien de calories y a-t-il dans une fraise ?
1. A quelle famille appartient la banane ?
1. Quel est le type du pokémon Gardevoir ? (Nul besoin de vous créer un compte)
1. Quelle est l’attaque la plus puissante du pokémon Gardevoir ?
1. Combien valait 1 euro en dollars le 01/01/2019 ?
1. Combien valait 1 euro en livres sterling le 01/01/2020 ?
1. Quelle est l’année de naissance du professeur Severus Snape dans Harry Potter ?
1. Quel est le nom de l’actrice jouant le rôle de Ginny Weasley dans Harry Potter ?

```
➜  ~ curl https://api.dictionaryapi.dev/api/v2/entries/en/programmer
[{"word":"programmer","phonetic":"/ˈpɹəʊɡɹæmə/","phonetics":[{"text":"/ˈpɹəʊɡɹæmə/","audio":""},{"text":"/ˈpɹoʊɡɹæmɚ/","audio":""}],"meanings":[{"partOfSpeech":"noun","definitions":[{"definition":"One who writes computer programs; a software developer.","synonyms":["coder"],"antonyms":[]},{"definition":"One who decides which programs will be shown on a television station, or which songs will be played on a radio station.","synonyms":[],"antonyms":[]},{"definition":"A device that installs or controls a software program in some other machine.","synonyms":[],"antonyms":[]},{"definition":"A short film feature as part of a longer film program.","synonyms":[],"antonyms":[]}],"synonyms":["coder"],"antonyms":[]}],"license":{"name":"CC BY-SA 3.0","url":"https://creativecommons.org/licenses/by-sa/3.0"},"sourceUrls":["https://en.wiktionary.org/wiki/programmer"]}]
```

```
➜  ~ curl https://fruityvice.com/api/fruit/strawberry
{
    "genus": "Fragaria",
    "name": "Strawberry",
    "id": 3,
    "family": "Rosaceae",
    "order": "Rosales",
    "nutritions": {
        "carbohydrates": 5.5,
        "protein": 0.8,
        "fat": 0.4,
        "calories": 29,
        "sugar": 5.4
    }
}
```

```
➜  ~ curl 'https://api.pokemontcg.io/v2/cards?q=name:gardevoir'
{"data":[{"id":"ex9-4","name":"Gardevoir","supertype":"Pokémon","subtypes":["Stage 2"],"hp":"100","types":["Psychic"],"evolvesFrom":"Kirlia","abilities":[{"name":"Heal Dance","text":"Once during your turn (before your attack), you may remove 2 damage counter from 1 of your Pokémon. You can't use more than 1 Heal Dance Poké-Power each turn. This power can't be used if Gardevoir is affected by a Special Condition.","type":"Poké-Power"}]
```

```
"attacks": [
                {
                    "name": "Psypunch",
                    "cost": ["Psychic", "Colorless"],
                    "convertedEnergyCost": 2,
                    "damage": "30",
                    "text": "",
                },
                {
                    "name": "Mind Shock",
                    "cost": ["Psychic", "Colorless", "Colorless", "Colorless"],
                    "convertedEnergyCost": 4,
                    "damage": "60",
                    "text": "This attack's damage isn't affected by Weakness or Resistance.",
```

```
➜  ~ curl 'https://api.exchangerate.host/convert?from=USD&to=EUR&date=2019-01-01'
{"motd":{"msg":"If you or your company use this project or like what we doing, please consider backing us so we can continue maintaining and evolving this project.","url":"https://exchangerate.host/#/donate"},"success":true,"query":{"from":"USD","to":"EUR","amount":1},"info":{"rate":0.870095},"historical":true,"date":"2019-01-01","result":0.870095}
```

```
➜  ~ curl 'https://api.exchangerate.host/convert?from=EUR&to=GBP&date=2020-01-01'
{"motd":{"msg":"If you or your company use this project or like what we doing, please consider backing us so we can continue maintaining and evolving this project.","url":"https://exchangerate.host/#/donate"},"success":true,"query":{"from":"GBP","to":"EUR","amount":1},"info":{"rate":1.181754},"historical":true,"date":"2020-01-01","result":1.181754}
```

```
response1 = "coder"
response2 = "29"
response3 = "Musaceae"
response4 = "Psychic"
response5 = "Mind Shock"
response6 = "1.1493"
response7 = "0.8462"
response8 = "1960"
response9 = "Bonnie Wright"
```