# XSS vulnérabilité

## SETUP

Vous n’êtes pas sans savoir qu’en plus d’analyser les bugs dans le code source d’une application, SonarQube est capable d’analyser les vulnérabilités qui peuvent être critiques pour la sûreté des utilisateurs et de leurs données.

A travers ce challenge vous allez récupérer une application de tchat qui peut paraître toute simple de prime abord, mais qui semble cacher d’importantes failles de sécurité. Saurez-vous les détecter et les régler ?

👉 Récupérez la ressource "xssvulnerability.zip" depuis l’onglet dédié sur Ariane.

👉 Commencez par installer les dépendances de cette application Node.js via l’utilitaire en ligne de commande yarn.

👉 Essayez l’application pendant quelques instants puis versionnez-la sur un nouveau repository GitLab nommé "simple-chat".

## ANALYSE DES VULNÉRABILITÉS

👉 Avant la mise en place d’une pipeline d’intégration continue, lancez une analyse via une instance locale de SonarQube.

Une fois l’analyse terminée, le rapport semble mettre en avant un "Security Hotspot", c’est-à-dire une potentielle faille de sécurité qui pourrait être exploitée par des hackers afin de nuire à votre application !

👉 Tentez d’exploiter la faille rapportée par SonarQube en tapant directement un message dans le tchat.

Grâce à SonarQube, vous avez découvert une faille critique qui permettrait à n’importe quel utilisateur malveillant d’exécuter une commande côté serveur : cela lui permettrait potentiellement de voler des fichiers et installer n’importe quel programme.

👉 Créer une nouvelle branche nommée "hotfix" afin de soumettre une nouvelle version du code source responsable de la vulnérabilité via une merge request.

Lors de la création de la merge request, ne cochez pas l’option permettant de supprimer la branche d’origine.

👉 Maintenant que l’application semble sûre, mettez en place un nouveau job chargé de l’analyse du code source de l’application via SonarCloud, uniquement lors d’une merge request vers la branche principale du dépôt.

👉 Créez un second job afin de déployer l’application en production via le service Vercel (comme vu il y a quelques jours) lors d’un nouveau commit sur la branche "main".
Vercel s’occupera de créer une pipeline externe, nul besoin de modifier le fichier ".gitlab-ci.yml"

## BONUS

En regardant de plus près la liste des vulnérabilités JavaScript analysées par SonarQube, vous pouvez vous rendre compte qu’elles ne sont pas très nombreuses et pour cause, SonarQube n’est pas capable d’analyser toutes les failles existantes et leurs variantes.
Êtes-vous certain(e) que l’application ne comporte pas d'autres vulnérabilités ?


👉 Toujours via un simple message sur le tchat tentez d’exploiter un autre type de vulnérabilités JavaScript qui n’est pas directement référencé par SonarQube.

👉 Réglez cette vulnérabilité en versionnant ce fix sur la branche dédiée (hotfix) et créez une merge request vers la branche principale.