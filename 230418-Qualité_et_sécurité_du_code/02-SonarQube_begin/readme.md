# SONARQUBE BEGIN

## INSTALLATION DE SONARQUBE

SonarQube est un logiciel open source de gestion de qualité et sécurité du code, principalement utilisé pour inspecter le code source d’applications en développement afin de détecter des bugs, des vulnérabilités de sécurité ou d’autres anomalies pouvant nuire à la qualité du code source et donc au bon fonctionnement de l’application.

La mise en place de SonarQube a pour objectif d’aider les développeurs à créer un code de meilleure qualité en pointant les problèmes et en proposant des solutions adéquates pour près de 29 langages de programmation.

👉 Commencez par installer Java 11 qui est requis pour le lancement de SonarQube.

👉 Vérifiez que Java 11 soit bien installé grâce à la commande suivante.

`java -version`

👉 Suivez la documentation d’installation de SonarQube afin de télécharger et extraire l’archive zip de la "Community Edition".

👉 Placez-vous dans le dossier de SonarQube et exécutez la commande suivante afin de démarrer l’instance.
Le premier démarrage peut prendre jusqu’à 3 minutes.

`./bin/linux-x86-64/sonar.sh console`

👉 Une fois que l’instance est démarrée et fonctionnelle, visitez l’interface de SonarQube en utilisant les informations données dans la documentation.

L’instance est désormais prête à être utilisée, mais à des fins pédagogiques seulement.
En effet, une instance utilisée pour un vrai projet professionnel doit normalement être installée sur un serveur dans le cloud pour des raisons d'accessibilité et de performance.

## SCANNER UN PROJET LOCAL

👉 Depuis l’interface de SonarQube, créé un projet nommé "CovidTracker" avec "CT" comme clé de projet.

👉 Sélectionnez l’analyse de projet en local et suivez les instructions afin de générer un token et télécharger l’outil en ligne de commande sonar-scanner.
Vous pouvez ignorer l’instruction qui demande d’ajouter le répertoire "bin" dans le "PATH".

👉 Clonez le répertoire GitHub du projet open source CovidTracker : https://github.com/CovidTrackerFr/covidtracker-web 

👉 Positionnez vous dans le dossier "covidtracker-web" et grâce à la documentation et la notion de chemin absolu, exécutez la CLI de sonar-scanner afin de scanner le projet CovidTracker via SonarQube.

Une fois l’analyse terminée, vous êtes censés voir les résultats de l’analyse sur le dashboard de SonarQube.

Vous pouvez voir que de nombreux bugs sont détectés : cela ne signifie pas que l’application ne fonctionne pas en l’état, mais cela énumère plutôt chaque bug potentiel sur chaque ligne de code et pour chaque fichier analysé (ce qui explique le grand nombre de bugs détectés)

👉 Parcourez le résultat du scan et filtrez les bugs détectés par "Severity" afin de ne récupérer que le bug considéré comme critique par SonarQube.