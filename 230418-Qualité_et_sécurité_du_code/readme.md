# Qualité et sécurité du code

## Contexte

Certains bugs ne provoquent pas de crash direct de l'application, mais peuvent nuire à la stabilité de l'application.

La mauvaise conception de certaines parties du code peuvent compliquer l'évolution de fonctionnalités.

C'est ce qu'on appelle le code smell.

Un code vulnérable permettrait à une personne malveillante d'accéder à des données privées ou rendre indisponible l'application.

Il faut des outils capables d'analyser en profondeur chaque ligne de code pour détecter ces problèmes en amont.

### Fonctionnement

L'installation de SonarQube permet d'avoir un rapport détaillé des problèmes détectés dans le code.

SonarQube pourra analyser le code d'un projet local ou distant grâce à son dépôt Git.

SonarQube peut s'intégrer dans un pipeline d'intégration continue, pour déclencher l'analyse du code à chaque push.

## Exo

### 04 - XSS vulnérabilité

#### SETUP

Vous n’êtes pas sans savoir qu’en plus d’analyser les bugs dans le code source d’une application, SonarQube est capable d’analyser les vulnérabilités qui peuvent être critiques pour la sûreté des utilisateurs et de leurs données.

A travers ce challenge vous allez récupérer une application de tchat qui peut paraître toute simple de prime abord, mais qui semble cacher d’importantes failles de sécurité. Saurez-vous les détecter et les régler ?

👉 Récupérez la ressource "xssvulnerability.zip" depuis l’onglet dédié sur Ariane.

👉 Commencez par installer les dépendances de cette application Node.js via l’utilitaire en ligne de commande yarn.

👉 Essayez l’application pendant quelques instants puis versionnez-la sur un nouveau repository GitLab nommé "simple-chat".

#### ANALYSE DES VULNÉRABILITÉS

👉 Avant la mise en place d’une pipeline d’intégration continue, lancez une analyse via une instance locale de SonarQube.

Une fois l’analyse terminée, le rapport semble mettre en avant un "Security Hotspot", c’est-à-dire une potentielle faille de sécurité qui pourrait être exploitée par des hackers afin de nuire à votre application !

👉 Tentez d’exploiter la faille rapportée par SonarQube en tapant directement un message dans le tchat.

Grâce à SonarQube, vous avez découvert une faille critique qui permettrait à n’importe quel utilisateur malveillant d’exécuter une commande côté serveur : cela lui permettrait potentiellement de voler des fichiers et installer n’importe quel programme.

👉 Créer une nouvelle branche nommée "hotfix" afin de soumettre une nouvelle version du code source responsable de la vulnérabilité via une merge request.

Lors de la création de la merge request, ne cochez pas l’option permettant de supprimer la branche d’origine.

👉 Maintenant que l’application semble sûre, mettez en place un nouveau job chargé de l’analyse du code source de l’application via SonarCloud, uniquement lors d’une merge request vers la branche principale du dépôt.

👉 Créez un second job afin de déployer l’application en production via le service Vercel (comme vu il y a quelques jours) lors d’un nouveau commit sur la branche "main".
Vercel s’occupera de créer une pipeline externe, nul besoin de modifier le fichier ".gitlab-ci.yml"


#### BONUS

En regardant de plus près la liste des vulnérabilités JavaScript analysées par SonarQube, vous pouvez vous rendre compte qu’elles ne sont pas très nombreuses et pour cause, SonarQube n’est pas capable d’analyser toutes les failles existantes et leurs variantes.
Êtes-vous certain(e) que l’application ne comporte pas d'autres vulnérabilités ?


👉 Toujours via un simple message sur le tchat tentez d’exploiter un autre type de vulnérabilités JavaScript qui n’est pas directement référencé par SonarQube.

👉 Réglez cette vulnérabilité en versionnant ce fix sur la branche dédiée (hotfix) et créez une merge request vers la branche principale.

### 03 - SonarQube meet GitLab

#### SETUP

Maintenant que vous avez découvert le concept de qualité et sécurité du code dans un environnement local, il est temps d’aller plus loin en intégrant cette vérification dans une pipeline d’intégration continue via une instance SonarQube dans le cloud, grâce à SonarCloud.

👉 Créez un compte sur SonarCloud.

👉 Reprenez le repository GitLab "pokedex" issu du challenge "Deploy to Vercel" et assurez-vous que l’intégralité du code source est identique sur les branches "main" et "prod".

N’hésitez pas à repartir d’un nouveau repository GitLab si besoin.

👉 Vérifiez que la mise en production sur Vercel ne se fait qu’à partir de la branche "prod"

👉 Modifiez la visibilité du projet sur GitLab en le passant en public.
Cette étape est obligatoire pour pouvoir utiliser SonarCloud dans sa version gratuite.

#### INTÉGRATION DE SONARQUBE

👉 À partir de l’interface de SonarCloud, créez un nouveau projet nommé "Pokedex" et suivez scrupuleusement les étapes (en les adaptant si nécessaire) pour mettre en place l’analyse du code source du dépôt.


👉 Avant de lancer la première analyse et à partir de la branche "main", modifiez le fichier ".gitlab-ci.yml" afin que l’analyse du code via SonarQube ne soit effectuée que lors d’une merge request vers la branche "main" (et non pas lors d’une merge request vers "prod", par exemple).

Après quelques minutes, vous êtes censés obtenir un résumé de l’analyse de la branche principale, comme dans le screen ci-dessous.

👉 Mettez vous dans la peau d’un développeur quelques instants en créant une nouvelle branche "codequality" (toujours à partir de "main") où vous devrez régler les bugs rapportés par SonarQube, notamment sur les fichiers "index.html" et "style.css".


👉 Une fois les bugs réglés, lancez une nouvelle analyse de SonarQube en créant une merge request afin de fusionner la branche "codequality" dans la branche "main".

👉 Finissez par créer une merge request pour la branche "main" vers la branche "prod" afin de mettre en production l’application vers Vercel.

### 02 - SONARQUBE BEGIN

#### INSTALLATION DE SONARQUBE

SonarQube est un logiciel open source de gestion de qualité et sécurité du code, principalement utilisé pour inspecter le code source d’applications en développement afin de détecter des bugs, des vulnérabilités de sécurité ou d’autres anomalies pouvant nuire à la qualité du code source et donc au bon fonctionnement de l’application.

La mise en place de SonarQube a pour objectif d’aider les développeurs à créer un code de meilleure qualité en pointant les problèmes et en proposant des solutions adéquates pour près de 29 langages de programmation.

👉 Commencez par installer Java 11 qui est requis pour le lancement de SonarQube.

👉 Vérifiez que Java 11 soit bien installé grâce à la commande suivante.

`java -version`

👉 Suivez la documentation d’installation de SonarQube afin de télécharger et extraire l’archive zip de la "Community Edition".

👉 Placez-vous dans le dossier de SonarQube et exécutez la commande suivante afin de démarrer l’instance.
Le premier démarrage peut prendre jusqu’à 3 minutes.

`./bin/linux-x86-64/sonar.sh console`

👉 Une fois que l’instance est démarrée et fonctionnelle, visitez l’interface de SonarQube en utilisant les informations données dans la documentation.

L’instance est désormais prête à être utilisée, mais à des fins pédagogiques seulement.
En effet, une instance utilisée pour un vrai projet professionnel doit normalement être installée sur un serveur dans le cloud pour des raisons d'accessibilité et de performance.

#### SCANNER UN PROJET LOCAL

👉 Depuis l’interface de SonarQube, créé un projet nommé "CovidTracker" avec "CT" comme clé de projet.

👉 Sélectionnez l’analyse de projet en local et suivez les instructions afin de générer un token et télécharger l’outil en ligne de commande sonar-scanner.
Vous pouvez ignorer l’instruction qui demande d’ajouter le répertoire "bin" dans le "PATH".

👉 Clonez le répertoire GitHub du projet open source CovidTracker : https://github.com/CovidTrackerFr/covidtracker-web 

👉 Positionnez vous dans le dossier "covidtracker-web" et grâce à la documentation et la notion de chemin absolu, exécutez la CLI de sonar-scanner afin de scanner le projet CovidTracker via SonarQube.

Une fois l’analyse terminée, vous êtes censés voir les résultats de l’analyse sur le dashboard de SonarQube.

Vous pouvez voir que de nombreux bugs sont détectés : cela ne signifie pas que l’application ne fonctionne pas en l’état, mais cela énumère plutôt chaque bug potentiel sur chaque ligne de code et pour chaque fichier analysé (ce qui explique le grand nombre de bugs détectés)

👉 Parcourez le résultat du scan et filtrez les bugs détectés par "Severity" afin de ne récupérer que le bug considéré comme critique par SonarQube.