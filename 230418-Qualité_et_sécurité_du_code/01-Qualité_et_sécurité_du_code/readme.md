# Qualité et sécurité du code

## Contexte

Certains bugs ne provoquent pas de crash direct de l'application, mais peuvent nuire à la stabilité de l'application.

La mauvaise conception de certaines parties du code peuvent compliquer l'évolution de fonctionnalités.

C'est ce qu'on appelle le code smell.

Un code vulnérable permettrait à une personne malveillante d'accéder à des données privées ou rendre indisponible l'application.

Il faut des outils capables d'analyser en profondeur chaque ligne de code pour détecter ces problèmes en amont.

### Fonctionnement

L'installation de SonarQube permet d'avoir un rapport détaillé des problèmes détectés dans le code.

SonarQube pourra analyser le code d'un projet local ou distant grâce à son dépôt Git.

SonarQube peut s'intégrer dans un pipeline d'intégration continue, pour déclencher l'analyse du code à chaque push.