# SonarQube meet GitLab

## SETUP

Maintenant que vous avez découvert le concept de qualité et sécurité du code dans un environnement local, il est temps d’aller plus loin en intégrant cette vérification dans une pipeline d’intégration continue via une instance SonarQube dans le cloud, grâce à SonarCloud.

👉 Créez un compte sur SonarCloud.

👉 Reprenez le repository GitLab "pokedex" issu du challenge "Deploy to Vercel" et assurez-vous que l’intégralité du code source est identique sur les branches "main" et "prod".

N’hésitez pas à repartir d’un nouveau repository GitLab si besoin.

👉 Vérifiez que la mise en production sur Vercel ne se fait qu’à partir de la branche "prod"

👉 Modifiez la visibilité du projet sur GitLab en le passant en public.
Cette étape est obligatoire pour pouvoir utiliser SonarCloud dans sa version gratuite.

## INTÉGRATION DE SONARQUBE

👉 À partir de l’interface de SonarCloud, créez un nouveau projet nommé "Pokedex" et suivez scrupuleusement les étapes (en les adaptant si nécessaire) pour mettre en place l’analyse du code source du dépôt.


👉 Avant de lancer la première analyse et à partir de la branche "main", modifiez le fichier ".gitlab-ci.yml" afin que l’analyse du code via SonarQube ne soit effectuée que lors d’une merge request vers la branche "main" (et non pas lors d’une merge request vers "prod", par exemple).

Après quelques minutes, vous êtes censés obtenir un résumé de l’analyse de la branche principale, comme dans le screen ci-dessous.

👉 Mettez vous dans la peau d’un développeur quelques instants en créant une nouvelle branche "codequality" (toujours à partir de "main") où vous devrez régler les bugs rapportés par SonarQube, notamment sur les fichiers "index.html" et "style.css".


👉 Une fois les bugs réglés, lancez une nouvelle analyse de SonarQube en créant une merge request afin de fusionner la branche "codequality" dans la branche "main".

👉 Finissez par créer une merge request pour la branche "main" vers la branche "prod" afin de mettre en production l’application vers Vercel.