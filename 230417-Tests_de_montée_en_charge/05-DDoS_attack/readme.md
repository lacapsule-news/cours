# Ddos attack

## ATTAQUE PAR DÉNI DE SERVICE

Afin de vous rendre compte des possibilités et de la puissance de Locust, vous allez vous mettre pendant quelques instants dans la peau d’un véritable hacker en menant une attaque DDoS sur votre propre serveur !


👉 Créez un nouveau dossier nommé "ddosattack" qui pourra contenir le(s) fichier(s) de ce challenge.


👉 Installez le package Linux htop afin de superviser les ressources de la machine.


👉 Trouvez un moyen de démarrer un simple serveur web local sur le port 8585 via le module http-server de Python (à l’aide d’une seule commande sur le terminal).

👉 Une fois le serveur web démarré, créez un test de montée en charge assez conséquent et analysez les courbes sur l’onglet "Charts", notamment le temps de réponse.


👉 Arrêtez le test de montée en charge pour le moment et lancez le programme htop afin de monitorer la charge du système en temps réel.

👉 Gardez un œil sur le monitoring du système et relancez le test de montée en charge afin de constater des ressources utilisées par le serveur web (et non pas par Locust qui est censé être plus gourmand).