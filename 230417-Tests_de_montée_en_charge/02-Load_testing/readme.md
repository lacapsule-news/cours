# Load testing

## SETUP

👉 Créez et démarrez un backend Django nommé "load_testing", comme vu pendant la journée dédiée au framework.
Vous n’avez pas besoin de créer un webservice ou des routes spécifiques, seule la page d'accueil sera utilisée pour le challenge.

👉 Installez Locust sur votre machine : https://docs.locust.io/en/stable/installation.html

## PREMIER TEST DE MONTÉE EN CHARGE

👉 À la racine de votre dossier "load_testing", créez un nouveau fichier appelé "locustfile.py" contenant le code suivant.

```
from locust import HttpUser, task


class First_Load_Test(HttpUser):

    @task

    def first_test(self):

        self.client.get("/")
```

👉 Dans le même dossier, exécutez la commande suivante qui ouvrira par défaut le test contenu dans "locustfile.py".


`locust`


👉 Découvrez l’interface web de Locust en visitant l’URL suivante : http://localhost:8089


👉 Préparez votre premier test de montée en charge via l’interface graphique de Locust en précisant le nombre d’utilisateurs qui requêtent simultanément votre serveur (10 par exemple), le nombre de nouveaux utilisateurs par seconde (1 par exemple), et l’URL de votre serveur.

👉 Assurez-vous que votre serveur Django soit bien démarré avant de lancer l'opération puis analysez le résultat.

👉 Grâce aux options avancées, configurez le test de montée en charge pour qu’il s’exécute pendant 15s seulement.

👉 Lancez un nouveau directement depuis le terminal en précisant les mêmes informations en ligne de commande.


`locust --headless --users 10 --spawn-rate 1 -H http://IP:PORT -t 15s`


Vous l’avez sans doute remarqué, le résultat affiché sur le terminal est assez peu lisible…


👉 Trouvez une option à la CLI de Locust afin de n’afficher que le résumé à la fin du test de montée en charge.