# Stand the line

## SETUP DE L’APPLICATION

Certains sites de e-commerce mettent en place un système de fille d’attente pour contrôler d’importants flux de visiteurs lors de journées de grande affluence telles que le Black Friday.


Dans ce challenge, vous allez jouer ce scénario dans vos tests avec Locust et Cypress afin de vous assurer que le système de queue mis en place est fonctionnel.

👉 Récupérez le code source du serveur de l’application e-commerce sur GitLab : https://gitlab.com/la-capsule-bootcamp/bikeshop 

👉 Positionnez-vous dans le dossier et installez les dépendances (vous pouvez utiliser yarn ou npm avec Node.js).

`npm install`


👉 Lancez le serveur et vérifiez qu’il fonctionne en visitant la page http://localhost:3000 dans votre navigateur.

`npm start`

## LOAD TESTING AVEC LOCUST

👉 Lancez un test de montée en charge sur la page "/" du serveur avec 1000 utilisateurs qui visitent le site en simultané pendant 20s (à raison de 20 utilisateurs toutes les secondes).


👉 Une fois le test de montée en charge lancé avec Locust, visitez la page d'accueil de l’application depuis votre navigateur afin de vérifier si vous êtes bien redirigé vers la file d’attente.

👉 Vous pouvez éteindre le serveur Node.js (via Ctrl + C) et le relancer pour réinitialiser le décompte des requêtes. Revisitez alors le site via votre navigateur.