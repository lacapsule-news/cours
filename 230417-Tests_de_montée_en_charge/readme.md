# Test de montée en charge

## Contexte

Une application qui fonctionne avec 100 utilisateurs peut ne plus fonctionné avec 1000 utilisateurs.

Il est donc nécessaire de simuler des utilisateurs et port déterminé les limites de l'application et du serveur de production.

Pour dépasser ces seuils il est faudra probablement optimisé sont code ou midifié la puissance du serveur.

### Fonctionnement

La mise en place de framwork Locust permet de fournir un écosystème facilitant la gestion et l'éxecution des test de montée en charge.

Il est possible de paramétrer et exécuter les tests de deux façons :
- Via l'interface graphique de Locust
- En ligne de commande via le terminal

Via GitLab CI/CD, l'exécution des test pourra être déclenchée à chaque push, comme d'habitude !

On peut définir les paramètres suivant:
- Nombre d'utilisateurs simultané
- Nombre de nouveaux utilisateurs par seconde
- La durée de la sesssion de test

Une fois les test exécutés Locust affichera le détail du test de montée en charge:
- Nombre de requêtes réussies / échouées
- Temps de réponse du serveur

Si un des test n'est pas validé, la fusion de la branche est bloquée et le détail du test qui a échoué sera disponible via GitLab CI/CD

## Exo

### 05 - Ddos attack

#### ATTAQUE PAR DÉNI DE SERVICE

Afin de vous rendre compte des possibilités et de la puissance de Locust, vous allez vous mettre pendant quelques instants dans la peau d’un véritable hacker en menant une attaque DDoS sur votre propre serveur !


👉 Créez un nouveau dossier nommé "ddosattack" qui pourra contenir le(s) fichier(s) de ce challenge.


👉 Installez le package Linux htop afin de superviser les ressources de la machine.


👉 Trouvez un moyen de démarrer un simple serveur web local sur le port 8585 via le module http-server de Python (à l’aide d’une seule commande sur le terminal).

👉 Une fois le serveur web démarré, créez un test de montée en charge assez conséquent et analysez les courbes sur l’onglet "Charts", notamment le temps de réponse.


👉 Arrêtez le test de montée en charge pour le moment et lancez le programme htop afin de monitorer la charge du système en temps réel.

👉 Gardez un œil sur le monitoring du système et relancez le test de montée en charge afin de constater des ressources utilisées par le serveur web (et non pas par Locust qui est censé être plus gourmand).

### 04 - Locust in my pipeline

#### CRÉATION D’UNE PIPELINE

👉 Reprenez le dépôt local du challenge précédent.


👉 Retirez le lien avec le dépôt distant.

👉 Créez un nouveau repository GitLab nommé "bikeshop" et liez-le à votre dépôt local.

👉 Poussez la branche master sur le dépôt distant.

👉 À partir de GitLab CI/CD, créez une pipeline capable de lancer Locust pour un test de montée en charge avec 100 utilisateurs qui visitent le site en simultané pendant 5s (à raison de 20 utilisateurs toutes les secondes).


Vous devrez préciser l’image Docker suivante (à la première ligne) afin de partir sur un worker Linux avec Node.js et Python préinstallés :


image: nikolaik/python-nodejs:latest

### 03 - Stand the line

#### SETUP DE L’APPLICATION

Certains sites de e-commerce mettent en place un système de fille d’attente pour contrôler d’importants flux de visiteurs lors de journées de grande affluence telles que le Black Friday.


Dans ce challenge, vous allez jouer ce scénario dans vos tests avec Locust et Cypress afin de vous assurer que le système de queue mis en place est fonctionnel.

👉 Récupérez le code source du serveur de l’application e-commerce sur GitLab : https://gitlab.com/la-capsule-bootcamp/bikeshop 

👉 Positionnez-vous dans le dossier et installez les dépendances (vous pouvez utiliser yarn ou npm avec Node.js).

`npm install`


👉 Lancez le serveur et vérifiez qu’il fonctionne en visitant la page http://localhost:3000 dans votre navigateur.

`npm start`

#### LOAD TESTING AVEC LOCUST

👉 Lancez un test de montée en charge sur la page "/" du serveur avec 1000 utilisateurs qui visitent le site en simultané pendant 20s (à raison de 20 utilisateurs toutes les secondes).


👉 Une fois le test de montée en charge lancé avec Locust, visitez la page d'accueil de l’application depuis votre navigateur afin de vérifier si vous êtes bien redirigé vers la file d’attente.

👉 Vous pouvez éteindre le serveur Node.js (via Ctrl + C) et le relancer pour réinitialiser le décompte des requêtes. Revisitez alors le site via votre navigateur.

### 02 - Load testing

#### SETUP

👉 Créez et démarrez un backend Django nommé "load_testing", comme vu pendant la journée dédiée au framework.
Vous n’avez pas besoin de créer un webservice ou des routes spécifiques, seule la page d'accueil sera utilisée pour le challenge.

👉 Installez Locust sur votre machine : https://docs.locust.io/en/stable/installation.html

#### PREMIER TEST DE MONTÉE EN CHARGE

👉 À la racine de votre dossier "load_testing", créez un nouveau fichier appelé "locustfile.py" contenant le code suivant.

```
from locust import HttpUser, task


class First_Load_Test(HttpUser):

    @task

    def first_test(self):

        self.client.get("/")
```

👉 Dans le même dossier, exécutez la commande suivante qui ouvrira par défaut le test contenu dans "locustfile.py".


`locust`


👉 Découvrez l’interface web de Locust en visitant l’URL suivante : http://localhost:8089


👉 Préparez votre premier test de montée en charge via l’interface graphique de Locust en précisant le nombre d’utilisateurs qui requêtent simultanément votre serveur (10 par exemple), le nombre de nouveaux utilisateurs par seconde (1 par exemple), et l’URL de votre serveur.

👉 Assurez-vous que votre serveur Django soit bien démarré avant de lancer l'opération puis analysez le résultat.

👉 Grâce aux options avancées, configurez le test de montée en charge pour qu’il s’exécute pendant 15s seulement.

👉 Lancez un nouveau directement depuis le terminal en précisant les mêmes informations en ligne de commande.


`locust --headless --users 10 --spawn-rate 1 -H http://IP:PORT -t 15s`


Vous l’avez sans doute remarqué, le résultat affiché sur le terminal est assez peu lisible…


👉 Trouvez une option à la CLI de Locust afin de n’afficher que le résumé à la fin du test de montée en charge.