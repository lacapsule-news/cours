# Locust in my pipeline

## CRÉATION D’UNE PIPELINE

👉 Reprenez le dépôt local du challenge précédent.


👉 Retirez le lien avec le dépôt distant.

👉 Créez un nouveau repository GitLab nommé "bikeshop" et liez-le à votre dépôt local.

👉 Poussez la branche master sur le dépôt distant.

👉 À partir de GitLab CI/CD, créez une pipeline capable de lancer Locust pour un test de montée en charge avec 100 utilisateurs qui visitent le site en simultané pendant 5s (à raison de 20 utilisateurs toutes les secondes).


Vous devrez préciser l’image Docker suivante (à la première ligne) afin de partir sur un worker Linux avec Node.js et Python préinstallés :


image: nikolaik/python-nodejs:latest