# Test de montée en charge

## Contexte

Une application qui fonctionne avec 100 utilisateurs peut ne plus fonctionné avec 1000 utilisateurs.

Il est donc nécessaire de simuler des utilisateurs et port déterminé les limites de l'application et du serveur de production.

Pour dépasser ces seuils il est faudra probablement optimisé sont code ou midifié la puissance du serveur.

### Fonctionnement

La mise en place de framwork Locust permet de fournir un écosystème facilitant la gestion et l'éxecution des test de montée en charge.

Il est possible de paramétrer et exécuter les tests de deux façons :
- Via l'interface graphique de Locust
- En ligne de commande via le terminal

Via GitLab CI/CD, l'exécution des test pourra être déclenchée à chaque push, comme d'habitude !

On peut définir les paramètres suivant:
- Nombre d'utilisateurs simultané
- Nombre de nouveaux utilisateurs par seconde
- La durée de la sesssion de test

Une fois les test exécutés Locust affichera le détail du test de montée en charge:
- Nombre de requêtes réussies / échouées
- Temps de réponse du serveur

Si un des test n'est pas validé, la fusion de la branche est bloquée et le détail du test qui a échoué sera disponible via GitLab CI/CD
