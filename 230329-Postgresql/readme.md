# Start with postgresql

## La base de données

> C'est un endroit ou l'on va stocker durablement les données d'une application.

> Elle permet de gere et d'adapté un très grand nombre de données.

## Fonctionement

> La base de données organise et structure des données afin de travailler rapidement sur un grand nombre de données.

> Les principale opération d'un base de données sont celle qui compose le CRUD (Create, Read, Update, Delete)

> Une base de données est composé de tables, ces tables ton composé de colone comprenant des champs, les données stocké se retrouve dans ces champs.

> Un systeme de droit est présent, il est possible de crée des utilisateur pouvant acceder seulment a certaine base de données et aussi de leur attibué les action qu'il peuvent éffectuer.

## INSTALLATION ET CONFIGURATION DU SERVICE POSTGRESQL

- Installez le serveur PostgreSQL à l’aide des 3 commandes suivantes.


> sudo apt update

> sudo apt install -y postgresql postgresql-contrib

> sudo pg_ctlcluster 13 main start


- Vérifiez que votre serveur est bien installé et démarré en vous y connectant avec l’utilisateur "postgres" via sudo, puis affichez la version de PostgreSQL.


> sudo -u postgres psql
> postgres=# SELECT VERSION();


## CRÉATION D’UN UTILISATEUR LOCAL

- À l’installation, le super user par défaut est "postgres". Accédez au shell psql en tant que "postgres" et créez un nouvel utilisateur "developer" avec le mot de passe "qwerty".


> postgres=# CREATE USER developer WITH ENCRYPTED PASSWORD 'qwerty';



## CRÉATION D’UNE BASE DE DONNÉES

- Créez une base de données appelée "mydb".


> postgres=# CREATE DATABASE mydb;


- Attribuez tous les droits à l’utilisateur "developer" sur la base de données nouvellement créée.


> postgres=# GRANT ALL PRIVILEGES ON DATABASE mydb TO developer;


- Déconnectez vous de la base de données via le raccourci "Ctrl + D" puis connectez-vous à la base de données avec le nouvel utilisateur "developer"



> psql -U developer -h 127.0.0.1 -d mydb


- Contrairement à tout à l’heure, vous ne passez par sudo afin de vous connecter à la base de données, car vous n’utilisez plus l’utilisateur "postgres".


1. L’option "-U" permet de spécifier le nom de l’utilisateur avec lequel vous voulez vous connecter à la base de données
1. L’option "-h" permet de forcer la connexion à la base de données local (d’où l’IP 127.0.0.1), c’est obligatoire pour les utilisateurs autres que "postgres"
1. L’option "-d" permet de sélectionner la base de données "mydb"


## CRÉATION D’UNE TABLE
 

- Créez une nouvelle table "myusers" qui doit contenir les clés suivantes avec les types de données appropriés :
1. id
1. firstname
1. lastname
1. email
1. role
1. created_on
1. last_login

```
mydb=> CREATE TABLE myusers(

  id serial PRIMARY KEY,

  firstname VARCHAR NOT NULL,

  lastname VARCHAR NOT NULL,

  email VARCHAR UNIQUE NOT NULL,

  role VARCHAR NOT NULL,

  created_on TIMESTAMP NOT NULL,

  last_login TIMESTAMP

);

```
👉
- Affichez la liste des tables de votre base de données.


> mydb=> \d


- Enregistrez un nouvel utilisateur dans la table "myusers" avec les données proposées dans la requête SQL suivante.


> mydb=> INSERT INTO myusers(firstname, lastname, email, role, created_on, last_login) VALUES ('John', 'Doe', 'john.doe@gmail.com', 'admin', now(), now());


- Affichez le contenu de toutes les colonnes contenues dans la table "myusers".


> mydb=> SELECT * FROM myusers;


- Supprimez la table "myusers".


> mydb=> DROP TABLE myusers;

- Enfin, connectez-vous avec l’utilisateur "postgres" puis supprimez la base de données "mydb".

> postgres=# DROP DATABASE mydb;

## Pratique

### 08 - FAKE USERS

#### ENREGISTRER DES DONNÉES À PARTIR D’UN FICHIER CSV
 
> Récupérez la ressource "fakeusers.zip" depuis l’onglet dédié sur Ariane.


> Créez un script qui récupère les données dans le fichier userdata.csv et les enregistre dans une base de données nommée "fakeusers", dans la table "users".

CREATE TABLE myusers(

  id serial PRIMARY KEY,

  firstname VARCHAR NOT NULL,

  lastname VARCHAR NOT NULL,

  email VARCHAR UNIQUE NOT NULL,

  role VARCHAR NOT NULL,

  created_on TIMESTAMP NOT NULL,

  last_login TIMESTAMP

);
`export PGPASSWORD="azerty";psql -U developer -h 127.0.0.1 -d attacks -c "DROP TABLE IF EXISTS fakeusers;";psql -U developer -h 127.0.0.1 -d attacks -c "CREATE TABLE IF NOT EXISTS fakeusers(id serial PRIMARY KEY,firstname VARCHAR NOT NULL,lastname VARCHAR NOT NULL,email VARCHAR NOT NULL,role VARCHAR NOT NULL,created_on DATE NOT NULL,last_login DATE NOT NULL);";psql -U developer -h 127.0.0.1 -d attacks -c "COPY fakeusers(id,firstname,lastname,email,role,created_on,last_login) FROM '/home/sunburst/fakeusers/userdata.csv' DELIMITER ',' CSV HEADER"`



#### SUPPRIMER DES DONNÉES DEPUIS UN FICHIER CSV

> Nous avons remarqué une recrudescence de faux comptes sur le serveur. Ils sont créés avec de fausses adresses email du service Mailinator.

> Créez un script qui supprimera tous les utilisateurs dont l’email est hébergé sur Mailinator. La liste des domaines alternatifs pointant vers Mailinator se trouve dans le fichier blacklist.csv.

```
export PGPASSWORD="azerty"; cat blacklist.csv | while read a; do printf $a" "; psql -U developer -h 127.0.0.1 -d attacks -c "DELETE FROM fakeusers WHERE email like '%$a';"; done
```

### 07 - SAVE THE LOGS

#### EXTRAIRE DES INFORMATIONS D’UN FICHIER DE LOGS

- Vous êtes chargé de repérer de quels pays proviennent les tentatives de connexion non autorisées sur votre serveur. Vous allez créer un script afin d’extraire les informations utiles depuis un fichier de logs, classer ces informations, trouver les pays d’origine de ces requêtes, et enregistrer ces informations en base de données.


- Récupérez la ressource "savethelogs.zip" depuis l’onglet dédié sur Ariane.


- Dans le fichier "auth.log", repérez les informations qui correspondent aux tentatives de connexion avec un identifiant non valide.


- Créez un script bash appelé "save_attacks.sh".

- Dans ce script, utilisez la commande grep pour extraire les lignes qui contiennent la chaîne de caractères "Invalid user". Chaque ligne contient l’adresse IP à l’origine de l’attaque.

- Une fois ce premier filtrage effectué, utilisez la commande awk pour extraire l’adresse IP sur chacune de ces tentatives d’attaque.
Indice : L’adresse IP est la dixième chaîne de caractères sur chaque ligne.

- Exécutez la commande awk après grep en séparant avec un "pipe" | (barre verticale).

`cat auth.log | grep 'Invalid user' | awk '{print $NF}'`

#### ENREGISTRER LES LOGS EN BASE DE DONNÉES

- Sur votre serveur PostgreSQL, créez une base de données nommée "security". Elle contiendra une table "attacks" et ces 3 colonnes :
```
CREATE TABLE attacks(

  id serial PRIMARY KEY,

  ip INET NOT NULL,

  country VARCHAR
);
```

- Dans votre script, ajoutez une boucle qui enregistre chaque adresse IP filtrée dans la table "attacks".

#### BONUS - LOCALISATION DE L’ORIGINE DES ATTAQUES

- Utilisez l’utilitaire whois pour localiser le pays d’origine des adresses IP.  


- Installez whois avec apt et ajoutez à votre script l’exécution d’un whois à chaque tour de boucle.


- En utilisant à nouveau grep et awk, vous allez extraire le pays renvoyé par le whois (CN, US, FR, etc) et l’enregistrer dans la colonne country de chaque IP.

`export PGPASSWORD="azerty";cat auth.log | grep 'Invalid user' | awk '{print $NF}'|while read a; do psql -U developer -h 127.0.0.1 -d attacks -c "INSERT INTO attacks (ip,country) values ('$a','$(whois $a | grep -iw country|head -1|awk '{print $2}')');";done`


`export PGPASSWORD='azerty';psql -U developer -h 127.0.0.1 -d attacks -c "SELECT * FROM attacks;"`

id  |       ip        | country
------|------|------
   1 | 34.229.55.10    | US
   2 | 34.229.55.10    | US

### 06 - PGADMIN

#### INSTALLATION DE PGADMIN ET RESTAURATION D’UN DUMP

- Félicitations, vous savez désormais administrer une base de données en ligne de commande. Comme pour tous les types de bases de données, il existe également un outil d’administration graphique pour PostgreSQL.


- Récupérez la ressource "pgadmin.zip" depuis l’onglet dédié sur Ariane.


- Commencez par installer pgAdmin : https://www.pgadmin.org/


- Avant d’utiliser pgAdmin, donnez les droits "SUPERUSER" à l’utilisateur "developer".

`postgres=# ALTER USER developer WITH SUPERUSER;`

- Avec pgAdmin, connectez-vous à votre base de données en local (127.0.0.1) via l’utilisateur "developer".


- Une première base de données nommée postgres est créée par défaut. Nous allons créer notre propre base de données. Pour ce faire, faîtes un clic droit sur Databases > Create > Database …

- Nommez votre base de données frenchtowns. Celle-ci apparaît désormais dans la liste des bases de données. Faîtes un clic droit sur frenchtowns > Restore … puis sélectionnez le dump frenchtownsdump.sql 

- Ce dump partagé par l’INSEE contient les 36684 communes de France et leurs départements associés.


#### AFFICHAGE ET ÉDITION DES DONNÉES

- Affichez toutes les communes présentes dans la table towns : Schemas > Public > Tables.


- Les trois tables apparaissent, faites un clic droit sur la table towns > View/Edit data.

- pgAdmin permet aussi de manipuler votre base de données en écrivant vos propres requêtes PostgreSQL.

- Afficher toutes les communes du département 13 commençant par la lettre M.


> SELECT * FROM towns WHERE department = '13' AND name LIKE 'M%';


- Vous pouvez désormais exécuter des instructions PostgreSQL comme sur l’outil psql en ligne de commandes. Clic droit sur votre base de données > Query tools.Vous exécuterez votre requête en cliquant sur le bouton de lecture ▶️.

- Vous avez sans doute remarqué qu’il manque la plus grande ville du département 13 : Marseille. En repassant par l’interface graphique, ajoutez la ville de Marseille (avec pour code la valeur “055”) et enregistrez le changement.

`INSERT INTO towns (code,name,department) VALUES ('055','Marseille',13);`

#### SAUVEGARDE DE LA BASE DE DONNÉES

- Souvenez-vous de pg_dump, l’outil qui permet de faire une sauvegarde de base de données. Nous allons effectuer la même manipulation dans l’interface graphique pgAdmin. Pour ce faire, faites un clic droit sur votre base de données > Backup…

### 05 - PLAY WITH REQUESTS

#### AFFICHAGE ET MISE À JOUR DES DONNÉES

- Récupérez la ressource "playwithrequests.zip" depuis l’onglet dédié sur Ariane.


- Restaurez le dump fourni dans une nouvelle base de données nommée "playwithrequests".


- Affichez tous les utilisateurs de la table users.



- Affichez uniquement les utilisateurs dont le rôle est égal "admin" ou "moderator" grâce à la requête SQL suivante.


```
playwithrequests=> SELECT * FROM users WHERE role = 'admin' OR role = 'moderator';
```

- Vous n’êtes censés voir que 3 utilisateurs : Jean, Ernest et Thomas.


- L’utilisateur Jerome Dufour a fait une erreur dans son adresse email. Modifiez son adresse email actuelle par "jerome.dufour@lacapsule.academy" grâce à la requête SQL quivante.


> playwithrequests=> UPDATE users SET email = 'jerome.dufour@lacapsule.academy' WHERE firstname = 'Jerome' AND lastname = 'Dufour';


- Affichez tous les utilisateurs de la table afin de vérifier que l’adresse email de Jerome a été mise à jour.

```
play_with_request=> SELECT * FROM users WHERE firstname='Jerome';
 id | firstname | lastname |       email        |   role   |     created_on      |     last_login
----+-----------+----------+--------------------+----------+---------------------+---------------------
 17 | Jerome    | Dufour   | jeromedf@gmail.com | customer | 2021-03-23 13:23:00 | 2021-03-24 16:00:22
(1 ligne)

play_with_request=> UPDATE users SET email = 'jerome.dufour@lacapsule.academy' WHERE firstname = 'Jerome' AND lastname = 'Dufour';
UPDATE 1
play_with_request=> SELECT * FROM users WHERE firstname='Jerome';
 id | firstname | lastname |              email              |   role   |     created_on      |     last_login
----+-----------+----------+---------------------------------+----------+---------------------+---------------------
 17 | Jerome    | Dufour   | jerome.dufour@lacapsule.academy | customer | 2021-03-23 13:23:00 | 2021-03-24 16:00:22
 ```

### 04 - AUTO DUMP

#### RESTAURATION D’UN DUMP POSTGRESQL

- Récupérez la ressource "autodump.zip" depuis l’onglet dédié sur Ariane.

- Créez une nouvelle base de données nommée "autodump" puis importez le dump fourni.

#### AUTOMATISATION DU DUMP

- Une fois le dump restauré, créez un script qui sera chargé d’exporter dans un nouveau dump une table uniquement. Vous passerez le nom de la table à "dumper" en argument lors du lancement du script.

alias exportDB='f(){ pg_dump -U developer -h 127.0.0.1 --format=p --file=newdump.sql "$@" ; unset -f f; }; f'

### 03 - ARE YOU AWAKE?

#### STATUT D’UNE BASE DE DONNÉES

- Exécutez une commande afin de vous assurer que votre service PostgreSQL répond correctement. Vous pouvez chercher dans la documentation PostgreSQL.

> pg_isready --dbname=dumpmydatabase --host=localhost --username=developer

- Intégrez cette commande dans un script qui fera cette vérification toutes les 5 secondes.

`watch -n 5 pg_isready --dbname=dumpmydatabase --host=localhost --username=developer`

### 02 - DUMP MY DATABASE

#### RESTAURATION D’UN DUMP POSTGRESQL

- Récupérez la ressource "dumpmydatabase.zip" depuis l’onglet dédié sur Ariane.

- Créez une nouvelle base de données nommée "dumpmydatabase".

`postgres=# CREATE DATABASE dumpmydatabase;`

- Attribuez tous les droits à l’utilisateur "developer" sur la base de données nouvellement créée.

`GRANT ALL PRIVILEGES ON DATABASE dumpmydatabase TO developer;`

- Importez le dump fourni grâce à la commande suivante. Le dump contient la structure et le contenu de la base de données.


> psql -U developer -h 127.0.0.1 -d dumpmydatabase -f dump.sql


- Vérifiez que le dump a bien été important en récupérant le contenu des tables "users" puis "logs".

```
dumpmydatabase=> SELECT * FROM users LIMIT 1;
 id | firstname | lastname |      email       | role  |     created_on      |     last_login
----+-----------+----------+------------------+-------+---------------------+---------------------
  1 | Jean      | Boyat    | jeanboyat@me.com | admin | 2018-06-01 12:20:05 | 2022-04-18 02:24:56
```

```
dumpmydatabase=> SELECT * FROM logs LIMIT 1;
 id |      ip       | service | status |     login_time      |     logout_time
----+---------------+---------+--------+---------------------+---------------------
 15 | 194.1.151.101 | Apache2 |     10 | 2022-08-24 12:17:41 | 2022-08-24 12:17:55
```

#### EXPORTATION D’UN DUMP POSTGRESQL

- Une fois la base de données restaurée, ajoutez un nouvel utilisateur dans la table users.


> INSERT INTO users(firstname, lastname, email, role, created_on, last_login) VALUES ('John', 'Doe', 'john.doe@gmail.com', 'admin', now(), now());


- Enfin, exportez un nouveau dump nommé "newdump.sql" grâce à la commande Linux suivante. Le dump contient la structure et le contenu de la base de données, y compris le nouvel utilisateur créé précédemment.


> pg_dump -U developer -h 127.0.0.1 --format=p --file=newdump.sql dumpmydatabase  


⚠️ Attention : cette commande doit être exécutée dans votre terminal et non pas lorsque vous êtes connecté à une base de données via psql.