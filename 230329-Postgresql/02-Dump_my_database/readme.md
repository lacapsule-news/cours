# DUMP MY DATABASE

## RESTAURATION D’UN DUMP POSTGRESQL

- Récupérez la ressource "dumpmydatabase.zip" depuis l’onglet dédié sur Ariane.

- Créez une nouvelle base de données nommée "dumpmydatabase".

`postgres=# CREATE DATABASE dumpmydatabase;`

- Attribuez tous les droits à l’utilisateur "developer" sur la base de données nouvellement créée.

`GRANT ALL PRIVILEGES ON DATABASE dumpmydatabase TO developer;`

- Importez le dump fourni grâce à la commande suivante. Le dump contient la structure et le contenu de la base de données.


> psql -U developer -h 127.0.0.1 -d dumpmydatabase -f dump.sql


- Vérifiez que le dump a bien été important en récupérant le contenu des tables "users" puis "logs".

```
dumpmydatabase=> SELECT * FROM users LIMIT 1;
 id | firstname | lastname |      email       | role  |     created_on      |     last_login
----+-----------+----------+------------------+-------+---------------------+---------------------
  1 | Jean      | Boyat    | jeanboyat@me.com | admin | 2018-06-01 12:20:05 | 2022-04-18 02:24:56
```

```
dumpmydatabase=> SELECT * FROM logs LIMIT 1;
 id |      ip       | service | status |     login_time      |     logout_time
----+---------------+---------+--------+---------------------+---------------------
 15 | 194.1.151.101 | Apache2 |     10 | 2022-08-24 12:17:41 | 2022-08-24 12:17:55
```

## EXPORTATION D’UN DUMP POSTGRESQL

- Une fois la base de données restaurée, ajoutez un nouvel utilisateur dans la table users.


> INSERT INTO users(firstname, lastname, email, role, created_on, last_login) VALUES ('John', 'Doe', 'john.doe@gmail.com', 'admin', now(), now());


- Enfin, exportez un nouveau dump nommé "newdump.sql" grâce à la commande Linux suivante. Le dump contient la structure et le contenu de la base de données, y compris le nouvel utilisateur créé précédemment.


> pg_dump -U developer -h 127.0.0.1 --format=p --file=newdump.sql dumpmydatabase  


⚠️ Attention : cette commande doit être exécutée dans votre terminal et non pas lorsque vous êtes connecté à une base de données via psql.