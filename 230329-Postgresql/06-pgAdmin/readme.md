# PGADMIN

## INSTALLATION DE PGADMIN ET RESTAURATION D’UN DUMP

- Félicitations, vous savez désormais administrer une base de données en ligne de commande. Comme pour tous les types de bases de données, il existe également un outil d’administration graphique pour PostgreSQL.


- Récupérez la ressource "pgadmin.zip" depuis l’onglet dédié sur Ariane.


- Commencez par installer pgAdmin : https://www.pgadmin.org/


- Avant d’utiliser pgAdmin, donnez les droits "SUPERUSER" à l’utilisateur "developer".

`postgres=# ALTER USER developer WITH SUPERUSER;`

- Avec pgAdmin, connectez-vous à votre base de données en local (127.0.0.1) via l’utilisateur "developer".


- Une première base de données nommée postgres est créée par défaut. Nous allons créer notre propre base de données. Pour ce faire, faîtes un clic droit sur Databases > Create > Database …

- Nommez votre base de données frenchtowns. Celle-ci apparaît désormais dans la liste des bases de données. Faîtes un clic droit sur frenchtowns > Restore … puis sélectionnez le dump frenchtownsdump.sql 

- Ce dump partagé par l’INSEE contient les 36684 communes de France et leurs départements associés.


## AFFICHAGE ET ÉDITION DES DONNÉES

- Affichez toutes les communes présentes dans la table towns : Schemas > Public > Tables.


- Les trois tables apparaissent, faites un clic droit sur la table towns > View/Edit data.

- pgAdmin permet aussi de manipuler votre base de données en écrivant vos propres requêtes PostgreSQL.

- Afficher toutes les communes du département 13 commençant par la lettre M.


> SELECT * FROM towns WHERE department = '13' AND name LIKE 'M%';


- Vous pouvez désormais exécuter des instructions PostgreSQL comme sur l’outil psql en ligne de commandes. Clic droit sur votre base de données > Query tools.Vous exécuterez votre requête en cliquant sur le bouton de lecture ▶️.

- Vous avez sans doute remarqué qu’il manque la plus grande ville du département 13 : Marseille. En repassant par l’interface graphique, ajoutez la ville de Marseille (avec pour code la valeur “055”) et enregistrez le changement.

`INSERT INTO towns (code,name,department) VALUES ('055','Marseille',13);`

## SAUVEGARDE DE LA BASE DE DONNÉES

- Souvenez-vous de pg_dump, l’outil qui permet de faire une sauvegarde de base de données. Nous allons effectuer la même manipulation dans l’interface graphique pgAdmin. Pour ce faire, faites un clic droit sur votre base de données > Backup…