# PLAY WITH REQUESTS

## AFFICHAGE ET MISE À JOUR DES DONNÉES

- Récupérez la ressource "playwithrequests.zip" depuis l’onglet dédié sur Ariane.


- Restaurez le dump fourni dans une nouvelle base de données nommée "playwithrequests".


- Affichez tous les utilisateurs de la table users.



- Affichez uniquement les utilisateurs dont le rôle est égal "admin" ou "moderator" grâce à la requête SQL suivante.


```
playwithrequests=> SELECT * FROM users WHERE role = 'admin' OR role = 'moderator';
```

- Vous n’êtes censés voir que 3 utilisateurs : Jean, Ernest et Thomas.


- L’utilisateur Jerome Dufour a fait une erreur dans son adresse email. Modifiez son adresse email actuelle par "jerome.dufour@lacapsule.academy" grâce à la requête SQL quivante.


> playwithrequests=> UPDATE users SET email = 'jerome.dufour@lacapsule.academy' WHERE firstname = 'Jerome' AND lastname = 'Dufour';


- Affichez tous les utilisateurs de la table afin de vérifier que l’adresse email de Jerome a été mise à jour.

```
play_with_request=> SELECT * FROM users WHERE firstname='Jerome';
 id | firstname | lastname |       email        |   role   |     created_on      |     last_login
----+-----------+----------+--------------------+----------+---------------------+---------------------
 17 | Jerome    | Dufour   | jeromedf@gmail.com | customer | 2021-03-23 13:23:00 | 2021-03-24 16:00:22
(1 ligne)

play_with_request=> UPDATE users SET email = 'jerome.dufour@lacapsule.academy' WHERE firstname = 'Jerome' AND lastname = 'Dufour';
UPDATE 1
play_with_request=> SELECT * FROM users WHERE firstname='Jerome';
 id | firstname | lastname |              email              |   role   |     created_on      |     last_login
----+-----------+----------+---------------------------------+----------+---------------------+---------------------
 17 | Jerome    | Dufour   | jerome.dufour@lacapsule.academy | customer | 2021-03-23 13:23:00 | 2021-03-24 16:00:22
 ```