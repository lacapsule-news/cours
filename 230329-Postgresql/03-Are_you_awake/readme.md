# ARE YOU AWAKE?

## STATUT D’UNE BASE DE DONNÉES

- Exécutez une commande afin de vous assurer que votre service PostgreSQL répond correctement. Vous pouvez chercher dans la documentation PostgreSQL.

> pg_isready --dbname=dumpmydatabase --host=localhost --username=developer

- Intégrez cette commande dans un script qui fera cette vérification toutes les 5 secondes.

`watch -n 5 pg_isready --dbname=dumpmydatabase --host=localhost --username=developer`