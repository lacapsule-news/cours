# SAVE THE LOGS

## EXTRAIRE DES INFORMATIONS D’UN FICHIER DE LOGS

- Vous êtes chargé de repérer de quels pays proviennent les tentatives de connexion non autorisées sur votre serveur. Vous allez créer un script afin d’extraire les informations utiles depuis un fichier de logs, classer ces informations, trouver les pays d’origine de ces requêtes, et enregistrer ces informations en base de données.


- Récupérez la ressource "savethelogs.zip" depuis l’onglet dédié sur Ariane.


- Dans le fichier "auth.log", repérez les informations qui correspondent aux tentatives de connexion avec un identifiant non valide.


- Créez un script bash appelé "save_attacks.sh".

- Dans ce script, utilisez la commande grep pour extraire les lignes qui contiennent la chaîne de caractères "Invalid user". Chaque ligne contient l’adresse IP à l’origine de l’attaque.

- Une fois ce premier filtrage effectué, utilisez la commande awk pour extraire l’adresse IP sur chacune de ces tentatives d’attaque.
Indice : L’adresse IP est la dixième chaîne de caractères sur chaque ligne.

- Exécutez la commande awk après grep en séparant avec un "pipe" | (barre verticale).

`cat auth.log | grep 'Invalid user' | awk '{print $NF}'`

## ENREGISTRER LES LOGS EN BASE DE DONNÉES

- Sur votre serveur PostgreSQL, créez une base de données nommée "security". Elle contiendra une table "attacks" et ces 3 colonnes :
```
CREATE TABLE attacks(

  id serial PRIMARY KEY,

  ip INET NOT NULL,

  country VARCHAR
);
```

- Dans votre script, ajoutez une boucle qui enregistre chaque adresse IP filtrée dans la table "attacks".

## BONUS - LOCALISATION DE L’ORIGINE DES ATTAQUES

- Utilisez l’utilitaire whois pour localiser le pays d’origine des adresses IP.  


- Installez whois avec apt et ajoutez à votre script l’exécution d’un whois à chaque tour de boucle.


- En utilisant à nouveau grep et awk, vous allez extraire le pays renvoyé par le whois (CN, US, FR, etc) et l’enregistrer dans la colonne country de chaque IP.

`export PGPASSWORD="azerty";cat auth.log | grep 'Invalid user' | awk '{print $NF}'|while read a; do psql -U developer -h 127.0.0.1 -d attacks -c "INSERT INTO attacks (ip,country) values ('$a','$(whois $a | grep -iw country|head -1|awk '{print $2}')');";done`


`export PGPASSWORD='azerty';psql -U developer -h 127.0.0.1 -d attacks -c "SELECT * FROM attacks;"`

id  |       ip        | country
------|------|------
   1 | 34.229.55.10    | US
   2 | 34.229.55.10    | US