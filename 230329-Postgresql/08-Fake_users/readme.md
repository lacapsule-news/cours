# FAKE USERS

## ENREGISTRER DES DONNÉES À PARTIR D’UN FICHIER CSV
 
> Récupérez la ressource "fakeusers.zip" depuis l’onglet dédié sur Ariane.


> Créez un script qui récupère les données dans le fichier userdata.csv et les enregistre dans une base de données nommée "fakeusers", dans la table "users".

CREATE TABLE myusers(

  id serial PRIMARY KEY,

  firstname VARCHAR NOT NULL,

  lastname VARCHAR NOT NULL,

  email VARCHAR UNIQUE NOT NULL,

  role VARCHAR NOT NULL,

  created_on TIMESTAMP NOT NULL,

  last_login TIMESTAMP

);
`export PGPASSWORD="azerty";psql -U developer -h 127.0.0.1 -d attacks -c "DROP TABLE IF EXISTS fakeusers;";psql -U developer -h 127.0.0.1 -d attacks -c "CREATE TABLE IF NOT EXISTS fakeusers(id serial PRIMARY KEY,firstname VARCHAR NOT NULL,lastname VARCHAR NOT NULL,email VARCHAR NOT NULL,role VARCHAR NOT NULL,created_on DATE NOT NULL,last_login DATE NOT NULL);";psql -U developer -h 127.0.0.1 -d attacks -c "COPY fakeusers(id,firstname,lastname,email,role,created_on,last_login) FROM '/home/sunburst/fakeusers/userdata.csv' DELIMITER ',' CSV HEADER"`



## SUPPRIMER DES DONNÉES DEPUIS UN FICHIER CSV

> Nous avons remarqué une recrudescence de faux comptes sur le serveur. Ils sont créés avec de fausses adresses email du service Mailinator.

> Créez un script qui supprimera tous les utilisateurs dont l’email est hébergé sur Mailinator. La liste des domaines alternatifs pointant vers Mailinator se trouve dans le fichier blacklist.csv.

```
export PGPASSWORD="azerty"; cat blacklist.csv | while read a; do printf $a" "; psql -U developer -h 127.0.0.1 -d attacks -c "DELETE FROM fakeusers WHERE email like '%$a';"; done
```