# Start with postgresql

## La base de données

> C'est un endroit ou l'on va stocker durablement les données d'une application.

> Elle permet de gere et d'adapté un très grand nombre de données.

## Fonctionement

> La base de données organise et structure des données afin de travailler rapidement sur un grand nombre de données.

> Les principale opération d'un base de données sont celle qui compose le CRUD (Create, Read, Update, Delete)

> Une base de données est composé de tables, ces tables ton composé de colone comprenant des champs, les données stocké se retrouve dans ces champs.

> Un systeme de droit est présent, il est possible de crée des utilisateur pouvant acceder seulment a certaine base de données et aussi de leur attibué les action qu'il peuvent éffectuer.

## INSTALLATION ET CONFIGURATION DU SERVICE POSTGRESQL

- Installez le serveur PostgreSQL à l’aide des 3 commandes suivantes.


> sudo apt update

> sudo apt install -y postgresql postgresql-contrib

> sudo pg_ctlcluster 13 main start


- Vérifiez que votre serveur est bien installé et démarré en vous y connectant avec l’utilisateur "postgres" via sudo, puis affichez la version de PostgreSQL.


> sudo -u postgres psql
> postgres=# SELECT VERSION();


## CRÉATION D’UN UTILISATEUR LOCAL

- À l’installation, le super user par défaut est "postgres". Accédez au shell psql en tant que "postgres" et créez un nouvel utilisateur "developer" avec le mot de passe "qwerty".


> postgres=# CREATE USER developer WITH ENCRYPTED PASSWORD 'qwerty';



## CRÉATION D’UNE BASE DE DONNÉES

- Créez une base de données appelée "mydb".


> postgres=# CREATE DATABASE mydb;


- Attribuez tous les droits à l’utilisateur "developer" sur la base de données nouvellement créée.


> postgres=# GRANT ALL PRIVILEGES ON DATABASE mydb TO developer;


- Déconnectez vous de la base de données via le raccourci "Ctrl + D" puis connectez-vous à la base de données avec le nouvel utilisateur "developer"



> psql -U developer -h 127.0.0.1 -d mydb


- Contrairement à tout à l’heure, vous ne passez par sudo afin de vous connecter à la base de données, car vous n’utilisez plus l’utilisateur "postgres".


1. L’option "-U" permet de spécifier le nom de l’utilisateur avec lequel vous voulez vous connecter à la base de données
1. L’option "-h" permet de forcer la connexion à la base de données local (d’où l’IP 127.0.0.1), c’est obligatoire pour les utilisateurs autres que "postgres"
1. L’option "-d" permet de sélectionner la base de données "mydb"


## CRÉATION D’UNE TABLE
 

- Créez une nouvelle table "myusers" qui doit contenir les clés suivantes avec les types de données appropriés :
1. id
1. firstname
1. lastname
1. email
1. role
1. created_on
1. last_login

```
mydb=> CREATE TABLE myusers(

  id serial PRIMARY KEY,

  firstname VARCHAR NOT NULL,

  lastname VARCHAR NOT NULL,

  email VARCHAR UNIQUE NOT NULL,

  role VARCHAR NOT NULL,

  created_on TIMESTAMP NOT NULL,

  last_login TIMESTAMP

);

```
👉
- Affichez la liste des tables de votre base de données.


> mydb=> \d


- Enregistrez un nouvel utilisateur dans la table "myusers" avec les données proposées dans la requête SQL suivante.


> mydb=> INSERT INTO myusers(firstname, lastname, email, role, created_on, last_login) VALUES ('John', 'Doe', 'john.doe@gmail.com', 'admin', now(), now());


- Affichez le contenu de toutes les colonnes contenues dans la table "myusers".


> mydb=> SELECT * FROM myusers;


- Supprimez la table "myusers".


> mydb=> DROP TABLE myusers;

- Enfin, connectez-vous avec l’utilisateur "postgres" puis supprimez la base de données "mydb".

> postgres=# DROP DATABASE mydb;