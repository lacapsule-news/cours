# AUTO DUMP

## RESTAURATION D’UN DUMP POSTGRESQL

- Récupérez la ressource "autodump.zip" depuis l’onglet dédié sur Ariane.

- Créez une nouvelle base de données nommée "autodump" puis importez le dump fourni.

## AUTOMATISATION DU DUMP

- Une fois le dump restauré, créez un script qui sera chargé d’exporter dans un nouveau dump une table uniquement. Vous passerez le nom de la table à "dumper" en argument lors du lancement du script.

alias exportDB='f(){ pg_dump -U developer -h 127.0.0.1 --format=p --file=newdump.sql "$@" ; unset -f f; }; f'