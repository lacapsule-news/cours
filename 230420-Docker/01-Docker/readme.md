# Docker

## Contexte

Une application a besoin d'être hébergée sur un serveur pour délivré un service h24.

Les applications vont devoir cohabiter sur les même serveur et donc partager les dépendances et les ressources.

Si une application pose problème, elle peut perturber les autre applications.

Pour gagner en stabilité et souplesse il est préférable de cloisinner les applications grâce à la notion de conteneur.

### Fonctionnement

Chaque application qui va être intalée dans son propre conteneur et pourra fonctionner en autonomie.

Chaque conteneur va pouvoir être configurer individuellement au traers d'une image Docker.

Une image est un OS minimal sur lequel des outils (Python, Nginx, Node.js) sont préinstallés.

Un conteneur est une machine autonome dans laquelle on peut exécuter des commandes et qui peut être démarrée, supprimée et dupliquée très rapidement.

Les modifications à apporter à un conteneur devront être faites dans le fichier Dockerfile afin de créer une nouvelle image.

L'image permet à un conteneur qui est recrée et retrouver toutes ses modifications.

L'activité de chaque conteneur peut être monitorée en temps réel.