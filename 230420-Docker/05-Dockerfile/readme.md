# DOCKERFILE

## CRÉATION D’UN DOCKERFILE

👉 Reprenez le conteneur créé dans le challenge "My best friend Nginx" afin de le redémarrer.

👉 Exécutez une requête vers le serveur web afin de constater que le message inséré précédemment est toujours persistant.

👉 Stoppez et supprimez le container "webserver" afin de le recréer avec le même nom et à partir de la même image nginx.

👉 Exécutez de nouveau une requête vers le serveur web afin de constater que le message inséré précédemment a disparu.

Cela s’explique par le fait que toutes les modifications apportées précédemment ont été menées dans le conteneur qui ne reste qu’un espace temporaire, mais pas au niveau de l’image : c’est que vous allez apprendre à faire à travers ce challenge !

👉 Stoppez et supprimez le container "webserver", il ne sera plus basé sur l’image nginx, mais sur votre propre image Docker dans quelques instants.

👉 Créez un fichier Dockerfile et insérez-y les instructions suivantes.

```
FROM nginx:latest
RUN touch /test.txt
```

Ce premier Dockerfile est assez simple pour commencer, car il se contente de reprendre l’image nginx dans sa dernière version (latest) et de créer un fichier "test.txt" à la racine du conteneur.

👉 À partir de la commande docker build et du Dockerfile, créez une image nommée "mynginx".

👉 Vérifiez la création de votre image personnalisée via la commande docker images.

Sachez qu’il est également possible de préciser une autre version en suivant la syntaxe suivante lors du build : "nomimage:version".

## UTILISATION D’UNE IMAGE PERSONNALISÉE

👉 Levez un container Docker nommé "webserver2" à partir de cette image "mynginx".

👉 Exécutez une requête vers le serveur web afin de constater qu’il est bien déployé.

Si vous récupérez une page HTML avec un message de bienvenue de Nginx, c’est que tout est opérationnel !

👉 Vérifiez si le fichier "test.txt" créé lors du build de l’image est bien présent à la racine du container.

## L’EXPLORATEUR D’IMAGES DIVE

👉 Installez l’explorateur d’images Docker dive.

👉 Exécutez dive sur votre image "mynginx" afin de visualiser chaque étape de build et l’impact sur le système de fichier du futur conteneur.

`dive mynginx`