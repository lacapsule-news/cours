# MY BEST FRIEND NGINX

## LE RÉSEAU AVEC DOCKER

👉 Levez un conteneur Docker nommé "webserver" basé sur l’image officielle de nginx : https://hub.docker.com/_/nginx 

Attention, à l’inverse de l’image debian, vous devrez lever le conteneur en mode détaché (via l’option -d) afin qu’il puisse se démarrer en tâche de fond.

👉 Vérifiez les informations du conteneur via la commande docker ps, notamment la colonne port qui montre que le port 80 est disponible.

👉 Tentez une requête vers le serveur web via curl.

`curl http://localhost`

Si vous avez une erreur du type "Failed to connect", c’est tout à fait normal, car le port 80 a beau être disponible dans le conteneur, il n’est pas exposé à la machine hôte.
C’est tout le principe des conteneurs : l’isolation, c’est à dire que de base, tout ce qui se passe dans un conteneur reste dans un conteneur.

👉 Supprimez le conteneur précédemment créé et re-créez en un nouveau avec l’option -p en précisant le port que l’on souhaite "bind" sur la machine hôte ainsi que le port cible.

`docker run -d -p 8080:80 --name webserver nginx`

Grâce à cette commande, le port 8080 sur la machine hôte redirige vers le port 80 à l’intérieur du conteneur.

👉 Vérifiez les informations du conteneur via la commande docker ps, notamment la colonne qui montre qu’à partir toutes les adresses IP (0.0.0.0), le port 8080 redirigera vers le port 80.

👉 Re-tentez une requête vers le serveur web, mais cette fois-ci sur le port 8080.

`curl http://localhost:8080`

Si vous récupérez une page HTML avec un message de bienvenue de Nginx, c’est que tout est opérationnel !

## LES LOGS AVEC DOCKER

👉 Dans la documentation des commandes Docker, trouvez la commande et l’option vous permettant de consulter les logs du conteneur en temps réel. Essayez de faire une nouvelle requête afin de voir si elle est affichée dans les logs.

👉 Trouvez un moyen "d’entrer" dans le conteneur afin de modifier le contenu du fichier chargé d’afficher le message de bienvenue de Nginx pour le remplacer par votre propre message.