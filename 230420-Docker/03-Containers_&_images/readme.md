# CONTAINERS & IMAGES

## RÉCUPÉRATION D’UNE IMAGE

Une image Docker est un modèle en lecture seule, utilisée pour créer des conteneurs. Elle est composée de plusieurs couches empaquetant toutes les installations et dépendances nécessaires pour disposer d’un environnement de conteneur opérationnel.

👉 Listez les images disponibles sur le système.

`docker images`

Vous êtes censé voir l’image "hello-world" précédemment utilisée. Elle a été téléchargée automatiquement lors du lancement du conteneur, mais vous verrez qu’il est possible de récupérer une image sans lancer systématiquement un conteneur.

👉 À partir de la bibliothèque d’images Docker Hub, recherchez l’image officielle du système d’exploitation Debian et récupérez-la via la commande docker pull.

👉 Vérifiez que l’image a bien été téléchargée via la commande docker images.

Vous pouvez voir que l’image "debian" dans sa version "latest" est beaucoup plus lourde, ce qui est logique, car il s’agit d’un système d’exploitation en version "light".

Pour information, si aucun tag n’est précisé, la version "latest" sera récupérée.

## UTILISATION D’UNE IMAGE

👉 Levez un conteneur à partir de l’image "debian". Le conteneur devra porter le nom "mydebian" plutôt qu’un nom auto-généré par Docker.

Une fois la commande exécutée, rien ne semble s’être passé et pourtant aucune erreur ne s’est affichée.

👉 Regardez du côté de la commande docker ps afin de voir l’état du conteneur et la commande lancée.

On peut voir que le conteneur était censé lancer un terminal bash et a été quitté : c’est tout à fait normal, car nous n’avons pas lancer le conteneur en mode "interactif" afin de pouvoir directement lier le conteneur à notre terminal.

👉 Supprimez le conteneur "mydebian" et recréez le conteneur avec l’option -it afin de le lancer en mode interactif et le contrôler grâce à un terminal bash.

Bravo, vous avez réussi à lancer un conteneur Debian sur un système d’exploitation Debian : debianception 🤯

Vous pouvez vous amuser à faire quelques commandes Linux même si vous apprendrez plus tard à rendre cet environnement persistant et à faire des choses plus poussées évidemment.

👉 Pour finir, quittez le terminal interactif avec la commande exit et supprimez le container

Vous devrez arrêter le container avant de le supprimer.

```
docker stop mydebian
docker rm mydebian
```