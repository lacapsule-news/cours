# ADVANCED DOCKERFILE

## CONFIGURATION D’UN SERVEUR WEB

👉 Grâce à un nouveau fichier Dockerfile, créez votre propre image Docker nommée "nginx-lacapsule" qui permettra de déployer un serveur web suivant les contraintes suivantes :

- Le serveur web Nginx sera utilisé en version stable (et non pas latest comme dans les challenges précédents)
- Le header de réponse ne devra pas afficher la version de Nginx (visible grâce à l’option -v de curl)
- Le port d’écoute de Nginx devra être remplacé par 81
- La page de bienvenue de Nginx sera remplacée par le fichier HTML fourni dans les ressources du challenge
- Fichier de configuration "/etc/nginx/conf.d/default.conf" :
`Remove`
- Fichier de configuration "/etc/nginx/nginx.conf" :
```
user nginx;
worker_processes 5;
error_log /var/log/nginx/error.log notice;
pid /var/run/nginx.pid;
worker_rlimit_nofile 8192;

events {
    worker_connections 4096;
}

http {
    server {
        listen 81;
        server_tokens off;
        root /data/www/;
        index index-lacapsule.html;
    }
}
```
- Dockerfile :
```
FROM nginx:stable

RUN mkdir -p /data/www
RUN rm /etc/nginx/nginx.conf /etc/nginx/conf.d/default.conf
COPY lacapsule.site/index-lacapsule.html /data/www
COPY lacapsule.site/nginx.conf /etc/nginx/nginx.conf
```

👉 Une fois le Dockerfile prêt, créez l’image "nginx-lacapsule" qui devra être taguée avec "alpha" en guise de version (en plus de "latest" automatiquement)

`docker build -t lacapsule:dev -t lacapsule:alpha .`

👉 Vérifiez que chaque contrainte du challenge est respectée en démarrant un conteneur et en requêtant le serveur grâce aux commandes suivantes.

```
docker run -d -p 8080:81 --name webserver nginx-lacapsule:alpha
curl -v http://localhost:8080
```