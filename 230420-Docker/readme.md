# Docker

## Contexte

Une application a besoin d'être hébergée sur un serveur pour délivré un service h24.

Les applications vont devoir cohabiter sur les même serveur et donc partager les dépendances et les ressources.

Si une application pose problème, elle peut perturber les autre applications.

Pour gagner en stabilité et souplesse il est préférable de cloisinner les applications grâce à la notion de conteneur.

### Fonctionnement

Chaque application qui va être intalée dans son propre conteneur et pourra fonctionner en autonomie.

Chaque conteneur va pouvoir être configurer individuellement au traers d'une image Docker.

Une image est un OS minimal sur lequel des outils (Python, Nginx, Node.js) sont préinstallés.

Un conteneur est une machine autonome dans laquelle on peut exécuter des commandes et qui peut être démarrée, supprimée et dupliquée très rapidement.

Les modifications à apporter à un conteneur devront être faites dans le fichier Dockerfile afin de créer une nouvelle image.

L'image permet à un conteneur qui est recrée et retrouver toutes ses modifications.

L'activité de chaque conteneur peut être monitorée en temps réel.

## Exo

### 06 - ADVANCED DOCKERFILE

#### CONFIGURATION D’UN SERVEUR WEB

👉 Grâce à un nouveau fichier Dockerfile, créez votre propre image Docker nommée "nginx-lacapsule" qui permettra de déployer un serveur web suivant les contraintes suivantes :

- Le serveur web Nginx sera utilisé en version stable (et non pas latest comme dans les challenges précédents)
- Le header de réponse ne devra pas afficher la version de Nginx (visible grâce à l’option -v de curl)
- Le port d’écoute de Nginx devra être remplacé par 81
- La page de bienvenue de Nginx sera remplacée par le fichier HTML fourni dans les ressources du challenge
- Fichier de configuration "/etc/nginx/conf.d/default.conf" :
`Remove`
- Fichier de configuration "/etc/nginx/nginx.conf" :
```
user nginx;
worker_processes 5;
error_log /var/log/nginx/error.log notice;
pid /var/run/nginx.pid;
worker_rlimit_nofile 8192;

events {
    worker_connections 4096;
}

http {
    server {
        listen 81;
        server_tokens off;
        root /data/www/;
        index index-lacapsule.html;
    }
}
```
- Dockerfile :
```
FROM nginx:stable

RUN mkdir -p /data/www
RUN rm /etc/nginx/nginx.conf /etc/nginx/conf.d/default.conf
COPY lacapsule.site/index-lacapsule.html /data/www
COPY lacapsule.site/nginx.conf /etc/nginx/nginx.conf
```

👉 Une fois le Dockerfile prêt, créez l’image "nginx-lacapsule" qui devra être taguée avec "alpha" en guise de version (en plus de "latest" automatiquement)

`docker build -t lacapsule:dev -t lacapsule:alpha .`

👉 Vérifiez que chaque contrainte du challenge est respectée en démarrant un conteneur et en requêtant le serveur grâce aux commandes suivantes.

```
docker run -d -p 8080:81 --name webserver nginx-lacapsule:alpha
curl -v http://localhost:8080
```

### 05 - DOCKERFILE

#### CRÉATION D’UN DOCKERFILE

👉 Reprenez le conteneur créé dans le challenge "My best friend Nginx" afin de le redémarrer.

👉 Exécutez une requête vers le serveur web afin de constater que le message inséré précédemment est toujours persistant.

👉 Stoppez et supprimez le container "webserver" afin de le recréer avec le même nom et à partir de la même image nginx.

👉 Exécutez de nouveau une requête vers le serveur web afin de constater que le message inséré précédemment a disparu.

Cela s’explique par le fait que toutes les modifications apportées précédemment ont été menées dans le conteneur qui ne reste qu’un espace temporaire, mais pas au niveau de l’image : c’est que vous allez apprendre à faire à travers ce challenge !

👉 Stoppez et supprimez le container "webserver", il ne sera plus basé sur l’image nginx, mais sur votre propre image Docker dans quelques instants.

👉 Créez un fichier Dockerfile et insérez-y les instructions suivantes.

```
FROM nginx:latest
RUN touch /test.txt
```

Ce premier Dockerfile est assez simple pour commencer, car il se contente de reprendre l’image nginx dans sa dernière version (latest) et de créer un fichier "test.txt" à la racine du conteneur.

👉 À partir de la commande docker build et du Dockerfile, créez une image nommée "mynginx".

👉 Vérifiez la création de votre image personnalisée via la commande docker images.

Sachez qu’il est également possible de préciser une autre version en suivant la syntaxe suivante lors du build : "nomimage:version".

#### UTILISATION D’UNE IMAGE PERSONNALISÉE

👉 Levez un container Docker nommé "webserver2" à partir de cette image "mynginx".

👉 Exécutez une requête vers le serveur web afin de constater qu’il est bien déployé.

Si vous récupérez une page HTML avec un message de bienvenue de Nginx, c’est que tout est opérationnel !

👉 Vérifiez si le fichier "test.txt" créé lors du build de l’image est bien présent à la racine du container.

#### L’EXPLORATEUR D’IMAGES DIVE

👉 Installez l’explorateur d’images Docker dive.

👉 Exécutez dive sur votre image "mynginx" afin de visualiser chaque étape de build et l’impact sur le système de fichier du futur conteneur.

`dive mynginx`

### 04 - MY BEST FRIEND NGINX

#### LE RÉSEAU AVEC DOCKER

👉 Levez un conteneur Docker nommé "webserver" basé sur l’image officielle de nginx : https://hub.docker.com/_/nginx 

Attention, à l’inverse de l’image debian, vous devrez lever le conteneur en mode détaché (via l’option -d) afin qu’il puisse se démarrer en tâche de fond.

👉 Vérifiez les informations du conteneur via la commande docker ps, notamment la colonne port qui montre que le port 80 est disponible.

👉 Tentez une requête vers le serveur web via curl.

`curl http://localhost`

Si vous avez une erreur du type "Failed to connect", c’est tout à fait normal, car le port 80 a beau être disponible dans le conteneur, il n’est pas exposé à la machine hôte.
C’est tout le principe des conteneurs : l’isolation, c’est à dire que de base, tout ce qui se passe dans un conteneur reste dans un conteneur.

👉 Supprimez le conteneur précédemment créé et re-créez en un nouveau avec l’option -p en précisant le port que l’on souhaite "bind" sur la machine hôte ainsi que le port cible.

`docker run -d -p 8080:80 --name webserver nginx`

Grâce à cette commande, le port 8080 sur la machine hôte redirige vers le port 80 à l’intérieur du conteneur.

👉 Vérifiez les informations du conteneur via la commande docker ps, notamment la colonne qui montre qu’à partir toutes les adresses IP (0.0.0.0), le port 8080 redirigera vers le port 80.

👉 Re-tentez une requête vers le serveur web, mais cette fois-ci sur le port 8080.

`curl http://localhost:8080`

Si vous récupérez une page HTML avec un message de bienvenue de Nginx, c’est que tout est opérationnel !

#### LES LOGS AVEC DOCKER

👉 Dans la documentation des commandes Docker, trouvez la commande et l’option vous permettant de consulter les logs du conteneur en temps réel. Essayez de faire une nouvelle requête afin de voir si elle est affichée dans les logs.

👉 Trouvez un moyen "d’entrer" dans le conteneur afin de modifier le contenu du fichier chargé d’afficher le message de bienvenue de Nginx pour le remplacer par votre propre message.

### 03 - CONTAINERS & IMAGES

#### RÉCUPÉRATION D’UNE IMAGE

Une image Docker est un modèle en lecture seule, utilisée pour créer des conteneurs. Elle est composée de plusieurs couches empaquetant toutes les installations et dépendances nécessaires pour disposer d’un environnement de conteneur opérationnel.

👉 Listez les images disponibles sur le système.

`docker images`

Vous êtes censé voir l’image "hello-world" précédemment utilisée. Elle a été téléchargée automatiquement lors du lancement du conteneur, mais vous verrez qu’il est possible de récupérer une image sans lancer systématiquement un conteneur.

👉 À partir de la bibliothèque d’images Docker Hub, recherchez l’image officielle du système d’exploitation Debian et récupérez-la via la commande docker pull.

👉 Vérifiez que l’image a bien été téléchargée via la commande docker images.

Vous pouvez voir que l’image "debian" dans sa version "latest" est beaucoup plus lourde, ce qui est logique, car il s’agit d’un système d’exploitation en version "light".

Pour information, si aucun tag n’est précisé, la version "latest" sera récupérée.

#### UTILISATION D’UNE IMAGE

👉 Levez un conteneur à partir de l’image "debian". Le conteneur devra porter le nom "mydebian" plutôt qu’un nom auto-généré par Docker.

Une fois la commande exécutée, rien ne semble s’être passé et pourtant aucune erreur ne s’est affichée.

👉 Regardez du côté de la commande docker ps afin de voir l’état du conteneur et la commande lancée.

On peut voir que le conteneur était censé lancer un terminal bash et a été quitté : c’est tout à fait normal, car nous n’avons pas lancer le conteneur en mode "interactif" afin de pouvoir directement lier le conteneur à notre terminal.

👉 Supprimez le conteneur "mydebian" et recréez le conteneur avec l’option -it afin de le lancer en mode interactif et le contrôler grâce à un terminal bash.

Bravo, vous avez réussi à lancer un conteneur Debian sur un système d’exploitation Debian : debianception 🤯

Vous pouvez vous amuser à faire quelques commandes Linux même si vous apprendrez plus tard à rendre cet environnement persistant et à faire des choses plus poussées évidemment.

👉 Pour finir, quittez le terminal interactif avec la commande exit et supprimez le container

Vous devrez arrêter le container avant de le supprimer.

```
docker stop mydebian
docker rm mydebian
```

### 02 - Docker begin

#### INSTALLATION DE DOCKER

👉 Commencez par installer la CLI de Docker : https://docs.docker.com/engine/install/debian/ 

👉 Avant de continuer, exécutez les commandes suivantes afin de pouvoir utiliser la CLI de Docker sans passer par sudo.

```
sudo usermod -aG docker $USER

newgrp docker
```

👉 Vérifiez l’installation de Docker.

`docker --version`

#### MANIPULATION D’UN CONTENEUR

Un conteneur Docker est une machine virtualisée légère et autonome, qui comprend tous les éléments nécessaires pour exécuter une application.

👉 Lancez un premier conteneur à partir d’une simple image nommé "hello-word" et créée par l’équipe de Docker.

`docker run hello-world`

👉 Prenez le temps de regarder le retour de la commande précédente. Celle-ci décrit les étapes menées par Docker pour lever le conteneur.

👉 Listez les conteneurs de la machine hôte.

`docker ps -a`

👉 Une nouvelle fois, prenez le temps de regarder le retour de la commande précédente.
Celle ci sera très utile afin de voir l’état des conteneurs Docker à tout moment :

CONTAINER ID : Identifiant unique du conteneur
IMAGE : Image utilisée pour lever le conteneur (ici "hello-world" où son seul rôle sera d’afficher un message sur le terminal)
COMMAND : Commande exécutée dans le conteneur ("/hello" qui correspond à un script créé par l’équipe de Docker pour afficher le message que vous avez pu voir tout à l’heure)
CREATED : Sans grande surprise, le délai écoulé depuis la création du container
STATUS : Information très importante, l’état du container ("exited" car il a été levé, la commande a été exécutée, puis le conteneur a été quitté)
PORTS : La liste des ports exposés entre le conteneur et la machine hôte (aucun pour le moment, car ce n’est pas utile)
NAMES : Le nom unique de chaque container pour les manipuler plus facilement (il est auto généré par Docker ou spécifié lors de la commande docker run avec l’option --name)

👉 Enfin, supprimez le conteneur via son ID ou directement son nom.

`docker rm {CONTAINER ID or NAME}`