# Déploiement continu

## Contexte

Chaque évolution ou correction d'une application fera l'objet d'une mise en production.

La configuration de l'environnement de production est souvent très différente de l'environnement de développement.

L'application risque de ne pas avoir le même comportement.

La création d'un environnement de test permet d'avoir un environnement qui correspond à celui de production.

La mise en place d'une pipeline est cécessaire pour que l'équipe de développement puisse travailler facilement entre les différents environnements.

### Fonctionnement

L'intégration et le déploiement continu sont indissociables.
Pourtant, chacun à un rôle et un périmètre bien précis.

- L'itégration continue représente les automatisation mises en place impliquant le **code source**
- Déploiement continu représente les automatisations mises en place impliquant le **serveur de production**

Gitlab permet via un fichier de configuration de mettre en place des automatisations qui se déclencheront à chaque push.

Les services d'hébergement peuvent proposer leur propre service de déploiment automatique, sans passer par un fichier de configuration gitlab ci/cd.