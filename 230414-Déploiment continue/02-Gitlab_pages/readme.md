# Gitlab pages

## DÉPLOIEMENT D’UN SITE STATIQUE

Les GitLab Pages permettent de publier un site statique directement à partir d’un repository GitLab. Cette méthode est souvent utilisée pour les projets open sources ou les équipes de développement qui souhaitent publier une documentation liée à leur dépôt GitLab.

Dans votre cas, les GitLab Pages seront très utiles pour vous entraîner à déployer un simple site web.

👉 Créer un répertoire GitLab nommé "mystaticpage" et suivez la documentation afin de créer une GitLab Page via le générateur de site statique (SGG) Jekyll.

👉 Après avoir suivi les étapes de création des fichiers du projet (dont le fichier ".gitlab-ci.yml"), poussez vos commits vers la branche main afin de lancer le job chargé de déployer le site en statique.

👉 Enfin, vérifiez que le site statique a bien été déployé en visant l’URL donnée sur GitLab dans "Settings > Pages".