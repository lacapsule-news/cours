# Deployment preview

## ENVIRONNEMENT DE PREVIEW

Vercel est également capable de gérer des environnements de preview (parfois appelés pre-prod) qui sont utiles pour essayer une nouvelle fonctionnalité dans en environnement semblable à celui de production.

👉 Reprenez le répertoire GitLab créé dans le challenge précédent.

👉 Créez une nouvelle branche nommée "newfeature" et faîtes une modification dessus. Poussez cette branche vers GitLab.

👉 Faîtes une demande de merge request de la branche "newfeature" vers "prod" et récupérez l’URL de preview donnée par Vercel en commentaire afin de la visiter.