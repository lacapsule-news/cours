# Protected branches

## PROTECTION D’UNE BRANCHE

👉 Récupérez la ressource "protectedbranches.zip" depuis l’onglet dédié sur Ariane.


👉 Créez un répertoire GitLab nommé "protectedbranches" et poussez le code précédemment récupéré sur "main" ainsi qu'une nouvelle branche "prod".

👉 Déployez l’application vers Vercel, uniquement à partir de la branche "prod".


👉 Visitez l’URL "/api" sur le site en production. Une réponse en JSON est censée être affichée.

👉 Mettez-vous dans la peau d’un développeur inexpérimenté (ou maladroit) en supprimant le code de la ligne 4 à 6 dans le fichier "routes/index.js" puis poussez directement sur la branche "prod".

👉 Visitez de nouveau l’URL "/api". Une erreur est censée s’afficher.


Problème : le site en production crash et personne n’a pu vérifier le problème en amont car il est possible de push directement sur la branche "prod", sans passer par une merge request.


👉 À partir de GitLab, protégez la branche "prod" afin de forcer la création d’une merge request lors d’une mise en production.