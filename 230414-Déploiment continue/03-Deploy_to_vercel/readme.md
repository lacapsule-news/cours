# DEPLOY TO VERCEL

## SETUP

Il existe peu de façons d’héberger son code source en ligne (GitHub, GitLab, Bitbucket…), mais à contrario, il existe une multitude de plateformes capables d’héberger une application frontend et/ou backend.

À travers ce challenge, vous allez découvrir Vercel, une plateforme assez appréciée par les développeurs et les DevOps car elle capable de déployer une application frontend très facilement et sans passer par une pipeline complexe de CI/CD.


👉 Récupérez la ressource "deploytovercel.zip" depuis l’onglet dédié sur Ariane.


👉 Créez un répertoire GitLab nommé "pokedex" et poussez le code précédemment récupéré.

## DÉPLOIEMENT D’UNE APPLICATION WEB

👉 Déployez l’application vers Vercel en créant un lien avec le projet GitLab.


👉 Une fois le site déployé, visitez-le afin de vérifier que tout s’est bien passé.


👉 Modifiez le nom de domaine attribué par Vercel afin de suivre le format suivant : pokedex-votreprenom-datedujour.vercel.app (par exemple : pokedex-antoine-2512.vercel.app)


👉 Modifiez le fichier "index.html" et poussez votre commit vers la branche main.

Vercel est censé trigger le changement et re-déployer l’application ! Magique n’est-ce pas ? 🪄


👉 Par défaut, Vercel déploie la branche main. Modifiez ce paramètre pour que la branche "prod" soit déployée.


👉 Depuis GitLab, créez la branche "prod" afin de vérifier si l’étape précédente a bien été réalisée.

Pour conclure, il est tout à fait possible de créer plusieurs projets Vercel pour un seul répertoire git afin de multiplier les environnements (test, pré-production, production)