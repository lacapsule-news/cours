# Déploiement continu

## Contexte

Chaque évolution ou correction d'une application fera l'objet d'une mise en production.

La configuration de l'environnement de production est souvent très différente de l'environnement de développement.

L'application risque de ne pas avoir le même comportement.

La création d'un environnement de test permet d'avoir un environnement qui correspond à celui de production.

La mise en place d'une pipeline est cécessaire pour que l'équipe de développement puisse travailler facilement entre les différents environnements.

### Fonctionnement

L'intégration et le déploiement continu sont indissociables.
Pourtant, chacun à un rôle et un périmètre bien précis.

- L'itégration continue représente les automatisation mises en place impliquant le **code source**
- Déploiement continu représente les automatisations mises en place impliquant le **serveur de production**

Gitlab permet via un fichier de configuration de mettre en place des automatisations qui se déclencheront à chaque push.

Les services d'hébergement peuvent proposer leur propre service de déploiment automatique, sans passer par un fichier de configuration gitlab ci/cd.

## Exo

### Production logs

#### TEST D’INTÉGRATION CONTINUE

Les exemples des challenges précédents vous ont permis de découvrir quelques fonctionnalités de GitLab CI/CD. Vous allez maintenant appliquer le concept de continuous intégration (CI) en exécutant un linter de code.

👉 Récupérez la ressource "productionlogs.zip" depuis l’onglet dédié sur Ariane.

👉 Créez un répertoire GitLab nommé "productionlogs" et poussez le code précédemment récupéré.

👉 Déployez l’application vers Vercel.


👉 Visitez l’URL "/api" et constatez que le navigateur vous renvoie une erreur peu parlante : "Internal Server Error".


👉 Vérifiez si le déploiement s’est bien passé en commençant par regarder côté GitLab, puis côté Vercel dans l’onglet "Deployments".


👉 Le déploiement semble s’être bien passé. Regardez du côté des logs, sur la page du projet bouton "View functions logs" et relancez une requête afin de déterminer la cause du problème.


👉 Une fois l’erreur identifiée et même si celle-ci doit être réglée par l’équipement de développement, corrigez-là.

### 05 - Protected branches

#### PROTECTION D’UNE BRANCHE

👉 Récupérez la ressource "protectedbranches.zip" depuis l’onglet dédié sur Ariane.


👉 Créez un répertoire GitLab nommé "protectedbranches" et poussez le code précédemment récupéré sur "main" ainsi qu'une nouvelle branche "prod".

👉 Déployez l’application vers Vercel, uniquement à partir de la branche "prod".


👉 Visitez l’URL "/api" sur le site en production. Une réponse en JSON est censée être affichée.

👉 Mettez-vous dans la peau d’un développeur inexpérimenté (ou maladroit) en supprimant le code de la ligne 4 à 6 dans le fichier "routes/index.js" puis poussez directement sur la branche "prod".

👉 Visitez de nouveau l’URL "/api". Une erreur est censée s’afficher.


Problème : le site en production crash et personne n’a pu vérifier le problème en amont car il est possible de push directement sur la branche "prod", sans passer par une merge request.


👉 À partir de GitLab, protégez la branche "prod" afin de forcer la création d’une merge request lors d’une mise en production.

### 04 - Deployment preview

#### ENVIRONNEMENT DE PREVIEW

Vercel est également capable de gérer des environnements de preview (parfois appelés pre-prod) qui sont utiles pour essayer une nouvelle fonctionnalité dans en environnement semblable à celui de production.

👉 Reprenez le répertoire GitLab créé dans le challenge précédent.

👉 Créez une nouvelle branche nommée "newfeature" et faîtes une modification dessus. Poussez cette branche vers GitLab.

👉 Faîtes une demande de merge request de la branche "newfeature" vers "prod" et récupérez l’URL de preview donnée par Vercel en commentaire afin de la visiter.

### 03 - DEPLOY TO VERCEL

#### SETUP

Il existe peu de façons d’héberger son code source en ligne (GitHub, GitLab, Bitbucket…), mais à contrario, il existe une multitude de plateformes capables d’héberger une application frontend et/ou backend.

À travers ce challenge, vous allez découvrir Vercel, une plateforme assez appréciée par les développeurs et les DevOps car elle capable de déployer une application frontend très facilement et sans passer par une pipeline complexe de CI/CD.


👉 Récupérez la ressource "deploytovercel.zip" depuis l’onglet dédié sur Ariane.


👉 Créez un répertoire GitLab nommé "pokedex" et poussez le code précédemment récupéré.

#### DÉPLOIEMENT D’UNE APPLICATION WEB

👉 Déployez l’application vers Vercel en créant un lien avec le projet GitLab.


👉 Une fois le site déployé, visitez-le afin de vérifier que tout s’est bien passé.


👉 Modifiez le nom de domaine attribué par Vercel afin de suivre le format suivant : pokedex-votreprenom-datedujour.vercel.app (par exemple : pokedex-antoine-2512.vercel.app)


👉 Modifiez le fichier "index.html" et poussez votre commit vers la branche main.

Vercel est censé trigger le changement et re-déployer l’application ! Magique n’est-ce pas ? 🪄


👉 Par défaut, Vercel déploie la branche main. Modifiez ce paramètre pour que la branche "prod" soit déployée.


👉 Depuis GitLab, créez la branche "prod" afin de vérifier si l’étape précédente a bien été réalisée.

Pour conclure, il est tout à fait possible de créer plusieurs projets Vercel pour un seul répertoire git afin de multiplier les environnements (test, pré-production, production)

### 02 - Gitlab pages

#### DÉPLOIEMENT D’UN SITE STATIQUE

Les GitLab Pages permettent de publier un site statique directement à partir d’un repository GitLab. Cette méthode est souvent utilisée pour les projets open sources ou les équipes de développement qui souhaitent publier une documentation liée à leur dépôt GitLab.

Dans votre cas, les GitLab Pages seront très utiles pour vous entraîner à déployer un simple site web.

👉 Créer un répertoire GitLab nommé "mystaticpage" et suivez la documentation afin de créer une GitLab Page via le générateur de site statique (SGG) Jekyll.

👉 Après avoir suivi les étapes de création des fichiers du projet (dont le fichier ".gitlab-ci.yml"), poussez vos commits vers la branche main afin de lancer le job chargé de déployer le site en statique.

👉 Enfin, vérifiez que le site statique a bien été déployé en visant l’URL donnée sur GitLab dans "Settings > Pages".