# Production logs

## TEST D’INTÉGRATION CONTINUE

Les exemples des challenges précédents vous ont permis de découvrir quelques fonctionnalités de GitLab CI/CD. Vous allez maintenant appliquer le concept de continuous intégration (CI) en exécutant un linter de code.

👉 Récupérez la ressource "productionlogs.zip" depuis l’onglet dédié sur Ariane.

👉 Créez un répertoire GitLab nommé "productionlogs" et poussez le code précédemment récupéré.

👉 Déployez l’application vers Vercel.


👉 Visitez l’URL "/api" et constatez que le navigateur vous renvoie une erreur peu parlante : "Internal Server Error".


👉 Vérifiez si le déploiement s’est bien passé en commençant par regarder côté GitLab, puis côté Vercel dans l’onglet "Deployments".


👉 Le déploiement semble s’être bien passé. Regardez du côté des logs, sur la page du projet bouton "View functions logs" et relancez une requête afin de déterminer la cause du problème.


👉 Une fois l’erreur identifiée et même si celle-ci doit être réglée par l’équipement de développement, corrigez-là.