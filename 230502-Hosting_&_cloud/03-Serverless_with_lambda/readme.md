# SERVERLESS WITH LAMBDA

## PUISSANCE DE CALCUL SANS SERVEUR

👉 Commencez par vous documenter sur la notion de serverless à l’aide du lien suivant (première partie de l’article) : https://www.cloudflare.com/fr-fr/learning/serverless/what-is-serverless/

AWS Lambda est un service de calcul d'événement sans serveur (serverless) qui permet aux développeurs de pouvoir exécuter du code pour tout type d'application, sans se soucier de l'allocation ou de la gestion des serveurs.

👉 Sur AWS Lambda, créez une nouvelle fonction nommée "myfirstfunction" en sélectionnant l’option "Créer à partir de zéro". L’exécution sera basée sur la dernière version de Python.

Le fichier "lambda_function.py" contient une fonction "lambda_handler" qui sera le point d’entrée du script lors de son exécution par AWS Lambda.

👉 Modifiez la fonction "lambda_handler" en y insérant un simple print du message de votre choix.

👉 Cliquez sur le bouton "Test" afin de créer un événement de test basé sur le modèle "hello-world" (qui n’envoie que des données de test en simulant un service) afin de lancer une exécution de test sur votre fonction.

👉 Une fois l’événement de test créé, cliquez de nouveau sur le bouton "Test" et vérifiez l’exécution grâce au nouvel onglet ouvert afin de retrouver le print inséré dans le code Python.

Le service AWS Lambda s’exécute à partir d’un déclencheur (trigger), par exemple avec le service S3 :

Nouveau fichier uploadé > Exécution de la lambda en envoyant des infos au format JSON (nom du fichier, date de création, taille, etc…) > Traitement du fichier (renommer le fichier avec la date de création, par exemple)

👉 Grâce aux règles du service CloudWatch, trouvez un moyen d’exécuter votre fonction "myfirstfunction" toutes les 5 minutes.

Le service CloudWatch est basé le principe de cron-job qui est capable d’effectuer des actions répétées, par exemple :
Toutes les 5 minutes, récupérer le cours des cryptomonnaies via une API afin de créer un fichier CSV sur S3 qui pourra par la suite être téléchargé et utilisé par une autre application.

👉 Assurez-vous de résilier et libérer toutes les ressources crées afin d’éviter une surfacturation et de dépasser le tier gratuit d’AWS :

Serverless : supprimez la fonction "myfirstfunction"
Cloudwatch : supprimez la règle permettant l’exécution automatique toutes les 5 minutes