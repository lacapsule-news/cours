# STATIC DEPLOYMENT WITH CODEPIPELINE

## SETUP

Le service CodeSuite d’AWS propose des outils de développements utiles tout au long de la vie d’un projet tels que CodeBuild, CodePipeline, etc.

La philosophie d’AWS est avec CodeSuite est de proposer une expérience unifiée afin de créer plus facilement des automatisations entre les services d’AWS (vers S3, par exemple).

L’intérêt de ce challenge (qui sera moins guidé que le précédent) est d’automatiser le déploiement du frontend de l’application Ultimate Timer afin que chaque push sur le dépôt de code puisse entraîner une mise à jour automatique du projet hébergé sur S3.

👉 Après avoir sélectionné le service CodeBuild, rendez-vous dans les settings des "Developpers tools" afin de créer une nouvelle connexion avec votre compte GitHub. .

👉 Créez un bucket S3 vide nommé "ultimatetimer-front" et paramétrez le service CloudFront afin de publier sur une URL unique les fichiers statiques qui seront déployés dans ce bucket.

👉 Récupérez la ressource "ultimatetimer-front.zip" depuis l’onglet dédié sur Ariane.

👉 Avant de versionner le code source sur GitHub, modifiez la première ligne du fichier "script.js" et plus précisément le contenu de la variable "BACKEND_URL" afin d’y insérer le lien de l’API déployée dans le challenge précédent, le tout entre guillemets.

👉 Créez un nouveau repository GitHub privé nommé "UltimateTimer-Front" et hébergez-y le code source du frontend du projet.

## CRÉATION D’UNE PIPELINE VIA AWS

Maintenant que les pré-requis sont remplis, il est temps de s’attaquer aux différents outils de CodeSuite afin d’automatiser le déploiement du frontend de l’application Ultimate Timer.

👉 Faites en sorte que toutes les modifications publiées soient automatiquement uploadées vers le bucket S3 créé précédemment grâce au service CodePipeline.

👉 Ajoutez un simple texte dans le fichier "index.html" du projet afin de vérifier le déploiement en visitant l’URL publique donnée par CloudFront.

👉 Avant de passer au challenge suivant, veillez à supprimer toutes les ressources créées depuis le début de la journée afin d’éviter une facturation indésirable : AppRunner, CodePipeline et le bucket S3.