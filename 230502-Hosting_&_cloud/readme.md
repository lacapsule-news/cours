# Hosting & cloud

## 03 - SERVERLESS WITH LAMBDA

### PUISSANCE DE CALCUL SANS SERVEUR

👉 Commencez par vous documenter sur la notion de serverless à l’aide du lien suivant (première partie de l’article) : https://www.cloudflare.com/fr-fr/learning/serverless/what-is-serverless/

AWS Lambda est un service de calcul d'événement sans serveur (serverless) qui permet aux développeurs de pouvoir exécuter du code pour tout type d'application, sans se soucier de l'allocation ou de la gestion des serveurs.

👉 Sur AWS Lambda, créez une nouvelle fonction nommée "myfirstfunction" en sélectionnant l’option "Créer à partir de zéro". L’exécution sera basée sur la dernière version de Python.

Le fichier "lambda_function.py" contient une fonction "lambda_handler" qui sera le point d’entrée du script lors de son exécution par AWS Lambda.

👉 Modifiez la fonction "lambda_handler" en y insérant un simple print du message de votre choix.

👉 Cliquez sur le bouton "Test" afin de créer un événement de test basé sur le modèle "hello-world" (qui n’envoie que des données de test en simulant un service) afin de lancer une exécution de test sur votre fonction.

👉 Une fois l’événement de test créé, cliquez de nouveau sur le bouton "Test" et vérifiez l’exécution grâce au nouvel onglet ouvert afin de retrouver le print inséré dans le code Python.

Le service AWS Lambda s’exécute à partir d’un déclencheur (trigger), par exemple avec le service S3 :

Nouveau fichier uploadé > Exécution de la lambda en envoyant des infos au format JSON (nom du fichier, date de création, taille, etc…) > Traitement du fichier (renommer le fichier avec la date de création, par exemple)

👉 Grâce aux règles du service CloudWatch, trouvez un moyen d’exécuter votre fonction "myfirstfunction" toutes les 5 minutes.

Le service CloudWatch est basé le principe de cron-job qui est capable d’effectuer des actions répétées, par exemple :
Toutes les 5 minutes, récupérer le cours des cryptomonnaies via une API afin de créer un fichier CSV sur S3 qui pourra par la suite être téléchargé et utilisé par une autre application.

👉 Assurez-vous de résilier et libérer toutes les ressources crées afin d’éviter une surfacturation et de dépasser le tier gratuit d’AWS :

Serverless : supprimez la fonction "myfirstfunction"
Cloudwatch : supprimez la règle permettant l’exécution automatique toutes les 5 minutes

## 02 -STATIC DEPLOYMENT WITH CODEPIPELINE

### SETUP

Le service CodeSuite d’AWS propose des outils de développements utiles tout au long de la vie d’un projet tels que CodeBuild, CodePipeline, etc.

La philosophie d’AWS est avec CodeSuite est de proposer une expérience unifiée afin de créer plus facilement des automatisations entre les services d’AWS (vers S3, par exemple).

L’intérêt de ce challenge (qui sera moins guidé que le précédent) est d’automatiser le déploiement du frontend de l’application Ultimate Timer afin que chaque push sur le dépôt de code puisse entraîner une mise à jour automatique du projet hébergé sur S3.

👉 Après avoir sélectionné le service CodeBuild, rendez-vous dans les settings des "Developpers tools" afin de créer une nouvelle connexion avec votre compte GitHub. .

👉 Créez un bucket S3 vide nommé "ultimatetimer-front" et paramétrez le service CloudFront afin de publier sur une URL unique les fichiers statiques qui seront déployés dans ce bucket.

👉 Récupérez la ressource "ultimatetimer-front.zip" depuis l’onglet dédié sur Ariane.

👉 Avant de versionner le code source sur GitHub, modifiez la première ligne du fichier "script.js" et plus précisément le contenu de la variable "BACKEND_URL" afin d’y insérer le lien de l’API déployée dans le challenge précédent, le tout entre guillemets.

👉 Créez un nouveau repository GitHub privé nommé "UltimateTimer-Front" et hébergez-y le code source du frontend du projet.

### CRÉATION D’UNE PIPELINE VIA AWS

Maintenant que les pré-requis sont remplis, il est temps de s’attaquer aux différents outils de CodeSuite afin d’automatiser le déploiement du frontend de l’application Ultimate Timer.

👉 Faites en sorte que toutes les modifications publiées soient automatiquement uploadées vers le bucket S3 créé précédemment grâce au service CodePipeline.

👉 Ajoutez un simple texte dans le fichier "index.html" du projet afin de vérifier le déploiement en visitant l’URL publique donnée par CloudFront.

👉 Avant de passer au challenge suivant, veillez à supprimer toutes les ressources créées depuis le début de la journée afin d’éviter une facturation indésirable : AppRunner, CodePipeline et le bucket S3.

## 01 - API DEPLOYMENT WITH APP RUNNER

### DÉPLOIEMENT D’API À GRANDE ÉCHELLE

AWS App Runner est un service proposant l’hébergement d’une API capable de gérer des milliers de requêtes à la seconde.

L’objectif de ce challenge est de déployer le webservice Ultimate Timer qui sera le fil rouge d’une partie de la journée.

👉 Récupérez la ressource "ultimatetimer-api.zip" depuis l’onglet dédié sur Ariane.

👉 Récupérez le projet d’API développé en Node.js dans le fichier ultimatetimer-api.zip fourni

👉 Installez les dépendances et lancez l’API en local grâce aux commandes suivantes.

```
npm install
npm start
```

👉 Exécutez une requête en GET sur l’URL suivante afin de vérifier le bon fonctionnement de l’API : http://localhost:3000/time

Pour information, AWS App Runner permet de sélectionner deux types de sources afin de récupérer le projet :

Container registry : Dépôt d’image Docker qui nécessite la conteneurisation du projet et l’hébergement de l’image sur un registre privé via AWS (Amazon ECR)
Source code repository : Lien direct avec un dépôt GitHub (GitLab n’est pas supporté pour ce service AWS)

👉 Créez un compte GitHub et créez un repository privé nommé "UltimateTimer-API" où vous versionnerez le projet fourni dans l’étape précédente. La branche principale du projet sera "prod".

👉 Créez un nouveau service App Runner en le liant à votre dépôt GitHub pour que le déploiement soit automatisé à chaque push sur la branche principale.

👉 Lors de la step 2 "Configure build", remplissez les bonnes options afin d’installer les dépendances et démarrer l’API webservice sur le bon port.

👉 Lors de la step 3 "Configure service", nommez le service "UltimateTimer-API" et vérifiez la configuration de l’auto scaling qui est la véritable force du service AWS App Runner qui est capable d’augmenter les ressources en fonction des requêtes reçues.

👉Toujours lors de la step 3, configurez le "health check" sur l’URL /health qui permettra de vérifier si l’application est en bonne santé en faisant une requête GET périodique sur cet endpoint.
Vous pouvez vérifier par vous même le retour de cette route en exécutant une requête en local.

👉 Lors de la dernière étape "Review and create", vérifiez la configuration du service puis déployez-le. Le déploiement peut durer de longues minutes, armez-vous de patience !

👉 Récupérez l’URL publique donnée par AWS et tentez de nouveau une requête en GET sur l’URL /time pour vérifier le déploiement.

👉 Ne supprimez pas le service "UltimateTimer-API", il sera utile pour le prochain challenge.