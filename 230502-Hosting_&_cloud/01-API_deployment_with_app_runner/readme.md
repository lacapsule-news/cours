# API DEPLOYMENT WITH APP RUNNER

## DÉPLOIEMENT D’API À GRANDE ÉCHELLE

AWS App Runner est un service proposant l’hébergement d’une API capable de gérer des milliers de requêtes à la seconde.

L’objectif de ce challenge est de déployer le webservice Ultimate Timer qui sera le fil rouge d’une partie de la journée.

👉 Récupérez la ressource "ultimatetimer-api.zip" depuis l’onglet dédié sur Ariane.

👉 Récupérez le projet d’API développé en Node.js dans le fichier ultimatetimer-api.zip fourni

👉 Installez les dépendances et lancez l’API en local grâce aux commandes suivantes.

```
npm install
npm start
```

👉 Exécutez une requête en GET sur l’URL suivante afin de vérifier le bon fonctionnement de l’API : http://localhost:3000/time

Pour information, AWS App Runner permet de sélectionner deux types de sources afin de récupérer le projet :

Container registry : Dépôt d’image Docker qui nécessite la conteneurisation du projet et l’hébergement de l’image sur un registre privé via AWS (Amazon ECR)
Source code repository : Lien direct avec un dépôt GitHub (GitLab n’est pas supporté pour ce service AWS)

👉 Créez un compte GitHub et créez un repository privé nommé "UltimateTimer-API" où vous versionnerez le projet fourni dans l’étape précédente. La branche principale du projet sera "prod".

👉 Créez un nouveau service App Runner en le liant à votre dépôt GitHub pour que le déploiement soit automatisé à chaque push sur la branche principale.

👉 Lors de la step 2 "Configure build", remplissez les bonnes options afin d’installer les dépendances et démarrer l’API webservice sur le bon port.

👉 Lors de la step 3 "Configure service", nommez le service "UltimateTimer-API" et vérifiez la configuration de l’auto scaling qui est la véritable force du service AWS App Runner qui est capable d’augmenter les ressources en fonction des requêtes reçues.

👉Toujours lors de la step 3, configurez le "health check" sur l’URL /health qui permettra de vérifier si l’application est en bonne santé en faisant une requête GET périodique sur cet endpoint.
Vous pouvez vérifier par vous même le retour de cette route en exécutant une requête en local.

👉 Lors de la dernière étape "Review and create", vérifiez la configuration du service puis déployez-le. Le déploiement peut durer de longues minutes, armez-vous de patience !

👉 Récupérez l’URL publique donnée par AWS et tentez de nouveau une requête en GET sur l’URL /time pour vérifier le déploiement.

👉 Ne supprimez pas le service "UltimateTimer-API", il sera utile pour le prochain challenge.