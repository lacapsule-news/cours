# Docker compose

## Contexte

Une machine peut faire tourner plusieurs conteneurs qui fonctionnent en autonomie.

L'utilisation de commandes Dockers est nécessaire pour administrer les concteneurs, rendant le travail fastidieux et risqué.

Docker Compose permet d'administrer un groupe de conteneurs, rendant la gestion plus rapide.

Les commandes se font sur un groupe et non un seul conteneur.

### Fonctionnement

Chaque conteneur est désormais représenté par un service.
L'objectif est de regrouper ces services pour former une application complète.

Chaque service est basé sur une image Docker.

Le fichier docker-compose.yml permet d'associer chaque image avec un service.

Il est possible de configurer individuellement chaque service dans un fichier docker-compose.yml

Cette configuration écrasera ou complètera la configuration par défaut de l'image.

Les commandes docker sont remplacées par celles de docker-compose.
Elles permettent maintenant d'agir sur un groupe de service.