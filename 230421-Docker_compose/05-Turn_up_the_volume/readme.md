# TURN UP THE VOLUME

## SANS VOLUME

👉 Reprenez le fichier Docker Compose du challenge précédent "Web compose" et démarrez le service "nginx".

👉 Trouver un moyen d’exécuter l'interpréteur de commande bash dans le conteneur lié au service "nginx" via la commande docker-compose.

👉 Une fois le terminal du conteneur disponible et attaché, exécutez la commande suivante afin de remplacer rapidement le contenu du fichier HTML utilisé par défaut par le serveur web.

`echo "<p>This is a test</p>" > /usr/share/nginx/html/index-lacapsule.html`

👉 Quittez le terminal interactif du conteneur et faites une requête vers le serveur web via curl afin de constater la modification.

👉 Arrêtez et supprimez le conteneur lié au service "nginx".

👉 Démarrez de nouveau le service nginx afin de refaire une requête vers le serveur web via curl.

Vous constatez que le fichier HTML originel est revenu, car ce qui se passe dans le conteneur est temporaire, il n’y a que ce qui est enregistré dans l’image qui sera conservé en cas de suppression du conteneur (du moins, pour le moment)

## AVEC VOLUME

👉 Grâce à la documentation et la notion de volumes Docker, trouvez les instructions à ajouter dans le fichier Docker Compose afin que le dossier "/usr/share/nginx/html" soit monté (bind mount) sur la machine hôte.
Ce dossier devra s’appeler "html" et sera automatiquement créé dans le même dossier que le fichier "docker-compose.yml" sur la machine hôte.

```
version: "3.3"

services:
  mywebsite:
    image: "lacapsule:alpha"
    ports:
      - 8080:81
    volumes:
    - ./html:/data/www
```
- J'ai monté le volume sur /data/www car mon docker file crée un fichier de config pour lire ce dossier

👉 Démarrez le service "nginx" et vérifiez que le dossier "html" a bien été créé sur la machine hôte.

👉 À l’intérieur de ce dossier "html", créez un fichier "index-lacapsule.html" avec le contenu de votre choix.

👉 Exécutez une requête via curl auprès du serveur web afin de constater que le contenu correspond bien au fichier créé dans l’étape précédente.

```
curl 127.0.0.1:8080
<!DOCTYPE html>
<html>

<head>
  <title>Nginx welcome page</title>
</head>

<body>
  <p>If you see this, you probably passed the challenge with docker compose. Congrats!</p>
</body>

</html>
```

👉 Enfin, arrêtez et supprimez le conteneur lié au service "nginx" et recréez-le.

Vous constatez que le contenu reste le même, car le fichier est préservé sur la machine hôte et monté à chaque création du service "nginx".