# Docker compose

## Contexte

Une machine peut faire tourner plusieurs conteneurs qui fonctionnent en autonomie.

L'utilisation de commandes Dockers est nécessaire pour administrer les concteneurs, rendant le travail fastidieux et risqué.

Docker Compose permet d'administrer un groupe de conteneurs, rendant la gestion plus rapide.

Les commandes se font sur un groupe et non un seul conteneur.

### Fonctionnement

Chaque conteneur est désormais représenté par un service.
L'objectif est de regrouper ces services pour former une application complète.

Chaque service est basé sur une image Docker.

Le fichier docker-compose.yml permet d'associer chaque image avec un service.

Il est possible de configurer individuellement chaque service dans un fichier docker-compose.yml

Cette configuration écrasera ou complètera la configuration par défaut de l'image.

Les commandes docker sont remplacées par celles de docker-compose.
Elles permettent maintenant d'agir sur un groupe de service.

## Exo

### 06 - DATABASE DEPLOYMENT WITH DOCKER

#### DÉPLOIEMENT D’UNE BASE DE DONNÉES

👉 Sur le hub d’images Docker, trouvez l’image adaptée afin de déployer une base de données PostgreSQL.

👉 Créez un nouveau fichier Docker Compose contenant un service nommé "database" et  basé sur l’image précédemment trouvée.

Le service devra être configuré pour respecter ces deux contraintes :

Le mot de passe de l’utilisateur créé par défaut sera "acknowledge_me"
Même si le conteneur lié au service est supprimé, les données devront être persistantes grâce à un volume créé et managé par Docker plutôt qu’il soit directement monté sur la machine hôte). Ce volume nommé "db_data" devra être créé et utilisé dans le fichier Docker Compose.

👉 Démarrez le service "database" et vérifiez le statut du conteneur ainsi que la liste des volumes Docker grâce aux commandes suivantes.

```
docker-compose ps
docker volume ls
```

👉 Modifiez le service "database" afin de "binder" le port par défaut de PostgreSQL sur la machine hôte.

#### VÉRIFICATION DU DÉPLOIEMENT

👉 Trouvez un moyen de récupérer l’adresse IP locale du conteneur lié au service "database". Cette adresse est censée débuter par "172" et vous pouvez la ping afin de vérifier si le conteneur répond bien.

👉 Sur votre machine hôte, utilisez l’utilitaire en ligne de commande psql afin de vous connecter à la base de données PostgreSQL.

Vous devrez préciser l’adresse IP précédemment récupéré (via l’option -h) ainsi que l’utilisateur par défaut postgres (via l’option -p)

👉 Une fois l'interpréteur de commande psql démarré, vérifiez la version utilisée par la base de données grâce à l’instruction suivante.

`postgres=# SELECT VERSION();`

👉 Dans la base de données, créez une nouvelle table qui ne sera utilisée que pour vérifier la persistance des données.

👉 Supprimez puis recréez le conteneur lié au service "database" et connectez-vous à la base de données via psql afin de vérifier si la table créée précédemment est toujours présente.

#### BONUS

👉 Créez un nouveau service permettant d’utiliser le gestionnaire de base de données adminer afin d’administrer facilement la base de données PostgreSQL via un navigateur.

```
version: "3.3"

services:
  postgres:
    image: "postgres:latest"
    environment:
      POSTGRES_PASSWORD: superstrongpassword
      PGDATA: /data/postgres
    volumes:
       - ./postgres:/data/postgres
    networks:
      - postgres
  
  adminer:
    image: adminer
    restart: always
    volumes:
       - ./pgadmin:/var/lib/pgadmin
    networks:
      - postgres
    ports:
      - 8080:8080

networks:
  postgres:
    driver: bridge
```

### 05 - TURN UP THE VOLUME

#### SANS VOLUME

👉 Reprenez le fichier Docker Compose du challenge précédent "Web compose" et démarrez le service "nginx".

👉 Trouver un moyen d’exécuter l'interpréteur de commande bash dans le conteneur lié au service "nginx" via la commande docker-compose.

👉 Une fois le terminal du conteneur disponible et attaché, exécutez la commande suivante afin de remplacer rapidement le contenu du fichier HTML utilisé par défaut par le serveur web.

`echo "<p>This is a test</p>" > /usr/share/nginx/html/index-lacapsule.html`

👉 Quittez le terminal interactif du conteneur et faites une requête vers le serveur web via curl afin de constater la modification.

👉 Arrêtez et supprimez le conteneur lié au service "nginx".

👉 Démarrez de nouveau le service nginx afin de refaire une requête vers le serveur web via curl.

Vous constatez que le fichier HTML originel est revenu, car ce qui se passe dans le conteneur est temporaire, il n’y a que ce qui est enregistré dans l’image qui sera conservé en cas de suppression du conteneur (du moins, pour le moment)

#### AVEC VOLUME

👉 Grâce à la documentation et la notion de volumes Docker, trouvez les instructions à ajouter dans le fichier Docker Compose afin que le dossier "/usr/share/nginx/html" soit monté (bind mount) sur la machine hôte.
Ce dossier devra s’appeler "html" et sera automatiquement créé dans le même dossier que le fichier "docker-compose.yml" sur la machine hôte.

```
version: "3.3"

services:
  mywebsite:
    image: "lacapsule:alpha"
    ports:
      - 8080:81
    volumes:
    - ./html:/data/www
```
- J'ai monté le volume sur /data/www car mon docker file crée un fichier de config pour lire ce dossier

👉 Démarrez le service "nginx" et vérifiez que le dossier "html" a bien été créé sur la machine hôte.

👉 À l’intérieur de ce dossier "html", créez un fichier "index-lacapsule.html" avec le contenu de votre choix.

👉 Exécutez une requête via curl auprès du serveur web afin de constater que le contenu correspond bien au fichier créé dans l’étape précédente.

```
curl 127.0.0.1:8080
<!DOCTYPE html>
<html>

<head>
  <title>Nginx welcome page</title>
</head>

<body>
  <p>If you see this, you probably passed the challenge with docker compose. Congrats!</p>
</body>

</html>
```

👉 Enfin, arrêtez et supprimez le conteneur lié au service "nginx" et recréez-le.

Vous constatez que le contenu reste le même, car le fichier est préservé sur la machine hôte et monté à chaque création du service "nginx".

### 04 - WEB COMPOSE

#### CRÉATION D’UN SERVICE WEB

👉 Créez un nouveau fichier "docker-compose.yml" contenant un service nommé "nginx" basé sur l’image nginx:stable en exposant le port d’écoute par défaut du serveur web pour qu’il soit joignable depuis le port 8080 sur la machine hôte.

```
version: "3.3"

services:
  mywebsite:
    image: "nginx:stable"
    ports:
      - 8080:80
```

👉 Démarrez le service "nginx" et vérifiez le lancement du service avec une requête curl.

```
curl 127.0.0.1:8080
<!DOCTYPE html>
...
```


👉 Arrêtez le service et supprimez le conteneur lié via la commande docker-compose down.


👉 Reprendre l’image "nginx-lacapsule" du challenge de la veille "Advanced Dockerfile" afin de remplacer l’image nginx:stable par cette dernière.

Si vous n’avez pas terminé ce challenge, récupérez les solutions directement depuis Ariane 😉

```
version: "3.3"

services:
  mywebsite:
    image: "lacapsule:alpha"
    ports:
      - 8080:81

```

👉 Démarrez le service "nginx" et vérifiez le lancement du service avec une requête curl et l’option -v.

Vous êtes censé voir la page HTML personnalisée et constater l’absence de la version de nginx dans les headers de la réponse.

```
curl -v 127.0.0.1:8080
*   Trying 127.0.0.1:8080...
* Connected to 127.0.0.1 (127.0.0.1) port 8080 (#0)
> GET / HTTP/1.1
> Host: 127.0.0.1:8080
> User-Agent: curl/7.74.0
> Accept: */*
> 
* Mark bundle as not supporting multiuse
< HTTP/1.1 200 OK
< Server: nginx
< Date: Fri, 21 Apr 2023 05:38:11 GMT
< Content-Type: text/html
< Content-Length: 170
< Last-Modified: Thu, 20 Apr 2023 08:54:41 GMT
< Connection: keep-alive
< ETag: "6440fdd1-aa"
< Accept-Ranges: bytes
```

👉 Vérifiez le statut des services liés au fichier Docker Compose via la commande docker-compose ps.

```
docker-compose ps
         Name                       Command               State              Ports            
----------------------------------------------------------------------------------------------
helloworld_mywebsite_1   /docker-entrypoint.sh ngin ...   Up      80/tcp, 0.0.0.0:8080->81/tcp
```

### 03 - Hello world!

#### DÉCOUVERTE DE DOCKER COMPOSE

👉 Commencez par installer par docker-compose.

```
sudo curl -L https://github.com/docker/compose/releases/download/v2.11.1/docker-compose-linux-x86_64 -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```

👉 Vérifiez l’installation de docker-compose en affichant sa version.

👉 Créez un fichier "docker-compose.yml" censé créer un service (conteneur) lançant l’image "hello-world".
L’instruction "version" permet de spécifier la version du fichier Docker Compose qu’on souhaite utiliser.

```
version: "3.9"


services:

  helloworld:

    image: "hello-world:latest"
```

👉 Lancez directement le conteneur puis lancez-le en tâche de fond.

```
docker-compose up
docker-compose up -d
```

👉 Afin de vous entraîner à manipuler le fichier Docker Compose, ajoutez un second service nommé "helloworld2" et basé sur la même image que le premier service.

👉 Démarrez tous les services en une seule commande, puis le service "helloworld2" seulement.

```
docker-compose up
docker-compose up helloworld2
```

👉 Visualisez les conteneurs liés au fichier Docker Compose seulement.

`docker-compose ps`

Enfin, sachez qu’il existe de nombreuses autres commandes permettant de manipuler les services gérés via Docker Compose tels que restart, stop ou down.