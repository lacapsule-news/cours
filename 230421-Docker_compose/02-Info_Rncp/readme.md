# Le titre [rncp 6](https://www.francecompetences.fr/recherche/rncp/36061/)

Un dossier professionnel résumant votre parcours de formation.

Un dossier projet basé sur votre projet de fin de batch.

Une soutenance de 2h05 devant un jury.

## Compétences évaluées

- Automatiser le déploiment d'une infrastructure dans le cloud.
    - Automatiser la création de serveurs à l'aide de scripts.
    - Automatiser le déploiment d'une infrastructure.
    - Sécuriser l'infrastructure.
    - Mettre l'infrastructure en production dans le cloud.
- Déployer en continue une application.
    - Préparer un environnement de test
    - Gérer le stockage des données
    - Gérer des containers
    - Automatiser la mise en production d'une application
- Superviser les services déployées.
    - Définir et mettre en placedes statistique de services
    - Exploiter une solution de supervision
    - Echanger sur des réseaux professionnels éventuellement en anglais.

Pour rappel le passage du titre est conditionné par une évaluation continue basée sur le référentiel d'évaluation des certificats de compétences professionnelles (CCP).
- 6 QCM à faire directement sur ariane
- 6 cas pratiques (challenges) évalués par votre teacher.

## Dossier professionnel

- Le dossier professionnel est un document obigatoire pour le passage du titre.
- Il doit être templi selon le modèle fourni par le ministère du travaille([récupérable en ligne](https://drive.google.com/file/d/13M3Uhk-CCAlsQ-TR0a3mYoyGgjxm80eS/view))

Le dossier professionnel doit être rempli 1 mois avant le passage du titre afin qu'il soit présenté lors de la soutenance devant le jury.

Vous avez jusqu'au lundi de la semain 8 avant 12h pour nous transmettre vos dossiers qu format PDF via ce [lien](https://www.google.com/url?q=https://share.hsforms.com/1fhiL3Lc2QQOtkcPpX3kTgg5fdg8&sa=D&source=editors&ust=1682046698288397&usg=AOvVaw2xswzZ2dEYGJuKMFAEiV2v)

## Le dossier projet

Basé sur votre projet de fin de batch, le dossier projet doit être réalisé pendant 3 semaines de préparation après la formation.

Voici le plan type:
- Liste des compétences du référentiel qui sont couvertes par le projet.
- Cahier des charges.
- Spécifications technique du projet avec un schéma de l'infrastructure à déployer.
- Description de la démarche et des outils utilisés.
- Réalisation individuelles comportant les scripts et les configurations les plus significatifs, en les argumentant.
- Description d'une situation de travaille ayant nécéssité une recherche efféctuée durant le projet.

Aucune contrainte n'est imposée sur le nombre de pages du dossier projet.

## Soutenance

En amont de la soutenance, vous devez préparer des diaporamas présentant votre projet final.

Voici le plan type:
- Contexte du projet; cahier des charges, contraintes et livrables attendus
- Présentation de l'application à mettre en production
- Présentation d'un exemple significatif du travail réalisé individuellement.
- Présentation d'un exemple de recherche éffectuée
- Synthèse et conclusion : satisfactions et difficultés rencontrées.

Le support de présentation doit convenir pour une présentation de 30 minutes maximum.

## Déroulé

La soutenance de 2h05 est découpée comme suit:
- 30 min d'étude d'un document technique en anglais avec réponse en anglais par écrit à une série de questions ouvertes.
    - Réaliser le jour des la convocation, avant le passage.
- 15 min de lecture du dossier par le jury
    - A imprimer chez vous avant le passage
- 30 min de présentation orale du projet
    - Avec support de présentation
- 30 min d'entretien technique
    - Questions sur les activités type ave le projet
- 20 min d'échange sur le projet professionnel.

## Nos conseils

- Soyez honnête sur les difficultés rencontrées: ne cherchez pas à montrer un projet lisse et sans encombres.
- Même s'il n'est pas possible de connaître en avance les questions posées par le jury, préparez chaque sujet évalué en vous basant sur la liste des compétences professionnelles évaluées
- Révisez autant les sujet complexes que les sujet basiques: bonnes pratique de bases, les types de solution (SaaS, Paas), fifférence entre une VM et un CT...
- Venez devant le jury avec votre ordinateur et tous les éléments réalisés pendant le projet : fichier de configuration, Gitlab ...

## Rattrapage

Une session de rattrapage est organisée pour :
- Les élèves ayant validé au moins un des trois blocs de compétences(activités type)
- Les élèves absent lors de la première session, uniquement s'ils justifient cette absence par un cas de force majeure

**Le jour de l'examen**

- Les retards ou abssences non justifiés entraînent une annulation du passage des candidats, sans possibilité de rattrapage.
- Les candidats doivent impérativement se présenter à la session organisée par la Capsule. Ils ne peuvent pas choisir de se présenter à une autre session.