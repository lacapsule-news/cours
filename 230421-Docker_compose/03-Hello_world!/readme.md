# Hello world!

## DÉCOUVERTE DE DOCKER COMPOSE

👉 Commencez par installer par docker-compose.

```
sudo curl -L https://github.com/docker/compose/releases/download/v2.11.1/docker-compose-linux-x86_64 -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```

👉 Vérifiez l’installation de docker-compose en affichant sa version.

👉 Créez un fichier "docker-compose.yml" censé créer un service (conteneur) lançant l’image "hello-world".
L’instruction "version" permet de spécifier la version du fichier Docker Compose qu’on souhaite utiliser.

```
version: "3.9"


services:

  helloworld:

    image: "hello-world:latest"
```

👉 Lancez directement le conteneur puis lancez-le en tâche de fond.

```
docker-compose up
docker-compose up -d
```

👉 Afin de vous entraîner à manipuler le fichier Docker Compose, ajoutez un second service nommé "helloworld2" et basé sur la même image que le premier service.

👉 Démarrez tous les services en une seule commande, puis le service "helloworld2" seulement.

```
docker-compose up
docker-compose up helloworld2
```

👉 Visualisez les conteneurs liés au fichier Docker Compose seulement.

`docker-compose ps`

Enfin, sachez qu’il existe de nombreuses autres commandes permettant de manipuler les services gérés via Docker Compose tels que restart, stop ou down.