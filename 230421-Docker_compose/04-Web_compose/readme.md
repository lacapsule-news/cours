# WEB COMPOSE

## CRÉATION D’UN SERVICE WEB

👉 Créez un nouveau fichier "docker-compose.yml" contenant un service nommé "nginx" basé sur l’image nginx:stable en exposant le port d’écoute par défaut du serveur web pour qu’il soit joignable depuis le port 8080 sur la machine hôte.

```
version: "3.3"

services:
  mywebsite:
    image: "nginx:stable"
    ports:
      - 8080:80
```

👉 Démarrez le service "nginx" et vérifiez le lancement du service avec une requête curl.

```
curl 127.0.0.1:8080
<!DOCTYPE html>
...
```


👉 Arrêtez le service et supprimez le conteneur lié via la commande docker-compose down.


👉 Reprendre l’image "nginx-lacapsule" du challenge de la veille "Advanced Dockerfile" afin de remplacer l’image nginx:stable par cette dernière.

Si vous n’avez pas terminé ce challenge, récupérez les solutions directement depuis Ariane 😉

```
version: "3.3"

services:
  mywebsite:
    image: "lacapsule:alpha"
    ports:
      - 8080:81

```

👉 Démarrez le service "nginx" et vérifiez le lancement du service avec une requête curl et l’option -v.

Vous êtes censé voir la page HTML personnalisée et constater l’absence de la version de nginx dans les headers de la réponse.

```
curl -v 127.0.0.1:8080
*   Trying 127.0.0.1:8080...
* Connected to 127.0.0.1 (127.0.0.1) port 8080 (#0)
> GET / HTTP/1.1
> Host: 127.0.0.1:8080
> User-Agent: curl/7.74.0
> Accept: */*
> 
* Mark bundle as not supporting multiuse
< HTTP/1.1 200 OK
< Server: nginx
< Date: Fri, 21 Apr 2023 05:38:11 GMT
< Content-Type: text/html
< Content-Length: 170
< Last-Modified: Thu, 20 Apr 2023 08:54:41 GMT
< Connection: keep-alive
< ETag: "6440fdd1-aa"
< Accept-Ranges: bytes
```

👉 Vérifiez le statut des services liés au fichier Docker Compose via la commande docker-compose ps.

```
docker-compose ps
         Name                       Command               State              Ports            
----------------------------------------------------------------------------------------------
helloworld_mywebsite_1   /docker-entrypoint.sh ngin ...   Up      80/tcp, 0.0.0.0:8080->81/tcp
```