# Kubernetes avancé

## 04 - Popular CMS 2

### CONFIGURATION

L’objectif de ce challenge est de déployer un environnement complet pour le CMS Drupal via Kubernetes.

👉 Créez un manifeste ConfigMap dédié à la configuration des variables d’environnement :

Le manifeste sera nommé "drupal-config" et devra être rattaché à l’application nommée "drupal"
Le mot de passe de l’utilisateur "root" pour la base de données MySQL sera "ihatewordpress"
La base de données MySQL créee par défaut sera "drupal-dev"

👉 Appliquez le manifeste ConfigMap pour le namespace "drupal-dev".

### BASE DE DONNÉES

👉 Créez des manifestes PersistentVolume et PersistentVolumeClaim (dans le même fichier) dédiées au stockage persistant de la base de données :

Les manifestes seront respectivement nommés "drupal-database-pv" et "drupal-database-pvc". Ils devront également être rattachés à l’application nommée "drupal"
Allocation d’un espace d’au moins 1 Gb

👉 Appliquez les manifestes PersistentVolume et PersistentVolumeClaim pour le namespace "drupal-dev".

👉 Créez des manifestes de déploiement et de service (dans le même fichier) dédiés à la base de données MySQL :

Le manifeste de service sera nommé "drupal-database-service" et devra être rattaché à l’application nommée "drupal"
Le service se contentera d’exposer le port par défaut de MySQL (sans load balancer)
Le manifeste de déploiement sera nommé "drupal-database-deployment" et devra être rattaché à l’application nommée "drupal"
Le conteneur déployé sera nommé "mysql" et basé sur la fameuse image officielle de MySQL dans sa dernière version
Le port par défaut de MySQL sera exposé via "containerPort" dans le tableau "ports"
Les variables d’environnement définies dans le manifest ConfigMap créé précédemment devront être utilisées pour la configuration de la base de données
Le volume créé précédemment devra être utilisé pour rendre persistantes les données de MySQL
Un seul pod sera utilisé pour le déploiement du conteneur "mysql"

👉 Appliquez les manifestes de déploiement et de service pour le namespace "drupal-dev".

👉 Prenez le temps de vérifier que le pod est bien en statut "Running" et connectez-vous à la base de données via la CLI mysql avec les informations configurés dans le manifest ConfigMap.

### CONFIGURATION

👉 Créez des manifestes de déploiement et de service (dans le même fichier) dédiés à l'application web de Drupal :

Le manifeste de service sera nommé "drupal-webapp-service" et devra être rattaché à l’application nommée "drupal"
Le service se contentera d’exposer le port par défaut de Drupal (avec un load balancer)
Le manifeste de déploiement sera nommé "drupal-webapp-deployment" et devra être rattaché à l’application nommée "drupal"
Le conteneur déployé sera nommé "drupal" et basé sur l’image officielle de Drupal dans sa dernière version
Un seul pod sera utilisé pour le déploiement du conteneur "drupal"

👉 Appliquez les manifestes de déploiement et de service pour le namespace "drupal-dev".

👉 Connectez-vous à l’interface web de Drupal en créant un tunnel entre la machine hôte et le pod déployé via la commande kubectl port-forward.

👉 Complétez la configuration de Drupal via son interface web en précisant les informations de connexion à la base de données.

Comme à l'accoutumée, si le CMS vous souhaite la bienvenue, c’est que vous avez terminé le challenge. Félicitations ! 🎉

## 03 - Database deployment with kubernetes

### SETUP

👉 Créez un manifeste ConfigMap dédié au déploiement d’une base de données PostgreSQL en vous assurant que les contraintes suivantes soient respectées :


Le ConfigMap sera nommé "mydatabase-config" et dédié à une application nommée "mydatabase"
Le nom de la base de données par défaut sera "myawesomeapp"
Le nom d’utilisateur par défaut sera "themiz" avec "IAWAWESOME" comme mot de passe
Le nom des trois variables d’environnement précédentes devront être conformes à celles utilisées par l’image postgres

👉 Appliquez le manifeste et vérifiez sa création grâce aux commandes habituelles kubectl get et kubectl describe.

### VOLUME PERSISTANT

👉 Créez un fichier nommé "mydatabase-storage.yml" contenant les manifestes suivants.

```
kind: PersistentVolume
apiVersion: v1
metadata:
  name: mydatabase-pv
  labels:
    app: mydatabase

spec:
  storageClassName: manual
  capacity:
    storage: 5M
  accessModes:
    - ReadWriteMany
  hostPath:
    path: "/mnt/mydatabase"

---

kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: mydatabase-pvc
  labels:
    app: mydatabase

spec:
  storageClassName: manual
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 5M
```

Pour commencer, vous pouvez constater que votre fichier ne contient pas un manifeste, mais deux, séparés par "---".
Sachez qu’il est techniquement possible de rassembler tous vos manifestes (déploiement, service, etc.) dans un seul fichier, mais ce n’est pas une excellente pratique vous pouvez imaginer qu’on risque d’avoir un seul gros bloc illisible et difficilement maintenable.

👉 Avant d’aller plus loin, prenez connaissance du concept de "persistent volumes" en lisant attentivement l’introduction de la documentation suivante : https://kubernetes.io/fr/docs/concepts/storage/persistent-volumes/ 

👉 Appliquez les manifestes et vérifiez leurs états grâce aux commandes suivantes.

```
kubectl get persistentvolumes
kubectl get persistentvolumeclaims
```

### DÉPLOIEMENT DE LA BASE DE DONNÉES

Les variables d’environnements et le volume sont prêts à être utilisés, il ne reste plus qu’à déployer la base de données au sein du cluster.

👉 Créez un manifeste de déploiement en vous assurant que les contraintes suivantes soient respectées :

Le nom de l’application déployée sera "mydatabase"
Le nom du manifeste sera "mydatabase-deployment"
Le conteneur déployé sera nommé "postgres" et basé sur la fameuse image officielle de PostgreSQL dans sa dernière version
Le port par défaut de PostgreSQL sera exposé via "containerPort" dans le tableau "ports"
Les variables d’environnement définies dans le manifest ConfigMap créé précédemment devront être utilisées pour la configuration de la base de données
Le volume créé précédemment devra être utilisé pour rendre persistantes les données de PostgreSQL
Un seul pod sera utilisé pour le déploiement du conteneur "postgres"

👉 Afin de vérifier le déploiement de tout l’environnement de base de données, trouvez un moyen d’exécuter une commande dans le seul pod déployer afin d’utiliser la CLI psql afin de vous connecter à la base de données PostgreSQL.

👉 Une fois l'interpréteur de commande psql démarré, vérifiez la version utilisée par la base de données grâce à l’instruction suivante.

`postgres=# SELECT VERSION();`

👉 Dans la base de données, créez une nouvelle table de test qui ne sera utilisée que pour vérifier la persistance des données.

👉 Supprimez le pod dédié à l’hébergement de la base de données et connectez-vous à la CLI psql du pod redéployé afin de vérifier si la table créée précédemment est toujours présente.

`postgres=# \d`

## 02 - Environment variables with kubernetes

### SETUP

👉 Créez un manifeste de déploiement en vous assurant que les contraintes suivantes soient respectées :

Le nom de l’application déployée sera "motd"
Le nom du manifeste sera "motd-deployment"
Le conteneur déployé sera nommé "alpine" et basé sur l’image alpine dans sa dernière version
5 pods devront être utilisés pour le déploiement du conteneur "alpine"

Si vous tentez d’appliquer ce manifeste de déploiement, vos pods seront en erreur et c’est tout à fait normal : le conteneur basé sur l’image alpine s’arrête après son lancement, car il n’a aucune action à effectuer.

### CONFIGMAPS

De la même façon qu’il existe des manifestes pour le déploiement et pour les services, la configuration des variables d’environnement peut également se faire avec un simple fichier grâce au concept de ConfigMaps.

👉 Créez un fichier nommé "motd-config.yml" contenant le manifeste suivant.

```
apiVersion: v1
kind: ConfigMap

metadata:
  name: motd-config
  labels:
    app: motd

data:
  MESSAGE: "Hi i'm a simple container inside a pod"
  OTHER_MESSAGE: "You can't see me!"
```

Si vous prenez le temps d’analyser ce fichier, vous pouvez voir que les metadata sont semblables à un manifeste de déploiement et que les variables d’environnement sont simplement définies dans la tableau "data".

👉 Appliquez ce manifeste grâce à la commande habituelle donnée ci-dessous.

`kubectl apply -f motd-config.yml`

👉 Trouvez la commande kubectl capable d’afficher tous les manifestes ConfigMaps du cluster afin de vérifier que "motd-config" a bien été créé.

`kubectl get configmaps`

👉 Trouvez la commande kubectl capable de lister chaque variable d'environnement créé dans un manifeste ConfigMap précis.

`kubectl describe configmap motd-config`

👉 Modifiez le manifeste de déploiement créé au début de ce challenge afin de faire en sorte que tous les conteneurs affichent la variable d’environnement "MESSAGE".

Si vos pods sont en erreur, c’est tout à fait normal : le conteneur basé sur l’image alpine s’arrête après son lancement et l’exécution de la commande echo, car il n’a aucune autre action à effectuer.

👉 Après avoir appliqué le manifeste de déploiement, vérifiez que le message soit affiché par chaque conteneur grâce à la commande suivante.

`kubectl logs -f -l app=motd`

Si vous voyez 5 fois le message "Hi i'm a simple container inside a pod", c’est que les manifestes ont bien été développés et appliqués, bravo ! 🎉

👉 Pour finir, modifiez la variable d’environnement "MESSAGE" et re-déployez l’application "motd" afin de voir si ce nouveau message s’affiche correctement.

## 01 - Labels and namespaces

### LABELS

Les labels sont des données de type clé/valeur qui sont attachés aux objets permettant de les identifier plus facilement.

Si vous regardez d’un peu plus près le contenu d’un des fichiers de déploiement que vous avez créé, vous remarquerez que la partie "metadata" contient un tableau "labels" possédant une étiquette "app".

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: httpd-server-deployment
  labels:
    app: httpd-server

spec:
  replicas: 4
  selector:
    matchLabels:
      app: httpd-server

  template:
    metadata:
      labels:
        app: httpd-server

    spec:
      containers:
      - name: httpd-server
        image: httpd:latest
        imagePullPolicy: Always
        ports:
        - containerPort: 80
```

👉 Déployez le manifeste ci-dessus et listez tous les pods du cluster avec l’option "--show-labels".
Le label "app=httpd-server" est censé s’afficher à côté de chaque pod lié au manifeste de déploiement.

👉 Trouvez la commande kubectl permettant de lister les pods en appliquant un filtre sur le label "app" afin de ne voir que les pods liés à notre application "httpd-server".

`kubectl get pods -l app=httpd-server`

👉 Trouvez la commande kubectl permettant d’afficher les logs en direct de tous les pods liés à notre application "httpd-server".

`kubectl logs -f -l app=httpd-server`

### NAMESPACES

Kubernetes est capable de prendre en charge plusieurs clusters virtuels présents sur le même cluster physique, ces clusters virtuels sont appelés des namespaces.

Les namespaces vont plus loin que la notion de labels, car ils permettent de gérer plusieurs applications ou environnements sur le même cluster, auprès du même master node et en les isolant afin d’assurer un maximum de sécurité, bien évidemment.

👉 Listez les namespaces du cluster grâce à la commande ci-dessous.


`kubectl get namespaces`


L’output de cette commande est censée vous montrer quatres namespaces initiaux créés par Kubernetes, notamment "defaut" qui sera sans grande surprise le namespace par défaut pour les objets (déploiements, services, etc.) créés sans namespace.


👉 Trouvez la commande kubectl permettant de créer un nouveau namespace nommé "webapp-prod".

`kubectl create namespace webapp-prod`

👉 Démarrez un pod contenant un seul conteneur nommé "httpd-server", basé sur l’image officielle de httpd et en exposant le port 80.
Ce pod devra être créé en mode impératif (sans passer par un manifeste) et appartiendra au namespace créé précédemment.

`kubectl run httpd-server --image=httpd --port=80 --namespace=webapp-prod`

👉 Listez les pods liés à un namespace en particulier grâce à la commande ci-dessous.

`kubectl get pods -n webapp-prod`

Si vous tentez de lister les pods sans préciser de namespace, kubectl vous affichera les pods du namespace "default" et votre pod créé précédemment ne sera pas affiché.

👉 Supprimez le pod créé précédemment via la commande kubectl delete.

👉 Récupérez le manifeste partagé au début de ce challenge (httpd-server-deployment) et appliquez-le au namespace "webapp-prod".

👉 Vérifiez les informations des pods déployés, notamment leur namespace, grâce à la commande suivante.

`kubectl describe pods -n webapp-prod`

Une nouvelle fois, si vous tentez d’obtenir des informations sur tous les pods sans préciser de namespace, kubectl vous affichera les pods du namespace "default" et vos pods déployés précédemment ne seront pas affichés.