# Popular CMS 2

## CONFIGURATION

L’objectif de ce challenge est de déployer un environnement complet pour le CMS Drupal via Kubernetes.

👉 Créez un manifeste ConfigMap dédié à la configuration des variables d’environnement :

Le manifeste sera nommé "drupal-config" et devra être rattaché à l’application nommée "drupal"
Le mot de passe de l’utilisateur "root" pour la base de données MySQL sera "ihatewordpress"
La base de données MySQL créee par défaut sera "drupal-dev"

👉 Appliquez le manifeste ConfigMap pour le namespace "drupal-dev".

## BASE DE DONNÉES

👉 Créez des manifestes PersistentVolume et PersistentVolumeClaim (dans le même fichier) dédiées au stockage persistant de la base de données :

Les manifestes seront respectivement nommés "drupal-database-pv" et "drupal-database-pvc". Ils devront également être rattachés à l’application nommée "drupal"
Allocation d’un espace d’au moins 1 Gb

👉 Appliquez les manifestes PersistentVolume et PersistentVolumeClaim pour le namespace "drupal-dev".

👉 Créez des manifestes de déploiement et de service (dans le même fichier) dédiés à la base de données MySQL :

Le manifeste de service sera nommé "drupal-database-service" et devra être rattaché à l’application nommée "drupal"
Le service se contentera d’exposer le port par défaut de MySQL (sans load balancer)
Le manifeste de déploiement sera nommé "drupal-database-deployment" et devra être rattaché à l’application nommée "drupal"
Le conteneur déployé sera nommé "mysql" et basé sur la fameuse image officielle de MySQL dans sa dernière version
Le port par défaut de MySQL sera exposé via "containerPort" dans le tableau "ports"
Les variables d’environnement définies dans le manifest ConfigMap créé précédemment devront être utilisées pour la configuration de la base de données
Le volume créé précédemment devra être utilisé pour rendre persistantes les données de MySQL
Un seul pod sera utilisé pour le déploiement du conteneur "mysql"

👉 Appliquez les manifestes de déploiement et de service pour le namespace "drupal-dev".

👉 Prenez le temps de vérifier que le pod est bien en statut "Running" et connectez-vous à la base de données via la CLI mysql avec les informations configurés dans le manifest ConfigMap.

## CONFIGURATION

👉 Créez des manifestes de déploiement et de service (dans le même fichier) dédiés à l'application web de Drupal :

Le manifeste de service sera nommé "drupal-webapp-service" et devra être rattaché à l’application nommée "drupal"
Le service se contentera d’exposer le port par défaut de Drupal (avec un load balancer)
Le manifeste de déploiement sera nommé "drupal-webapp-deployment" et devra être rattaché à l’application nommée "drupal"
Le conteneur déployé sera nommé "drupal" et basé sur l’image officielle de Drupal dans sa dernière version
Un seul pod sera utilisé pour le déploiement du conteneur "drupal"

👉 Appliquez les manifestes de déploiement et de service pour le namespace "drupal-dev".

👉 Connectez-vous à l’interface web de Drupal en créant un tunnel entre la machine hôte et le pod déployé via la commande kubectl port-forward.

👉 Complétez la configuration de Drupal via son interface web en précisant les informations de connexion à la base de données.

Comme à l'accoutumée, si le CMS vous souhaite la bienvenue, c’est que vous avez terminé le challenge. Félicitations ! 🎉