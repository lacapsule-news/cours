# My first dashboard

## CRÉATION D’UN DASHBOARD

Même si vous avez déjà créé votre premier dashboard Grafana, il ne s’agissait que de données fictives : vous allez maintenant créer un dashboard de monitoring à partir de vraies metrics récoltées depuis Prometheus afin d’arriver à un résultat semblable au screen ci-dessous.

👉 Assurez-vous que Prometheus et Node Exporter sont bien démarrés et que les différentes metrics sont accessibles, notamment celles du système.

👉 Créez un nouveau dashboard nommé "My first dashboard".

👉 Créez un panel dédié à l’affichage du statut de Prometheus :

Si Prometheus est en ligne, le panel affichera "Up" en vert
Si Prometheus est hors ligne, le panel affichera "Down" en rouge

👉 Créez un panel dédié à l’affichage du nombre total de requêtes réussies sur le port 9090 de Prometheus.

Si Prometheus est hors ligne, ce panel affichera seulement "N/A".

👉 Créez un panel dédié à l’affichage du pourcentage de ram (mémoire vive) utilisée en temps réel.

Vous aurez certainement besoin de faire quelques opérations afin de transformer les résultats initiaux (en bytes) en pourcentage.

👉 Créez un dernier panel dédié à l’affichage de l’utilisation du disque principal (/dev/sda), en pourcentage.

👉 Vérifiez les données affichées par votre dashboard et arrêtez volontairement Prometheus afin de constater que les données ne s’affichent plus, comme sur le screen ci-dessous.