# Node exporter dashboard

## DASHBOARD LINUX

Vous l’avez sans doute remarqué, créer un dashboard complet peut se révéler assez fastidieux. Evidemment, Grafana est une solution complète et propose des dashboards sur mesure auxquels il suffira de brancher une source.

👉 Tout comme dans le précédent challenge, assurez-vous que Prometheus et Node Exporter soient bien démarrés

👉 Parmi tous les dashboards proposés par Grafana et la communauté, trouvez et importez-en un sur mesure capable d’afficher toutes les metrics de Node Exporter.

Assurez-vous que les données des 30 dernières minutes sont affichées par défaut sur le dashboard.