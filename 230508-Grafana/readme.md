# Grafana

## Contexte

L'agrégation des métriques du système d'information peut fournir un volume important de données.

Les données agrégées sont dans un format non exploitable par un humain.

Il est nécessaire d'avoir une solution qui met en forme ces métriques afin de les exploiter efficacement.

### fonctionnement

Il est nécessaire d'installer grafana sur une machine dédiée à la visualisation des métriques.

Via le language de requête PromQL, Grafana va récupérer les métriques préparées par Prometheus.

Les métriques sont mises en forme à travers des dashboard personnalisables.

## Exo

### 05 - Nginx dashboard

#### DASHBOARD NGINX

Il est maintenant temps de vous confronter à d’autres metrics et d’autres dashboard à travers votre solution de serveur web préférée : Nginx !

👉 Assurez-vous que Prometheus et Nginx Prometheus Exporter (via Docker) soient bien démarrés et que les différentes metrics soient accessibles, notamment celles de Nginx.

👉 Trouvez et importez le dashboard officiel de Nginx Prometheus Exporter.

👉 Personnalisez le dashboard et ajoutez 2 panels dédiés à l’affichage du nombre de connexions actives et de requêtes HTTP (voir screen ci-dessous).

Comme dans le challenge "Nginx metrics", n’hésitez pas à ouvrir le site en navigation privée et sur plusieurs navigateurs pour tester votre dashboard.

👉 Une fois votre journée terminée, assurez-vous de résilier le serveur utilisé depuis hier afin d’éviter de dépasser le crédit offert par Linode.

### 04 - Node exporter dashboard

#### DASHBOARD LINUX

Vous l’avez sans doute remarqué, créer un dashboard complet peut se révéler assez fastidieux. Evidemment, Grafana est une solution complète et propose des dashboards sur mesure auxquels il suffira de brancher une source.

👉 Tout comme dans le précédent challenge, assurez-vous que Prometheus et Node Exporter soient bien démarrés

👉 Parmi tous les dashboards proposés par Grafana et la communauté, trouvez et importez-en un sur mesure capable d’afficher toutes les metrics de Node Exporter.

Assurez-vous que les données des 30 dernières minutes sont affichées par défaut sur le dashboard.

### 03 - My first dashboard

#### CRÉATION D’UN DASHBOARD

Même si vous avez déjà créé votre premier dashboard Grafana, il ne s’agissait que de données fictives : vous allez maintenant créer un dashboard de monitoring à partir de vraies metrics récoltées depuis Prometheus afin d’arriver à un résultat semblable au screen ci-dessous.

👉 Assurez-vous que Prometheus et Node Exporter sont bien démarrés et que les différentes metrics sont accessibles, notamment celles du système.

👉 Créez un nouveau dashboard nommé "My first dashboard".

👉 Créez un panel dédié à l’affichage du statut de Prometheus :

Si Prometheus est en ligne, le panel affichera "Up" en vert
Si Prometheus est hors ligne, le panel affichera "Down" en rouge

👉 Créez un panel dédié à l’affichage du nombre total de requêtes réussies sur le port 9090 de Prometheus.

Si Prometheus est hors ligne, ce panel affichera seulement "N/A".

👉 Créez un panel dédié à l’affichage du pourcentage de ram (mémoire vive) utilisée en temps réel.

Vous aurez certainement besoin de faire quelques opérations afin de transformer les résultats initiaux (en bytes) en pourcentage.

👉 Créez un dernier panel dédié à l’affichage de l’utilisation du disque principal (/dev/sda), en pourcentage.

👉 Vérifiez les données affichées par votre dashboard et arrêtez volontairement Prometheus afin de constater que les données ne s’affichent plus, comme sur le screen ci-dessous.

### 02 - Start with grafana

#### INSTALLATION DE GRAFANA

👉 Reprenez votre VM créée sur Linode hier et connectez-vous y via SSH.

👉 Suivez la documentation afin d’installer Grafana : https://grafana.com/docs/grafana/latest/setup-grafana/installation/debian/ 

⚠️ Prenez le temps de lire les étapes avant de vous lancer dans le copier/coller des commandes, vous devez installer la version "OSS" (open-source)

👉 Assurez-vous que Grafana est bien installé et démarré grâce à la commande suivante.

`sudo systemctl status grafana-server`

Contrairement à Prometheus, Grafana est lancé en arrière plan sur votre serveur et ne nécessite pas de laisser un terminal ouvert.

👉 Visitez l’interface web exposée par Grafana via votre navigateur.

👉 Ajoutez Prometheus en tant que nouvelle source de données (data source) sur Grafana.

#### DÉCOUVERTE DE GRAFANA

Afin de découvrir l’interface de Grafana et la notion de dashboards, vous avez à disposition des métriques fictives que vous pouvez afficher sous la forme de graphiques, listes, compteurs, etc.

👉 Suivez la documentation de Grafana afin de créer un dashboard semblable à celui présenté sur le screen ci-dessous (vous pouvez ouvrir l’image dans un nouvel onglet via un clic droit).