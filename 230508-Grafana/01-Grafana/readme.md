# Grafana

## Contexte

L'agrégation des métriques du système d'information peut fournir un volume important de données.

Les données agrégées sont dans un format non exploitable par un humain.

Il est nécessaire d'avoir une solution qui met en forme ces métriques afin de les exploiter efficacement.

### fonctionnement

Il est nécessaire d'installer grafana sur une machine dédiée à la visualisation des métriques.

Via le language de requête PromQL, Grafana va récupérer les métriques préparées par Prometheus.

Les métriques sont mises en forme à travers des dashboard personnalisables.