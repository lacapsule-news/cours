# Nginx dashboard

## DASHBOARD NGINX

Il est maintenant temps de vous confronter à d’autres metrics et d’autres dashboard à travers votre solution de serveur web préférée : Nginx !

👉 Assurez-vous que Prometheus et Nginx Prometheus Exporter (via Docker) soient bien démarrés et que les différentes metrics soient accessibles, notamment celles de Nginx.

👉 Trouvez et importez le dashboard officiel de Nginx Prometheus Exporter.

👉 Personnalisez le dashboard et ajoutez 2 panels dédiés à l’affichage du nombre de connexions actives et de requêtes HTTP (voir screen ci-dessous).

Comme dans le challenge "Nginx metrics", n’hésitez pas à ouvrir le site en navigation privée et sur plusieurs navigateurs pour tester votre dashboard.

👉 Une fois votre journée terminée, assurez-vous de résilier le serveur utilisé depuis hier afin d’éviter de dépasser le crédit offert par Linode.