# Start with grafana

## INSTALLATION DE GRAFANA

👉 Reprenez votre VM créée sur Linode hier et connectez-vous y via SSH.

👉 Suivez la documentation afin d’installer Grafana : https://grafana.com/docs/grafana/latest/setup-grafana/installation/debian/ 

⚠️ Prenez le temps de lire les étapes avant de vous lancer dans le copier/coller des commandes, vous devez installer la version "OSS" (open-source)

👉 Assurez-vous que Grafana est bien installé et démarré grâce à la commande suivante.

`sudo systemctl status grafana-server`

Contrairement à Prometheus, Grafana est lancé en arrière plan sur votre serveur et ne nécessite pas de laisser un terminal ouvert.

👉 Visitez l’interface web exposée par Grafana via votre navigateur.

👉 Ajoutez Prometheus en tant que nouvelle source de données (data source) sur Grafana.

## DÉCOUVERTE DE GRAFANA

Afin de découvrir l’interface de Grafana et la notion de dashboards, vous avez à disposition des métriques fictives que vous pouvez afficher sous la forme de graphiques, listes, compteurs, etc.

👉 Suivez la documentation de Grafana afin de créer un dashboard semblable à celui présenté sur le screen ci-dessous (vous pouvez ouvrir l’image dans un nouvel onglet via un clic droit).