# Adresses Ip, ports & protocoles réseaux

## Théorie

### Réseau Infomatique

> Les addresse IP, les ports et les protocoles sont des mécaniques qui permettent la communication entre différentes machines.

### Fonctionnement

> L'adresse IP est une adresse physique une machine sur un réseau afin d'établir un dialogue.

> Une ip est constituée 4 nombres allant de 0 a 255 séparés par un point.

> Et d'un masque de meme longueur (255.255.255.0 cela peut varié mais il aller plus loin en réseau)

- network : 192.168.1.0/24
	- linux: 192.168.1.1
	- windows: 192.168.1.2
		- Si windows veut parler a linux il utilisera sont adresse ip(192.168.1.1)

> Ces addresse ip sont utilisé par tout les services en ligne, avec un séparation entre un reseau local ou les machine communique et des point d'entrée(routeur) qui permette d'aller vers d'autre reseau ou de laisser d'autre réseau "rentré"

> Pour facilité l'utilisation des different réseau il existe le protocole dns qui va faire l'association entre une ip est un nom de domaine.

> Le nom de domaine fonctione en 'palier' on a d'abords les domaine de premier niveau(.com,.fr,.ml),puis il est possible d'enregistrer un sous domaine(netflix.com) qui en général est payant mais laisse ensuite acces a tout les sous domaine(\*.netflix.com)

> Notre ordinateur a des fin de rapidité garde des données dns en cache.

> Lorsque l'on fait un requete sur google.com, nous contactons d'abord les dns en cache, puis notre dns distant enregistrer, il nous répond avec l'addresse ip et on peut maintenant communiqué avec le serveur.

### Les ports

> Pour bien comprendre les ports, il faut avoir les base du modele osi ou tcp/ip

OSI | TCP/IP
------|------
Application | Application
Presentation | Application
Sessions | Application
Transport | Transport(TCP)
Réseau | Internet(IP)
Liaison(MAC) | Acces réseau
Physique(cable)| Acces réseau

> Le port est un nombre allant de 0 a 65535 (fonctionalité de la couche transport), un permet de geré un flux de donnée en fontion d'un application/sessions.

> Il existe un grange quantité de port par defaut mais customizable dans la grande majorité.

### Les protocoles

> Par dessus ces dit port nous allons rajouté des protocole afin d'avoir un systeme de communication entre les machine(HTTP,FTP,SMTP).

## Pratique

### 06 - Https please

#### SETUP

> Récupérez la ressource "httpsplease.zip" depuis l’onglet dédié sur Ariane.


> Importez le fichier "httpsplease.gns3project" dans GNS3 et démarrez tous les équipements réseau via le bouton "Start".


> Cette nouvelle simulation de réseau contient une simple machine virtuelle Debian qui rencontre un problème assez embêtant : certains sites sont joignables et d’autres non !

> A travers les étapes qui vont suivre, vous allez enquêter et surtout tenter de régler cet étrange problème…


#### WHAT’S WRONG WITH HTTPS?

> Commencez par vérifier ce soi-disant problème en exécutant une requête vers l’URL de Youtube, en HTTP et en HTTPS.


`curl -v http://www.youtube.com`

`curl -v https://www.youtube.com`


> La première requête en HTTP fonctionne bien sans aucune erreur (malgré le fait que rien ne s’affiche, mais l’option -v nous montre que le serveur a bien répondu) alors que la requête en HTTPS n’aboutit pas.


> Lancez une requête ICMP vers le serveur de www.youtube.com.



> C’est étrange, car le serveur répond bien. Le problème vient forcément de la machine… Peut être qu’un scan des ports du serveur pourrait donner plus d’indices ?


> Scannez les ports ouverts sur le serveur de www.youtube.com.



> Seul le service HTTP (port 80) s’affiche. Pourtant, Youtube est forcément HTTPS et aucune mention de ce service ou du port 443 : quelque chose doit forcément le bloquer !


> Capturez le trafic entre la VM "Debian" et "Switch" afin de comparer les paquets qui transitent lors d’une requête HTTP puis lors d’une requête HTTPS.


> Vous l’avez peut être remarqué grâce au challenge précédent, mais il y a beaucoup plus de paquets qui sont censés transiter lors d’une requête HTTPS, notamment des paquets en TCP (protocole utilisé pour communiquer en web).

#### RÉSOLUTION DU PROBLÈME

> Cette fois-ci, rien à voir avec le DNS, mais plutôt un blocage (in)volontaire du port 443 (port du protocole HTTPS), peut-être un firewall installé sur la machine ?


> Trouvez le pare-feu installé sur le système d’exploitation Debian afin de lister les règles pouvant éventuellement bloquer le port 443.



> Retirez la règle de pare-feu qui cause le blocage du port 443.

`ufw allow 443`

> Vérifiez que le problème soit résolu en exécutant une requête vers l’URL de Youtube, en HTTP et en HTTPS.


`curl -v http://www.youtube.com`

`curl -v https://www.youtube.com`


Félicitations, vous avez diagnostiqué et résolu ce problème de firewall

- Le probleme venait donc du firewall ufw inclus par defaut dans debian, il est recommander de l'activer afin d'avoir un controle suplémentaire sur les port.

### 06 - DUMMY NAME SYSTEM

#### SETUP

> Récupérez la ressource "dummynamesystem.zip" depuis l’onglet dédié sur Ariane.


> Importez le fichier "dummynamesystem.gns3project" dans GNS3 et démarrez tous les équipements réseau via le bouton "Start".


> Cette nouvelle simulation réseau contient une simple machine virtuelle Debian qui rencontre un problème assez embêtant : le site de Google est injoignable, contrairement à tous les autres sites internet !

> A travers les étapes qui vont suivre, vous allez enquêter et surtout tenter de régler cet étrange problème qui semble être lié aux paramètres DNS…


#### WHAT’S WRONG WITH THE DNS?

> Commencez par vérifier ce soi-disant problème en exécutant une requête vers l’URL de Google, en HTTP et en HTTPS.

`curl -v http://google.com`

`curl -v https://google.com`


> Les deux requêtes renvoient bien deux erreurs différentes, mais rien de probant… Peut être un problème des serveurs de Google directement, qui sait ?

> Scannez les ports ouverts sur le serveur de google.com grâce à la commande nmap.

> Malgré une réponse un peu illisible et la nécessité de scroller un peu vers le haut, les ports HTTP (80) et HTTPS (443) sont bien ouverts. Peut-être un blocage (in)volontaire de toutes les communications vers le serveur ?


> Lancez une requête ICMP vers le serveur de google.com grâce à la commande ping.

> Rien ne semble bloquer les communications vers Google, même si cette adresse renvoyée par le ping n’est pas commune, mais après tout, le ping fonctionne bien…


> Vérifiez que d’autres sites internet sont bien joignables en exécutant les requêtes suivantes.


`curl -v https://www.lacapsule.academy`
`curl -v https://www.google.com`


> Pour information "www.google.com" et "google.com" sont techniquement deux sites différents même si généralement, l’un redirige vers l’autre, mais cela montre bien que quelque chose sur le système a mal été configuré…


> Comparez les retours de ces deux commandes traceroute permettant de suivre les chemins qu'un paquet de données va prendre pour aller de la machine locale à une autre machine connectée au réseau (local ou via internet).

 

`traceroute google.com`
`traceroute google.fr`


> Il est clair que le chemin n’est pas du tout le même alors qu’il s’agit du même site et si vous analysez la dernière ligne du traceroute vers google.com, il y a une IP qui n’a pas de sens : l’adresse 1.1.1.1 semble être reliée à un service nommé Cloudflare, qui n’a rien à voir avec Google.

#### RÉSOLUTION DU PROBLÈME

> Il est évident que les DNS ont été trafiqués sur le système, volontairement ou pas, mais le constat est le suivant : le nom de domaine "google.com" pointe vers 1.1.1.1, alors qu’il ne devrait pas.


> Trouvez le fichier permettant de configurer la résolution des noms de domaine (traduction d’un nom de domaine vers une IP) en local grâce à la page de manuel de "hosts".



> Une fois le fichier identifié, vérifiez que rien de suspect ne s’y trouve et vérifiez également dans ce qui est appelé le "master file".



> Retirez les lignes suspectes via nano et redémarrez l’interface réseau afin d’appliquer les modifications grâce à la commande suivante.


`sudo /etc/init.d/networking restart`


> Vérifiez que le problème soit résolu en exécutant une requête vers l’URL de Google, en HTTP et en HTTPS.


`curl -v http://google.com`

`curl -v https://google.com`


> Vous voyez des réponses en HTML plutôt que des messages d’erreur incompréhensibles ? Félicitations, vous avez diagnostiqué et résolu ce problème de configuration DNS en local.

- Le probleme venait donc de la configuration local des dns, il n'arrivait pas a lire le ficher et le touver pas de serveur dns racine.

### 05 - REQUESTS AND CURL

#### LES PROTOCOLES HTTP & HTTPS

> La machine virtuelle "Debian-Server" n’héberge pas qu’un serveur FTP, mais aussi un serveur web (basé sur le logiciel Nginx) qui, comme son nom l’indique, est chargé de renvoyer un site web lorsqu’on lui en fait la demande.

> C’est notamment le cas lorsque vous utilisez un navigateur internet (Chrome ou Firefox), mais il est également possible de faire des requêtes web via le protocole HTTP ou HTTPS grâce à cURL.


> À partir de la VM "Debian-Client" et grâce à la commande nmap, récupérez le port d’écoute du service HTTP (nommé "nginx") hébergé sur la machine "Debian-Server".



> Requêtez le serveur web hébergé sur la VM "Debian-Server" grâce à son IP, de la façon suivante via curl.


`curl 192.168.1.X:80`


> Vous avez normalement réussi à joindre le serveur web via le port 80, qui est le port par défaut du protocole HTTP.
Le port 443 (qui n’est pas exposé par "Debian-Server") est quant à lui rattaché au protocole HTTPS, la fameuse version "sécurisée" de HTTP via SSL.


> Requêtez le serveur web grâce à son IP, de la façon suivante via curl.


```
➜  ~ curl 192.168.0.52:80
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
```

> La réponse est la même et la requête aussi puisque "http://" n’est qu’un alias vers le port 80 qui peut être omis lors d’une requête.


> Requêtez le serveur web une nouvelle fois en ajoutant l’option "-v" à la commande curl afin d’afficher toutes les étapes de la communication entre le client et le serveur.


`curl -v http://192.168.1.X`


#### CAPTURE DE TRAFIC VIA WIRESHARK

> Le logiciel Wireshark est un analyseur de paquets et de requêtes. Il est notamment utilisé dans le dépannage et l’analyse de réseaux informatiques puisqu’il permet par exemple d’observer le trafic d’une requête web entre deux machines.


> Sur GNS3, faites un clic droit sur le câble entre "Debian-Client" et "Switch" et sélectionnez l’option "Start capture" afin de capturer le trafic entre la VM "Debian-Client" et le reste du réseau via Wireshark.

> Laissez la fenêtre de Wireshark ouverte et lancez une requête depuis "Debian-Client" vers le serveur web hébergé sur "Debian-Server".


> Vous constatez que de nombreux paquets ont transité et qu’ils sont complétement visibles, c’est la "faiblesse" de HTTP.
Attardez-vous sur le second paquet du protocole HTTP afin de récupérer la page web qui a été envoyée par le serveur (VM "Debian-Server") au client (VM "Debian-Client")


> Interrompez la capture du trafic en fermant Wireshark et en faisant un clic droit sur le câble entre "Debian-Client" et "Switch" afin de sélectionner l’option "Stop capture".


> Capturez le trafic entre "Switch" et "Router" et laissez la fenêtre de Wireshark ouverte.


> Lancez une requête depuis "Debian-Client" vers le serveur web hébergé sur "Debian-Server".
> Vous ne voyez aucun paquet s’afficher ? C’est tout à fait normal, car la requête ne quitte pas le réseau local et se contente de passer d’un appareil à l’autre.


> Lancez une requête depuis "Debian-Client" vers l’URL https://google.com.
Cette fois-ci, vous êtes censés voir des paquets transiter hors du réseau local, vers internet et même en plus grand nombre, car il s’agit d’une communication chiffrée via le protocole HTTPS (contrairement à tout à l’heure où tout était en clair).


> Interrompez la capture du trafic en fermant Wireshark et en faisant un clic droit sur le câble entre "Switch" et "Router" afin de sélectionner l’option "Stop capture".

### 04 - FILE TRANSFER PROTOCOL

#### LE PROTOCOLE FTP

> Le protocole FTP (pour File Transfer Protocol) est un protocole de communication destiné au partage de fichiers sur un réseau. Il permet de copier des fichiers vers une autre machine du réseau (on parle de serveur FTP) ou encore de supprimer ou de modifier des fichiers sur ce serveur.
Ce protocole est souvent utilisé pour alimenter un site web hébergé chez un tiers en transférant rapidement des fichiers depuis un logiciel client tel que FileZilla.


> À partir de la VM "Debian-Client" et grâce à la commande nmap, récupérez le port d’écoute du service FTP (nommé "vsftpd") hébergé sur la machine "Debian-Server".

`21/tcp open  ftp     OpenBSD ftpd 6.4 (Linux port 0.17)`

> Toujours sur la VM "Debian-Client", installez le client FTP sobrement intitulé ftp grâce à la commande suivante.


`sudo apt update && sudo apt install ftp -y`


> En recherchant dans la documentation de la commande ftp (man ftp), exécutez la CLI ftp puis connectez-vous au serveur FTP hébergé sur "Debian-Server" avec "lacapsule" en nom d’utilisateur et mot de passe.

`ftp user@ip`

> Toujours grâce à la documentation, trouvez une commande permettant de lister les fichiers présents sur le serveur FTP.

`ls`

> Vous êtes censés voir une liste contenant un seul fichier : passwords.txt.


> Trouvez une commande permettant de récupérer le fichier "passwords.txt".

`get passwords.txt`

Le fichier téléchargé vers la VM "Debian-Client" sera disponible dans /home/debian/passwords.txt, n’hésitez pas à y jeter un coup d’oeil via la commande cat 😉


> Créez un fichier "hello.txt" via la commande touch dans la VM "Debian-Client".


> Laissez une trace de votre passage en uploadant le fichier "hello.txt" créé précédemment.

`mget hello.txt`

> Listez de nouveau les fichiers présents sur le serveur FTP afin de vérifier que le fichier "hello.txt" a bien été uploadé.


> Enfin, ouvrez un terminal connecté à la VM "Debian-Server" en double cliquant sur son icône (sur GNS3) et exécutez la commande suivante afin de découvrir où sont "physiquement" stockés les fichiers du serveur FTP.


`ls -l /home/lacapsule`

### 03 - SCAN WITH NMAP

#### SCAN DES PORTS

> Chaque service dépendant d'un protocole est hébergé sur une machine (serveur de fichier, serveur web, etc.) et doit exposer un port afin d’être joignable, depuis le réseau local ou même depuis internet.
Même si la plupart des services utilisent des ports réservés (80 ou 443 pour un serveur web, par exemple), il est possible de découvrir les services utilisés par une machine grâce au scanneur de ports nmap.


- Toujours sur GNS3 et depuis la VM "Debian-Client", installez la CLI nmap grâce à la commande suivante.


`sudo apt update && sudo apt install nmap -y`


- Exécutez la commande nmap suivie de l’adresse IP de la VM "Debian-Server" afin de scanner les ports ouverts.

- La VM "Debian-Server" est censée exposer trois ports : 21, 22 et 80.


- Trouvez une option à ajouter à la commande nmap afin d’obtenir les noms et les versions des services liés aux ports ouverts d’une machine.



- Entraînez-vous à scanner les ports d’adresses "connues" afin de découvrir les ports ouverts et les services associés :

- Serveur DNS de Google : 8.8.8.8 (le scan peut prendre un peu de temps)
- Serveur mail deOffice 365 (Microsoft) : smtp.office365.com

### 02 - PING PONG

#### INSTALLATION ET CONFIGURATION DE GNS3

- Récupérez la ressource "pingpong.zip" depuis l’onglet dédié sur Ariane.


- Décompressez l’archive zip et exécutez le script "gns3_install.sh" (contenu dans la ressource du challenge) afin d’installer le simulateur de réseaux informatiques GNS3.


⚠️ Pendant de l’exécution du script, lorsque la question "Should non-superusers be able to capture packets?" est posée, vous devez sélectionner "Yes" via les flèches directionnelles de votre clavier.


- Depuis le menu "Applications" de votre machine, section "Education", lancez GNS3.

- Pendant le lancement de l’application, dans le "Setup Wizard", sélectionnez "Run appliances on my local computer" puis faites "Next" autant de fois que nécessaire.


- Terminez la configuration de GNS3 en allant dans le menu "Edit" puis "Preferences", section "QEMU" et décochez l’option "Enable hardware acceleration". N’oubliez pas d’appliquer les changements.


- Importez le fichier "lab.gns3project" à partir du menu "File" puis "Import portable projet". Ce lab sera utilisé pour les challenges à venir.


- Cliquez sur le bouton "Start" afin de démarrer tous les équipements réseaux.

#### LE PROTOCOLE ICMP

- Double-cliquez sur l’icône de la machine virtuelle (VM) "Debian-Server" afin de lancer un terminal et connectez-vous avec le login "debian" et le password "debian".

```
Dans mon cas je vais utiliser deux machine de mon réseau local au maximum.

Réseau : 192.168.0.0/24
	Debian-Server:raspberrypi:192.168.0.52
	Debian-Client:ma-lap:192.168.0.84
```

- Récupérez l’adresse IP locale de cette première VM grâce à la commande habituelle. N’hésitez pas à retourner sur les cours des commandes système 😉


- Laissez le premier terminal ouvert et connectez-vous à la seconde VM "Debian-Client" afin d’obtenir également son adresse IP.


- À partir de la VM "Debian-Client", exécutez la commande ping en spécifiant l’IP de la VM "Debian-Server".

```
➜  ~ ping 192.168.0.52
PING 192.168.0.52 (192.168.0.52) 56(84) octets de données.
64 octets de 192.168.0.52 : icmp_seq=1 ttl=64 temps=173ms
```

- Si le ping fonctionne, c’est que les deux machines virtuelles sont capables de communiquer entre elles via le protocole ICMP, représenté par la commande ping.


- À partir de la VM "Debian-Server", exécutez la commande ping en spécifiant l’IP de la VM "Debian-Client".

```
raspberrypi:~ $ ping 192.168.0.84
PING 192.168.0.84 (192.168.0.84) 56(84) bytes of data.
64 bytes from 192.168.0.84: icmp_seq=1 ttl=64 time=34.3 ms
```

- Sans grande surprise, le ping fonctionne également et permet de confirmer que les deux machines virtuelles peuvent communiquer entre elles.


- À partir de la VM "Debian-Client", exécutez la commande ping vers l’URL google.com.

- Cela confirme que la machine virtuelle est bien connectée à internet et que le site de Google est en ligne (heureusement !) puisqu’il est joignable directement depuis son nom de domaine, le tout grâce à un DNS.


```
➜  ~ ping google.com
PING google.com(par21s20-in-x0e.1e100.net (2a00:1450:4007:818::200e)) 56 octets de données
```


- Récupérez l’adresse IP entre parenthèses affichée par la commande précédente et visitez-la depuis un navigateur internet.

- Vous êtes censé pouvoir accéder au site de Google depuis cette adresse IP,  car le site le plus visité de la planète est joignable depuis une seule adresse internet en HTTP/HTTPS (google.com), mais aussi depuis une multitude d’adresses IP !