# FILE TRANSFER PROTOCOL

## LE PROTOCOLE FTP

> Le protocole FTP (pour File Transfer Protocol) est un protocole de communication destiné au partage de fichiers sur un réseau. Il permet de copier des fichiers vers une autre machine du réseau (on parle de serveur FTP) ou encore de supprimer ou de modifier des fichiers sur ce serveur.
Ce protocole est souvent utilisé pour alimenter un site web hébergé chez un tiers en transférant rapidement des fichiers depuis un logiciel client tel que FileZilla.


> À partir de la VM "Debian-Client" et grâce à la commande nmap, récupérez le port d’écoute du service FTP (nommé "vsftpd") hébergé sur la machine "Debian-Server".

`21/tcp open  ftp     OpenBSD ftpd 6.4 (Linux port 0.17)`

> Toujours sur la VM "Debian-Client", installez le client FTP sobrement intitulé ftp grâce à la commande suivante.


`sudo apt update && sudo apt install ftp -y`


> En recherchant dans la documentation de la commande ftp (man ftp), exécutez la CLI ftp puis connectez-vous au serveur FTP hébergé sur "Debian-Server" avec "lacapsule" en nom d’utilisateur et mot de passe.

`ftp user@ip`

> Toujours grâce à la documentation, trouvez une commande permettant de lister les fichiers présents sur le serveur FTP.

`ls`

> Vous êtes censés voir une liste contenant un seul fichier : passwords.txt.


> Trouvez une commande permettant de récupérer le fichier "passwords.txt".

`get passwords.txt`

Le fichier téléchargé vers la VM "Debian-Client" sera disponible dans /home/debian/passwords.txt, n’hésitez pas à y jeter un coup d’oeil via la commande cat 😉


> Créez un fichier "hello.txt" via la commande touch dans la VM "Debian-Client".


> Laissez une trace de votre passage en uploadant le fichier "hello.txt" créé précédemment.

`mget hello.txt`

> Listez de nouveau les fichiers présents sur le serveur FTP afin de vérifier que le fichier "hello.txt" a bien été uploadé.


> Enfin, ouvrez un terminal connecté à la VM "Debian-Server" en double cliquant sur son icône (sur GNS3) et exécutez la commande suivante afin de découvrir où sont "physiquement" stockés les fichiers du serveur FTP.


`ls -l /home/lacapsule`