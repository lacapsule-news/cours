# PING PONG

## INSTALLATION ET CONFIGURATION DE GNS3

- Récupérez la ressource "pingpong.zip" depuis l’onglet dédié sur Ariane.


- Décompressez l’archive zip et exécutez le script "gns3_install.sh" (contenu dans la ressource du challenge) afin d’installer le simulateur de réseaux informatiques GNS3.


⚠️ Pendant de l’exécution du script, lorsque la question "Should non-superusers be able to capture packets?" est posée, vous devez sélectionner "Yes" via les flèches directionnelles de votre clavier.


- Depuis le menu "Applications" de votre machine, section "Education", lancez GNS3.

- Pendant le lancement de l’application, dans le "Setup Wizard", sélectionnez "Run appliances on my local computer" puis faites "Next" autant de fois que nécessaire.


- Terminez la configuration de GNS3 en allant dans le menu "Edit" puis "Preferences", section "QEMU" et décochez l’option "Enable hardware acceleration". N’oubliez pas d’appliquer les changements.


- Importez le fichier "lab.gns3project" à partir du menu "File" puis "Import portable projet". Ce lab sera utilisé pour les challenges à venir.


- Cliquez sur le bouton "Start" afin de démarrer tous les équipements réseaux.

## LE PROTOCOLE ICMP

- Double-cliquez sur l’icône de la machine virtuelle (VM) "Debian-Server" afin de lancer un terminal et connectez-vous avec le login "debian" et le password "debian".

```
Dans mon cas je vais utiliser deux machine de mon réseau local au maximum.

Réseau : 192.168.0.0/24
	Debian-Server:raspberrypi:192.168.0.52
	Debian-Client:ma-lap:192.168.0.84
```

- Récupérez l’adresse IP locale de cette première VM grâce à la commande habituelle. N’hésitez pas à retourner sur les cours des commandes système 😉


- Laissez le premier terminal ouvert et connectez-vous à la seconde VM "Debian-Client" afin d’obtenir également son adresse IP.


- À partir de la VM "Debian-Client", exécutez la commande ping en spécifiant l’IP de la VM "Debian-Server".

```
➜  ~ ping 192.168.0.52
PING 192.168.0.52 (192.168.0.52) 56(84) octets de données.
64 octets de 192.168.0.52 : icmp_seq=1 ttl=64 temps=173ms
```

- Si le ping fonctionne, c’est que les deux machines virtuelles sont capables de communiquer entre elles via le protocole ICMP, représenté par la commande ping.


- À partir de la VM "Debian-Server", exécutez la commande ping en spécifiant l’IP de la VM "Debian-Client".

```
raspberrypi:~ $ ping 192.168.0.84
PING 192.168.0.84 (192.168.0.84) 56(84) bytes of data.
64 bytes from 192.168.0.84: icmp_seq=1 ttl=64 time=34.3 ms
```

- Sans grande surprise, le ping fonctionne également et permet de confirmer que les deux machines virtuelles peuvent communiquer entre elles.


- À partir de la VM "Debian-Client", exécutez la commande ping vers l’URL google.com.

- Cela confirme que la machine virtuelle est bien connectée à internet et que le site de Google est en ligne (heureusement !) puisqu’il est joignable directement depuis son nom de domaine, le tout grâce à un DNS.


```
➜  ~ ping google.com
PING google.com(par21s20-in-x0e.1e100.net (2a00:1450:4007:818::200e)) 56 octets de données
```


- Récupérez l’adresse IP entre parenthèses affichée par la commande précédente et visitez-la depuis un navigateur internet.

- Vous êtes censé pouvoir accéder au site de Google depuis cette adresse IP,  car le site le plus visité de la planète est joignable depuis une seule adresse internet en HTTP/HTTPS (google.com), mais aussi depuis une multitude d’adresses IP !