# DUMMY NAME SYSTEM

## SETUP

> Récupérez la ressource "dummynamesystem.zip" depuis l’onglet dédié sur Ariane.


> Importez le fichier "dummynamesystem.gns3project" dans GNS3 et démarrez tous les équipements réseau via le bouton "Start".


> Cette nouvelle simulation réseau contient une simple machine virtuelle Debian qui rencontre un problème assez embêtant : le site de Google est injoignable, contrairement à tous les autres sites internet !

> A travers les étapes qui vont suivre, vous allez enquêter et surtout tenter de régler cet étrange problème qui semble être lié aux paramètres DNS…


## WHAT’S WRONG WITH THE DNS?

> Commencez par vérifier ce soi-disant problème en exécutant une requête vers l’URL de Google, en HTTP et en HTTPS.

`curl -v http://google.com`

`curl -v https://google.com`


> Les deux requêtes renvoient bien deux erreurs différentes, mais rien de probant… Peut être un problème des serveurs de Google directement, qui sait ?

> Scannez les ports ouverts sur le serveur de google.com grâce à la commande nmap.

> Malgré une réponse un peu illisible et la nécessité de scroller un peu vers le haut, les ports HTTP (80) et HTTPS (443) sont bien ouverts. Peut-être un blocage (in)volontaire de toutes les communications vers le serveur ?


> Lancez une requête ICMP vers le serveur de google.com grâce à la commande ping.

> Rien ne semble bloquer les communications vers Google, même si cette adresse renvoyée par le ping n’est pas commune, mais après tout, le ping fonctionne bien…


> Vérifiez que d’autres sites internet sont bien joignables en exécutant les requêtes suivantes.


`curl -v https://www.lacapsule.academy`
`curl -v https://www.google.com`


> Pour information "www.google.com" et "google.com" sont techniquement deux sites différents même si généralement, l’un redirige vers l’autre, mais cela montre bien que quelque chose sur le système a mal été configuré…


> Comparez les retours de ces deux commandes traceroute permettant de suivre les chemins qu'un paquet de données va prendre pour aller de la machine locale à une autre machine connectée au réseau (local ou via internet).

 

`traceroute google.com`
`traceroute google.fr`


> Il est clair que le chemin n’est pas du tout le même alors qu’il s’agit du même site et si vous analysez la dernière ligne du traceroute vers google.com, il y a une IP qui n’a pas de sens : l’adresse 1.1.1.1 semble être reliée à un service nommé Cloudflare, qui n’a rien à voir avec Google.

## RÉSOLUTION DU PROBLÈME

> Il est évident que les DNS ont été trafiqués sur le système, volontairement ou pas, mais le constat est le suivant : le nom de domaine "google.com" pointe vers 1.1.1.1, alors qu’il ne devrait pas.


> Trouvez le fichier permettant de configurer la résolution des noms de domaine (traduction d’un nom de domaine vers une IP) en local grâce à la page de manuel de "hosts".



> Une fois le fichier identifié, vérifiez que rien de suspect ne s’y trouve et vérifiez également dans ce qui est appelé le "master file".



> Retirez les lignes suspectes via nano et redémarrez l’interface réseau afin d’appliquer les modifications grâce à la commande suivante.


`sudo /etc/init.d/networking restart`


> Vérifiez que le problème soit résolu en exécutant une requête vers l’URL de Google, en HTTP et en HTTPS.


`curl -v http://google.com`

`curl -v https://google.com`


> Vous voyez des réponses en HTML plutôt que des messages d’erreur incompréhensibles ? Félicitations, vous avez diagnostiqué et résolu ce problème de configuration DNS en local.

- Le probleme venait donc de la configuration local des dns, il n'arrivait pas a lire le ficher et le touver pas de serveur dns racine.