# ADRESSES IP, PORTS & PROTOCOLES RÉSEAUX

## Contexte

### Réseau Infomatique

> Les addresse IP, les ports et les protocoles sont des mécaniques qui permettent la communication entre différentes machines.

### Fonctionnement

> L'adresse IP est une adresse physique une machine sur un réseau afin d'établir un dialogue.

> Une ip est constituée 4 nombres allant de 0 a 255 séparés par un point.

> Et d'un masque de meme longueur (255.255.255.0 cela peut varié mais il aller plus loin en réseau)

- network : 192.168.1.0/24
	- linux: 192.168.1.1
	- windows: 192.168.1.2
		- Si windows veut parler a linux il utilisera sont adresse ip(192.168.1.1)

> Ces addresse ip sont utilisé par tout les services en ligne, avec un séparation entre un reseau local ou les machine communique et des point d'entrée(routeur) qui permette d'aller vers d'autre reseau ou de laisser d'autre réseau "rentré"

> Pour facilité l'utilisation des different réseau il existe le protocole dns qui va faire l'association entre une ip est un nom de domaine.

> Le nom de domaine fonctione en 'palier' on a d'abords les domaine de premier niveau(.com,.fr,.ml),puis il est possible d'enregistrer un sous domaine(netflix.com) qui en général est payant mais laisse ensuite acces a tout les sous domaine(\*.netflix.com)

> Notre ordinateur a des fin de rapidité garde des données dns en cache.

> Lorsque l'on fait un requete sur google.com, nous contactons d'abord les dns en cache, puis notre dns distant enregistrer, il nous répond avec l'addresse ip et on peut maintenant communiqué avec le serveur.

### Les ports

> Pour bien comprendre les ports, il faut avoir les base du modele osi ou tcp/ip

OSI | TCP/IP
------|------
Application | Application
Presentation | Application
Sessions | Application
Transport | Transport(TCP)
Réseau | Internet(IP)
Liaison(MAC) | Acces réseau
Physique(cable)| Acces réseau

> Le port est un nombre allant de 0 a 65535 (fonctionalité de la couche transport), un permet de geré un flux de donnée en fontion d'un application/sessions.

> Il existe un grange quantité de port par defaut mais customizable dans la grande majorité.

### Les protocoles

> Par dessus ces dit port nous allons rajouté des protocole afin d'avoir un systeme de communication entre les machine(HTTP,FTP,SMTP).