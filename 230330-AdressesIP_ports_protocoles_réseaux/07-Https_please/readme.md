# SETUP

> Récupérez la ressource "httpsplease.zip" depuis l’onglet dédié sur Ariane.


> Importez le fichier "httpsplease.gns3project" dans GNS3 et démarrez tous les équipements réseau via le bouton "Start".


> Cette nouvelle simulation de réseau contient une simple machine virtuelle Debian qui rencontre un problème assez embêtant : certains sites sont joignables et d’autres non !

> A travers les étapes qui vont suivre, vous allez enquêter et surtout tenter de régler cet étrange problème…


## WHAT’S WRONG WITH HTTPS?

> Commencez par vérifier ce soi-disant problème en exécutant une requête vers l’URL de Youtube, en HTTP et en HTTPS.


`curl -v http://www.youtube.com`

`curl -v https://www.youtube.com`


> La première requête en HTTP fonctionne bien sans aucune erreur (malgré le fait que rien ne s’affiche, mais l’option -v nous montre que le serveur a bien répondu) alors que la requête en HTTPS n’aboutit pas.


> Lancez une requête ICMP vers le serveur de www.youtube.com.



> C’est étrange, car le serveur répond bien. Le problème vient forcément de la machine… Peut être qu’un scan des ports du serveur pourrait donner plus d’indices ?


> Scannez les ports ouverts sur le serveur de www.youtube.com.



> Seul le service HTTP (port 80) s’affiche. Pourtant, Youtube est forcément HTTPS et aucune mention de ce service ou du port 443 : quelque chose doit forcément le bloquer !


> Capturez le trafic entre la VM "Debian" et "Switch" afin de comparer les paquets qui transitent lors d’une requête HTTP puis lors d’une requête HTTPS.


> Vous l’avez peut être remarqué grâce au challenge précédent, mais il y a beaucoup plus de paquets qui sont censés transiter lors d’une requête HTTPS, notamment des paquets en TCP (protocole utilisé pour communiquer en web).

## RÉSOLUTION DU PROBLÈME

> Cette fois-ci, rien à voir avec le DNS, mais plutôt un blocage (in)volontaire du port 443 (port du protocole HTTPS), peut-être un firewall installé sur la machine ?


> Trouvez le pare-feu installé sur le système d’exploitation Debian afin de lister les règles pouvant éventuellement bloquer le port 443.



> Retirez la règle de pare-feu qui cause le blocage du port 443.

`ufw allow 443`

> Vérifiez que le problème soit résolu en exécutant une requête vers l’URL de Youtube, en HTTP et en HTTPS.


`curl -v http://www.youtube.com`

`curl -v https://www.youtube.com`


Félicitations, vous avez diagnostiqué et résolu ce problème de firewall

- Le probleme venait donc du firewall ufw inclus par defaut dans debian, il est recommander de l'activer afin d'avoir un controle suplémentaire sur les port.