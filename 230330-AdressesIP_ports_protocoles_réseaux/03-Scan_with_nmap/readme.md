# SCAN WITH NMAP

## SCAN DES PORTS

> Chaque service dépendant d'un protocole est hébergé sur une machine (serveur de fichier, serveur web, etc.) et doit exposer un port afin d’être joignable, depuis le réseau local ou même depuis internet.
Même si la plupart des services utilisent des ports réservés (80 ou 443 pour un serveur web, par exemple), il est possible de découvrir les services utilisés par une machine grâce au scanneur de ports nmap.


- Toujours sur GNS3 et depuis la VM "Debian-Client", installez la CLI nmap grâce à la commande suivante.


`sudo apt update && sudo apt install nmap -y`


- Exécutez la commande nmap suivie de l’adresse IP de la VM "Debian-Server" afin de scanner les ports ouverts.

- La VM "Debian-Server" est censée exposer trois ports : 21, 22 et 80.


- Trouvez une option à ajouter à la commande nmap afin d’obtenir les noms et les versions des services liés aux ports ouverts d’une machine.



- Entraînez-vous à scanner les ports d’adresses "connues" afin de découvrir les ports ouverts et les services associés :

- Serveur DNS de Google : 8.8.8.8 (le scan peut prendre un peu de temps)
- Serveur mail deOffice 365 (Microsoft) : smtp.office365.com