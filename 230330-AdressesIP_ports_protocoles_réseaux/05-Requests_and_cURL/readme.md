# REQUESTS AND CURL

## LES PROTOCOLES HTTP & HTTPS

> La machine virtuelle "Debian-Server" n’héberge pas qu’un serveur FTP, mais aussi un serveur web (basé sur le logiciel Nginx) qui, comme son nom l’indique, est chargé de renvoyer un site web lorsqu’on lui en fait la demande.

> C’est notamment le cas lorsque vous utilisez un navigateur internet (Chrome ou Firefox), mais il est également possible de faire des requêtes web via le protocole HTTP ou HTTPS grâce à cURL.


> À partir de la VM "Debian-Client" et grâce à la commande nmap, récupérez le port d’écoute du service HTTP (nommé "nginx") hébergé sur la machine "Debian-Server".



> Requêtez le serveur web hébergé sur la VM "Debian-Server" grâce à son IP, de la façon suivante via curl.


`curl 192.168.1.X:80`


> Vous avez normalement réussi à joindre le serveur web via le port 80, qui est le port par défaut du protocole HTTP.
Le port 443 (qui n’est pas exposé par "Debian-Server") est quant à lui rattaché au protocole HTTPS, la fameuse version "sécurisée" de HTTP via SSL.


> Requêtez le serveur web grâce à son IP, de la façon suivante via curl.


```
➜  ~ curl 192.168.0.52:80
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
```

> La réponse est la même et la requête aussi puisque "http://" n’est qu’un alias vers le port 80 qui peut être omis lors d’une requête.


> Requêtez le serveur web une nouvelle fois en ajoutant l’option "-v" à la commande curl afin d’afficher toutes les étapes de la communication entre le client et le serveur.


`curl -v http://192.168.1.X`


## CAPTURE DE TRAFIC VIA WIRESHARK

> Le logiciel Wireshark est un analyseur de paquets et de requêtes. Il est notamment utilisé dans le dépannage et l’analyse de réseaux informatiques puisqu’il permet par exemple d’observer le trafic d’une requête web entre deux machines.


> Sur GNS3, faites un clic droit sur le câble entre "Debian-Client" et "Switch" et sélectionnez l’option "Start capture" afin de capturer le trafic entre la VM "Debian-Client" et le reste du réseau via Wireshark.

> Laissez la fenêtre de Wireshark ouverte et lancez une requête depuis "Debian-Client" vers le serveur web hébergé sur "Debian-Server".


> Vous constatez que de nombreux paquets ont transité et qu’ils sont complétement visibles, c’est la "faiblesse" de HTTP.
Attardez-vous sur le second paquet du protocole HTTP afin de récupérer la page web qui a été envoyée par le serveur (VM "Debian-Server") au client (VM "Debian-Client")


> Interrompez la capture du trafic en fermant Wireshark et en faisant un clic droit sur le câble entre "Debian-Client" et "Switch" afin de sélectionner l’option "Stop capture".


> Capturez le trafic entre "Switch" et "Router" et laissez la fenêtre de Wireshark ouverte.


> Lancez une requête depuis "Debian-Client" vers le serveur web hébergé sur "Debian-Server".
> Vous ne voyez aucun paquet s’afficher ? C’est tout à fait normal, car la requête ne quitte pas le réseau local et se contente de passer d’un appareil à l’autre.


> Lancez une requête depuis "Debian-Client" vers l’URL https://google.com.
Cette fois-ci, vous êtes censés voir des paquets transiter hors du réseau local, vers internet et même en plus grand nombre, car il s’agit d’une communication chiffrée via le protocole HTTPS (contrairement à tout à l’heure où tout était en clair).


> Interrompez la capture du trafic en fermant Wireshark et en faisant un clic droit sur le câble entre "Switch" et "Router" afin de sélectionner l’option "Stop capture".