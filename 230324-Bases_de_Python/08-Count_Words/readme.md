# Count Words

> En utilisant l'instruction split, écrivez l'instruction qui permet de découper cette citation de Jean Claude Van Damme en mots distincts.
Stockez le résultat généré par la méthode split dans une variable nommée text_split.
Stockez le nombre total de mots trouvés dans la variable count_word. 


> En utilisant les variables count_word (pour la condition d’arrêt de la boucle) et text_split (pour la condition dans la boucle), ajoutez un traitement pour ne compter que les mots de plus de 2 caractères.
Stockez le résultat obtenu dans la variable count_word_filtered.

```
def counting_words(text):
    count_word = 0
    count_word_filtered = 0
    
    # Insert your code here
    for word in text.split():
        if len(word) <= 2:
            count_word_filtered+=1
        else:
            count_word+=1
            

    return count_word, count_word_filtered
   
print(counting_words("Si tu travailles avec un marteau-piqueur pendant un tremblement de terre, désynchronise-toi, sinon tu travailles pour rien"))
print("Note : Le premier chiffre ci-dessus correspond à count_word et le second à count_word_filtered")
```