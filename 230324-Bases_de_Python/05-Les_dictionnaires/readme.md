# Les dictionnaires

## Initialisation

> Complétez la fonction display_contact() pour stocker dans la variable contact le dictionnaire suivant :

- Une clé prenom avec pour valeur "Vanessa"
- Une clé phone avec pour valeur "0612345678"
- Une clé email avec pour valeur "vanessa@gmail.com"

> Attention à ne pas mettre de majuscule ni de caractère accentué dans le nom des clés.


```
def display_contact():
    contact = None

    # Insert your code here
    contact = {
        'prenom':'Vanessa',
        'phone':'0612345678',
        'email':'vanessa@gmail.com'
    }


    # End of code insertion

    return contact

print(display_contact())
```

## Lire

> Complétez la fonction display_first_name() en modifiant la variable first_name à l’endroit indiqué afin d’afficher uniquement la valeur de la clé prenom.

```
contact = {
    "prenom": "Vanessa",
    "phone": "0612345678",
    "email": "vanessa@gmail.com"
}
    
def display_first_name(contact):
    first_name = None
    
    # Insert your code here

    first_name=contact['prenom']

    # End of code insertion

    return first_name

print(display_first_name(contact))
```

## Modifier

> Complétez la fonction update_phone() à l’endroit indiqué afin de mettre à jour le numéro de téléphone contenu dans la variable contact avec la valeur suivante :  "0712345678".

```
contact = {
    "prenom": "Vanessa",
    "phone": "0612345678",
    "email": "vanessa@gmail.com"
}
    
def update_phone(contact):
    # Insert your code here

    contact['phone']='0712345678'

    # End of code insertion

    return contact

print(update_phone(contact))
```

## Liste de dictionnaires

> Complétez la fonction add_john() afin d’ajouter un 3e utilisateur à la liste en utilisant la méthode append sur la liste users.

> Le contact aura une clé nom avec comme valeur "John", une clé phone avec comme valeur "0123456789" et une clé email avec comme valeur "john@gmail.com".

```
users = [
    {
        "nom": "Vanessa",
        "phone": "0544125478",
        "email": "vanessa@gmail.com"
    },
    {
        "nom": "Théo",
        "phone": "0544125478",
        "email": "theo@gmail.com"
    }
]

def add_john(users):
    # Insert your code here

    users.append({
        "nom":"John",
        "phone":"0123456789",
        "email":"john@gmail.com"
    })

    # End of code insertion

    return users

print(add_john(users))
```