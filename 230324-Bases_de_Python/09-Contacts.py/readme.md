# CONTACTS.py

- Mettez à jour la nouvelle liste "new_list" en conservant toutes les données de la liste "contacts" mais en transformant les numéros de téléphone au format international (+33612345678).


- Dans la fonction address_book, ajoutez une nouvelle clé "admin" pour chaque contact. Cette clé doit être à True pour john et à False pour les autres.


- Modifiez le tableau pour supprimer les doublons de numéros de téléphone. (L'entrée elisa doit etre supprimer du tableau)


```
contacts = [
   {
       "prenom" : 'john',
       "telephone" : '0611223344',
   },
   {
       "prenom" : 'elise',
       "telephone" : '0655667799'
   },
   {
       "prenom" : 'franck',
       "telephone" : '0612345678'
   },
   {
       "prenom" : 'elisa',
       "telephone" : '0612345678'
   }
]

def address_book(contacts = contacts):
    new_list = []
    
    # Insert your code here
    phone_list=[]
    for contact in contacts:
        
        contact['telephone']="+33"+contact['telephone'][1:]
        
        if contact['prenom'] == 'john':
            contact['admin']=True
        else:
            contact['admin']=False
        
        if contact['telephone'] not in phone_list:
            phone_list.append(contact['telephone'])
            new_list.append(contact)
    
    
    # End of code insertion
    
    return new_list

print(address_book(contacts))
```