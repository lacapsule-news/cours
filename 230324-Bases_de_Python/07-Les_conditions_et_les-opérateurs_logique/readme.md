# Les conditions et les opérateurs logiques

## Initialisation

> Dans la fonction check_playlist(),  ajoutez une condition qui attribue à message la valeur suivante : "Attention, il vous reste moins de 4 titres" s'il y a un nombre de titres inférieur à 4.

```
playlist = [
    "Katy Perry - Chained To The Rhythm", 
    "Ed Sheeran - Shape of You", 
    "Maroon 5 - Don't Wanna Know"
]

def check_playlist(playlist = playlist):
    message = "Rien à signaler."

    # Insert your code here
    if len(playlist) < 4:
        message = "Attention, il vous reste moins de 4 titres"


    # End of code insertion

    return message

print(check_playlist(playlist))
```

## Les conditions et les types

> Vous allez compléter la fonction check_data() afin de vérifier les informations et renvoyer un message selon certaines conditions.
En utilisant la variable email, définissez une condition qui ajoute "email validé" dans la variable messages si la longueur de email est supérieur à 10. 

> En utilisant la variable age, définissez une condition qui ajoute "accès bloqué" dans la variable messages si age est inférieur à 18. 

> En utilisant la variable basket, définissez une condition qui ajoute "commande validée" dans la variable messages si la longueur de basket est supérieur à 0. 

```
email = "john.doe@lacapsule.academy"
age = 32
basket = [
    {
        "article": "jean",
        "price": 32
    },
    {
        "article": "t-shirt",
        "price": 15
    },
    {
        "article": "pull",
        "price": 40
    }
]

def check_data(email = email, age = age, basket = basket):
    messages = []

    # Insert your code here
    if len(email) > 10:
        messages.append("email validé")
    if age < 18:
        messages.append("accès bloqué")
    if len(basket) > 0:
        messages.append("commande validée")


    # End of code insertion

    return messages

print(check_data(email, age, basket))
```

## Les opérateurs logiques

> Vous allez compléter la fonction check_data() pour remplir dynamiquement la liste messages. En utilisant les variables email et basket, définissez une condition qui ajoute le message "panier validé" si la longueur de email est supérieur à 10 et si la longueur de basket est supérieur à 0. 


> En utilisant les variables email et basket, ajoutez une 2ème condition qui enregistre le message "problème de commande" si la longueur de email vaut 0 ou si la longueur de basket vaut 0. 


> En utilisant la variable age et l’opérateur not, ajoutez une 3ème condition qui ajoute le message "accès autorisé" si age n’est pas inférieur à 18. 

```
email = "john.doe@lacapsule.academy"
age = 32
basket = [
    {
        "article": "jean",
        "price": 32
    },
    {
        "article": "t-shirt",
        "price": 15
    },
    {
        "article": "pull",
        "price": 40
    }
]

def check_data(email = email, age = age, basket = basket):
    messages = []

    # Insert your code here
    if len(email) > 10 and len(basket) > 0:
        messages.append("panier validé")
    elif len(email)== 0 or len(basket) ==0:
        messages.append("problème de commande")
    
    if not age < 18:
        messages.append("accès autorisé")


    # End of code insertion

    return messages

print(check_data(email, age, basket))
```

## Les conditions et les boucles

> Dans la fonction check_order(), créer une boucle pour passer sur tous les éléments de commandes.
> Cette boucle devra ajouter le message "commande nom_du_produit validée" dans la liste messages seulement si le stock est supérieur ou égal à la quantité commandée.

```
commandes = [
    {
        'produit': 'jean',
        'qty': 2,
        'stock': 5
    },
    {
        'produit': 't-shirt',
        'qty': 4,
        'stock': 3
    },
    {
        'produit': 'pull',
        'qty': 1,
        'stock': 10
    }   
]

def check_order(commandes = commandes):
    messages = []

    # Insert your code here
    for product in commandes:
        if product['stock'] >= product['qty']:
            messages.append('commande '+product['produit']+' validée')


    # End of code insertion

    return messages

print(check_order(commandes))
```