# Les chaine de caratères

## Initialisation

> Créer une variable nom en l’initialisant à la valeur "John".

> Attention à bien respecter les majuscules et minuscules de la chaîne de caractère !

> Affichez ensuite la valeur de la variable nom avec l’instruction print.

```
nom="John"
print(nom)
```

## Concaténation

> Définissez une variable hello avec comme valeur "Bonjour comment ça va".

> À la ligne en dessous, définissez une deuxième variable nommée prenom avec comme valeur le prénom "John".

> Sur une nouvelle ligne, définissez une troisième variable phrase égale à la concaténation des variables hello et prenom.
Affichez la valeur de la variable phrase grâce à la commande print.


> Vous pouvez constater que le résultat peut être encore amélioré, le prénom "John" est collé directement au mot "va".

> Il devrait y avoir une espace entre les deux, mais Python n’en ajoutera pas automatiquement si on ne lui indique pas explicitement.

> Il suffira d’ajouter un espace à la fin de la chaîne de caractère "Bonjour comment ça va "

> L’espace sera placé entre la dernière lettre de la phrase et le guillemet. 

```
hello="Bonjour comment ça va "
prenom = "John"
phrase = hello + prenom

print(phrase)
```

## Longueur

> Vous avez à disposition une variable exemple initialisée à la valeur "Je travaille en py".
> Créer une variable compteur qui aura pour valeur la longueur de la chaîne de caractères contenue dans la variable exemple.

> Affichez compteur dans la console.

```
exemple = "Je travaille en py"
compteur = len(exemple)

print(compteur)
```

## Cibler un caractère

> Nous avons initialisé une variable job avec comme valeur "DevOps".
> Créez une variable shortened_job. En utilisant la concaténation du 1er et du 4ème caractère, assignez à cette variable la valeur "DO".

```
job = "DevOps"
shortened_job=job[0]+job[3]

print(shortened_job)
```

## Découper

> À vous de jouer, définir une variable dessert initialisée avec la chaîne de caractères "corbeille de fruits frais"
> Créez une variable segment qui contiendra le segment "fruits" de la variable dessert.

> Affichez la variable segment dans la console.

```
dessert = "corbeille de fruits frais"
segment = dessert[13:19]

print(segment)
```

## Transformer

> Nous avons initialisé une variable name avec la valeur "John Doe"
> Créez une variable maj qui aura pour valeur name en majsucules.

> Créez une variable min qui aura pour valeur name en minuscules.

> Affichez avec premier print affichez la valeur de maj.

> À la ligne suivante, affichez la valeur de min.

```
name = "John Doe"
maj=name.upper()
min=name.lower()
print(maj)
print(min)
```