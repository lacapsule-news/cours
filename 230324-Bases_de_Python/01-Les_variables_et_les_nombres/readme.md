# Les variables et les nombres

## Initialisation

> Définissez une variable qui se nomme second et l’initialiser à 60, puis affichez le contenu de cette variable.

```
second = 60

print(second)
```

## Assignation

>Définissez une variable qui se nomme minute et initialisez-la à 20.

> À la suite, modifiez la variable minute en lui donnant comme nouvelle valeur 30, puis affichez le contenu de cette variable. 

```
minute = 20

minute = 30

print(minute)
```

## Opération

> Définissez une variable qui se nomme second et initialisez-la à 60.

> À la suite, définissez une autre variable minute initialisée à 30.

> Créez une dernière variable total qui sera égale à la multiplication de second et minute, puis affichez le contenu de cette variable. 

```
second = 60
minute = 30
total = second*minute
print(total)
```

## Modifier

> Imaginons une variable nommée email qui corresponde au nombre d’e-mails reçus dans la journée. Initialisez cette variable à 23.

> À la suite, ajoutez 5 à la variable en utilisant la manière décrite ci-dessus, puis affichez le contenu de cette variable.

```
email =23

email += 5
```

## Raccourci

> Imaginons une variable nommée score_game qui correspond au score d’un jeu vidéo. Initialisez cette variable à 420. 

> À la ligne en dessous, ajoutez 15 au score en utilisant un raccourci syntaxique, puis affichez le contenu de cette variable.

```
score_game = 420
score_game += 15
print(score_game)
```
