# Anagramme

- Vous allez modifier la fonction list_anagrams() pour pouvoir identifier tous les anagrammes du mot "ordre" ("ordre" étant inclus) parmi les mots de la liste data. Vous stockerez tous ces mots dans la nouvelle liste anagrams.

```
data = [ "ordre", "donner", "roder", "recevoir", "dorer", "plaisir", "aaaaa"]

def list_anagrams(data = data):
    anagrams = []
    origin_word = "ordre"
    
    # Insert your code here
    for element in data:
        if sorted(element) == sorted(origin_word):
            anagrams.append(element)

    # End of code insertion
    return anagrams
    
print(list_anagrams(data))
```

