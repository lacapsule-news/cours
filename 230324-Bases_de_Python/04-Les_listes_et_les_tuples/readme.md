# Les listes et les tuples

## Initialisation

- Créez une variable fruits qui va contenir une liste avec les valeurs suivantes : pommes, poires, bananes (attention à bien respecter l’ordre des fruits lors de la création de la liste).

- Afficher cette variable fruits avec print. 

```
fruits = ["pommes","poires","bananes"]
print(fruits)
```
## Lire

> Nous avons déclaré une variable contact qui contient une liste avec les valeurs suivantes : Vanessa, Hugo, Michael.

> Créez la variable third_user contenant le troisième utilisateur de la liste contact en utilisant la méthode décrite ci-dessus puis affichez-la dans la console. 

```
contact = ["Vanessa", "Hugo", "Michael"]
third_user=contact[2]
print(third_user)
```

## Modifier

> En suivant la méthode décrite ci-dessus, modifiez le 3ème prénom contenu dans la liste contact afin de mettre "Léo" à la place de "Michael" puis affichez la variable dans la console.

```
contact = ["Vanessa", "Hugo", "Michael"]

contact[2]="Léo"
```

## Longueur

> Créez un variable count qui contiendra le nombre d'éléments de la liste contact. Affichez count dans la console. 

> À la suite de votre bloc de code, créez une variable last_one qui contiendra le dernier élément de la liste contact, quelle que soit sa longueur (donc sans écrire contact[2]).

> Il vous faudra utiliser deux notions précédemment abordées : l’accès aux éléments et le calcul de la longueur.

```
contact = ["Vanessa", "Hugo", "Michael"]

count = len(contact)

print(count)
last_one=contact[-1]
print(last_one)
```

## Ajouter

> Nous retrouvons la liste contact avec 3 prénoms.

> Ajoutez "Jules" au début de la liste ainsi que "Marion" à la fin de la liste puis affichez-la.

```
contact = ["Vanessa", "Hugo", "Michael"]
contact.insert(0,"Jules")
contact.append("Marion")
print(contact)
```

## Supprimer

> Nous disposons de la liste contact avec 4 prénoms.

> Supprimez la dernière valeur de la liste ainsi que la première valeur de la liste puis affichez-la dans la console. 

```
contact = ["Vanessa", "Hugo", "Michael", "Léo"]

contact.pop(-1)
contact.pop(0)
print(contact)
```

## Les tuples

> Vous disposez de la variable contact qui représente un tuple avec les valeurs suivantes : Vanessa, Hugo, Michael, Léo.

> Enregistrez dans la variable second_user le 2ème élément du tuple puis affichez-la.

```
contact = ("Vanessa", "Hugo", "Michael", "Léo")

second_user=contact[1]

print(second_user)
```