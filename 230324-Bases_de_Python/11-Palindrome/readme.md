# Palindrome

- Dans la liste proposée, filtrer les palindromes et les stocker dans la nouvelle liste palindromes.

```
liste = ["bonjour", "kayak", "salut", "ressasser", "toto", "python", "été", "algo"]

def is_palindrome(liste = liste):
    palindromes = []
    
    # Insert your code here
    for element in liste:
        if element == element[::-1]:
            palindromes.append(element)
    
    # End of code insertion
    
    return palindromes

print(is_palindrome(liste))
```