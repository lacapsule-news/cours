# Les boucles

## Initialisation

> Dans la fonction update_data(), créer une boucle qui va tourner 5 fois en utilisant pour compteur de boucle une variable nommée i initialisée à 0. 

> Cette boucle viendra ajouter 1 à la variable counter à chaque tour de boucle. 


> Complétez la boucle afin d’ajouter un message dans la liste messages à chaque tour de boucle. Chaque message prendra cette forme : "tour de boucle numéro 1", "tour de boucle numéro 2", etc.

> Penser à mettre un espace entre la dernière lettre de la chaîne de caractères et le guillemet. 


> Au début du programme une variable contact a été créée, elle contient une liste avec les valeurs suivantes : Vanessa, Hugo, Michael, Léo, John.

> Une boucle pourrait nous servir à parcourir l’intégralité des prénoms enregistrés dans cette liste.

> Modifier la boucle pour que chaque élément de la liste messages contienne également le prénom associé à l’index i. Chaque message prendra cette forme : "tour de boucle numéro 1 Vanessa", "tour de boucle numéro 2 Hugo", .etc


> Nous avons utilisé la variable i en tant qu’indice de la liste, ce qui nous a permis de lire la totalité de son contenu. En effet, elle est passée progressivement de 0 à 4 ce qui correspond aux indices de la liste contact. 

> Améliorons encore cette approche pour éviter d'avoir à modifier les conditions de la boucle à chaque fois que la liste évolue (c’est-à-dire si l’on y ajoute ou supprime des éléments).

> Essayez d’ajouter quelques prénoms dans contact puis remplacez le chiffre 5 de la condition de la boucle par l’instruction permettant de récupérer dynamiquement la longueur de la liste contact. 

```
counter = 0
messages = []
contact = ["Vanessa", "Hugo", "Michael", "Léo", "John", "test"]

def update_data(counter = counter, messages = messages, contact = contact):
    # Insert your code here
    for i in range(len(contact)):
        counter += 1
        messages.append("tour de boucle numéro "+str(counter)+" "+contact[i])



    # End of code insertion

    return counter, messages

print(update_data(counter, messages, contact))
```

## Manipulation de données

> Au début du programme une variable factures a été créée, elle contient une liste avec des valeurs de factures différentes.

> Une boucle pourrait nous servir à additionner le total de toutes les factures enregistrées dans cette liste.

> Nous avons utilisé la longueur d’une liste pour créer une boucle pour que le code puisse passer sur chaque élément d’une liste.

> En utilisant cette approche, complétez la fonction calc_total() avec une boucle qui mettra à jour le compteur sur chaque tour de boucle (en utilisant une syntaxe raccourcie de type +=). La variable compteur servira pour stocker le résultat.

```
factures = [23, 45, 65, 12, 86]
compteur = 0

def calc_total(factures = factures, compteur = compteur):
    # Insert your code here
    for facture in factures:
        compteur += facture



    # End of code insertion

    return compteur

print(calc_total(factures, compteur))
```
## Modification de texte

> Allons un peu plus loin! Nous avons vu que les boucles nous permettent de traiter et manipuler les données dans une liste. Revenons sur notre liste contact. Dans ce cas, les noms dans la liste ne sont pas formatés correctement - les majuscules et minuscules ne sont pas respectées. Complétez la fonction format_text() avec une boucle. Cette boucle modifiera chaque élément dans la liste pour afficher la première lettre en majuscule et le reste du mot en minuscule.

```
contact = ["john", "vanessa", "FRANCK"]

def format_text(contact):
    # Insert your code here
    contacts=[]
    for person in contact:
        contacts.append(person[0].upper()+person[1:].lower())
    contact = contacts


    # End of code insertion

    return contact

print(format_text(contact))
```