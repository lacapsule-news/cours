# Base de python

## Théorique

### Introduction a python

#### Contexte

> Un ordinateur c'est puissant, mais pas intéligent... On doit lui transmettre des instruction

> Python est un langage de programmation interprété. Très utilisé dans l'automatisation de tâche, mais également dans le monde scientifique pour ses librairies optimisées pour le calcul numérique.

#### Types de données a variables

- Les nombres
- Les chaines de caractères
- Les booléens

##### Les nombres

- Entier : 150
- Négatif : -150
- A virgule : 1,5

##### Les chaînes de caratères

- Récupérérer la longueur d'une chaîne de caratères
`len("Hello World!")`

- Modifier la case
`"Ma chaine".lower() | "Ma chaine".upper()`

- Imprimer un certain nombre de caratères

`"Hello world!"[0:5]`

##### Les booléens

> Peut valoir true ou false

### Les variables

`variable = "assignment"`


#### Incrémentation/Décrementation

```
score = 5
score +=2
score -=2
```

### Les Liste

```
names=[
"Vanessa",
"Hugo",
"Victor"]
```

`print(names[1])`
`names.append('New user')`
`names.insert(0,'New user')`
`names.pop()`
`names.pop(0)`

### Les tuples

```
names= (
"Vanessa",
"Hugo",
"Victor"
)
```

### Les dictionnaires

```
contact = {
	"name": "Vannessa",
	"phone": "0654334522",
	"mail": "mail@mail.mail"

}

names = [{
	"name": "Vannessa",
	"phone": "0654334522",
	"mail": "mail@mail.mail"

},{
	"name": "Hugo",
	"phone": "065854263",
	"mail": "mail@mail.mail"

}]
```

### Boucle, conditions & opérateurs logiques

`for i in range(0,100):`
`if score > 100:`

## Pratique

### XX Cards classe

```
import random

class cards():
	def __init__(self,nbPlayer,nbCarte):
		cards=[[number, color] for color in ['Piques','Carreaux','Cœurs','Trèfles'] for number in range(1,13)]

		self.stack=self.shuffle(cards)
		self.distribution(nbPlayer,nbCarte)
		

	def shuffle(self,cards):
		random.shuffle(cards)
		return cards
	
	def distribution(self,nbPlayer,nbCarte):
		self.deck=[['player '+str(i+1)] for i in range(nbPlayer)]
		for cards in range(nbCarte):
			for player in range(nbPlayer):
				self.deck[player].append(self.stack[0])
				del self.stack[0]
		return self.deck

	def pioche(self,player):
		self.deck[player-1].append(self.stack[0])
		del self.stack[0]
		return

	def get_deck(self,player_number):
		return self.deck[player_number-1]

	def get_stack(self):
		return self.stack




if __name__ == '__main__':
	sess1 = cards(2,4)
	print(sess1.get_deck(1))
	print(sess1.get_deck(2))
	print(sess1.get_stack())
	sess1.pioche(1)
	print(sess1.get_deck(1))
```

### 12 Anagramme

- Vous allez modifier la fonction list_anagrams() pour pouvoir identifier tous les anagrammes du mot "ordre" ("ordre" étant inclus) parmi les mots de la liste data. Vous stockerez tous ces mots dans la nouvelle liste anagrams.

```
data = [ "ordre", "donner", "roder", "recevoir", "dorer", "plaisir", "aaaaa"]

def list_anagrams(data = data):
    anagrams = []
    origin_word = "ordre"
    
    # Insert your code here
    for element in data:
        if sorted(element) == sorted(origin_word):
            anagrams.append(element)

    # End of code insertion
    return anagrams
    
print(list_anagrams(data))
```


### 11 Palindrome

- Dans la liste proposée, filtrer les palindromes et les stocker dans la nouvelle liste palindromes.

```
liste = ["bonjour", "kayak", "salut", "ressasser", "toto", "python", "été", "algo"]

def is_palindrome(liste = liste):
    palindromes = []
    
    # Insert your code here
    for element in liste:
        if element == element[::-1]:
            palindromes.append(element)
    
    # End of code insertion
    
    return palindromes

print(is_palindrome(liste))
```

### 10 Shopping price

- Vous allez compléter la fonction calc_total() afin de calculer le montant total du panier shopping, sans prendre en compte les quantités.

- Refaites les mêmes opérations en prenant en compte la quantité.


- Si le total est supérieur à 60, mettez à jour la variable free_shipping_cost à True.

```
shopping = [
    { "product" : "Livre", "unit_price" : 10.99, "quantity" : 1},
    { "product" : "CD", "unit_price" : 15.99, "quantity" : 1},
    { "product" : "DVD", "unit_price" : 23, "quantity" : 3}
]

def calc_total(shopping = shopping):
    total = 0.
    free_shipping_cost = False

    # Insert your code here
    for product in shopping:
        total += product['unit_price']*product['quantity']
    if total > 60:
        free_shipping_cost=True

    # End of code insertion
    
    return total, free_shipping_cost

print(calc_total(shopping))
```

### 09 CONTACTS.py

- Mettez à jour la nouvelle liste "new_list" en conservant toutes les données de la liste "contacts" mais en transformant les numéros de téléphone au format international (+33612345678).


- Dans la fonction address_book, ajoutez une nouvelle clé "admin" pour chaque contact. Cette clé doit être à True pour john et à False pour les autres.


- Modifiez le tableau pour supprimer les doublons de numéros de téléphone. (L'entrée elisa doit etre supprimer du tableau)


```
contacts = [
   {
       "prenom" : 'john',
       "telephone" : '0611223344',
   },
   {
       "prenom" : 'elise',
       "telephone" : '0655667799'
   },
   {
       "prenom" : 'franck',
       "telephone" : '0612345678'
   },
   {
       "prenom" : 'elisa',
       "telephone" : '0612345678'
   }
]

def address_book(contacts = contacts):
    new_list = []
    
    # Insert your code here
    phone_list=[]
    for contact in contacts:
        
        contact['telephone']="+33"+contact['telephone'][1:]
        
        if contact['prenom'] == 'john':
            contact['admin']=True
        else:
            contact['admin']=False
        
        if contact['telephone'] not in phone_list:
            phone_list.append(contact['telephone'])
            new_list.append(contact)
    
    
    # End of code insertion
    
    return new_list

print(address_book(contacts))
```

### 08 Count Words

> En utilisant l'instruction split, écrivez l'instruction qui permet de découper cette citation de Jean Claude Van Damme en mots distincts.
Stockez le résultat généré par la méthode split dans une variable nommée text_split.
Stockez le nombre total de mots trouvés dans la variable count_word. 


> En utilisant les variables count_word (pour la condition d’arrêt de la boucle) et text_split (pour la condition dans la boucle), ajoutez un traitement pour ne compter que les mots de plus de 2 caractères.
Stockez le résultat obtenu dans la variable count_word_filtered.

```
def counting_words(text):
    count_word = 0
    count_word_filtered = 0
    
    # Insert your code here
    for word in text.split():
        if len(word) <= 2:
            count_word_filtered+=1
        else:
            count_word+=1
            

    return count_word, count_word_filtered
   
print(counting_words("Si tu travailles avec un marteau-piqueur pendant un tremblement de terre, désynchronise-toi, sinon tu travailles pour rien"))
print("Note : Le premier chiffre ci-dessus correspond à count_word et le second à count_word_filtered")
```

### 07 Les conditions et les opérateurs logiques

#### Initialisation

> Dans la fonction check_playlist(),  ajoutez une condition qui attribue à message la valeur suivante : "Attention, il vous reste moins de 4 titres" s'il y a un nombre de titres inférieur à 4.

```
playlist = [
    "Katy Perry - Chained To The Rhythm", 
    "Ed Sheeran - Shape of You", 
    "Maroon 5 - Don't Wanna Know"
]

def check_playlist(playlist = playlist):
    message = "Rien à signaler."

    # Insert your code here
    if len(playlist) < 4:
        message = "Attention, il vous reste moins de 4 titres"


    # End of code insertion

    return message

print(check_playlist(playlist))
```

#### Les conditions et les types

> Vous allez compléter la fonction check_data() afin de vérifier les informations et renvoyer un message selon certaines conditions.
En utilisant la variable email, définissez une condition qui ajoute "email validé" dans la variable messages si la longueur de email est supérieur à 10. 

> En utilisant la variable age, définissez une condition qui ajoute "accès bloqué" dans la variable messages si age est inférieur à 18. 

> En utilisant la variable basket, définissez une condition qui ajoute "commande validée" dans la variable messages si la longueur de basket est supérieur à 0. 

```
email = "john.doe@lacapsule.academy"
age = 32
basket = [
    {
        "article": "jean",
        "price": 32
    },
    {
        "article": "t-shirt",
        "price": 15
    },
    {
        "article": "pull",
        "price": 40
    }
]

def check_data(email = email, age = age, basket = basket):
    messages = []

    # Insert your code here
    if len(email) > 10:
        messages.append("email validé")
    if age < 18:
        messages.append("accès bloqué")
    if len(basket) > 0:
        messages.append("commande validée")


    # End of code insertion

    return messages

print(check_data(email, age, basket))
```

#### Les opérateurs logiques

> Vous allez compléter la fonction check_data() pour remplir dynamiquement la liste messages. En utilisant les variables email et basket, définissez une condition qui ajoute le message "panier validé" si la longueur de email est supérieur à 10 et si la longueur de basket est supérieur à 0. 


> En utilisant les variables email et basket, ajoutez une 2ème condition qui enregistre le message "problème de commande" si la longueur de email vaut 0 ou si la longueur de basket vaut 0. 


> En utilisant la variable age et l’opérateur not, ajoutez une 3ème condition qui ajoute le message "accès autorisé" si age n’est pas inférieur à 18. 

```
email = "john.doe@lacapsule.academy"
age = 32
basket = [
    {
        "article": "jean",
        "price": 32
    },
    {
        "article": "t-shirt",
        "price": 15
    },
    {
        "article": "pull",
        "price": 40
    }
]

def check_data(email = email, age = age, basket = basket):
    messages = []

    # Insert your code here
    if len(email) > 10 and len(basket) > 0:
        messages.append("panier validé")
    elif len(email)== 0 or len(basket) ==0:
        messages.append("problème de commande")
    
    if not age < 18:
        messages.append("accès autorisé")


    # End of code insertion

    return messages

print(check_data(email, age, basket))
```

#### Les conditions et les boucles

> Dans la fonction check_order(), créer une boucle pour passer sur tous les éléments de commandes.
> Cette boucle devra ajouter le message "commande nom_du_produit validée" dans la liste messages seulement si le stock est supérieur ou égal à la quantité commandée.

```
commandes = [
    {
        'produit': 'jean',
        'qty': 2,
        'stock': 5
    },
    {
        'produit': 't-shirt',
        'qty': 4,
        'stock': 3
    },
    {
        'produit': 'pull',
        'qty': 1,
        'stock': 10
    }   
]

def check_order(commandes = commandes):
    messages = []

    # Insert your code here
    for product in commandes:
        if product['stock'] >= product['qty']:
            messages.append('commande '+product['produit']+' validée')


    # End of code insertion

    return messages

print(check_order(commandes))
```

### 06 Les boucles

#### Initialisation

> Dans la fonction update_data(), créer une boucle qui va tourner 5 fois en utilisant pour compteur de boucle une variable nommée i initialisée à 0. 

> Cette boucle viendra ajouter 1 à la variable counter à chaque tour de boucle. 


> Complétez la boucle afin d’ajouter un message dans la liste messages à chaque tour de boucle. Chaque message prendra cette forme : "tour de boucle numéro 1", "tour de boucle numéro 2", etc.

> Penser à mettre un espace entre la dernière lettre de la chaîne de caractères et le guillemet. 


> Au début du programme une variable contact a été créée, elle contient une liste avec les valeurs suivantes : Vanessa, Hugo, Michael, Léo, John.

> Une boucle pourrait nous servir à parcourir l’intégralité des prénoms enregistrés dans cette liste.

> Modifier la boucle pour que chaque élément de la liste messages contienne également le prénom associé à l’index i. Chaque message prendra cette forme : "tour de boucle numéro 1 Vanessa", "tour de boucle numéro 2 Hugo", .etc


> Nous avons utilisé la variable i en tant qu’indice de la liste, ce qui nous a permis de lire la totalité de son contenu. En effet, elle est passée progressivement de 0 à 4 ce qui correspond aux indices de la liste contact. 

> Améliorons encore cette approche pour éviter d'avoir à modifier les conditions de la boucle à chaque fois que la liste évolue (c’est-à-dire si l’on y ajoute ou supprime des éléments).

> Essayez d’ajouter quelques prénoms dans contact puis remplacez le chiffre 5 de la condition de la boucle par l’instruction permettant de récupérer dynamiquement la longueur de la liste contact. 

```
counter = 0
messages = []
contact = ["Vanessa", "Hugo", "Michael", "Léo", "John", "test"]

def update_data(counter = counter, messages = messages, contact = contact):
    # Insert your code here
    for i in range(len(contact)):
        counter += 1
        messages.append("tour de boucle numéro "+str(counter)+" "+contact[i])



    # End of code insertion

    return counter, messages

print(update_data(counter, messages, contact))
```

#### Manipulation de données

> Au début du programme une variable factures a été créée, elle contient une liste avec des valeurs de factures différentes.

> Une boucle pourrait nous servir à additionner le total de toutes les factures enregistrées dans cette liste.

> Nous avons utilisé la longueur d’une liste pour créer une boucle pour que le code puisse passer sur chaque élément d’une liste.

> En utilisant cette approche, complétez la fonction calc_total() avec une boucle qui mettra à jour le compteur sur chaque tour de boucle (en utilisant une syntaxe raccourcie de type +=). La variable compteur servira pour stocker le résultat.

```
factures = [23, 45, 65, 12, 86]
compteur = 0

def calc_total(factures = factures, compteur = compteur):
    # Insert your code here
    for facture in factures:
        compteur += facture



    # End of code insertion

    return compteur

print(calc_total(factures, compteur))
```
#### Modification de texte

> Allons un peu plus loin! Nous avons vu que les boucles nous permettent de traiter et manipuler les données dans une liste. Revenons sur notre liste contact. Dans ce cas, les noms dans la liste ne sont pas formatés correctement - les majuscules et minuscules ne sont pas respectées. Complétez la fonction format_text() avec une boucle. Cette boucle modifiera chaque élément dans la liste pour afficher la première lettre en majuscule et le reste du mot en minuscule.

```
contact = ["john", "vanessa", "FRANCK"]

def format_text(contact):
    # Insert your code here
    contacts=[]
    for person in contact:
        contacts.append(person[0].upper()+person[1:].lower())
    contact = contacts


    # End of code insertion

    return contact

print(format_text(contact))
```

### 05 Les dictionnaires

#### Initialisation

> Complétez la fonction display_contact() pour stocker dans la variable contact le dictionnaire suivant :

- Une clé prenom avec pour valeur "Vanessa"
- Une clé phone avec pour valeur "0612345678"
- Une clé email avec pour valeur "vanessa@gmail.com"

> Attention à ne pas mettre de majuscule ni de caractère accentué dans le nom des clés.


```
def display_contact():
    contact = None

    # Insert your code here
    contact = {
        'prenom':'Vanessa',
        'phone':'0612345678',
        'email':'vanessa@gmail.com'
    }


    # End of code insertion

    return contact

print(display_contact())
```

#### Lire

> Complétez la fonction display_first_name() en modifiant la variable first_name à l’endroit indiqué afin d’afficher uniquement la valeur de la clé prenom.

```
contact = {
    "prenom": "Vanessa",
    "phone": "0612345678",
    "email": "vanessa@gmail.com"
}
    
def display_first_name(contact):
    first_name = None
    
    # Insert your code here

    first_name=contact['prenom']

    # End of code insertion

    return first_name

print(display_first_name(contact))
```

#### Modifier

> Complétez la fonction update_phone() à l’endroit indiqué afin de mettre à jour le numéro de téléphone contenu dans la variable contact avec la valeur suivante :  "0712345678".

```
contact = {
    "prenom": "Vanessa",
    "phone": "0612345678",
    "email": "vanessa@gmail.com"
}
    
def update_phone(contact):
    # Insert your code here

    contact['phone']='0712345678'

    # End of code insertion

    return contact

print(update_phone(contact))
```

#### Liste de dictionnaires

> Complétez la fonction add_john() afin d’ajouter un 3e utilisateur à la liste en utilisant la méthode append sur la liste users.

> Le contact aura une clé nom avec comme valeur "John", une clé phone avec comme valeur "0123456789" et une clé email avec comme valeur "john@gmail.com".

```
users = [
    {
        "nom": "Vanessa",
        "phone": "0544125478",
        "email": "vanessa@gmail.com"
    },
    {
        "nom": "Théo",
        "phone": "0544125478",
        "email": "theo@gmail.com"
    }
]

def add_john(users):
    # Insert your code here

    users.append({
        "nom":"John",
        "phone":"0123456789",
        "email":"john@gmail.com"
    })

    # End of code insertion

    return users

print(add_john(users))
```

### 04 Les listes et les tuples

#### Initialisation

- Créez une variable fruits qui va contenir une liste avec les valeurs suivantes : pommes, poires, bananes (attention à bien respecter l’ordre des fruits lors de la création de la liste).

- Afficher cette variable fruits avec print. 

```
fruits = ["pommes","poires","bananes"]
print(fruits)
```
#### Lire

> Nous avons déclaré une variable contact qui contient une liste avec les valeurs suivantes : Vanessa, Hugo, Michael.

> Créez la variable third_user contenant le troisième utilisateur de la liste contact en utilisant la méthode décrite ci-dessus puis affichez-la dans la console. 

```
contact = ["Vanessa", "Hugo", "Michael"]
third_user=contact[2]
print(third_user)
```

#### Modifier

> En suivant la méthode décrite ci-dessus, modifiez le 3ème prénom contenu dans la liste contact afin de mettre "Léo" à la place de "Michael" puis affichez la variable dans la console.

```
contact = ["Vanessa", "Hugo", "Michael"]

contact[2]="Léo"
```

#### Longueur

> Créez un variable count qui contiendra le nombre d'éléments de la liste contact. Affichez count dans la console. 

> À la suite de votre bloc de code, créez une variable last_one qui contiendra le dernier élément de la liste contact, quelle que soit sa longueur (donc sans écrire contact[2]).

> Il vous faudra utiliser deux notions précédemment abordées : l’accès aux éléments et le calcul de la longueur.

```
contact = ["Vanessa", "Hugo", "Michael"]

count = len(contact)

print(count)
last_one=contact[-1]
print(last_one)
```

#### Ajouter

> Nous retrouvons la liste contact avec 3 prénoms.

> Ajoutez "Jules" au début de la liste ainsi que "Marion" à la fin de la liste puis affichez-la.

```
contact = ["Vanessa", "Hugo", "Michael"]
contact.insert(0,"Jules")
contact.append("Marion")
print(contact)
```

#### Supprimer

> Nous disposons de la liste contact avec 4 prénoms.

> Supprimez la dernière valeur de la liste ainsi que la première valeur de la liste puis affichez-la dans la console. 

```
contact = ["Vanessa", "Hugo", "Michael", "Léo"]

contact.pop(-1)
contact.pop(0)
print(contact)
```

#### Les tuples

> Vous disposez de la variable contact qui représente un tuple avec les valeurs suivantes : Vanessa, Hugo, Michael, Léo.

> Enregistrez dans la variable second_user le 2ème élément du tuple puis affichez-la.

```
contact = ("Vanessa", "Hugo", "Michael", "Léo")

second_user=contact[1]

print(second_user)
```

### 03 Les booléens

> Initialisez une variable paiement avec la valeur True.

> Affichez cette variable avec print. 

> Supprimez le code précédent et initialisez une variable paiement avec la valeur False. 

> Affichez cette variable avec print. 

```
paiement = False
print(paiement)
```

### 02 - Les chaine de caratères

#### Initialisation

> Créer une variable nom en l’initialisant à la valeur "John".

> Attention à bien respecter les majuscules et minuscules de la chaîne de caractère !

> Affichez ensuite la valeur de la variable nom avec l’instruction print.

```
nom="John"
print(nom)
```

#### Concaténation

> Définissez une variable hello avec comme valeur "Bonjour comment ça va".

> À la ligne en dessous, définissez une deuxième variable nommée prenom avec comme valeur le prénom "John".

> Sur une nouvelle ligne, définissez une troisième variable phrase égale à la concaténation des variables hello et prenom.
Affichez la valeur de la variable phrase grâce à la commande print.


> Vous pouvez constater que le résultat peut être encore amélioré, le prénom "John" est collé directement au mot "va".

> Il devrait y avoir une espace entre les deux, mais Python n’en ajoutera pas automatiquement si on ne lui indique pas explicitement.

> Il suffira d’ajouter un espace à la fin de la chaîne de caractère "Bonjour comment ça va "

> L’espace sera placé entre la dernière lettre de la phrase et le guillemet. 

```
hello="Bonjour comment ça va "
prenom = "John"
phrase = hello + prenom

print(phrase)
```

#### Longueur

> Vous avez à disposition une variable exemple initialisée à la valeur "Je travaille en py".
> Créer une variable compteur qui aura pour valeur la longueur de la chaîne de caractères contenue dans la variable exemple.

> Affichez compteur dans la console.

```
exemple = "Je travaille en py"
compteur = len(exemple)

print(compteur)
```

#### Cibler un caractère

> Nous avons initialisé une variable job avec comme valeur "DevOps".
> Créez une variable shortened_job. En utilisant la concaténation du 1er et du 4ème caractère, assignez à cette variable la valeur "DO".

```
job = "DevOps"
shortened_job=job[0]+job[3]

print(shortened_job)
```

#### Découper

> À vous de jouer, définir une variable dessert initialisée avec la chaîne de caractères "corbeille de fruits frais"
> Créez une variable segment qui contiendra le segment "fruits" de la variable dessert.

> Affichez la variable segment dans la console.

```
dessert = "corbeille de fruits frais"
segment = dessert[13:19]

print(segment)
```

#### Transformer

> Nous avons initialisé une variable name avec la valeur "John Doe"
> Créez une variable maj qui aura pour valeur name en majsucules.

> Créez une variable min qui aura pour valeur name en minuscules.

> Affichez avec premier print affichez la valeur de maj.

> À la ligne suivante, affichez la valeur de min.

```
name = "John Doe"
maj=name.upper()
min=name.lower()
print(maj)
print(min)
```

### 01 - Les variables et les nombres

#### Initialisation

> Définissez une variable qui se nomme second et l’initialiser à 60, puis affichez le contenu de cette variable.

```
second = 60

print(second)
```

#### Assignation

>Définissez une variable qui se nomme minute et initialisez-la à 20.

> À la suite, modifiez la variable minute en lui donnant comme nouvelle valeur 30, puis affichez le contenu de cette variable. 

```
minute = 20

minute = 30

print(minute)
```

#### Opération

> Définissez une variable qui se nomme second et initialisez-la à 60.

> À la suite, définissez une autre variable minute initialisée à 30.

> Créez une dernière variable total qui sera égale à la multiplication de second et minute, puis affichez le contenu de cette variable. 

```
second = 60
minute = 30
total = second*minute
print(total)
```

#### Modifier

> Imaginons une variable nommée email qui corresponde au nombre d’e-mails reçus dans la journée. Initialisez cette variable à 23.

> À la suite, ajoutez 5 à la variable en utilisant la manière décrite ci-dessus, puis affichez le contenu de cette variable.

```
email =23

email += 5
```

#### Raccourci

> Imaginons une variable nommée score_game qui correspond au score d’un jeu vidéo. Initialisez cette variable à 420. 

> À la ligne en dessous, ajoutez 15 au score en utilisant un raccourci syntaxique, puis affichez le contenu de cette variable.

```
score_game = 420
score_game += 15
print(score_game)
```
