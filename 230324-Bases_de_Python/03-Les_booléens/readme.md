# Les booléens

> Initialisez une variable paiement avec la valeur True.

> Affichez cette variable avec print. 

> Supprimez le code précédent et initialisez une variable paiement avec la valeur False. 

> Affichez cette variable avec print. 

```
paiement = False
print(paiement)
```