import random

class cards():
	def __init__(self,nbPlayer,nbCarte):
		cards=[[number, color] for color in ['Piques','Carreaux','Cœurs','Trèfles'] for number in range(1,13)]

		self.stack=self.shuffle(cards)
		self.distribution(nbPlayer,nbCarte)
		

	def shuffle(self,cards):
		random.shuffle(cards)
		return cards
	
	def distribution(self,nbPlayer,nbCarte):
		self.deck=[['player '+str(i+1)] for i in range(nbPlayer)]
		for cards in range(nbCarte):
			for player in range(nbPlayer):
				self.deck[player].append(self.stack[0])
				del self.stack[0]
		return self.deck

	def pioche(self,player):
		self.deck[player-1].append(self.stack[0])
		del self.stack[0]
		return

	def get_deck(self,player_number):
		return self.deck[player_number-1]

	def get_stack(self):
		return self.stack




if __name__ == '__main__':
	sess1 = cards(2,4)
	print(sess1.get_deck(1))
	print(sess1.get_deck(2))
	print(sess1.get_stack())
	sess1.pioche(1)
	print(sess1.get_deck(1))