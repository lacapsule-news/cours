# FIND THE COMMAND

## MANIPULATION DE COMMANDES


- Touvez la commande permettant d’afficher le nom de l’utilisateur actif sur la machine et écrivez le résultat de la commande dans le fichier whoami.txt

`whoami > whoami.txt`

- Trouvez la commande permettant de lister l’ensemble des processus en cours et écrivez le résultat de la commande dans le fichier ps.txt

`ps -aux`

- Trouvez la commande permettant d’afficher la valeur d’espace disque disponible sur la machine et écrivez le résultat de la commande dans le fichier df.txt

`df > df.txt`

- Trouvez la commande permettant d’afficher les informations du réseau local de la machine et écrivez le résultat de la commande dans le fichier ifconfig.txt

`ip a > ifconfig.txt`

- Trouvez la commande permettant de récupérer l’adresse IP publique de la machine via l’URL ifconfig.me et écrivez le résultat de la commande dans le fichier ip.txt.

`curl ifconfig.me`