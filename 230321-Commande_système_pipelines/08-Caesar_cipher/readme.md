# CAESAR CIPHER

## DÉCHIFFREMENT D’UN FICHIER

Le chiffrement de César consiste à substituer une lettre par une autre un plus loin dans l'alphabet, c'est-à-dire qu'une lettre est toujours remplacée par la même lettre et que l'on applique le même décalage à toutes les lettres, cela rend très simple le décodage d'un message puisqu'il n’y a que 25 décalages possibles.
Exemple : ABC avec un décalage de 1 vers la droite donne BCD.
Récupérez la ressource "caesarcipher.zip" depuis l’onglet dédié sur Ariane.
Trouvez une méthode pour déchiffrer le contenu du fichier cipher.txt et stockez-le dans le fichier message.txt.
Note : le décalage est de 3 lettres, à vous de déterminer si le décalage est vers la droite (A -> D) ou vers la gauche (X <- A).

`cat ceasercipher/cipher.txt | tr '[A-Z]' '[D-ZA-C]'`