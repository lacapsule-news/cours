# PLAY WITH PIPELINES

## COMBINAISON DE COMMANDES

- Récupérez la ressource "playwithpipelines.zip" depuis l’onglet dédié sur Ariane.

`unzip playwithpipelines.zip`

- Filtrez les lignes du fichier log1.txt contenant le texte INFO via la commande grep.

`grep -e 'INFO' playwithpipelines/log1.txt`

- Affichez le contenu de tous les fichiers de logs via la commande cat.

`cat playwithpipelines/log*`

- Reprenez les commandes précédentes pour filtrer les lignes INFO sur l’ensemble des fichiers du dossier logs, en une seule commande grâce à une pipeline.

`grep -ne 'INFO' playwithpipelines/log*.txt`

## NOMBRE D'OCCURRENCES PAR FICHIER

- En vous servant de la mécanique des pipelines et de la commande xargs, trouvez le nombre d’apparitions du mot INFO pour chaque fichier.

`grep -nc 'INFO' playwithpipelines/log*.txt`