# COUNT OCCURRENCES

## NOMBRE TOTAL D’OCCURRENCES

- Récupérez la ressource "countoccurrences.zip" depuis l’onglet dédié sur Ariane.

`unzip countoccurrences.zip`

- En vous servant de la mécanique des pipelines, trouvez une méthode pour compter le nombre d’apparitions du mot INFO dans le fichier log1.txt.

`cat filesandfolders/log1.txt | grep -nc 'INFO'`

- Toujours en vous servant de la mécanique des pipelines, modifiez la commande précédente pour qu’elle retourne le nombre d’apparitions du mot INFO dans l’ensemble des fichiers de logs.

`cat filesandfolders/log?.txt | grep -nc 'INFO'`

## NOMBRE D'OCCURRENCES PAR FICHIER

- En vous servant de la mécanique des pipelines et de la commande xargs, trouvez le nombre d’apparitions du mot INFO pour chaque fichier.

`find filesandfolders/log* | xargs grep -nc 'INFO'`