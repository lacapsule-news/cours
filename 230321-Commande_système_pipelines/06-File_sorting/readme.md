# FILE SORTING

## TRI DES FICHIERS PAR POIDS

- Récupérez la ressource "filesorting.zip" depuis l’onglet dédié sur Ariane.

`unzip filesorting.zip`

- Construisez une commande permettant de trier les informations du fichier ls.txt par poids en bytes.

`sort -k5br filesorting/ls.txt`

- Stockez dans un fichier largest.txt les noms des 3 fichiers les plus lourds.

`sort -k5br filesorting/ls.txt | head -n 3 | awk '{print $9} > largest.txt`