# COMMANDES SYSTÈMES & PIPELINES

## Temps perso

### zsh

> Zsh est un terminal qui ma beaucoup était recomandé, j'ai commencé par modifié mon .zshrc

```
alias ls='ls -lrth --color=auto'
alias ll='ls -lrtha --color=auto'
alias mc='f(){ sudo ifconfig "$@" down; sudo macchanger -r "$@"; sudo ifconfig "$@" up; unset -f f; }; f'

PS1=$'%{\033[01;38;5;240m%}[%{\033[01;33m%}%n%{\033[01;38;5;240m%}@%{\033[01;32m%}%m %{\033[01;38;5;240m%}> %{\033[01;37m%}%c%{\033[01;38;5;240m%}] $ %{\033[00m%}'
```

Mon zsh est composé pour l'instant de 4 élément :
- 2 alias pous un ls agreable et un posibilité d'analyse
- un alias a macchanger qui permet de changer d'addresse mac sur un réseau(sans perdre la connexion via ssh ;))
- Un prompt custom avec des couleur

## Theorique

### Contexte

> Elles permettent d'intéragir avec le système d'exploitation, Permettant ainsi de le superviser et le monitorer.

> Identifier l'utilisateur actuellement actif sur le terminal.

```get_user_linux
whoami
```

> Afficher les processus en cours.

```
ps
```

> Terminer un processus.

```
kill [pid]
```

> Afficher la valeur d'éspace disque disponible des systèmes de fichier de l'utilisateur.

```
df -h
```

> Afficher l'historique des commandes de l'utilisateur

```
history
```

> Afficher ses infomation de cartes réseaux

```
ip a
```

#### Commandes avancées

> Requete cURL

```
curl ifconfig.me
```

> Execution simultanée

```
cat log1.txt && cat log2.txt
```

> Execution enchainé

```
cat logs.txt | wc -l
find log* | xargs grep -c 'WARNING'
```

## Pratique

### 08 CAESAR CIPHER

#### DÉCHIFFREMENT D’UN FICHIER

Le chiffrement de César consiste à substituer une lettre par une autre un plus loin dans l'alphabet, c'est-à-dire qu'une lettre est toujours remplacée par la même lettre et que l'on applique le même décalage à toutes les lettres, cela rend très simple le décodage d'un message puisqu'il n’y a que 25 décalages possibles.
Exemple : ABC avec un décalage de 1 vers la droite donne BCD.
Récupérez la ressource "caesarcipher.zip" depuis l’onglet dédié sur Ariane.
Trouvez une méthode pour déchiffrer le contenu du fichier cipher.txt et stockez-le dans le fichier message.txt.
Note : le décalage est de 3 lettres, à vous de déterminer si le décalage est vers la droite (A -> D) ou vers la gauche (X <- A).

`cat ceasercipher/cipher.txt | tr '[A-Z]' '[D-ZA-C]'`

### 07 MY ALIAS

#### SUPPRESSION DES ESPACES VIDES D’UN FICHIER

- Récupérez la ressource "myalias.zip" depuis l’onglet dédié sur Ariane.

`unzip myalias.zip`

- Trouvez une solution afin de supprimer la répétition d’espaces vides dans le fichier "ls.txt" via la commande tr.

`tr -s [:space:] < ls.txt`


#### CRÉATION D’UN ALIAS

- A partir de la commande alias, créez une nouvelle commande delspace dans votre terminal prenant comme paramètre le nom d’un fichier et permettant de supprimer les espaces répétés dans celui-ci.

`alias rmspace='f(){ tr -s [:space:] < "$@"; }; f'`

- Vous pouvez aller jusqu’à insérer cette instruction dans le fichier ".bashrc" à la racine de votre dossier utilisateur afin de l’utiliser tout le temps, même après avoir relancé le terminal.

```
$ rmspace ls.txt

total 40
-rw-r--r-- 1 tristangbn staff 0B 20 mai 16:15 anonymes.txt
-rw-rw-rw-@ 1 tristangbn staff 222B 17 mai 16:05 employes.txt
-rw-r--r--@ 1 tristangbn staff 286B 18 mai 12:18 index.html
drwxr-xr-x 8 tristangbn staff 256B 23 mai 18:24 logs
-rw-r--r-- 1 tristangbn staff 0B 24 mai 15:45 ls.txt
-rw-r--r--@ 1 tristangbn staff 10K 16 mai 15:15 quiz.zip
```

### 06 FILE SORTING

#### TRI DES FICHIERS PAR POIDS

- Récupérez la ressource "filesorting.zip" depuis l’onglet dédié sur Ariane.

`unzip filesorting.zip`

- Construisez une commande permettant de trier les informations du fichier ls.txt par poids en bytes.

`sort -k5hr filesorting/ls.txt`

- Stockez dans un fichier largest.txt les noms des 3 fichiers les plus lourds.

`sort -k5br filesorting/ls.txt | head -n 3 | awk '{print $9} > largest.txt`

### 05 FREE SPACE

#### TRAITEMENT DU RÉSULTAT D’UNE COMMANDE

- L’objectif de ce challenge est de construire une commande permettant d’afficher l’espace libre sur un ou plusieurs disques d’une machine.

- Construisez une commande permettant de connaître les informations du disque /dev/sda1 de la machine.

`df -h /dev/nvme0n1p2`

- Construisez une commande permettant de retourner l’espace disponible (en %) du disque /dev/sda1 de la machine et écrivez le résultat dans le fichier free.txt.

`df -h /dev/nvme0n1p2 | awk 'NR==2 {$SUM=100*$4/$2 ;print $SUM "%"}' > free.txt`



### 04 IP FINDER

#### TRAITEMENT DU RÉSULTAT D’UNE COMMANDE

- L’objectif de ce challenge est de construire une commande permettant d’afficher l’IP locale de votre machine à partir du terminal.
- Exécutez la commande ifconfig et observez la réponse obtenue.

```
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
```

- La réponse du ifconfig est assez longue et contient de nombreuses informations plus ou moins en lien avec ce qui nous intéresse.
- Ajoutez l’argument en0 à la commande ifconfig et observez la réponse obtenue.

`ip addr show lo`

- Cette fois-ci la réponse est plus légère et on voit que l’adresse IP que nous recherchons peut être trouvée à la ligne “inet”.
- En complétant la commande précédente et en utilisant la mécanique des pipelines, trouvez une solution pour filtrer la réponse précédente afin qu’elle n’affiche que le contenu de la ligne portant la mention “inet”.
- Trouvez une solution pour éliminer la tabulation inutile en début de ligne.
- Trouvez une solution pour extraire la deuxième colonne de texte afin de récupérer l’IP au format X.X.X.X et écrire le résultat dans le fichier ip.txt.

`ip addr show lo | grep inet | awk '{print $2}'`

### 03 COUNT OCCURRENCES

#### NOMBRE TOTAL D’OCCURRENCES

- Récupérez la ressource "countoccurrences.zip" depuis l’onglet dédié sur Ariane.

`unzip countoccurrences.zip`

- En vous servant de la mécanique des pipelines, trouvez une méthode pour compter le nombre d’apparitions du mot INFO dans le fichier log1.txt.

`cat filesandfolders/log1.txt | grep -nc 'INFO'`

- Toujours en vous servant de la mécanique des pipelines, modifiez la commande précédente pour qu’elle retourne le nombre d’apparitions du mot INFO dans l’ensemble des fichiers de logs.

`cat filesandfolders/log?.txt | grep -nc 'INFO'`

#### NOMBRE D'OCCURRENCES PAR FICHIER

- En vous servant de la mécanique des pipelines et de la commande xargs, trouvez le nombre d’apparitions du mot INFO pour chaque fichier.

`find filesandfolders/log* | xargs grep -nc 'INFO'`

### 02 PLAY WITH PIPELINES

#### COMBINAISON DE COMMANDES

- Récupérez la ressource "playwithpipelines.zip" depuis l’onglet dédié sur Ariane.

`unzip playwithpipelines.zip`

- Filtrez les lignes du fichier log1.txt contenant le texte INFO via la commande grep.

`grep -e 'INFO' playwithpipelines/log1.txt`

- Affichez le contenu de tous les fichiers de logs via la commande cat.

`cat playwithpipelines/log*`

- Reprenez les commandes précédentes pour filtrer les lignes INFO sur l’ensemble des fichiers du dossier logs, en une seule commande grâce à une pipeline.

`grep -ne 'INFO' playwithpipelines/log*.txt`

#### NOMBRE D'OCCURRENCES PAR FICHIER

- En vous servant de la mécanique des pipelines et de la commande xargs, trouvez le nombre d’apparitions du mot INFO pour chaque fichier.

`grep -nc 'INFO' playwithpipelines/log*.txt`

### 01 - FIND THE COMMAND

#### MANIPULATION DE COMMANDES


- Touvez la commande permettant d’afficher le nom de l’utilisateur actif sur la machine et écrivez le résultat de la commande dans le fichier whoami.txt

`whoami > whoami.txt`

- Trouvez la commande permettant de lister l’ensemble des processus en cours et écrivez le résultat de la commande dans le fichier ps.txt

`ps -aux`

- Trouvez la commande permettant d’afficher la valeur d’espace disque disponible sur la machine et écrivez le résultat de la commande dans le fichier df.txt

`df > df.txt`

- Trouvez la commande permettant d’afficher les informations du réseau local de la machine et écrivez le résultat de la commande dans le fichier ifconfig.txt

`ip a > ifconfig.txt`

- Trouvez la commande permettant de récupérer l’adresse IP publique de la machine via l’URL ifconfig.me et écrivez le résultat de la commande dans le fichier ip.txt.

`curl ifconfig.me`