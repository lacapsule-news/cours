# MY ALIAS

## SUPPRESSION DES ESPACES VIDES D’UN FICHIER

- Récupérez la ressource "myalias.zip" depuis l’onglet dédié sur Ariane.

`unzip myalias.zip`

- Trouvez une solution afin de supprimer la répétition d’espaces vides dans le fichier "ls.txt" via la commande tr.

`tr -s [:space:] < ls.txt`


## CRÉATION D’UN ALIAS

- A partir de la commande alias, créez une nouvelle commande delspace dans votre terminal prenant comme paramètre le nom d’un fichier et permettant de supprimer les espaces répétés dans celui-ci.

`alias rmspace='f(){ tr -s [:space:] < "$@"; }; f'`

- Vous pouvez aller jusqu’à insérer cette instruction dans le fichier ".bashrc" à la racine de votre dossier utilisateur afin de l’utiliser tout le temps, même après avoir relancé le terminal.

```
$ rmspace ls.txt

total 40
-rw-r--r-- 1 tristangbn staff 0B 20 mai 16:15 anonymes.txt
-rw-rw-rw-@ 1 tristangbn staff 222B 17 mai 16:05 employes.txt
-rw-r--r--@ 1 tristangbn staff 286B 18 mai 12:18 index.html
drwxr-xr-x 8 tristangbn staff 256B 23 mai 18:24 logs
-rw-r--r-- 1 tristangbn staff 0B 24 mai 15:45 ls.txt
-rw-r--r--@ 1 tristangbn staff 10K 16 mai 15:15 quiz.zip
```