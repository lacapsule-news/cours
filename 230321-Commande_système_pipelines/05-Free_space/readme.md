# FREE SPACE

## TRAITEMENT DU RÉSULTAT D’UNE COMMANDE

- L’objectif de ce challenge est de construire une commande permettant d’afficher l’espace libre sur un ou plusieurs disques d’une machine.

- Construisez une commande permettant de connaître les informations du disque /dev/sda1 de la machine.

`df -h /dev/nvme0n1p2`

- Construisez une commande permettant de retourner l’espace disponible (en %) du disque /dev/sda1 de la machine et écrivez le résultat dans le fichier free.txt.

`df -h /dev/nvme0n1p2 | awk 'NR==2 {$SUM=100*$4/$2 ;print $SUM "%"}' > free.txt`

