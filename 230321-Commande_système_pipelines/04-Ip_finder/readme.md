# IP FINDER

## TRAITEMENT DU RÉSULTAT D’UNE COMMANDE

- L’objectif de ce challenge est de construire une commande permettant d’afficher l’IP locale de votre machine à partir du terminal.
- Exécutez la commande ifconfig et observez la réponse obtenue.

```
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
```

- La réponse du ifconfig est assez longue et contient de nombreuses informations plus ou moins en lien avec ce qui nous intéresse.
- Ajoutez l’argument en0 à la commande ifconfig et observez la réponse obtenue.

`ip addr show lo`

- Cette fois-ci la réponse est plus légère et on voit que l’adresse IP que nous recherchons peut être trouvée à la ligne “inet”.
- En complétant la commande précédente et en utilisant la mécanique des pipelines, trouvez une solution pour filtrer la réponse précédente afin qu’elle n’affiche que le contenu de la ligne portant la mention “inet”.
- Trouvez une solution pour éliminer la tabulation inutile en début de ligne.
- Trouvez une solution pour extraire la deuxième colonne de texte afin de récupérer l’IP au format X.X.X.X et écrire le résultat dans le fichier ip.txt.

`ip addr show lo | grep inet | awk '{print $2}'`