# Awesome pipelines

## AWESOME PIPELINE

Bienvenue à votre second hackathon dédié à la mise en place d’une pipeline de CI/CD complète pour un projet d’application web 🔥

Vous travaillerez chacun sur votre machine, mais vous avancerez en groupe et au même rythme afin d’avoir le même résultat à la fin de la journée.


👉 Commencez par récupérer le dépôt Git cypress-realworld-app représentant un exemple d’application web dotée d’un frontend et d’un backend JavaScript.


👉 Démarrez le projet en suivant les instructions spécifiées sur la page GitHub et testez "manuellement" l’application en vous créant un compte.

## GITLAB FLOW

👉 Dans le dépôt local, retirez le lien avec le dépôt distant et hébergez le projet sur votre propre dépôt gitlab privé.


👉 En suivant la documentation de GitLab, mettez en place un GitLab flow en créant les branches nécessaires et en paramétrant le projet.


## CYPRESS

👉 Faites le tri dans les tests Cypress en ne gardant que ceux qui correspondent à l’API (backend) puis supprimez le fichier ".gitlab-ci.yml".


👉 Créez une pipeline d’intégration continue pour que les tests Cypress s’exécutent automatiquement.

La mise en place des tests doit se faire en accord avec le GitLab flow puisqu’ils ne se déclencheront pas pour toutes les branches.


⚠️ Pendant toute la durée du hackathon, vous devez veiller à optimiser l’exécution des runners afin d’éviter de dépenser inutilement tout le crédit (en minutes) accordé par GitLab.


## TESTS DE MONTÉE EN CHARGE

👉 En utilisant l’outil Ddosify (comparable à Locust), mettez en place des tests de montée en charge cohérents dans une pipeline CI/CD.

La mise en place des tests doit se faire en accord avec le GitLab flow puisqu’ils ne se déclencheront pas pour toutes les branches.

## DÉPLOIEMENT CONTINU

Place à la dernière partie du hackathon avec l’automatisation de la mise en production des deux parties de l’application : le frontend et le backend.


👉 Commencez par créer une pipeline chargée d’automatiser le déploiement du backend développé en NodeJS en passant par la plateforme Northflank.


👉 Une fois le backend déployé, modifiez temporairement la configuration des tests Cypress pour qu’ils s’exécutent sur le backend en production plutôt que celui en local afin de vérifier que le déploiement s’est bien déroulé


👉 Automatisez le déploiement du frontend développé en JavaScript via la librairie React en passant par la plateforme Vercel.


# Ce que j'ai fait

## Downloads

- Le lien étant cassé mais les explication claire, je suis aller chercher cypress-realworld-app sur google et j'ai cloner ce repo:

`git clone https://github.com/cypress-io/cypress-realworld-app`

- J'ai ensuite crée un projet gitlab et changer les origin du projet pour faire tourner la pipeline une premiere fois.

```
709  cd cypress-realworld-app/
710  git remote remove origin 
711  git remote add origin git@gitlab.com:5unburst/cypress-hackathon.git
```

- J'ai changer la branch pour main afin de mettre en place git flow a partir de celci 

```
714  git branch -m main
716  git push origin main
```

### Pourquoi utilisé les tache parallels

Lorsque plusieur runner sont disponible comme sur gitlab certaine application vont être capable d'utiliser ces parallels pour lancé des test différents et réduire le temps de tests.

- J'ai supprimer le --record de cypress car je n'ai pas de compte

`npx cypress run --record --key ffbf0b6b-1e9f-46a2-81e4-2e6261a3ac75 --parallel --browser firefox --group "UI - Firefox - Mobile" --spec "cypress/tests/ui/*" --config "viewportWidth=375,viewportHeight=667"`

- J'ai ajouter ddosify pour avoir un préavis de ce qu'il donne en ci

`Failed`

- Je m'attaque au branche pour ne pas consommer trop de runner git lab
    - pour commencer j'ai activer pipeline musts succeed for merge
    - main (production,protected)
    - dev (pre-preprod,protected,default)
    - create a new branch to add code and merge it
![pipeline must suceed image](pipelinemustsuceed.png)
![branch protected](branch.protected.png)

Le pipeline a étais optimisé afin d'éxecuté les test d'api lors du merge request et seulement les test de qualité de code pendant le dev

- Je n'avais plus de temps de runner pour finir les test ddosify
- J'ai donc ajouter mon rpi
- J'ai modifié la pipelie pour les architecture arm64
- J'ai réussi a faire passer les tests

- J'ai eu des problemes de connexions
![Northflank payment](NorthFlank.loginissue.png)
- Pour la suite des exo les framwork étais payant
![Northflank payment](Northflank.payment.png)
![fly ask cb](fly.payment.png)
- J'ai trouver le projet open source [cap cover](https://caprover.com/)
- Je pense a modifier mon infrastructure en rajoutant du docker pour bénéficier de ce projet et plein d'autres

