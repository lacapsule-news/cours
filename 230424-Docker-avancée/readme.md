# Docker avancée

## 03 - POPULAR CMS

### DÉPLOIEMENT DE LA BASE DE DONNÉES

Ce challenge est un peu particulier puisqu’il vous permettra de vous préparer au hackathon de demain en déployant un environnement complet pour le fameux CMS WordPress couplé à une base de données et un back-office.

👉 Créez un fichier "docker-compose.wp.yml" qui contiendra tous les services que vous allez créer dans les étapes suivantes.

👉 Au sein de ce fichier Docker Compose, créez un premier service nommé "mariadb" dédié à la base de données du CMS et qui devra respecter les contraintes suivantes :


Le service sera basé sur l’image Docker officielle du moteur de base de données MariaDB
Un volume nommé "mariadb" sera créé et utilisé afin de rendre persistantes les données
Une base de données nommée "wordpress" sera créée par défaut
Un utilisateur par défaut nommé "admin" avec le mot de passe "changeme123" sera créé par défaut
Le conteneur lié au service sera systématiquement redémarré (lors d’un crash ou du démarrage de la machine hôte)
L’ensemble des variables d’environnement utilisées pour le service devront être définies dans un fichier nommé ".wp.env"

👉 Créez un second service nommé "adminer" dédié à la visualisation de la base de données MariaDB (basée sur MySQL) et qui devra respecter les contraintes suivantes :


Le service sera basé sur l’image Docker officielle de Adminer
Le port par défaut d’Adminer sera exposé afin d’être joignable à partir du port 8686 sur la machine hôte
Le conteneur lié au service ne devra démarrer que lorsque que le service "mariadb" sera prêt
Le conteneur lié au service sera systématiquement redémarré sauf s’il a été manuellement arrêté auparavant

👉 Démarrez les services "mariadb" et "adminer" et visitez l’URL localhost:8686 depuis un navigateur internet sur la machine hôte afin de vous connecter à la base de données MariaDB et vérifier que le service a correctement été configuré.

### DÉPLOIEMENT DU CMS

👉 Créez un dernier service nommé "wordpress" dédié au CMS du même nom et qui devra respecter les contraintes suivantes :


Le service sera basé sur l’image Docker officielle du CMS WordPress
Le port par défaut de Wordpress sera exposé afin d’être joignable à partir du port 8585 sur la machine hôte
Un volume nommé "wordpress" sera créé et utilisé afin de rendre persistantes les données
Le service devra évidemment être configuré via des variables d’environnement pour communiquer avec la base de données précédemment déployée
Le conteneur lié au service ne devra démarrer que lorsque que le service "mariadb" sera prêt
Le conteneur lié au service sera systématiquement redémarré (lors d’un crash ou du démarrage de la machine hôte)
L’ensemble des variables d’environnement utilisées pour le service devront être définies dans un fichier nommé ".wp.env"

👉 Démarrez tous les services et visitez l’URL localhost:8585 depuis un navigateur internet sur la machine hôte afin de configurer le CMS WordPress et publier un article afin de vérifier que tout est bien configuré.


👉 Connectez-vous de nouveau à l’interface d’Adminer pour modifier directement le titre de l’article précédemment créé en base de données.


👉 Arrêtez l’ensemble des services via la commande docker-compose stop et démarrez-les de nouveau afin de vérifier que les données sont bien persistantes.

`docker-compose --env-file .wp.env -f docker-compose.wp.yml up`

```docker-compose.wp.yml
version: '3.3'

services:
  wordpress:
    image: wordpress:latest
    depends_on:
      - mariadb
    networks:
      - front_wp
    ports:
       - 8585:80
    restart: always
    volumes:
       - wordpress:/var/www/html
    environment:
      WORDPRESS_DB_HOST: mariadb:3306
      WORDPRESS_DB_USER: ${DB_USER}
      WORDPRESS_DB_PASSWORD: ${DB_PASSWORD}
      WORDPRESS_DB_NAME: ${ENV_DB}

  mariadb:
    image: mariadb:latest
    volumes:
      - mariadb:/var/lib/mysql
    environment:
      - MARIADB_ROOT_PASSWORD=${DB_ROOT_PASS}
      - MARIADB_DATABASE=${ENV_DB}
      - MARIADB_USER=${DB_USER}
      - MARIADB_PASSWORD=${DB_PASSWORD}
    networks:
      - db_mon
      - front_wp
    restart: always

  adminer:
    image: adminer:latest
    depends_on:
      - mariadb
    environment:
      - ADMINER_DEFAULT_SERVER=mariadb
    networks:
      - db_mon
    ports:
      - 8686:8080
    restart: always

volumes:
  mariadb:
  wordpress:

networks:
  front_wp:
  db_mon:
```

```.wp.env
ENV_DB=wordpress
DB_USER=admin
DB_PASSWORD=changeme123
DB_ROOT_PASS=superstrongpassword
```


## 02 - Advenced docker compose

### LE FICHIER DOCKER-COMPOSE.YML

Vous allez commencer tranquillement la découverte des fonctionnalités avancées de Docker Compose par un détour sur le fameux fichier YAML responsable de la configuration des services.

👉 Créez un fichier nommé "docker-compose.prod.yml" contenant un service "hello" basé sur la célèbre image hello-world.

👉 Trouvez un moyen de démarrer ce service via la commande docker-compose up.

### L’INSTRUCTION "RESTART"

👉 Créez une image Docker nommée "errorimg" à partir du Dockerfile ci-dessous.

```
FROM debian:latest
CMD exit 1
```

Vous constaterez que cette image est un peu particulière, car elle se contente d’exécuter la commande "exit 1". L’idée est de simuler un conteneur qui crash subitement.

👉 Créez un fichier Docker Compose doté d’un service basé sur l’image créée précédemment et démarrez-le.

👉 Exécutez la commande docker-compose ps afin de constater que le conteneur est en statut "Exited".

👉 Trouvez une instruction à ajouter dans le fichier Docker Compose capable de redémarrer automatiquement le conteneur lorsqu’il est arrêté suite à une erreur / un crash.

👉 Démarrez le service et exécutez la commande docker-compose ps afin de constater que le conteneur se redémarre automatiquement en boucle.

Sachez qu’il existe d’autres instructions de redémarrage, notamment "always" qui est capable de redémarrer le service en cas d’erreur, mais également au démarrage du service Docker (ou de la machine hôte).

### L’INSTRUCTION "DEPENDS_ON"

👉 Créez une image Docker nommée "what-time-is-it" à partir du Dockerfile ci-dessous.

```
FROM debian:latest
CMD date
```

Vous constatez que cette image se contente d’exécuter la commande date.

👉 Créez un fichier Docker Compose doté de deux services distincts nommés "service-alpha" et "service-beta" basés sur l’image créée précédemment.

👉Démarrez les deux services en mode attaché (sans l’option -d) afin de constater que chacun affiche la date du jour, à la seconde près.

Si vous démarrez plusieurs fois de suite les services, vous constaterez qu’ils ne démarrent pas toujours dans le même ordre, car techniquement, ils sont démarrés quasiment en même temps.

👉 Trouvez une instruction à ajouter dans le fichier Docker Compose capable de démarrer le service "service-alpha" uniquement lorsque le service "service-beta" a été démarré.

```
version: '3.3'

services:
  services-beta:
    image: what-time-is-it

  services-alpha:
    image: what-time-is-it
    depends_on:
      - services-beta
```

`docker-compose -f docker-compose.prod.yml up -d`


## 01 - ENVIRONMENT VARIABLES WITH DOCKER

### LES VARIABLES D’ENVIRONNEMENT

Vous avez pu brièvement vous confronter au principe d’environnement précédemment dans la formation, notamment dans le challenge "Database deployment".

```
version: "3.9"


services:

  database:

    image: "postgres:latest"

    ports:

      - "5432:5432"

    volumes:

      - ./db_data:/var/lib/postgresql/data

    environment:

      POSTGRES_PASSWORD: acknowledge_me
```

Si vous analysez ce fichier Docker Compose, vous pouvez voir qu’une variable d’environnement est spécifiée au lancement du conteneur : "POSTGRES_PASSWORD" qui aura pour valeur "acknowledge_me".

👉 Afin de découvrir le fonctionnement des variables d’environnement, démarrez le service "database" décrit dans le fichier Docker Compose ci-dessus et exécutez un terminal bash à l’intérieur du conteneur.



👉 Exécutez la commande env afin de constater que la variable d’environnement précisée dans le fichier Docker Compose apparaît. Cette dernière sera exploitée par le service de PostgreSQL.

### AVEC DOCKER

👉 Créez un fichier Dockerfile afin de construire une image nommée "mymails" basée sur debian (dans sa dernière version).

Cette image se contentera de récupérer une variable d’environnement nommée "EMAIL" qui aura "admin@test.com" comme valeur par défaut afin de l’afficher via la commande echo.

👉 Buildez l’image "mymails" associée à ce Dockerfile afin de pouvoir l’utiliser dans un fichier Docker Compose.

👉 Créez un nouveau fichier Docker Compose contenant un service "mymails" basé sur l’image précédemment buildée.

👉 Démarrez le service "mymails" en mode attaché (sans l’option -d) afin de vérifier si l’email précisé dans le Dockerfile est bien affiché.

### AVEC DOCKER COMPOSE

👉 Trouvez un moyen d’écraser la variable d’environnement "EMAIL" directement à partir du fichier Docker Compose et démarrez le service afin de vérifier si l’email a bien été changé.

👉 Grâce à la documentation, trouvez un moyen de définir une variable d’environnement dans le fichier Docker Compose, mais en précisant sa valeur dans un fichier à part nommé ".mymails.env", dans le même dossier.

👉 Démarrez tous les services en spécifiant le fichier d’environnement ".mymails.env" afin de vérifier que l’email soit bien affiché.

```Dockerfile
FROM debian:stable

ENV EMAIL=admin@test.com

ENTRYPOINT echo "$EMAIL is the mail"
```

```docker-compose.yml
version: "3.3"

services:
  mydebian:
    image: "mydeb:latest"
    environment:
      EMAIL: ${EMAIL}
```

```.env
EMAIL="protected.env@mail.test"
```