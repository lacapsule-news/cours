# ENVIRONMENT VARIABLES WITH DOCKER

## LES VARIABLES D’ENVIRONNEMENT

Vous avez pu brièvement vous confronter au principe d’environnement précédemment dans la formation, notamment dans le challenge "Database deployment".

```
version: "3.9"


services:

  database:

    image: "postgres:latest"

    ports:

      - "5432:5432"

    volumes:

      - ./db_data:/var/lib/postgresql/data

    environment:

      POSTGRES_PASSWORD: acknowledge_me
```

Si vous analysez ce fichier Docker Compose, vous pouvez voir qu’une variable d’environnement est spécifiée au lancement du conteneur : "POSTGRES_PASSWORD" qui aura pour valeur "acknowledge_me".

👉 Afin de découvrir le fonctionnement des variables d’environnement, démarrez le service "database" décrit dans le fichier Docker Compose ci-dessus et exécutez un terminal bash à l’intérieur du conteneur.



👉 Exécutez la commande env afin de constater que la variable d’environnement précisée dans le fichier Docker Compose apparaît. Cette dernière sera exploitée par le service de PostgreSQL.

## AVEC DOCKER

👉 Créez un fichier Dockerfile afin de construire une image nommée "mymails" basée sur debian (dans sa dernière version).

Cette image se contentera de récupérer une variable d’environnement nommée "EMAIL" qui aura "admin@test.com" comme valeur par défaut afin de l’afficher via la commande echo.

👉 Buildez l’image "mymails" associée à ce Dockerfile afin de pouvoir l’utiliser dans un fichier Docker Compose.

👉 Créez un nouveau fichier Docker Compose contenant un service "mymails" basé sur l’image précédemment buildée.

👉 Démarrez le service "mymails" en mode attaché (sans l’option -d) afin de vérifier si l’email précisé dans le Dockerfile est bien affiché.

## AVEC DOCKER COMPOSE

👉 Trouvez un moyen d’écraser la variable d’environnement "EMAIL" directement à partir du fichier Docker Compose et démarrez le service afin de vérifier si l’email a bien été changé.

👉 Grâce à la documentation, trouvez un moyen de définir une variable d’environnement dans le fichier Docker Compose, mais en précisant sa valeur dans un fichier à part nommé ".mymails.env", dans le même dossier.

👉 Démarrez tous les services en spécifiant le fichier d’environnement ".mymails.env" afin de vérifier que l’email soit bien affiché.

```Dockerfile
FROM debian:stable

ENV EMAIL=admin@test.com

ENTRYPOINT echo "$EMAIL is the mail"
```

```docker-compose.yml
version: "3.3"

services:
  mydebian:
    image: "mydeb:latest"
    environment:
      EMAIL: ${EMAIL}
```

```.env
EMAIL="protected.env@mail.test"
```