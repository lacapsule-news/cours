# Jira meets gitlab

## SETUP

👉 Créez un token API pour Jira sur Atlassian Cloud à l’aide de la documentation suivante : https://docs.gitlab.com/ee/integration/jira/jira_cloud_configuration.html

👉 Créez un nouveau groupe sur GitLab que vous nommerez jira-meets-gitlab. Créez un nouveau projet/repository dans ce groupe, avec un fichier README.

👉 Créez un projet de "Développement Logiciel" sur Jira au format Kanban.

👉 Dans le menu en haut de page, cliquez sur "Personnes" > "Create team". Créez votre équipe et invitez votre buddy.

## INTÉGRATION DE GITLAB SUR JIRA

Il est possible d’intégrer facilement Jira avec des milliers d’apps, plugins et extensions.

👉 Installez l’application GitLab.com for Jira Cloud.

👉 Assurez-vous d’être connecté à GitLab puis sélectionnez le namespace GitLab sur lequel vous souhaitez travailler via Jira.

## LIER UN COMMIT SUR GITLAB À UN TICKET JIRA

C’est en mentionnant les clés de tickets Jira dans vos noms de branches, messages de commit et titres de merge request que vous verrez vos dépôts liés à Jira automatiquement.

👉 Créez un ticket de type "Story" sur Jira avec le contenu suivant : "As a user, I want to integrate GitLab and Jira together"

👉 Sur GitLab, modifiez le fichier README.md en ajoutant une ligne grâce à l’IDE en ligne.

`# I am in charge of this user story.`

👉 Pour enregistrer vos changements et les voir transposés sur Jira, créez un commit dont le message contient la clé du ticket Jira. Le commit apparaît sur le ticket Jira.

## MERGE REQUEST ET CHANGEMENT DE STATUT DU TICKET

👉 Ajoutez une modification sur le fichier "README.md", faites un nouveau commit et demandez une merge request.
Le message du commit précisera que vous souhaitez passer le ticket dans la colonne "done" automatiquement :

Message de commit :

`CléTicketJira now #done`