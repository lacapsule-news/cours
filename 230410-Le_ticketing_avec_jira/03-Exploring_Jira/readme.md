# Exploring jira

## CRÉATION D’UN PROJET KANBAN

Jira est un outil de gestion de projet puissant qui, couplé à d’autres applications telles que GitLab en font un incontournable du métier de DevOps. Cet outil porte bien son nom : Jira est le diminutif de Gojira, le nom japonais de Godzilla.

👉 Créez un compte Atlassian et accédez à la version cloud de Jira.

👉 Créez un projet Kanban sur le modèle "Développement Logiciel" et sélectionnez le type "Company Managed".

👉 Une fois le projet créé, modifiez l’icône de votre projet dans les paramètres.

👉 Par défaut, Jira a créé un tableau kanban avec 4 colonnes :

- Backlog
- Sélectionné pour le développement
- En cours
- Terminé

## AJOUT DE TICKETS

👉 Votre board kanban est vide : créez deux premiers tickets de type "Story" et nommez-les de la façon suivante :

As a user, I want to upload pictures taken with my phone camera
As a user, I want to send private messages to other users

👉 Créez un troisième ticket de type "Bug" :

Fix loading screen

👉 Vos trois tickets sont placés par défaut dans le backlog. Déplacez la première user story dans la colonne "Sélectionné pour le développement".  


## JIRA QUERY LANGUAGE (JQL)

Jira a créé son propre langage de requêtes afin de filtrer les tickets. Vous remarquerez quelques similitudes avec des requêtes de bases de données comme PostgreSQL.

👉 Pour pratiquer le Jira Query Language, créez un nouveau projet Kanban en accédant à vos tableaux sur l’URL au format suivant : https://<votre_url>.atlassian.net/jira/boards

Seule cette URL /jira/boards vous donne accès à la création d’un tableau kanban avec des données d’exemple.


👉 Cliquez sur "Créer un tableau Kanban avec des données d’exemple (sample data)"


👉 Dans l’onglet "Tickets", manipulez les filtres et affichez la requête associée.

Vous l’aurez compris, inutile d’apprendre le langage de requête JQL pour débuter. L’interface graphique vous permettra de faire des requêtes simples avec une grande facilité.


👉 Utilisez les filtres pour afficher tous les tickets résolus créés dans les deux dernières semaines. Affichez la requête au format JQL.

👉 Enfin, affichez tous les tickets ayant la priorité la plus élevée.