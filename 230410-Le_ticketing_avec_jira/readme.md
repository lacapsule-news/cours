# Le ticketing avce Jira

## Contexte

Un projet IT à besoin d'être maintenu.
De nombreuses demandes, appelées tickets, peuvent être faites de la part des différentes équipes de l'entreprise.

Les ticket convergent vers les équipes techniques.
Ils vont devoir être validés et priorisés.

Pour facilité l'oganisation et le suivi des tickets, les équipes passent par des outils spécialisés dans le domaine de la tech.

Un des plus connu dans le domaine : Jira

### Fonctionnement

Les utilisateur vont pouvoir déclarer une demande directement depuis la plateforme de ticket.

Le chef de projet est chargé de l'organisation des tickets, il va pouvoir valider chaque ticket et définir une priorité en prenant en compte la criticité de celui-ci.

Chaque ticket est rattaché à sont créateur qui sera automatiquement informé des évolutions de celui-ci.

Il est également possible de mettre en place des automatisations.

On peut par exemple, lier les tickets à un dépôt Gitlab:
Lorsqu'une merge request liée à un ticket se ferme, le ticket sera automatiquement résolu.

## Exo

### 06 Jira scripting

#### CONNEXION À L’API REST DE JIRA

Il est temps d’aller un peu plus loin avec Jira en mobilisant vos skills en scripting.
Vous l’avez sans doute remarqué, beaucoup de choses sont possibles avec Jira Automation, mais il sera souvent nécessaire de construire des scripts "maison" pour répondre à des besoins précis.

Pour ce challenge, appuyez-vous sur la documentation de l’API Jira sur Atlassian Cloud :

https://developer.atlassian.com/cloud/jira/platform/rest/v3/intro/

👉 Commencez par créer un token API sur Atlassian Cloud.

👉 Construisez une chaîne de caractères en concaténant votre adresse email et le token, séparés par ":".

mymail@gmail.com:API_TOKEN

👉 Encodez cette chaîne de caractères en base64. Ce sera votre clé d’accès à l’API que vous passerez dans le header "Authorization" de chacune de vos requêtes.


#### AFFICHER DES DONNÉES

👉Utilisez curl pour récupérer tous vos projets Jira.

L’API vous renvoie un objet JSON peu lisible. Utilisez l’utilitaire jq pour n’afficher que les informations suivantes :

- id
- key
- name
- lead.displayName

👉 Dumpez ces informations dans un fichier "projects.csv".

```
curl --request GET \
  --url 'https://fuworld.atlassian.net/rest/api/3/project/recent' \
  --user 'user:token' \
  --header 'Accept: application/json' | jq '.[] | .id,.key,.name'
```

#### RÉCUPÉRER DES TICKETS AVEC JQL

👉 Créez un filtre de recherche de tickets sur l’interface de Jira. Ce filtre n’affichera que les tickets ayant pour statut "Done". Récupérez l’ID de ce filtre.

https://VOTRE_URL.atlassian.net/rest/api/3/search?jql=filter=VOTRE_FILTRE

👉 Créez un script bash qui requête l’API toutes les 10 secondes et affiche ces tickets sous forme de notifications sur votre bureau.

Pour cela, vous pouvez installer l’outil notify-send.

```
curl --request GET \
  --url 'https://fuworld.atlassian.net/rest/api/3/search?jql=filter=VOTRE_FILTRE' \
  --user '5unburst@proton.me:ATATT3xFfGF00vQ0laUEfpyrNOksJjqsa7J6cr1sFs4L58l02xn77B3jUg8sGT0Fd2Nc0Y5KXcGYlwPTribPRBt9ETbfrU8aJRSLa-QsbeM6uF63W-eodBZk2LaHbFOmdomgVITFrvp0ubh61ozezq-l-lLHxMTxr9MXNlU3ZF9lxPxbrblOyiI=42F824E3' \
  --header 'Accept: application/json'
```

### 05 JIRA AUTOMATION

#### SETUP

Explorez la documentation de Jira pour prendre en main l’outil d’automatisation no-code : https://www.atlassian.com/fr/software/jira/guides/expand-jira/automation 

Chaque automatisation repose sur les trois étapes suivantes :

- Déclencheur de l’action que l’on veut automatiser
- Condition
- Action à effectuer

👉 Créez un projet sur GitLab et liez-le à un tableau kanban sur Jira. Invitez votre buddy sur ces deux projets (vous créerez chacun un projet sur GitLab et Jira).

👉 Ajoutez un ticket et associez un commit depuis l’IDE de GitLab en utilisant la clé du ticket Jira dans le message du commit.

#### DÉCLENCHEUR ET CONDITIONS

Les fonctionnalités d’automatisation de Jira sont fondamentalement basées sur des règles "if-then" déclenchées en réponse à différents événements dans Jira.

👉 Ajoutez une automatisation pour attribuer à votre buddy tous les nouveaux tickets de type "Tâches" dont la priorité est haute.

👉 Dans la même règle d’automatisation, attribuez-vous tous les tickets de type "Bug".

👉 Créez deux tickets correspondants à ces conditions et vérifiez que l’automatisation fonctionne.

#### SMART VALUES

Les valeurs intelligentes sont des variables qui vous permettent d’ajouter plus de complexité à vos règles d’automatisation. Vous pouvez les utiliser pour récupérer des valeurs en temps réel dans vos champs et déclencher des règles.
Par exemple, la valeur intelligente {{now.plusDays (5)}} fait référence à l’heure actuelle et y ajoute 5 jours, tandis que {{issue.summary}} imprimera le résumé du problème.

👉 En utilisant Jira Automation, faites en sorte de recevoir un email à chaque merge request.
Utilisez des smart values pour inclure l’URL, la clé du ticket et la date du jour dans le corps du mail.

👉 Testez cette automatisation en demandant à votre buddy de créer une merge request depuis l’IDE sur GitLab.

#### DEVOPS AUTOMATION

👉 Créez un ticket de type Task (tâche) avec pour titre : "Build frontend".

👉 Créez une automatisation qui passera automatiquement ce ticket au statut "In Progress" quand une branche est créée et comprend la clé du ticket.

👉 De la même manière, pour vérifier cette automatisation, demandez à votre buddy de créer une branche sur GitLab dont le nom contient la clé du ticket.

#### AUDIT LOG

Le journal d’audit conserve une trace de toutes vos actions déclenchées par vos règles d’automatisation (audit logs).

Pour chaque ticket créé, en utilisant les smart variables, personnalisez vos logs en incluant :

- La clé du ticket
- Le résumé du ticket ("summary")
- Le nom du projet associé

### 04 - Jira meets gitlab

#### SETUP

👉 Créez un token API pour Jira sur Atlassian Cloud à l’aide de la documentation suivante : https://docs.gitlab.com/ee/integration/jira/jira_cloud_configuration.html

👉 Créez un nouveau groupe sur GitLab que vous nommerez jira-meets-gitlab. Créez un nouveau projet/repository dans ce groupe, avec un fichier README.

👉 Créez un projet de "Développement Logiciel" sur Jira au format Kanban.

👉 Dans le menu en haut de page, cliquez sur "Personnes" > "Create team". Créez votre équipe et invitez votre buddy.

#### INTÉGRATION DE GITLAB SUR JIRA

Il est possible d’intégrer facilement Jira avec des milliers d’apps, plugins et extensions.

👉 Installez l’application GitLab.com for Jira Cloud.

👉 Assurez-vous d’être connecté à GitLab puis sélectionnez le namespace GitLab sur lequel vous souhaitez travailler via Jira.

#### LIER UN COMMIT SUR GITLAB À UN TICKET JIRA

C’est en mentionnant les clés de tickets Jira dans vos noms de branches, messages de commit et titres de merge request que vous verrez vos dépôts liés à Jira automatiquement.

👉 Créez un ticket de type "Story" sur Jira avec le contenu suivant : "As a user, I want to integrate GitLab and Jira together"

👉 Sur GitLab, modifiez le fichier README.md en ajoutant une ligne grâce à l’IDE en ligne.

`# I am in charge of this user story.`

👉 Pour enregistrer vos changements et les voir transposés sur Jira, créez un commit dont le message contient la clé du ticket Jira. Le commit apparaît sur le ticket Jira.

#### MERGE REQUEST ET CHANGEMENT DE STATUT DU TICKET

👉 Ajoutez une modification sur le fichier "README.md", faites un nouveau commit et demandez une merge request.
Le message du commit précisera que vous souhaitez passer le ticket dans la colonne "done" automatiquement :

Message de commit :

`CléTicketJira now #done`

### 03 - Exploring jira

#### CRÉATION D’UN PROJET KANBAN

Jira est un outil de gestion de projet puissant qui, couplé à d’autres applications telles que GitLab en font un incontournable du métier de DevOps. Cet outil porte bien son nom : Jira est le diminutif de Gojira, le nom japonais de Godzilla.

👉 Créez un compte Atlassian et accédez à la version cloud de Jira.

👉 Créez un projet Kanban sur le modèle "Développement Logiciel" et sélectionnez le type "Company Managed".

👉 Une fois le projet créé, modifiez l’icône de votre projet dans les paramètres.

👉 Par défaut, Jira a créé un tableau kanban avec 4 colonnes :

- Backlog
- Sélectionné pour le développement
- En cours
- Terminé

#### AJOUT DE TICKETS

👉 Votre board kanban est vide : créez deux premiers tickets de type "Story" et nommez-les de la façon suivante :

As a user, I want to upload pictures taken with my phone camera
As a user, I want to send private messages to other users

👉 Créez un troisième ticket de type "Bug" :

Fix loading screen

👉 Vos trois tickets sont placés par défaut dans le backlog. Déplacez la première user story dans la colonne "Sélectionné pour le développement".  


#### JIRA QUERY LANGUAGE (JQL)

Jira a créé son propre langage de requêtes afin de filtrer les tickets. Vous remarquerez quelques similitudes avec des requêtes de bases de données comme PostgreSQL.

👉 Pour pratiquer le Jira Query Language, créez un nouveau projet Kanban en accédant à vos tableaux sur l’URL au format suivant : https://<votre_url>.atlassian.net/jira/boards

Seule cette URL /jira/boards vous donne accès à la création d’un tableau kanban avec des données d’exemple.


👉 Cliquez sur "Créer un tableau Kanban avec des données d’exemple (sample data)"


👉 Dans l’onglet "Tickets", manipulez les filtres et affichez la requête associée.

Vous l’aurez compris, inutile d’apprendre le langage de requête JQL pour débuter. L’interface graphique vous permettra de faire des requêtes simples avec une grande facilité.


👉 Utilisez les filtres pour afficher tous les tickets résolus créés dans les deux dernières semaines. Affichez la requête au format JQL.

👉 Enfin, affichez tous les tickets ayant la priorité la plus élevée.

### 02 - Gitlab ticketing

#### CRÉATION D’UN MILESTONE

GitLab propose une solution de gestion de projet et de ticketing intégrée qui permet de suivre votre projet et attribuer des tâches aux membres de votre équipe : https://docs.gitlab.com/ee/user/project/milestones/ 

👉 Créez un projet sur GitLab et invitez votre buddy.

Les milestones permettent d’englober dans une tâche commune un ensemble de tickets, et de préciser une deadline.

👉 Créez un milestone appelé "User authentication" avec une deadline pour le vendredi de la semaine prochaine.


#### CRÉATION DE TICKETS

👉 Créez deux tickets sur ce milestone.

👉 Créez un label "dev" et appliquez ce label aux deux tickets créés.

👉 Assignez ces deux tickets à votre buddy.


#### SERVICE DESK

GitLab propose également un Service Desk par email qui s’occupe de créer automatiquement un ticket associé.

👉 Mettez-vous dans la peau d’un utilisateur du projet de votre buddy et signalez-lui par email un souci sur la connexion à son application.

👉 Ajoutez le ticket généré au milestone "User authentication" et attribuez-le à votre buddy.

👉 Votre buddy doit résoudre ce ticket via l’ajout d’un fichier auth.py et une merge request.