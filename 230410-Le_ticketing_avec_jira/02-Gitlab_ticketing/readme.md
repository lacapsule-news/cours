# Gitlab ticketing

## CRÉATION D’UN MILESTONE

GitLab propose une solution de gestion de projet et de ticketing intégrée qui permet de suivre votre projet et attribuer des tâches aux membres de votre équipe : https://docs.gitlab.com/ee/user/project/milestones/ 

👉 Créez un projet sur GitLab et invitez votre buddy.

Les milestones permettent d’englober dans une tâche commune un ensemble de tickets, et de préciser une deadline.

👉 Créez un milestone appelé "User authentication" avec une deadline pour le vendredi de la semaine prochaine.


## CRÉATION DE TICKETS

👉 Créez deux tickets sur ce milestone.

👉 Créez un label "dev" et appliquez ce label aux deux tickets créés.

👉 Assignez ces deux tickets à votre buddy.


## SERVICE DESK

GitLab propose également un Service Desk par email qui s’occupe de créer automatiquement un ticket associé.

👉 Mettez-vous dans la peau d’un utilisateur du projet de votre buddy et signalez-lui par email un souci sur la connexion à son application.

👉 Ajoutez le ticket généré au milestone "User authentication" et attribuez-le à votre buddy.

👉 Votre buddy doit résoudre ce ticket via l’ajout d’un fichier auth.py et une merge request.