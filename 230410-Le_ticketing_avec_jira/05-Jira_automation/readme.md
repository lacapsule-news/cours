# JIRA AUTOMATION

## SETUP

Explorez la documentation de Jira pour prendre en main l’outil d’automatisation no-code : https://www.atlassian.com/fr/software/jira/guides/expand-jira/automation 

Chaque automatisation repose sur les trois étapes suivantes :

- Déclencheur de l’action que l’on veut automatiser
- Condition
- Action à effectuer

👉 Créez un projet sur GitLab et liez-le à un tableau kanban sur Jira. Invitez votre buddy sur ces deux projets (vous créerez chacun un projet sur GitLab et Jira).

👉 Ajoutez un ticket et associez un commit depuis l’IDE de GitLab en utilisant la clé du ticket Jira dans le message du commit.

## DÉCLENCHEUR ET CONDITIONS

Les fonctionnalités d’automatisation de Jira sont fondamentalement basées sur des règles "if-then" déclenchées en réponse à différents événements dans Jira.

👉 Ajoutez une automatisation pour attribuer à votre buddy tous les nouveaux tickets de type "Tâches" dont la priorité est haute.

👉 Dans la même règle d’automatisation, attribuez-vous tous les tickets de type "Bug".

👉 Créez deux tickets correspondants à ces conditions et vérifiez que l’automatisation fonctionne.

## SMART VALUES

Les valeurs intelligentes sont des variables qui vous permettent d’ajouter plus de complexité à vos règles d’automatisation. Vous pouvez les utiliser pour récupérer des valeurs en temps réel dans vos champs et déclencher des règles.
Par exemple, la valeur intelligente {{now.plusDays (5)}} fait référence à l’heure actuelle et y ajoute 5 jours, tandis que {{issue.summary}} imprimera le résumé du problème.

👉 En utilisant Jira Automation, faites en sorte de recevoir un email à chaque merge request.
Utilisez des smart values pour inclure l’URL, la clé du ticket et la date du jour dans le corps du mail.

👉 Testez cette automatisation en demandant à votre buddy de créer une merge request depuis l’IDE sur GitLab.

## DEVOPS AUTOMATION

👉 Créez un ticket de type Task (tâche) avec pour titre : "Build frontend".

👉 Créez une automatisation qui passera automatiquement ce ticket au statut "In Progress" quand une branche est créée et comprend la clé du ticket.

👉 De la même manière, pour vérifier cette automatisation, demandez à votre buddy de créer une branche sur GitLab dont le nom contient la clé du ticket.

## AUDIT LOG

Le journal d’audit conserve une trace de toutes vos actions déclenchées par vos règles d’automatisation (audit logs).

Pour chaque ticket créé, en utilisant les smart variables, personnalisez vos logs en incluant :

- La clé du ticket
- Le résumé du ticket ("summary")
- Le nom du projet associé