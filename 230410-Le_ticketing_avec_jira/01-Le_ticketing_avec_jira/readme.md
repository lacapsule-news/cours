# Le ticketing avce Jira

## Contexte

Un projet IT à besoin d'être maintenu.
De nombreuses demandes, appelées tickets, peuvent être faites de la part des différentes équipes de l'entreprise.

Les ticket convergent vers les équipes techniques.
Ils vont devoir être validés et priorisés.

Pour facilité l'oganisation et le suivi des tickets, les équipes passent par des outils spécialisés dans le domaine de la tech.

Un des plus connu dans le domaine : Jira

## Fonctionnement

Les utilisateur vont pouvoir déclarer une demande directement depuis la plateforme de ticket.

Le chef de projet est chargé de l'organisation des tickets, il va pouvoir valider chaque ticket et définir une priorité en prenant en compte la criticité de celui-ci.

Chaque ticket est rattaché à sont créateur qui sera automatiquement informé des évolutions de celui-ci.

Il est également possible de mettre en place des automatisations.

On peut par exemple, lier les tickets à un dépôt Gitlab:
Lorsqu'une merge request liée à un ticket se ferme, le ticket sera automatiquement résolu.