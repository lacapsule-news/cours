# Jira scripting

## CONNEXION À L’API REST DE JIRA

Il est temps d’aller un peu plus loin avec Jira en mobilisant vos skills en scripting.
Vous l’avez sans doute remarqué, beaucoup de choses sont possibles avec Jira Automation, mais il sera souvent nécessaire de construire des scripts "maison" pour répondre à des besoins précis.

Pour ce challenge, appuyez-vous sur la documentation de l’API Jira sur Atlassian Cloud :

https://developer.atlassian.com/cloud/jira/platform/rest/v3/intro/

👉 Commencez par créer un token API sur Atlassian Cloud.

👉 Construisez une chaîne de caractères en concaténant votre adresse email et le token, séparés par ":".

mymail@gmail.com:API_TOKEN

👉 Encodez cette chaîne de caractères en base64. Ce sera votre clé d’accès à l’API que vous passerez dans le header "Authorization" de chacune de vos requêtes.


## AFFICHER DES DONNÉES

👉Utilisez curl pour récupérer tous vos projets Jira.

L’API vous renvoie un objet JSON peu lisible. Utilisez l’utilitaire jq pour n’afficher que les informations suivantes :

- id
- key
- name
- lead.displayName

👉 Dumpez ces informations dans un fichier "projects.csv".

```
curl --request GET \
  --url 'https://fuworld.atlassian.net/rest/api/3/project/recent' \
  --user 'user:token' \
  --header 'Accept: application/json' | jq '.[] | .id,.key,.name'
```

## RÉCUPÉRER DES TICKETS AVEC JQL

👉 Créez un filtre de recherche de tickets sur l’interface de Jira. Ce filtre n’affichera que les tickets ayant pour statut "Done". Récupérez l’ID de ce filtre.

https://VOTRE_URL.atlassian.net/rest/api/3/search?jql=filter=VOTRE_FILTRE

👉 Créez un script bash qui requête l’API toutes les 10 secondes et affiche ces tickets sous forme de notifications sur votre bureau.

Pour cela, vous pouvez installer l’outil notify-send.

```
curl --request GET \
  --url 'https://fuworld.atlassian.net/rest/api/3/search?jql=filter=VOTRE_FILTRE' \
  --user '5unburst@proton.me:ATATT3xFfGF00vQ0laUEfpyrNOksJjqsa7J6cr1sFs4L58l02xn77B3jUg8sGT0Fd2Nc0Y5KXcGYlwPTribPRBt9ETbfrU8aJRSLa-QsbeM6uF63W-eodBZk2LaHbFOmdomgVITFrvp0ubh61ozezq-l-lLHxMTxr9MXNlU3ZF9lxPxbrblOyiI=42F824E3' \
  --header 'Accept: application/json'
```