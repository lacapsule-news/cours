# Ansible

Cette exercice est sauvgarder [ici](https://gitea.sunburst.ml/sunburst/lacapsule-ansible)

## Contexte

Lorsque les serveurs sont deployés automatiquements, ils sont dans une configuration standrad.

La configuration manuelle des serveurs peut vite être fastidieuse.

Grâce a un modele, les serveurs pourront être configurés facilement.

### Fonctionnement

Ansible est une solution d'Infrastructure As Code (IaC) qui permet de définir des modèles de configuration système.

L'inventaire est la liste des addresse IP des serveurs de l'infrastructure.

Les modèle est définie dans un playbook qui détaille précisement les taches à executer sur chaque serveur.

Création d'unutilisateurs, installation d'un package, modification des droits...

Via la CLI Ansible, les tâches décrites dans le playbook seront automatiquement executées via une connexion SSH.