# Ansible

Cette exercice est sauvgarder [ici](https://gitea.sunburst.ml/sunburst/lacapsule-ansible)

## Contexte

Lorsque les serveurs sont deployés automatiquements, ils sont dans une configuration standrad.

La configuration manuelle des serveurs peut vite être fastidieuse.

Grâce a un modele, les serveurs pourront être configurés facilement.

### Fonctionnement

Ansible est une solution d'Infrastructure As Code (IaC) qui permet de définir des modèles de configuration système.

L'inventaire est la liste des addresse IP des serveurs de l'infrastructure.

Les modèle est définie dans un playbook qui détaille précisement les taches à executer sur chaque serveur.

Création d'unutilisateurs, installation d'un package, modification des droits...

Via la CLI Ansible, les tâches décrites dans le playbook seront automatiquement executées via une connexion SSH.

## Exo

### 04 - Variables in the vault

#### LA NOTION DE VARIABLES

Ansible est capable de gérer des variables afin de stocker des valeurs pouvant être utilisées dans les playbooks en remplaçant la variable par sa valeur lorsqu’une tâche est exécutée.

Ces variables peuvent être définies directement dans des fichiers YAML situés dans deux répertoires nommés "host_vars" pour les variables d’un hôte en particulier et "group_vars" pour les variables d’un groupe d’hôtes.

👉 En reprenant le répertoire de travail du challenge précédent, créer un fichier de variables pour le groupe d’hôtes "linode" qui contiendra les variables suivantes.

N’hésitez pas à parcourir la documentation d’Ansible pour comprendre l’importance du nom du fichier de variables.

```
linux_username: paige
linux_password: il0v3AEW
```

👉 Modifiez le playbook du challenge précédent afin que la tâche chargée de créer un nouvel utilisateur Linux utilise les variables "linux_username" et "linux_password".

👉 Démarrez votre playbook puis vérifiez-le en vous connectant en SSH aux 2 serveurs via l’utilisateur "paige" et la clé privée "test-ansible".

Si vous arrivez à vous connecter à ce nouvel utilisateur, c’est que vos variables sont fonctionnelles, mais pas pour autant sécurisées…

```
.
├── playbook.yml
├── inventory.ini
└── group_vars/
    └── SSHonlan.yml
```

#### ANSIBLE VAULT

Ansible Vault permet de chiffrer et gérer des données sensibles telles que des mots de passe qui peuvent être stockés dans des fichiers de variables, par exemple.

👉 Trouvez un moyen de chiffrer le contenu du fichier de variables créé précédemment via la commande ansible vault spécifiant le mot de passe de votre choix.

Une fois le fichier chiffré, vous n’êtes pas censé directement l’ouvrir avec un éditeur de texte, mais vous pouvez passer par ces deux commandes afin de le visionner ou l’éditer.

```
ansible-vault create SSHonlan.yml
ansible-vault view VARIABLE_FILE
ansible-vault edit VARIABLE-FILE
```

👉 Démarrez votre playbook puis vérifiez-le en vous connectant en SSH aux 2 serveurs via l’utilisateur "paige" et la clé privée "test-ansible".

`ansible-playbook playbook.yml -i host.yml -u root --private-key test-ansible --ask-vault-pass`

### 03 - I came to play

#### CRÉATION D’UN PLAYBOOK

Un playbook Ansible est un modèle d’automatisation au format YAML utilisé pour configurer un ou plusieurs serveurs à l’aide de tâches définissant les opérations qu’Ansible va mener.

👉 Sur Linode, créez une VM vierge nommée "test-ansible2" qui aura les mêmes caractéristiques et viendra compléter la VM "test-ansible".

👉 Ajoutez cette nouvelle VM à votre inventaire "linode" et vérifiez qu’elle réponde bien à un ping via Ansible.

👉 Plutôt que de passer par une commande Ansible, créez un playbook dans le fichier "simple-playbook.yml" chargé de ping tous les hôtes de l’inventaire.

👉 Démarrez le playbook grâce à la commande suivante.

`ansible-playbook simple-playbook.yml -u root --private-key PATH`

Si la partie "RECAP" des tâches vous affiche "ok" pour les deux serveurs, c’est que tout est opérationnel pour vous attaquer à un playbook plus ambitieux !


```
raspberrypi:~/first_inventory $ ansible all -i host.yml -m ping -u root --private-key test-ansible
192.168.0.98 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false,
    "ping": "pong"
}
192.168.0.89 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false,
    "ping": "pong"
}
```

#### PLAYBOOK AVANCÉ

👉 Créez une nouvelle paire de clés SSH nommée "test-ansible" qui sera uniquement dédiée à la connexion aux utilisateurs courants (autre que root) créés via Ansible


👉 En vous inspirant d’exemples trouvés en ligne, créez un nouveau playbook dans le fichier "advanced-playbook.yml" qui devra effectuer les tâches suivantes :

Création d’un nouvel utilisateur "nginx" avec le mot de passe "ihateapache"
Ajout de l’utilisateur "nginx" au groupe sudo (en conservant ses autres groupes par défaut)
Modification du terminal par défaut (bash) pour l’utilisateur "nginx"
Désactivation de la possibilité de se connecter avec un mot de passe via SSH (afin de privilégier l’authentification par paire de clé, car plus sécurisée)
Configuration de la connexion à l’utilisateur "nginx" via SSH et la paire de clés générée précédemment
Installation du package Nginx

N’hésitez pas à lancer plusieurs fois le playbook afin de tester vos tâches : Ansible est plutôt bien fait, car il est capable d’exécuter plusieurs fois la même tâche sans pour autant provoquer des erreurs.

👉 Vérifiez votre playbook en vous connectant en SSH aux 2 serveurs via l’utilisateur "nginx" et la clé privée "test-ansible", puis en requêtant le serveur en HTTP.


```
---
- name: Create users with additional parameters
  hosts: SSHonlan
  become: true
  tasks:
    - name: Add users
      user:
        name: nginx
        password: "$6$UNQZp.H84jrsDb0/$JOc7k/82AkZNdbysmw2cUfuDXUBEj0ptBtqyVHSUXlfxr6B.HV4a83y/cXY/YeWe/ZJunGOzot8xmupody2zO/"
        shell: /bin/bash
        groups: sudo
        append: yes

    - name: Copy SSH key file
      authorized_key:
        user: nginx
        key: "{{ lookup('file', './nginx-ansible.pub') }}"


- name: Désactivation de l'authentification par mot de passe via SSH
  hosts: SSHonlan
  become: true
  tasks:
    - name: SSH No password
      lineinfile:
        path: /etc/ssh/sshd_config
        regexp: '^#?PasswordAuthentication'
        line: 'PasswordAuthentication no'
        backup: yes
    - name: SSH allow pubkey
      lineinfile:
        path: /etc/ssh/sshd_config
        regexp: '^#?PubkeyAuthentication'
        line: 'PubkeyAuthentication yes'
        backup: yes
    - name: SSH RSAAuthentication yes
      lineinfile:
        path: /etc/ssh/sshd_config
        regexp: '^#?RSAAuthentication'
        line: 'RSAAuthentication yes'
        backup: yes


- name: Configuration de la connexion utilisateur "nginx" via SSH avec une paire de clés
  hosts: SSHonlan
  become: true
  tasks:
    - name: Créer le répertoire .ssh pour l'utilisateur nginx
      file:
        path: /home/nginx/.ssh
        state: directory
        owner: nginx
        group: nginx

    - name: Définir les permissions du répertoire .ssh et du fichier authorized_keys
      file:
        path: /home/nginx/.ssh/*
        state: directory
        owner: nginx
        group: nginx
        mode: '0644'
      notify: Restart SSH Service

  handlers:
    - name: Restart SSH Service
      systemd:
        name: sshd
        state: restarted

- name: Installation du package sudo
  hosts: SSHonlan
  become: true
  tasks:
    - name: Installer sudo
      package:
        name: sudo
        state: present

- name: Installation du package Nginx
  hosts: SSHonlan
  become: true
  tasks:
    - name: Installer Nginx
      package:
        name: nginx
        state: present
```

### 02 Start with ansible

#### INSTALLATION D’ANSIBLE

👉 Installez Ansible via le gestionnaire de package Python pip.

Nul besoin de passer par un environnement virtuel, vous allez directement l’installer sur votre machine hôte.

La commande ansible n’étant pas encore reconnue, il est nécessaire de faire comprendre à votre terminal où trouver l’exécutable.


👉 Ajoutez la ligne ci-dessous à la fin du fichier `~/.bashrc` de votre machine hôte.

`export PATH="$PATH:/home/engineer/.local/bin"`

👉 Appliquez les modifications effectuées dans le fichier de configuration du terminal en exécutant la commande ci-dessous.

`source ~/.bashrc`

👉 Enfin, vérifiez l’installation d’Ansible en récupérant sa version.

`ansible --version`

#### INVENTAIRE ANSIBLE

Un inventaire Ansible est une liste d’hôtes sous forme d’adresses IP permettant de simplifier l’exécution des tâches pour un groupe de serveurs.

👉 Sur Linode, créez une VM vierge nommée "test-ansible" qui sera votre terrain de jeu pour découvrir Ansible :

```
Debian 11
Dedicated 4 GB (Dedicated CPU)
```

👉 Sur votre machine hôte, créez un inventaire Ansible nommé "linode" en ajoutant l’adresse IP du serveur créé précédemment dans le fichier de configuration dédié.

👉 Vérifiez que l’hôte a bien été ajouté dans votre inventaire en exécutant la commande suivante.

`ansible all --list-hosts`

#### EXÉCUTION D’ANSIBLE

Pour exécuter les différentes tâches, Ansible communique avec les hôtes en se connectant via SSH.

👉 Vérifiez que vous pouvez vous connecter manuellement au serveur créé dans la partie précédente via SSH.

👉 Exécutez un ping sur l’ensemble des machines de l’inventaire (une seule pour le moment) en prenant soin de préciser le chemin vers la clé SSH privée permettant de se connecter au serveur.

```
[SSHonlan]
192.168.0.98
192.168.0.89
```

`ansible all -m ping -u root --private-key PATH`

Si le serveur répond "pong", c’est que tout est bien configuré et que vous êtes prêt à utiliser Ansible 🚀

👉 Ne supprimez pas la VM Linode, elle sera utilisée pour les prochains challenges.