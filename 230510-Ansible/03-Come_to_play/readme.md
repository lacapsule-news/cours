# I came to play

## CRÉATION D’UN PLAYBOOK

Un playbook Ansible est un modèle d’automatisation au format YAML utilisé pour configurer un ou plusieurs serveurs à l’aide de tâches définissant les opérations qu’Ansible va mener.

👉 Sur Linode, créez une VM vierge nommée "test-ansible2" qui aura les mêmes caractéristiques et viendra compléter la VM "test-ansible".

👉 Ajoutez cette nouvelle VM à votre inventaire "linode" et vérifiez qu’elle réponde bien à un ping via Ansible.

👉 Plutôt que de passer par une commande Ansible, créez un playbook dans le fichier "simple-playbook.yml" chargé de ping tous les hôtes de l’inventaire.

👉 Démarrez le playbook grâce à la commande suivante.

`ansible-playbook simple-playbook.yml -u root --private-key PATH`

Si la partie "RECAP" des tâches vous affiche "ok" pour les deux serveurs, c’est que tout est opérationnel pour vous attaquer à un playbook plus ambitieux !


```
raspberrypi:~/first_inventory $ ansible all -i host.yml -m ping -u root --private-key test-ansible
192.168.0.98 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false,
    "ping": "pong"
}
192.168.0.89 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false,
    "ping": "pong"
}
```

## PLAYBOOK AVANCÉ

👉 Créez une nouvelle paire de clés SSH nommée "test-ansible" qui sera uniquement dédiée à la connexion aux utilisateurs courants (autre que root) créés via Ansible


👉 En vous inspirant d’exemples trouvés en ligne, créez un nouveau playbook dans le fichier "advanced-playbook.yml" qui devra effectuer les tâches suivantes :

Création d’un nouvel utilisateur "nginx" avec le mot de passe "ihateapache"
Ajout de l’utilisateur "nginx" au groupe sudo (en conservant ses autres groupes par défaut)
Modification du terminal par défaut (bash) pour l’utilisateur "nginx"
Désactivation de la possibilité de se connecter avec un mot de passe via SSH (afin de privilégier l’authentification par paire de clé, car plus sécurisée)
Configuration de la connexion à l’utilisateur "nginx" via SSH et la paire de clés générée précédemment
Installation du package Nginx

N’hésitez pas à lancer plusieurs fois le playbook afin de tester vos tâches : Ansible est plutôt bien fait, car il est capable d’exécuter plusieurs fois la même tâche sans pour autant provoquer des erreurs.

👉 Vérifiez votre playbook en vous connectant en SSH aux 2 serveurs via l’utilisateur "nginx" et la clé privée "test-ansible", puis en requêtant le serveur en HTTP.


```
---
- name: Create users with additional parameters
  hosts: SSHonlan
  become: true
  tasks:
    - name: Add users
      user:
        name: nginx
        password: "$6$UNQZp.H84jrsDb0/$JOc7k/82AkZNdbysmw2cUfuDXUBEj0ptBtqyVHSUXlfxr6B.HV4a83y/cXY/YeWe/ZJunGOzot8xmupody2zO/"
        shell: /bin/bash
        groups: sudo
        append: yes

    - name: Copy SSH key file
      authorized_key:
        user: nginx
        key: "{{ lookup('file', './nginx-ansible.pub') }}"


- name: Désactivation de l'authentification par mot de passe via SSH
  hosts: SSHonlan
  become: true
  tasks:
    - name: SSH No password
      lineinfile:
        path: /etc/ssh/sshd_config
        regexp: '^#?PasswordAuthentication'
        line: 'PasswordAuthentication no'
        backup: yes
    - name: SSH allow pubkey
      lineinfile:
        path: /etc/ssh/sshd_config
        regexp: '^#?PubkeyAuthentication'
        line: 'PubkeyAuthentication yes'
        backup: yes
    - name: SSH RSAAuthentication yes
      lineinfile:
        path: /etc/ssh/sshd_config
        regexp: '^#?RSAAuthentication'
        line: 'RSAAuthentication yes'
        backup: yes


- name: Configuration de la connexion utilisateur "nginx" via SSH avec une paire de clés
  hosts: SSHonlan
  become: true
  tasks:
    - name: Créer le répertoire .ssh pour l'utilisateur nginx
      file:
        path: /home/nginx/.ssh
        state: directory
        owner: nginx
        group: nginx

    - name: Définir les permissions du répertoire .ssh et du fichier authorized_keys
      file:
        path: /home/nginx/.ssh/*
        state: directory
        owner: nginx
        group: nginx
        mode: '0644'
      notify: Restart SSH Service

  handlers:
    - name: Restart SSH Service
      systemd:
        name: sshd
        state: restarted

- name: Installation du package sudo
  hosts: SSHonlan
  become: true
  tasks:
    - name: Installer sudo
      package:
        name: sudo
        state: present

- name: Installation du package Nginx
  hosts: SSHonlan
  become: true
  tasks:
    - name: Installer Nginx
      package:
        name: nginx
        state: present
```