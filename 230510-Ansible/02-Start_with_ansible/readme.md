# Start with ansible

## INSTALLATION D’ANSIBLE

👉 Installez Ansible via le gestionnaire de package Python pip.

Nul besoin de passer par un environnement virtuel, vous allez directement l’installer sur votre machine hôte.

La commande ansible n’étant pas encore reconnue, il est nécessaire de faire comprendre à votre terminal où trouver l’exécutable.


👉 Ajoutez la ligne ci-dessous à la fin du fichier `~/.bashrc` de votre machine hôte.

`export PATH="$PATH:/home/engineer/.local/bin"`

👉 Appliquez les modifications effectuées dans le fichier de configuration du terminal en exécutant la commande ci-dessous.

`source ~/.bashrc`

👉 Enfin, vérifiez l’installation d’Ansible en récupérant sa version.

`ansible --version`

## INVENTAIRE ANSIBLE

Un inventaire Ansible est une liste d’hôtes sous forme d’adresses IP permettant de simplifier l’exécution des tâches pour un groupe de serveurs.

👉 Sur Linode, créez une VM vierge nommée "test-ansible" qui sera votre terrain de jeu pour découvrir Ansible :

```
Debian 11
Dedicated 4 GB (Dedicated CPU)
```

👉 Sur votre machine hôte, créez un inventaire Ansible nommé "linode" en ajoutant l’adresse IP du serveur créé précédemment dans le fichier de configuration dédié.

👉 Vérifiez que l’hôte a bien été ajouté dans votre inventaire en exécutant la commande suivante.

`ansible all --list-hosts`

## EXÉCUTION D’ANSIBLE

Pour exécuter les différentes tâches, Ansible communique avec les hôtes en se connectant via SSH.

👉 Vérifiez que vous pouvez vous connecter manuellement au serveur créé dans la partie précédente via SSH.

👉 Exécutez un ping sur l’ensemble des machines de l’inventaire (une seule pour le moment) en prenant soin de préciser le chemin vers la clé SSH privée permettant de se connecter au serveur.

```
[SSHonlan]
192.168.0.98
192.168.0.89
```

`ansible all -m ping -u root --private-key PATH`

Si le serveur répond "pong", c’est que tout est bien configuré et que vous êtes prêt à utiliser Ansible 🚀

👉 Ne supprimez pas la VM Linode, elle sera utilisée pour les prochains challenges.