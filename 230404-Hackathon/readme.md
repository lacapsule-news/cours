# AWESOME LAB

Bienvenue à votre premier hackathon dédié à la mise en place d’un réseau virtuel complet 🔥

Vous travaillerez chacun sur votre machine, mais vous avancerez en groupe et au même rythme afin d’avoir le même résultat à la fin de la journée.


- Récupérez la ressource "awesomelab.zip" depuis l’onglet dédié sur Ariane.


- Importez le fichier "awesomelab.gns3project" dans GNS3 et démarrez tous les équipements réseau via le bouton "Start".

Cette simulation de réseau contient seulement un routeur relié à internet qui distribue les adresses IP en DHCP ainsi qu’un switch.

## Mon installation

J'utilise l'hyperviseur proxmox : https://www.proxmox.com/en/

![proxmox](images/proxmox.png)

Sur lequel 2 CT tourne, apache.lacapsule.lan et dns.lacapsule.lan.

J'ai aussi un routeur pfsense, ainsi qu'un lan dédié 192.168.150.0/24, le tout routé correctement.

![running-instance](images/infra.png)

# LET THE MAGIC BEGIN!

- Créez un serveur web en utilisant le logiciel Apache 2 sous Debian.
- Mise a jours du système : apt update && apt upgrade -y
- Installation du paquet : apt install apache2
- Vérification du lancement : systemctl status apache2

![apache2-status](images/apache2.status.png)

- Créez votre propre serveur DNS sur une machine Debian (une suppémentaire au serveur web) en utilisant la solution bind9.
- Mise à jours du système : apt update && apt upgrade -y
- Installation du paquet : apt install bind9
- Vérification du lancement : systemctl status bind9

![bind9-status](images/bind9.status.png)

- Assurez-vous que toutes les requêtes DNS passent par votre propre serveur DNS plutôt que par celui configuré par défaut (8.8.8.8) sur le routeur.
- Ajout d'un serveu dhcp
- range-min: 192.168.150.20
- range-max: 192.168.150.40
- gateway: 192.168.150.254
- dns: 192.168.150.252

![enable-dhcp](images/dhcp.enable.png)
![dhcp-range](images/dhcp.range.png)
![dhcp-dns](images/dhcp.dnssetup.png)


- Grâce à bind9, faîtes en sorte que le site hébergé sur le serveur web puisse être joignable par d’autres machines depuis le nom de domaine "awesome.lab".
- J'ai ajouté mon serveur Windows pour simuler un client
![windows-http-rqt](images/windows.iphttp.png)
- Edit /etc/bind/named.conf.options like source/bind9/named.conf.options
- Add to /etc/bind/named.conf.local, source/bind9/named.conf.local


L’extension ".lab" n’est pas officiellement reconnue, mais cela ne devrait pas vous poser de problème étant donné que le nom de domaine sera utilisé seulement en local.

![it-works](images/itworks.png)

- Pour obtenir les fonctionnalisé de forwarding (contacter google.fr depuis notre dns) il faut éditer le fichier named.conf.options pour ajouter 1 (ou plus) forwarders ainsi qu'autoriser la récursion sinon le message 'UnKnown can't find google.fr: Query refused' nous est retourner.

- consulter ses requête dhcp afin de vérifier la bonne attribution du dns
`tail /var/lib/dhcp/dhclient.leases`