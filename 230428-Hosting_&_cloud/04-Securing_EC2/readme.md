# SECURING EC2

## SÉCURISATION D’UNE INSTANCE EC2

👉 Créez une nouvelle instance nommée "secureme", avec les mêmes caractéristiques que le challenge précédent et en créant une nouvelle paire de clés SSH.
Nul besoin d’assigner cette instance une adresse IP élastique.

👉 Par défaut, le port SSH est ouvert et accessible depuis internet. Trouvez la commande pour vérifier les tentatives de connexion via SSH.

Théoriquement, si vous laissez le serveur SSH avec les paramètres par défaut pendant quelques minutes / heures, vous verrez que des bots tentent de se connecter au serveur même s’il s’agit d’une connexion par clé SSH.

👉 Sécurisez le serveur SSH en désactivant la connexion en tant qu'utilisateur root et en modifiant le port par défaut.

👉 À partir de la console AWS, trouvez l’endroit permettant de configurer le pare-feu en créant un nouveau groupe de sécurité nommé "web-server", basé sur des règles en IPv4 seulement avec les règles entrantes suivantes :

Ping : IP actuelle seulement
Nouveau port SSH : IP actuelle seulement ou toutes les IP
Port HTTP et HTTPS : toutes les IP

👉 Afin de sécuriser davantage le serveur, installez l’application fail2ban qui est chargée de chercher des tentatives répétées de connexions infructueuses dans les logs systèmes et procéder à un bannissement de l’adresse IP incriminée.

👉 Assurez-vous de résilier et de libérer toutes les ressources crées afin d’éviter une surfacturation et de dépasser le tier gratuit d’AWS :

Instances : résiliez (terminate) l’instance "secureme"