# Setup aws

## CRÉATION DU COMPTE AWS

Amazon Web Services (AWS) est un service d’Amazon spécialisé dans les services de cloud computing à la demande pour les entreprises et particuliers.

Basé sur une tarification à l’usage (généralement à l’heure) qui peut rapidement être coûteuse,  AWS propose néanmoins une offre gratuite afin d’essayer la plupart de ses produits : https://aws.amazon.com/free 

👉 Créez un compte sur AWS.com en suivant ces étapes :

Choisissez "Personnel" à la question “Comment prévoyez-vous d'utiliser AWS ?"
Remplissez vos informations de CB, elles ne seront utilisées que pour bloquer 1$ pendant quelques jours afin d’éviter les fraudes et activer l’offre gratuite
Remplissez votre numéro de téléphone pour valider le compte avec un code reçu par SMS (le numéro pourra être réutilisé pour un autre compte AWS et ne sert que pour la validation du compte)
Sélectionnez le niveau de support basique

## CONFIGURATION DU COMPTE AWS

À travers sa plateforme, AWS offre plus de 200 services capables de couvrir tous les besoins potentiels d’une application dans l’internet d’aujourd’hui.

Rassurez-vous, même si on peut rapidement se perdre dans AWS, vous aurez toujours une liste des services récents et favoris pour vous y retrouver facilement.

👉 Recherchez "AWS Budgets" dans la barre de recherche des services et rendez-vous sur la partie "Modes de paiement" dans "Préférences" afin de vérifier que les informations de facturation ont bien été validées et que votre identité a bien été vérifiée.

👉 Toujours sur le service AWS Budgets, grâce à l’outil "Budgets" dans la partie "Cost Management", créez un budget qui vous alertera par mail dès que vos dépenses dépassent les limites de l'offre gratuite d'AWS.