# EC2 TEMPLATING

## TEMPLATE D’INSTANCE EC2

👉 À partir du tableau de bord EC2, créez un nouveau modèle de lancement nommé "web-server" qui permettra de déployer en un clic une instance paramétrée comme suit :

Système d’exploitation : Ubuntu Server 20.04, 64 bits (x86)
Type d’instance : t3.micro
Paire de clés : Créez une nouvelle clé SSH dédiée à ce modèle
Paramètres réseau : Sélectionnez le groupe de sécurité créé dans le challenge précédent
Stockage : 15 Gio

👉 À partir de la section "Détails avancés", trouvez un moyen d’exécuter un script au lancement de l’instance qui devra exécuter les actions suivantes :

Mise à jour des informations des dépots apt
Installation du serveur web nginx via apt
Modification de la configuration de nginx pour cacher le numéro de version dans le header de réponse
Redémarrer le serveur web afin d’appliquer les modifications

👉 Déployez une instance à partir du modèle créé précédemment.

👉 Après quelques minutes, récupérez l’adresse IP publique de l’instance et vérifiez que le serveur web est prêt et que le reverse proxy a bien été configuré.

`curl -v http://XXX.XXX.XXX.XXX`

👉 Assurez-vous de résilier et libérer toutes les ressources crées afin d’éviter une surfacturation et de dépasser le tier gratuit d’AWS :

Instances : résiliez (terminate) l’instance "web-server"