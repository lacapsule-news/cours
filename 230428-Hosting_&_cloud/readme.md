# Hosting & cloud

## Contexte

Pour des raisons de coûts, mais également de scalabilité, une entreprise ne peut pas toujours héberger sa propre infrastructure.

Les clouds providers permettent aux entreprises de dématérialiser leurs infrasctuctures pour l'adopter au besoin du moment.

### Fonctionnement

Il existe plusieurs cloud providers: Google Cloud, Microsoft Azure, AWS...

Ils fournissent les mêmes services et leus puissances dépendent de leurs data centers répartis partout dans le monde.

Ces clouds providers proposent différents services clés en main tels que:
- Serveur de production
- Service de stockage
- Base de données
- CDN

Le cloud providers loue ses services à l'utilisation ou à l'heure.

La gestion des services se fait au travers de l'interface web du cloud provider.

## Exo

### 05 - EC2 TEMPLATING

#### TEMPLATE D’INSTANCE EC2

👉 À partir du tableau de bord EC2, créez un nouveau modèle de lancement nommé "web-server" qui permettra de déployer en un clic une instance paramétrée comme suit :

Système d’exploitation : Ubuntu Server 20.04, 64 bits (x86)
Type d’instance : t3.micro
Paire de clés : Créez une nouvelle clé SSH dédiée à ce modèle
Paramètres réseau : Sélectionnez le groupe de sécurité créé dans le challenge précédent
Stockage : 15 Gio

👉 À partir de la section "Détails avancés", trouvez un moyen d’exécuter un script au lancement de l’instance qui devra exécuter les actions suivantes :

Mise à jour des informations des dépots apt
Installation du serveur web nginx via apt
Modification de la configuration de nginx pour cacher le numéro de version dans le header de réponse
Redémarrer le serveur web afin d’appliquer les modifications

👉 Déployez une instance à partir du modèle créé précédemment.

👉 Après quelques minutes, récupérez l’adresse IP publique de l’instance et vérifiez que le serveur web est prêt et que le reverse proxy a bien été configuré.

`curl -v http://XXX.XXX.XXX.XXX`

👉 Assurez-vous de résilier et libérer toutes les ressources crées afin d’éviter une surfacturation et de dépasser le tier gratuit d’AWS :

Instances : résiliez (terminate) l’instance "web-server"

### 04 - SECURING EC2

#### SÉCURISATION D’UNE INSTANCE EC2

👉 Créez une nouvelle instance nommée "secureme", avec les mêmes caractéristiques que le challenge précédent et en créant une nouvelle paire de clés SSH.
Nul besoin d’assigner cette instance une adresse IP élastique.

👉 Par défaut, le port SSH est ouvert et accessible depuis internet. Trouvez la commande pour vérifier les tentatives de connexion via SSH.

Théoriquement, si vous laissez le serveur SSH avec les paramètres par défaut pendant quelques minutes / heures, vous verrez que des bots tentent de se connecter au serveur même s’il s’agit d’une connexion par clé SSH.

👉 Sécurisez le serveur SSH en désactivant la connexion en tant qu'utilisateur root et en modifiant le port par défaut.

👉 À partir de la console AWS, trouvez l’endroit permettant de configurer le pare-feu en créant un nouveau groupe de sécurité nommé "web-server", basé sur des règles en IPv4 seulement avec les règles entrantes suivantes :

Ping : IP actuelle seulement
Nouveau port SSH : IP actuelle seulement ou toutes les IP
Port HTTP et HTTPS : toutes les IP

👉 Afin de sécuriser davantage le serveur, installez l’application fail2ban qui est chargée de chercher des tentatives répétées de connexions infructueuses dans les logs systèmes et procéder à un bannissement de l’adresse IP incriminée.

👉 Assurez-vous de résilier et de libérer toutes les ressources crées afin d’éviter une surfacturation et de dépasser le tier gratuit d’AWS :

Instances : résiliez (terminate) l’instance "secureme"

### 03 - VIRTUAL SERVERS WITH EC2

#### CRÉATION D’UNE INSTANCE EC2

Le service EC2 permet de louer des serveurs virtuels sur lesquels vous pouvez par exemple héberger des applications web en production, on parle alors de serveur de production.

Le fonctionnement de AWS est basé sur un système de régions basé sur les différents centres de données (data center) d’Amazon partout dans le monde.

La plupart du temps, lorsque vous louez ou utilisez un service AWS, il est rattaché à une région.

👉 Même si vous disposez de l’offre gratuite, comparez les tarifs des instances EC2 en Europe afin de choisir la région la moins chère pour une instance de type "t3.micro".

👉 Sur la console AWS, en haut à droite, sélectionnez la région choisie dans l’étape précédente.
Veillez à toujours vérifier quelle région est sélectionnée sur la console, car une instance EC2 créée dans une région ne sera pas visible pour une autre région, tout est compartimenté.

👉 Préparez le lancement d’une instance EC2 en sélectionnant et remplissant les informations suivantes :

Nom : myfirstvps
Système d’exploitation : Debian 11, 64 bits (x86)
Type d’instance : t3.micro
Paramètres réseau : "Sélectionner un groupe de sécurité existant" > "default"
Configurer le stockage : 15 Gio
Nombre d’instances : 1

👉 Pour la partie "Paire de clés (connexion)", créez une nouvelle paire de clés SSH portant le même nom que l’instance que vous vous apprêtez à créer, avec les options par défaut.

Gardez bien de côté le fichier .pem (clé privée) qui se télécharge à la création de la paire de clé, il vous sera utile pour vous connecter au serveur.

👉 Consultez le résumé puis lancez l’instance EC2. Cela ne devrait prendre que quelques secondes grâce à la magie du cloud ☁️

👉 Vérifiez que l’instance a bien été créée et que son état est bien "En cours d’exécution" à partir du menu "Instances".

#### UTILISATION D’UNE INSTANCE EC2

👉 Récupérez l’adresse IPv4 publique de l’instance.
Ne confondez pas avec le DNS IPv4 public qui est une adresse web en .com censée rediriger vers votre instance.


👉 Tentez d’exécuter la commande ping sur cette adresse IP.

Rien ne se passe ? C’est tout à fait normal. Par défaut et pour des raisons de sécurité, tous les accès entrant sont bloqués, pour tous les ports et les protocoles (y compris ICMP pour ping).

👉 Modifiez le groupe de sécurité par défaut afin d’autoriser tout le trafic entrant, pour tous les protocoles et à partir de n’importe quelle adresse IP.
Cette modification est évidemment temporaire, les règles seront davantage sécurisées dans le challenge suivant.

👉 À l’aide d’un terminal, connectez-vous au serveur via SSH en spécifiant le chemin vers la clé privée téléchargée précédemment.

Pour ce faire, vous devez trouver le nom de l’utilisateur par défaut créé par AWS lors de la configuration de l’instance et vous assurer que la clé corresponde aux exigences de OpenSSH en matière de permissions.

👉 Une fois connecté au serveur, vérifiez l’utilisateur courant grâce à la commande suivante.

`whoami`

#### ADMINISTRATION D’UNE INSTANCE EC2

👉 Depuis l’interface de la console EC2, arrêtez complètement l’instance en sélectionnant "Arrêter l’instance" puis démarrez-là de nouveau.

👉 Tentez de vous connecter au serveur via SSH en reprenant la commande que vous avez précédemment exécutée.


Rien ne se passe ? De nouveau, c’est tout à fait normal. Par défaut, lors de la création d’une instance EC2, une adresse IP publique dynamique est attribuée et celle-ci est libérée à chaque arrêt de l’instance.


👉 Trouvez un moyen d’allouer et attribuer un adresse IP publique statique à votre instance via les adresses IP élastiques.

Ce service est inclus dans le prix d’une instance EC2, mais peut être facturé si l’adresse IP commandée n’est pas attribuée ou bien si elle est attachée à une instance arrêtée.

👉 Une fois la nouvelle adresse IP publique statique rattachée à votre instance, tentez de l’utiliser pour vous connecter via SSH.

👉 Pour terminer ce challenge, assurez-vous de résilier et libérer toutes les ressources crées afin d’éviter une surfacturation et de dépasser le tier gratuit d’AWS :


Adresses IP élastiques : dissociez puis libérez l’adresse IP publique précédemment réservée
Instances : résiliez (terminate) l’instance "myfirstvps"

### 02 - Setup aws

#### CRÉATION DU COMPTE AWS

Amazon Web Services (AWS) est un service d’Amazon spécialisé dans les services de cloud computing à la demande pour les entreprises et particuliers.

Basé sur une tarification à l’usage (généralement à l’heure) qui peut rapidement être coûteuse,  AWS propose néanmoins une offre gratuite afin d’essayer la plupart de ses produits : https://aws.amazon.com/free 

👉 Créez un compte sur AWS.com en suivant ces étapes :

Choisissez "Personnel" à la question “Comment prévoyez-vous d'utiliser AWS ?"
Remplissez vos informations de CB, elles ne seront utilisées que pour bloquer 1$ pendant quelques jours afin d’éviter les fraudes et activer l’offre gratuite
Remplissez votre numéro de téléphone pour valider le compte avec un code reçu par SMS (le numéro pourra être réutilisé pour un autre compte AWS et ne sert que pour la validation du compte)
Sélectionnez le niveau de support basique

#### CONFIGURATION DU COMPTE AWS

À travers sa plateforme, AWS offre plus de 200 services capables de couvrir tous les besoins potentiels d’une application dans l’internet d’aujourd’hui.

Rassurez-vous, même si on peut rapidement se perdre dans AWS, vous aurez toujours une liste des services récents et favoris pour vous y retrouver facilement.

👉 Recherchez "AWS Budgets" dans la barre de recherche des services et rendez-vous sur la partie "Modes de paiement" dans "Préférences" afin de vérifier que les informations de facturation ont bien été validées et que votre identité a bien été vérifiée.

👉 Toujours sur le service AWS Budgets, grâce à l’outil "Budgets" dans la partie "Cost Management", créez un budget qui vous alertera par mail dès que vos dépenses dépassent les limites de l'offre gratuite d'AWS.