# Hosting & cloud

## Contexte

Pour des raisons de coûts, mais également de scalabilité, une entreprise ne peut pas toujours héberger sa propre infrastructure.

Les clouds providers permettent aux entreprises de dématérialiser leurs infrasctuctures pour l'adopter au besoin du moment.

### Fonctionnement

Il existe plusieurs cloud providers: Google Cloud, Microsoft Azure, AWS...

Ils fournissent les mêmes services et leus puissances dépendent de leurs data centers répartis partout dans le monde.

Ces clouds providers proposent différents services clés en main tels que:
- Serveur de production
- Service de stockage
- Base de données
- CDN

Le cloud providers loue ses services à l'utilisation ou à l'heure.

La gestion des services se fait au travers de l'interface web du cloud provider.