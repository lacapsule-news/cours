# Secret code

## Récupérez, dans l’ordre, les indices de chaque étape

Récupérez le fichier secretcode.zip et décompressez-le.

 - Le nombre de lettres dans le nom du seul dossier présent dans "secretcode" sera votre 1er indice. **8**

 - Le nom du fichier JavaScript contenu dans le dossier "myfolder" sera votre 2ème indice. **5**

 - Créez le fichier "terminal.txt", le nombre de lettres (sans l’extention ".txt") dans le nom de ce fichier sera votre 3ème indice. **8**

 - Enfin, revenez dans le répertoire principal et affichez le contenu du fichier "lastindice.txt" à l’aide de la commande "cat". La valeur affichée dans le terminal sera votre 4ème et dernier indice. **9**