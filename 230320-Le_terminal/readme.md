# Le terminal

## Le terminal

> L'idée, c’est de passer par le logiciel pour émuler (ou simuler) l'équipement terminal physique et toutes ses fonctionnalités.

## Temps libre

### Awk

> Awk est depuis inclus dans la norme POSIX ; aussi, sur tout système UNIX qui se respecte.

> awk est très compliqué et necessite un cours a lui tout seul, malgré toute les notion voila ce qu'il en ressort.

1. Les chaînes de caractères
1. Les fonctions mathématiques
1. Les conditions
1. Les boucles

Pour ne cité que les plus rentable (attrayant/comprenable)

### Synchoniser des fichier avec rsync

> **rsync** [Arguments] [Source] [Destionation]

Synchonisation du fichier source avec celui de destination, fonctione aussi avec ssh. 

## Pratique

### 07 - SCORES

#### TRAITEMENT AVANCÉ DES DONNÉES

Récupérez la ressource "scores.zip" depuis l’onglet dédié sur Ariane.

- Trouvez un moyen d’afficher les colonnes 2 et 4 du fichier scores.txt et stockez-les dans un fichier results.txt.

`awk '{print $4}' scores/scores.txt`

- Trouvez un moyen de calculer la moyenne de toutes les notes présentes dans le fichier results.txt et stockez-la dans un fichier average.txt.

`awk '{SUM=SUM+$4} END {print SUM/NR}' scores/scores.txt`

### 06 - DATA PROCESSING

#### TRAITEMENT DES DONNÉES

Récupérez la ressource "dataprocessing.zip" depuis l’onglet dédié sur Ariane.


- Trouvez un moyen de trier les données du fichier testLogs.txt par date et stockez le résultat dans un fichier sortedLogs.txt.

`sort dataprocessing/testLogs.txt > sortedLogs.txt`

- Trouvez un moyen d’afficher les colonnes 1 et 6 du fichier sortedLogs.txt et stockez-les dans un fichier data.txt.

`cat -n sortedLogs.txt > data.txt`

### 05 - Count Lines

#### COMPTER AVEC LE TERMINAL

Récupérez la ressource "countlines.zip" depuis l’onglet dédié sur Ariane.

- Affichez le contenu du répertoire via la commande ls.

- Stockez le résultat de la commande précédente dans un fichier numberLines.txt.

`ls countlines/ > numberLines.tx`

- Comptez le nombre de lignes présentes dans le fichier numberLines.txt.

```
cat -n numberLines.txt


	- **n** grep will print ligne number
```

### 04 - Error logs

#### FILTRAGE DE LOGS

Récupérez la ressource "errorlogs.zip" depuis l’onglet dédié sur Ariane.

- Stockez contenu de l’ensemble des fichiers log.txt présents dans le répertoire dans le fichier globalLogs.txt.

`cat errorlogs/log*.txt > globalLogs.txt`

- Filtrez le contenu du fichier globalLogs.txt pour n’afficher que les lignes contenant la mention WARNING.

- Stockez le résultat de la commande précédente dans un fichier warningLogs.txt.

```
grep -rn WARNING errorlogs/ > warningLogs.txt

	- **r** grep will go recursive
	- **n** grep will print ligne number
```

- Supprimez le fichier globalLogs.txt.

### 03 - Merge Files

#### FUSION DE FICHIERS

Récupérez la ressource "mergefiles.zip" depuis l’onglet dédié sur Ariane.

Affichez le contenu du fichier log1.txt via la commande cat.

- Modifiez la commande cat pour afficher le contenu de l’ensemble des fichiers logs présents dans le dossier.

-  Fusionnez le contenu de tous les fichiers logs du dossier dans un fichier globalLogs.txt en une seule commande.

- Affichez le contenu paginé du fichier globalLogs.txt.

```
cat -n mergefiles/log*.txt > mergefiles/globalLogs.txt

	- **n** grep will print ligne number
```

### 02 - Files and Folders

#### CRÉER ET SUPPRIMER UN DOSSIER

Le dossier récupéré contient différents fichiers est assez mal rangé, vous allez mieux l’organiser en vous servant uniquement du terminal.

- Créez un nouveau dossier appelé "txt" dans le dossier "filesandfolders" via la commande mkdir.
`mkdir filesandfolders/txt`

Supprimez le dossier "txt" via la commande rm et créez un répertoire "logs" à la place.

`rmdir filesandfolders/txt && mkdir filesandfolders/logs`

#### DÉPLACER UN FICHIER

- Déplacez le fichier log1.txt dans le dossier "logs" via la commande mv.

`mv filesandfolders/log1.txt filesandfolders/logs/`

- Déplacez les fichiers de logs restants dans le dossier "logs" en une seule commande grâce aux wildcards.

`mv filesandfolders/log*.txt filesandfolders/logs/`

### 01 - Secret code

##### Récupérez, dans l’ordre, les indices de chaque étape

Récupérez le fichier secretcode.zip et décompressez-le.

 - Le nombre de lettres dans le nom du seul dossier présent dans "secretcode" sera votre 1er indice. **8**

 - Le nom du fichier JavaScript contenu dans le dossier "myfolder" sera votre 2ème indice. **5**

 - Créez le fichier "terminal.txt", le nombre de lettres (sans l’extention ".txt") dans le nom de ce fichier sera votre 3ème indice. **8**

 - Enfin, revenez dans le répertoire principal et affichez le contenu du fichier "lastindice.txt" à l’aide de la commande "cat". La valeur affichée dans le terminal sera votre 4ème et dernier indice. **9**