# SCORES

## TRAITEMENT AVANCÉ DES DONNÉES

Récupérez la ressource "scores.zip" depuis l’onglet dédié sur Ariane.

- Trouvez un moyen d’afficher les colonnes 2 et 4 du fichier scores.txt et stockez-les dans un fichier results.txt.

`awk '{print $4}' scores/scores.txt`

- Trouvez un moyen de calculer la moyenne de toutes les notes présentes dans le fichier results.txt et stockez-la dans un fichier average.txt.

`awk '{SUM=SUM+$4; i=i+1} END {print SUM/i}' scores/scores.txt`