# Files and Folders

## CRÉER ET SUPPRIMER UN DOSSIER

Le dossier récupéré contient différents fichiers est assez mal rangé, vous allez mieux l’organiser en vous servant uniquement du terminal.

- Créez un nouveau dossier appelé "txt" dans le dossier "filesandfolders" via la commande mkdir.
`mkdir filesandfolders/txt`

Supprimez le dossier "txt" via la commande rm et créez un répertoire "logs" à la place.

`rmdir filesandfolders/txt && mkdir filesandfolders/logs`

## DÉPLACER UN FICHIER

- Déplacez le fichier log1.txt dans le dossier "logs" via la commande mv.

`mv filesandfolders/log1.txt filesandfolders/logs/`

- Déplacez les fichiers de logs restants dans le dossier "logs" en une seule commande grâce aux wildcards.

`mv filesandfolders/log*.txt filesandfolders/logs/`