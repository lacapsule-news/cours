# DATA PROCESSING

## TRAITEMENT DES DONNÉES

Récupérez la ressource "dataprocessing.zip" depuis l’onglet dédié sur Ariane.


- Trouvez un moyen de trier les données du fichier testLogs.txt par date et stockez le résultat dans un fichier sortedLogs.txt.

`sort -k 3,4 dataprocessing/testLogs.txt > sortedLogs.txt`

- Trouvez un moyen d’afficher les colonnes 1 et 6 du fichier sortedLogs.txt et stockez-les dans un fichier data.txt.

`cat -n sortedLogs.txt > data.txt`