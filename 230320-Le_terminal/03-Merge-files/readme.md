# Merge Files

## FUSION DE FICHIERS

Récupérez la ressource "mergefiles.zip" depuis l’onglet dédié sur Ariane.

Affichez le contenu du fichier log1.txt via la commande cat.

- Modifiez la commande cat pour afficher le contenu de l’ensemble des fichiers logs présents dans le dossier.

-  Fusionnez le contenu de tous les fichiers logs du dossier dans un fichier globalLogs.txt en une seule commande.

- Affichez le contenu paginé du fichier globalLogs.txt.

`cat -n mergefiles/log*.txt > mergefiles/globalLogs.txt`
