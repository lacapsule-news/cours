# Error logs

## FILTRAGE DE LOGS

Récupérez la ressource "errorlogs.zip" depuis l’onglet dédié sur Ariane.

Stockez contenu de l’ensemble des fichiers log.txt présents dans le répertoire dans le fichier globalLogs.txt.

Filtrez le contenu du fichier globalLogs.txt pour n’afficher que les lignes contenant la mention WARNING.

Stockez le résultat de la commande précédente dans un fichier warningLogs.txt.

Supprimez le fichier globalLogs.txt.

`grep -rn WARNING errorlogs/`

- **-r** grep will go recursive
- **n** grep will print ligne number


`cat errorlogs/log*.txt > globalLogs.txt`

