# Count Lines

## COMPTER AVEC LE TERMINAL

Récupérez la ressource "countlines.zip" depuis l’onglet dédié sur Ariane.

Affichez le contenu du répertoire via la commande ls.

Stockez le résultat de la commande précédente dans un fichier numberLines.txt.

`ls countlines/ > numberLines.tx`

Comptez le nombre de lignes présentes dans le fichier numberLines.txt.

`cat -n numberLines.txt`