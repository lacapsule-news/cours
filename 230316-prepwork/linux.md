# Linux

- LaCapsule - Prep Work
- Openclassrooms - Administrez un système Linux

------
## Les terminaux


> L'idée, c’est de passer par le logiciel pour émuler (ou simuler) l'équipement terminal physique et toutes ses fonctionnalités.

> Ce logiciel est souvent un petit programme qui se lance sur un système d'exploitation et qui permet de se connecter localement ou à distance sur le serveur à administrer.

> Il devient donc possible de lancer plusieurs terminaux simultanément depuis le même environnement !


À vrai dire, la console de Linux propose 7 terminaux en mode texte, appelés aussi les terminaux physiques. Ils sont directement sur le clavier branché à l'ordinateur et disponibles à partir des combinaisons de touches :

- “CTRL+ALT+F1”;
- “CTRL+ALT+F2” ;
- jusqu’à “CTRL+ALT+F7”.

Chacune de ces combinaisons de touches propose l'émulation d'un terminal (en mode console) différent sur lequel il est possible de se connecter de manière indépendante avec un compte utilisateur différent.

------
### Shell

> Le shell exécuté lors de la connexion d'un utilisateur sur un terminal est configuré dans le fichier  /etc/passwd

> Petite astuce : modifier le fichier **/etc/passwd** et indiquer un interpréteur de commandes comme **/usr/bin/nologin** ou **/dev/zero** ou encore **/dev/null**  , garantit que l'utilisateur ne pourra jamais lancer de shell à la suite du processus d'authentification lors de la connexion.

> Et donc, il ne pourra pas faire grand chose…  Mais chut ! Je ne vous ai rien dit… Revenons donc à Bash.


- Tips:
	- **Command linecompletion** - Ce mécanisme permet à l'interpréteur de commandes de compléter automatiquement une saisie à partir des premiers caractères saisis. L'administrateur déclenche cette fonctionnalité en frappant la touche TAB pendant la saisie et Bash essaie de compléter la saisie en fonction de son sens : un chemin, une commande, un argument, une option, etc.
	- **Signal handling** - Les signaux sont une forme de communication entre processus. Ils permettent d'émettre et de recevoir un ordre ou une information pendant leur exécution. L'exemple le plus connu est sûrement l'interruption clavier composée de la frappe des touches CTLR+C qui permet d'émettre le signal sigint (numéro 2). Si le processus écoute sur ce signal, il reçoit alors l'ordre d'interruption et se termine proprement.
	- **Exit code** - L'interpréteur de commandes Bash permet de récupérer le code de sortie d'un programme dans une variable nommée$?, ce mécanisme est très pratique pour interpréter le code et déterminer dans quelle condition le programme s'est terminé.


------
## Variable d'environement

```PS1_prompt
[sunburst@MA-Lap ~]$ echo $PS1
[\u@\h \W]\$
```
Voici une liste des argument possible

- \a an ASCII bell character (07)
- \d the date in "Weekday Month Date" format (e.g., "Tue May 26")
- \D{format} - the format is passed to strftime(3) and the result is inserted into the prompt string; an empty format results in a locale-specific time representation. The braces are required
- \e an ASCII escape character (033)
- \h the hostname up to the first part
- \H the hostname
- \j the number of jobs currently managed by the shell
- \l the basename of the shell's terminal device name
- \n newline
- \r carriage return
- \s the name of the shell, the basename of $0 (the portion following the final slash)
- \t the current time in 24-hour HH:MM:SS format
- \T the current time in 12-hour HH:MM:SS format
- \@ the current time in 12-hour am/pm format
- \A the current time in 24-hour HH:MM format
- \u the username of the current user
- \v the version of bash (e.g., 2.00)
- \V the release of bash, version + patch level (e.g., 2.00.0)
- \w the current working directory, with $HOME abbreviated with a tilde
- \W the basename of the current working directory, with $HOME abbreviated with a tilde
- \! the history number of this command
- \# the command number of this command
- \$ if the effective UID is 0, a #, otherwise a $
- \nnn the character corresponding to the octal number nnn \\ a backslash
- \[ begin a sequence of non-printing characters, which could be used to embed a terminal control sequence into the prompt
- \] end a sequence of non-printing character

Le .bashrc de chaque utilisateur gere le lancement de ça session. Dans mon cas :

- alias ls='ls -lrth --color=auto'
- alias ll='ls -lrtha --color=auto'
- PS1='[\u@\h \W]\$ '
- alias mc='f(){ sudo ifconfig "$@" down; sudo macchanger -r "$@"; sudo ifconfig "$@" up; unset -f f; }; f'

## L’arborescence (FileSystem)

### /usr

- **/bin**: ce sont les commandes critiques pour le bon fonctionnement du système, quel que soit son objectif. Elles sont lancées par le système et par l'administrateur ;
- **/sbin**: ce sont les commandes uniquement à destination de l'administrateur pour la gestion du système.
- **/usr/bin**: ce répertoire contient majoritairement les commandes à destination de tous les utilisateurs du système, privilégiés ou non.
- **/usr/sbin**: ce sont des commandes à nouveau à destination unique de l'administrateur, mais non critiques pour le bon fonctionnement du système.

> Maintenant il existe aussi un répertoire **/usr/local/bin**! Ce dernier de la famille va contenir tous les binaires qui sont compilés manuellement par l'administrateur après l'installation du système.

Le répertoire **/usr** est très important, il est marqué "shareable" et "static", ce qui implique que d'un système à l'autre, les éléments contenus dans ce répertoire sont censés fonctionner exactement de la même manière.

### /var

- **/var/log**: répertoire contenant l'arborescence de toutes les traces systèmes et applicatives. C'est dans ce répertoire qu'il est possible de consulter les traces des historiques de démarrage du système, de connexion des comptes utilisateurs, d'activité des services réseaux (SSH, HTTPD, SMTP, etc.) ainsi que les traces du noyau. Généralement les applications installées sur le système disposent de leur propre sous-répertoire (/var/log/apache2par exemple).
- **/var/run**: répertoire contenant toutes les données relatives aux processus en cours d'exécution, les sémaphores, les données applicatives, les fichiers numéro de processus, etc.
- **/var/spool**: répertoire contenant des données stockées de manière temporaire entre processus. Souvent, ce répertoire est utilisé pour stocker des données relatives à des actions ou tâches à effectuer dans un futur proche par les processus en cours d'exécution.
- **/var/mail**: c'est le répertoire de stockage des messageries électroniques locales des comptes utilisateurs du système.

> Certains programmes vont stocker par défaut leurs données temporaires dans l'arborescence/var. Ainsi, le serveur web Apache HTTPD par exemple, lorsqu'il est installé via les packages des distributions majeures est configuré avec un docroot(répertoire contenant par défaut les sources des sites web) pointant vers **/var/www**.

Ce qui fait que :

- certains sous-répertoires de **/var** seront marqués variables **unshareable**  (par exemple **/var/log**)
- alors que d'autres seront marqués variables **shareable**(comme **/var/mail** ou **/var/www** par exemple).

### /proc

Fichiers sont intéressants à relever dans cette arborescence :
- **/proc/cpuinfo** contient les informations sur le(s) processeur(s) maintenues par le noyau,
- **/proc/version** contient la version exacte du noyau en exécution, 
- **/proc/meminfo**, les informations détaillés sur la mémoire vive gérée par le noyau,
- **/proc/uptime**, le temps d'exécution cumulé,
- **/proc/cmdline**, les paramètres passés au démarrage du noyau,

> /proc contient également beaucoup de répertoires, dont la grande majorité porte des noms à base de chiffres. En effet, tous les processus en exécution sur le système sont identifiés par un numéro unique géré par le noyau. 

> Ainsi ce dernier met à disposition les informations concernant chaque processus dans le répertoire portant son numéro associé.

### /sys

> Cette seconde arborescence fonctionne sur le même principe que sa petite sœur **/proc**. Elle présente des informations maintenues en temps réel par le noyau. À une différence fondamentale près : certains éléments de cette arborescence sont accessibles en écriture aux comptes privilégiés du système et notamment les variables systèmes du noyau dans le répertoire **kernel**.

Cette arborescence contient les informations sur les périphériques gérés par le noyau, notamment :
- Les périphériques de type bloc ou caractères dans les répertoires **/sys/block** ou **/sys/dev**,
- Les drivers dans **/sys/devices**,
- Les différents systèmes de fichiers dans **/sys/fs**,
- Les modules du noyau dans **/sys/module**.

> Le répertoire **/sys/kernel** contient une arborescence de fichiers représentant des variables du noyau accessibles en écriture et permettant de modifier le comportement à chaud du système. Par exemple, le répertoire **/sys/kernel/debug** contient des fichiers permettant d'activer des fonctions de traces et de débogage du noyau.

> Sur les distributions principales de Linux, le répertoire **/proc/sys/** est également accessible en écriture sur certains paramètres. En effet, d'un point de vue historique, seule l'arborescence **/proc** existait. **/sys** a été ajoutée à partir des versions 2.6 du noyau, notamment pour différencier la gestion des informations concernant les périphériques.