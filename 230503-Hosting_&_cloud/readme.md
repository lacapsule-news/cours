# Hosting & cloud

## 03 - KUBERNETES IN PRODUCTION

### PRÉPARATION DES MANIFESTES

👉 Créez des manifestes de déploiement et de service (dans le même fichier) dédiés au déploiement d’un serveur web Nginx en respectant les contraintes suivantes :

Le manifeste de service sera nommé "mywebserver-service" et devra être rattaché à l’application nommée "mywebserver"
Le service se contentera d’exposer le port par défaut de Nginx (avec un load balancer)
Le manifeste de déploiement sera nommé "mywebserver-deployment" et devra être rattaché à l’application nommée "mywebserver"
Le conteneur déployé sera nommé "nginx" et basé sur l’image officielle de Nginx dans sa dernière version
6 pods seront utilisés pour le déploiement du conteneur "nginx"

👉 Testez le déploiement du serveur web en local (via minikube) en appliquant les manifestes grâce à la commande kubectl apply.

👉 Utilisez la commande suivante afin d’obtenir une URL censée rediriger, via le service déployé précédemment, vers un pod exposant le port 80 du conteneur qu’il héberge.

`minikube service mws-service --url`

### MISE EN PRODUCTION DE L’APPLICATION

👉 Sur l’interface de Linode, rendez-vous sur l’interface Kubernetes afin de créer un cluster respectant les contraintes suivantes :

Nom du cluster : myfirstcluster
Région : Frankfurt, DE
Version de Kubernetes : La plus récente parmi celles proposées
Node Pools : Shared CPU > Linode 2 GB x3

👉 Après quelques minutes et une fois le cluster déployé, téléchargez le fichier Kubeconfig "myfirstcluster-kubeconfig.yaml" qui permettra à la commande kubectl de joindre votre cluster plutôt que de passer par minikube qui, pour rappel, n’est utile que dans un environnement de développement ou de test.

👉 En vous positionnant dans le dossier où le fichier Kubeconfig a été téléchargé, exécutez la commande suivante afin d’appliquer la configuration de votre cluster pour kubectl.

`export KUBECONFIG=myfirstcluster-kubeconfig.yaml`

👉 Vérifiez que kubectl est bien paramétré pour votre cluster Linode en exécutant la commande suivante.

`kubectl cluster-info`

👉 Maintenant que vous êtes en production, listez les nodes de votre cluster et comparez-les avec la liste affichée par Linode dans la section "Node Pools".

👉 Appliquez les manifestes créés précédemment.

👉 Vérifiez l’état du déploiement et de vos pods grâce aux commandes habituelles.

Votre application est maintenant déployée en production via Kubernetes ! Facile non ?
Allons vérifier ça en peu plus en détail en récupérant l’IP publique du cluster afin de visiter la belle page de bienvenue de Nginx.

👉 Listez les services du cluster grâce à la commande habituelle.

D’habitude, la colonne "EXTERNAL-IP" de vos services n'affiche que l’information "<pending>", mais cette époque est révolue, car vos cluster n’est plus dépendant de minikube et d’un environnement local.

👉 Récupérez et visitez l’adresse IP publique de votre cluster afin de vérifier le déploiement de votre application basée sur Nginx.

👉 Afin de comprendre un peu plus le fonctionnement des pods et des worker nodes, exécutez la commande suivante.

`kubectl describe pods | grep "Node:"`

Vous êtes censés voir 6 lignes (une pour chaque pod) où chacune indique au sein de quel worker un pod est déployé. On remarque que Kubernetes fait bien les choses, car nous avons en tout 6 pods (comme indiqué dans notre manifeste) répartis équitablement parmi les 3 workers.

👉 Assurez-vous de résilier toutes les ressources créées afin d’éviter une surfacturation et de dépasser le crédit offert par Linode, notamment votre cluster Kubernetes, mais également le NodeBalancer créé automatiquement.

## 02 - Docker in production

### PRÉPARATION DU DOCKER COMPOSE

👉 Créez un fichier Docker Compose chargé de déployer une plateforme collaborative d’hébergement de fichiers Nextcloud en respectant les contraintes suivantes :

Le conteneur sera basé sur l’image officielle de Nextcloud, dans sa dernière version
Le port par défaut de Nextcloud sera exposé afin d’être joignable à partir du port 8585 sur la machine hôte
Un volume nommé "nextcloud" (managé par Docker) sera créé afin de rendre persistant le dossier "/var/www/html" du conteneur

👉 Démarrez le service "nextcloud" via la commande docker-compose afin de le tester en local avant de le déployer dans la partie suivante.

👉 Visitez l’URL localhost:8585 afin de vérifier que la solution Nextcloud a bien été déployée.

👉 Configurez Nextcloud via son interface avec les paramètres de base et uploadez un simple fichier texte afin de vérifier la persistance des données.

👉 Stoppez puis supprimez le conteneur lié au service "nextcloud".

👉 Redémarrez le service "nextcloud" afin de vérifier que le fichier texte précédemment uploadé est toujours présent.

👉 Créez un répertoire GitLab public afin d’y héberger votre fichier "docker-compose.yml".

👉 Récupérez le lien direct vers le fichier brut (raw). L’URL est censée ressemble à ceci : https://gitlab.com/NOM_COMPTE/NOM_REPO/-/raw/main/docker-compose.yml

Cette dernière étape est essentielle pour la suite, car Linode a besoin d’un lien vers le fichier Docker Compose afin de déployer un conteneur en production.

### DÉPLOIEMENT EN PRODUCTION

👉 À partir de la marketplace de Linode, sélectionnez l’application Docker et configurez-la via les options proposées afin de lancer le service défini dans votre fichier Docker Compose.
Vous devez sélectionner une instance "Nanode 1 GB" dans "Shared CPU", le tout sous Debian 11.

👉 Quelques minutes après le lancement du serveur, récupérez son IP publique et vérifiez le démarrage de Nextcloud en visitant l’URL sur le port 8585.

👉 Configurez Nextcloud via son interface avec les paramètres de base et uploadez un simple fichier texte afin de vérifier la persistance des données.

👉 Redémarrez le serveur via l’interface de Linode (option "Reboot") puis vérifiez que le fichier texte précédemment uploadé est toujours présent.

Félicitations, vous avez déployé votre premier conteneur en production ! 🎉

👉 Assurez-vous de résilier toutes les ressources créées afin d’éviter une surfacturation et de dépasser le crédit offert par Linode, notamment le serveur Linode dédié au déploiement de votre application Docker.

## 01 - Setup linode

### CRÉATION D’UN COMPTE LINODE

Linode est un fournisseur d'hébergement cloud spécialisé dans la location de machines virtuelles Linux et d’environnements de production Docker et Kubernetes .

👉 Inscrivez-vous à Linode en suivant le lien suivant afin de bénéficier des 100$ de crédit : https://www.linode.com/lp/free-credit-100/

👉 Une fois le compte créé et validé, rendez-vous à l’adresse suivante afin de vérifier que le crédit offert par Linode est bien visible : https://cloud.linode.com/account/billing

### CLÉ SSH

👉 A partir d’un terminal, exécutez la commande suivante afin de créer une paire de clé SSH dédiée à Linode.

`ssh-keygen -f linode -N ""`

Cette commande permet de générer une clé privée nommée "linode" et une clé publique nommée "linode.pub" (sans passphrase) dans le répertoire courant "~/.ssh/".

👉 A partir du menu "SSH Keys" sur le panel Linode, créez une nouvelle clé SSH nommée "VM Linux" et collez le contenu du fichier "linode.pub" en guise de "SSH Public Key".

Gardez précieusement la paire de clé SSH générée précédemment, elle vous sera utile afin de vous connecter aux serveurs virtuels.