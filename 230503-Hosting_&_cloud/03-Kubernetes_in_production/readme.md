# KUBERNETES IN PRODUCTION

## PRÉPARATION DES MANIFESTES

👉 Créez des manifestes de déploiement et de service (dans le même fichier) dédiés au déploiement d’un serveur web Nginx en respectant les contraintes suivantes :

Le manifeste de service sera nommé "mywebserver-service" et devra être rattaché à l’application nommée "mywebserver"
Le service se contentera d’exposer le port par défaut de Nginx (avec un load balancer)
Le manifeste de déploiement sera nommé "mywebserver-deployment" et devra être rattaché à l’application nommée "mywebserver"
Le conteneur déployé sera nommé "nginx" et basé sur l’image officielle de Nginx dans sa dernière version
6 pods seront utilisés pour le déploiement du conteneur "nginx"

👉 Testez le déploiement du serveur web en local (via minikube) en appliquant les manifestes grâce à la commande kubectl apply.

👉 Utilisez la commande suivante afin d’obtenir une URL censée rediriger, via le service déployé précédemment, vers un pod exposant le port 80 du conteneur qu’il héberge.

`minikube service mws-service --url`

## MISE EN PRODUCTION DE L’APPLICATION

👉 Sur l’interface de Linode, rendez-vous sur l’interface Kubernetes afin de créer un cluster respectant les contraintes suivantes :

Nom du cluster : myfirstcluster
Région : Frankfurt, DE
Version de Kubernetes : La plus récente parmi celles proposées
Node Pools : Shared CPU > Linode 2 GB x3

👉 Après quelques minutes et une fois le cluster déployé, téléchargez le fichier Kubeconfig "myfirstcluster-kubeconfig.yaml" qui permettra à la commande kubectl de joindre votre cluster plutôt que de passer par minikube qui, pour rappel, n’est utile que dans un environnement de développement ou de test.

👉 En vous positionnant dans le dossier où le fichier Kubeconfig a été téléchargé, exécutez la commande suivante afin d’appliquer la configuration de votre cluster pour kubectl.

`export KUBECONFIG=myfirstcluster-kubeconfig.yaml`

👉 Vérifiez que kubectl est bien paramétré pour votre cluster Linode en exécutant la commande suivante.

`kubectl cluster-info`

👉 Maintenant que vous êtes en production, listez les nodes de votre cluster et comparez-les avec la liste affichée par Linode dans la section "Node Pools".

👉 Appliquez les manifestes créés précédemment.

👉 Vérifiez l’état du déploiement et de vos pods grâce aux commandes habituelles.

Votre application est maintenant déployée en production via Kubernetes ! Facile non ?
Allons vérifier ça en peu plus en détail en récupérant l’IP publique du cluster afin de visiter la belle page de bienvenue de Nginx.

👉 Listez les services du cluster grâce à la commande habituelle.

D’habitude, la colonne "EXTERNAL-IP" de vos services n'affiche que l’information "<pending>", mais cette époque est révolue, car vos cluster n’est plus dépendant de minikube et d’un environnement local.

👉 Récupérez et visitez l’adresse IP publique de votre cluster afin de vérifier le déploiement de votre application basée sur Nginx.

👉 Afin de comprendre un peu plus le fonctionnement des pods et des worker nodes, exécutez la commande suivante.

`kubectl describe pods | grep "Node:"`

Vous êtes censés voir 6 lignes (une pour chaque pod) où chacune indique au sein de quel worker un pod est déployé. On remarque que Kubernetes fait bien les choses, car nous avons en tout 6 pods (comme indiqué dans notre manifeste) répartis équitablement parmi les 3 workers.

👉 Assurez-vous de résilier toutes les ressources créées afin d’éviter une surfacturation et de dépasser le crédit offert par Linode, notamment votre cluster Kubernetes, mais également le NodeBalancer créé automatiquement.