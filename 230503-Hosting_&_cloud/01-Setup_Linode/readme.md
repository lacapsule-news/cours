# Setup linode

## CRÉATION D’UN COMPTE LINODE

Linode est un fournisseur d'hébergement cloud spécialisé dans la location de machines virtuelles Linux et d’environnements de production Docker et Kubernetes .

👉 Inscrivez-vous à Linode en suivant le lien suivant afin de bénéficier des 100$ de crédit : https://www.linode.com/lp/free-credit-100/

👉 Une fois le compte créé et validé, rendez-vous à l’adresse suivante afin de vérifier que le crédit offert par Linode est bien visible : https://cloud.linode.com/account/billing

## CLÉ SSH

👉 A partir d’un terminal, exécutez la commande suivante afin de créer une paire de clé SSH dédiée à Linode.

`ssh-keygen -f linode -N ""`

Cette commande permet de générer une clé privée nommée "linode" et une clé publique nommée "linode.pub" (sans passphrase) dans le répertoire courant "~/.ssh/".

👉 A partir du menu "SSH Keys" sur le panel Linode, créez une nouvelle clé SSH nommée "VM Linux" et collez le contenu du fichier "linode.pub" en guise de "SSH Public Key".

Gardez précieusement la paire de clé SSH générée précédemment, elle vous sera utile afin de vous connecter aux serveurs virtuels.