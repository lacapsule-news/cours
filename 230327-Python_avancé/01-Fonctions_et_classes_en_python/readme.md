# Les fonctions et classes en python

## Les fonctions

> Une fonction est un bloc de code que l'on va pouvoir réutiliser.

> Par exemple, on ecrit un fonction qui retourne un nombre random, a chaque fois que notre programme aura besoin d'un nombre random on aura juste a appeler la fonction

```
import random, math

def getRandomNumber(min,max):
	randomNumber = random.random() * (min-max)+min
	randomNumber = math.floor(randomNumber)

	return randomNumber
```

> Pour executer le code, il va falloir l'appeler par sont nom suivie des argument entre parenthese

`getRandomNumber(1-100)`


## Les classes

> une classe est un modèle qui va servir a créer des objets. Elle possède des attributs avec des valeurs par défaut. Un objet hérite des attributs de sa classe et peut les personnaliser.

