class CompteBancaire():
	def __init__(self,nom: str,solde: int) -> None:
		self.nom = nom
		self.solde= solde

	def depot(self,somme:int) -> int:
		self.solde += somme
		return self.solde

	def retrait(self,somme:int) -> int:
		self.solde -= somme
		return self.solde

	def __repr__(self):
		return  "Compte bancaire de "+str(self.nom)+" : "+str(self.solde)+" euros."

user = CompteBancaire("Matthieu",-2)
print(user)
user.depot(300)
print(user)
print(user.retrait(98))

