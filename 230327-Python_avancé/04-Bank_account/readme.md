# Bank Account

## DÉFINITION D’UN OBJET

> L’objectif de ce challenge est de créer un objet permettant de définir simplement le compte bancaire d’un utilisateur.


1. Rendez-vous sur le site Replit afin d’exécuter du code Python hors d’Ariane.
1. Initialisez une classe CompteBancaire(), permettant d’instancier des objets à partir des attributs nom (string) et solde (int).
1. Mettez en place trois méthodes :
	- depot(self, somme) : ajoute la somme indiquée au solde.
	- retrait(self, somme) : retire la somme indiquée au solde.
	- __repr__(self) : affiche le nom du titulaire et du solde du compte. N’hésitez pas à regarder sur internet pour comprendre l'intérêt et l’importance de cette méthode.

> Par exemple : print(CompteBancaire("Vanessa", 2000)) affichera dans la console : "Compte bancaire de Vanessa : 2000 euros."

```
class CompteBancaire():
	def __init__(self,nom: str,solde: int) -> None:
		self.nom = nom
		self.solde= solde

	def depot(self,somme:int) -> int:
		self.solde += somme
		return self.solde

	def retrait(self,somme:int) -> int:
		self.solde -= somme
		return self.solde

	def __repr__(self):
		return  "Compte bancaire de "+str(self.nom)+" : "+str(self.solde)+" euros."

user = CompteBancaire("Matthieu",-2)
print(user)
user.depot(300)
print(user)
print(user.retrait(98))
```