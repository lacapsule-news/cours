from Person import Person

class Work(Person):
	"""docstring for Work"""
	def __init__(self,lastname:str,firstname:str,phone:str,mail:str,company:str,address:str,company_phone:str):
		super().__init__(lastname,firstname,phone,mail)
		self.company = company
		self.address = address
		self.company_phone = company_phone

	def __repr__(self):
		return f"Nom : {self.lastname}.\nPrenom : {self.firstname}.\nNuméro : {self.phone}.\nMail : {self.mail}.\nCompany : {self.company}.\nAddresse : {self.address}.\nNuméro profesionnel : {self.company_phone}.\n"


