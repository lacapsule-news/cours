class Person():
	def __init__(self,lastname:str,firstname:str,phone:str,mail:str):
		self.lastname = lastname
		self.firstname = firstname
		self.phone = phone
		self.mail = mail

	def __repr__(self):
		return f"Nom : {self.lastname}.\nPrenom : {self.firstname}.\nNuméro : {self.phone}.\nMail : {self.mail}.\n"