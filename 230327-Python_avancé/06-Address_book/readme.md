# ADDRESS BOOK

## STRUCTURE DU CARNET

> L’objectif de ce challenge est de créer un ensemble de classes permettant de construire un carnet d’adresses.


1. Initialisez une classe Person(), permettant d’instancier des objets à partir d’attributs lastname, firstname, phone et mail. L’objet disposera également de la méthode __repr__ pour imprimer les informations de la personne.
1. Initialisez une classe Work(), permettant d’instancier des objets à partir d’attributs lastname, firstname, phone, mail, company, address et company_phone. L’objet disposera également de la méthode __repr__ pour imprimer les informations de la personne et de son travail.
1. Initialisez une classe Dev(), permettant d’instancier des objets à partir d’attributs lastname, firstname, phone, mail, company, address, company_phone, languages (sous forme de liste) et preferred_stack ("backend" ou "frontend"). L’objet disposera également de la méthode __repr__ pour imprimer les informations de la personne, de son travail et de ses préférences de dev.

## MISE EN PLACE DU CARNET

1. Initialisez une classe Contacts(), qui contiendra un ensemble d’objets sous forme de liste (des objets de Person, Work ou Dev, peu importe). Associez à Contacts une méthode __init__ ne prenant pas d’arguments, permettant simplement d’initialiser une instance, et une méthode __repr__ permettant d’afficher le contenu de l’instance sous forme de chaîne de caractères.
1. Ajoutez à votre classe Contacts une méthode add_contact permettant d’ajouter un contact à partir d’une instance de Person, une méthode search_contact permettant de trouver un contact à partir de son nom (argument obligatoire) et de son prénom (argument optionnel) si plusieurs contacts ont le même nom dans le carnet d’adresses, une méthode remove_contact permettant de supprimer un contact du carnet en connaissant son nom et son prénom.

```
class Person:
    def __init__(self, lastname, firstname, phone, mail):
        self.lastname = lastname
        self.firstname = firstname
        self.phone = phone
        self.mail = mail
        
    def __repr__(self):
        return f"{self.firstname} {self.lastname}\nPhone: {self.phone}\nEmail: {self.mail}"
    
    
class Work(Person):
    def __init__(self, lastname, firstname, phone, mail, company, address, company_phone):
        super().__init__(lastname, firstname, phone, mail)
        self.company = company
        self.address = address
        self.company_phone = company_phone
        
    def __repr__(self):
        return f"{super().__repr__()}\nCompany: {self.company}\nAddress: {self.address}\nCompany Phone: {self.company_phone}"
    
    
class Dev(Work):
    def __init__(self, lastname, firstname, phone, mail, company, address, company_phone, languages, preferred_stack):
        super().__init__(lastname, firstname, phone, mail, company, address, company_phone)
        self.languages = languages
        self.preferred_stack = preferred_stack
        
    def __repr__(self):
        return f"{super().__repr__()}\nLanguages: {', '.join(self.languages)}\nPreferred Stack: {self.preferred_stack}"


class Contacts:
    def __init__(self):
        self.contacts = []
        
    def __repr__(self):
        return "\n\n".join(str(contact) for contact in self.contacts)
    
    def add_contact(self, contact):
        self.contacts.append(contact)
        
    def search_contact(self, lastname, firstname=None):
        matching_contacts = [contact for contact in self.contacts if contact.lastname == lastname]
        if firstname is not None:
            matching_contacts = [contact for contact in matching_contacts if contact.firstname == firstname]
        return matching_contacts
    
    def remove_contact(self, lastname, firstname):
        matching_contacts = self.search_contact(lastname, firstname)
        if len(matching_contacts) == 0:
            print(f"No contact found with lastname '{lastname}' and firstname '{firstname}'.")
        elif len(matching_contacts) == 1:
            self.contacts.remove(matching_contacts[0])
            print(f"Contact '{firstname} {lastname}' has been removed from the address book.")
        else:
            print(f"Multiple contacts found with lastname '{lastname}' and firstname '{firstname}'. Please specify the contact to remove.")
```
