from Person import Person
from Work import Work
from Dev import Dev

class Contacts():
	def __init__(self):
		self.contactsList = []

	def add(self,contact: Person):
		self.contactsList.append(contact)

	def del_contact(self,lastname,firstname):
		newContactsList = []
		for contact in self.contactsList:
			if contact.firstname == firstname and contact.lastname == lastname :
				print(f"Your contact {firstname},{lastname} has been deleted")
			else:
				newContactsList.append(contact)
		self.contactsList = newContactsList
		return self.contactsList

	def get_contact(self,lastname,firstname=None):
		tempContact = []
		if firstname:
			for contact in self.contactsList:
				if  contact.firstname == firstname and contact.lastname == lastname:
					tempContact.append(contact)
		else:
			for contact in self.contactsList:
				if contact.lastname == lastname:
					tempContact.append(contact)
		return tempContact

	def get_all(self):
		return self.contactsList

	def __repr__(self):
		return str(self.contactsList)


app = Contacts()

people_to_add =[
Person("John","Doe","06XX","john.doe@mail.com"),
Work("John","Do","06XX","john.doe@mail.com","company","somewhere on earth","04XX"),
Dev("Pat","L'étoile","06XX","pat@etoile.com","Crosoft","somewhere else","04XX",["Python","C","Rust"],"backend")]

app.add(people_to_add[0])
app.add(people_to_add[1])
app.add(people_to_add[2])
print(app)