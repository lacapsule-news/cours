from Work import Work

class Dev(Work):
	def __init__(self,lastname:str,firstname:str,phone:str,mail:str,company:str,address:str,company_phone:str,languages:list,preferred_stack:str):
		Work.__init__(self,lastname,firstname,phone,mail,company,address,company_phone)
		self.languages = languages
		self.preferred_stack = preferred_stack

	def __repr__(self):
		return f"Nom : {self.lastname}.\nPrenom : {self.firstname}.\nNuméro : {self.phone}.\nMail : {self.mail}.\nCompany : {self.company}.\nAddresse : {self.address}.\nNuméro profesionnel : {self.company_phone}.\nLanguage : {self.languages}.\nStack : {self.preferred_stack}.\n"