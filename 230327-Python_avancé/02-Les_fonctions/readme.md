# Les fonctions

- Créez une fonction login qui renverra un message sous la forme "X utilisateur(s) connecté(s)" où X représente le nombre d’utilisateurs stockés dans la variable connected.


- Modifiez la fonction login afin qu’elle réceptionne les arguments mail et pwd représentant des chaînes de caractères associées à une adresse mail et à un mot de passe. La fonction devra ajouter les valeurs de mail et pwd dans la liste connected en respectant les labels des dictionnaires déjà présents. 

```
connected = [
    {
        "email": "bot@lacapsule.academy",
        "password": "bot12345"
    },
    {
        "email": "test@lacapsule.academy",
        "password": "test12345"
    },
    {
        "email": "admin@lacapsule.academy",
        "password": "azerty"
    }
]

def login(mail, pwd):
    connected.append({ "email": mail, "password": pwd })
    return str(len(connected)) + " utilisateur(s) connecté(s)"
```