class Range():
	def __init__(self,start,end,interval=1):
		self.output = []
		if start < end and interval > 0:
			while start != end or start > end:
				self.output.append(start)
				start += interval
		elif start > end and interval < 0:
			while start != end or start < end:
				self.output.append(start)
				start += interval
		else:
			self.output.append("incorrect param")

	def get_range(self):
		return self.output

	def __repr__(self):
		return repr(self.output)

	def __str__(self):
		return self.output

app = Range(6,10,1)

print(app.get_range())

print(list(range(0,10,2)))