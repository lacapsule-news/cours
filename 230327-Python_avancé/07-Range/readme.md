# Range

## COMPTE AVEC MOI !

> L’objectif de ce challenge est de recréer simplement l’objet range natif à Python.


- Initialisez une classe Range, pouvant prendre 3 arguments : une borne inférieure, une borne supérieure et un pas (facultatif, par défaut, fixé à 1) permettant de générer une liste de nombres compris entre la borne inférieure (incluse) et la borne supérieure (exclue) avec le pas indiqué par l’utilisateur. L’affichage de la classe Range (print(Range(0,6)) devra retourner la liste générée. 

> Note : tout comme la fonction range en Python, l’utilisateur doit pouvoir indiquer comme paramètres une borne inférieure qui soit plus grande que la borne supérieure, avec un pas négatif, et l’objet sera renvoyé au bon format : Range(6, 0, -1) renverra [6, 5, 4, 3, 2, 1].


- Mettez en place une méthode reverse() permettant d’inverser l’ordre des éléments de l’objet Range. Par exemple : Range(0,4) renverra [0, 1, 2, 3] et Range(0, 4).reverse() renverra [3, 2, 1, 0].

```
class Range():
	def __init__(self,start,end,interval=1):
		self.output = []
		if start < end and interval > 0:
			while start != end or start > end:
				self.output.append(start)
				start += interval
		elif start > end and interval < 0:
			while start != end or start < end:
				self.output.append(start)
				start += interval
		else:
			self.output.append("incorrect param")

	def get_range(self):
		return self.output

	def __repr__(self):
		return repr(self.output)

	def __str__(self):
		return self.output

app = Range(6,10,1)

print(app.get_range())

print(list(range(0,10,2)))
```