# Les classes

## Définir une classe

- Créez une classe Shape ayant comme attributs color à "red" et name à "triangle". 

```
# Insert your code here
class Shape():
    color="red"
    name="triangle"


# End of code insertion

s = Shape()
my_shape_color = s.color
my_shape_name = s.name
```

## Instance d'objet

- Créez une instance de la classe Shape et stockez-la dans une variable s1. En utilisant la notation pointée, stockez dans une variable my_shape_name le contenu de l’attribut name de l’objet stocké dans s1.


- En utilisant la notation pointée, stockez dans une variable my_shape_color le contenu de l’attribut color de l’objet stocké dans s1.


- En utilisant la notation pointée, mettez à jour le contenu de l’attribut color de l’objet stocké dans s1 en lui affectant la valeur "blue". Stockez dans une variable my_new_color le nouveau contenu de l’attribut color de s1 et assurez-vous que la valeur a bien été mise à jour avec print.

```
class Shape:
    color = "red"
    name = "triangle"

class Rectangle:
    width = 10
    height = 30


s1 = Shape()
my_shape_name = s1.name
my_shape_color = s1.color
s1.color = "blue"
my_new_color = s1.color
```

## Méthodes

- Mettez en place la méthode area de la classe Shape qui retourne le produit de la largeur et de la hauteur de l’objet. Créez une instance de Shape nommée my_shape et stockez dans une variable nommée calculation la valeur de sa méthode area(). 


- Mettez en place la méthode message de la classe Shape, celle-ci renverra la chaîne de caractères "I am a beautiful rectangle" (où rectangle correspond à l’attribut name). Stockez dans une variable nommée talk_to_me le message de my_shape. Pensez à effacer vos prints pour valider l’exercice.


- Avec la notation pointée, modifiez le name de l’instance my_shape pour "square" , la largeur (width) pour 30. Stockez dans une variable talk_to_me_again appelez de nouveau message de l’instance my_shape. Pensez à effacer vos prints pour valider l’exercice.

```
class Shape:
    color = "red"
    name = "rectangle"
    width = 10
    height = 30
    
    def area(self):
        return self.width*self.height
    
    def message(self):
        return "I am a beautiful "+self.name


my_shape = Shape()
calculation = my_shape.area()
talk_to_me = my_shape.message()
my_shape.name = "square"
my_shape.width = 30

talk_to_me_again = my_shape.message()
```

## Le constructeur

- Mettez à jour votre classe Shape avec un constructeur permettant d’initialiser une instance en indiquant sa couleur et son nom.

- Créez deux instances de Shape dont la première représentera un carré rouge et la seconde un rectangle vert. Stockez-les respectivement dans des variables s et r. Attention à bien nommer vos valeurs en anglais et pensez à effacer vos prints pour valider l’exercice. 

- Modifiez le constructeur afin qu’il permette d’initialiser une instance Shape en indiquant également sa largeur et sa hauteur.
Supprimez les précédentes instances de Shape et créez deux nouvelles instances où la première représentera un rectangle bleu de dimensions 10x30 et la seconde un rectangle jaune de dimensions 20x50.

- Stockez-les respectivement dans les variables s et r. Attention à bien nommer vos valeurs en anglais.
Stockez les aires de ces deux rectangles dans deux variables area_s et area_r.

- Pensez à effacer vos prints pour valider l’exercice. 

```
class Shape:
    width = 10
    height = 30
    def __init__(self,color,name,width,height):
        self.color=color
        self.name=name
        self.height=height
        self.width=width
    
    def message(self):
        return f"I am a {self.color} {self.name}"
    
    def area(self):
        return self.width * self.height






s = Shape("blue","rectangle",10,30)
r = Shape("yellow","rectangle",20,50)

area_s =s.area()
area_r =r.area()









# Do not edit/remove code under this line
sc = s.color
sn = s.name
rc = r.color
rn = r.name
sw = s.width
sh = s.height
rw = r.width
rh = r.height
```