class Moto():
	"""docstring for Moto"""
	def __init__(self,brand,color):
		self.brand = brand
		self.color = color
		self.speed = 0
		self.pilot = ""

	def change_pilot(self,nom):
		self.pilot = nom
		return self.pilot
	def set_speed(self,rate,duration):
		if self.pilot != "":
			self.speed = float(rate)/float(duration)

	def __repr__(self):
		return f"Moto de la marque : {self.brand}.\nDe couleur : {self.color}.\nConduite par : {self.pilot}.\nAllant a une vitesse de :{self.speed}."


moto = Moto("Yamaha","Rouge")
moto.set_speed("10","0.1")
print(moto)

moto.change_pilot("Matthieu")
moto.set_speed("10","0.1")
print(moto)
