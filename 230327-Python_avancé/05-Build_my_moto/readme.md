# BUILD MY MOTO

## DÉFINITION D’UN OBJET

> L’objectif de ce challenge va être de créer un objet définissant une moto.


1. Initialisez une classe Moto(), permettant d’instancier des objets compte à partir d’attributs brand et color. L’objet disposera également d’attributs par défaut : pilot à "" (chaîne de caractères vide) et speed à 0 qui ne pourront pas être initialisés par l’utilisateur.


👉 Mettez en place trois méthodes :

change_pilot(self, pilot) : modifie le pilote sur la Moto.
set_speed(self, rate, duration) : définit la vitesse de la Moto (produit de rate et duration) si celle-ci dispose bien d’un pilote.
__repr__(self) : affiche les informations de la Moto.

```
class Moto():
	"""docstring for Moto"""
	def __init__(self,brand,color):
		self.brand = brand
		self.color = color
		self.speed = 0
		self.pilot = ""

	def change_pilot(self,nom):
		self.pilot = nom
		return self.pilot
	def set_speed(self,rate,duration):
		if self.pilot != "":
			self.speed = float(rate)/float(duration)

	def __repr__(self):
		return f"Moto de la marque : {self.brand}.\nDe couleur : {self.color}.\nConduite par : {self.pilot}.\nAllant a une vitesse de :{self.speed}."


moto = Moto("Yamaha","Rouge")
moto.set_speed("10","0.1")
print(moto)

moto.change_pilot("Matthieu")
moto.set_speed("10","0.1")
print(moto)

```