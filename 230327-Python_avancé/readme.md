# Python avancé

## Théorique

### Les fonctions et classes en python

#### Les fonctions

> Une fonction est un bloc de code que l'on va pouvoir réutiliser.

> Par exemple, on ecrit un fonction qui retourne un nombre random, a chaque fois que notre programme aura besoin d'un nombre random on aura juste a appeler la fonction

```
import random, math

def getRandomNumber(min,max):
	randomNumber = random.random() * (min-max)+min
	randomNumber = math.floor(randomNumber)

	return randomNumber
```

> Pour executer le code, il va falloir l'appeler par sont nom suivie des argument entre parenthese

`getRandomNumber(1-100)`


#### Les classes

> une classe est un modèle qui va servir a créer des objets. Elle possède des attributs avec des valeurs par défaut. Un objet hérite des attributs de sa classe et peut les personnaliser.


## Pratique

### 07 - Range

#### COMPTE AVEC MOI !

> L’objectif de ce challenge est de recréer simplement l’objet range natif à Python.


- Initialisez une classe Range, pouvant prendre 3 arguments : une borne inférieure, une borne supérieure et un pas (facultatif, par défaut, fixé à 1) permettant de générer une liste de nombres compris entre la borne inférieure (incluse) et la borne supérieure (exclue) avec le pas indiqué par l’utilisateur. L’affichage de la classe Range (print(Range(0,6)) devra retourner la liste générée. 

> Note : tout comme la fonction range en Python, l’utilisateur doit pouvoir indiquer comme paramètres une borne inférieure qui soit plus grande que la borne supérieure, avec un pas négatif, et l’objet sera renvoyé au bon format : Range(6, 0, -1) renverra [6, 5, 4, 3, 2, 1].


- Mettez en place une méthode reverse() permettant d’inverser l’ordre des éléments de l’objet Range. Par exemple : Range(0,4) renverra [0, 1, 2, 3] et Range(0, 4).reverse() renverra [3, 2, 1, 0].

```
class Range():
	def __init__(self,start,end,interval=1):
		self.output = []
		if start < end and interval > 0:
			while start != end or start > end:
				self.output.append(start)
				start += interval
		elif start > end and interval < 0:
			while start != end or start < end:
				self.output.append(start)
				start += interval
		else:
			self.output.append("incorrect param")

	def get_range(self):
		return self.output

	def __repr__(self):
		return repr(self.output)

	def __str__(self):
		return self.output

app = Range(6,10,1)

print(app.get_range())

print(list(range(0,10,2)))
```

### 06 - ADDRESS BOOK

#### STRUCTURE DU CARNET

> L’objectif de ce challenge est de créer un ensemble de classes permettant de construire un carnet d’adresses.


1. Initialisez une classe Person(), permettant d’instancier des objets à partir d’attributs lastname, firstname, phone et mail. L’objet disposera également de la méthode __repr__ pour imprimer les informations de la personne.
1. Initialisez une classe Work(), permettant d’instancier des objets à partir d’attributs lastname, firstname, phone, mail, company, address et company_phone. L’objet disposera également de la méthode __repr__ pour imprimer les informations de la personne et de son travail.
1. Initialisez une classe Dev(), permettant d’instancier des objets à partir d’attributs lastname, firstname, phone, mail, company, address, company_phone, languages (sous forme de liste) et preferred_stack ("backend" ou "frontend"). L’objet disposera également de la méthode __repr__ pour imprimer les informations de la personne, de son travail et de ses préférences de dev.

#### MISE EN PLACE DU CARNET

1. Initialisez une classe Contacts(), qui contiendra un ensemble d’objets sous forme de liste (des objets de Person, Work ou Dev, peu importe). Associez à Contacts une méthode __init__ ne prenant pas d’arguments, permettant simplement d’initialiser une instance, et une méthode __repr__ permettant d’afficher le contenu de l’instance sous forme de chaîne de caractères.
1. Ajoutez à votre classe Contacts une méthode add_contact permettant d’ajouter un contact à partir d’une instance de Person, une méthode search_contact permettant de trouver un contact à partir de son nom (argument obligatoire) et de son prénom (argument optionnel) si plusieurs contacts ont le même nom dans le carnet d’adresses, une méthode remove_contact permettant de supprimer un contact du carnet en connaissant son nom et son prénom.

```
class Person:
    def __init__(self, lastname, firstname, phone, mail):
        self.lastname = lastname
        self.firstname = firstname
        self.phone = phone
        self.mail = mail
        
    def __repr__(self):
        return f"{self.firstname} {self.lastname}\nPhone: {self.phone}\nEmail: {self.mail}"
    
    
class Work(Person):
    def __init__(self, lastname, firstname, phone, mail, company, address, company_phone):
        super().__init__(lastname, firstname, phone, mail)
        self.company = company
        self.address = address
        self.company_phone = company_phone
        
    def __repr__(self):
        return f"{super().__repr__()}\nCompany: {self.company}\nAddress: {self.address}\nCompany Phone: {self.company_phone}"
    
    
class Dev(Work):
    def __init__(self, lastname, firstname, phone, mail, company, address, company_phone, languages, preferred_stack):
        super().__init__(lastname, firstname, phone, mail, company, address, company_phone)
        self.languages = languages
        self.preferred_stack = preferred_stack
        
    def __repr__(self):
        return f"{super().__repr__()}\nLanguages: {', '.join(self.languages)}\nPreferred Stack: {self.preferred_stack}"


class Contacts:
    def __init__(self):
        self.contacts = []
        
    def __repr__(self):
        return "\n\n".join(str(contact) for contact in self.contacts)
    
    def add_contact(self, contact):
        self.contacts.append(contact)
        
    def search_contact(self, lastname, firstname=None):
        matching_contacts = [contact for contact in self.contacts if contact.lastname == lastname]
        if firstname is not None:
            matching_contacts = [contact for contact in matching_contacts if contact.firstname == firstname]
        return matching_contacts
    
    def remove_contact(self, lastname, firstname):
        matching_contacts = self.search_contact(lastname, firstname)
        if len(matching_contacts) == 0:
            print(f"No contact found with lastname '{lastname}' and firstname '{firstname}'.")
        elif len(matching_contacts) == 1:
            self.contacts.remove(matching_contacts[0])
            print(f"Contact '{firstname} {lastname}' has been removed from the address book.")
        else:
            print(f"Multiple contacts found with lastname '{lastname}' and firstname '{firstname}'. Please specify the contact to remove.")
```


### 05 - BUILD MY MOTO

#### DÉFINITION D’UN OBJET

> L’objectif de ce challenge va être de créer un objet définissant une moto.


1. Initialisez une classe Moto(), permettant d’instancier des objets compte à partir d’attributs brand et color. L’objet disposera également d’attributs par défaut : pilot à "" (chaîne de caractères vide) et speed à 0 qui ne pourront pas être initialisés par l’utilisateur.


👉 Mettez en place trois méthodes :

change_pilot(self, pilot) : modifie le pilote sur la Moto.
set_speed(self, rate, duration) : définit la vitesse de la Moto (produit de rate et duration) si celle-ci dispose bien d’un pilote.
__repr__(self) : affiche les informations de la Moto.

```
class Moto():
	"""docstring for Moto"""
	def __init__(self,brand,color):
		self.brand = brand
		self.color = color
		self.speed = 0
		self.pilot = ""

	def change_pilot(self,nom):
		self.pilot = nom
		return self.pilot
	def set_speed(self,rate,duration):
		if self.pilot != "":
			self.speed = float(rate)/float(duration)

	def __repr__(self):
		return f"Moto de la marque : {self.brand}.\nDe couleur : {self.color}.\nConduite par : {self.pilot}.\nAllant a une vitesse de :{self.speed}."


moto = Moto("Yamaha","Rouge")
moto.set_speed("10","0.1")
print(moto)

moto.change_pilot("Matthieu")
moto.set_speed("10","0.1")
print(moto)

```

### 04 - Bank Account

#### DÉFINITION D’UN OBJET

> L’objectif de ce challenge est de créer un objet permettant de définir simplement le compte bancaire d’un utilisateur.


1. Rendez-vous sur le site Replit afin d’exécuter du code Python hors d’Ariane.
1. Initialisez une classe CompteBancaire(), permettant d’instancier des objets à partir des attributs nom (string) et solde (int).
1. Mettez en place trois méthodes :
	- depot(self, somme) : ajoute la somme indiquée au solde.
	- retrait(self, somme) : retire la somme indiquée au solde.
	- __repr__(self) : affiche le nom du titulaire et du solde du compte. N’hésitez pas à regarder sur internet pour comprendre l'intérêt et l’importance de cette méthode.

> Par exemple : print(CompteBancaire("Vanessa", 2000)) affichera dans la console : "Compte bancaire de Vanessa : 2000 euros."

```
class CompteBancaire():
	def __init__(self,nom: str,solde: int) -> None:
		self.nom = nom
		self.solde= solde

	def depot(self,somme:int) -> int:
		self.solde += somme
		return self.solde

	def retrait(self,somme:int) -> int:
		self.solde -= somme
		return self.solde

	def __repr__(self):
		return  "Compte bancaire de "+str(self.nom)+" : "+str(self.solde)+" euros."

user = CompteBancaire("Matthieu",-2)
print(user)
user.depot(300)
print(user)
print(user.retrait(98))
```

### 03 - Les classes

#### Définir une classe

- Créez une classe Shape ayant comme attributs color à "red" et name à "triangle". 

```
# Insert your code here
class Shape():
    color="red"
    name="triangle"


# End of code insertion

s = Shape()
my_shape_color = s.color
my_shape_name = s.name
```

#### Instance d'objet

- Créez une instance de la classe Shape et stockez-la dans une variable s1. En utilisant la notation pointée, stockez dans une variable my_shape_name le contenu de l’attribut name de l’objet stocké dans s1.


- En utilisant la notation pointée, stockez dans une variable my_shape_color le contenu de l’attribut color de l’objet stocké dans s1.


- En utilisant la notation pointée, mettez à jour le contenu de l’attribut color de l’objet stocké dans s1 en lui affectant la valeur "blue". Stockez dans une variable my_new_color le nouveau contenu de l’attribut color de s1 et assurez-vous que la valeur a bien été mise à jour avec print.

```
class Shape:
    color = "red"
    name = "triangle"

class Rectangle:
    width = 10
    height = 30


s1 = Shape()
my_shape_name = s1.name
my_shape_color = s1.color
s1.color = "blue"
my_new_color = s1.color
```

#### Méthodes

- Mettez en place la méthode area de la classe Shape qui retourne le produit de la largeur et de la hauteur de l’objet. Créez une instance de Shape nommée my_shape et stockez dans une variable nommée calculation la valeur de sa méthode area(). 


- Mettez en place la méthode message de la classe Shape, celle-ci renverra la chaîne de caractères "I am a beautiful rectangle" (où rectangle correspond à l’attribut name). Stockez dans une variable nommée talk_to_me le message de my_shape. Pensez à effacer vos prints pour valider l’exercice.


- Avec la notation pointée, modifiez le name de l’instance my_shape pour "square" , la largeur (width) pour 30. Stockez dans une variable talk_to_me_again appelez de nouveau message de l’instance my_shape. Pensez à effacer vos prints pour valider l’exercice.

```
class Shape:
    color = "red"
    name = "rectangle"
    width = 10
    height = 30
    
    def area(self):
        return self.width*self.height
    
    def message(self):
        return "I am a beautiful "+self.name


my_shape = Shape()
calculation = my_shape.area()
talk_to_me = my_shape.message()
my_shape.name = "square"
my_shape.width = 30

talk_to_me_again = my_shape.message()
```

#### Le constructeur

- Mettez à jour votre classe Shape avec un constructeur permettant d’initialiser une instance en indiquant sa couleur et son nom.

- Créez deux instances de Shape dont la première représentera un carré rouge et la seconde un rectangle vert. Stockez-les respectivement dans des variables s et r. Attention à bien nommer vos valeurs en anglais et pensez à effacer vos prints pour valider l’exercice. 

- Modifiez le constructeur afin qu’il permette d’initialiser une instance Shape en indiquant également sa largeur et sa hauteur.
Supprimez les précédentes instances de Shape et créez deux nouvelles instances où la première représentera un rectangle bleu de dimensions 10x30 et la seconde un rectangle jaune de dimensions 20x50.

- Stockez-les respectivement dans les variables s et r. Attention à bien nommer vos valeurs en anglais.
Stockez les aires de ces deux rectangles dans deux variables area_s et area_r.

- Pensez à effacer vos prints pour valider l’exercice. 

```
class Shape:
    width = 10
    height = 30
    def __init__(self,color,name,width,height):
        self.color=color
        self.name=name
        self.height=height
        self.width=width
    
    def message(self):
        return f"I am a {self.color} {self.name}"
    
    def area(self):
        return self.width * self.height






s = Shape("blue","rectangle",10,30)
r = Shape("yellow","rectangle",20,50)

area_s =s.area()
area_r =r.area()









# Do not edit/remove code under this line
sc = s.color
sn = s.name
rc = r.color
rn = r.name
sw = s.width
sh = s.height
rw = r.width
rh = r.height
```

### 02 - Les fonctions

- Créez une fonction login qui renverra un message sous la forme "X utilisateur(s) connecté(s)" où X représente le nombre d’utilisateurs stockés dans la variable connected.


- Modifiez la fonction login afin qu’elle réceptionne les arguments mail et pwd représentant des chaînes de caractères associées à une adresse mail et à un mot de passe. La fonction devra ajouter les valeurs de mail et pwd dans la liste connected en respectant les labels des dictionnaires déjà présents. 

```
connected = [
    {
        "email": "bot@lacapsule.academy",
        "password": "bot12345"
    },
    {
        "email": "test@lacapsule.academy",
        "password": "test12345"
    },
    {
        "email": "admin@lacapsule.academy",
        "password": "azerty"
    }
]

def login(mail, pwd):
    connected.append({ "email": mail, "password": pwd })
    return str(len(connected)) + " utilisateur(s) connecté(s)"
```

### 01 - Les fonctions et classes en python

#### Les fonctions

> Une fonction est un bloc de code que l'on va pouvoir réutiliser.

> Par exemple, on ecrit un fonction qui retourne un nombre random, a chaque fois que notre programme aura besoin d'un nombre random on aura juste a appeler la fonction

```
import random, math

def getRandomNumber(min,max):
	randomNumber = random.random() * (min-max)+min
	randomNumber = math.floor(randomNumber)

	return randomNumber
```

> Pour executer le code, il va falloir l'appeler par sont nom suivie des argument entre parenthese

`getRandomNumber(1-100)`


#### Les classes

> une classe est un modèle qui va servir a créer des objets. Elle possède des attributs avec des valeurs par défaut. Un objet hérite des attributs de sa classe et peut les personnaliser.

