# Awesome media server

## AWESOME MEDIA SERVER

Bienvenue à ce hackathon dédié à la mise en place d’un serveur multimédia via Docker 🔥

L’objectif de ce hackathon est de créer un environnement constitué de plusieurs applications communiquant entre elles afin de pouvoir facilement télécharger des films (libres de droit, évidemment 🏴‍☠️) et de les visionner en streaming.

Vous travaillerez chacun sur votre machine, mais vous avancerez en groupe et au même rythme afin d’avoir le même résultat à la fin de la journée.

👉 Créez un dossier "media-server" qui contiendra toutes les ressources du projet, telles que le fichier "docker-compose.yml".

👉 Avant de vous lancer dans la création et la configuration des services, prenez le temps d’analyser le sujet avec votre groupe afin de comprendre le but du projet et le rôle de chaque application.

Tout au long du hackathon, vous devrez vous assurer que les données de chaque application (configuration, préférences, fichiers multimédias…) soient préservées, y compris lors de la suppression des conteneurs.

## JELLYFIN

Jellyfin est une application conçue pour organiser, partager et visionner des fichiers multimédias tels que des films sur des appareils en réseau.

👉 Installez et configurez la solution Jellyfin dans un conteneur Docker.

👉 Vérifiez votre installation en téléchargeant manuellement le film suivant et en le visionnant en streaming : https://archive.org/download/BigBuckBunny_124/Content/big_buck_bunny_720p_surround.mp4 

## DELUGE

Deluge est un client BitTorrent web permettant le partage de fichier en peer-to-peer (P2P) via le protocole torrent, souvent utilisé pour le téléchargement de films et de séries.


👉 Installez et configurez la solution Deluge dans un conteneur Docker.


👉 Vérifiez votre installation en initiant le téléchargement d’un film via le fichier torrent suivant : https://archive.org/download/bloodsport-1988/bloodsport-1988_archive.torrent


👉 Faîtes en sorte que les fichiers téléchargés par Deluge soient automatiquement importés et visionnables sur Jellyfin.

## JACKETT

Jackett est un moteur de recherche capable de trouver des fichiers multimédias auprès de plus de 500 trackers afin de les télécharger via un client BitTorrent tel que Deluge.


👉 Installez et configurez la solution Jackett dans un conteneur Docker.


👉 Ajoutez l’indexer "Internet Archive" et recherchez le film "The Birds" d’Alfred Hitchcock afin de récupérer un fichier torrent.


👉 Téléchargez le film précédent recherché via Deluge.
Quelques minutes après le téléchargement, le film est censé être automatiquement importé et visionnable sur Jellyfin.

```
---
version: "3.3"
services:
  jellyfin:
    image: lscr.io/linuxserver/jellyfin:latest
    container_name: jellyfin
    environment:
      - PUID=1000
      - PGID=1000
      - TZ=Europe/Paris
    volumes:
      - ./jellyfin/lib:/config
      - ./jellyfin/series:/data/tvshows
      - ./jellyfin/movies:/data/movies
      - /opt/vc/lib:/opt/vc/lib
    ports:
      - 8096:8096
    restart: unless-stopped
  
  deluge:
    image: lscr.io/linuxserver/deluge:latest
    container_name: deluge
    environment:
      - PUID=1000
      - PGID=1000
      - TZ=Europe/Paris
      - DELUGE_LOGLEVEL=error #optional
    volumes:
      - ./deluge:/config
      - ./jackett/torrents:/downloads/torrents
      - ./jellyfin/movies:/downloads/movies
      - ./jellyfin/series:/downloads/series
    ports:
      - 8112:8112
      - 6881:6881
      - 6881:6881/udp
    restart: unless-stopped
  
  jackett:
    image: lscr.io/linuxserver/jackett:latest
    container_name: jackett
    environment:
      - PUID=1000
      - PGID=1000
      - TZ=Europe/Paris
      - AUTO_UPDATE=true #optional
    volumes:
      - ./jackett/config:/config
      - ./jackett/torrents:/downloads
    ports:
      - 9117:9117
    restart: unless-stopped
```