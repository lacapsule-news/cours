# Gitlab flow

## AUTHENTIFICATION PAR CLÉ SSH

En accédant à votre dashboard GitLab, vous avez sans doute remarqué un bandeau vous invitant à mettre en place la connexion par clés SSH.

Pour vous authentifier, vous allez créer une paire de clés SSH afin que votre client Git en local puisse communiquer avec votre compte GitLab de façon sécurisée. GitLab recommande l’usage de clés de type ED25519.

1. 👉 ssh-keygen -t ed25519 -C "GitLab Key Pair"
1. 👉 Repérez l’emplacement de vos clés SSH.
1. 👉 Copiez le contenu de votre clé publique.
1. 👉 Entrez votre clé publique sur la page "User Settings" > "SSH Keys".
1. 👉 Vérifiez que l’authentification SSH fonctionne grâce à la commande suivante.

ssh -T git@gitlab.com

GitLab vous renvoie un message de bienvenue : vous êtes désormais capables de vous authentifier via SSH depuis votre machine en ligne de commande.

## CRÉATION D’UN GROUPE SUR GITLAB
 

1. 👉 Créez un nouveau groupe privé sur GitLab.
1. 👉 Invitez votre buddy au sein de ce groupe.
1. 👉 Créez un nouveau projet privé dans ce groupe. ("Blank Project" avec fichier README)

## CRÉATION D’UN TICKET SUR GITLAB

- 👉 Créez un ticket (issue) pour proposer une modification du fichier README
- 👉 Utilisez la syntaxe MarkDown pour styliser et hiérarchiser votre description. Nous utilisons ici le symbole * pour créer un point de liste.

## GITLAB FLOW

Le GitLab flow commence par la création d’un ticket (issue). Un ticket définit la portée du travail à accomplir.

- 👉 Éditez le ticket pour préciser quelles modifications vous souhaitez faire sur le fichier README. Utilisez à nouveau la syntaxe MarkDown pour créer une checkbox.

GitLab a ainsi créé une checklist et suit l’avancement de votre projet.

- 👉 Depuis l’interface GitLab, dans l’onglet repository, créez une nouvelle branche nommée production.
Après avoir créé la branche, vous êtes automatiquement dirigé vers la nouvelle branche.


- 👉 Dans l’onglet Branches de votre repository sont désormais listées les deux branches :

main :  la branche par défaut, protégée (seuls les utilisateurs qui ont certaines permissions peuvent pusher directement sur cette branche)
production

- 👉 Ajoutez une protection sur la branche production pour que seuls les utilisateurs disposant de certaines permissions puissent pusher sur cette branche (qui constitue l’environnement de production dans le flow GitLab).
Seuls les utilisateurs ayant le statut "Maintainer" pourront pusher sur cette branche.


## CLONER LA CODEBASE EN LOCAL

Afin de modifier le fichier "README.md", vous allez cloner le projet sur votre machine.

- 👉 Copiez l’URL SSH de votre repository.
- 👉 Collez l’URL dans votre terminal, précédée de la commande git clone.
- 👉 Affichez le contenu de votre repository en local. Il contient désormais le fichier Readme.md (ainsi que le dossier caché .git qui prouve que nous sommes bien dans un repository git)
- 👉 Créez une branche nommée readme-introduction, et positionnez-vous sur cette branche.
- 👉 Ouvrez le fichier Readme.md avec nano et ajoutez une brève introduction en utilisant la syntaxe MD comme suit.
- 👉 Committez vos changements.

## CRÉATION D’UNE MERGE REQUEST

- 👉 Synchronisez les changements effectués en local dans le repository sur GitLab.

`git push -u origin readme-introduction`

- 👉 Actualisez la page de votre repository sur GitLab, vérifiez que la branche readme-introduction et les modifications sont bien présentes.

Suite à votre push, un bandeau vous invite à créer une merge request.
Une merge request constitue l’action de demander à un membre du groupe de fusionner une branche dans une autre.


- 👉 Cliquez sur le bouton "Create merge request", ajoutez une courte description des changements apportés. Conservez l’option qui supprimera la branche dès que le merge est effectué.
Rappelez-vous, une branche pour une feature a une durée de vie courte, contrairement à la branche principale et à la branche production.


- 👉 Dans l’onglet Changes de la merge request, ajoutez un commentaire invitant à ajouter plus d’informations concernant ses hobbys.

- 👉 Faites les modifications nécessaires en local puis un git push vers le dépôt distant (sans -u puisque la branche readme-introduction existe déjà sur GitLab).


- 👉 Retournez sur GitLab pour terminer la review de la merge request. Procédez au merge et supprimez la branche readme-introduction. Retournez sur la page principale de votre repository : les changements ont bien été enregistrés sur la branche main.

## MERGE DE MAIN DANS PRODUCTION

Rappelez-vous, dans le GitLab Flow, la branche main joue le rôle de branche de pré-production. Il est temps de merger la branche main dans la branche production.


- 👉 Créez une seconde merge mequest comme suit :

- branche source : main

- branche de destination : production 


👉 Validez ensuite directement la merge request.


## FINALISEZ VOTRE PREMIÈRE RELEASE

La V1 de votre projet est prête !


- 👉 Dans l’onglet "Tags" de votre repository, créez depuis la branche production un nouveau tag que vous nommerez "v1.0".

Vous retrouvez cette release dans l’onglet Deployments. Elle contient notamment votre codebase disponible au téléchargement.

- 👉 Revenez sur votre machine en local, positionnez-vous sur la branche main (la branche readme-introduction n’existe plus sur le dépôt distant).


- 👉 Supprimez la version locale de la branche readme-introduction.

- 👉 Vous pouvez fermer le ticket créé en début de challenge. Retournez sur le ticket, cochez la checkbox, cliquez sur "Close Issue".