# Gitlab basics

## CRÉER UN REPOSITORY SUR GITLAB

Pour conserver vos versions sur le cloud ou tout simplement pour partager votre travail avec votre équipe, il va être essentiel de créer un dépôt distant.

C’est ce que proposent des services Web d'hébergement comme GitHub, Bitbucket ou GitLab en mettant à disposition l’url de ce dépôt distant.


- 👉 Sur votre machine, créez un nouveau dossier "gitlabbasics" dans lequel vous initialisez un dépôt Git local.


- 👉 Créez un fichier "ReadMe.txt" puis faites un premier commit.


- 👉 Assurez-vous que la branche principale s’appelle "main". Si ce n’est pas le cas, exécutez la commande adéquate pour renommer la branche principale.


- 👉 Connectez-vous à GitLab et cliquez sur le bouton "+" > New Project/Repository. Créez un nouveau projet (Modèle Blank) que vous nommerez "gitlabbasics".

## SE CONNECTER À UN DÉPÔT DISTANT

À chaque commit effectué, vous enregistrez vos modifications dans votre dépôt local. Une fois votre dépôt distant créé, plusieurs options s’offrent à vous. Nous allons déposer (push) notre dépôt local créé au préalable dans ce dépôt distant.

GitLab vous propose la série de commandes adaptée à ce cas :

- 👉 Le dépôt local étant déjà créé, exécutez la commande suivante à la racine de votre projet.

`git remote add origin git@gitlab.com:lacapsule5051914/gitlabbasics.git`

La commande "remote" suivie de "add" permet d’associer votre dépôt à un autre dépôt.

"origin" est le nom que vous souhaitez donner à ce dépôt distant. Vous êtes libre de donner le nom que vous souhaitez, par convention on le nomme "origin".

L’url correspond à l’url du dépôt distant. Elle est fournie par GitLab après la création de votre dépôt distant.


Ici, nous relions notre dépôt local vers ce dépôt distant que nous nommons "origin". Vous pouvez désormais lister tous les dépôts distants liés à votre dépôt local.

`git remote -v`

## ENVOYER DES MODIFICATIONS

- 👉 Maintenant que la liaison a bien été créée, nous allons vouloir envoyer nos modifications sur le dépôt distant. Dans quel but ? Pour avoir une sauvegarde sur internet de notre projet et de son historique, mais également pour pouvoir partager ce projet avec d’autres développeurs.

Pour rappel, grâce à la commande "commit", nous avons enregistré des versions dans notre dépôt local. Ces différentes versions vont pouvoir être envoyées (push) vers un dépôt distant.

`git push origin main`

Actualisez la page de votre dépôt sur GitLab. Votre projet est désormais initialisé et le premier commit apparaît.

## RÉCUPÉRER LES MODIFICATIONS

Vous souhaitez importer les modifications sur un autre poste en local ou vous travaillez en équipe et souhaitez récupérer les modifications faites par les développeurs de votre équipe ? Git met à disposition une commande pour récupérer ces informations distantes dans votre dépôt local.

Avant de réellement travailler en équipe, utilisons une fonctionnalité supplémentaire de GitLab : l’édition de code et la création de commit en ligne dans le navigateur.

- 👉 Dans l’IDE en ligne de GitLab, modifiez le fichier ReadMe.txt en ajoutant une ligne de texte. Enregistrez les modifications en créant un commit, toujours depuis le navigateur.

Cette modification est donc bien enregistrée au sein de votre dépôt distant. Notez que le fichier ReadMe.txt est un fichier un peu spécifique pour la plupart des plateformes d’hébergement de dépots distants tels que GitLab ou GitHub. Le contenu de ce fichier est affiché automatiquement sur la page d’accueil du repository. Il contient généralement des informations importantes au sujet du projet, que l’on souhaite partager avec les utilisateurs ou des développeurs amenés à travailler avec nous.

- 👉 Revenez sur votre machine et récupérez maintenant cette modification sur votre dépôt local.

`git pull origin main`

La commande "pull" permet de récupérer les versions (commit) qui n’étaient pas encore présentes dans notre dépôt. Une fois le git pull effectué, la commande git log fait apparaître le commit que vous avez créé sur l’IDE en ligne.