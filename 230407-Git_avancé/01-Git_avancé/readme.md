# Git avancé

## Contexte

Lorsqu'on veut ajouter de nouvelles fonctionnalités à un projet, on va préférer travailler sur une copie du projet pour ne pas impacter le projet d'origine.

La finalité d'un version finalisée est de passer en production.

Il est necessaire d'avoir un procédure permettant de garantir la fiabilité et la stabilité du développement avant le passage en production.

## Fonctionnement

Les branches permettent de travailler sur une copie d'un projet sans en impacté le projet d'origine.

Les modifications (commit) se feront alors sur la copie du projet.

Lorsque le développement est terminé sur la copie, les commits doivent être rapatriés sur le projet d'origine.

Pour garantir le contrôle et la fiabilité des développements, une organisation de travail doit être mise en place avec des droits pour chaque membre de l'équipe.

### Step by step

- Création d'un branch

`git branch translation`

- Changement de branch

`git checkout translation`

Le commit s'applique uniquement sur la branch active.

- Rapatrier le travail
    - Sélectioner la branch principale
    `git checkout main`
    - Fusionner les commits
    `git merge translation`
    - Suppression de la branche
    `git branch -d translation`