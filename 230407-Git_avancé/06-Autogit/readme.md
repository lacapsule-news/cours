# Autogit

## README TEMPLATING

Les commandes Git sont parfois complexes et répétitives… Vous allez user de vos skills en scripting pour automatiser certaines opérations !

- 👉 Récupérez la ressource "autogit.zip" depuis l’onglet dédié sur Ariane.

- 👉 Créez un dossier dans lequel vous initialisez un repository Git local.

- 👉 Créez un fichier README.md et copiez le contenu du fichier README fourni.

- 👉 En ligne de commande, sans utiliser l’interface web de GitLab, créez deux repositories sur GitLab (sans fichier README) : https://docs.gitlab.com/ee/user/project/working_with_projects.html

- 👉 Connectez et poussez votre dépôt local sur chacun de ces deux repositories. Vérifiez que le template du README s’affiche correctement. N’hésitez pas à le personnaliser pour la suite du challenge !

- 👉 Créez une branche appelée feature et positionnez-vous sur cette branche.

## BASH SCRIPTING

- 👉 Imaginez maintenant un script bash qui automatise toutes ces étapes depuis l’initialisation du repository local jusqu’au premier push sur les deux dépôts distants. Il sera particulièrement pratique si vous souhaitez démarrer un nouveau projet sur deux dépôts distants (ou plus).

Au lancement du script le prompt vous invitera à entrer successivement les noms des 2 repositories distants. Le script nommera la branche principale main et le prompt demandera également le nom de la première feature branche que vous souhaitez créer pour commencer à travailler sur une fonctionnalité.

Une fois le script exécuté, vous retrouverez sur GitLab deux repositories avec 2 branches et un fichier README qui contiendra le template fourni dans le setup.

- 👉 Pour aller plus loin, modifiez votre script pour qu’il inscrive dynamiquement dans le README :
    - Le nom du contributeur principal (vous)
    - Le nom du projet
    - Les principales technos utilisées

- 👉 Ajoutez la possibilité de créer jusqu’à 5 branches (et noms associés).

`git push --set-upstream git@gitlab.com:lacapsule5051914/autogit.git main`
`git push --set-upstream git@gitlab.com:lacapsule5051914/autogitx.git main`

```
user=$(git config --global user.name)

mkdir $1
cd $1
git init
touch README.md
echo $user > README.md
echo '' >> README.md
echo $1 >> README.md
echo '' >> README.md
echo 'Created with bash and git' >> README.md
git add .
git commit -m 'first commit'
git push --set-upstream git@gitlab.com:lacapsule5051914/$1.git main
read -p 'Votre branche de dev ?' branch
git checkout -b $branch
```