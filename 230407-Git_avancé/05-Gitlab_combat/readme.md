# GITLAB COMBAT

## CONFLITS AVEC GIT

Il est maintenant temps d’appliquer le principe de GitLab Flow en conditions réelles de travail d’équipe. Vous allez mobiliser votre buddy et gérer une série de conflits sur un projet commun.


- 👉 Créez un nouveau projet sur GitLab et invitez votre buddy.

- 👉 Commitez un fichier quelconque contenant quelques lignes de texte.

- 👉 Créez volontairement une situation de conflit en modifiant exactement la même ligne de code (chacun de votre côté) et en poussant vers la branche principale du dépôt distant.

- 👉 Résolvez le conflit comme vu dans le challenge "Branch Theft Auto"

- 👉 Créez un nouveau projet sur GitLab et inversez les rôles : la personne qui a provoqué le conflit est maintenant censé le régler 😉