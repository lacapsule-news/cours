# BRANCH THEFT AUTO

## DÉCOUVERTE DES BRANCHES

Avant Git, quand on avait un projet qui nécessitait de développer une nouvelle version, il était souvent nécessaire de faire une copie complète du projet pour pouvoir travailler dessus sereinement.

Cela obligeait à travailler sur plusieurs versions en parallèle. Les deux projets devenant indépendants, il était compliqué de répercuter les modifications faites sur un des environnements sur l’autre. Le travail était fastidieux, on risquait alors de perdre des fonctionnalités.

Lorsque la nouvelle version était finalisée, il fallait alors remplacer l’ancienne par la nouvelle. Là encore, les risques d’erreur étaient importants.


Créer une branche, c’est en quelque sorte comme créer une “copie” de votre projet pour développer et tester de nouvelles fonctionnalités sans impacter le projet de base. Git rend la création de nouvelles branches et la fusion de branche très facile à réaliser.

Une branche, dans Git, est simplement un pointeur vers un commit. La fusion de deux branches constitue un commit.


Cet outil en ligne permet de visualiser la création de commits et de branches : https://onlywei.github.io/explain-git-with-d3/


- 👉 Cliquez sur git branch et expérimentez avec la création de commits et de branches (cf. commandes sur le screenshot ci-dessous).

## INITIALISATION D’UN REPOSITORY

- 👉 Créez un dossier "comparator" dans lequel vous initialisez un dépôt Git.

`git init`

La branche par défaut s’appelle master. Cette branche master va se déplacer automatiquement à chaque nouveau commit pour pointer sur le dernier commit effectué tant qu’on reste sur cette branche.

Pour rappel, l’un des intérêts de créer des branches est d’éviter de travailler sur la branche principale qui est la branche fonctionnelle. En travaillant directement sur la branche principale, vous prenez le risque de créer des bugs qui mettraient en péril le bon fonctionnement du projet.

Notez que la branche master n’est pas une branche spéciale pour Git : elle est traitée de la même façon que les autres branches. L’idée est que lorsqu’on tape une commande git init, une branche est automatiquement créée et que le nom donné à cette branche par Git par défaut est "master".

- 👉 Modifiez le nom de votre branche principale pour "main". Vous pouvez faire une configuration globale qui s’appliquera à tous vos projets

`git config --global init.defaultBranch main`

- 👉 Récupérez la ressource "branchtheftauto.zip" depuis l’onglet dédié sur Ariane.

- 👉 Récupérez le script "comparator.py" et placez ce fichier dans votre dossier "comparator".

- 👉 Enregistrez cette version avec les commandes git add et git commit.

`git add comparator.py`

`git commit -m "project initialization"`

## CRÉATION D’UNE BRANCHE

Vous allez ajouter une version bilingue de ce programme (anglais-français).

- 👉 Pour ce faire, créez une branche que vous nommerez "french".

`git branch french`

- 👉 Basculez vers cette branche.

`git checkout french`

- 👉 Vous pouvez vérifier à tout moment sur quelle branche vous vous situez grâce à la commande suivante.

`git branch`

- 👉 Une fois sur la branche "french", ajoutez dans un premier temps une seconde ligne de commentaire sur la ligne 2 du script "comparator.py" comme ci-dessous.

```
# Python Program to find largest of Two Numbers

# V2 Bilingue EN-FR
```

- 👉 Enregistrez votre fichier et committez vos changements sur la branche "french".

## FUSIONNER DES BRANCHES

Vous allez maintenant rapatrier les changements effectués sur la branche "french" vers la branche "main".


- 👉 Revenez sur la branche "main" via la commande git checkout.


- 👉 La commande git diff vous indique les fichiers et les lignes sur lesquelles il existe une différence.

`git diff main..french`

- 👉 Rapatriez le contenu de la branche "french" dans la branche "main" grâce à la commande suivante.

`git merge french`

- 👉 Une fois la branche "french" mergée dans "main", exécutez la commande git log.
Vous remarquez que le commit effectué sur "french" est désormais sur la branche "main".

## GESTION DE CONFLITS

Imaginons maintenant que vous souhaitez modifier une même ligne sur deux branches différentes. Que se passe-t-il au moment de la fusion ?

- 👉 Sur la branche "french", modifiez à nouveau votre script "comparator.py", précisément la ligne 1.

`# Python Program to compare two integers in both French and English`

- 👉 Committez ce changement sur french, retournez sur la branche "main" et modifiez la ligne 1 à nouveau avec un commentaire différent.

- 👉 Faites un commit de cette version sur "main".

- 👉 Essayez de fusionner "french" dans "main".
Vous remarquez que Git bloque le merge et signale un conflit dans le fichier "comparator.py"

- 👉 Comme proposé par Git, ouvrez votre script avec nano.

Git a ajouté une syntaxe spécifique indiquant les points de conflits dans votre code. Il y a deux sections dans ce bloc de code :

Les sept symboles "<" présentent les changements dans votre branche actuelle (HEAD qui est votre branche actuelle). Les symboles "=" marquent la fin de cette première section.
La deuxième section contient les changements proposés par la branche que vous avez essayé de merger. Elle se termine par les sept symboles ">"

- 👉 En tant que développeur, c’est à vous de décider quelle ligne de code sera conservée pour le merge. Éditez votre code pour ne garder que la ligne 1 souhaitée puis enregistrez votre fichier.


- 👉 Faites un commit qui signalera à Git que vous avez résolu le conflit.

`git add comparator.py`

`git commit -m "merge french branch"`


- 👉 Vous pouvez enfin supprimer la branche "french".


## VISUALISATION DES BRANCHES

Des outils existent afin de faciliter la visualisation de l’historique de vos commits et vos branches, c’est le cas de Gitk.

- 👉 Commencez par installer Gitk.

`sudo apt update`

`sudo apt -y install gitk`

- 👉 Une fois Gitk installé, positionnez-vous dans votre projet versionné avec Git et exécutez la commande suivante.

`gitk`

Vous pouvez ainsi visualiser l’historique des versions et branches de votre projet.