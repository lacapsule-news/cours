# Git avancé

## Contexte

Lorsqu'on veut ajouter de nouvelles fonctionnalités à un projet, on va préférer travailler sur une copie du projet pour ne pas impacter le projet d'origine.

La finalité d'un version finalisée est de passer en production.

Il est necessaire d'avoir un procédure permettant de garantir la fiabilité et la stabilité du développement avant le passage en production.

## Fonctionnement

Les branches permettent de travailler sur une copie d'un projet sans en impacté le projet d'origine.

Les modifications (commit) se feront alors sur la copie du projet.

Lorsque le développement est terminé sur la copie, les commits doivent être rapatriés sur le projet d'origine.

Pour garantir le contrôle et la fiabilité des développements, une organisation de travail doit être mise en place avec des droits pour chaque membre de l'équipe.

### Step by step

- Création d'un branch

`git branch translation`

- Changement de branch

`git checkout translation`

Le commit s'applique uniquement sur la branch active.

- Rapatrier le travail
    - Sélectioner la branch principale
    `git checkout main`
    - Fusionner les commits
    `git merge translation`
    - Suppression de la branche
    `git branch -d translation`

## EXO

### 06 - Autogit

#### README TEMPLATING

Les commandes Git sont parfois complexes et répétitives… Vous allez user de vos skills en scripting pour automatiser certaines opérations !

- 👉 Récupérez la ressource "autogit.zip" depuis l’onglet dédié sur Ariane.

- 👉 Créez un dossier dans lequel vous initialisez un repository Git local.

- 👉 Créez un fichier README.md et copiez le contenu du fichier README fourni.

- 👉 En ligne de commande, sans utiliser l’interface web de GitLab, créez deux repositories sur GitLab (sans fichier README) : https://docs.gitlab.com/ee/user/project/working_with_projects.html

- 👉 Connectez et poussez votre dépôt local sur chacun de ces deux repositories. Vérifiez que le template du README s’affiche correctement. N’hésitez pas à le personnaliser pour la suite du challenge !

- 👉 Créez une branche appelée feature et positionnez-vous sur cette branche.

#### BASH SCRIPTING

- 👉 Imaginez maintenant un script bash qui automatise toutes ces étapes depuis l’initialisation du repository local jusqu’au premier push sur les deux dépôts distants. Il sera particulièrement pratique si vous souhaitez démarrer un nouveau projet sur deux dépôts distants (ou plus).

Au lancement du script le prompt vous invitera à entrer successivement les noms des 2 repositories distants. Le script nommera la branche principale main et le prompt demandera également le nom de la première feature branche que vous souhaitez créer pour commencer à travailler sur une fonctionnalité.

Une fois le script exécuté, vous retrouverez sur GitLab deux repositories avec 2 branches et un fichier README qui contiendra le template fourni dans le setup.

- 👉 Pour aller plus loin, modifiez votre script pour qu’il inscrive dynamiquement dans le README :
    - Le nom du contributeur principal (vous)
    - Le nom du projet
    - Les principales technos utilisées

- 👉 Ajoutez la possibilité de créer jusqu’à 5 branches (et noms associés).

`git push --set-upstream git@gitlab.com:lacapsule5051914/autogit.git main`
`git push --set-upstream git@gitlab.com:lacapsule5051914/autogitx.git main`

```
user=$(git config --global user.name)

mkdir $1
cd $1

git init

touch README.md
echo '# '$1 >> README.md
echo '' >> README.md
echo $user >> README.md
echo "------" >> README.md
echo 'Its has been created by bash on gitlab' >> README.md

git add .
git commit -m 'first commit'
git push --set-upstream git@gitlab.com:lacapsule5051914/$1.git main
git remote add origin git@gitlab.com:lacapsule5051914/$1.git

read -p 'Votre branche de dev ?' branch
git checkout -b $branch
git push -u origin $branch
```

### 05 - GITLAB COMBAT

#### CONFLITS AVEC GIT

Il est maintenant temps d’appliquer le principe de GitLab Flow en conditions réelles de travail d’équipe. Vous allez mobiliser votre buddy et gérer une série de conflits sur un projet commun.


- 👉 Créez un nouveau projet sur GitLab et invitez votre buddy.

- 👉 Commitez un fichier quelconque contenant quelques lignes de texte.

- 👉 Créez volontairement une situation de conflit en modifiant exactement la même ligne de code (chacun de votre côté) et en poussant vers la branche principale du dépôt distant.

- 👉 Résolvez le conflit comme vu dans le challenge "Branch Theft Auto"

- 👉 Créez un nouveau projet sur GitLab et inversez les rôles : la personne qui a provoqué le conflit est maintenant censé le régler 😉

### 04 - Gitlab flow

#### AUTHENTIFICATION PAR CLÉ SSH

En accédant à votre dashboard GitLab, vous avez sans doute remarqué un bandeau vous invitant à mettre en place la connexion par clés SSH.

Pour vous authentifier, vous allez créer une paire de clés SSH afin que votre client Git en local puisse communiquer avec votre compte GitLab de façon sécurisée. GitLab recommande l’usage de clés de type ED25519.

1. 👉 ssh-keygen -t ed25519 -C "GitLab Key Pair"
1. 👉 Repérez l’emplacement de vos clés SSH.
1. 👉 Copiez le contenu de votre clé publique.
1. 👉 Entrez votre clé publique sur la page "User Settings" > "SSH Keys".
1. 👉 Vérifiez que l’authentification SSH fonctionne grâce à la commande suivante.

ssh -T git@gitlab.com

GitLab vous renvoie un message de bienvenue : vous êtes désormais capables de vous authentifier via SSH depuis votre machine en ligne de commande.

#### CRÉATION D’UN GROUPE SUR GITLAB
 

1. 👉 Créez un nouveau groupe privé sur GitLab.
1. 👉 Invitez votre buddy au sein de ce groupe.
1. 👉 Créez un nouveau projet privé dans ce groupe. ("Blank Project" avec fichier README)

#### CRÉATION D’UN TICKET SUR GITLAB

- 👉 Créez un ticket (issue) pour proposer une modification du fichier README
- 👉 Utilisez la syntaxe MarkDown pour styliser et hiérarchiser votre description. Nous utilisons ici le symbole * pour créer un point de liste.

#### GITLAB FLOW

Le GitLab flow commence par la création d’un ticket (issue). Un ticket définit la portée du travail à accomplir.

- 👉 Éditez le ticket pour préciser quelles modifications vous souhaitez faire sur le fichier README. Utilisez à nouveau la syntaxe MarkDown pour créer une checkbox.

GitLab a ainsi créé une checklist et suit l’avancement de votre projet.

- 👉 Depuis l’interface GitLab, dans l’onglet repository, créez une nouvelle branche nommée production.
Après avoir créé la branche, vous êtes automatiquement dirigé vers la nouvelle branche.


- 👉 Dans l’onglet Branches de votre repository sont désormais listées les deux branches :

main :  la branche par défaut, protégée (seuls les utilisateurs qui ont certaines permissions peuvent pusher directement sur cette branche)
production

- 👉 Ajoutez une protection sur la branche production pour que seuls les utilisateurs disposant de certaines permissions puissent pusher sur cette branche (qui constitue l’environnement de production dans le flow GitLab).
Seuls les utilisateurs ayant le statut "Maintainer" pourront pusher sur cette branche.


#### CLONER LA CODEBASE EN LOCAL

Afin de modifier le fichier "README.md", vous allez cloner le projet sur votre machine.

- 👉 Copiez l’URL SSH de votre repository.
- 👉 Collez l’URL dans votre terminal, précédée de la commande git clone.
- 👉 Affichez le contenu de votre repository en local. Il contient désormais le fichier Readme.md (ainsi que le dossier caché .git qui prouve que nous sommes bien dans un repository git)
- 👉 Créez une branche nommée readme-introduction, et positionnez-vous sur cette branche.
- 👉 Ouvrez le fichier Readme.md avec nano et ajoutez une brève introduction en utilisant la syntaxe MD comme suit.
- 👉 Committez vos changements.

#### CRÉATION D’UNE MERGE REQUEST

- 👉 Synchronisez les changements effectués en local dans le repository sur GitLab.

`git push -u origin readme-introduction`

- 👉 Actualisez la page de votre repository sur GitLab, vérifiez que la branche readme-introduction et les modifications sont bien présentes.

Suite à votre push, un bandeau vous invite à créer une merge request.
Une merge request constitue l’action de demander à un membre du groupe de fusionner une branche dans une autre.


- 👉 Cliquez sur le bouton "Create merge request", ajoutez une courte description des changements apportés. Conservez l’option qui supprimera la branche dès que le merge est effectué.
Rappelez-vous, une branche pour une feature a une durée de vie courte, contrairement à la branche principale et à la branche production.


- 👉 Dans l’onglet Changes de la merge request, ajoutez un commentaire invitant à ajouter plus d’informations concernant ses hobbys.

- 👉 Faites les modifications nécessaires en local puis un git push vers le dépôt distant (sans -u puisque la branche readme-introduction existe déjà sur GitLab).


- 👉 Retournez sur GitLab pour terminer la review de la merge request. Procédez au merge et supprimez la branche readme-introduction. Retournez sur la page principale de votre repository : les changements ont bien été enregistrés sur la branche main.

#### MERGE DE MAIN DANS PRODUCTION

Rappelez-vous, dans le GitLab Flow, la branche main joue le rôle de branche de pré-production. Il est temps de merger la branche main dans la branche production.


- 👉 Créez une seconde merge mequest comme suit :

- branche source : main

- branche de destination : production 


👉 Validez ensuite directement la merge request.


#### FINALISEZ VOTRE PREMIÈRE RELEASE

La V1 de votre projet est prête !


- 👉 Dans l’onglet "Tags" de votre repository, créez depuis la branche production un nouveau tag que vous nommerez "v1.0".

Vous retrouvez cette release dans l’onglet Deployments. Elle contient notamment votre codebase disponible au téléchargement.

- 👉 Revenez sur votre machine en local, positionnez-vous sur la branche main (la branche readme-introduction n’existe plus sur le dépôt distant).


- 👉 Supprimez la version locale de la branche readme-introduction.

- 👉 Vous pouvez fermer le ticket créé en début de challenge. Retournez sur le ticket, cochez la checkbox, cliquez sur "Close Issue".

### 03 - Gitlab basics

#### CRÉER UN REPOSITORY SUR GITLAB

Pour conserver vos versions sur le cloud ou tout simplement pour partager votre travail avec votre équipe, il va être essentiel de créer un dépôt distant.

C’est ce que proposent des services Web d'hébergement comme GitHub, Bitbucket ou GitLab en mettant à disposition l’url de ce dépôt distant.


- 👉 Sur votre machine, créez un nouveau dossier "gitlabbasics" dans lequel vous initialisez un dépôt Git local.


- 👉 Créez un fichier "ReadMe.txt" puis faites un premier commit.


- 👉 Assurez-vous que la branche principale s’appelle "main". Si ce n’est pas le cas, exécutez la commande adéquate pour renommer la branche principale.


- 👉 Connectez-vous à GitLab et cliquez sur le bouton "+" > New Project/Repository. Créez un nouveau projet (Modèle Blank) que vous nommerez "gitlabbasics".

#### SE CONNECTER À UN DÉPÔT DISTANT

À chaque commit effectué, vous enregistrez vos modifications dans votre dépôt local. Une fois votre dépôt distant créé, plusieurs options s’offrent à vous. Nous allons déposer (push) notre dépôt local créé au préalable dans ce dépôt distant.

GitLab vous propose la série de commandes adaptée à ce cas :

- 👉 Le dépôt local étant déjà créé, exécutez la commande suivante à la racine de votre projet.

`git remote add origin git@gitlab.com:lacapsule5051914/gitlabbasics.git`

La commande "remote" suivie de "add" permet d’associer votre dépôt à un autre dépôt.

"origin" est le nom que vous souhaitez donner à ce dépôt distant. Vous êtes libre de donner le nom que vous souhaitez, par convention on le nomme "origin".

L’url correspond à l’url du dépôt distant. Elle est fournie par GitLab après la création de votre dépôt distant.


Ici, nous relions notre dépôt local vers ce dépôt distant que nous nommons "origin". Vous pouvez désormais lister tous les dépôts distants liés à votre dépôt local.

`git remote -v`

#### ENVOYER DES MODIFICATIONS

- 👉 Maintenant que la liaison a bien été créée, nous allons vouloir envoyer nos modifications sur le dépôt distant. Dans quel but ? Pour avoir une sauvegarde sur internet de notre projet et de son historique, mais également pour pouvoir partager ce projet avec d’autres développeurs.

Pour rappel, grâce à la commande "commit", nous avons enregistré des versions dans notre dépôt local. Ces différentes versions vont pouvoir être envoyées (push) vers un dépôt distant.

`git push origin main`

Actualisez la page de votre dépôt sur GitLab. Votre projet est désormais initialisé et le premier commit apparaît.

#### RÉCUPÉRER LES MODIFICATIONS

Vous souhaitez importer les modifications sur un autre poste en local ou vous travaillez en équipe et souhaitez récupérer les modifications faites par les développeurs de votre équipe ? Git met à disposition une commande pour récupérer ces informations distantes dans votre dépôt local.

Avant de réellement travailler en équipe, utilisons une fonctionnalité supplémentaire de GitLab : l’édition de code et la création de commit en ligne dans le navigateur.

- 👉 Dans l’IDE en ligne de GitLab, modifiez le fichier ReadMe.txt en ajoutant une ligne de texte. Enregistrez les modifications en créant un commit, toujours depuis le navigateur.

Cette modification est donc bien enregistrée au sein de votre dépôt distant. Notez que le fichier ReadMe.txt est un fichier un peu spécifique pour la plupart des plateformes d’hébergement de dépots distants tels que GitLab ou GitHub. Le contenu de ce fichier est affiché automatiquement sur la page d’accueil du repository. Il contient généralement des informations importantes au sujet du projet, que l’on souhaite partager avec les utilisateurs ou des développeurs amenés à travailler avec nous.

- 👉 Revenez sur votre machine et récupérez maintenant cette modification sur votre dépôt local.

`git pull origin main`

La commande "pull" permet de récupérer les versions (commit) qui n’étaient pas encore présentes dans notre dépôt. Une fois le git pull effectué, la commande git log fait apparaître le commit que vous avez créé sur l’IDE en ligne.

### 02 - BRANCH THEFT AUTO

#### DÉCOUVERTE DES BRANCHES

Avant Git, quand on avait un projet qui nécessitait de développer une nouvelle version, il était souvent nécessaire de faire une copie complète du projet pour pouvoir travailler dessus sereinement.

Cela obligeait à travailler sur plusieurs versions en parallèle. Les deux projets devenant indépendants, il était compliqué de répercuter les modifications faites sur un des environnements sur l’autre. Le travail était fastidieux, on risquait alors de perdre des fonctionnalités.

Lorsque la nouvelle version était finalisée, il fallait alors remplacer l’ancienne par la nouvelle. Là encore, les risques d’erreur étaient importants.


Créer une branche, c’est en quelque sorte comme créer une “copie” de votre projet pour développer et tester de nouvelles fonctionnalités sans impacter le projet de base. Git rend la création de nouvelles branches et la fusion de branche très facile à réaliser.

Une branche, dans Git, est simplement un pointeur vers un commit. La fusion de deux branches constitue un commit.


Cet outil en ligne permet de visualiser la création de commits et de branches : https://onlywei.github.io/explain-git-with-d3/


- 👉 Cliquez sur git branch et expérimentez avec la création de commits et de branches (cf. commandes sur le screenshot ci-dessous).

#### INITIALISATION D’UN REPOSITORY

- 👉 Créez un dossier "comparator" dans lequel vous initialisez un dépôt Git.

`git init`

La branche par défaut s’appelle master. Cette branche master va se déplacer automatiquement à chaque nouveau commit pour pointer sur le dernier commit effectué tant qu’on reste sur cette branche.

Pour rappel, l’un des intérêts de créer des branches est d’éviter de travailler sur la branche principale qui est la branche fonctionnelle. En travaillant directement sur la branche principale, vous prenez le risque de créer des bugs qui mettraient en péril le bon fonctionnement du projet.

Notez que la branche master n’est pas une branche spéciale pour Git : elle est traitée de la même façon que les autres branches. L’idée est que lorsqu’on tape une commande git init, une branche est automatiquement créée et que le nom donné à cette branche par Git par défaut est "master".

- 👉 Modifiez le nom de votre branche principale pour "main". Vous pouvez faire une configuration globale qui s’appliquera à tous vos projets

`git config --global init.defaultBranch main`

- 👉 Récupérez la ressource "branchtheftauto.zip" depuis l’onglet dédié sur Ariane.

- 👉 Récupérez le script "comparator.py" et placez ce fichier dans votre dossier "comparator".

- 👉 Enregistrez cette version avec les commandes git add et git commit.

`git add comparator.py`

`git commit -m "project initialization"`

#### CRÉATION D’UNE BRANCHE

Vous allez ajouter une version bilingue de ce programme (anglais-français).

- 👉 Pour ce faire, créez une branche que vous nommerez "french".

`git branch french`

- 👉 Basculez vers cette branche.

`git checkout french`

- 👉 Vous pouvez vérifier à tout moment sur quelle branche vous vous situez grâce à la commande suivante.

`git branch`

- 👉 Une fois sur la branche "french", ajoutez dans un premier temps une seconde ligne de commentaire sur la ligne 2 du script "comparator.py" comme ci-dessous.

```
# Python Program to find largest of Two Numbers

# V2 Bilingue EN-FR
```

- 👉 Enregistrez votre fichier et committez vos changements sur la branche "french".

#### FUSIONNER DES BRANCHES

Vous allez maintenant rapatrier les changements effectués sur la branche "french" vers la branche "main".


- 👉 Revenez sur la branche "main" via la commande git checkout.


- 👉 La commande git diff vous indique les fichiers et les lignes sur lesquelles il existe une différence.

`git diff main..french`

- 👉 Rapatriez le contenu de la branche "french" dans la branche "main" grâce à la commande suivante.

`git merge french`

- 👉 Une fois la branche "french" mergée dans "main", exécutez la commande git log.
Vous remarquez que le commit effectué sur "french" est désormais sur la branche "main".

#### GESTION DE CONFLITS

Imaginons maintenant que vous souhaitez modifier une même ligne sur deux branches différentes. Que se passe-t-il au moment de la fusion ?

- 👉 Sur la branche "french", modifiez à nouveau votre script "comparator.py", précisément la ligne 1.

`# Python Program to compare two integers in both French and English`

- 👉 Committez ce changement sur french, retournez sur la branche "main" et modifiez la ligne 1 à nouveau avec un commentaire différent.

- 👉 Faites un commit de cette version sur "main".

- 👉 Essayez de fusionner "french" dans "main".
Vous remarquez que Git bloque le merge et signale un conflit dans le fichier "comparator.py"

- 👉 Comme proposé par Git, ouvrez votre script avec nano.

Git a ajouté une syntaxe spécifique indiquant les points de conflits dans votre code. Il y a deux sections dans ce bloc de code :

Les sept symboles "<" présentent les changements dans votre branche actuelle (HEAD qui est votre branche actuelle). Les symboles "=" marquent la fin de cette première section.
La deuxième section contient les changements proposés par la branche que vous avez essayé de merger. Elle se termine par les sept symboles ">"

- 👉 En tant que développeur, c’est à vous de décider quelle ligne de code sera conservée pour le merge. Éditez votre code pour ne garder que la ligne 1 souhaitée puis enregistrez votre fichier.


- 👉 Faites un commit qui signalera à Git que vous avez résolu le conflit.

`git add comparator.py`

`git commit -m "merge french branch"`


- 👉 Vous pouvez enfin supprimer la branche "french".


#### VISUALISATION DES BRANCHES

Des outils existent afin de faciliter la visualisation de l’historique de vos commits et vos branches, c’est le cas de Gitk.

- 👉 Commencez par installer Gitk.

`sudo apt update`

`sudo apt -y install gitk`

- 👉 Une fois Gitk installé, positionnez-vous dans votre projet versionné avec Git et exécutez la commande suivante.

`gitk`

Vous pouvez ainsi visualiser l’historique des versions et branches de votre projet.