# SSH BEGIN

## SETUP

- Récupérez la ressource "sshbegin.zip" depuis l’onglet dédié sur Ariane.


- Importez le fichier "lab2.gns3project" dans GNS3.


Cette simulation de réseau contient tous les équipements nécessaires à la création d’un lab de machine virtuelles connectées à internet.


- Créez deux machines virtuelles Debian respectivement nommées "Debian-Server" et "Debian-Client".


- Reliez les deux VM au switch puis démarrez tous les équipements réseau.

Ayant l'équipement dans mon réseau local, voici les machine utiliser:
1. Debian-Client:MA-Lap:192.168.0.84
2. Debian-Server:RPI:192.168.0.52

## UTILISATION ET CONFIGURATION DE SSH

Secure Shell (SSH) est un protocole de communication ainsi qu’un programme en ligne de commande permettant de se connecter à une machine afin d'obtenir un shell (terminal).

Il est est généralement utilisé à distance afin de permettre aux utilisateurs de contrôler et de modifier leurs serveurs distants en passant par internet.
Aujourd’hui, vous allez seulement manipuler SSH afin d’administrer des machines en local.


⚠️ Attention : pendant toute la durée de ce challenge, vous ne devrez pas vous connecter directement à la VM "Debian-Server" en double-cliquant dessus, mais plutôt en passant par le protocole SSH comme demandé dans les étapes suivantes.


- À partir de la VM "Debian-Client", connectez-vous à l’utilisateur "debian" sur la VM "Debian-Server" grâce à la commande suivante.

`ssh debian@IP`


Vous pouvez temporairement ouvrir un terminal relié à la VM "Debian-Server" afin de récupérer son adresse IP.


- Une fois connecté à la VM "Debian-Server" via SSH, changez le port par défaut du serveur SSH par 5022 en modifiant le fichier "/etc/ssh/sshd_config" via nano.


- Redémarrez le serveur SSH afin d’appliquer la modification grâce à la commande suivante.


```
➜  ~ sudo nano /etc/ssh/sshd_config
➜  ~ systemctl reload ssh
➜  ~ nmap -sV -p 5022 192.168.0.52
PORT     STATE SERVICE VERSION
5022/tcp open  ssh     OpenSSH 8.4p1 Debian 5+deb11u1 (protocol 2.0)
```

Cette commande est censée vous déconnecter de la VM "Debian-Server".


- Toujours à partir de la VM "Debian-Client", connectez-vous de nouveau à la VM "Debian-Server" en précisant le nouveau port SSH grâce à l’option "-p".



- Une fois (re)connecté à la VM "Debian-Server" via SSH, créez un nouvel utilisateur "serveradmin" avec le mot de passe de votre choix grâce à la commande adduser.


- Exécutez la commande exit afin de vous déconnecter puis connectez-vous de nouveau avec l’utilisateur "serveradmin" (et le mot de passe créé dans l’étape précédente) grâce à la commande suivante.


`ssh -p 5022 serveradmin@IP`


- Une fois connecté à la VM "Debian-Server" en tant qu’utilisateur "serveradmin", tentez de faire une commande en tant qu’utilisateur root via sudo.


`sudo whoami`


Ça ne fonctionne pas ? C’est tout à fait normal, car tous les nouveaux utilisateurs ne sont pas autorisés à utiliser la commande sudo pour des raisons de sécurité.


- En tant qu’utilisateur "debian" connecté à la VM "Debian-Server", trouvez la commande permettant d’ajouter "serveradmin" au groupe "sudo".


- Enfin, connectez-vous en tant qu’utilisateur "serveradmin" et re-tentez la commande vue précédemment afin de vérifier qu’il est autorisé à exécuter des commandes en tant qu’utilisateur root.