# NGINX AND LOCATIONS

## CONFIGURATION D’UN SERVEUR WEB

- Reprenez le lab utilisé dans le challenge précédent afin de récupérer les équipements réseau ainsi que les machines virtuelles "Debian-Client" et "Debian-Server".


⚠️ Attention : pendant toute la durée de ce challenge, vous ne devrez pas vous connecter directement à la VM "Debian-Server" en double-cliquant dessus, mais plutôt en passant par le protocole SSH comme demandé dans les étapes suivantes.


- Sur la machine hébergeant le serveur web, créez un nouveau dossier nommé "intranet" dans "/var/www/html" avec un simple fichier "index.html" dedans (avec le contenu de votre choix).
Ce nouveau dossier représentera un second site à héberger sur le serveur web.

- À partir de la documentation de Nginx (ici et ici), faîtes en sorte que le page de bienvenue de Nginx soit affichée quand on visite l’URL / (ce qui est le cas par défaut) et que le nouvelle page HTML précédemment créée lorsqu’on visite l’URL /intranet.

```
location = /intranet {
    index intranet/index.html;
}
```