# Administration serveurs

## Theorique

### Administration de serveurs

#### Contexte

> Les différents serveurs qui constituent une infrastructure infomatique ont besoin d'être administrés:
- Mise en place d'un serveur
- Modification de la configuration système
- Installation d'un service

> A tout moment l'administrateur a besoin de s'y connecter pour prendre la main et agir dessus.

#### Fonctionement

Pour administrer un serveur, on peut utilisé une vaste quantité de protocole.

Aujourd'hui le plus fiable est le protocole ssh car il encrypt automatiquement les différent message ainsi que l'authetification contrairement a telnet.

Pour une authetifiaction par mots de passe le client aura juste besoin de connaitre le nom d'utilisateur, sont mots de passe, ainsi que l'ip du poste distant.

L'échange ssh se déroule comme ceci:
1. Le client SSH se connecte au serveur SSH en utilisant l'adresse IP du serveur et son nom d'utilisateur.
1. Le serveur SSH envoie un message de bienvenue au client SSH, comprenant notamment une identification du serveur.
1. Le client SSH vérifie l'identification du serveur en comparant la signature cryptographique fournie par le serveur à une clé publique enregistrée sur le client.
1. Si l'identification est validée, le serveur SSH demande au client de saisir un mot de passe.
1. Le client SSH envoie le mot de passe saisi au serveur SSH.
1. Le serveur SSH compare le mot de passe reçu avec le mot de passe stocké pour l'utilisateur correspondant.
1. Si les mots de passe correspondent, l'authentification est réussie et le client SSH est autorisé à accéder au shell du serveur.
1. Si les mots de passe ne correspondent pas, l'authentification échoue et le client SSH reçoit un message d'erreur.
1. Si l'authentification réussit, le client SSH peut exécuter des commandes sur le serveur via le shell.

Il est important de noté qu'il existe d'autre type d'authentification comme les clé privée/public.

Dans l'idée il faut générer un couple de clé sur le client, donner la clé public qu serveur, et configurer le client pour utilisé la clé lors de l'authentification.

Cela permet d'éviter de faire transité des mots de passe sur le réseau qui pourrait être intercepté.

## Pratique

### 05 - 401 AUTHORIZATION REQUIRED

#### SÉCURISATION D’UN SITE WEB

- Reprenez le lab utilisé dans le challenge précédent afin de récupérer les équipements réseau ainsi que les machines virtuelles "Debian-Client" et "Debian-Server".


⚠️ Attention : pendant toute la durée de ce challenge, vous ne devrez pas vous connecter directement à la VM "Debian-Server" en double-cliquant dessus, mais plutôt en passant par le protocole SSH comme demandé dans les étapes suivantes.


- Grâce à la notion de htpasswd et le package apache2-utils, faîtes en sorte que l’URL /intranet soit uniquement accessible en spécifiant un nom d’utilisateur et un mot de passe de la façon suivante.


> curl -u "admin:qwerty" -L http://IP/intranet

```
location /intranet {
        auth_basic      "Admin's area";
        auth_basic_user_file /etc/apache2/.htpasswd;
}
```

### 04 - NGINX AND LOCATIONS

#### CONFIGURATION D’UN SERVEUR WEB

- Reprenez le lab utilisé dans le challenge précédent afin de récupérer les équipements réseau ainsi que les machines virtuelles "Debian-Client" et "Debian-Server".


⚠️ Attention : pendant toute la durée de ce challenge, vous ne devrez pas vous connecter directement à la VM "Debian-Server" en double-cliquant dessus, mais plutôt en passant par le protocole SSH comme demandé dans les étapes suivantes.


- Sur la machine hébergeant le serveur web, créez un nouveau dossier nommé "intranet" dans "/var/www/html" avec un simple fichier "index.html" dedans (avec le contenu de votre choix).
Ce nouveau dossier représentera un second site à héberger sur le serveur web.

- À partir de la documentation de Nginx (ici et ici), faîtes en sorte que le page de bienvenue de Nginx soit affichée quand on visite l’URL / (ce qui est le cas par défaut) et que le nouvelle page HTML précédemment créée lorsqu’on visite l’URL /intranet.

```
location = /intranet {
    index intranet/index.html;
}
```

### 03 - MY FRIEND NGINX

#### INSTALLATION D’UN SERVEUR WEB

Reprenez le lab utilisé dans le challenge précédent afin de récupérer les équipements réseau ainsi que les machines virtuelles "Debian-Client" et "Debian-Server".


⚠️ Attention : pendant toute la durée de ce challenge, vous ne devrez pas vous connecter directement à la VM "Debian-Server" en double-cliquant dessus, mais plutôt en passant par le protocole SSH comme demandé dans les étapes suivantes.

- Installez le serveur web Nginx via apt.


- Toujours sur "Debian-Client" et via la commande curl, requêtez le serveur fraîchement installé sur "Debian-Server".


- En étant connecté à "Debian-Server" via SSH, localisez le fichier HTML renvoyé par défaut par le serveur Nginx afin de modifier son contenu (en ajoutant un texte, par exemple).

`sudo bash -c 'echo "exemple" > /var/www/html/index.nginx-debian.html'`

- Toujours sur "Debian-Client" et via la commande curl, requêtez le serveur web afin de vérifier que le contenu du fichier HTML ait bien été modifié.

```
sunburst@raspberrypi:~ $ curl localhost:80
exemple
```

### 02 - SECURE SSH

#### SÉCURISATION DE SSH

- Reprenez le lab utilisé dans le challenge précédent afin de récupérer les équipements réseau ainsi que les machines virtuelles "Debian-Client" et "Debian-Server".


- Ouvrez un terminal directement relié à la machine "Debian-Server" (sans passer par SSH) et sécurisez l’accès SSH en désactivant la possibilité de se connecter avec un mot de passe afin de privilégier l’accès par une clé SSH (pour des raisons de sécurité).


- Redémarrez le serveur SSH afin d’appliquer les modifications apportées précédemment.


- À partir de "Debian-Client", trouvez la commande permettant de générer une clé SSH nommée "debianclient" (sans passphrase).



- Récupérez la clé SSH publique (contenue dans le fichier debianclient.pub) et trouvez un moyen de l’enregistrer sur "Debian-Server" pour l’utilisateur "serveradmin".
Vous pouvez exceptionnellement vous connecter directement à "Debian-Server" sans passer par SSH pour cette étape.




- À partir de la VM "Debian-Client", tentez de vous connecter à l’utilisateur "serveradmin" sur la VM "Debian-Server" via SSH, de la même manière que dans le challenge précédent.


Cela ne fonctionne pas ? C’est tout à fait normal ! Vous avez bloqué la possibilité de se connecter via un mot de passe afin de forcer la connexion via une clé SSH.


- Trouvez la commande permettant de vous connecter à l’utilisateur "serveradmin" sur la VM "Debian-Server" en spécifiant la clé SSH privée contenue dans le fichier "debianclient".

1. Générer une paire de clés SSH sur votre ordinateur client : Vous pouvez utiliser la commande "ssh-keygen" pour générer une paire de clés SSH. Vous devrez fournir un nom de fichier pour votre clé privée et choisir une phrase de passe pour la protéger.
1. Copier la clé publique sur le serveur : Une fois que vous avez généré la paire de clés, vous devez copier la clé publique sur le serveur distant. Vous pouvez utiliser la commande "ssh-copy-id" pour copier votre clé publique sur le serveur. Assurez-vous de spécifier l'utilisateur et l'adresse IP corrects du serveur distant.
1. Modifier les paramètres d'authentification sur le serveur : Maintenant que la clé publique est sur le serveur distant, vous devez modifier les paramètres d'authentification sur le serveur pour permettre l'authentification par clé publique. Ouvrez le fichier "/etc/ssh/sshd_config" sur le serveur distant et ajoutez ou modifiez la ligne suivante : "PubkeyAuthentication yes". Enregistrez et fermez le fichier.
1. Redémarrez le service SSH : Après avoir modifié les paramètres d'authentification sur le serveur, vous devez redémarrer le service SSH pour qu'il prenne en compte les modifications. Utilisez la commande "sudo systemctl restart sshd" sur le serveur pour redémarrer le service.
1. Connectez-vous au serveur en utilisant la clé privée : Maintenant que l'authentification par clé publique est activée sur le serveur, vous pouvez vous connecter au serveur distant en utilisant votre clé privée. Utilisez la commande "ssh" sur votre ordinateur client pour vous connecter au serveur distant en spécifiant l'utilisateur et l'adresse IP corrects du serveur.

------

1. Crée les clé avec ssh-keygen
1. ssh-copy-id -i testssh.pub -n -p 5022 sunburst@192.168.0.52
    - -i precise la clé a envoyer
    - -n precise de ne rien envoyé seulment de dire ce qui va etre fait
    - -p precise le port sur lequelle se connecter
1. Ajouter dans le ficheir /etc/ssh/sshd_config
    - PubkeyAuthentication yes
    - PasswordAuthentication no
1. recharger les configuration du serveur ssh
    - sudo systemctl reload sshd
1. Se connecter
    1. Sans clé le serveur nous renvoie : sunburst@192.168.0.52: Permission denied (publickey)
    1. Si l'on utilise la clé, il va nous demander le mots de passe de la clé puis se connecter sans probleme

### 01 - SSH BEGIN

#### SETUP

- Récupérez la ressource "sshbegin.zip" depuis l’onglet dédié sur Ariane.


- Importez le fichier "lab2.gns3project" dans GNS3.


Cette simulation de réseau contient tous les équipements nécessaires à la création d’un lab de machine virtuelles connectées à internet.


- Créez deux machines virtuelles Debian respectivement nommées "Debian-Server" et "Debian-Client".


- Reliez les deux VM au switch puis démarrez tous les équipements réseau.

Ayant l'équipement dans mon réseau local, voici les machine utiliser:
1. Debian-Client:MA-Lap:192.168.0.84
2. Debian-Server:RPI:192.168.0.52

#### UTILISATION ET CONFIGURATION DE SSH

Secure Shell (SSH) est un protocole de communication ainsi qu’un programme en ligne de commande permettant de se connecter à une machine afin d'obtenir un shell (terminal).

Il est est généralement utilisé à distance afin de permettre aux utilisateurs de contrôler et de modifier leurs serveurs distants en passant par internet.
Aujourd’hui, vous allez seulement manipuler SSH afin d’administrer des machines en local.


⚠️ Attention : pendant toute la durée de ce challenge, vous ne devrez pas vous connecter directement à la VM "Debian-Server" en double-cliquant dessus, mais plutôt en passant par le protocole SSH comme demandé dans les étapes suivantes.


- À partir de la VM "Debian-Client", connectez-vous à l’utilisateur "debian" sur la VM "Debian-Server" grâce à la commande suivante.

`ssh debian@IP`


Vous pouvez temporairement ouvrir un terminal relié à la VM "Debian-Server" afin de récupérer son adresse IP.


- Une fois connecté à la VM "Debian-Server" via SSH, changez le port par défaut du serveur SSH par 5022 en modifiant le fichier "/etc/ssh/sshd_config" via nano.


- Redémarrez le serveur SSH afin d’appliquer la modification grâce à la commande suivante.


```
➜  ~ sudo nano /etc/ssh/sshd_config
➜  ~ systemctl reload ssh
➜  ~ nmap -sV -p 5022 192.168.0.52
PORT     STATE SERVICE VERSION
5022/tcp open  ssh     OpenSSH 8.4p1 Debian 5+deb11u1 (protocol 2.0)
```

Cette commande est censée vous déconnecter de la VM "Debian-Server".


- Toujours à partir de la VM "Debian-Client", connectez-vous de nouveau à la VM "Debian-Server" en précisant le nouveau port SSH grâce à l’option "-p".



- Une fois (re)connecté à la VM "Debian-Server" via SSH, créez un nouvel utilisateur "serveradmin" avec le mot de passe de votre choix grâce à la commande adduser.


- Exécutez la commande exit afin de vous déconnecter puis connectez-vous de nouveau avec l’utilisateur "serveradmin" (et le mot de passe créé dans l’étape précédente) grâce à la commande suivante.


`ssh -p 5022 serveradmin@IP`


- Une fois connecté à la VM "Debian-Server" en tant qu’utilisateur "serveradmin", tentez de faire une commande en tant qu’utilisateur root via sudo.


`sudo whoami`


Ça ne fonctionne pas ? C’est tout à fait normal, car tous les nouveaux utilisateurs ne sont pas autorisés à utiliser la commande sudo pour des raisons de sécurité.


- En tant qu’utilisateur "debian" connecté à la VM "Debian-Server", trouvez la commande permettant d’ajouter "serveradmin" au groupe "sudo".


- Enfin, connectez-vous en tant qu’utilisateur "serveradmin" et re-tentez la commande vue précédemment afin de vérifier qu’il est autorisé à exécuter des commandes en tant qu’utilisateur root.