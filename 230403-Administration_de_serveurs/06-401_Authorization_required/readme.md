# 401 AUTHORIZATION REQUIRED

## SÉCURISATION D’UN SITE WEB

- Reprenez le lab utilisé dans le challenge précédent afin de récupérer les équipements réseau ainsi que les machines virtuelles "Debian-Client" et "Debian-Server".


⚠️ Attention : pendant toute la durée de ce challenge, vous ne devrez pas vous connecter directement à la VM "Debian-Server" en double-cliquant dessus, mais plutôt en passant par le protocole SSH comme demandé dans les étapes suivantes.


- Grâce à la notion de htpasswd et le package apache2-utils, faîtes en sorte que l’URL /intranet soit uniquement accessible en spécifiant un nom d’utilisateur et un mot de passe de la façon suivante.


> curl -u "admin:qwerty" -L http://IP/intranet

```
location /intranet {
        auth_basic      "Admin's area";
        auth_basic_user_file /etc/apache2/.htpasswd;
}
```