# Administration de serveurs

## Contexte

> Les différents serveurs qui constituent une infrastructure infomatique ont besoin d'être administrés:
- Mise en place d'un serveur
- Modification de la configuration système
- Installation d'un service

> A tout moment l'administrateur a besoin de s'y connecter pour prendre la main et agir dessus.

## Fonctionement

Pour administrer un serveur, on peut utilisé une vaste quantité de protocole.

Aujourd'hui le plus fiable est le protocole ssh car il encrypt automatiquement les différent message ainsi que l'authetification contrairement a telnet.

Pour une authetifiaction par mots de passe le client aura juste besoin de connaitre le nom d'utilisateur, sont mots de passe, ainsi que l'ip du poste distant.

L'échange ssh se déroule comme ceci:
1. Le client SSH se connecte au serveur SSH en utilisant l'adresse IP du serveur et son nom d'utilisateur.
1. Le serveur SSH envoie un message de bienvenue au client SSH, comprenant notamment une identification du serveur.
1. Le client SSH vérifie l'identification du serveur en comparant la signature cryptographique fournie par le serveur à une clé publique enregistrée sur le client.
1. Si l'identification est validée, le serveur SSH demande au client de saisir un mot de passe.
1. Le client SSH envoie le mot de passe saisi au serveur SSH.
1. Le serveur SSH compare le mot de passe reçu avec le mot de passe stocké pour l'utilisateur correspondant.
1. Si les mots de passe correspondent, l'authentification est réussie et le client SSH est autorisé à accéder au shell du serveur.
1. Si les mots de passe ne correspondent pas, l'authentification échoue et le client SSH reçoit un message d'erreur.
1. Si l'authentification réussit, le client SSH peut exécuter des commandes sur le serveur via le shell.

Il est important de noté qu'il existe d'autre type d'authentification comme les clé privée/public.

Dans l'idée il faut générer un couple de clé sur le client, donner la clé public qu serveur, et configurer le client pour utilisé la clé lors de l'authentification.

Cela permet d'éviter de faire transité des mots de passe sur le réseau qui pourrait être intercepté.