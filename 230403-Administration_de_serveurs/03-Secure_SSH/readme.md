# SECURE SSH

## SÉCURISATION DE SSH

- Reprenez le lab utilisé dans le challenge précédent afin de récupérer les équipements réseau ainsi que les machines virtuelles "Debian-Client" et "Debian-Server".


- Ouvrez un terminal directement relié à la machine "Debian-Server" (sans passer par SSH) et sécurisez l’accès SSH en désactivant la possibilité de se connecter avec un mot de passe afin de privilégier l’accès par une clé SSH (pour des raisons de sécurité).


- Redémarrez le serveur SSH afin d’appliquer les modifications apportées précédemment.


- À partir de "Debian-Client", trouvez la commande permettant de générer une clé SSH nommée "debianclient" (sans passphrase).



- Récupérez la clé SSH publique (contenue dans le fichier debianclient.pub) et trouvez un moyen de l’enregistrer sur "Debian-Server" pour l’utilisateur "serveradmin".
Vous pouvez exceptionnellement vous connecter directement à "Debian-Server" sans passer par SSH pour cette étape.




- À partir de la VM "Debian-Client", tentez de vous connecter à l’utilisateur "serveradmin" sur la VM "Debian-Server" via SSH, de la même manière que dans le challenge précédent.


Cela ne fonctionne pas ? C’est tout à fait normal ! Vous avez bloqué la possibilité de se connecter via un mot de passe afin de forcer la connexion via une clé SSH.


- Trouvez la commande permettant de vous connecter à l’utilisateur "serveradmin" sur la VM "Debian-Server" en spécifiant la clé SSH privée contenue dans le fichier "debianclient".

1. Générer une paire de clés SSH sur votre ordinateur client : Vous pouvez utiliser la commande "ssh-keygen" pour générer une paire de clés SSH. Vous devrez fournir un nom de fichier pour votre clé privée et choisir une phrase de passe pour la protéger.
1. Copier la clé publique sur le serveur : Une fois que vous avez généré la paire de clés, vous devez copier la clé publique sur le serveur distant. Vous pouvez utiliser la commande "ssh-copy-id" pour copier votre clé publique sur le serveur. Assurez-vous de spécifier l'utilisateur et l'adresse IP corrects du serveur distant.
1. Modifier les paramètres d'authentification sur le serveur : Maintenant que la clé publique est sur le serveur distant, vous devez modifier les paramètres d'authentification sur le serveur pour permettre l'authentification par clé publique. Ouvrez le fichier "/etc/ssh/sshd_config" sur le serveur distant et ajoutez ou modifiez la ligne suivante : "PubkeyAuthentication yes". Enregistrez et fermez le fichier.
1. Redémarrez le service SSH : Après avoir modifié les paramètres d'authentification sur le serveur, vous devez redémarrer le service SSH pour qu'il prenne en compte les modifications. Utilisez la commande "sudo systemctl restart sshd" sur le serveur pour redémarrer le service.
1. Connectez-vous au serveur en utilisant la clé privée : Maintenant que l'authentification par clé publique est activée sur le serveur, vous pouvez vous connecter au serveur distant en utilisant votre clé privée. Utilisez la commande "ssh" sur votre ordinateur client pour vous connecter au serveur distant en spécifiant l'utilisateur et l'adresse IP corrects du serveur.

------

1. Crée les clé avec ssh-keygen
1. ssh-copy-id -i testssh.pub -n -p 5022 sunburst@192.168.0.52
    - -i precise la clé a envoyer
    - -n precise de ne rien envoyé seulment de dire ce qui va etre fait
    - -p precise le port sur lequelle se connecter
1. Ajouter dans le ficheir /etc/ssh/sshd_config
    - PubkeyAuthentication yes
    - PasswordAuthentication no
1. recharger les configuration du serveur ssh
    - sudo systemctl reload sshd
1. Se connecter
    1. Sans clé le serveur nous renvoie : sunburst@192.168.0.52: Permission denied (publickey)
    1. Si l'on utilise la clé, il va nous demander le mots de passe de la clé puis se connecter sans probleme