# MY FRIEND NGINX

## INSTALLATION D’UN SERVEUR WEB

Reprenez le lab utilisé dans le challenge précédent afin de récupérer les équipements réseau ainsi que les machines virtuelles "Debian-Client" et "Debian-Server".


⚠️ Attention : pendant toute la durée de ce challenge, vous ne devrez pas vous connecter directement à la VM "Debian-Server" en double-cliquant dessus, mais plutôt en passant par le protocole SSH comme demandé dans les étapes suivantes.

- Installez le serveur web Nginx via apt.


- Toujours sur "Debian-Client" et via la commande curl, requêtez le serveur fraîchement installé sur "Debian-Server".


- En étant connecté à "Debian-Server" via SSH, localisez le fichier HTML renvoyé par défaut par le serveur Nginx afin de modifier son contenu (en ajoutant un texte, par exemple).

`sudo bash -c 'echo "exemple" > /var/www/html/index.nginx-debian.html'`

- Toujours sur "Debian-Client" et via la commande curl, requêtez le serveur web afin de vérifier que le contenu du fichier HTML ait bien été modifié.

```
sunburst@raspberrypi:~ $ curl localhost:80
exemple
```