# Jest meets gitlab

## SETUP

Vous allez récupérer les tests unitaires du challenge précédent et automatiser l’exécution de ces tests dans une pipeline GitLab.


⚠️ Attention : GitLab limite l’usage de ses outils CI/CD à 400 minutes dans sa version gratuite.
Évitez les pushs inutiles et vérifiez bien que les jobs ne durent pas plus de quelques minutes (dans l’onglet CI/CD > Jobs) et stoppez les jobs qui ne sont pas nécessaires.


👉 Hébergez le projet du challenge précédent sur GitLab.

## CRÉATION D’UNE PIPELINE

👉 Créez un fichier ".gitlab-ci.yml" chargé d’exécuter les tests Jest à chaque commit.

Vous devrez choisir l’image Docker la plus adaptée par rapport à l’environnement du projet (NodeJS)


👉 Une fois la pipeline créée, effectuez un push vers GitLab et suivez les logs du job en cours afin de vérifier que le runner exécute bien les tests.


👉 Modifiez un des tests afin de vérifier que la pipeline est bien en erreur lorsqu’un test n’est pas validé.

```
stages:
  - test

jest-framwork:
  stage: test
  image: node:latest
  script:
    - yarn install
    - yarn test
```