# Frmaeworks de test

## Contexte

Les fameworks de test peuvent permettre de définir les objectif technique que le développeur devra respecter.

Les objectifs d'une fonctionnalité peuvent évoluer avec le temps, ce qui veut dire que le test aura étais modifié en amont.

Le permet de mettre l'action sur les fonctionalité mal dévelopé ou les effet de débordement.

### Fonctionnement

Le lead développeur précise les objectifs d'une fonctionnalité.

Développer une fonction qui calcule le total d'un panier.
Par exemple, si j'ajoute :
- 2 produits à 15€ / unité et 3€ de frais de port.
- j'obtien un total de 33€

Le lead développeur formalise les règles dans un fichier qu'il place dans le projet.

Le développeur code la conctionnalité en tenant compte des objectifs.

Une fois le développement terminé, le développeur lance une commande qui exécute les tests afin de valider son travail.

## Exo

### 03 - Jest meets gitlab

#### SETUP

Vous allez récupérer les tests unitaires du challenge précédent et automatiser l’exécution de ces tests dans une pipeline GitLab.


⚠️ Attention : GitLab limite l’usage de ses outils CI/CD à 400 minutes dans sa version gratuite.
Évitez les pushs inutiles et vérifiez bien que les jobs ne durent pas plus de quelques minutes (dans l’onglet CI/CD > Jobs) et stoppez les jobs qui ne sont pas nécessaires.


👉 Hébergez le projet du challenge précédent sur GitLab.

#### CRÉATION D’UNE PIPELINE

👉 Créez un fichier ".gitlab-ci.yml" chargé d’exécuter les tests Jest à chaque commit.

Vous devrez choisir l’image Docker la plus adaptée par rapport à l’environnement du projet (NodeJS)


👉 Une fois la pipeline créée, effectuez un push vers GitLab et suivez les logs du job en cours afin de vérifier que le runner exécute bien les tests.


👉 Modifiez un des tests afin de vérifier que la pipeline est bien en erreur lorsqu’un test n’est pas validé.

```
stages:
  - test

jest-framwork:
  stage: test
  image: node:latest
  script:
    - yarn install
    - yarn test
```

### 02 - Jest begin

#### INSTALLATION DE JEST

Vous avez eu l’occasion de découvrir la notion de test end-to-end avec Cypress et il est maintenant temps de se focaliser sur un type de test en particulier : les tests unitaires, qui font partie intégrante de la notion de tests e2e.

Les tests unitaires ont pour seul objectif de vérifier le retour d’une fonction créée par les développeurs, sans pour autant vérifier son intégration dans le projet d’un point de vue utilisateur final.

Ce challenge constitue l’occasion de prendre en main le fonctionnement global de Jest, un framework de tests unitaires pour les projets web & mobile en JavaScript.

👉 Récupérez la ressource "jestbegin.zip" depuis l’onglet dédié sur Ariane et décompressez l’archive.

Cette archive contient un simple projet JavaScript avec quelques fonctions qui permettent d’effectuer l’addition, la soustraction ou la multiplication de deux nombres.

👉 En vous basant sur la documentation, installez Jest via la commande yarn, tout en étant positionné dans le répertoire du projet.

👉 Ajoutez la section suivante dans le fichier "package.json" afin que la commande yarn test puisse exécuter Jest.

```
"scripts": {

  "test": "jest"

},
```

#### LANCEMENT DES TESTS

👉 Lancez les tests Jest avec la commande suivante.

`yarn test`

Vous êtes censés voir que le premier test intitulé "Addition - 5 + 6 = 11" a été validé.

👉 Ouvrez le fichier "calc.test.js" et identifiez la partie responsable de la vérification du bon fonctionnement de la fonction "addition”.

Cette partie du test unitaire peut être traduite de la façon suivante : le test nommé "Addition - 5 + 6 = 11" s’attend (expect) à ce que le résultat de l’exécution de la fonction "addition" (avec les arguments 5 et 6) soit égal à 11.

👉 Décommentez le second test contenu dans le fichier "calc.test.js" en retirant les "//" en début de ligne et exécutez de nouveau la suite de tests.

A votre grande surprise, vous êtes censé constater que le second test n’est pas validé puis qu’il y a une différence entre la valeur de retour attendue ("Expected") et le résultat reçu (“Received") :

👉 Même s’il s’agit d’une tâche réservée aux développeurs, modifiez le test afin de corriger l’erreur dans la valeur attendue et exécutez de nouveau la suite de tests.

#### CRÉATION D’UN TEST

👉 Créez un dernier test dans le fichier "calc.test.js" qui sera chargé de vérifier le bon fonctionnement de la fonction "multiplication" avec les données de votre choix.

```
const { addition, subtraction, multiplication } = require('./calc');

test('Addition - 5 + 6 = 11', () => {
  expect(addition(5, 6)).toBe(11);
});

test('Subtraction - 27 - 5 = 22', () => {
  expect(subtraction(27, 5)).toBe(22);
});

test('Multiplication - 30 x 5 = 150', () => {
  expect(multiplication(30, 5)).toBe(150);
});
```

## QCM

### READ THE DOCUMENTATION : CYPRESS

- How can Cypress be useful for developers?
    - Cypress is a front end testing tool designed for modern web applications. It helps developers and QA engineers to quickly set up, write, run and debug tests. Unlike Selenium, Cypress is not limited by restrictions, allowing for faster, easier and more reliable testing.

- How do you write a test with Cypress?
    - To write a test with Cypress, you don't need to install any servers, drivers, or configure any dependencies. The process is simple and straightforward. In just 60 seconds, you can write your first passing test. The Cypress API is built on familiar tools and is designed to be easy to read and understand, making the test writing process even more effortless.

- How do you launch tests with Cypress?
    - Cypress tests can be launched directly from the Cypress interface and run as fast as your browser can render content. This allows you to see the tests run in real-time as you develop your applications, which helps to make testing a more seamless and efficient process.Additionally, the readable error messages provided by Cypress help you to quickly debug any issues that may arise.

- What is the "RWA" created by Cypress? What tests are included?
    - The RWA is a full stack example application that demonstrates practical and realistic testing scenarios with Cypress. The RWA achieves full code-coverage with end-to-end tests across multiple browsers and device sizes, and includes visual regression tests, API tests, unit tests, and runs them all in an efficient CI pipeline.

### READ THE DOCUMENTATION : GITLAB FLOW

- What is the Git flow?
    - The Git flow is a three-step process for sharing commits with colleagues in Git. Unlike most version control systems which only have one step of committing from the working copy to a shared server, Git requires you to add files from the working copy to the staging area, then commit them to your local repository, and finally push to a shared remote repository.

- In which cases is it not possible to use the GitHub flow?
    - The GitHub flow may not be applicable in cases where you cannot deploy to production every time you merge a feature branch. This could be due to a lack of control over the release timing, such as in an iOS application that is released only after it passes App Store validation, or due to limited deployment windows, such as workdays with specific time frames.

- What is a merge/pull request ?
    - A merge/pull request is a request made in a Git management application, such as GitHub or Bitbucket, to merge two branches. The request is made by asking an assigned person to merge the feature branch into the main branch.The name "pull request" is used by some tools because the first manual action is to pull the feature branch, while the name "merge request" is used by other tools because the final action is to merge the feature branch.

- How to automatically close an issue linked to a merge request with GitLab?
    - In order to automatically close an issue linked to a merge request with GitLab, you should link the issue to the merge request by mentioning it in the commit message or description using keywords like "fixes" or "closes" followed by the issue number (e.g. "fixes #14"). GitLab will then automatically close the issue when the code is merged into the default branch.

### READ THE DOCUMENTATION : GITLAB CI/CD

- What are the prerequisites to create a CI/CD pipeline with GitLab ?
    - To create a CI/CD pipeline with GitLab, you need to have: 1. a project in GitLab that you want to use for CI/CD, and 2. the Maintainer or Owner role for the project.If you don't have a project, you can create a public project for free on https://gitlab.com.

- What step can be skipped if you’re using GitLab.com to create a CI/CD pipeline? Why ?
    - If you're using GitLab.com to create a CI/CD pipeline, you can skip the step of ensuring you have runners available to run your jobs. This is because GitLab.com provides shared runners for you, so you don't need to worry about setting up your own runners.

- What is the purpose of the ".gitlab-ci.yml" file?
    - The ".gitlab-ci.yml" file is used in GitLab CI/CD as a configuration file written in YAML. It contains the instructions for GitLab CI/CD to follow, including the structure and order of jobs to be executed by the runner, as well as the decisions the runner should make when certain conditions are met.

- Explain what the "build-job" job does in the example given in the section "Creating a .gitlab-ci.yml file".
    - The "build-job" job is assigned to the "build" stage and prints the string "Hello, $GITLAB_USER_LOGIN!" to the console.

- Explain what the "test-job2" job does in the example given in the section "Creating a .gitlab-ci.yml file".
    - The "test-job2" job belongs to the "test" stage and performs several tasks including echoing messages, and sleeping for 20 seconds.