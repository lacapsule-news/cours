# Jest begin

## INSTALLATION DE JEST

Vous avez eu l’occasion de découvrir la notion de test end-to-end avec Cypress et il est maintenant temps de se focaliser sur un type de test en particulier : les tests unitaires, qui font partie intégrante de la notion de tests e2e.

Les tests unitaires ont pour seul objectif de vérifier le retour d’une fonction créée par les développeurs, sans pour autant vérifier son intégration dans le projet d’un point de vue utilisateur final.

Ce challenge constitue l’occasion de prendre en main le fonctionnement global de Jest, un framework de tests unitaires pour les projets web & mobile en JavaScript.

👉 Récupérez la ressource "jestbegin.zip" depuis l’onglet dédié sur Ariane et décompressez l’archive.

Cette archive contient un simple projet JavaScript avec quelques fonctions qui permettent d’effectuer l’addition, la soustraction ou la multiplication de deux nombres.

👉 En vous basant sur la documentation, installez Jest via la commande yarn, tout en étant positionné dans le répertoire du projet.

👉 Ajoutez la section suivante dans le fichier "package.json" afin que la commande yarn test puisse exécuter Jest.

```
"scripts": {

  "test": "jest"

},
```

## LANCEMENT DES TESTS

👉 Lancez les tests Jest avec la commande suivante.

`yarn test`

Vous êtes censés voir que le premier test intitulé "Addition - 5 + 6 = 11" a été validé.

👉 Ouvrez le fichier "calc.test.js" et identifiez la partie responsable de la vérification du bon fonctionnement de la fonction "addition”.

Cette partie du test unitaire peut être traduite de la façon suivante : le test nommé "Addition - 5 + 6 = 11" s’attend (expect) à ce que le résultat de l’exécution de la fonction "addition" (avec les arguments 5 et 6) soit égal à 11.

👉 Décommentez le second test contenu dans le fichier "calc.test.js" en retirant les "//" en début de ligne et exécutez de nouveau la suite de tests.

A votre grande surprise, vous êtes censé constater que le second test n’est pas validé puis qu’il y a une différence entre la valeur de retour attendue ("Expected") et le résultat reçu (“Received") :

👉 Même s’il s’agit d’une tâche réservée aux développeurs, modifiez le test afin de corriger l’erreur dans la valeur attendue et exécutez de nouveau la suite de tests.

## CRÉATION D’UN TEST

👉 Créez un dernier test dans le fichier "calc.test.js" qui sera chargé de vérifier le bon fonctionnement de la fonction "multiplication" avec les données de votre choix.