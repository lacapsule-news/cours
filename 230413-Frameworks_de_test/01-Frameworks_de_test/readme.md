# Frmaeworks de test

## Contexte

Les fameworks de test peuvent permettre de définir les objectif technique que le développeur devra respecter.

Les objectifs d'une fonctionnalité peuvent évoluer avec le temps, ce qui veut dire que le test aura étais modifié en amont.

Le permet de mettre l'action sur les fonctionalité mal dévelopé ou les effet de débordement.

### Fonctionnement

Le lead développeur précise les objectifs d'une fonctionnalité.

Développer une fonction qui calcule le total d'un panier.
Par exemple, si j'ajoute :
- 2 produits à 15€ / unité et 3€ de frais de port.
- j'obtien un total de 33€

Le lead développeur formalise les règles dans un fichier qu'il place dans le projet.

Le développeur code la conctionnalité en tenant compte des objectifs.

Une fois le développement terminé, le développeur lance une commande qui exécute les tests afin de valider son travail.