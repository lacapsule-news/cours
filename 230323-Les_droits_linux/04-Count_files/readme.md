# Count files

## COMPTER LE NOMBRE DE FICHIERS PAR UTILISATEUR

- Créez un script "countFiles.sh" qui prend en paramètre un chemin d’accès vers un dossier

- afin d’afficher le nombre de fichiers contenus dans le répertoire pour chaque propriétaire.


Exemple de réponse :


root 1

user1 2

user2 5

nobody 1

