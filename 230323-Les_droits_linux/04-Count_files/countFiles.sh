#!/usr/bin/env bash

# Créez un script "countFiles.sh" qui prend en paramètre un chemin d’accès vers un dossier

if [ -d $1 ];then
	find $1 -printf '%u\n'|sort|uniq -c
fi