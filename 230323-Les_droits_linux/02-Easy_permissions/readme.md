# EASY PERMISSIONS

## MODIFICATION DES PROPRIÉTAIRES D’UN ENSEMBLE DE FICHIERS

- Récupérez la ressource "easypermissions.zip" depuis l’onglet dédié sur Ariane.


- Créez l’utilisateur et le groupe "johncena" grâce aux commandes suivantes.

```
sudo useradd johncena

sudo groupadd johncena
```

- Ajoutez l’utilisateur "johncena" au groupe éponyme.


`sudo usermod -aG johncena johncena`


- Créez un script easyOwner.sh chargé de lire l’entrée du terminal (via la commande read) afin de récupérer le chemin vers un ou plusieurs fichiers, puis le nouveau propriétaire et le nouveau groupe de ces fichiers.

```
Files to update :

> file1 file2 file3


New owner and group :

> johncena


file1 owner "engineer" changed to "johncena"

file2 owner "engineer" changed to "johncena"

file3 owner "engineer" changed to "johncena"
```

## MODIFICATION DES DROITS D’UN ENSEMBLE DE FICHIERS

- Créez un second script easyPermissions.sh chargé de lire l’entrée du terminal afin de récupérer le chemin vers un ou plusieurs fichiers, puis les nouvelles permissions au format "XXX", en suivant une représentation en octal.

```
Files to update :

> file1 file2 file3


New permission :

> 444


file1 permissions changed to 444

file2 permissions changed to 444
```