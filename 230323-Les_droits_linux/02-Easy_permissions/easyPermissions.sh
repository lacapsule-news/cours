#!/usr/bin/env bash
fUpdate="easypermissions/file5.env easypermissions/file4.env easypermissions/file3.env easypermissions/file2.env easypermissions/file1.env"
uPerms="640"
#read -p "File to update : " fUpdate

echo "> "$fUpdate

#read -p "New permission : " uPerms

echo "> "$uPerms

for file in $fUpdate;do
	if [[ -f $file ]]; then
		oPerms=$(stat -c '%a' $file)
		sudo chmod $uPerms $file
		echo $file" permissions "$oPerms" changed to "$(stat -c '%a' $file)
	fi
done