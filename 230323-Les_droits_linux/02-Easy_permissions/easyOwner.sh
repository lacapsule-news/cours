#!/usr/bin/env bash
#fUpdate="easypermissions/file5.env dfshgjhf easypermissions/file4.env easypermissions/file3.env easypermissions/file2.env easypermissions/file1.env"
#uOwner="sunburst"
read -p "File to update : " fUpdate

echo "> "$fUpdate

read -p "New owner and group : " uOwner

echo "> "$uOwner

for file in $fUpdate;do
	if [[ -f $file ]]; then
		oOwner=$(ls -l $file|awk '{print $3}')
		sudo chown $uOwner $file
		echo $file" owner "$oOwner" changed to "$uOwner
	else
		echo "not a file"
	fi
done