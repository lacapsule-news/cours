#!/usr/bin/env bash

# demande à l'utilisateur d'entrer un chemin d'accès vers un fichier ou un dossier, 

read "Enter a file : "file

# affiche les permissions associées au fichier/dossier.

perm=$(stat -c '%A' $file)
echo $perm

# Si l'utilisateur est le propriétaire du fichier/dossier,
echo $USER
# le script doit également demander s'il souhaite modifier les permissions et, le cas échéant, les modifier en fonction des réponses de l'utilisateur.