# Exo Bonus

- demande à l'utilisateur d'entrer un chemin d'accès vers un fichier ou un dossier, 
- affiche les permissions associées au fichier/dossier.
- Si l'utilisateur est le propriétaire du fichier/dossier, 
- le script doit également demander s'il souhaite modifier les permissions et, le cas échéant, les modifier en fonction des réponses de l'utilisateur.