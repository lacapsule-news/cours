#!/usr/bin/env bash

# prenant en paramètre un chemin d’accès vers un fichier afin de vérifier s’il existe et ses permissions.
# Si le fichier n’existe pas, le message suivant sera affiché : "FILE does not exist"
# Si l’utilisateur actuel dispose des droits d’exécutions sur le fichier, le message suivant sera affiché : "You have permissions to execute FILE" sinon "You do not have permissions to execute FILE".

if [ -e $1 ];then
	if [ -x $1 ];then
		echo "You have permissions to execute FILE"
	else
		echo "You do not have permissions to execute FILE"
	fi
else
	echo "FILE does not exist"
fi