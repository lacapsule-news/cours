# Les droit linux

## Theorique

### Contexte

- Isoler certaine fichier utilisateur / systeme
- se gere grace a des droits
`chmod +x fichier`
- le root a tout les droits
- groupe d'utilisateur (groupe de personne ou groupe de droits)
- 3 type de droits
`[USER][GROUPE][AUTRES]`
- 3 droits differents par type
`w[Ecriture],r[Lecture],x[Execution]`

```
[sunburst@MA-Lap > 06-Vacation_pictures] $ ls -l
total 16K
drwxr-xr-x 2 sunburst sunburst 4,0K 22 mars  15:35 picturesToRename
-rw-r--r-- 1 sunburst sunburst 1,1K 22 mars  15:58 readme.md
drwxr-xr-x 2 sunburst sunburst 4,0K 23 mars  09:29 renamedPictures
-rwxr-xr-x 1 sunburst sunburst  394 23 mars  10:38 renameFiles.sh
```

- changer les droit
`chmod o=r-- script.sh` : other
`chmod g=r-- script.sh` : groups
`chmod u=r-- script.sh` : user

- changer l'utilisateur et le groupe d'un fichier
`chown sunburst:docker docker-compose.yml`

## Pratique

### XX - Exo Bonus

- demande à l'utilisateur d'entrer un chemin d'accès vers un fichier ou un dossier, 
- affiche les permissions associées au fichier/dossier.
- Si l'utilisateur est le propriétaire du fichier/dossier, 
- le script doit également demander s'il souhaite modifier les permissions et, le cas échéant, les modifier en fonction des réponses de l'utilisateur.

### 05 - PERMISSIONS MATRIX

#### MATRICE DE PERMISSION D’UN FICHIER

- Récupérez la ressource "permisisonsmatrix.zip" depuis l’onglet dédié sur Ariane.


- Créez un script "permissionsMatrix.sh" qui prend en paramètre un chemin d’accès vers un fichier et qui affiche les droits d’accès associés en respectant le format suivant (où 0 signifie vrai et 1 signifie faux).

```
./permissionsMatrix.sh passwords.txt


File: passwords.txt

Permissions: rw-r--r--


        READ    WRITE   EXECUTE

USER     0        0        1

GROUP    0        1        1

OTHER    0        1        1
```



#### MATRICE DE PERMISSION D’UN DOSSIER

- Créez un second script "permissionsMatrixFolder.sh" qui prend en paramètre un chemin d’accès vers un dossier de l’ordinateur et qui affiche les droits d’accès associés à chacun des fichiers de ce dossier, en respectant le format suivant.

```
./permissionsMatrix.sh myfolder


File: passwords.txt

Permissions: rw-r--r--


        READ    WRITE   EXECUTE

USER     0        0        1

GROUP    0        1        1

OTHER    0        1        1


File: myscript.sh

Permissions: rwxr-xr-x


        READ    WRITE   EXECUTE

USER     0        0        0

GROUP    0        1        0

OTHER    0        1        0
```


### 04 - Count files

#### COMPTER LE NOMBRE DE FICHIERS PAR UTILISATEUR

- Créez un script "countFiles.sh" qui prend en paramètre un chemin d’accès vers un dossier

- afin d’afficher le nombre de fichiers contenus dans le répertoire pour chaque propriétaire.


Exemple de réponse :


root 1

user1 2

user2 5

nobody 1



### 03 PERMISSIONS CHECKER

#### VÉRIFICATION DES DROITS D’EXÉCUTION D’UN FICHIER

- Créez un script "checkPermissions.sh" prenant en paramètre un chemin d’accès vers un fichier afin de vérifier s’il existe et ses permissions.


- Si le fichier n’existe pas, le message suivant sera affiché : "FILE does not exist"

- Si l’utilisateur actuel dispose des droits d’exécutions sur le fichier, le message suivant sera affiché : "You have permissions to execute FILE" sinon "You do not have permissions to execute FILE".

### 02 - EASY PERMISSIONS

#### MODIFICATION DES PROPRIÉTAIRES D’UN ENSEMBLE DE FICHIERS

- Récupérez la ressource "easypermissions.zip" depuis l’onglet dédié sur Ariane.


- Créez l’utilisateur et le groupe "johncena" grâce aux commandes suivantes.

```
sudo useradd johncena

sudo groupadd johncena
```

- Ajoutez l’utilisateur "johncena" au groupe éponyme.


`sudo usermod -aG johncena johncena`


- Créez un script easyOwner.sh chargé de lire l’entrée du terminal (via la commande read) afin de récupérer le chemin vers un ou plusieurs fichiers, puis le nouveau propriétaire et le nouveau groupe de ces fichiers.

```
Files to update :

> file1 file2 file3


New owner and group :

> johncena


file1 owner "engineer" changed to "johncena"

file2 owner "engineer" changed to "johncena"

file3 owner "engineer" changed to "johncena"
```

#### MODIFICATION DES DROITS D’UN ENSEMBLE DE FICHIERS

- Créez un second script easyPermissions.sh chargé de lire l’entrée du terminal afin de récupérer le chemin vers un ou plusieurs fichiers, puis les nouvelles permissions au format "XXX", en suivant une représentation en octal.

```
Files to update :

> file1 file2 file3


New permission :

> 444


file1 permissions changed to 444

file2 permissions changed to 444
```

### 01 - PLAY WITH PERMISSIONS

#### LES PERMISSIONS

> Il peut arriver d’être limité dans une action par le terminal, car nous ne disposons pas des droits suffisants pour exécuter un script ou une commande système.


> La commande sudo (superuser do) permet de pallier ce problème en donnant temporairement les droits d’administrateur à un utilisateur, via son mot de passe, afin que celui-ci puisse exécuter certaines commandes.


- Récupérez la ressource "playwithpermissions.zip" depuis l’onglet dédié sur Ariane.


- Regardez dans le manuel (commande man) ce que permet de faire l’option "-l" de la commande ls, puis exécutez la commande suivante.

```
ls -l


Pour chaque fichier, vous obtenez une chaîne de caractères sous ce format :


rw-r--r--
\ /\ /\ /
v  v  v
|  |  droits des autres utilisateurs (o)
|  |
|  droits des utilisateurs appartenant au groupe (g)
|
droits du propriétaire (u)


r : read | w : write | x : execute
```

- Vous constaterez que chaque fichier dispose de certaines permissions par défaut : le propriétaire (vous) peut lire et écrire dans ces fichiers tandis que les utilisateurs du groupe lié au fichier et tous les autres utilisateurs peuvent seulement les lire.


- La troisième et quatrième colonne représentent respectivement l’utilisateur et le groupe propriétaire du fichier.


#### MODIFICATION DES PERMISSIONS

> Les permissions sont regroupées par paquets de 3 caractères (r, w, x ou un tiret). Imaginons qu’on souhaite modifier les permissions du propriétaire du fichier représentés par les 3 premiers caractères.


- Essayez d’exécuter le script "unexec.sh" grâce à la commande suivante.

```
./unexec.sh
```

- Vous ne devrez pas être en mesure de pouvoir l’exécuter, car le propriétaire dispose seulement des droits en lecture et en écriture (les caractères r et w).


- Modifiez les permissions du propriétaire (avec u=) grâce à la commande suivante.

```
chmod u=rwx unexec.sh
```

- Vérifiez la mise à jour des permissions grâce à la commande ls.


- Vérifiez que le script "unexec.sh" peut maintenant s’exécuter.


#### PROPRIÉTAIRES ET PERMISSIONS

> Intéressons-nous à l’influence entre les permissions et le droit de propriété d’un fichier.


- Modifiez le propriétaire du fichier "unexec.sh" en le remplaçant par l’utilisateur "nobody" (qui est un faux utilisateur créé par votre système) grâce à la commande suivante.


`sudo chown nobody unexec.sh`


> Le préfixe sudo est nécessaire car nous modifions le propriétaire d’un fichier.


- Essayez d’exécuter le script "unexec.sh".

> Vous ne devrez pas être en mesure de pouvoir l’exécuter, car vous n’êtes plus le propriétaire du fichier et le groupe (qui est inchangé et dont vous êtes membre) n’a pas les permissions d’exécution.


- Modifiez les permissions du groupé lié au fichier (avec g=) grâce à la commande suivante.


`sudo chmod g=rx unexec.sh`


- Vérifiez que le script unexec.sh peut maintenant s’exécuter.


> Remarque : jusqu’à présent, pour rendre exécutable un script nous utilisions la commande chmod +x nomDuFichier. Cela permet de rendre le fichier exécutable quel que soit l’utilisateur courant en une seule commande.