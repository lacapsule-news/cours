#!/usr/bin/env bash

for ARG in $@;do
	if [ -e $ARG ];then
		permsOctal=$(stat -c '%a' $ARG)
		permsHr=$(stat -c '%A' $ARG)

		uRead=$(if [ -r $ARG ];then echo '1';else echo '0';fi)
		uWrite=$(if [ -w $ARG ];then echo '1';else echo '0';fi)
		uExec=$(if [ -x $ARG ];then echo '1';else echo '0';fi)

		gRead=$(if [ ${permsHr:4:1} == "r" ];then echo '1';else echo '0';fi)
		gWrite=$(if [ ${permsHr:5:1} == "w" ];then echo '1';else echo '0';fi)
		gExec=$(if [ ${permsHr:6:1} == "x" ];then echo '1';else echo '0';fi)

		oRead=$(if [ ${permsHr:7:1} == "r" ];then echo '1';else echo '0';fi)
		oWrite=$(if [ ${permsHr:8:1} == "w" ];then echo '1';else echo '0';fi)
		oExec=$(if [ ${permsHr:9:1} == "x" ];then echo '1';else echo '0';fi)		

		echo "File : "$ARG
		echo "Permission : "$permsOctal","$permsHr
		echo "	READ	WRITE	EXECUTE"
		echo "User	"$uRead"	"$uWrite"	"$uExec
		echo "Group	"$gRead"	"$gWrite"	"$gExec
		echo "OTHER	"$oRead"	"$oWrite"	"$oExec
		echo "------"
	else
		echo "File or folder do not exist"
	fi
done
