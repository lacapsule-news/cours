# PERMISSIONS MATRIX

## MATRICE DE PERMISSION D’UN FICHIER

- Récupérez la ressource "permisisonsmatrix.zip" depuis l’onglet dédié sur Ariane.


- Créez un script "permissionsMatrix.sh" qui prend en paramètre un chemin d’accès vers un fichier et qui affiche les droits d’accès associés en respectant le format suivant (où 0 signifie vrai et 1 signifie faux).

```
./permissionsMatrix.sh passwords.txt


File: passwords.txt

Permissions: rw-r--r--


        READ    WRITE   EXECUTE

USER     0        0        1

GROUP    0        1        1

OTHER    0        1        1
```



## MATRICE DE PERMISSION D’UN DOSSIER

- Créez un second script "permissionsMatrixFolder.sh" qui prend en paramètre un chemin d’accès vers un dossier de l’ordinateur et qui affiche les droits d’accès associés à chacun des fichiers de ce dossier, en respectant le format suivant.

```
./permissionsMatrix.sh myfolder


File: passwords.txt

Permissions: rw-r--r--


        READ    WRITE   EXECUTE

USER     0        0        1

GROUP    0        1        1

OTHER    0        1        1


File: myscript.sh

Permissions: rwxr-xr-x


        READ    WRITE   EXECUTE

USER     0        0        0

GROUP    0        1        0

OTHER    0        1        0
```
