# Public bank

## PREMIERS TESTS

👉 Clonez le repository suivant sur GitLab : https://gitlab.com/la-capsule-bootcamp/public-bank

👉 Exécutez les commandes suivantes afin d’installer les dépendances et lancer l’application.

cd public-bank
yarn install

yarn dev


Vous devrez compléter le code et créer vos premiers tests qui porteront sur l’authentification dans le fichier "app.spec.ts", dans le dossier "cypress/tests".

👉 Créez un test qui simule la connexion (sign-in) avec nom d’utilisateur et mot de passe valides sur l’URL http://localhost:3000 :

```
Nom d’utilisateur : johndoe
Mot de passe : s3cret
```

👉 Créez un test qui simule un sign-in avec un mauvais mot de passe. Dans ce cas, on souhaite s’assurer que l’utilisateur reçoit un message d’erreur et n’ait pas accès à l’interface de la banque.


Vous visitez la même URL dans chaque test. Or, il existe des outils sur Cypress pour éviter la redondance de code.

👉 Utilisez la fonction de Cypress "beforeEach()" pour vous connecter à l’URL "http://localhost:3000" avant chaque test.


👉 Créez un test qui vérifie que la déconnexion fonctionne correctement.

## En suplement perso / Faire tourner la pipeline sur gitlab


- Ajout du runner au projet

```
sudo gitlab-runner register --url https://gitlab.com/ --registration-token $REGISTRATION_TOKEN
```

- Changer les remote

```
git remote remove origin
git remote add origin git@gitlab.com:5unburst/public-bank.git
```

1. pipeline error : The chromium binary is not available for arm64

Dans le readme une section mac M1 (utilisant des puce arm) parle de la variable d'environement `PUPPETEER_SKIP_CHROMIUM_DOWNLOAD: "true"`

- Ajouter PUPPETEER_SKIP_CHROMIUM_DOWNLOAD dans gitlab-ci

```
variables:
  npm_config_cache: "$CI_PROJECT_DIR/.npm"
  CYPRESS_CACHE_FOLDER: "$CI_PROJECT_DIR/cache/Cypress"
  PUPPETEER_SKIP_CHROMIUM_DOWNLOAD: "true"
```

2. Modules error : cypress/support/commands.ts(1,1): error TS1208: 'commands.ts' cannot be compiled under '--isolatedModules'

- Changer "isolatedModules": true a false dans tsconfig.json

```
    "module": "esnext",
    "moduleResolution": "node",
    "resolveJsonModule": true,
    "isolatedModules": false,
    "noEmit": true,
    "jsx": "react-jsx",
    "noFallthroughCasesInSwitch": true,
```

3. Compilation erro : src/components/Footer.tsx Line 11:73:  Duplicate key 'marginTop'  no-dupe-keys

- Dans le fichier src/components/Footer.tsx changé les 2 margin top pour un seul qui vaut la valeur des deux et on verra bien.
```
old$:style={{ marginTop: 50, display: "flex", flexDirection: "column", marginTop: "-30" }}
new$:style={{ marginTop: 20, display: "flex", flexDirection: "column"}}
```

4. Test error : Firefox et chrome non présent dans les image

- Modifier les module de pipeline chrome, firefox et api de la manière décrite ci-dessous:
    - L'api utilisera electron
    - ui-chrome deviendra ui electron
    - ui-firefox devra juste installer ca dépendances

```
ui-electron:
  image: cypress/browsers:node16.14.2-slim-chrome100-ff99-edge
  stage: test
  script:
    - yarn start:ci & npx wait-on http://localhost:3000
    - npx cypress run --browser electron --spec "cypress/tests/ui/*"

ui-electron-mobile:
  image: cypress/browsers:node16.14.2-slim-chrome100-ff99-edge
  stage: test
  script:
    - yarn start:ci & npx wait-on http://localhost:3000
    - npx cypress run --browser electron --spec "cypress/tests/ui/*" --config "viewportWidth=375,viewportHeight=667"

ui-firefox:
  image: cypress/browsers:node16.14.2-slim-chrome100-ff99-edge
  stage: test
  script:
    - apt update
    - apt install -y firefox-esr
    - yarn start:ci & npx wait-on http://localhost:3000
    - npx cypress run --browser firefox --spec "cypress/tests/ui/*"

ui-firefox-mobile:
  image: cypress/browsers:node16.14.2-slim-chrome100-ff99-edge
  stage: test
  script:
    - apt update
    - apt install -y firefox-esr
    - yarn start:ci & npx wait-on http://localhost:3000
    - npx cypress run --browser firefox --spec "cypress/tests/ui/*" --config "viewportWidth=375,viewportHeight=667"
```

5. Syntax error: Lint

Il y a une fonction dans la pipeline `npx prettier --check` qui peut poser probleme lors du debug.

Afin de mettre notre code en condition il est nécessaire de réalisé un `npx prettier --write .`

6. Optimisation des test

Vue que ce sont des test sur navigateur je n'ai pas vue l'utilité de séparer les test ce qui descend le temps de pipeline de 1h30 a 25 minutes. Les test aurait étais beaucoup plus rapide sur les runner gitlab mais j'utilise personellement un rpi comme runner et vais surmment passer a deux.

```
realcondition-browser:
  image: cypress/browsers:node16.14.2-slim-chrome100-ff99-edge
  stage: test
  script:
    - apt update
    - apt install -y firefox
    - yarn start:ci & npx wait-on http://localhost:3000
    - npx cypress run --browser electron --spec "cypress/tests/ui/*"
    - npx cypress run --browser electron --spec "cypress/tests/ui/*" --config "viewportWidth=375,viewportHeight=667"
    - npx cypress run --browser firefox --spec "cypress/tests/ui/*"
    - npx cypress run --browser firefox --spec "cypress/tests/ui/*" --config "viewportWidth=375,viewportHeight=667"
```

![commit](../../images/pipeline.public.bank.png)