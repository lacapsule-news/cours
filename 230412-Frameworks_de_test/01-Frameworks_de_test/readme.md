# Frameworks de test

## Contexte

Chaque évolution ou correction d'une application peut engendrer des bugs.

La phase de test est une étape cruciale, mais elle nécessite beaucoup de temps et de rigueur.

On peut catégoriser les tests en 3 types :
- **Test unitaires** Test sur un module de code (une fonction).
- **Tests d'intégrations** Tests sur l'ensemble de module.
- **Tests End-to-end(E2E)** Test sur un scénario.

Ces différents tests peuvent être exécutés automatiquement via des outils dédiés.

### Fonctionnement

La mise en place du framework Cypress fournira un écosystème facilitant la gestion et l'exécution des tests.

Les tests sont des instructions écrites dans un langage spécifique et stockées dans un fichier.
Les étapes de chaque test seront détaillées dans ce fichier.

Les étapes de chaque test seront détaillées dans ce fichier.

Via Gitlab CI/CD, l'exécution des test pourra être déclenchée à chaque push.

Si un des test n'est pas validé, la fusion de la branche est bloquée et le détail du test qui a échoué sera disponible via Gitlab CI/CD.