# Frameworks de test

## Contexte

Chaque évolution ou correction d'une application peut engendrer des bugs.

La phase de test est une étape cruciale, mais elle nécessite beaucoup de temps et de rigueur.

On peut catégoriser les tests en 3 types :
- **Test unitaires** Test sur un module de code (une fonction).
- **Tests d'intégrations** Tests sur l'ensemble de module.
- **Tests End-to-end(E2E)** Test sur un scénario.

Ces différents tests peuvent être exécutés automatiquement via des outils dédiés.

### Fonctionnement

La mise en place du framework Cypress fournira un écosystème facilitant la gestion et l'exécution des tests.

Les tests sont des instructions écrites dans un langage spécifique et stockées dans un fichier.
Les étapes de chaque test seront détaillées dans ce fichier.

Les étapes de chaque test seront détaillées dans ce fichier.

Via Gitlab CI/CD, l'exécution des test pourra être déclenchée à chaque push.

Si un des test n'est pas validé, la fusion de la branche est bloquée et le détail du test qui a échoué sera disponible via Gitlab CI/CD.

## Exo

### 06 - Show me the bug

#### CRÉATION DES TESTS

👉 Dans le fichier "debug-failing-test.cy.js" corrigez les erreurs de syntaxe afin de valider les tests.

👉 Complétez le fichier de tests "cypress-commands.cy.js" qui appellera deux commandes Cypress :

- getAllPosts() : récupère tous les posts renvoyé par l’API sur l’endpoint /api/posts
- getFirstPost() : récupère le premier post renvoyé par l’API sur l’endpoint /api/posts

Les commandes sont à déclarer dans le fichier de commandes à l’emplacement "support/commands.js"

Vous aurez sans doute besoin des documentations sur les méthodes cy.request()

et cy.wrap().

#### TEST DES REQUÊTES À L’API

Cypress permet de tester directement les endpoints de votre backend, notamment avec la méthode cy.request() qui prend la forme suivante :

```
cy.request("GET", "url_endpoint").then((response) => {

  ...

});
```

👉 Complétez le fichier de tests "network-requests.cy.js" en utilisant la méthode cy.request().
Ne modifiez pas le contenu de la méthode beforeEach().

### 05 - Cypress scraper

#### DATA SCRAPING

Sachez qu’il n’est officiellement pas possible de récupérer les données d’une application si celle-ci ne propose pas d’API (webservice) publique.
Il est parfois nécessaire de recourir au scraping pour extraire les données d’un site web. et Cypress peut aller au-delà du testing end-to-end et devenir un véritable outil de scraping !

👉 Utilisez Cypress sur un nouveau projet pour scraper les tarifs des hôtels à Londres pour la nuit du 31 décembre au 1er janvier. Contentez-vous de la première page de résultats

👉 Calculez le tarif moyen sur cette page et affichez-le dans les logs de Cypress.

👉 Toujours dans les logs de Cypress, affichez le nom de l’hôtel le mieux noté, et le nombre total de logements disponibles à ces dates sur Booking.com.

### 04 - Public bank

#### PREMIERS TESTS

👉 Clonez le repository suivant sur GitLab : https://gitlab.com/la-capsule-bootcamp/public-bank

👉 Exécutez les commandes suivantes afin d’installer les dépendances et lancer l’application.

cd public-bank
yarn install

yarn dev


Vous devrez compléter le code et créer vos premiers tests qui porteront sur l’authentification dans le fichier "app.spec.ts", dans le dossier "cypress/tests".

👉 Créez un test qui simule la connexion (sign-in) avec nom d’utilisateur et mot de passe valides sur l’URL http://localhost:3000 :

```
Nom d’utilisateur : johndoe
Mot de passe : s3cret
```

👉 Créez un test qui simule un sign-in avec un mauvais mot de passe. Dans ce cas, on souhaite s’assurer que l’utilisateur reçoit un message d’erreur et n’ait pas accès à l’interface de la banque.


Vous visitez la même URL dans chaque test. Or, il existe des outils sur Cypress pour éviter la redondance de code.

👉 Utilisez la fonction de Cypress "beforeEach()" pour vous connecter à l’URL "http://localhost:3000" avant chaque test.


👉 Créez un test qui vérifie que la déconnexion fonctionne correctement.

#### En suplement perso / Faire tourner la pipeline sur gitlab


- Ajout du runner au projet

```
sudo gitlab-runner register --url https://gitlab.com/ --registration-token $REGISTRATION_TOKEN
```

- Changer les remote

```
git remote remove origin
git remote add origin git@gitlab.com:5unburst/public-bank.git
```

1. pipeline error : The chromium binary is not available for arm64

Dans le readme une section mac M1 (utilisant des puce arm) parle de la variable d'environement `PUPPETEER_SKIP_CHROMIUM_DOWNLOAD: "true"`

- Ajouter PUPPETEER_SKIP_CHROMIUM_DOWNLOAD dans gitlab-ci

```
variables:
  npm_config_cache: "$CI_PROJECT_DIR/.npm"
  CYPRESS_CACHE_FOLDER: "$CI_PROJECT_DIR/cache/Cypress"
  PUPPETEER_SKIP_CHROMIUM_DOWNLOAD: "true"
```

2. Modules error : cypress/support/commands.ts(1,1): error TS1208: 'commands.ts' cannot be compiled under '--isolatedModules'

- Changer "isolatedModules": true a false dans tsconfig.json

```
    "module": "esnext",
    "moduleResolution": "node",
    "resolveJsonModule": true,
    "isolatedModules": false,
    "noEmit": true,
    "jsx": "react-jsx",
    "noFallthroughCasesInSwitch": true,
```

3. Compilation erro : src/components/Footer.tsx Line 11:73:  Duplicate key 'marginTop'  no-dupe-keys

- Dans le fichier src/components/Footer.tsx changé les 2 margin top pour un seul qui vaut la valeur des deux et on verra bien.
```
old$:style={{ marginTop: 50, display: "flex", flexDirection: "column", marginTop: "-30" }}
new$:style={{ marginTop: 20, display: "flex", flexDirection: "column"}}
```

4. Test error : Firefox et chrome non présent dans les image

- Modifier les module de pipeline chrome, firefox et api de la manière décrite ci-dessous:
    - L'api utilisera electron
    - ui-chrome deviendra ui electron
    - ui-firefox devra juste installer ca dépendances

```
ui-electron:
  image: cypress/browsers:node16.14.2-slim-chrome100-ff99-edge
  stage: test
  script:
    - yarn start:ci & npx wait-on http://localhost:3000
    - npx cypress run --browser electron --spec "cypress/tests/ui/*"

ui-electron-mobile:
  image: cypress/browsers:node16.14.2-slim-chrome100-ff99-edge
  stage: test
  script:
    - yarn start:ci & npx wait-on http://localhost:3000
    - npx cypress run --browser electron --spec "cypress/tests/ui/*" --config "viewportWidth=375,viewportHeight=667"

ui-firefox:
  image: cypress/browsers:node16.14.2-slim-chrome100-ff99-edge
  stage: test
  script:
    - apt update
    - apt install -y firefox-esr
    - yarn start:ci & npx wait-on http://localhost:3000
    - npx cypress run --browser firefox --spec "cypress/tests/ui/*"

ui-firefox-mobile:
  image: cypress/browsers:node16.14.2-slim-chrome100-ff99-edge
  stage: test
  script:
    - apt update
    - apt install -y firefox-esr
    - yarn start:ci & npx wait-on http://localhost:3000
    - npx cypress run --browser firefox --spec "cypress/tests/ui/*" --config "viewportWidth=375,viewportHeight=667"
```

5. Syntax error: Lint

Il y a une fonction dans la pipeline `npx prettier --check` qui peut poser probleme lors du debug.

Afin de mettre notre code en condition il est nécessaire de réalisé un `npx prettier --write .`

6. Optimisation des test

Vue que ce sont des test sur navigateur je n'ai pas vue l'utilité de séparer les test ce qui descend le temps de pipeline de 1h30 a 25 minutes. Les test aurait étais beaucoup plus rapide sur les runner gitlab mais j'utilise personellement un rpi comme runner et vais surmment passer a deux.

```
realcondition-browser:
  image: cypress/browsers:node16.14.2-slim-chrome100-ff99-edge
  stage: test
  script:
    - apt update
    - apt install -y firefox
    - yarn start:ci & npx wait-on http://localhost:3000
    - npx cypress run --browser electron --spec "cypress/tests/ui/*"
    - npx cypress run --browser electron --spec "cypress/tests/ui/*" --config "viewportWidth=375,viewportHeight=667"
    - npx cypress run --browser firefox --spec "cypress/tests/ui/*"
    - npx cypress run --browser firefox --spec "cypress/tests/ui/*" --config "viewportWidth=375,viewportHeight=667"
```

![commit](../../images/pipeline.public.bank.png)

### 03 - Cypress Meets gitlab

#### SETUP

Vous allez construire des tests e2e et automatiser l’exécution de ces tests dans une pipeline GitLab sur une simple application de to-do list.

Pour intégrer Cypress à votre système d’intégration continue sur GitLab, consultez la documentation de Cypress :
https://docs.cypress.io/guides/continuous-integration/gitlab-ci

⚠️ Attention : GitLab limite l’usage de ses outils CI/CD à 400 minutes dans sa version gratuite.
Évitez les pushs inutiles et vérifiez bien que les jobs ne durent pas plus de quelques minutes (dans l’onglet CI/CD > Jobs) et stoppez les jobs qui ne sont pas nécessaires.

👉 Clonez le repository suivant sur GitLab : https://gitlab.com/la-capsule-bootcamp/todo-app

👉 Exécutez les commandes suivantes afin d’installer les dépendances et lancer l’application.

```
cd todo-app
yarn install
yarn start
```

👉 Arrêtez le projet (via Ctrl + C) puis supprimez la liaison entre le dépôt local et le dépôt distant.


👉 Liez votre dépôt local à un nouveau repository GitLab que vous aurez créé au préalable.

#### CRÉATION DES TESTS

👉 Lancez Cypress directement depuis le terminal.

`yarn run cypress:open`

👉 Créez des tests simples qui s’assurent que les features principales de la todo app fonctionnent :

Ajout d’une todo et affichage de celle-ci
Compteur du nombre de todos
Sélection d’une todo et actualisation automatique du compteur de todos sélectionnées

👉 Lancez ces tests en local avant de passer à l’étape suivante.

#### CRÉATION D’UNE PIPELINE

👉 Créez un fichier ".gitlab-ci.yml" chargé d’exécuter vos tests Cypress à chaque commit.
Vous devrez préciser l’image Docker suivante (à la première ligne) afin de partir sur un runner Linux avec Cypress préinstallé :

`image: cypress/browsers:latest`

👉 Pushez vers GitLab et suivez les logs du job en cours afin de vérifier que le runner exécute bien vos tests.

### 02 - CYPRESS BEGIN

#### INSTALLATION DE CYPRESS

Un test end-to-end (e2e) est un test logiciel qui a pour but de valider que le système testé répond correctement à un cas d’utilisation, mais également de vérifier l’intégration de ce cas dans l’interface.
L’intérêt de ces tests est de s’assurer qu’une fonctionnalité développée répond à la demande du point de vue de l’utilisateur.

Ce challenge constitue l’occasion de prendre en main le fonctionnement global de Cypress.

👉 Installez et configurez l’environnement d’exécution JavaScript Node.js en exécutant les 8 commandes suivantes.

```
sudo apt remove nodejs npm -y

curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.2/install.sh | bash
echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p

echo "export NODE_OPTIONS=--openssl-legacy-provider" >> ~/.bashrc
source ~/.bashrc
nvm install stable

nvm use stable
sudo npm install -g yarn
```

👉 Créez un nouveau dossier "cypressbegin" dans lequel vous initialisez un nouveau projet Node.js.

`npm init -y`

👉 Ajoutez les lignes suivantes dans le fichier "package.json" (à la place de l’objet "scripts" déjà présent).

```
"scripts": {

  "test": "npx cypress run --config video=false",

  "cypress:open": "npx cypress open"

},
```

👉 Lancez Cypress avec la commande suivante.

`yarn run cypress:open`

👉 Sélectionnez le testing end-to-end (e2e) puis le navigateur "Electron".

👉 Cypress est désormais installé. Sélectionnez l’option suivante afin de générer des tests d’exemple.

👉 Avant de construire vos propres tests, ces specs d’exemple vous donneront un bon aperçu du fonctionnement de Cypress et de sa syntaxe. Vous retrouvez les tests dans le dossier "cypress/e2e".

👉 Sur l’onglet "Specs", sélectionnez une suite de tests (finissant par .cy.js) et cliquez pour ouvrir le test runner qui exécutera le test.
Sachez qu’il est également possible de lancer un test sans passer par l’interface graphique, directement depuis le terminal.

👉 Avant de passer à l’étape suivante, supprimez tout le contenu du dossier "e2e".

#### VISITER UNE PAGE

Comme nous l’avons vu durant la journée consacrée à la gestion de projet, les user stories, au coeur du framework Agile, reposent sur un format qui ressemble à : "En tant qu’utilisateur, j’effectue cette action pour obtenir ce résultat".

Pour effectuer un test sur cette user story, vous allez simuler l'action de l’utilisateur à travers le test, puis vous assurer que l'état de l'application qui en découle correspond à vos attentes.

Les tests prennent la structure suivante :

```
describe("Nom de la suite de tests", () => {

  it("Nom du scénario", () => {


        // Code du scénario …


  }

});
```

👉 Créez un fichier "wikipedia.cy.js" dans le dossier "e2e" (depuis VS Code) et explorez la documentation de Cypress pour construire un scénario qui simulera simplement la navigation vers la page d'accueil de Wikipédia.

👉 Lancez le test depuis le terminal grâce à la commande suivante.

`yarn test`

👉 Vous pouvez également lancer le test dans le test runner (via l’interface graphique) pour visualiser l’exécution des instructions dans le navigateur.

`yarn run cypress:open`

#### ACCÉDER À L’ÉLÉMENT D’UNE PAGE

Pour accéder à l’élément d’une page, vous pouvez utiliser l’inspecteur de la console de votre navigateur (Outils du développeur sur Chrome) en faisant un clic droit puis "Inspecter".

Les commandes d’accès aux éléments prennent souvent le format suivant :

```
cy.get("locator")
cy.contains("texte")
```

👉 Pour manipuler facilement les éléments de la page, installez l’extension Testing Playground sous Chrome. Celle-ci vous permettra de pointer efficacement sur l’élément souhaité et vous renverra la syntaxe appropriée.

Il est également possible d’utiliser la "cible" dans le code runner de Cypress pour pointer les locators.

👉 Complétez votre test afin qu’il accède au champ de recherche.

```
describe("Testing Wikipedia", () => {

  it("Search for content", () => {

    cy.visit("https://fr.wikipedia.org/");

    cy.get("input[type='search']")

  });

});
```

#### INTERAGIR AVEC DES ÉLÉMENTS

👉 Ajoutez la saisie du mot-clé "DevOps" et le clic sur le bouton "Rechercher".

```
describe("Testing Wikipedia", () => {

  it("Search for content", () => {

    cy.visit("https://fr.wikipedia.org/");

    cy.get("input[type='search']").type("DevOps");

    cy.get(".cdx-button").click();

  });

});
```

👉 Vérifiez que le lancement de la recherche mène bien à une page contenant le mot DevOps.

```
describe("Testing Wikipedia", () => {

  it("Search for content", () => {

    cy.visit("https://fr.wikipedia.org/");

    cy.get("input[type='search']").type("DevOps");

    cy.get(".cdx-button").click();
   cy.contains("DevOps");

  });

});
```

👉 Créez un nouveau fichier "switch_language.cy.js" et construisez un test qui simulera le changement de langue sur la page d’accueil de wikipedia pour le polonais. Assurez-vous d’être bien arrivé sur la page qui contiendra "Bienvenue sur Wikipedia" en polonais.
Notez qu’il faut parfois forcer l’attente via l’instruction cy.wait().