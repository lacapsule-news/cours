# Cypress Meets gitlab

## SETUP

Vous allez construire des tests e2e et automatiser l’exécution de ces tests dans une pipeline GitLab sur une simple application de to-do list.

Pour intégrer Cypress à votre système d’intégration continue sur GitLab, consultez la documentation de Cypress :
https://docs.cypress.io/guides/continuous-integration/gitlab-ci

⚠️ Attention : GitLab limite l’usage de ses outils CI/CD à 400 minutes dans sa version gratuite.
Évitez les pushs inutiles et vérifiez bien que les jobs ne durent pas plus de quelques minutes (dans l’onglet CI/CD > Jobs) et stoppez les jobs qui ne sont pas nécessaires.

👉 Clonez le repository suivant sur GitLab : https://gitlab.com/la-capsule-bootcamp/todo-app

👉 Exécutez les commandes suivantes afin d’installer les dépendances et lancer l’application.

```
cd todo-app
yarn install
yarn start
```

👉 Arrêtez le projet (via Ctrl + C) puis supprimez la liaison entre le dépôt local et le dépôt distant.


👉 Liez votre dépôt local à un nouveau repository GitLab que vous aurez créé au préalable.

## CRÉATION DES TESTS

👉 Lancez Cypress directement depuis le terminal.

`yarn run cypress:open`

👉 Créez des tests simples qui s’assurent que les features principales de la todo app fonctionnent :

Ajout d’une todo et affichage de celle-ci
Compteur du nombre de todos
Sélection d’une todo et actualisation automatique du compteur de todos sélectionnées

👉 Lancez ces tests en local avant de passer à l’étape suivante.

## CRÉATION D’UNE PIPELINE

👉 Créez un fichier ".gitlab-ci.yml" chargé d’exécuter vos tests Cypress à chaque commit.
Vous devrez préciser l’image Docker suivante (à la première ligne) afin de partir sur un runner Linux avec Cypress préinstallé :

`image: cypress/browsers:latest`

👉 Pushez vers GitLab et suivez les logs du job en cours afin de vérifier que le runner exécute bien vos tests.