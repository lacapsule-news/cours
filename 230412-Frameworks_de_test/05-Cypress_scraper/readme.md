# Cypress scraper

## DATA SCRAPING

Sachez qu’il n’est officiellement pas possible de récupérer les données d’une application si celle-ci ne propose pas d’API (webservice) publique.
Il est parfois nécessaire de recourir au scraping pour extraire les données d’un site web. et Cypress peut aller au-delà du testing end-to-end et devenir un véritable outil de scraping !

👉 Utilisez Cypress sur un nouveau projet pour scraper les tarifs des hôtels à Londres pour la nuit du 31 décembre au 1er janvier. Contentez-vous de la première page de résultats

👉 Calculez le tarif moyen sur cette page et affichez-le dans les logs de Cypress.

👉 Toujours dans les logs de Cypress, affichez le nom de l’hôtel le mieux noté, et le nombre total de logements disponibles à ces dates sur Booking.com.