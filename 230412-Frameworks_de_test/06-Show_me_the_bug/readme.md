# Show me the bug

## CRÉATION DES TESTS

👉 Dans le fichier "debug-failing-test.cy.js" corrigez les erreurs de syntaxe afin de valider les tests.

👉 Complétez le fichier de tests "cypress-commands.cy.js" qui appellera deux commandes Cypress :

- getAllPosts() : récupère tous les posts renvoyé par l’API sur l’endpoint /api/posts
- getFirstPost() : récupère le premier post renvoyé par l’API sur l’endpoint /api/posts

Les commandes sont à déclarer dans le fichier de commandes à l’emplacement "support/commands.js"

Vous aurez sans doute besoin des documentations sur les méthodes cy.request()

et cy.wrap().

## TEST DES REQUÊTES À L’API

Cypress permet de tester directement les endpoints de votre backend, notamment avec la méthode cy.request() qui prend la forme suivante :

```
cy.request("GET", "url_endpoint").then((response) => {

  ...

});
```

👉 Complétez le fichier de tests "network-requests.cy.js" en utilisant la méthode cy.request().
Ne modifiez pas le contenu de la méthode beforeEach().