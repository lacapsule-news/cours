# CYPRESS BEGIN

## INSTALLATION DE CYPRESS

Un test end-to-end (e2e) est un test logiciel qui a pour but de valider que le système testé répond correctement à un cas d’utilisation, mais également de vérifier l’intégration de ce cas dans l’interface.
L’intérêt de ces tests est de s’assurer qu’une fonctionnalité développée répond à la demande du point de vue de l’utilisateur.

Ce challenge constitue l’occasion de prendre en main le fonctionnement global de Cypress.

👉 Installez et configurez l’environnement d’exécution JavaScript Node.js en exécutant les 8 commandes suivantes.

```
sudo apt remove nodejs npm -y

curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.2/install.sh | bash
echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p

echo "export NODE_OPTIONS=--openssl-legacy-provider" >> ~/.bashrc
source ~/.bashrc
nvm install stable

nvm use stable
sudo npm install -g yarn
```

👉 Créez un nouveau dossier "cypressbegin" dans lequel vous initialisez un nouveau projet Node.js.

`npm init -y`

👉 Ajoutez les lignes suivantes dans le fichier "package.json" (à la place de l’objet "scripts" déjà présent).

```
"scripts": {

  "test": "npx cypress run --config video=false",

  "cypress:open": "npx cypress open"

},
```

👉 Lancez Cypress avec la commande suivante.

`yarn run cypress:open`

👉 Sélectionnez le testing end-to-end (e2e) puis le navigateur "Electron".

👉 Cypress est désormais installé. Sélectionnez l’option suivante afin de générer des tests d’exemple.

👉 Avant de construire vos propres tests, ces specs d’exemple vous donneront un bon aperçu du fonctionnement de Cypress et de sa syntaxe. Vous retrouvez les tests dans le dossier "cypress/e2e".

👉 Sur l’onglet "Specs", sélectionnez une suite de tests (finissant par .cy.js) et cliquez pour ouvrir le test runner qui exécutera le test.
Sachez qu’il est également possible de lancer un test sans passer par l’interface graphique, directement depuis le terminal.

👉 Avant de passer à l’étape suivante, supprimez tout le contenu du dossier "e2e".

## VISITER UNE PAGE

Comme nous l’avons vu durant la journée consacrée à la gestion de projet, les user stories, au coeur du framework Agile, reposent sur un format qui ressemble à : "En tant qu’utilisateur, j’effectue cette action pour obtenir ce résultat".

Pour effectuer un test sur cette user story, vous allez simuler l'action de l’utilisateur à travers le test, puis vous assurer que l'état de l'application qui en découle correspond à vos attentes.

Les tests prennent la structure suivante :

```
describe("Nom de la suite de tests", () => {

  it("Nom du scénario", () => {


        // Code du scénario …


  }

});
```

👉 Créez un fichier "wikipedia.cy.js" dans le dossier "e2e" (depuis VS Code) et explorez la documentation de Cypress pour construire un scénario qui simulera simplement la navigation vers la page d'accueil de Wikipédia.

👉 Lancez le test depuis le terminal grâce à la commande suivante.

`yarn test`

👉 Vous pouvez également lancer le test dans le test runner (via l’interface graphique) pour visualiser l’exécution des instructions dans le navigateur.

`yarn run cypress:open`

## ACCÉDER À L’ÉLÉMENT D’UNE PAGE

Pour accéder à l’élément d’une page, vous pouvez utiliser l’inspecteur de la console de votre navigateur (Outils du développeur sur Chrome) en faisant un clic droit puis "Inspecter".

Les commandes d’accès aux éléments prennent souvent le format suivant :

```
cy.get("locator")
cy.contains("texte")
```

👉 Pour manipuler facilement les éléments de la page, installez l’extension Testing Playground sous Chrome. Celle-ci vous permettra de pointer efficacement sur l’élément souhaité et vous renverra la syntaxe appropriée.

Il est également possible d’utiliser la "cible" dans le code runner de Cypress pour pointer les locators.

👉 Complétez votre test afin qu’il accède au champ de recherche.

```
describe("Testing Wikipedia", () => {

  it("Search for content", () => {

    cy.visit("https://fr.wikipedia.org/");

    cy.get("input[type='search']")

  });

});
```

## INTERAGIR AVEC DES ÉLÉMENTS

👉 Ajoutez la saisie du mot-clé "DevOps" et le clic sur le bouton "Rechercher".

```
describe("Testing Wikipedia", () => {

  it("Search for content", () => {

    cy.visit("https://fr.wikipedia.org/");

    cy.get("input[type='search']").type("DevOps");

    cy.get(".cdx-button").click();

  });

});
```

👉 Vérifiez que le lancement de la recherche mène bien à une page contenant le mot DevOps.

```
describe("Testing Wikipedia", () => {

  it("Search for content", () => {

    cy.visit("https://fr.wikipedia.org/");

    cy.get("input[type='search']").type("DevOps");

    cy.get(".cdx-button").click();
   cy.contains("DevOps");

  });

});
```

👉 Créez un nouveau fichier "switch_language.cy.js" et construisez un test qui simulera le changement de langue sur la page d’accueil de wikipedia pour le polonais. Assurez-vous d’être bien arrivé sur la page qui contiendra "Bienvenue sur Wikipedia" en polonais.
Notez qu’il faut parfois forcer l’attente via l’instruction cy.wait().