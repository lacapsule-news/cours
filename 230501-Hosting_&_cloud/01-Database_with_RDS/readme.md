# DATABASE WITH RDS

## CONFIGURATION DU SERVICE

👉 À partir du service Relational Database Service (RDS) d’AWS, créez une base de données PostgreSQL configurée comme suit :

- Modèle : Offre gratuite
- Identifiant d'instance de base de données : myfirstdatabaseincloud
- Identifiant principal : postgres
- Mot de passe : Généré automatiquement par RDS
- Classe d’instance : db.t3.micro
- Stockage alloué : Désactiver la "Mise à l'échelle automatique du stockage"
- Connectivité : Ne pas se connecter à une ressource de calcul EC2
- Accès public : Oui
- Port de base de données : 5499
- Analyse des performances : Désactiver "Performance Insights"
- Configuration supplémentaire : Désactiver la "Mise à niveau automatique des versions mineures"

Les options de configuration qui ne sont pas précisées seront laissées à leur valeur par défaut.

👉 Pendant la création de la base de données, n’oubliez pas de récupérer les informations d’identifications, notamment le mot de passe généré automatiquement par RDS.

👉 Une fois la base de données déployée, créez un nouveau groupe de sécurité nommé "firstdatabase" à partir de la console EC2 afin d’autoriser le trafic entrant sur le port de la base de données PostgreSQL uniquement à partir de votre adresse IP.

👉 Modifiez les paramètres de l’instance de base de données afin d’utiliser le nouveau groupe de sécurité "firstdatabase". Appliquez immédiatement les modifications de la configuration.

## UTILISATION DU SERVICE

👉 Récupérez le point de terminaison afin de vous connecter à la base de données via la CLI psql.

👉 Une fois l'interpréteur de commande psql démarré, vérifiez la version utilisée par la base de données grâce à l’instruction suivante.

`postgres=# SELECT VERSION();`

👉 Modifiez les paramètres de la base de données afin de :

Sauvegarder automatiquement la base de données à 4h du matin
Conserver les sauvegardes automatiques pendant 1 jour (au lieu de 7 par défaut)

👉 Appliquez immédiatement les modifications de la configuration.

Le service RDS est basé sur un concept de sauvegarde nommé "instantanés" (ou snapshot). Ce système permet de restaurer rapidement une sauvegarde de base de données en créant une nouvelle instance en quelques clics.
Malheureusement, cette fonctionnalité n’est pas incluse dans l’offre gratuite d’AWS.

👉 Assurez-vous de résilier et libérer toutes les ressources crées afin d’éviter une surfacturation et de dépasser le tier gratuit d’AWS :

Base de données RDS : supprimez l’instance "myfirstdatabaseincloud"
Instantanés : supprimez tous les éventuels snapshots restants