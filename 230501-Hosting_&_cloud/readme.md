# Hosting & cloud

## Cours

Il existe un outils (aws cli) permettant de consulter les information d'aws via l'invité de commande.
Cela permet de travaillé seulment depuis sont invité de commande.
Nous avons vue comment le configurer via la commande `aws configure`.

## EXO

### 03 - CDN with cloudfront

#### SERVICE DE CDN

Le service CloudFront d’AWS est un CDN (Content Delivery Network) : il fonctionne avec S3 en copiant les fichiers stockés vers la périphérie des serveurs d’Amazon pour une récupération rapide grâce à leurs nombreux data centers.

Ce challenge a pour objectif de déployer un site statique où les fichiers et assets seront stockés par S3 tandis que CloudFront s’occupera d’attribuer une URL unique (en HTTPS), le tout optimisé grâce au réseau de distribution d’AWS.


👉 Récupérez la ressource "cdnwithcloudfront.zip" depuis l’onglet dédié sur Ariane.

👉 Créez un nouveau bucket S3 en le paramétrant de la même façon que le challenge précédent et uploadez le contenu du dossier "ecw-website" via la CLI d’AWS.

👉 Sur votre bucket S3, trouvez l’option permettant d’activer l’hébergement de site Web statique en précisant le document d’index "index.html".

👉 Assurez-vous que chaque objet contenu dans le bucket soit publique en lecture en modifiant la stratégie de compartiment (bucket policy) suivante dans l’onglet "Autorisations".

```
{

    "Version": "2012-10-17",

    "Statement": [

        {

            "Sid": "PublicReadGetObject",

            "Effect": "Allow",

            "Principal": "*",

            "Action": "s3:GetObject",

            "Resource": "arn:aws:s3:::BUCKET_NAME/*"

        }

    ]

}
```

👉 Visitez l’URL statique donnée par S3 en prenant soin d’ajouter "/index.html" à la fin.

👉 Sur la console du service CloudFront, créez une nouvelle distribution basée le domaine créé par le service S3. Vous pouvez laisser les paramètres par défaut à l’exception de la redirection forcée de HTTP vers HTTPS.


👉 Après quelques minutes (le temps que la distribution se propage), visitez l’URL statique donnée par CloudFront en prenant soin d’ajouter "/index.html" à la fin.
L’URL est censée vous rediriger automatiquement vers HTTPS.

👉 Assurez-vous de résilier et libérer toutes les ressources crées afin d’éviter une surfacturation et de dépasser le tier gratuit d’AWS :

CloudFront : désactiver la distribution puis supprimez-là quelques minutes plus tard (le bouton "Supprimer" sera grisé le temps que la désactivation fasse effet)
S3 : supprimez tous les objets uploadés pendant ce challenge puis le bucket en lui-même

### 02 - STORAGE WITH S3

#### SERVICE DE STOCKAGE

Le service S3 d’AWS est un service de stockage de données en ligne où on peut uploader (télécharger) des fichiers afin de récupérer une URL unique pour chaque fichier.

C’est un des services les plus populaires de la plateforme, car très utilisé par les développeurs pour stocker facilement les assets d’une application.

👉 À partir du service S3, créez un compartiment de stockage (bucket) en lui donnant le nom de votre choix.
Attention, ce nom doit être unique et ne pas être utilisé par un autre compte AWS.


👉 Lors de la création de votre bucket, laissez les options par défaut à l’exception de l’option "Bloquer tous les accès publics" qui doit être décochée : cela permettra de pouvoir stocker vos fichiers et les rendre accessibles depuis internet.


👉 Récupérez la ressource "storagewiths3.zip" depuis l’onglet dédié sur Ariane.


👉 Parmi toutes les images présentes dans le dossier "wwe", choisissez-en une seule afin de l’uploader vers S3 en laissant les options par défaut afin que le fichier soit public.


👉 Assurez-vous que chaque objet contenu dans le bucket soit publique en lecture en modifiant la stratégie de compartiment (bucket policy) suivante dans l’onglet "Autorisations".

```
{

    "Version": "2012-10-17",

    "Statement": [

        {

            "Sid": "PublicReadGetObject",

            "Effect": "Allow",

            "Principal": "*",

            "Action": "s3:GetObject",

            "Resource": "arn:aws:s3:::BUCKET_NAME/*"

        }

    ]

}
```

👉 Trouvez un moyen de récupérer l’URL publique du fichier précédemment uploadé et vérifiez que vous pouvez visionner l’image en ouvrant une nouvelle fenêtre en navigation privée.

👉 Avant de passer aux étapes suivantes, supprimez le fichier précédemment uploadé

👉 Installez la CLI d’AWS et vérifiez l’installation grâce à la commande suivante.

aws --version

👉 Via la CLI d’AWS, trouvez un moyen d’uploader tout le contenu du dossier "wwe" vers votre bucket.

👉 Vérifiez que l’upload a bien réussi en listant les objets présents, toujours via la CLI d’AWS.

👉 Assurez-vous de résilier et libérer toutes les ressources crées afin d’éviter une surfacturation et de dépasser le tier gratuit d’AWS :

S3 : supprimez tous les objets uploadés pendant ce challenge puis le bucket en lui même

### 01 - DATABASE WITH RDS

#### CONFIGURATION DU SERVICE

👉 À partir du service Relational Database Service (RDS) d’AWS, créez une base de données PostgreSQL configurée comme suit :

- Modèle : Offre gratuite
- Identifiant d'instance de base de données : myfirstdatabaseincloud
- Identifiant principal : postgres
- Mot de passe : Généré automatiquement par RDS
- Classe d’instance : db.t3.micro
- Stockage alloué : Désactiver la "Mise à l'échelle automatique du stockage"
- Connectivité : Ne pas se connecter à une ressource de calcul EC2
- Accès public : Oui
- Port de base de données : 5499
- Analyse des performances : Désactiver "Performance Insights"
- Configuration supplémentaire : Désactiver la "Mise à niveau automatique des versions mineures"

Les options de configuration qui ne sont pas précisées seront laissées à leur valeur par défaut.

👉 Pendant la création de la base de données, n’oubliez pas de récupérer les informations d’identifications, notamment le mot de passe généré automatiquement par RDS.

👉 Une fois la base de données déployée, créez un nouveau groupe de sécurité nommé "firstdatabase" à partir de la console EC2 afin d’autoriser le trafic entrant sur le port de la base de données PostgreSQL uniquement à partir de votre adresse IP.

👉 Modifiez les paramètres de l’instance de base de données afin d’utiliser le nouveau groupe de sécurité "firstdatabase". Appliquez immédiatement les modifications de la configuration.

#### UTILISATION DU SERVICE

👉 Récupérez le point de terminaison afin de vous connecter à la base de données via la CLI psql.

👉 Une fois l'interpréteur de commande psql démarré, vérifiez la version utilisée par la base de données grâce à l’instruction suivante.

`postgres=# SELECT VERSION();`

👉 Modifiez les paramètres de la base de données afin de :

Sauvegarder automatiquement la base de données à 4h du matin
Conserver les sauvegardes automatiques pendant 1 jour (au lieu de 7 par défaut)

👉 Appliquez immédiatement les modifications de la configuration.

Le service RDS est basé sur un concept de sauvegarde nommé "instantanés" (ou snapshot). Ce système permet de restaurer rapidement une sauvegarde de base de données en créant une nouvelle instance en quelques clics.
Malheureusement, cette fonctionnalité n’est pas incluse dans l’offre gratuite d’AWS.

👉 Assurez-vous de résilier et libérer toutes les ressources crées afin d’éviter une surfacturation et de dépasser le tier gratuit d’AWS :

Base de données RDS : supprimez l’instance "myfirstdatabaseincloud"
Instantanés : supprimez tous les éventuels snapshots restants