# Kubernetes

## Contexte

L'architecture doit être dupliquée sur plusieurs serveur de production pour assurer la haute disponibilité des services.

La gestion des différents serveur est etrêmement fastidieuse, car il faut intervenir manuellement sur chacun d'entre eux.

Kubernetes permet d'orchestrer tous les conteneurs répartis sur l'ensemble des serveur de productions.

### Fonctionnement

Un pod est un ensemble de conteneurs.
C'est l'équivalent du fonctionnemet de Docker Compose.

L'objectif est de dupliquer les pods sur plusieurs serveur afin de garantir leur disponibilité et d'optimiser le traffic.

Les pods sont administrés à travers des commandes qui sont reçu par le master node.

Le master node est un serveur à part dont le seul but est d'orchestrer les pods.

Un serveur peut contenir plusieurs pods, on parle alors de worker node.

Pour assurer le principe de haute disponibilité, les pods ne doivent pas être dupliqués sur le même serveur.

C'est le rôle du master node de répartir équitablement les pods entre les différents worker node.

Par exemple, si un des pods crash, il sera recrée automatiquement sur un worker node disponible.

Le cluster est un terme qui désigne le master node et l'ensemble des worker nodes.

## Exo

### 06 - They see me rolling update

#### ROLLING UPDATE

👉 Créez et appliquez un manifeste de déploiement en vous assurant que les contraintes suivantes soient respectées :

Le nom de l’application déployée sera "rollingapp"
Le nom du manifeste sera "rollingapp-deployment"
Le conteneur déployé sera nommé "nginx-hello", basé sur l’image "nginxdemos/hello" dans sa dernière version et devra exposer le port 80
3 pods devront être utilisés pour le déploiement du conteneur "nginx-hello"

👉 Créez et appliquez un service dédié au load balancing de l’application "rollingapp" déployée précédemment.

👉 Consultez la documentation pour la commande kubectl set afin de procéder à une mise à jour de l’application déployée en modifiant l’image utilisée par les conteneurs "nginx-hello".
La nouvelle version à déployée est basée sur une autre version de la même image taguée

"plain-text".

`kubectl set image deployment/rollingapp-deployment nginx-hello=nginxdemos/hello:plain-text`

Si vous vérifiez la liste des pods pendant que la mise à jour est déployée, vous constaterez que les "anciens" pods sont petit à petit supprimés pour laisser place à des nouveaux dotés de conteneurs basés sur la nouvelle version de l’image.

👉 Vérifiez les informations détaillées de tous les pods en une seule commande afin de vérifier que chacun héberge bien un conteneur basé sur l’image "nginxdemos/hello:plain-text".

```
kubectl describe deployment rollingapp-deployment | grep Image
    Image:        nginxdemos/hello:plain-text
```

#### ROLLBACK UPDATE

Vous commencez à vous rendre compte que Kubernetes est un véritable must-have pour la gestion et le déploiement de conteneurs d’applications, surtout à grande échelle.

Imaginez le contexte suivant : suite au déploiement d’une nouvelle version, les développeurs se rendent compte qu’une fonctionnalité n’a pas été recettée (vérifiée) en profondeur et qu’une mise à jour vers la version précédente doit être faite de toute urgence…

👉 Trouvez la commande permettant de revenir sur une version précédente du déploiement et annuler la dernière mise à jour, sans utiliser la commande kubectl set image.

👉 Vérifiez les informations détaillées de tous les pods (en une seule commande) afin de vérifier que chacun héberge bien un conteneur basé sur l’image "nginxdemos/hello:latest".

```
kubectl rollout undo deployment/rollingapp-deployment
    deployment.apps/rollingapp-deployment rolled back
kubectl describe deployment rollingapp-deployment | grep Image
    Image:        nginxdemos/hello:latest
```

### 05 - Service et load balancer

#### SETUP

👉 Créez un manifeste "mywebserver-deployment.yml" dédié à un déploiement de pods en suivant les contraintes suivantes :

- Le nom de l’application déployée sera "mywebserver"
- Le nom du manifeste de déploiement sera "mws-deployment"
- Le conteneur déployé sera nommé "nginx-hello", basé sur l’image nginxdemos/hello dans sa dernière version et devra exposer le port 80
- 5 pods devront être utilisés pour le déploiement du conteneur "nginx-hello"

👉 Appliquez le manifeste créé précédemment afin de déployer les pods contenant chacun un seul conteneur "nginx-hello".

`kubectl apply -f mywebserver-deployment.yml`

#### LA NOTION DE SERVICE

Comme vu dans les challenges précédents, la commande kubectl port-forward permet de binder un port d’un conteneur sur la machine hôte, ce qui est utile pour comprendre le fonctionnement de Kubernetes et s'entraîner, mais pas vraiment adapté dans un environnement de production.

En effet, la notion de service avec Kubernetes sera plus adaptée car elle permettra de rendre disponible une application web sur internet, mais également de rediriger le trafic équitablement entre les pods afin de répartir la charge, c’est ce que l’on appelle un load balancer.

👉 Créez un nouveau fichier nommé "mywebserver-service.yml" et complétez le service ci-dessous afin de l’adapter à votre déploiement précédemment créé.

```
apiVersion: v1
kind: Service
metadata:
  name: mws-service
  labels:
    app: ???
spec:
  type: LoadBalancer
  ports:
  - name: http
    port: ???
    protocol: TCP
    targetPort: ???
  selector:
    app: ???
  sessionAffinity: None
```

👉 Appliquez le service créé précédemment via la commande kubectl apply.

👉 Trouvez l’option à appliquer à la commande kubectl get afin de vérifier l’état des services.

L’output de cette commande doit afficher une nouvelle ligne contenant "mws-service". Dans un environnement de production, la colonne "EXTERNAL-IP" affiche l’IP publique permettant de joindre le service et donc l’application déployée via Kubernetes.

Fort heureusement, notre meilleur ami minikube permet de rediriger l’hôte local vers le service afin de simuler un contexte dans lequel l’environnement de production (déployé via AWS, par exemple) expose une IP publique. Ça peut paraître compliqué, mais rassurez-vous, Kubernetes se charge de tout !

👉 Utilisez la commande suivante afin d’obtenir une URL censée rediriger, via le service déployé précédemment, vers un pod exposant le port 80 du conteneur qu’il héberge.

`minikube service mws-service --url`

👉 Ouvrez l’URL affichée par la commande précédente à partir d’un navigateur afin de constater que l’image "nginxdemos/hello" affiche les informations du serveur web ou plus précisément celles du pod hébergeant l’application.

👉 Récupérez le nom du pod affiché sur la ligne "Server name" afin de le supprimer via la commande kubectl delete.

👉 Actualisez la page précédemment ouverte afin de constater qu’un autre pod a pris le relais automatiquement grâce au manifeste de déploiement et au load balancer configuré via le service.

👉 Pour finir, supprimez le manifeste de déploiement ainsi que le service de ce challenge.

`kubectl delete deployment mws-deployment`
`kubectl delete service mws-service`


```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: mws-deployment
  labels:
    app: mywebserver
spec:
  replicas: 5
  selector:
    matchLabels:
      app: mywebserver
  template:
    metadata:
      labels:
        app: mywebserver
    spec:
      containers:
      - name: nginx-hello
        image: nginxdemos/hello:latest
        ports:
        - containerPort: 80
        imagePullPolicy: Always
```

```
apiVersion: v1
kind: Service
metadata:
  name: mws-service
spec:
  selector:
    app: mywebserver
  ports:
  - name: http
    port: 80
    targetPort: 80
  type: NodePort
```

```
NAME          TYPE       CLUSTER-IP     EXTERNAL-IP   PORT(S)        AGE
mws-service   NodePort   10.99.28.116   <none>        80:31690/TCP   90m
```

```
sunburst@raspberrypi:~/lacapsule/docker/kube $ minikube ip
192.168.67.2
sunburst@raspberrypi:~/lacapsule/docker/kube $ curl http://192.168.67.2:31690
<!DOCTYPE html>
<html>
<head>
```

### 04 - Deploy my pods

#### CRÉATION D’UN MANIFESTE DE DÉPLOIEMENT

Jusqu’à maintenant, vous avez pu déployer un pod contenant un seul conteneur via Kubernetes (avec kubectl) et soyons honnête, à part quelques différences au niveau de la syntaxe et de la rapidité de déploiement, rien de bien révolutionnaire par rapport à Docker !

Mais ne vous inquiétez pas, les choses sérieuses commencent avec ce challenge qui va vous permettre de vous confronter à l’équivalent du fichier Docker Compose version Kubernetes, mais surtout à la notion de scaling de ressources.

👉 Créez un fichier nommé "httpd-server-deployment.yml" et insérez le manifeste ci-dessous. Ce fichier permet de décrire la stratégie de déploiement d’un conteneur basé sur l’image httpd.

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: httpd-server-deployment
  labels:
    app: httpd-server

spec:
  replicas: 1
  selector:
    matchLabels:
      app: httpd-server
  template:
    metadata:
      labels:
        app: httpd-server
    spec:
      containers:
      - name: httpd-server
        image: httpd:latest
        imagePullPolicy: Always
```

👉 Appliquez ce manifeste grâce à la commande ci-dessous.

`kubectl apply -f httpd-server-deployment.yml`

👉 Trouvez l’option à appliquer à la commande kubectl get afin de vérifier l’état des déploiements.

`kubectl get deployment`

👉 Modifiez le fichier créé précédemment et dédié au déploiement de l’application "httpd-server" afin d’exposer le port 80.

👉 Appliquez la modification apportée au manifeste via la commande kubectl apply.

👉 Comme vu dans le challenge précédent, redirigez le port 8080 de la machine hôte vers le port 80 du conteneur httpd-server.

`kubectl port-forward deployment/httpd-server-deployment 8080:80`

Prenez le temps de vérifier le nom du pod créé lors du déploiement via la commande kubectl get pods.

👉 Gardez ouvert le terminal où la commande précédente a été exécutée et à partir d’un nouveau terminal, tentez une requête HTTP via curl sur localhost (ou 127.0.0.1), en précisant le port 8080.

curl -v http://localhost:8080

#### MODIFICATION D’UN MANIFESTE DE DÉPLOIEMENT

Dans certains cas, vous n’aurez pas la possibilité d’utiliser VSCode pour modifier un manifeste de déploiement, car la machine hôte utilisée ne dispose pas d’interface graphique (GUI).

Sachez qu’il existe deux modes de création des ressources Kubernetes :

Le mode déclaratif utilisant ce qu’on appelle des manifestes écrit en YAML, c’est ce que vous avez fait jusqu’à maintenant
Le mode impératif créant et modifiant les ressources à la volée, c’est ce que vous allez faire plus tard dans ce challenge

👉 Trouvez une commande kubectl permettant d’éditer rapidement le manifeste un déploiement en mode impératif, sans avoir à mettre à jour le fichier YAML directement.

⚠️ Attention : les modifications apportées via kubectl seront directement appliquées, mais ne seront pas répercutées sur le fichier "httpd-server-deployment.yml".

👉 Après avoir exécuté la commande précédente, modifiez le manifeste afin de déployer le conteneur "httpd-server" sur 4 pods distincts au lieu d’un seul.

L’éditeur par défaut sera vim, il est un peu particulier car compliqué à prendre en main de prime abord, mais c’est l’occasion de prendre en main l’un des "concurrents" de nano.

👉 À l’ouverture, l'éditeur vim est en mode "lecture seule", pressez la touche "i" pour passer en mode INSERT.

👉 Une fois vos modifications terminées, appuyez sur "Echap" pour repasser en mode "lecture seule", puis saisissez la commande ":wq" afin de sauvegarder le fichier et quitter l’éditeur.

👉 Vérifiez la création des 4 pods grâce à la commande kubectl get pods.

#### SCALING DES PODS

Dans un environnement de production, ces 4 pods seront répartis entre les différents workers disponibles.
Ainsi, dans l’éventualité d’un crash d’un conteneur ou d’un worker HS, le reste des pods pourront prendre le relais.

👉 Trouvez une commande kubectl permettant changer le nombre de pods déployés (replicas) à 2 en mode impératif, sans modifier directement le manifeste de déploiement.

`kubectl scale deployment/httpd-server-deployment --replicas=2`

👉 Pour finir, trouvez la commande permettant de supprimer un déploiement.
Cette commande est également censée supprimer les pods liés au déploiement.

`kubectl delete deployment httpd-server-deployment`

### 03 - Network et transfert de port

#### LE RÉSEAU AVEC KUBERNETES

👉 Via la commande kubectl run, démarrez un pod nommé "httpd-server" contenant un seul conteneur, basé sur l’image officielle de httpd et en exposant le port 80.

`kubectl run httpd-server --image=httpd --port=80`

👉 Après quelques secondes, vérifiez l’état du pod via la commande permettant de lister les pods liés au cluster minikube.

`kubectl get pods`


👉 À partir de la documentation de kubectl, trouvez une commande permettant d’obtenir toutes les informations détaillées d’une ressource Kubernetes et exécutez-là afin de décrire le pod "httpd-server".

`kubectl describe pod httpd-server`

👉 Grâce à la commande précédente, vous êtes censé pouvoir récupérer l’adresse IP locale du pod. Tentez de faire une requête HTTP via curl à partir de cette adresse.

curl http://172.17.X.X

La requête n’aboutit pas ? C’est tout à fait normal ! Rappelez-vous que les conteneurs Docker sont isolés, y compris au niveau du réseau.

C’est le même principe pour Kubernetes : les conteneurs inclus dans un pod peuvent communiquer entre eux, mais il faut explicitement rediriger (forward) un port de la machine hôte vers un conteneur afin de communiquer avec lui, y compris si le port en question a déjà été exposé lors de la création du pod.

👉 Afin de mieux comprendre le fonctionnement de l’isolation réseau de Kubernetes, trouvez une commande permettant de rediriger le port 8080 de la machine hôte vers le port 80 du conteneur "httpd-server”.

`kubectl port-forward pod/httpd-server 8080:80`

👉 Gardez ouvert le terminal où la commande précédente a été exécutée et à partir d’un nouveau terminal, tentez une requête HTTP via curl sur localhost (ou 127.0.0.1), en précisant le port 8080.

curl -v http://localhost:8080

👉 Pour finir, assurez-vous de supprimer le pod créé au début de ce challenge.

### 02 - K8S & Minikube

#### INSTALLATION DU CLUSTER

👉 Installez l’utilitaire Minikube permettant de simuler un cluster Kubernetes en local afin de s’entraîner : https://minikube.sigs.k8s.io/docs/start/ 

👉 Vérifiez l’installation de l’outil en ligne de commande minikube.


`minikube version`

👉 Installez la CLI de kubectl qui vous permettra de communiquer avec le cluster Kubernetes : https://kubernetes.io/fr/docs/tasks/tools/install-kubectl/ 

👉 Vérifiez l’installation de l’outil en ligne de commande kubectl.


`kubectl version`

👉 Démarrez le cluster Kubernetes en local.

`minikube start`

👉 À partir de la documentation de minikube, trouvez la commande permettant de vérifier le bon démarrage du cluster.

#### DÉMARRAGE D’UN POD

👉 Démarrez un pod contenant un seul conteneur nommé "myfirstpod" basé sur la fameuse image hello-world.

```
kubectl run myfirstpod --image=hello-world:latest
```

Un pod permet de rassembler différents conteneurs liés à une application ou un environnement.
Cette notion vient remplacer celle de Docker Compose avec les services puisque Kubernetes permettra d’orchestrer différents conteneurs d’une façon bien plus poussée et "production ready".

👉 Exécutez la commande suivante afin d’obtenir la liste des pods liés au cluster.

`kubectl get pods`

La commande kubectl communique directement avec le "master node" qui se charge à son tour de communiquer avec les workers nodes qui contiennent les fameux pods.

Dans votre cas, Minikube simule cette architecture avec un master node et un seul worker sur la même machine, mais en production chaque partie est une machine à part entière.

👉 Trouvez la commande permettant de récupérer les logs d’un pod précis.

👉 Enfin, supprimez le pod créé précédemment.

`kubectl delete pods myfirstpod`

Le pod créé dans ce challenge ne contenait qu’un seul container, mais il ne s’agit que d’un exemple pour prendre un main la CLI de kubectl.

Dans un contexte de production et de haute disponibilité, Kubernetes permet d'orchestrer un cluster constitué de nombreux pods, contenant chacun les mêmes conteneurs.

- Installation

```
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-arm64
sudo install minikube-linux-arm64 /usr/local/bin/minikube
minikube start
```
- création d'un alias : `alias kubectl="minikube kubectl --"`

```
kubectl version

WARNING: This version information is deprecated and will be replaced with the output from kubectl version --short.  Use --output=yaml|json to get the full version.
Client Version: version.Info{Major:"1", Minor:"26", GitVersion:"v1.26.3", GitCommit:"9e644106593f3f4aa98f8a84b23db5fa378900bd", GitTreeState:"clean", BuildDate:"2023-03-15T13:40:17Z", GoVersion:"go1.19.7", Compiler:"gc", Platform:"linux/arm64"}
Kustomize Version: v4.5.7
Server Version: version.Info{Major:"1", Minor:"26", GitVersion:"v1.26.3", GitCommit:"9e644106593f3f4aa98f8a84b23db5fa378900bd", GitTreeState:"clean", BuildDate:"2023-03-15T13:33:12Z", GoVersion:"go1.19.7", Compiler:"gc", Platform:"linux/arm64"}

minikube start

😄  minikube v1.30.1 sur Raspbian 11.6 (arm64)
✨  Utilisation du pilote docker basé sur le profil existant
👍  Démarrage du noeud de plan de contrôle minikube dans le cluster minikube
🚜  Extraction de l'image de base...
🏃  Mise à jour du container docker en marche "minikube" ...
🐳  Préparation de Kubernetes v1.26.3 sur Docker 23.0.2...
🔎  Vérification des composants Kubernetes...
    ▪ Utilisation de l'image docker.io/kubernetesui/dashboard:v2.7.0
    ▪ Utilisation de l'image docker.io/kubernetesui/metrics-scraper:v1.0.8
    ▪ Utilisation de l'image gcr.io/k8s-minikube/storage-provisioner:v5
💡  Certaines fonctionnalités du tableau de bord nécessitent le module metrics-server. Pour activer toutes les fonctionnalités, veuillez exécuter :
        minikube addons enable metrics-server
🌟  Modules activés: storage-provisioner, default-storageclass, dashboard
💡  kubectl introuvable. Si vous en avez besoin, essayez : 'minikube kubectl -- get pods -A'
🏄  Terminé ! kubectl est maintenant configuré pour utiliser "minikube" cluster et espace de noms "default" par défaut.
```

- Démarrer des pods


`kubectl get pods`

No resources found in default namespace.

`kubectl run myfirstpod --image=hello-world:latest`

pod/myfirstpod created

`kubectl get pods`

NAME         READY   STATUS             RESTARTS     AGE
myfirstpod   0/1     CrashLoopBackOff   1 (7s ago)   11s

`kubectl logs myfirstpod`

Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
...

```kubectl delete pods myfirstpod

pod "myfirstpod" deleted
```