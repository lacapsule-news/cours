# K8S & Minikube

## INSTALLATION DU CLUSTER

👉 Installez l’utilitaire Minikube permettant de simuler un cluster Kubernetes en local afin de s’entraîner : https://minikube.sigs.k8s.io/docs/start/ 

👉 Vérifiez l’installation de l’outil en ligne de commande minikube.


`minikube version`

👉 Installez la CLI de kubectl qui vous permettra de communiquer avec le cluster Kubernetes : https://kubernetes.io/fr/docs/tasks/tools/install-kubectl/ 

👉 Vérifiez l’installation de l’outil en ligne de commande kubectl.


`kubectl version`

👉 Démarrez le cluster Kubernetes en local.

`minikube start`

👉 À partir de la documentation de minikube, trouvez la commande permettant de vérifier le bon démarrage du cluster.

## DÉMARRAGE D’UN POD

👉 Démarrez un pod contenant un seul conteneur nommé "myfirstpod" basé sur la fameuse image hello-world.

```
kubectl run myfirstpod --image=hello-world:latest
```

Un pod permet de rassembler différents conteneurs liés à une application ou un environnement.
Cette notion vient remplacer celle de Docker Compose avec les services puisque Kubernetes permettra d’orchestrer différents conteneurs d’une façon bien plus poussée et "production ready".

👉 Exécutez la commande suivante afin d’obtenir la liste des pods liés au cluster.

`kubectl get pods`

La commande kubectl communique directement avec le "master node" qui se charge à son tour de communiquer avec les workers nodes qui contiennent les fameux pods.

Dans votre cas, Minikube simule cette architecture avec un master node et un seul worker sur la même machine, mais en production chaque partie est une machine à part entière.

👉 Trouvez la commande permettant de récupérer les logs d’un pod précis.

👉 Enfin, supprimez le pod créé précédemment.

`kubectl delete pods myfirstpod`

Le pod créé dans ce challenge ne contenait qu’un seul container, mais il ne s’agit que d’un exemple pour prendre un main la CLI de kubectl.

Dans un contexte de production et de haute disponibilité, Kubernetes permet d'orchestrer un cluster constitué de nombreux pods, contenant chacun les mêmes conteneurs.

- Installation

```
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-arm64
sudo install minikube-linux-arm64 /usr/local/bin/minikube
minikube start
```
- création d'un alias : `alias kubectl="minikube kubectl --"`

```
kubectl version

WARNING: This version information is deprecated and will be replaced with the output from kubectl version --short.  Use --output=yaml|json to get the full version.
Client Version: version.Info{Major:"1", Minor:"26", GitVersion:"v1.26.3", GitCommit:"9e644106593f3f4aa98f8a84b23db5fa378900bd", GitTreeState:"clean", BuildDate:"2023-03-15T13:40:17Z", GoVersion:"go1.19.7", Compiler:"gc", Platform:"linux/arm64"}
Kustomize Version: v4.5.7
Server Version: version.Info{Major:"1", Minor:"26", GitVersion:"v1.26.3", GitCommit:"9e644106593f3f4aa98f8a84b23db5fa378900bd", GitTreeState:"clean", BuildDate:"2023-03-15T13:33:12Z", GoVersion:"go1.19.7", Compiler:"gc", Platform:"linux/arm64"}

minikube start

😄  minikube v1.30.1 sur Raspbian 11.6 (arm64)
✨  Utilisation du pilote docker basé sur le profil existant
👍  Démarrage du noeud de plan de contrôle minikube dans le cluster minikube
🚜  Extraction de l'image de base...
🏃  Mise à jour du container docker en marche "minikube" ...
🐳  Préparation de Kubernetes v1.26.3 sur Docker 23.0.2...
🔎  Vérification des composants Kubernetes...
    ▪ Utilisation de l'image docker.io/kubernetesui/dashboard:v2.7.0
    ▪ Utilisation de l'image docker.io/kubernetesui/metrics-scraper:v1.0.8
    ▪ Utilisation de l'image gcr.io/k8s-minikube/storage-provisioner:v5
💡  Certaines fonctionnalités du tableau de bord nécessitent le module metrics-server. Pour activer toutes les fonctionnalités, veuillez exécuter :
        minikube addons enable metrics-server
🌟  Modules activés: storage-provisioner, default-storageclass, dashboard
💡  kubectl introuvable. Si vous en avez besoin, essayez : 'minikube kubectl -- get pods -A'
🏄  Terminé ! kubectl est maintenant configuré pour utiliser "minikube" cluster et espace de noms "default" par défaut.
```

- Démarrer des pods


`kubectl get pods`

No resources found in default namespace.

`kubectl run myfirstpod --image=hello-world:latest`

pod/myfirstpod created

`kubectl get pods`

NAME         READY   STATUS             RESTARTS     AGE
myfirstpod   0/1     CrashLoopBackOff   1 (7s ago)   11s

`kubectl logs myfirstpod`

Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
...

`kubectl delete pods myfirstpod`

pod "myfirstpod" deleted