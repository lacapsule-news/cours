# Kubernetes

## Contexte

L'architecture doit être dupliquée sur plusieurs serveur de production pour assurer la haute disponibilité des services.

La gestion des différents serveur est etrêmement fastidieuse, car il faut intervenir manuellement sur chacun d'entre eux.

Kubernetes permet d'orchestrer tous les conteneurs répartis sur l'ensemble des serveur de productions.

### Fonctionnement

Un pod est un ensemble de conteneurs.
C'est l'équivalent du fonctionnemet de Docker Compose.

L'objectif est de dupliquer les pods sur plusieurs serveur afin de garantir leur disponibilité et d'optimiser le traffic.

Les pods sont administrés à travers des commandes qui sont reçu par le master node.

Le master node est un serveur à part dont le seul but est d'orchestrer les pods.

Un serveur peut contenir plusieurs pods, on parle alors de worker node.

Pour assurer le principe de haute disponibilité, les pods ne doivent pas être dupliqués sur le même serveur.

C'est le rôle du master node de répartir équitablement les pods entre les différents worker node.

Par exemple, si un des pods crash, il sera recrée automatiquement sur un worker node disponible.

LE cluster est un terme qui désigne le master node et l'ensemble des worker nodes.