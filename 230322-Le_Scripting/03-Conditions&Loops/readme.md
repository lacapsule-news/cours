# CONDITIONS & LOOPS

## CONDITIONS

> Selon le contenu d’une variable, il est possible d'exécuter certaines commandes à l’aide d’une condition if… then.

 

- À l’aide de nano, créez un nouveau script appelé conditions.sh contenant le code suivant.

```
#!/usr/bin/env bash

 

age=16

if [ $age -gt 18 ]

then

        echo "You are an adult"

else         

        echo "You are a minor"

fi
```

- Sauvegardez le script et rendez-le exécutable.


- Déterminez quel message sera affiché en lisant seulement le code puis exécutez le script.

- Mettez à jour le contenu de la variable "age" du script en remplaçant 16 par 42.

- Exécutez à nouveau le script et regardez le résultat.

- Modifiez le script en ajoutant une condition intermédiaire "elif" avec le code suivant.

```
#!/usr/bin/env bash


age=18

if [ $age -gt 18 ]

then         

        echo "You are an adult"

elif [ $age -eq 18 ] ; then

        echo "You have just reached majority"

else         

        echo "You are a minor"

fi

exit
```

- Déterminez quel message sera affiché en lisant seulement le code puis exécutez le script.

## BOUCLES

> Une autre fonctionnalité utile lors de de l'exécution d’un script est la répétition d’une action, grâce à la syntaxe for… do.

- Créez un nouveau script appelé "loops.sh" contenant le code suivant.

```
#!/usr/bin/env bash


week="monday tuesday wednesday thursday friday saturday sunday"

for day in $week ; do

    echo "$day"

done
```

- Sauvegardez le script et rendez-le exécutable.


- Déterminez quel message sera affiché en lisant seulement le code, puis exécutez le script.