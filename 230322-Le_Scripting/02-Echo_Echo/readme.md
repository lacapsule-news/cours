# ECHO ECHO

## VARIABLES SIMPLES

> Le script du challenge précédent était relativement simple et ne servait qu’à afficher la chaîne de caractères "Hello world". C’est un bon début, mais il est possible d’aller plus loin dans la commande echo.


- À l’aide de nano, créez un nouveau script appelé variables.sh contenant le code suivant.

```
#!/usr/bin/env bash


firstname=john

echo "home folder /home/$firstname"
```
- Sauvegardez le script et rendez-le exécutable.
- Exécutez le script.

> Vous avez stocké la chaîne de caractères "john" dans la variable "firstname", ainsi la chaîne de caractères affichée dans le terminal correspond à "home folder /home/john".


- Remplacez le contenu de la variable "firstname" par "vanessa" et exécutez à nouveau le script.

## VARIABLES D’ENVIRONNEMENT

> Les variables permettent de stocker des valeurs et de les réutiliser ou de les mettre à jour en y faisant appel.

> Il existe également des variables définies de manière globale au sein de votre terminal, on les appelle variables d’environnement et celles-ci contiennent des informations utiles pouvant être transmises aux processus de votre session shell.


- Depuis votre terminal, exécutez la commande printenv HOME.


> La commande précédente retourne le chemin d’accès vers le répertoire par défaut de votre terminal.


- Affichez la liste de toutes les variables d’environnement avec la commande printenv.


## VARIABLES PRÉPOSITIONNÉES

> Il est possible de transmettre des valeurs à stocker dans des variables via les paramètres du script, il s’agit de variables prépositionnées.


- Créez un nouveau script appelé "params.sh" contenant le code suivant.

```
#!/usr/bin/env bash


echo "Welcome $1! You are $2 years old."
```

- Sauvegardez le script et rendez-le exécutable.


- Exécutez le script en lui passant des paramètres grâce à la commande suivante.

```
./params.sh Vanessa 42
```

> Chaque argument est identifié par sa position : $1 correspond au premier argument passé, $2 au second et ainsi de suite… N’hésitez pas à modifier les paramètres passés au script pour bien comprendre ce fonctionnement.



## INTERACTION UTILISATEUR

- Créez un nouveau script appelé interact.sh contenant le code suivant.

```
#!/usr/bin/env bash


echo "What is your name?"

read response

echo "Hello $response!"
```

- Sauvegardez le script et rendez-le exécutable.


- Exécutez le script.


> La commande read lit les valeurs entrées au clavier et les stocke dans une variable à réutiliser.


- Editez le code précédent en y ajoutant l’extrait de code suivant.


```
read -p "How old are you?" response

echo "You are $response years old"
```

- Sauvegardez le script puis exécutez-le.