# FOLDER OR FILE?

## CONNAÎTRE LE TYPE D’UN ÉLÉMENT

- Créez un nouveau script appelé "checkType.sh" qui reçoit en argument un chemin d’accès et qui affiche le type de l’élément ciblé (dossier ou fichier).

Par exemple :
Si le script prend en entrée le chemin "/Users/antoine/Documents", le script affichera le message : "/Users/antoine/Documents is a directory"

Si le script prend en entrée le chemin /Users/antoine/Documents/compta.docx, le script affichera "/Users/antoine/Documents/compta.docx is a regular file"

## CONNAÎTRE LE TYPE DE PLUSIEURS ÉLÉMENTS

Modifiez le script précédent pour qu’il accepte un nombre quelconque de chemins d’accès passés en argument à l’exécution. Le script devra afficher autant de messages que de chemins reçus.