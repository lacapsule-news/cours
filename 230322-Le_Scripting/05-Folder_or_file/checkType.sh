#!/usr/bin/env bash

for ARG in $@; do
	if [ -d $ARG ];then(echo $ARG ': is a directory');
	elif [ -f $ARG ]; then(echo $ARG ': is a file');
	else echo 'Cant process';
fi
done
