# EXECUTE MY SCRIPT

## CRÉATION D’UN PREMIER SCRIPT

> Vous utiliserez l’utilitaire nano pour créer vos scripts afin de bénéficier d’un éditeur de texte intégré au terminal.

> Pour créer et éditer un nouveau fichier, il suffit d’utiliser la commande suivante : nano nomDuFichier.extension.


- Exécutez la commande nano myscript.sh et copiez le code suivant dans la fenêtre de l’éditeur de texte :

```
#!/usr/bin/env bash


echo "Hello world!"
```

- Sauvegardez le fichier en appuyant simultanément sur les touches CTRL et X, puis en appuyant sur Y (yes) et enfin sur Enter.

## EXÉCUTION D’UN SCRIPT

> Votre script a bien été créé et vous allez maintenant l’exécuter à l’aide de votre terminal.

> Pour cela, la première chose à faire est de donner les droits d’exécution au script.

- Exécutez la commande suivante.


`chmod +x myscript.sh`


> Pour rappel, la commande chmod (abréviation de "change mode") permet de changer les permissions d’un fichier donné. La mention +x indique que nous souhaitons rendre ce fichier exécutable.

- Exécutez le script contenu dans le fichier myscript.sh avec la commande suivante.


`./myscript.sh`


> Pour rappel, il est possible de faire appel à un script dans le répertoire courant en saisissant ./nomduscript.sh.


## Answer

1. echo '#!/usr/bin/env bash' > helloWorld.sh
1. echo 'echo "Hello world!" >> helloWorld.sh
1. echo 'echo "Hello world!"' >> helloWorld.sh
1. cat helloWorld.sh
1. chmod +x helloWorld.sh
1. ./helloWorld.sh