# Exo Bonus
- prend un fichier en entrée
- compte le nombre de lignes vides 
- non vides dans ce fichier.

- affiche :
	- le nombre total de lignes
	- le nombre de lignes non vides
	- le nombre de lignes vides.

```
#!/usr/bin/env bash
for ARG in $@;do
	if [ -f $ARG ];
	then(
		echo 'file : '$ARG'---';echo 'total : '$(awk 'END{print NR}' $ARG);
		echo 'used : '$(awk '/./ {x+=1};END {print x}' $ARG);
		echo 'unused : '$(awk '/^$/ {x += 1};END {print x }' $ARG;));
	else
		echo 'Cant process';
	fi
done
```

```
file : readme.md---
total : 30
used : 27
unused : 3
```