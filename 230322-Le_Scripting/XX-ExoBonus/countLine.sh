#!/usr/bin/env bash
for ARG in $@;do
	if [ -f $ARG ];
	then(
		echo 'file : '$ARG'---';echo 'total : '$(awk 'END{print NR}' $ARG);
		echo 'used : '$(awk '/./ {x+=1};END {print x}' $ARG);
		echo 'unused : '$(awk '/^$/ {x += 1};END {print x }' $ARG;));
	else
		echo 'Cant process';
	fi
done