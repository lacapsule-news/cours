# Le scripting

## Théorique

### Contexte 

#### Automatisation des traitements

Traitée un grande quantité de fichier

![](Images/automation.png)

> Etre capable d'enchainer des commande ainsi que de les stocker dans un fichier.

### Fonctionnement

#### Création d'un fichier script

1. Ecrire dans un fichier, des tâches à réaliser sous forme d'insctuctions
1. Exécution du fichier pour réaliser les tâches

#### Environnement nécessaire

#### Step by step

![](Images/step1.png)
![](Images/step2.png)
![](Images/step3.png)
![](Images/step4.png)
![](Images/step5.png)

## Pratique

### XX - ExoBonus

- prend un fichier en entrée
- compte le nombre de lignes vides 
- non vides dans ce fichier.

- affiche :
	- le nombre total de lignes
	- le nombre de lignes non vides
	- le nombre de lignes vides.

```
#!/usr/bin/env bash
for ARG in $@;do
	if [ -f $ARG ];
	then(
		echo 'file : '$ARG'---';echo 'total : '$(awk 'END{print NR}' $ARG);
		echo 'used : '$(awk '/./ {x+=1};END {print x}' $ARG);
		echo 'unused : '$(awk '/^$/ {x += 1};END {print x }' $ARG;));
	else
		echo 'Cant process';
	fi
done
```

```
file : readme.md---
total : 30
used : 27
unused : 3
```

### 06 - VACATION PICTURES

#### FORMATAGE DES NOMS DE FICHIERS

- Créez un nouveau script appelé "renameFiles.sh" chargé de renommer les fichiers portant l’extension .jpg (et seulement ces fichiers) contenus dans le répertoire "picturesToRename".


- Les fichiers devront porter le nom suivant : YYYY-MM-DD-nomDuFichier.jpeg où "YYYY-MM-DD" correspond à la date du jour au format année-mois-jour et "nomDuFichier" au nom original du fichier avant d’être renommé.

- Les fichiers renommés devront être déplacés dans le répertoire "renamedPictures", sans altérer les fichiers originaux.


> Par exemple :
Si nous sommes le 2 juin 2050 et que le script traite le fichier "picturesToRename/beach.jpg", il sera renommé en "renamedPictures/2050-06-02-beach.jpg".

- Modifiez le script précédent pour qu’il renomme tous les fichiers présents dans "renamedPictures" selon un type d’extension indiqué par l’utilisateur en argument.


> Par exemple :
Si l’utilisateur exécute le script avec la commande ./renameFiles.sh png, tous les fichiers présents dans le dossier "renamedPictures" auront l’extension .png.


------
### 05 - FOLDER OR FILE?

#### CONNAÎTRE LE TYPE D’UN ÉLÉMENT

- Créez un nouveau script appelé "checkType.sh" qui reçoit en argument un chemin d’accès et qui affiche le type de l’élément ciblé (dossier ou fichier).

>Par exemple :

> Si le script prend en entrée le chemin "/Users/antoine/Documents", le script affichera le message : "/Users/antoine/Documents is a directory"

> Si le script prend en entrée le chemin /Users/antoine/Documents/compta.docx, le script affichera "/Users/antoine/Documents/compta.docx is a regular file"
#### CONNAÎTRE LE TYPE DE PLUSIEURS ÉLÉMENTS

> Modifiez le script précédent pour qu’il accepte un nombre quelconque de chemins d’accès passés en argument à l’exécution. Le script devra afficher autant de messages que de chemins reçus.

------
### 04 - I LOVE PETS

#### EXÉCUTION DE MANIÈRE ITÉRATIVE


- À l’aide de nano, créez un nouveau script appelé "pets.sh".

- Votre script devra afficher successivement les noms d’animaux suivants:

	- bear
	- pig
	- dog
	- cat
	- sheep


- Essayez de rédiger un script contenant le moins de lignes possibles.

```
#!/usr/bin/env bash

pets='bear pig dog cat sheep'

for pet in $pets; do printf "%b\n" "$pet"; done
```

------
### 03 - CONDITIONS & LOOPS

#### CONDITIONS

> Selon le contenu d’une variable, il est possible d'exécuter certaines commandes à l’aide d’une condition if… then.

 

- À l’aide de nano, créez un nouveau script appelé conditions.sh contenant le code suivant.

```
#!/usr/bin/env bash

 

age=16

if [ $age -gt 18 ]

then

        echo "You are an adult"

else         

        echo "You are a minor"

fi
```

- Sauvegardez le script et rendez-le exécutable.


- Déterminez quel message sera affiché en lisant seulement le code puis exécutez le script.

- Mettez à jour le contenu de la variable "age" du script en remplaçant 16 par 42.

- Exécutez à nouveau le script et regardez le résultat.

- Modifiez le script en ajoutant une condition intermédiaire "elif" avec le code suivant.

```
#!/usr/bin/env bash


age=18

if [ $age -gt 18 ]

then         

        echo "You are an adult"

elif [ $age -eq 18 ] ; then

        echo "You have just reached majority"

else         

        echo "You are a minor"

fi

exit
```

- Déterminez quel message sera affiché en lisant seulement le code puis exécutez le script.

#### BOUCLES

> Une autre fonctionnalité utile lors de de l'exécution d’un script est la répétition d’une action, grâce à la syntaxe for… do.

- Créez un nouveau script appelé "loops.sh" contenant le code suivant.

```
#!/usr/bin/env bash


week="monday tuesday wednesday thursday friday saturday sunday"

for day in $week ; do

    echo "$day"

done
```

- Sauvegardez le script et rendez-le exécutable.


- Déterminez quel message sera affiché en lisant seulement le code, puis exécutez le script.

------
### 03 - ECHO ECHO

#### VARIABLES SIMPLES

> Le script du challenge précédent était relativement simple et ne servait qu’à afficher la chaîne de caractères "Hello world". C’est un bon début, mais il est possible d’aller plus loin dans la commande echo.


- À l’aide de nano, créez un nouveau script appelé variables.sh contenant le code suivant.

```
#!/usr/bin/env bash


firstname=john

echo "home folder /home/$firstname"
```
- Sauvegardez le script et rendez-le exécutable.
- Exécutez le script.

> Vous avez stocké la chaîne de caractères "john" dans la variable "firstname", ainsi la chaîne de caractères affichée dans le terminal correspond à "home folder /home/john".


- Remplacez le contenu de la variable "firstname" par "vanessa" et exécutez à nouveau le script.

#### VARIABLES D’ENVIRONNEMENT

> Les variables permettent de stocker des valeurs et de les réutiliser ou de les mettre à jour en y faisant appel.

> Il existe également des variables définies de manière globale au sein de votre terminal, on les appelle variables d’environnement et celles-ci contiennent des informations utiles pouvant être transmises aux processus de votre session shell.


- Depuis votre terminal, exécutez la commande printenv HOME.


> La commande précédente retourne le chemin d’accès vers le répertoire par défaut de votre terminal.


- Affichez la liste de toutes les variables d’environnement avec la commande printenv.


#### VARIABLES PRÉPOSITIONNÉES

> Il est possible de transmettre des valeurs à stocker dans des variables via les paramètres du script, il s’agit de variables prépositionnées.


- Créez un nouveau script appelé "params.sh" contenant le code suivant.

```
#!/usr/bin/env bash


echo "Welcome $1! You are $2 years old."
```

- Sauvegardez le script et rendez-le exécutable.


- Exécutez le script en lui passant des paramètres grâce à la commande suivante.

```
./params.sh Vanessa 42
```

> Chaque argument est identifié par sa position : $1 correspond au premier argument passé, $2 au second et ainsi de suite… N’hésitez pas à modifier les paramètres passés au script pour bien comprendre ce fonctionnement.



#### INTERACTION UTILISATEUR

- Créez un nouveau script appelé interact.sh contenant le code suivant.

```
#!/usr/bin/env bash


echo "What is your name?"

read response

echo "Hello $response!"
```

- Sauvegardez le script et rendez-le exécutable.


- Exécutez le script.


> La commande read lit les valeurs entrées au clavier et les stocke dans une variable à réutiliser.


- Editez le code précédent en y ajoutant l’extrait de code suivant.


```
read -p "How old are you?" response

echo "You are $response years old"
```

- Sauvegardez le script puis exécutez-le.

------
### 01 - EXECUTE MY SCRIPT

#### CRÉATION D’UN PREMIER SCRIPT

> Vous utiliserez l’utilitaire nano pour créer vos scripts afin de bénéficier d’un éditeur de texte intégré au terminal.

> Pour créer et éditer un nouveau fichier, il suffit d’utiliser la commande suivante : nano nomDuFichier.extension.


- Exécutez la commande nano myscript.sh et copiez le code suivant dans la fenêtre de l’éditeur de texte :

```
#!/usr/bin/env bash


echo "Hello world!"
```

- Sauvegardez le fichier en appuyant simultanément sur les touches CTRL et X, puis en appuyant sur Y (yes) et enfin sur Enter.

#### EXÉCUTION D’UN SCRIPT

> Votre script a bien été créé et vous allez maintenant l’exécuter à l’aide de votre terminal.

> Pour cela, la première chose à faire est de donner les droits d’exécution au script.

- Exécutez la commande suivante.


`chmod +x myscript.sh`


> Pour rappel, la commande chmod (abréviation de "change mode") permet de changer les permissions d’un fichier donné. La mention +x indique que nous souhaitons rendre ce fichier exécutable.

- Exécutez le script contenu dans le fichier myscript.sh avec la commande suivante.


`./myscript.sh`


> Pour rappel, il est possible de faire appel à un script dans le répertoire courant en saisissant ./nomduscript.sh.