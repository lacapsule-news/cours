# VACATION PICTURES

## FORMATAGE DES NOMS DE FICHIERS

- Créez un nouveau script appelé "renameFiles.sh" chargé de renommer les fichiers portant l’extension .jpg (et seulement ces fichiers) contenus dans le répertoire "picturesToRename".


- Les fichiers devront porter le nom suivant : YYYY-MM-DD-nomDuFichier.jpeg où "YYYY-MM-DD" correspond à la date du jour au format année-mois-jour et "nomDuFichier" au nom original du fichier avant d’être renommé.

- Les fichiers renommés devront être déplacés dans le répertoire "renamedPictures", sans altérer les fichiers originaux.


Par exemple :

Si nous sommes le 2 juin 2050 et que le script traite le fichier "picturesToRename/beach.jpg", il sera renommé en "renamedPictures/2050-06-02-beach.jpg".

Modifiez le script précédent pour qu’il renomme tous les fichiers présents dans "renamedPictures" selon un type d’extension indiqué par l’utilisateur en argument.


Par exemple :

Si l’utilisateur exécute le script avec la commande ./renameFiles.sh png, tous les fichiers présents dans le dossier "renamedPictures" auront l’extension .png.