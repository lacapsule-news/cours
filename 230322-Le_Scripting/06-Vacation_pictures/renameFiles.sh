#!/usr/bin/env bash

photos=$(ls ./picturesToRename/*.jpeg);
ymd=$(date +'%Y-%m-%d-');
extension=$1;

for photo in $photos; do 
	arrPhoto=(${photo//// });
	arrName=(${arrPhoto[2]//./ }[0]);
	
	if [ -z $extension ];
		then cp $photo ./renamedPictures/$ymd${arrPhoto[2]};
		else cp $photo ./renamedPictures/$ymd$arrName.$extension;
		fi;
	echo ./renamedPictures/$ymd${arrPhoto[2]} ' : done';
done