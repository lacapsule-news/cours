# I LOVE PETS

## EXÉCUTION DE MANIÈRE ITÉRATIVE


- À l’aide de nano, créez un nouveau script appelé "pets.sh".

- Votre script devra afficher successivement les noms d’animaux suivants:

	- bear
	- pig
	- dog
	- cat
	- sheep


- Essayez de rédiger un script contenant le moins de lignes possibles.

```
#!/usr/bin/env bash

pets='bear pig dog cat sheep'

for pet in $pets; do printf "%b\n" "$pet"; done
```