# DEBIAN IN THE LAN

## CRÉATION D’UN LAB COMPLET

> Sur GNS3, créez un nouveau projet nommé "debianinthelan".

Ajoutez les équipements suivants :
- 1 routeur Cisco nommé "Router"
- 1 switch nommé "Switch1"
- 1 PC virtuels nommés "PC1"

- En vous inspirant du challenge précédent, créez un nouveau template de machine virtuelle Debian (catégorie "Guests" dans "Appliances from server").


- Ajoutez le nouvel équipement précédemment importé et nommez-le "Debian".


- Reliez le routeur à une interface réseau du switch.


- Reliez toutes les machines virtuelles à une interface réseau du switch.


- Démarrez tous les équipements réseau à l’exception de la VM "Debian".


## ATTRIBUTION DES ADRESSES IP

> À partir d’un terminal communiquant avec "Router" et en récupérant les commandes vues dans le challenge précédent, paramétrez le protocole DHCP qui permettra à toutes les machines du réseau d’obtenir automatiquement une adresse IP.


> À partir d’un terminal communiquant avec "PC1", demandez l’attribution d’une adresse IP via le protocole DHCP.

> Vérifiez que l’attribution a bien fonctionné en tentant un ping vers le routeur.

> Démarrez la VM "Debian" puis à partir d’un terminal communiquant avec cette machine, constatez que le système d’exploitation a automatiquement demandé l’attribution d’une adresse IP via DHCP.


> Enfin, vérifiez que toutes les machines sont bien connectées au réseau et qu’elles puissent communiquer entre elles via ping.