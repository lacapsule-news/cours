# SIMPLE WEB SERVER

## SETUP

- Récupérez la ressource "simplewebserver.zip" depuis l’onglet dédié sur Ariane.


- Importez le fichier "simplewebserver.gns3project" dans GNS3 et démarrez tous les équipements réseau via le bouton "Start".


> Cette simulation de réseau contient seulement un routeur relié à internet qui distribue les adresses IP en DHCP ainsi qu’un switch.


Créez et reliez entre-elles les machines suivantes :
- 1 PC virtuel ne servant qu’à vérifier la connectivité à internet
- 2 VM Debian (un client "Debian" et un serveur "Web-Server")

- À partir d’un terminal communiquant avec "PC1", demandez l’attribution d’une adresse IP via le protocole DHCP.


- Vérifiez que l’attribution a bien fonctionné en tentant un ping vers le routeur.


- Vérifiez que le réseau est bien connecté à internet en tentant un ping vers google.com.

## INSTALLATION D’UN SERVEUR WEB

> Sur la VM "Web-Server", installez le serveur web Nginx qui fera office de serveur web censé distribuer une simple page HTML par défaut.

> Sur la VM "Debian", tentez de communiquer avec le serveur web via curl.

> Si vous récupérez une page HTML contenant le message "Welcome to nginx", cela signifie que le serveur web a bien été installé et qu’il est opérationnel 🎉