# Simple lan

## CRÉATION D’UN SIMPLE RÉSEAU

- Sur GNS3, créez un nouveau projet nommé "simplelan".


- Ajoutez les équipements suivants à partir du menu "Browse all devices" situé à gauche :


> 1 switch (Ethernet switch) nommé "Switch1"
> 3 PC virtuels (VPCS) respectivement nommés "PC1", "PC2" et "PC3"

- Reliez l’interface "Ethernet0" de "PC1" à l’interface "Ethernet0" de "Switch1" grâce à l’option "Add a link" située sur le menu de gauche.


- Reliez l’interface "Ethernet0" de "PC2" à l’interface "Ethernet1" de "Switch1".


- Reliez l’interface "Ethernet0" de "PC3" à l’interface "Ethernet2" de "Switch1".


- Cliquez sur le bouton "Start" afin de démarrer tous les équipements réseau.

## ATTRIBUTION DES ADRESSES IP

> À partir d’un terminal communiquant avec "PC1", exécutez les commandes suivantes afin de lui attribuer manuellement l’adresse IP "192.168.1.2".


- PC1# ip 192.168.1.2 255.255.255.0

- PC1# save


> En suivant la même méthode que l’étape précédente, attribuez manuellement l’adresse IP "192.168.1.3" à "PC2".



- Attribuez manuellement l’adresse IP "192.168.1.4" à "PC3".



> Afin de vérifier que les 3 machines sont bien connectées et identifiables sur le réseau grâce à votre adresse IP, utilisez la commande ping sur chaque PC de la façon suivante.


- PC1# ping 192.168.1.3

- PC1# ping 192.168.1.4


- PC2# ping 192.168.1.2

- PC2# ping 192.168.1.4


- PC3# ping 192.168.1.2

- PC3# ping 192.168.1.3