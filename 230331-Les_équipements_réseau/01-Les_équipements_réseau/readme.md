# Les équipements réseau

> Pour administrer et optimiser les flux de communication entre les machines, il est nécessaire de mettre en place certains équipements.

## Help with osi

OSI | TCP
------|------
Application | Application
Présentation | Application
Sessions | Application
Transport(TCP) | Transport(TCP)
Réseau(IP) | Réseau(IP)
Liaison(MAC) | Accès réseau
Physique | Accès réseau

## Le Hub

> Le Hub agissant a la couche 1 du model osi, il va récupère le réseau et le renvoyer a tout le monde, les hôte devront faire le traitement de si oui on non la transmission est pour eux.

## Le Switch

> Le switch agissant sur le niveau deux du modèle osi, il va être capable de faire transiter le réseau vers la bonne machine, pour ça il utilise le protocole ARP. Certains switchs récent sont capables de faire du vlan afin de segmenter le réseau, pour les autres il faudra passer par du VLSM (Variable Length Subnet Mask).

## Le routeur

> Le routeur agit à la couche 3 du modèle osi, cela lui permettra de router plusieurs réseaux entre eux grâce à leur adresse IP, les deux protocoles à connaître sont NAT(Network adresse Translation) et PAT(Port adresse Translation)

## Le Transport

> Afin de pouvoir se connecter à un autre appareil il est important que les différents appareils puissent parler la même langue, c'est là que la couche transport rentre en jeu, grace au protocole Ucp et Tcp elle va définir comment les informations vont circuler et s'il faut vérifier que toutes les données soient arrivées.

> Par exemple lors d'un appel on ne veut pas que d'anciens paquet soit renvoyer, cela impacterais la qualité du son.

## Le reste

> Au-dessus du transport vienne se placer les applications, souvent segmenté car le transport en réseau et assez exigeant.

> Par exemple lorsqu'un client arrive sur le réseau le dhcp :
- Le client va demander a tout le réseau grâce a du broadcast qui peut lui donner une adresse ip,
- Le serveur dhcp la prendre en compte ca demande et lui proposé une ip
- Le client va répondre afin d'être sur que tout ce soit bien passé
- Le serveur va lui dire qu'il a bien pris en compte sa réponse

## Le firewall


> Le firewall est un mot un peut four tout dans le sens ou il existe des firewall pour tout. Cependant voici les principaux firewall réseau :


- Le pare-feu matériel. Ce type de firewall est installé à l’entrée et à la sortie du réseau local. Son installation est généralement plus coûteuse que le firewall logiciel, mais il garantit davantage de protection en terme de sécurité. Cette solution est notamment privilégiée pour les réseaux comportant plusieurs ordinateurs, par exemple dans le cadre de sociétés privées (le pare-feu matériel se révèle alors moins onéreux qu’un pare-feu logiciel, et il assure une plus grande protection pour le réseau).

- Le pare-feu logiciel. Installé directement sur l’ordinateur, le pare-feu logiciel joue un rôle similaire au pare-feu matériel mais de façon locale. Il contrôle les paquets de données entrants et sortants et peut les bloquer si nécessaire. Son prix est moins élevé qu’un pare-feu matériel, et son utilisation est privilégiée lorsqu’il s’agit de protéger uniquement un ordinateur.

> Ces parfeu peuvent ou non réaliser les différent typer de filtrage suivant :

- Le pare-feu à états. Il vérifie que chaque paquet est conforme à une connexion en cours. Il s’assure donc que le paquet est bien la suite d’un précédent paquet, et la réponse à un paquet dans le sens inverse.

- Le pare-feu sans état. Il contrôle séparément chaque paquet en vérifiant qu’il répond aux règles définies.

- Le pare-feu applicatif. Aussi appelé proxy, le filtrage applicatif joue le rôle de filtre au niveau applicatif. Ce firewall contrôle la conformité complète du paquet à un protocole attendu.

- Le pare-feu identifiant. Il associe les utilisateurs et l’IP, pour suivre l’activité réseau de chaque utilisateur.

- Le pare-feu personnel. Ce firewall est installé directement sur un ordinateur et agit comme un pare-feu à états pour lutter contre les virus informatiques.