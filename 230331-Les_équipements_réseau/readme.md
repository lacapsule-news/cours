# Les équipements réseau

## Théorie

### Les équipements réseau

> Pour administrer et optimiser les flux de communication entre les machines, il est nécessaire de mettre en place certains équipements.

#### Help with osi

OSI | TCP
------|------
Application | Application
Présentation | Application
Sessions | Application
Transport(TCP) | Transport(TCP)
Réseau(IP) | Réseau(IP)
Liaison(MAC) | Accès réseau
Physique | Accès réseau

#### Le Hub

> Le Hub agissant a la couche 1 du model osi, il va récupère le réseau et le renvoyer a tout le monde, les hôte devront faire le traitement de si oui on non la transmission est pour eux.

#### Le Switch

> Le switch agissant sur le niveau deux du modèle osi, il va être capable de faire transiter le réseau vers la bonne machine, pour ça il utilise le protocole ARP. Certains switchs récent sont capables de faire du vlan afin de segmenter le réseau, pour les autres il faudra passer par du VLSM (Variable Length Subnet Mask).

#### Le routeur

> Le routeur agit à la couche 3 du modèle osi, cela lui permettra de router plusieurs réseaux entre eux grâce à leur adresse IP, les deux protocoles à connaître sont NAT(Network adresse Translation) et PAT(Port adresse Translation)

#### Le Transport

> Afin de pouvoir se connecter à un autre appareil il est important que les différents appareils puissent parler la même langue, c'est là que la couche transport rentre en jeu, grace au protocole Ucp et Tcp elle va définir comment les informations vont circuler et s'il faut vérifier que toutes les données soient arrivées.

> Par exemple lors d'un appel on ne veut pas que d'anciens paquet soit renvoyer, cela impacterais la qualité du son.

#### Le reste

> Au-dessus du transport vienne se placer les applications, souvent segmenté car le transport en réseau et assez exigeant.

> Par exemple lorsqu'un client arrive sur le réseau le dhcp :
- Le client va demander a tout le réseau grâce a du broadcast qui peut lui donner une adresse ip,
- Le serveur dhcp la prendre en compte ca demande et lui proposé une ip
- Le client va répondre afin d'être sur que tout ce soit bien passé
- Le serveur va lui dire qu'il a bien pris en compte sa réponse

#### Le firewall


> Le firewall est un mot un peut four tout dans le sens ou il existe des firewall pour tout. Cependant voici les principaux firewall réseau :


- Le pare-feu matériel. Ce type de firewall est installé à l’entrée et à la sortie du réseau local. Son installation est généralement plus coûteuse que le firewall logiciel, mais il garantit davantage de protection en terme de sécurité. Cette solution est notamment privilégiée pour les réseaux comportant plusieurs ordinateurs, par exemple dans le cadre de sociétés privées (le pare-feu matériel se révèle alors moins onéreux qu’un pare-feu logiciel, et il assure une plus grande protection pour le réseau).

- Le pare-feu logiciel. Installé directement sur l’ordinateur, le pare-feu logiciel joue un rôle similaire au pare-feu matériel mais de façon locale. Il contrôle les paquets de données entrants et sortants et peut les bloquer si nécessaire. Son prix est moins élevé qu’un pare-feu matériel, et son utilisation est privilégiée lorsqu’il s’agit de protéger uniquement un ordinateur.

> Ces parfeu peuvent ou non réaliser les différent typer de filtrage suivant :

- Le pare-feu à états. Il vérifie que chaque paquet est conforme à une connexion en cours. Il s’assure donc que le paquet est bien la suite d’un précédent paquet, et la réponse à un paquet dans le sens inverse.

- Le pare-feu sans état. Il contrôle séparément chaque paquet en vérifiant qu’il répond aux règles définies.

- Le pare-feu applicatif. Aussi appelé proxy, le filtrage applicatif joue le rôle de filtre au niveau applicatif. Ce firewall contrôle la conformité complète du paquet à un protocole attendu.

- Le pare-feu identifiant. Il associe les utilisateurs et l’IP, pour suivre l’activité réseau de chaque utilisateur.

- Le pare-feu personnel. Ce firewall est installé directement sur un ordinateur et agit comme un pare-feu à états pour lutter contre les virus informatiques.

## Pratique

### 05 SIMPLE WEB SERVER

#### SETUP

- Récupérez la ressource "simplewebserver.zip" depuis l’onglet dédié sur Ariane.


- Importez le fichier "simplewebserver.gns3project" dans GNS3 et démarrez tous les équipements réseau via le bouton "Start".


> Cette simulation de réseau contient seulement un routeur relié à internet qui distribue les adresses IP en DHCP ainsi qu’un switch.


Créez et reliez entre-elles les machines suivantes :
- 1 PC virtuel ne servant qu’à vérifier la connectivité à internet
- 2 VM Debian (un client "Debian" et un serveur "Web-Server")

- À partir d’un terminal communiquant avec "PC1", demandez l’attribution d’une adresse IP via le protocole DHCP.


- Vérifiez que l’attribution a bien fonctionné en tentant un ping vers le routeur.


- Vérifiez que le réseau est bien connecté à internet en tentant un ping vers google.com.

#### INSTALLATION D’UN SERVEUR WEB

> Sur la VM "Web-Server", installez le serveur web Nginx qui fera office de serveur web censé distribuer une simple page HTML par défaut.

> Sur la VM "Debian", tentez de communiquer avec le serveur web via curl.

> Si vous récupérez une page HTML contenant le message "Welcome to nginx", cela signifie que le serveur web a bien été installé et qu’il est opérationnel.

### 04 DEBIAN IN THE LAN

#### CRÉATION D’UN LAB COMPLET

> Sur GNS3, créez un nouveau projet nommé "debianinthelan".

Ajoutez les équipements suivants :
- 1 routeur Cisco nommé "Router"
- 1 switch nommé "Switch1"
- 1 PC virtuels nommés "PC1"

- En vous inspirant du challenge précédent, créez un nouveau template de machine virtuelle Debian (catégorie "Guests" dans "Appliances from server").


- Ajoutez le nouvel équipement précédemment importé et nommez-le "Debian".


- Reliez le routeur à une interface réseau du switch.


- Reliez toutes les machines virtuelles à une interface réseau du switch.


- Démarrez tous les équipements réseau à l’exception de la VM "Debian".


#### ATTRIBUTION DES ADRESSES IP

> À partir d’un terminal communiquant avec "Router" et en récupérant les commandes vues dans le challenge précédent, paramétrez le protocole DHCP qui permettra à toutes les machines du réseau d’obtenir automatiquement une adresse IP.


> À partir d’un terminal communiquant avec "PC1", demandez l’attribution d’une adresse IP via le protocole DHCP.

> Vérifiez que l’attribution a bien fonctionné en tentant un ping vers le routeur.

> Démarrez la VM "Debian" puis à partir d’un terminal communiquant avec cette machine, constatez que le système d’exploitation a automatiquement demandé l’attribution d’une adresse IP via DHCP.


> Enfin, vérifiez que toutes les machines sont bien connectées au réseau et qu’elles puissent communiquer entre elles via ping.

### 03 HOME SWEET HOME

#### CRÉATION D’UN RÉSEAU AVEC ROUTEUR

> Sur GNS3, créez un nouveau projet nommé "homesweethome".


Ajoutez les équipements suivants :
- 1 switch nommé "Switch1"
- 3 PC virtuels respectivement nommés "PC1", "PC2" et "PC3"

> Créez un nouveau template de routeur de la marque Cisco en suivant scrupuleusement les étapes décrites dans la vidéo ci-dessous.

- Ajoutez le nouvel équipement réseau précédemment importé et nommez-le "Router" en double-cliquant sur son nom.


- Reliez le routeur à une interface réseau du switch.


- Reliez chaque PC virtuel à une interface réseau du switch (l’ordre importe peu).


- Démarrez tous les équipements réseau.


#### ATTRIBUTION DES ADRESSES IP

> À partir d’un terminal communiquant avec "Router", exécutez les commandes suivantes afin de paramétrer le protocole DHCP qui permettra à toutes les machines du réseau d’obtenir automatiquement une adresse IP de la part du routeur.


1. Router# configure terminal
1. Router# interface FastEthernet0/0
1. Router# ip address 192.168.1.1 255.255.255.0
1. Router# no shutdown
1. Router# ip dhcp pool home
1. Router# network 192.168.1.0 255.255.255.0
1. Router# default-router 192.168.1.1
1. Router# dns-server 192.168.1.1
1. Router# end
1. Router# copy running-config startup-config


> Pour information, le routeur s’est manuellement attribué l’adresse IP suivante : 192.168.1.1.


- À partir d’un terminal communiquant avec "PC1", exécutez les commandes suivantes afin de demander l’attribution d’une adresse IP via le protocole DHCP.


1. PC1# ip dhcp
1. PC1# save
1. PC1# show ip


> Lors de la première commande, vous êtes censé voir les lettres "DORA" s’afficher.
Cette étape correspond au processus DORA permettant la découverte du réseau afin de rechercher un serveur DHCP (rôle joué par le routeur) et lui demander une adresse IP.


- Vérifiez que l’attribution a bien fonctionné en tentant un ping vers le routeur.


- Répétez les deux étapes précédentes sur "PC2" et "PC3" afin de demander l’attribution d’une adresse IP via DHCP.


- Enfin, vérifiez que toutes les machines sont bien connectées au réseau et qu’elles puissent communiquer entre elles via ping.


>Note : la configuration de chaque équipement est sauvegardée (grâce à la commande save) et sera persistante après un redémarrage.
Veillez tout de même à ce que le routeur soit démarré quelques secondes avant les autres machines du réseau, sinon l’attribution d’adresse IP via DHCP risque d’échouer.

### 02 Simple lan

#### CRÉATION D’UN SIMPLE RÉSEAU

- Sur GNS3, créez un nouveau projet nommé "simplelan".


- Ajoutez les équipements suivants à partir du menu "Browse all devices" situé à gauche :


> 1 switch (Ethernet switch) nommé "Switch1"
> 3 PC virtuels (VPCS) respectivement nommés "PC1", "PC2" et "PC3"

- Reliez l’interface "Ethernet0" de "PC1" à l’interface "Ethernet0" de "Switch1" grâce à l’option "Add a link" située sur le menu de gauche.


- Reliez l’interface "Ethernet0" de "PC2" à l’interface "Ethernet1" de "Switch1".


- Reliez l’interface "Ethernet0" de "PC3" à l’interface "Ethernet2" de "Switch1".


- Cliquez sur le bouton "Start" afin de démarrer tous les équipements réseau.

#### ATTRIBUTION DES ADRESSES IP

> À partir d’un terminal communiquant avec "PC1", exécutez les commandes suivantes afin de lui attribuer manuellement l’adresse IP "192.168.1.2".


- PC1# ip 192.168.1.2 255.255.255.0

- PC1# save


> En suivant la même méthode que l’étape précédente, attribuez manuellement l’adresse IP "192.168.1.3" à "PC2".



- Attribuez manuellement l’adresse IP "192.168.1.4" à "PC3".



> Afin de vérifier que les 3 machines sont bien connectées et identifiables sur le réseau grâce à votre adresse IP, utilisez la commande ping sur chaque PC de la façon suivante.


- PC1# ping 192.168.1.3

- PC1# ping 192.168.1.4


- PC2# ping 192.168.1.2

- PC2# ping 192.168.1.4


- PC3# ping 192.168.1.2

- PC3# ping 192.168.1.3