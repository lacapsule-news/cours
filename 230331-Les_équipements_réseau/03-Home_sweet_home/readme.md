# HOME SWEET HOME

## CRÉATION D’UN RÉSEAU AVEC ROUTEUR

> Sur GNS3, créez un nouveau projet nommé "homesweethome".


Ajoutez les équipements suivants :
- 1 switch nommé "Switch1"
- 3 PC virtuels respectivement nommés "PC1", "PC2" et "PC3"

> Créez un nouveau template de routeur de la marque Cisco en suivant scrupuleusement les étapes décrites dans la vidéo ci-dessous.

- Ajoutez le nouvel équipement réseau précédemment importé et nommez-le "Router" en double-cliquant sur son nom.


- Reliez le routeur à une interface réseau du switch.


- Reliez chaque PC virtuel à une interface réseau du switch (l’ordre importe peu).


- Démarrez tous les équipements réseau.


## ATTRIBUTION DES ADRESSES IP

> À partir d’un terminal communiquant avec "Router", exécutez les commandes suivantes afin de paramétrer le protocole DHCP qui permettra à toutes les machines du réseau d’obtenir automatiquement une adresse IP de la part du routeur.


1. Router# configure terminal
1. Router# interface FastEthernet0/0
1. Router# ip address 192.168.1.1 255.255.255.0
1. Router# no shutdown
1. Router# ip dhcp pool home
1. Router# network 192.168.1.0 255.255.255.0
1. Router# default-router 192.168.1.1
1. Router# dns-server 192.168.1.1
1. Router# end
1. Router# copy running-config startup-config


> Pour information, le routeur s’est manuellement attribué l’adresse IP suivante : 192.168.1.1.


- À partir d’un terminal communiquant avec "PC1", exécutez les commandes suivantes afin de demander l’attribution d’une adresse IP via le protocole DHCP.


1. PC1# ip dhcp
1. PC1# save
1. PC1# show ip


> Lors de la première commande, vous êtes censé voir les lettres "DORA" s’afficher.
Cette étape correspond au processus DORA permettant la découverte du réseau afin de rechercher un serveur DHCP (rôle joué par le routeur) et lui demander une adresse IP.


- Vérifiez que l’attribution a bien fonctionné en tentant un ping vers le routeur.


- Répétez les deux étapes précédentes sur "PC2" et "PC3" afin de demander l’attribution d’une adresse IP via DHCP.


- Enfin, vérifiez que toutes les machines sont bien connectées au réseau et qu’elles puissent communiquer entre elles via ping.


>Note : la configuration de chaque équipement est sauvegardée (grâce à la commande save) et sera persistante après un redémarrage.
Veillez tout de même à ce que le routeur soit démarré quelques secondes avant les autres machines du réseau, sinon l’attribution d’adresse IP via DHCP risque d’échouer.