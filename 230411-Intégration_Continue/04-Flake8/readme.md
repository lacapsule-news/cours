# Flake8

## TEST D’INTÉGRATION CONTINUE

Les exemples des challenges précédents vous ont permis de découvrir quelques fonctionnalités de GitLab CI/CD. Vous allez maintenant appliquer le concept de continuous intégration (CI) en exécutant un linter de code.

👉 Récupérez la ressource "flake8.zip" depuis l’onglet dédié sur Ariane.

👉 Récupérez le projet Python dans les ressources et hébergez-le sur un nouveau repo GitLab nommé "ultimate-length-app".

👉 Faites en sorte d’exécuter le linter Flake8 lors d’un push sur n’importe quelle branche.

Pour ce faire, vous aurez sans doute besoin d’utiliser la notion d’image afin de lancer un job dans un environnement spécifique (avec Python pré-installé, par exemple 😉)

Si la pipeline échoue, c’est que vous avez réussi à mettre en place le linter, car le fichier "app.py" comporte des erreurs de style de code (non-respect des sauts de lignes et des espaces, import inutilisé…)

👉 Même si c’est plutôt le rôle des développeurs, modifiez le code afin de ne plus avoir d’erreurs du linter.

Veillez à ne pas supprimer la ligne "import os", mais plutôt l’ignorer via flake8

```
running_the_app:
  stage: run
  image: python:alpine
  script:
    - python -m pip install flake8
    - flake8 app.py
```