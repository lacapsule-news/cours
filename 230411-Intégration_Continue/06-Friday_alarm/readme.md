# Friday alarm

## PIPELINE DE VÉRIFICATION

👉 En repartant du répertoire créé dans le challenge précédent créez un nouveau job qui sera en erreur si le commit a lieu un vendredi.

La demande de merge request est donc censée se bloquer automatiquement si elle est faite un vendredi, car vous découvrez (ou connaissez) l’adage : on ne met pas un production un vendredi !

👉 Testez le job en modifiant le jour où il est censé être en erreur pour correspondre à aujourd’hui. Vérifiez que la demande de merge request échoue également.

```
secure_weekend:
  stage: dontfuckupmyweekend
  script:
    - if [[ $(date +%u) -gt 4 ]];then echo 'Failed'; exit 1;fi;
```