# Maxium weight

## PIPELINE DE VÉRIFICATION V2

Ce challenge va vous permettre de créer un nouveau job qui sera en erreur si un des fichiers qui a été commit dépasse une certaine taille, paramétrable directement depuis GitLab.

👉 Repartez du répertoire GitLab utilisé dans le challenge précédent

👉 Depuis l’interface GitLab, créez une variable personnalisée nommée "MAXIMUM_WEIGHT" qui a pour valeur "2M" (pour 2 megabytes ou MB)

👉 Créez un script "checkSize.sh" recevant une taille maximum en argument (par exemple 12M pour 12 megabytes) afin de lister les fichiers excédant cette taille maximum.

```
./checkSize 12M
./doc.pdf 13M
./videos/demo.mp4 24M
```

👉 Créez un job chargé d’exécuter le script précédemment créé en passant la variable personnalisée "MAXIMUM_WEIGHT" en tant qu'argument.

La pipeline est censée être en erreur si un fichier ou plus excède la taille paramétrée depuis GitLab.

👉 Testez le job en créant un fichier de 5 MB via la commande dd et poussez-le vers le dépot de distant. La pipeline est censée échouer car le fichier excède la limite de 2 MB.

👉 Depuis l’interface de GitLab, modifiez la variable personnalisée "MAXIMUM_WEIGHT" avec "10M" comme nouvelle valeur. Relancez la dernière pipeline en échec qui devrait désormais passer.

```
heavyfile:
  stage: test-file-size
  script:
    - echo $MAXIMUM_WEIGHT
    - find ./ -type f -not -path '*/.*' -size +$MAXIMUM_WEIGHT -print0 | while read -d $'\0' file;do echo "$file $(du -h "$file" | cut -f1)";done;
```