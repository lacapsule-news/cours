# Intégration continue

## Contexte

Chaque évolution ou correction d'une application fera l'objet d'une mise en production.

La mise en production nécessite une etrême vigilance !

Il faut éviter de laisser passer un bug au risque de rendre l'application inexxploitable.

L'intégration et le déploiment continu (CI/CD) sont des méthodes de contrôle et de vérification du pipeline de mise en production.

En fluidifiant le pipeline de mise en prodcucation, les développeurs gagnent beaucoup de temps !

Cela permet de faire évoluer l'application plus souvent, on parle alors d'itération courte.

### Fonctionnement

L'intégration continu sont indissociables.
Pourtant, chacun à un rôle et un périmètre bien précis.

- L'intégration continue représente les automatisations mise en place impliquant le code source.
- Déploiment continue représente les automatisation mises en place impliquant le serveur de production.

Gitlab permet via un fichier de configuration de mettre en places des automatisations qui se déclencheront à chaque push.

Voici des exemples de tests automatiques que l'on peut mettre en place ans une pipeline d'intégration continue.
- Executer un linter
- Vérification de dépendances
- Imposer des contraintes à l'équipe
- Executer des test unitaires

Si un des test n'est pas validé, la fusion des la branche est bloquée jusqu'à la résolution du problème.