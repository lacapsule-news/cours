# Last commit

## VARIABLES PRÉDÉFINIES & PIPELINES

L’objectif de ce challenge est de récupérer et exploiter les informations d’un commit lors de l’exécution d’une pipeline : ID de commit, auteur, fichiers modifiés, etc.

👉 Reprenez le répertoire du challenge précédent et supprimez le contenu du fichier ".gitlab-ci.yml".

👉 Grâce à la notion de variables prédéfinies avec GitLab, créez un job "displayinfos" chargé d’afficher les infos suivantes :

- L’auteur du commit au format "Name <email>"
- L’ID du commit (au format SHA raccourci de 8 caractères)
- La liste des fichiers modifiés depuis le dernier commit

👉 Créez un nouveau script shell nommé "checkAuthor.sh" recevant un paramètre l’auteur d’un commit au format "Name <email>".

Si l’email de l’auteur contient bien "@", afficher "Valid address", sinon afficher "Invalid address" et terminer le script avec l’instruction "exit 1".

👉 Modifiez le fichier "gitlab-ci.yml" afin de créer un nouveau job "check-author" dans le stage "test" afin d’exécuter le script créé précédemment en lui passant la variable prédéfinie $CI_COMMIT_AUTHOR en guise d’argument.

👉 Testez le bon fonctionnement de votre script et l’exécution du job en modifiant la condition de vérification de l’email pour qu’elle échoue systématiquement (en vérifiant si l’email contient deux "@" de suite, par exemple).

Le job est censé s’afficher en "failed" sur GitLab, car le script s’est terminé avec l’instruction "exit 1", soit un code générique synonyme d’erreur.