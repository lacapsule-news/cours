# Intégration continue

## Contexte

Chaque évolution ou correction d'une application fera l'objet d'une mise en production.

La mise en production nécessite une etrême vigilance !

Il faut éviter de laisser passer un bug au risque de rendre l'application inexxploitable.

L'intégration et le déploiment continu (CI/CD) sont des méthodes de contrôle et de vérification du pipeline de mise en production.

En fluidifiant le pipeline de mise en prodcucation, les développeurs gagnent beaucoup de temps !

Cela permet de faire évoluer l'application plus souvent, on parle alors d'itération courte.

### Fonctionnement

L'intégration continu sont indissociables.
Pourtant, chacun à un rôle et un périmètre bien précis.

- L'intégration continue représente les automatisations mise en place impliquant le code source.
- Déploiment continue représente les automatisation mises en place impliquant le serveur de production.

Gitlab permet via un fichier de configuration de mettre en places des automatisations qui se déclencheront à chaque push.

Voici des exemples de tests automatiques que l'on peut mettre en place ans une pipeline d'intégration continue.
- Executer un linter
- Vérification de dépendances
- Imposer des contraintes à l'équipe
- Executer des test unitaires

Si un des test n'est pas validé, la fusion des la branche est bloquée jusqu'à la résolution du problème.

## Exo

### 07 - Maxium weight

#### PIPELINE DE VÉRIFICATION V2

Ce challenge va vous permettre de créer un nouveau job qui sera en erreur si un des fichiers qui a été commit dépasse une certaine taille, paramétrable directement depuis GitLab.

👉 Repartez du répertoire GitLab utilisé dans le challenge précédent

👉 Depuis l’interface GitLab, créez une variable personnalisée nommée "MAXIMUM_WEIGHT" qui a pour valeur "2M" (pour 2 megabytes ou MB)

👉 Créez un script "checkSize.sh" recevant une taille maximum en argument (par exemple 12M pour 12 megabytes) afin de lister les fichiers excédant cette taille maximum.

```
./checkSize 12M
./doc.pdf 13M
./videos/demo.mp4 24M
```

👉 Créez un job chargé d’exécuter le script précédemment créé en passant la variable personnalisée "MAXIMUM_WEIGHT" en tant qu'argument.

La pipeline est censée être en erreur si un fichier ou plus excède la taille paramétrée depuis GitLab.

👉 Testez le job en créant un fichier de 5 MB via la commande dd et poussez-le vers le dépot de distant. La pipeline est censée échouer car le fichier excède la limite de 2 MB.

👉 Depuis l’interface de GitLab, modifiez la variable personnalisée "MAXIMUM_WEIGHT" avec "10M" comme nouvelle valeur. Relancez la dernière pipeline en échec qui devrait désormais passer.

```
heavyfile:
  stage: test-file-size
  script:
    - echo $MAXIMUM_WEIGHT
    - find ./ -type f -not -path '*/.*' -size +$MAXIMUM_WEIGHT -print0 | while read -d $'\0' file;do echo "$file $(du -h "$file" | cut -f1)";done;
```

### 06 - Friday alarm

#### PIPELINE DE VÉRIFICATION

👉 En repartant du répertoire créé dans le challenge précédent créez un nouveau job qui sera en erreur si le commit a lieu un vendredi.

La demande de merge request est donc censée se bloquer automatiquement si elle est faite un vendredi, car vous découvrez (ou connaissez) l’adage : on ne met pas un production un vendredi !

👉 Testez le job en modifiant le jour où il est censé être en erreur pour correspondre à aujourd’hui. Vérifiez que la demande de merge request échoue également.

```
secure_weekend:
  stage: dontfuckupmyweekend
  script:
    - if [[ $(date +%u) -gt 4 ]];then echo 'Failed'; exit 1;fi;
```

### 05 - Api checker

#### MERGE REQUESTS & PIPELINES

L’objectif de ce challenge est de créer un pipeline sur une branche différente (autre que main) afin de vérifier qu’une API externe répond et ainsi bloquer une merge request si ce n’est pas le cas.

👉 Créez un nouveau répertoire GitLab nommé "awesome-app", versionnez quelques fichiers (leur contenu n’est pas important pour le déroulé du challenge) et faîtes un push sur la branche principale.

👉 Côté GitLab, configurez le répertoire pour que les merge requests soient appliquées (et validables) uniquement lorsque toutes les pipelines de CI/CD ont réussies.

👉 Sur la branche principale, créez un fichier ".gitlab-ci.yml" qui devra exécuter un job chargé de vérifier qu’une API (potentiellement utilisée dans le projet) est bien joignable afin de valider l’exécution de la pipeline.

Vous pouvez choisir la "fausse" API JSONPlaceholder et l’endpoint "GET /users".

👉 Créez une nouvelle branche locale "myfeature" et ajoutez ou modifiez un fichier.

👉 Poussez la branche "myfeature" vers le dépôt distant et faites une demande de merge request sur la branche principale.

`git push origin myfeature`

👉 Approuvez la demande de merge request. Elle est censée être complétée uniquement si la pipeline exécutée lors le dernier commit de la branche "myfeature" est validée.

👉 Testez la configuration de votre répertoire GitLab en modifiant le job afin qu’il échoue systématiquement (en modifiant avec une URL d’API invalide, par exemple). Vérifiez que votre demande de merge request échoue également.

```
running_the_app:
  stage: test-api
  script:
    - apk update && apk add curl
    - curl https://jsonplaceholder.typicode.com/users
```

### 04 - Flake8

#### TEST D’INTÉGRATION CONTINUE

Les exemples des challenges précédents vous ont permis de découvrir quelques fonctionnalités de GitLab CI/CD. Vous allez maintenant appliquer le concept de continuous intégration (CI) en exécutant un linter de code.

👉 Récupérez la ressource "flake8.zip" depuis l’onglet dédié sur Ariane.

👉 Récupérez le projet Python dans les ressources et hébergez-le sur un nouveau repo GitLab nommé "ultimate-length-app".

👉 Faites en sorte d’exécuter le linter Flake8 lors d’un push sur n’importe quelle branche.

Pour ce faire, vous aurez sans doute besoin d’utiliser la notion d’image afin de lancer un job dans un environnement spécifique (avec Python pré-installé, par exemple 😉)

Si la pipeline échoue, c’est que vous avez réussi à mettre en place le linter, car le fichier "app.py" comporte des erreurs de style de code (non-respect des sauts de lignes et des espaces, import inutilisé…)

👉 Même si c’est plutôt le rôle des développeurs, modifiez le code afin de ne plus avoir d’erreurs du linter.

Veillez à ne pas supprimer la ligne "import os", mais plutôt l’ignorer via flake8

```
running_the_app:
  stage: run
  image: python:alpine
  script:
    - python -m pip install flake8
    - flake8 app.py
```

### 03 - Last commit

#### VARIABLES PRÉDÉFINIES & PIPELINES

L’objectif de ce challenge est de récupérer et exploiter les informations d’un commit lors de l’exécution d’une pipeline : ID de commit, auteur, fichiers modifiés, etc.

👉 Reprenez le répertoire du challenge précédent et supprimez le contenu du fichier ".gitlab-ci.yml".

👉 Grâce à la notion de variables prédéfinies avec GitLab, créez un job "displayinfos" chargé d’afficher les infos suivantes :

- L’auteur du commit au format "Name <email>"
- L’ID du commit (au format SHA raccourci de 8 caractères)
- La liste des fichiers modifiés depuis le dernier commit

👉 Créez un nouveau script shell nommé "checkAuthor.sh" recevant un paramètre l’auteur d’un commit au format "Name <email>".

Si l’email de l’auteur contient bien "@", afficher "Valid address", sinon afficher "Invalid address" et terminer le script avec l’instruction "exit 1".

👉 Modifiez le fichier "gitlab-ci.yml" afin de créer un nouveau job "check-author" dans le stage "test" afin d’exécuter le script créé précédemment en lui passant la variable prédéfinie $CI_COMMIT_AUTHOR en guise d’argument.

👉 Testez le bon fonctionnement de votre script et l’exécution du job en modifiant la condition de vérification de l’email pour qu’elle échoue systématiquement (en vérifiant si l’email contient deux "@" de suite, par exemple).

Le job est censé s’afficher en "failed" sur GitLab, car le script s’est terminé avec l’instruction "exit 1", soit un code générique synonyme d’erreur.

### 02 - You had one job

#### CRÉATION D’UNE PIPELINE

La méthode CI/CD  consiste à construire, tester et déployer en permanence des modifications apportées au code d’une application et ce de manière itérative.
Cette méthode permet de réduire le risque de développer et déployer un code de mauvaise qualité ou buggé, le tout avec le moins d’intervention humaine possible grâce à l’automatisation et les pipelines.

Vous allez commencer par vous concentrer sur la partie intégration continue (CI) qui est pratique visant à build et tester automatiquement chaque changement soumis à un dépôt Git distant afin de garantir que ces modifications passent les normes de conformité du code établies pour l’application.

👉 Créez un repository GitLab nommé "testci".


👉 En vous inspirant de la documentation de GitLab et à partir de VS Code, créez un fichier ".gitlab-ci.yml" basique content un seul stage nommé "test" et contenant un job "say-hello" chargé de faire une simple commande : echo "Hello world!".



👉 Versionnez le fichier ".gitlab-ci.yml" et poussez la branche courante vers le dépôt distant.


👉 Sur GitLab, vérifiez l’exécution de la pipeline en regardant le résultat du job "sayhello" dans le stage "test" afin de repérer l’affichage du fameux message "Hello world!".

#### CONFIGURATION D’UNE PIPELINE

Pour l’instant, cette pipeline s’exécutera à chaque push sur le dépôt distant, peu importe la branche impliquée.

👉 Trouvez un moyen de lancer le job "say-hello" uniquement lors d’un push sur la branche "development" et testez ces changements en créant une branche en local puis en la poussant vers GitLab.