# Terraform 

## Contexte

Kubernetes repose sur la notion de cluster à la demande.
Il est uniquement basé sur le concept de conteneurisation.

Certaines applications ne peuvent pas être conteneurisées, mais nécessitent tout de même une infrastructure bien précise.

L'infrastructure devra être définie en fonction des besoins de chaque applications.

La mise en place manuelle de l'infrastructure devient fastidieuse voire impossible.

### Fonctionnement

Terraform est une solution d'Infrastructure As Code (IaC) qui permet de définir des modèles d'infrastructure.

Le modèle est défini dans un fichier de configuration qui détaille précisément les ressources cloud composant l'infrastructure.

Type de serveurs, nombre, localisation, puissance...

Via la CLI de terraform , les ressources décrites dans le fichier de configuration seront automatiquement crées.

On parle alors de provisionnement.

## Exo

### 04 - TERRAFORM CLOUD

#### SETUP

👉 Créez un compte Terraform Cloud.

👉 Exécutez la commande suivante pour relier la CLI de Terraform au compte Terraform Cloud.

`terraform login`

👉 Créez une nouvelle organisation Terraform : Start from scratch > Create a new organization (le nom doit être unique)

👉 Créez un nouveau workspace nommé "ec2-deployment" (CLI-driven workflow)

#### DÉPLOIEMENT VIA TERRAFORM CLOUD

👉 Créez un nouveau dossier nommé "terraformcloud".
 
👉 Reprenez le fichier de configuration Terraform du challenge précédent et ajoutez la section nécessaire afin de le lier avec votre workspace.

👉 Appliquez le fichier de configuration grâce à la commande habituelle.

Si vous avez une erreur de type "no valid credential sources for Terraform AWS Provider", c’est parce que le déploiement n’est plus effectué en local, mais du côté de Terraform Cloud qui n’a pas connaissance des clés d’accès AWS.

👉 Côté Terraform Cloud, trouvez un moyen de créer les variables d’environnement nécessaires pour l’authentification auprès d’AWS

👉 Appliquez de nouveau le fichier de configuration.

👉 Vérifiez le statut du déploiement de l’instance sur Terraform Cloud puis directement sur AWS.

👉 N’oubliez pas de supprimer toutes les ressources AWS créées via Terraform grâce à la commande suivante.

`terraform destroy`

### 03 - Deploy this VM

#### PRÉREQUIS

👉 Commencez par vérifier que la CLI d’AWS est installée sur votre machine hôte grâce à la commande suivante.

`aws --version`

👉 À partir de la console d’AWS, trouvez un moyen de générer des clés d’accès (access key id et secret access key)

👉 Exportez les deux variables correspondantes aux clés d’accès en exécutant les commandes suivantes.

export AWS_ACCESS_KEY_ID=XXXX
export AWS_SECRET_ACCESS_KEY=XXXX

Cette étape est nécessaire pour que Terraform puisse s’authentifier auprès d’AWS.

#### DÉPLOIEMENT D’UNE INSTANCE EC2

👉 Créez un nouveau dossier nommé "deploythisvm".

👉 En vous inspirant de la documentation de Terraform, créez un fichier de configuration Terraform nommé "main.tf" qui devra déployer une instance EC2 chez AWS en respectant les contraintes suivantes :

Une instance de type "t2.micro" devra être déployée dans le datacenter parisien d’Amazon
Le système d’exploitation utilisé sera Debian dans sa dernière version éligible à l’offre gratuite
Le nom d’affichage de l’instance EC2 sera "terraform-test"

👉 Initialisez le dossier contenant le fichier de configuration de Terraform.

👉 Avant d’appliquer le fichier de configuration Terraform, formattez et validez-le grâce aux commandes suivantes.

```
terraform fmt
terraform validate
```

👉 Appliquez le fichier de configuration Terraform afin de créer l’instance EC2 chez AWS.

👉 Vérifiez que l’instance EC2 a bien été créée sur la console d’AWS et qu’elle respecte les contraintes imposées plus tôt, notamment le système d’exploitation.

👉 Trouvez la commande terraform permettant d’inspecter l’état du déploiement et profitez-en pour récupérer l’adresse IP publique.

### 02 - START WITH TERRAFORM

#### INSTALLATION DE TERRAFORM

👉 Installez le package Terraform en suivant la documentation (section "Install Terraform", onglet "Linux") : https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli

👉 Vérifiez l’installation de Terraform grâce à la commande suivante.

`terraform -version`

#### FICHIER DE CONFIGURATION TERRAFORM

Même si Terraform est un outil d’IaC généralement utilisé pour déployer des infrastructures auprès des fournisseurs de cloud (AWS par exemple), il est théoriquement possible de l’utiliser pour déployer des conteneurs Docker, de la même manière qu’un Docker Compose.
C’est l’exemple parfait pour vous familiariser avec le concept de fichier de configuration Terraform !

👉 Étant donné que chaque fichier de configuration Terraform doit être dans son propre dossier, créez un nouveau dossier nommé "startwithterraform".

👉 En vous inspirant de la documentation (section "Quick start tutorial"), créez un fichier de configuration Terraform nommé "main.tf" qui devra créer un conteneur basé sur l’image officielle de httpd et rediriger le port 8888 vers le port 80 du conteneur.

👉 Initialisez le dossier contenant le fichier de configuration de Terraform grâce à la commande suivante.

`terraform init`

👉 Appliquez le fichier de configuration Terraform afin de provisionner le conteneur Docker basé sur l’image httpd grâce à la commande suivante.

`terraform apply`

👉 Vérifiez le lancement du conteneur en visitant l’URL /localhost:8888 et en exécutant la commande docker ps.

👉 Pour finir, arrêtez le conteneur, mais sans passer par une commande docker, en utilisant la commande terraform.