# Deploy this VM

## PRÉREQUIS

👉 Commencez par vérifier que la CLI d’AWS est installée sur votre machine hôte grâce à la commande suivante.

`aws --version`

👉 À partir de la console d’AWS, trouvez un moyen de générer des clés d’accès (access key id et secret access key)

👉 Exportez les deux variables correspondantes aux clés d’accès en exécutant les commandes suivantes.

export AWS_ACCESS_KEY_ID=XXXX
export AWS_SECRET_ACCESS_KEY=XXXX

Cette étape est nécessaire pour que Terraform puisse s’authentifier auprès d’AWS.

## DÉPLOIEMENT D’UNE INSTANCE EC2

👉 Créez un nouveau dossier nommé "deploythisvm".

👉 En vous inspirant de la documentation de Terraform, créez un fichier de configuration Terraform nommé "main.tf" qui devra déployer une instance EC2 chez AWS en respectant les contraintes suivantes :

Une instance de type "t2.micro" devra être déployée dans le datacenter parisien d’Amazon
Le système d’exploitation utilisé sera Debian dans sa dernière version éligible à l’offre gratuite
Le nom d’affichage de l’instance EC2 sera "terraform-test"

👉 Initialisez le dossier contenant le fichier de configuration de Terraform.

👉 Avant d’appliquer le fichier de configuration Terraform, formattez et validez-le grâce aux commandes suivantes.

```
terraform fmt
terraform validate
```

👉 Appliquez le fichier de configuration Terraform afin de créer l’instance EC2 chez AWS.

👉 Vérifiez que l’instance EC2 a bien été créée sur la console d’AWS et qu’elle respecte les contraintes imposées plus tôt, notamment le système d’exploitation.

👉 Trouvez la commande terraform permettant d’inspecter l’état du déploiement et profitez-en pour récupérer l’adresse IP publique.