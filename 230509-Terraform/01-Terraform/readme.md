# Terraform 

## Contexte

Kubernetes repose sur la notion de cluster à la demande.
Il est uniquement basé sur le concept de conteneurisation.

Certaines applications ne peuvent pas être conteneurisées, mais nécessitent tout de même une infrastructure bien précise.

L'infrastructure devra être définie en fonction des besoins de chaque applications.

La mise en place manuelle de l'infrastructure devient fastidieuse voire impossible.

### Fonctionnement

Terraform est une solution d'Infrastructure As Code (IaC) qui permet de définir des modèles d'infrastructure.

Le modèle est défini dans un fichier de configuration qui détaille précisément les ressources cloud composant l'infrastructure.

Type de serveurs, nombre, localisation, puissance...

Via la CLI de terraform , les ressources décrites dans le fichier de configuration seront automatiquement crées.

On parle alors de provisionnement.