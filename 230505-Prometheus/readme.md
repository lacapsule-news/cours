# Prometheus

## Contexte

Administrer un système d'information nécessite une analyse des métriques système en temps réel.

Chaque entité du sytème d'information expose ses métriques dans son propre format.

La récolte et l'analyse en temps réel de ses métriques devient fastidieuse voire impossible.

Prometheus premet de collecter les métriques d'une multitude de sources en temps réel.

### Fonctionnement

Il est nécessaire d'installer Prometheus sur une machine qui sera dédiée à la collecte des métriques.

L'exporter est un noeud qui permet de collecter les métriques d'une seule source.

Prometheus propose une visualisation basique métriques, mais ce n'est pas sont rôle principal.

La visualisation sous forme de dashboard devra se faire avec une solution spécialisée.

Il est possible de mettre en place des seuils d'alertes qui notifiera l'équipe dédiée pour intervenir.

[Nginx monitoring stack](https://gitea.sunburst.ml/sunburst/Nginx_monitoring_stack)

## Exo

### 05 - Alert manager

#### CRÉATION D’ALERTES

Avant de passer à la visualisation des metrics via un outil tel que Grafana, Prometheus vous réserve d’autres surprises, notamment via le principe d’Alerting où vous pouvez configurer des règles pour surveiller certaines metrics ainsi que des notifications par mail ou messagerie instantanée.

👉 En vous inspirant de la documentation de Prometheus, mettez en place l’Alertmanager sur votre serveur Linode.

👉 Créez les deux règles d’alertes suivantes :

Si le service Nginx est indisponible pendant plus d’une minute
Si le disque dur du serveur est rempli à plus de 80%

#### BONUS

👉 Mettez en place un envoi de mail automatique pour les deux règles d’alertes créées précédemment. Vous aurez certainement besoin d’une adresse Gmail pour faciliter la configuration de l’envoi de mail.

👉 Testez votre alerte en stoppant le conteneur Nginx pendant plus d’une minute .

👉 Ne supprimez pas la VM Linode, elle sera utilisée pour demain également 😉

```
  prometheus:
    container_name: prometheus
    image: prom/prometheus
    volumes:
      - ./prometheus-config/prometheus.yml:/etc/prometheus/prometheus.yml
      - ./prometheus-config/test-alert.yml:/etc/prometheus/test-alert.yml
    ports:
      - 9090:9090
    networks:
      - prom
    restart: unless-stopped
  
  alertmanager:
    container_name: alertmanager
    image: prom/alertmanager
    volumes:
      - ./alertmanager/:/etc/alertmanager/
    environment:
      - DOCKER_API_KEY=${ALERT_DISCORD_API}
    command:
      - '--config.file=/etc/alertmanager/config.yml'
      - '--storage.path=/alertmanager'
    networks:
      - prom
    restart: unless-stopped
```

```
groups:
 - name: Monitoring Loopback
   rules:
   - alert: Instance Down
     expr: up == 0
   - alert: More than 80% of cpu usage
     expr: 100 - (avg by (instance) (irate(node_cpu_seconds_total{mode="idle"}[5m])) * 100) >80
   - alert: More than 80% of ram usage
     expr: 100 - (node_memory_Active_bytes * 100 / node_memory_MemTotal_bytes) >80

 - name: Monitoring Web
   rules:
   - alert: Nginx Down
     expr: nginx_up == 0
   - alert: More than 100 connexion
     expr: nginx_connections_active > 100

 - name: Monitoring Disk Space
   rules:
   - alert: Up than 80% disk space
     expr: 100 - (node_filesystem_avail_bytes{mountpoint="/"} * 100 / node_filesystem_size_bytes{mountpoint="/"}) > 80
```

```
route:
  group_by: ['alertname', 'job']

  group_wait: 30s
  group_interval: 5m
  repeat_interval: 3h

  receiver: discord

receivers:
- name: discord
  discord_configs:
  - webhook_url: <Discord_web_hook>
```

### 04 - Nginx metrics

#### NGINX NODE EXPORTER

Maintenant que vous êtes capable de récolter les metrics du système hôte, il est temps d’aller plus loin en configurant un autre "exporter" capable de communiquer avec un web serveur et plus précisément Nginx et Nginx Prometheus Exporter.

👉 Pour commencer, installez Docker sur la VM Linode.

👉 Vérifiez l’installation de Docker en récupérant sa version via l’option "--version".

Pour simplifier l’installation du serveur web et l’export des metrics, vous allez utiliser Docker afin de créer des conteneurs qui seront capables de communiquer entre eux et avec Prometheus.

👉 Via la commande docker run, créez et démarrez un conteneur Docker basé sur l’image officielle de Nginx en exposant le port 80 sur le port 8080 de la machine hôte.

👉 Vérifiez que le serveur web a bien été démarré en visitant l’IP publique du serveur, sur le port 8080.

Comme d’habitude, si vous voyez le traditionnel message de bienvenue de Nginx, c’est que tout est opérationnel pour la suite !

👉 Trouvez un moyen d’activer la page de status de Nginx sur l’URL /stub_status en modifiant le fichier "/etc/nginx/conf.d/default.conf" sur le conteneur Nginx.

👉 Vérifiez que le serveur web expose bien l’URL "/stub_status" en visitant l’IP publique du serveur, sur le port 8080. Vous êtes censés obtenir des informations semblables à la réponse ci-dessous.

👉 En vous inspirant de la documentation de Nginx Prometheus Exporter, démarrez un conteneur basé sur l’image "nginx/nginx-prometheus-exporter" en exposant le port 9113 et en configurant l’exporter afin qu’il puisse communiquer avec l’URL "/stub_status" du serveur web Nginx.

👉 Visitez l’URL "/metrics" (sur le port exposé par Nginx Prometheus Exporter) afin de découvrir toutes les informations récoltées sur le serveur web Nginx.

👉 Assurez-vous que votre instance Prometheus soit configurée afin d’accéder aux metrics de Nginx Prometheus Exporter.

👉 À partir de l’interface graphique de Prometheus, trouvez les requêtes permettant de vous donner les informations suivantes :

Le statut du serveur Nginx (1 pour true, 0 pour false)
Le nombre total de requêtes HTTP reçues sur le serveur web
Le nombre de personnes connectées en temps réel sur le site (n’hésitez pas à ouvrir le site en navigation privée et sur plusieurs navigateurs pour tester)

👉 Ne supprimez pas la VM Linode, elle sera utilisée pour le prochain challenge.

```
  web-server:
    container_name: nginx
    image: nginx:stable
    volumes:
      - ./nginx.conf:/etc/nginx/nginx.conf
    ports:
      - 80:80
    networks:
      - nginx-metrics
    restart: unless-stopped

  web-metrics:
    container_name: nginx-exporter
    image: nginx/nginx-prometheus-exporter:0.10.0
    command:
      - -nginx.scrape-uri
      - http://nginx:8080/status
    networks:
      - prom
      - nginx-metrics
    depends_on:
      - web-server
    restart: unless-stopped
```
```

user  nginx;
worker_processes  auto;

error_log  /var/log/nginx/error.log notice;
pid        /var/run/nginx.pid;


events {
    worker_connections  1024;
}


http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile        on;
    #tcp_nopush     on;

    keepalive_timeout  65;

    #gzip  on;
    server{
        listen      8080;
        server_name nginx;

        location /status {
            stub_status;
        }
    }
    server {
        listen       80;
        listen  [::]:80;
        server_name  localhost;

        #access_log  /var/log/nginx/host.access.log  main;

        location / {
            root   /usr/share/nginx/html;
            index  index.html index.htm;
        }

        #error_page  404              /404.html;

        # redirect server error pages to the static page /50x.html
        #
        error_page   500 502 503 504  /50x.html;
        location = /50x.html {
            root   /usr/share/nginx/html;
        }

        # proxy the PHP scripts to Apache listening on 127.0.0.1:80
        #
        #location ~ \.php$ {
        #    proxy_pass   http://127.0.0.1;
        #}

        # pass the PHP scripts to FastCGI server listening on 127.0.0.1:9000
        #
        #location ~ \.php$ {
        #    root           html;
        #    fastcgi_pass   127.0.0.1:9000;
        #    fastcgi_index  index.php;
        #    fastcgi_param  SCRIPT_FILENAME  /scripts$fastcgi_script_name;
        #    include        fastcgi_params;
        #}

        # deny access to .htaccess files, if Apache's document root
        # concurs with nginx's one
        #
        #location ~ /\.ht {
        #    deny  all;
        #}
    }

}
```

### 03 - Linux metrics

#### LINUX NODE EXPORTER

Prometheus est maintenant configuré, mais il n’est capable de récolter que des metrics le concernant, ce qui n’est pas des plus excitant, on peut se l’avouer !

Evidemment, Prometheus est capable d’aller plus loin et de récolter des metrics de la machine Linux (espace disque disponible, trafic réseau, etc.) grâce au Node Exporter.

👉 En vous inspirant de la documentation de Prometheus, installez et démarrez le Node Exporter.

👉 Assurez-vous que votre instance Prometheus soit configurée afin d’accéder aux metrics de Node Exporter.
Le fichier prometheus.yml décrit les différentes sources et la fréquence à laquelle Prometheus va scraper (aspirer) les metrics.

👉 Redémarrez votre instance Prometheus et visitez l’URL "/metrics" (sur le port exposé par Node Exporter) afin de découvrir toutes les informations récoltées sur le système.

👉 À partir de l’interface graphique de Prometheus, trouvez les requêtes permettant de vous donner les informations suivantes :

- La quantité totale de ram sur le serveur (en bytes)
- Le nombre de coeurs sur le CPU (vérifiez ce nombre grâce à l’interface Linode)
- L’espace disque disponible sur le système (en bytes)
- La moyenne du trafic réseau transmis sur l’interface "eth0" (en bytes), par seconde et sur les 5 dernières minutes

👉 Ne supprimez pas la VM Linode, elle sera utilisée pour les prochains challenges.

Pour ma part je suis rester sur docker.

```
  node_exporter:
    image: quay.io/prometheus/node-exporter:latest
    container_name: node_exporter
    command:
      - '--path.rootfs=/host'
    networks:
      - prom
    pid: host
    restart: unless-stopped
    volumes:
      - '/:/host:ro,rslave'
    depends_on:
      - web-server
```

### 02 - Start with Prometheus

#### SETUP

👉 Sur Linode, créez une VM vierge nommée "test-prometheus" qui sera votre terrain de jeu pour découvrir la notion de monitoring :


- Debian 11
- Dedicated 4 GB (Dedicated CPU)

👉 Suivez la documentation afin d’installer Prometheus sur la VM Linode (jusqu’à la fin de l’étape "Starting Prometheus") : https://prometheus.io/docs/introduction/first_steps/


👉 Si ce n’est pas déjà fait, démarrez Prometheus grâce à la commande suivante.


`./prometheus --config.file=prometheus.yml`


Prometheus n’étant pas lancé en tâche de fond, vous devez laisser le terminal actif ouvert (ou bien trouver une méthode permettant d’éviter cela 😉).
N’hésitez pas à ouvrir une seconde connexion via SSH pour la suite du challenge.

#### UTILISATION DE PROMETHEUS

👉 Visitez l’interface web exposée par Prometheus via votre navigateur.

Pour ma part je suis passé par docker pour garder une alternative gratuite.

```
  prometheus:
    container_name: prometheus
    image: prom/prometheus
    volumes:
      - ./prometheus-config/prometheus.yml:/etc/prometheus/prometheus.yml
    ports:
      - 9090:9090
    networks:
      - prom
    restart: unless-stopped
```