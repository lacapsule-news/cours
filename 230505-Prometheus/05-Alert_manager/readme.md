# Alert manager

## CRÉATION D’ALERTES

Avant de passer à la visualisation des metrics via un outil tel que Grafana, Prometheus vous réserve d’autres surprises, notamment via le principe d’Alerting où vous pouvez configurer des règles pour surveiller certaines metrics ainsi que des notifications par mail ou messagerie instantanée.

👉 En vous inspirant de la documentation de Prometheus, mettez en place l’Alertmanager sur votre serveur Linode.

👉 Créez les deux règles d’alertes suivantes :

Si le service Nginx est indisponible pendant plus d’une minute
Si le disque dur du serveur est rempli à plus de 80%

## BONUS

👉 Mettez en place un envoi de mail automatique pour les deux règles d’alertes créées précédemment. Vous aurez certainement besoin d’une adresse Gmail pour faciliter la configuration de l’envoi de mail.

👉 Testez votre alerte en stoppant le conteneur Nginx pendant plus d’une minute .

👉 Ne supprimez pas la VM Linode, elle sera utilisée pour demain également 😉

```
  prometheus:
    container_name: prometheus
    image: prom/prometheus
    volumes:
      - ./prometheus-config/prometheus.yml:/etc/prometheus/prometheus.yml
      - ./prometheus-config/test-alert.yml:/etc/prometheus/test-alert.yml
    ports:
      - 9090:9090
    networks:
      - prom
    restart: unless-stopped
  
  alertmanager:
    container_name: alertmanager
    image: prom/alertmanager
    volumes:
      - ./alertmanager/:/etc/alertmanager/
    environment:
      - DOCKER_API_KEY=${ALERT_DISCORD_API}
    command:
      - '--config.file=/etc/alertmanager/config.yml'
      - '--storage.path=/alertmanager'
    networks:
      - prom
    restart: unless-stopped
```

```
groups:
 - name: Monitoring Loopback
   rules:
   - alert: Instance Down
     expr: up == 0
   - alert: More than 80% of cpu usage
     expr: 100 - (avg by (instance) (irate(node_cpu_seconds_total{mode="idle"}[5m])) * 100) >80
   - alert: More than 80% of ram usage
     expr: 100 - (node_memory_Active_bytes * 100 / node_memory_MemTotal_bytes) >80

 - name: Monitoring Web
   rules:
   - alert: Nginx Down
     expr: nginx_up == 0
   - alert: More than 100 connexion
     expr: nginx_connections_active > 100

 - name: Monitoring Disk Space
   rules:
   - alert: Up than 80% disk space
     expr: 100 - (node_filesystem_avail_bytes{mountpoint="/"} * 100 / node_filesystem_size_bytes{mountpoint="/"}) > 80
```

```
route:
  group_by: ['alertname', 'job']

  group_wait: 30s
  group_interval: 5m
  repeat_interval: 3h

  receiver: discord

receivers:
- name: discord
  discord_configs:
  - webhook_url: <Discord_web_hook>
```