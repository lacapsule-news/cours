# Start with Prometheus

## SETUP

👉 Sur Linode, créez une VM vierge nommée "test-prometheus" qui sera votre terrain de jeu pour découvrir la notion de monitoring :


- Debian 11
- Dedicated 4 GB (Dedicated CPU)

👉 Suivez la documentation afin d’installer Prometheus sur la VM Linode (jusqu’à la fin de l’étape "Starting Prometheus") : https://prometheus.io/docs/introduction/first_steps/


👉 Si ce n’est pas déjà fait, démarrez Prometheus grâce à la commande suivante.


`./prometheus --config.file=prometheus.yml`


Prometheus n’étant pas lancé en tâche de fond, vous devez laisser le terminal actif ouvert (ou bien trouver une méthode permettant d’éviter cela 😉).
N’hésitez pas à ouvrir une seconde connexion via SSH pour la suite du challenge.

## UTILISATION DE PROMETHEUS

👉 Visitez l’interface web exposée par Prometheus via votre navigateur.

Pour ma part je suis passé par docker pour garder une alternative gratuite.

```
  prometheus:
    container_name: prometheus
    image: prom/prometheus
    volumes:
      - ./prometheus-config/prometheus.yml:/etc/prometheus/prometheus.yml
    ports:
      - 9090:9090
    networks:
      - prom
    restart: unless-stopped
```