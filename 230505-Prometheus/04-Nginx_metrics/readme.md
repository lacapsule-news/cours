# Nginx metrics

## NGINX NODE EXPORTER

Maintenant que vous êtes capable de récolter les metrics du système hôte, il est temps d’aller plus loin en configurant un autre "exporter" capable de communiquer avec un web serveur et plus précisément Nginx et Nginx Prometheus Exporter.

👉 Pour commencer, installez Docker sur la VM Linode.

👉 Vérifiez l’installation de Docker en récupérant sa version via l’option "--version".

Pour simplifier l’installation du serveur web et l’export des metrics, vous allez utiliser Docker afin de créer des conteneurs qui seront capables de communiquer entre eux et avec Prometheus.

👉 Via la commande docker run, créez et démarrez un conteneur Docker basé sur l’image officielle de Nginx en exposant le port 80 sur le port 8080 de la machine hôte.

👉 Vérifiez que le serveur web a bien été démarré en visitant l’IP publique du serveur, sur le port 8080.

Comme d’habitude, si vous voyez le traditionnel message de bienvenue de Nginx, c’est que tout est opérationnel pour la suite !

👉 Trouvez un moyen d’activer la page de status de Nginx sur l’URL /stub_status en modifiant le fichier "/etc/nginx/conf.d/default.conf" sur le conteneur Nginx.

👉 Vérifiez que le serveur web expose bien l’URL "/stub_status" en visitant l’IP publique du serveur, sur le port 8080. Vous êtes censés obtenir des informations semblables à la réponse ci-dessous.

👉 En vous inspirant de la documentation de Nginx Prometheus Exporter, démarrez un conteneur basé sur l’image "nginx/nginx-prometheus-exporter" en exposant le port 9113 et en configurant l’exporter afin qu’il puisse communiquer avec l’URL "/stub_status" du serveur web Nginx.

👉 Visitez l’URL "/metrics" (sur le port exposé par Nginx Prometheus Exporter) afin de découvrir toutes les informations récoltées sur le serveur web Nginx.

👉 Assurez-vous que votre instance Prometheus soit configurée afin d’accéder aux metrics de Nginx Prometheus Exporter.

👉 À partir de l’interface graphique de Prometheus, trouvez les requêtes permettant de vous donner les informations suivantes :

Le statut du serveur Nginx (1 pour true, 0 pour false)
Le nombre total de requêtes HTTP reçues sur le serveur web
Le nombre de personnes connectées en temps réel sur le site (n’hésitez pas à ouvrir le site en navigation privée et sur plusieurs navigateurs pour tester)

👉 Ne supprimez pas la VM Linode, elle sera utilisée pour le prochain challenge.

```
  web-server:
    container_name: nginx
    image: nginx:stable
    volumes:
      - ./nginx.conf:/etc/nginx/nginx.conf
    ports:
      - 80:80
    networks:
      - nginx-metrics
    restart: unless-stopped

  web-metrics:
    container_name: nginx-exporter
    image: nginx/nginx-prometheus-exporter:0.10.0
    command:
      - -nginx.scrape-uri
      - http://nginx:8080/status
    networks:
      - prom
      - nginx-metrics
    depends_on:
      - web-server
    restart: unless-stopped
```
```

user  nginx;
worker_processes  auto;

error_log  /var/log/nginx/error.log notice;
pid        /var/run/nginx.pid;


events {
    worker_connections  1024;
}


http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile        on;
    #tcp_nopush     on;

    keepalive_timeout  65;

    #gzip  on;
    server{
        listen      8080;
        server_name nginx;

        location /status {
            stub_status;
        }
    }
    server {
        listen       80;
        listen  [::]:80;
        server_name  localhost;

        #access_log  /var/log/nginx/host.access.log  main;

        location / {
            root   /usr/share/nginx/html;
            index  index.html index.htm;
        }

        #error_page  404              /404.html;

        # redirect server error pages to the static page /50x.html
        #
        error_page   500 502 503 504  /50x.html;
        location = /50x.html {
            root   /usr/share/nginx/html;
        }

        # proxy the PHP scripts to Apache listening on 127.0.0.1:80
        #
        #location ~ \.php$ {
        #    proxy_pass   http://127.0.0.1;
        #}

        # pass the PHP scripts to FastCGI server listening on 127.0.0.1:9000
        #
        #location ~ \.php$ {
        #    root           html;
        #    fastcgi_pass   127.0.0.1:9000;
        #    fastcgi_index  index.php;
        #    fastcgi_param  SCRIPT_FILENAME  /scripts$fastcgi_script_name;
        #    include        fastcgi_params;
        #}

        # deny access to .htaccess files, if Apache's document root
        # concurs with nginx's one
        #
        #location ~ /\.ht {
        #    deny  all;
        #}
    }

}
```