# Prometheus

## Contexte

Administrer un système d'information nécessite une analyse des métriques système en temps réel.

Chaque entité du sytème d'information expose ses métriques dans son propre format.

La récolte et l'analyse en temps réel de ses métriques devient fastidieuse voire impossible.

Prometheus premet de collecter les métriques d'une multitude de sources en temps réel.

### Fonctionnement

Il est nécessaire d'installer Prometheus sur une machine qui sera dédiée à la collecte des métriques.

L'exporter est un noeud qui permet de collecter les métriques d'une seule source.

Prometheus propose une visualisation basique métriques, mais ce n'est pas sont rôle principal.

La visualisation sous forme de dashboard devra se faire avec une solution spécialisée.

Il est possible de mettre en place des seuils d'alertes qui notifiera l'équipe dédiée pour intervenir.

[Nginx monitoring stack](https://gitea.sunburst.ml/sunburst/Nginx_monitoring_stack)