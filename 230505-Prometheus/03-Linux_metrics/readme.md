# Linux metrics

## LINUX NODE EXPORTER

Prometheus est maintenant configuré, mais il n’est capable de récolter que des metrics le concernant, ce qui n’est pas des plus excitant, on peut se l’avouer !

Evidemment, Prometheus est capable d’aller plus loin et de récolter des metrics de la machine Linux (espace disque disponible, trafic réseau, etc.) grâce au Node Exporter.

👉 En vous inspirant de la documentation de Prometheus, installez et démarrez le Node Exporter.

👉 Assurez-vous que votre instance Prometheus soit configurée afin d’accéder aux metrics de Node Exporter.
Le fichier prometheus.yml décrit les différentes sources et la fréquence à laquelle Prometheus va scraper (aspirer) les metrics.

👉 Redémarrez votre instance Prometheus et visitez l’URL "/metrics" (sur le port exposé par Node Exporter) afin de découvrir toutes les informations récoltées sur le système.

👉 À partir de l’interface graphique de Prometheus, trouvez les requêtes permettant de vous donner les informations suivantes :

- La quantité totale de ram sur le serveur (en bytes)
- Le nombre de coeurs sur le CPU (vérifiez ce nombre grâce à l’interface Linode)
- L’espace disque disponible sur le système (en bytes)
- La moyenne du trafic réseau transmis sur l’interface "eth0" (en bytes), par seconde et sur les 5 dernières minutes

👉 Ne supprimez pas la VM Linode, elle sera utilisée pour les prochains challenges.

Pour ma part je suis rester sur docker.

```
  node_exporter:
    image: quay.io/prometheus/node-exporter:latest
    container_name: node_exporter
    command:
      - '--path.rootfs=/host'
    networks:
      - prom
    pid: host
    restart: unless-stopped
    volumes:
      - '/:/host:ro,rslave'
    depends_on:
      - web-server
```