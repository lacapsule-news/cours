# Les noms de domaine

## Contexte

Chaque machine d'un réseau est joignable par un identifiant unique: l'adresse IP.

Dans le contexte d'un réseau local, cette adresse IP locale est généralement attribuée par le routeur.

Une machine peut être exposée sur internet afin d'être joignable par d'autres machines qui sont à l'extérieur du réseau local, toujours via une adresse IP.

Dans le contexte, cette adresse IP publique est généralement attribuée par un fournisseur d'accés internet ou un cloud provider.

Une adresse IP reste compliquée à retenir et n'est pas du tout efficace pour représenter l'identité d'un service sur internet.

Les noms de domaine permettent de créer un alias vers une adresse IP, généralement pour rediriger vers un site web.

### Fonctionnement

Pour rappel, un serveur DNS est un sorte d'annuaire qui traduit les noms de domaines en adresses IP.

Un registar est un entreprise qui enregistre des noms de domaines pour les rendre disponibles pour l'utilisation sur internet.

Une zone DNS permet de configurer comment les noms de domaines sont associés à des adresses IP.

## Exo

### 05 - Nginx proxy manager

#### SETUP

Comme vous avez pu le constater pendant le challenge précédent, la configuration de Let’s Encrypt n’est pas vraiment compliquée.

Elle peut néanmoins se révéler fastidieuse si vous souhaitez gérer facilement les domaines et sous-domaines pour plusieurs applications (ou conteneurs) hébergés sur la même machine.

👉 Louez un simple serveur Linux basé sur Debian auprès d’AWS ou de Linode

👉 Une fois le serveur prêt, installez Docker et Docker Compose

👉 Créez deux services via Docker Compose qui auront pour seul objectif de démarrer chacun un conteneur basé sur l’image officielle de nginx.

Vous n’aurez pas besoin d’exposer de port vers la machine hôte pour l’instant.

👉 Configurez votre DNS afin de créer deux sous-domaines de votre choix pointant vers le serveur précédemment configuré.

Les ports des deux services n’étant pas exposés, ces deux sous-domaines ne seront pas encore fonctionnels.

#### CONFIGURATION DE NGINX PROXY MANAGER

Maintenant que vos deux "applications" sont déployées, il est temps de se confronter à un nouvel outil qui, une fois bien maîtrisé, se révèle indispensable dans la gestion de la résolution de ses noms de domaine, notamment vers des conteneurs Docker.

👉 En vous basant sur la documentation de Nginx Proxy Manager, installez et configurez l’interface afin que chaque sous-domaine puisse être redirigé vers un des services, le tout en HTTPS.

Vous n’aurez pas besoin de configurer individuellement chaque conteneur hébergeant Nginx puisque l’image de Nginx Proxy Manager jouera le rôle de "proxy" afin de rediriger les requêtes entrantes vers le bon service.

### 04 - Let's encrypt

#### SETUP

👉 Louez un simple serveur Linux basé sur Debian auprès d’AWS ou de Linode.

👉 Une fois le serveur prêt, installez le reverse proxy Nginx

👉 Personnalisez la page par défaut de Nginx afin d’y insérer le message de votre choix.

👉 Configurez votre zone DNS afin que le sous domaine "nginx" pointe vers le serveur précédemment configuré.

👉 Essayez de visiter le site en HTTPS.

Vous obtenez une erreur ? C’est tout à fait normal, par défaut Nginx n’est pas configuré pour passer par HTTPS. Pour cela vous avez besoin d’un certificat SSL délivré par une autorité reconnue.

#### OBTENTION D’UN CERTIFICAT SSL

Il n’est aujourd’hui pas obligatoire de payer pour obtenir un certificat SSL et donc de sécuriser la communication entre des clients et un serveur via HTTPS.

👉 En utilisant Let’s Encrypt et en configurant Nginx, faîtes en sorte que votre site soit accessible en HTTPS, sans aucune erreur du navigateur.

Vous trouverez très certainement des tas de tutoriels en ligne afin d’installer et utiliser Let’s Encrypt ainsi que pour configurer Nginx.

👉 Une fois le challenge terminé, vous pouvez supprimer le serveur loué auprès de votre cloud provider en prenant soin de garder une copie locale des fichiers de configuration créés sur le serveur.

### 03 - Configure my dns

#### DÉPLOIEMENT D’UNE APPLICATION

L’un des usages les plus fréquents des noms de domaine est la dénomination d’un site web ou même d’une entreprise sur internet : google.fr, lacapsule.academy, etc.

Ce challenge va vous permettre d’expérimenter un cas concret : le déploiement d’une application avec un nom de domaine personnalisé.

👉 Reprenez le challenge "Deploy to Vercel" du début de la semaine 5 de formation et déployez de nouveau l’application sur Vercel si ce n’est pas déjà fait.

👉 Assurez-vous que l’application est bien accessible via le nom de domaine attribué par Vercel.

#### CONFIGURATION DE LA ZONE DNS

Maintenant que vous êtes un peu plus à l’aise sur la configuration de zone DNS, vous pouvez aller un peu plus loin que la création de redirection !

👉 Configurez Vercel ainsi que votre zone DNS pour que le sous-domaine "pokedex" pointe vers l’application hébergée précédemment.

👉 Vérifiez la propagation du nom de domaine (mise à jour auprès des différents serveurs DNS) grâce à la commande dig.

### 02 - MY DOMAIN NAME

#### ACHAT D’UN NOM DE DOMAINE

Il existe de nombreux sites permettant d’acheter facilement un nom de domaine, on les appelle des registrars : OVH, Hostinger, GoDaddy, Namecheap, etc.
Ils ne se différencient que par les tarifs pratiqués ainsi que l’expérience utilisateur du panel permettant de gérer ses noms de domaine.

Pour cette journée dédiée à l’utilisation du nom de domaine, nous vous conseillons de passer par OVH, leader européen du cloud.

👉 Commencez par choisir un nom de domaine disponible ainsi que l’extension de votre choix en faisant attention à la potentielle différence de prix entre la première année et le renouvellement.

👉 Procédez à l’achat du nom de domaine en renseignant toutes les informations nécessaires. Si vous passez par OVH, vous devrez vous créer un compte.

⚠️ Veillez à ne pas souscrire à d’autres options payantes du type "DNS Anycast" ou un hébergement statique.

#### CONFIGURATION DE LA ZONE DNS

La zone DNS (Domain Name System) correspond à l’espace de gestion et de configuration d’un nom de domaine tel que "lacapsule.academy" ou d’un sous-domaine tel que "ariane.lacapsule.academy".

👉 Une fois votre nom de domaine acheté, rendez-vous sur la page de gestion de la zone DNS.

Sur OVH, cette partie se trouve dans le menu "Web Cloud" de votre panel puis sélectionnez votre nom de domaine dans menu éponyme sur la gauche.

La configuration par défaut de votre zone DNS devrait ressembler à ceci :

👉 Trouvez le type d’enregistrement permettant de créer une redirection vers un autre nom de domaine cible.

👉 Modifiez votre zone DNS afin que votre nom de domaine root (sans sous-domaine ou "www") redirige vers "google.com". Quelques minutes après la modification, vérifiez que la direction fonctionne.

👉 Modifiez votre zone DNS afin que le sous domaine "images" redirige vers "images.google.com". Quelques minutes après la modification, vérifiez que la direction fonctionne.