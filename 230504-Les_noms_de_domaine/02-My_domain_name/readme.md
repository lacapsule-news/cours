# MY DOMAIN NAME

## ACHAT D’UN NOM DE DOMAINE

Il existe de nombreux sites permettant d’acheter facilement un nom de domaine, on les appelle des registrars : OVH, Hostinger, GoDaddy, Namecheap, etc.
Ils ne se différencient que par les tarifs pratiqués ainsi que l’expérience utilisateur du panel permettant de gérer ses noms de domaine.

Pour cette journée dédiée à l’utilisation du nom de domaine, nous vous conseillons de passer par OVH, leader européen du cloud.

👉 Commencez par choisir un nom de domaine disponible ainsi que l’extension de votre choix en faisant attention à la potentielle différence de prix entre la première année et le renouvellement.

👉 Procédez à l’achat du nom de domaine en renseignant toutes les informations nécessaires. Si vous passez par OVH, vous devrez vous créer un compte.

⚠️ Veillez à ne pas souscrire à d’autres options payantes du type "DNS Anycast" ou un hébergement statique.

## CONFIGURATION DE LA ZONE DNS

La zone DNS (Domain Name System) correspond à l’espace de gestion et de configuration d’un nom de domaine tel que "lacapsule.academy" ou d’un sous-domaine tel que "ariane.lacapsule.academy".

👉 Une fois votre nom de domaine acheté, rendez-vous sur la page de gestion de la zone DNS.

Sur OVH, cette partie se trouve dans le menu "Web Cloud" de votre panel puis sélectionnez votre nom de domaine dans menu éponyme sur la gauche.

La configuration par défaut de votre zone DNS devrait ressembler à ceci :

👉 Trouvez le type d’enregistrement permettant de créer une redirection vers un autre nom de domaine cible.

👉 Modifiez votre zone DNS afin que votre nom de domaine root (sans sous-domaine ou "www") redirige vers "google.com". Quelques minutes après la modification, vérifiez que la direction fonctionne.

👉 Modifiez votre zone DNS afin que le sous domaine "images" redirige vers "images.google.com". Quelques minutes après la modification, vérifiez que la direction fonctionne.