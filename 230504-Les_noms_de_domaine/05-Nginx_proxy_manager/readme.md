# Nginx proxy manager

## SETUP

Comme vous avez pu le constater pendant le challenge précédent, la configuration de Let’s Encrypt n’est pas vraiment compliquée.

Elle peut néanmoins se révéler fastidieuse si vous souhaitez gérer facilement les domaines et sous-domaines pour plusieurs applications (ou conteneurs) hébergés sur la même machine.

👉 Louez un simple serveur Linux basé sur Debian auprès d’AWS ou de Linode

👉 Une fois le serveur prêt, installez Docker et Docker Compose

👉 Créez deux services via Docker Compose qui auront pour seul objectif de démarrer chacun un conteneur basé sur l’image officielle de nginx.

Vous n’aurez pas besoin d’exposer de port vers la machine hôte pour l’instant.

👉 Configurez votre DNS afin de créer deux sous-domaines de votre choix pointant vers le serveur précédemment configuré.

Les ports des deux services n’étant pas exposés, ces deux sous-domaines ne seront pas encore fonctionnels.

## CONFIGURATION DE NGINX PROXY MANAGER

Maintenant que vos deux "applications" sont déployées, il est temps de se confronter à un nouvel outil qui, une fois bien maîtrisé, se révèle indispensable dans la gestion de la résolution de ses noms de domaine, notamment vers des conteneurs Docker.

👉 En vous basant sur la documentation de Nginx Proxy Manager, installez et configurez l’interface afin que chaque sous-domaine puisse être redirigé vers un des services, le tout en HTTPS.

Vous n’aurez pas besoin de configurer individuellement chaque conteneur hébergeant Nginx puisque l’image de Nginx Proxy Manager jouera le rôle de "proxy" afin de rediriger les requêtes entrantes vers le bon service.