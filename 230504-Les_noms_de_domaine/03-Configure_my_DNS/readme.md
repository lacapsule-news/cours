# Configure my dns

## DÉPLOIEMENT D’UNE APPLICATION

L’un des usages les plus fréquents des noms de domaine est la dénomination d’un site web ou même d’une entreprise sur internet : google.fr, lacapsule.academy, etc.

Ce challenge va vous permettre d’expérimenter un cas concret : le déploiement d’une application avec un nom de domaine personnalisé.

👉 Reprenez le challenge "Deploy to Vercel" du début de la semaine 5 de formation et déployez de nouveau l’application sur Vercel si ce n’est pas déjà fait.

👉 Assurez-vous que l’application est bien accessible via le nom de domaine attribué par Vercel.

## CONFIGURATION DE LA ZONE DNS

Maintenant que vous êtes un peu plus à l’aise sur la configuration de zone DNS, vous pouvez aller un peu plus loin que la création de redirection !

👉 Configurez Vercel ainsi que votre zone DNS pour que le sous-domaine "pokedex" pointe vers l’application hébergée précédemment.

👉 Vérifiez la propagation du nom de domaine (mise à jour auprès des différents serveurs DNS) grâce à la commande dig.