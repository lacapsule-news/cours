# Les noms de domaine

## Contexte

Chaque machine d'un réseau est joignable par un identifiant unique: l'adresse IP.

Dans le contexte d'un réseau local, cette adresse IP locale est généralement attribuée par le routeur.

Une machine peut être exposée sur internet afin d'être joignable par d'autres machines qui sont à l'extérieur du réseau local, toujours via une adresse IP.

Dans le contexte, cette adresse IP publique est généralement attribuée par un fournisseur d'accés internet ou un cloud provider.

Une adresse IP reste compliquée à retenir et n'est pas du tout efficace pour représenter l'identité d'un service sur internet.

Les noms de domaine permettent de créer un alias vers une adresse IP, généralement pour rediriger vers un site web.

### Fonctionnement

Pour rappel, un serveur DNS est un sorte d'annuaire qui traduit les noms de domaines en adresses IP.

Un registar est un entreprise qui enregistre des noms de domaines pour les rendre disponibles pour l'utilisation sur internet.

Une zone DNS permet de configurer comment les noms de domaines sont associés à des adresses IP.