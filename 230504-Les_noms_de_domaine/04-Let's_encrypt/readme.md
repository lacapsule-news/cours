# Let's encrypt

## SETUP

👉 Louez un simple serveur Linux basé sur Debian auprès d’AWS ou de Linode.

👉 Une fois le serveur prêt, installez le reverse proxy Nginx

👉 Personnalisez la page par défaut de Nginx afin d’y insérer le message de votre choix.

👉 Configurez votre zone DNS afin que le sous domaine "nginx" pointe vers le serveur précédemment configuré.

👉 Essayez de visiter le site en HTTPS.

Vous obtenez une erreur ? C’est tout à fait normal, par défaut Nginx n’est pas configuré pour passer par HTTPS. Pour cela vous avez besoin d’un certificat SSL délivré par une autorité reconnue.

## OBTENTION D’UN CERTIFICAT SSL

Il n’est aujourd’hui pas obligatoire de payer pour obtenir un certificat SSL et donc de sécuriser la communication entre des clients et un serveur via HTTPS.

👉 En utilisant Let’s Encrypt et en configurant Nginx, faîtes en sorte que votre site soit accessible en HTTPS, sans aucune erreur du navigateur.

Vous trouverez très certainement des tas de tutoriels en ligne afin d’installer et utiliser Let’s Encrypt ainsi que pour configurer Nginx.

👉 Une fois le challenge terminé, vous pouvez supprimer le serveur loué auprès de votre cloud provider en prenant soin de garder une copie locale des fichiers de configuration créés sur le serveur.