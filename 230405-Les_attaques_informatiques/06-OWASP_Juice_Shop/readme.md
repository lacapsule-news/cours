# OWASP

## ATTAQUE D’UN SITE WEB

La difficulté monte d’un cran (et peut être même deux…) puisque vous allez vous confronter à une room bien connue dans la communauté des pentesteurs :un environnement web truffé de vulnérabilités et créé par la fondation OWASP.


- 👉 Attaquez-vous à l'application web vulnérable Juice Shop afin d’apprendre à identifier et à exploiter les vulnérabilités courantes des applications web : https://tryhackme.com/room/owaspjuiceshop


- Voici quelques aides et indications qui vous seront certainement utiles pour venir à bout de cette room :
	- Vous serez amené à mener une attaque bruteforce afin de trouver un mot de passe, nous vous conseillons d’utiliser ce "dictionnaire" : https://raw.githubusercontent.com/danielmiessler/SecLists/master/Passwords/Common-Credentials/best1050.txt 
	- Sur Burp Suite, l’attaque bruteforce par dictionnaire est limitée et peut prendre jusqu’à 10 minutes afin de trouver le bon mot de passe
	- Pour la complétion des tasks 6 et 7, vous devez retourner sur un navigateur hors Burp Suite afin de récupérer les flags (ou aller directement sur la plage leaderboard si ça ne s’affiche toujours pas)

Type d'injection | Explication
------|------
SQL Injection | SQL Injection is when an attacker enters a malicious or malformed query to either retrieve or tamper data from a database. And in some cases, log into accounts.
Command Injection | Command Injection is when web applications take input or user-controlled data and run them as system commands. An attacker may tamper with this data to execute their own system commands. This can be seen in applications that perform misconfigured ping tests. 
Email Injection | Email injection is a security vulnerability that allows malicious users to send email messages without prior authorization by the email server. These occur when the attacker adds extra data to fields, which are not interpreted by the server correctly. 