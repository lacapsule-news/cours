# Les attaques informatiques

## Contexte

> La sécurité informatique est un vaste sujet, car tous les perimetre sont impactés :

- Les réseaux
- Les machines Linux & Windows
- Les logiciels et les services
- Même les humains !

Il est quasiment impossible de trouver une personne qui maîtrise parfaitements l'ensemble d'un domaine de la sécurité informatique.

Il existe plutôt des spécialistes pour chaque périmètre.

Il est nécessaire pour un dev/ops de connaitre la philosophie et l'approche des hackers afins de se protéger de la plupart des attaques.

Pour préparer une attaque, on doit récolter et analyser les in informations de la machine ciblée :

- Adresse IP
- Ports ouverts
- Nom et version du système d'exploitation
- Nom et version des services exposés

Les système d'informations parlent trop !

Par exemple lorsque l'on fait une requête à un server web, celui-ci peut répondre en donnant le nom du serveur et sa version.

De nombreux outils existent et permettent très rapidement d'extraire un maximum d'information sur une cible : ping, nmap, traceroute, ...

Il existe un dictionnaire qui liste toutes les vulnérabilités connues d'un service ou d'un logiciel.

Il s'agit de la liste CVE pour "Common Vulnerabilities and Exposures".

Une fois que la reconnaissance est terminée et qu'un faille a été identifiée, l'attauqe peut être élaborée, mais seulement dans un cadre légal!