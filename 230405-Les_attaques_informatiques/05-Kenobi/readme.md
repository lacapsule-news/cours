# KENOBI

## ATTAQUE D’UNE MACHINE LINUX

Place à l'exploitation d'une machine Linux ! Vous allez vous attaquer à un serveur de stockage de fichier doté d’une service qui n’est plus à jour et plus précisément doté d’une version comportant une faille critique.


👉 Suivez les instructions de la room Kenobi afin d'accéder à l’utilisateur root via une élévation des privilèges : https://tryhackme.com/room/kenobi

- Active recon 

`nmap -p-2500 $ip`

- Enuméré les partage fichier
    - nmap est aussi capable d'éxecuter des script afin de faire de la reconnaisance encore plus active voir de l'attaque

`nmap -p 445 --script=smb-enum-shares.nse,smb-enum-users.nse $ip`

- Recherche de vulérabilité

```
root@ip-10-10-164-110:~# nmap -p-1000 -sV 10.10.75.47

Starting Nmap 7.60 ( https://nmap.org ) at 2023-04-05 14:31 BST
Nmap scan report for ip-10-10-75-47.eu-west-1.compute.internal (10.10.75.47)
Host is up (0.0013s latency).
Not shown: 994 closed ports
PORT    STATE SERVICE     VERSION
21/tcp  open  ftp         ProFTPD 1.3.5
22/tcp  open  ssh         OpenSSH 7.2p2 Ubuntu 4ubuntu2.7 (Ubuntu Linux; protocol 2.0)
80/tcp  open  http        Apache httpd 2.4.18 ((Ubuntu))
111/tcp open  rpcbind     2-4 (RPC #100000)
139/tcp open  netbios-ssn Samba smbd 3.X - 4.X (workgroup: WORKGROUP)
445/tcp open  netbios-ssn Samba smbd 3.X - 4.X (workgroup: WORKGROUP)
MAC Address: 02:D4:A0:BE:42:13 (Unknown)
```

```
root@ip-10-10-164-110:~# searchsploit ProFTPD 1.3.5
[i] Found (#2): /opt/searchsploit/files_exploits.csv
[i] To remove this message, please edit "/opt/searchsploit/.searchsploit_rc" for "files_exploits.csv" (package_array: exploitdb)

[i] Found (#2): /opt/searchsploit/files_shellcodes.csv
[i] To remove this message, please edit "/opt/searchsploit/.searchsploit_rc" for "files_shellcodes.csv" (package_array: exploitdb)

---------------------------------------------------------------------- ---------------------------------
 Exploit Title                                                        |  Path
---------------------------------------------------------------------- ---------------------------------
ProFTPd 1.3.5 - 'mod_copy' Command Execution (Metasploit)             | linux/remote/37262.rb
ProFTPd 1.3.5 - 'mod_copy' Remote Command Execution                   | linux/remote/36803.py
ProFTPd 1.3.5 - File Copy                                             | linux/remote/36742.txt
---------------------------------------------------------------------- ---------------------------------
Shellcodes: No Results
```

- Elevation de privilegre grace au SUID

- Sur linux il est possible grace au suid de laisser une certaine personne executer une commande qui necessite un privilege administrateur

Permission | On Files | On Directories
SUID Bit | User executes the file with permissions of the file owner | -
SGID Bit | User executes the file with the permission of the group owner. | File created in directory gets the same group owner.
Sticky Bit | No meaning | Users are prevented from deleting files from other users.

- Mappé tout les fichier 'find / perm /4000'