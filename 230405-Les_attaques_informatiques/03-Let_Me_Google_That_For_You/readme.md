# Let Me Google That For You

## L’IMPORTANCE DE GOOGLE

Au cas où vous ne l’avez pas remarqué depuis le début de votre formation, l’utilisation du moteur de recherche Google est primordiale dans votre apprentissage : recherche de commandes, d’erreurs, de documentation… c’est une étape incontournable pour tout bon DevOps, mais aussi dans le domaine de la sécurité informatique !


👉 Commencez par une brève introduction aux compétences de recherche pour le pentesting (test d’intrustion) : https://tryhackme.com/room/introtoresearch 

- Les base de la Steganography
    - Il es possible de cacher des information a l'intérieur d'image(ou d'autre media)
    - Découverte de l'outils steghide
        - Comment l'installer (apt install steghide)
        - Comment l'utiliser (man steghide)
- Les base de la recherche de vulnérabilité
    - [Exploit db](https://www.exploit-db.com/)
        - Moteur de recherche pour les exploit connue 
    - [NVD](https://nvd.nist.gov/vuln/search)
        - Moteur de recherche pour les vulnérabilités connue
    - [CVE Mitre](https://cve.mitre.org/)
        - Base de connaisance SOC mitre(si j'ai bien compris)
- Les manuel linux(vue dans les anciens cours 'man prg')

👉 Découvrez le fonctionnement avancé des moteurs de recherche afin de trouver du contenu caché : https://tryhackme.com/room/googledorking

### Fonctionement des moteur de recherche

#### Les crawlers

Il vont récolté des informations disponible publiquement sur internet de diverse manière.

Il vont ensuite essayer d'extraire un maximum d'information sous forme de mots clé ainsi que suivre tout les lien présents.

Pour aider les crawlers a référencé leur site les administrateur peut mettre en place un robot.txt et un sitemap.xml.

Les Crawlers fond une sorte de photo d'internet afin de le rendre disponible pour tous sous forme de mots clé

#### Les moteurs de recherche

Les moteurs de recherche vont faire le lien entre l'humain et cette base de données

Dans les moteurs de recherche comme goole l'objectifs et de renvoyer un bout du contenue de la page contenant les mots clées ainsi qu'un lien hypertext afin de pouvoir naviguez de manière aisé sur internet.

Afin d'être mieux référecé par ces moteurs de recherche, il es possible d'indiqué les acces au crawlers via le robot.txt et l'arborésence du site dans sitemap.xml

```robot.txt
User-agent: Googlebot
Disallow: /
Disallow: /*.conf$
```

```sitemap.xml
<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
  <url>
    <loc>http://www.example.com/page.html</loc>
  </url>
</urlset>

```

### Google dorking

Afin de ciblé la recherche google nous permet de passer de parametre dans la requete.

Term | Action
------|------
filetype: | Search for a file by its extension (e.g. PDF)
cache: | View Google's Cached version of a specified URL
intitle: | The specified phrase MUST appear in the title of the page
site: | permet de cibler une url