# TRYHACKME INTRODUCTION

## LA PLATEFORME TRYHACKME

Pour cette journée sous le thème de la sécurité et des attaques informatiques, vous aurez l’occasion de vous entraîner la plateforme TryHackMe qui est dédiée à l'apprentissage et à la pratique de la cybersécurité, via laboratoires pratiques et proches de la réalité.


Certains exercices de la plateforme seront sous forme de quiz, tandis que d’autres (plus complets et stimulants) nécessitent la manipulation d’un environnement distant auquel il faudra se connecter via un VPN.


- 👉 Créez un compte sur la plateforme TryHackMe.

- 👉 Commencez par la découverte d’OpenVPN, un client VPN qui vous permettra de vous connecter au réseau de TryHackMe afin de manipuler certains environnements distants : https://tryhackme.com/room/openvpn
    - Naviguez jusqu'a https://tryhackme.com/access
    - Téléchargé l'archive openvpn sur votre ordinateur
    - Importer la dans openvpn (apt install openvpn)
    - Verifier a l'aide des exo suivant si la connexion est fonctionnel

- 👉 Maintenant que vous êtes aptes à vous connecter au réseau de TryHackMe, apprenez à utiliser la plateforme à travers la room suivante : https://tryhackme.com/room/tutorial
    - Verifié que votre connexion openvpn soit bien active (ip a)
    - Rendez-vous sur le l'ip de la machine a l'aide de votre navigateur