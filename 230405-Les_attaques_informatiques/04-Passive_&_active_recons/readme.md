# PASSIVE & ACTIVE RECONS

## RECONNAISSANCE PASSIVE & ACTIVE

Vous brûlez d’envie d’attaquer votre première machine n’est-ce pas ? Encore un peu de patience !
La compréhension des attaques informatiques passe par la maîtrise des concepts de base : vous avez précédemment vu l’importance de la recherche, vous allez maintenant découvrir la première étape dédiée à la reconnaissance et la collecte d’informations.


👉 Découvrez les outils essentiels à la reconnaissance passive tels que whois, nslookup et dig : https://tryhackme.com/room/passiverecon

- Reconaissance grace au registre dns
    - Les commande nslookup, dig, whois sont utilisé pour faire de la reconaissance de dns
    - Il existe aussi des service web comme dnsdumpster qui nous permettent de modélisé les infomation de ses serveur.
- Shodan.io
    - Ce meteur de recherche permet de rechercher n'importe quelle objet vulnérable sur internet

👉 Passez à la reconnaissance active via des outils tels que traceroute, ping, telnet afin de recueillir des informations : https://tryhackme.com/room/activerecon

Cette room nécessite la connexion au réseau de TryHackMe via OpenVPN.

- Notre navigateur est très puissant (active recon)
    - Il embarque des outils de dévelopeur intégré qui peuvent nous données des informations
    - Il est aussi possible de rajouter de plugins afin d'avoir plus d'information ou dans une autre forme
- Fonctionalité active de détéction
    - Les commande ping font un teste de communication avec la machine distant
    - Traceroute indique a chaque routeur intérmediaire de données une réponse
    - Telnet anciennement utilisé pour établir des connexion a distance
    - Netcat, le couteau swiss réseau pour les connexion, les transfert ect
        - Tout les outils abordé ici sont sujet a changer avec le temps
        - Il est aussi possible de développer les sien