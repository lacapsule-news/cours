# Undoing things

## ANNULER UN COMMIT EN LOCAL

Dans ce challenge, vous allez aborder les stratégies et commandes d’annulation et de retour en arrière proposées par Git. Notez que Git ne propose pas de retour en arrière comme une application de traitement de texte.


Vous pouvez voir Git comme un utilitaire de gestion de chronologies. Les commits sont des photos prises à un point précis sur l’historique d’un projet. Il est même possible de gérer plusieurs chronologies grâce aux branches.

Quand vous faites une annulation dans Git, vous reculez dans le temps vers un point - commit - sur lequel les erreurs n’ont pas été commises.


Pour ce challenge, commencez par cloner ce repository en local : https://gitlab.com/la-capsule-bootcamp/fork-recalbox

`git clone git@gitlab.com:la-capsule-bootcamp/fork-recalbox.git`

- 👉 Créez un fichier info.txt dans le projet. Faites un commit sur la branche master. Tant que vous ne l’avez pas envoyé sur un autre dépôt, vous pouvez l’annuler localement. Annulez ce commit sans pour autant supprimer le fichier info.txt (vous pouvez regarder sur la documentation https://git-scm.com/docs/git-reset)

`git reset --soft HEAD^`

- 👉 Annulez le commit de David Barbion du mercredi 3 août 2022 ayant pour ID 8a5d7b83786f64a7228688a011470f36c9f5d55c

`git revert 8a5d7b83786f64a7228688a011470f36c9f5d55c`

## ANNULER UN COMMIT SUR UN DÉPÔT DISTANT

- 👉 Créez un repository sur GitLab et pushez votre projet sur ce dépôt.

- 👉 Modifiez le fichier README.md , faites un commit, puis faites un git push à nouveau.

`git revert [last working commit]`

- 👉 Annulez ce commit sur le dépôt distant.

`git push`

## RÉCUPÉRER UN COMMIT PRÉCIS SUR LE DÉPÔT DISTANT

- 👉 Dans l’IDE de gitlab.com, modifiez le fichier README.md puis faites un commit dans le navigateur. Copiez l’ID de ce commit.

- 👉 Faîtes une deuxième modification dans un autre fichier (ajoutez par exemple un commentaire dans un fichier en python) puis faites un second commit, toujours depuis le navigateur.

- 👉 Regardez du côté de la commande cherry-pick et récupérez en local uniquement l’avant-dernier commit, dont vous avez copié l’ID : https://git-scm.com/docs/git-cherry-pick

Permet de tester un commit localement.

`git cherry-pick 2e4cbe9872bbbbcecded995e8f09d32b9a8e80af`