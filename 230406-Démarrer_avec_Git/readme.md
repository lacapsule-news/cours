# Démarrer avec Git

## Contexte

A mesure qu'un projet évolue, il devient difficile de s'y retrouver

Git est un outil de versioning.
Il permet de suivre l'évolution des différents fichiers d'un projet.

Il permet de savoir:
- Qui a modifié le fichier ?
- Quand a eu lieu la modification ?
- Qu'est ce qui a été modifié ?
- Pourquoi cette modification a été réalisée ?

Un outil très utilisé en entreprise facilitant le travail collaboratif.

Git permet de traquer chaque modification apportée à un fichier.

## Fonctionnement

Git est un outil en ligne de commande.
Il s'utilise dans le terminal intégré à VSCode.

Chaque évolution du projet est enregistrée sur l'ordinateur grâce à une commande qui s'exécute depuis le terminal.

Un enregistrement s'appelle un commit.

Les différents commit sont stockés sur l'ordinateur.
Le lieu de stockage s'appelle un dépôt.

Il nous donne les information suivantes:
- Qui a modifié le fichier ?
- Quand a eu lieu la modification ?
- Qu'est ce qui a été modifié ?
- Pourquoi cette modification a été réalisée ?

## Setup

### Vérifier que [Git](https://git-scm.com/downloads) est bien installé en vérifiant sa version.

`git --version`

### Initialiser un dépôt local

- Se potitioné a la racine du fichier

`pwd`

- Initialiser le dépôt local se fait par projet

`git init`

### Travailler sur un dépôt local

- Le commit est un principe en 2 étapes:
    - Sélectionner les fichier a photographier
    - Prendre la photo du projet

La sélection des fichier se fait grace a:

`git add [Fichier] ou git add .`

La commande **git commit** permet de photographier son projet et d'enregistrer cette photo dans son dépôt local.

`git commit -m "First commit"`

La commande **git status** permet d'avoir un état des lieux du dépôt.

La commande **git log** permet de consulter l'historique des commits.

```
commit 9c45e5e5335124613d42b90d3d08b0506efb5d09 (HEAD -> master, origin/master)
Author: sunburst <5unburst@proton.me>
Date:   Wed Apr 5 22:41:02 2023 +0200

    better pdf

commit 9ab3d33c9437a7bd372daa6d5b99facf4dea56bb
Author: sunburst <5unburst@proton.me>
Date:   Wed Apr 5 22:38:02 2023 +0200
```

On peut consulté le details d'un commit grace a **git show [id]**

```
commit 9c45e5e5335124613d42b90d3d08b0506efb5d09 (HEAD -> master, origin/master)
Author: sunburst <5unburst@proton.me>
Date:   Wed Apr 5 22:41:02 2023 +0200

    better pdf

diff --git a/readme.md b/readme.md
index f3876a3..55644d6 100644
--- a/readme.md
+++ b/readme.md
```

### Travailler sur un dépôt distant

En entreprise plusieurs développeurs collaborent sur un même projet mais sur différentes machines.

Le dépôt distant permet de rendre le projet accessible depuis internet.

Gitlab permet de créer très facilement un dépôt distant.

Pour connecter notre dépôt local au dépôt distant il faut utiliser la commande **git remote add origin [REPO_URL]**

Pour syncroniser notre dépôt local au dépôt distant il faut passer par la commande **git push origin main**

Git - est le logicielle qui permet de géré les mise a jours du projet(localement).
Gitlab - est la source commune qui sert a géré le projet(disponible).

Pour cloner un repository il suffit de copier le lien de celui-ci et fait **git clone [URL]**

## Exo

### 05 - Pretty logs

#### HISTORIQUE DES COMMITS

L’affichage de l’historique est parfois assez peu lisible dans le terminal. Vous allez remédier à ce souci en améliorant l’apparence de vos git log. Aidez-vous de la documentation Git : https://git-scm.com/docs/git-log

👉 Clonez ce repository : https://gitlab.com/la-capsule-bootcamp/go-pulse

👉 Affichez uniquement les trois derniers commits de Felix Lange, avec une date dans le format suivant (format relatif) :

👉 Enregistrez les 20 derniers commits dans un fichier "last_commits.txt".

👉 Affichez les commits dans le format ci-dessous. Chaque information aura une couleur différente et l’affichage de la date est simplifié.

👉 En local, modifiez le commentaire du dernier commit de Shane Bammel.
Commit ID : 3749241ef6ed8e5ff3b8bb0c8e314789297125bd

👉 Affichez à nouveau commit et enregistrez-le dans votre fichier "last_commits.txt".

`git log -n 3`
`git log -n 20 > last_commits.txt`
`git rebase -i HEAD~3`
`git log --pretty=format:"%C(auto)%h %C(red)%an %C(blue)%ad %C(yellow)%s"`

### 04 - Undoing things

#### ANNULER UN COMMIT EN LOCAL

Dans ce challenge, vous allez aborder les stratégies et commandes d’annulation et de retour en arrière proposées par Git. Notez que Git ne propose pas de retour en arrière comme une application de traitement de texte.


Vous pouvez voir Git comme un utilitaire de gestion de chronologies. Les commits sont des photos prises à un point précis sur l’historique d’un projet. Il est même possible de gérer plusieurs chronologies grâce aux branches.

Quand vous faites une annulation dans Git, vous reculez dans le temps vers un point - commit - sur lequel les erreurs n’ont pas été commises.


Pour ce challenge, commencez par cloner ce repository en local : https://gitlab.com/la-capsule-bootcamp/fork-recalbox

`git clone git@gitlab.com:la-capsule-bootcamp/fork-recalbox.git`

- 👉 Créez un fichier info.txt dans le projet. Faites un commit sur la branche master. Tant que vous ne l’avez pas envoyé sur un autre dépôt, vous pouvez l’annuler localement. Annulez ce commit sans pour autant supprimer le fichier info.txt (vous pouvez regarder sur la documentation https://git-scm.com/docs/git-reset)

`git reset --soft HEAD^`

- 👉 Annulez le commit de David Barbion du mercredi 3 août 2022 ayant pour ID 8a5d7b83786f64a7228688a011470f36c9f5d55c

`git revert 8a5d7b83786f64a7228688a011470f36c9f5d55c`

#### ANNULER UN COMMIT SUR UN DÉPÔT DISTANT

- 👉 Créez un repository sur GitLab et pushez votre projet sur ce dépôt.

- 👉 Modifiez le fichier README.md , faites un commit, puis faites un git push à nouveau.

`git revert [last working commit]`

- 👉 Annulez ce commit sur le dépôt distant.

`git push`

#### RÉCUPÉRER UN COMMIT PRÉCIS SUR LE DÉPÔT DISTANT

- 👉 Dans l’IDE de gitlab.com, modifiez le fichier README.md puis faites un commit dans le navigateur. Copiez l’ID de ce commit.

- 👉 Faîtes une deuxième modification dans un autre fichier (ajoutez par exemple un commentaire dans un fichier en python) puis faites un second commit, toujours depuis le navigateur.

- 👉 Regardez du côté de la commande cherry-pick et récupérez en local uniquement l’avant-dernier commit, dont vous avez copié l’ID : https://git-scm.com/docs/git-cherry-pick

Permet de tester un commit localement.

`git cherry-pick 2e4cbe9872bbbbcecded995e8f09d32b9a8e80af`

### 03 - Fork and clone

#### FORK D’UN PROJET OPEN-SOURCE

Si un projet open-source est disponible et partagé sur une plateforme comme GitHub ou GitLab, il est possible d’en faire une copie sur votre compte. Cette copie contiendra l’ensemble des fichiers du projet et dupliquera également le dépôt, vous permettant d’avoir l’historique des versions du projet localement et de pouvoir charger ou restaurer l’une de ces versions.

Le principe de la contribution open-source étant de faire un fork du projet, modifier le code source et proposer par la suite l’intégration des modifications au projet d’origine.

- 👉 Le projet GitLab lui-même est open-source et s’appuie sur les contributions de milliers de développeurs. Le code source de GitLab se trouve sur GitLab (sans grande surprise) à l’adresse suivante : https://gitlab.com/gitlab-org/gitlab

- La page du projet GitLab présente le nombre de commits ayant été effectués sur la codebase, le nombre de branches, ainsi que la quantité de forks.

- 👉 Sélectionnez le projet open-source de votre choix et forkez-le sur votre compte GitLab.
Vous pouvez parcourir les projets open-source à l’adresse suivante : https://gitlab.com/explore/projects/starred

- Évitez les trop gros projets qui nécessiteraient par la suite beaucoup d’espace de stockage sur votre machine.

#### CRÉATION D’UN CLONE LOCAL DE VOTRE FORK

Pour cloner un projet (via HTTPS), il faut connaître l’url du dépôt (le repository qui contient votre fork sur votre compte GitLab).


git clone https://gitlab.com/urldevotrerepository.git


⚠️ Attention : la commande git clone permet de récupérer une copie conforme au dépôt distant, vous permettant de tester et modifier le code localement. Pour modifier le code distant, vous devez posséder le rôle adéquat dans les options du repository sur GitLab. C’est bien évidemment le cas s’il s’agit de votre propre repository.

👉 Modifiez un fichier et faites un commit en local uniquement.

👉 Affichez les dépôts distants liés à votre dépôt local.


git remote -v


#### TRAVAILLER AVEC PLUSIEURS DÉPÔTS DISTANTS

👉 Créez un nouveau repository sur GitLab, connectez votre dépôt local à ce nouveau dépôt distant auquel vous attribuerez l’alias "backup".

👉 Affichez à nouveau les dépôts distants liés à votre dépôt local.

Il est également possible de supprimer une liaison. Cette option peut être intéressante lorsque vous clonez un projet, mais souhaitez changer de dépôt distant.

👉 Supprimez la liaison avec le repository distant dont l’alias est origin.


git remote rm origin


👉 Affichez à nouveau la liste des liaisons avec des dépôts distants.

👉 En local, renommez votre dépôt distant "backup" en "sauvegarde".

👉 Affichez à nouveau la liste des liaisons avec des dépôts distants.


#### TRAVAILLER À PLUSIEURS SUR DES DÉPÔTS MULTIPLES

Vous allez maintenant mettre votre buddy à contribution. Les plateformes telles que GitLab et GitHub prennent bien entendu tout leur sens quand il s’agit de travailler à plusieurs.

Pour envoyer vos modifications sur le dépôt distant, il vous faut avoir les droits pour le faire. Si vous n’avez pas les droits, il ne sera pas possible de pusher ces modifications. Les droits sont à configurer directement sur les dépôts distants sur GitLab, onglet "Settings".

- 👉 Ajoutez votre buddy en tant que collaborateur sur votre projet. Invitez-le à cloner votre projet,  à faire une modification et à la pusher sur votre repository.

- 👉 De la même manière, de votre côté, clonez le projet de votre buddy, faites une modification et pushez sur son repository.

- 👉 Vous décidez de faire machine arrière et souhaitez annuler le dernier commit que vous venez de pousser sur le dépôt distant de votre buddy. Trouvez et exécutez la (ou les) commande(s) pour annuler ce commit sur le repository de votre buddy.

- 👉 Créez chacun un nouveau repository sur GitLab nommé production, connectez-le à votre dépôt local et faites un git push sur ce nouveau repository.

### 02 - GIT BEGIN

#### INSTALLATION DE GIT

Le contrôle de version permet de suivre et gérer efficacement les changements apportés à un code source afin de réduire le temps de développement et d’assurer le succès des déploiements.

Git va garder une trace de chaque changement apporté au code. Si une erreur est commise, les développeurs peuvent revenir en arrière et comparer les versions antérieures du code, ce qui leur permet de corriger l’erreur tout en minimisant les perturbations pour les autres membres de l’équipe.


Vous allez commencer par utiliser Git en local afin de versionner vos propres fichiers.


- 👉 Dans un premier temps, assurez-vous d’avoir installé Git sur votre machine.  


`git --version`

`git version 2.40.0`

- 👉 Si un message d’erreur apparaît, procédez à l’installation de Git grâce aux commandes suivantes.


`sudo apt update`

`sudo apt install git -y`


- 👉 Avant de commencer à utiliser Git, enregistrez votre nom d’utilisateur et votre email qui seront associés à vos commits grâce aux commandes suivantes.


git config --global user.name "John Doe"

git config --global user.email "johndoe@gmail.com"



#### INITIALISER UN DÉPÔT LOCAL

Nous allons commencer par utiliser Git en local afin de versionner nos propres fichiers.
Créez un nouveau dossier nommé "gitbegin" puis copiez cet extrait code un premier fichier "hello.py".

`print("Hello world")`


- 👉 Au sein du dossier, initialiser un nouveau dépôt Git.


`git init`


Cela aura pour conséquence de créer un dossier caché ".git" qui agira comme le chef d’orchestre de la mécanique de Git au sein de votre projet.


- 👉 Vérifiez la présence du dossier ".git" en affichant les fichiers et dossiers cachés via ls.



Ce dossier caché ".git" est ce qui fait du dossier parent un repository Git. Sans ce dossier caché, les versions ne sont pas trackées.

#### CRÉATION D’UN PREMIER COMMIT

- 👉 À ce stade, votre repository Git est créé mais aucun fichier n’est traqué. 

Une particularité de Git est son système de staging qui permet de sélectionner les fichiers à suivre lors du prochain commit. Vous pouvez imaginer ça comme une "zone d'attente" où l’on va lister les fichiers que l'on souhaite voir enregistrés (cf. documentation git).

Vous pouvez dire à Git de suivre votre premier fichier "hello.py".


`git add hello.py`

ou avec plusieurs fichiers :

`git add hello.py fichier_2.py`

ou pour ajouter tous les fichiers du répertoire :

`git add .`

- 👉 Vous pouvez à tout moment vérifier le statut de votre arbre de travail (environnement de staging, branche courante, commits en cours, etc) grâce à la commande suivante.


`git status`


- 👉 Une fois que la zone de staging est prête, vous allez pouvoir faire votre premier commit.
Un commit est une étape dans l'historique de votre projet, étape que l'on va pouvoir identifier avec un message particulier après l’option "-m".

`git commit -m "Project initialization"`

- 👉 Vous pouvez à tout moment consulter l’historique des commit grâce à la commande suivante.

`git log`

Par défaut, git a créé une branche principale nommée "master". Vous verrez le concept de branche dans le prochain challenge.

- 👉 Assurez-vous de renommer la branche principale en "main", qui est le nom couramment utilisé pour la branche en production.

`git branch -m main`

#### CONNEXION À UN DÉPOT DISTANT SUR GITLAB

À chaque commit effectué, vous enregistrez vos modifications dans votre dépôt local.
Pour conserver ces versions sur le cloud ou tout simplement pour partager votre travail avec votre équipe, il est essentiel de créer un dépôt distant.

C’est ce que proposent des services web d'hébergement comme GitLab, GitHub ou BitBucket en mettant à disposition l’url de ce dépôt distant.

- 👉 Créez un compte sur GitLab et démarrez un nouveau projet que vous nommerez "gitbegin".
Veillez à créer un "blank project", sans initialiser le répertoire avec un README (case à décocher).

- 👉 Exécutez la commande suivante à la racine de votre projet afin de lier votre dépôt local et votre dépôt distant.

`git remote add origin https://gitlab.com:lacapsule5051914/gitbegin2.git`

👉 Déposez le contenu de votre repository local dans votre dépôt distant avec la commande git push.
Grâce à cette commande, vous envoyez les versions enregistrées localement vers le dépôt distant nommé "origin" sur sa branche principale nommée "main".

`git push origin main`

Une fois vos identifiants GitLab entrés, votre dépôt local est déposé dans le dépôt distant. Actualisez la page de votre repository sur GitLab pour retrouver votre code et toutes ses versions désormais en ligne.


#### Conflit d'authetification avec mes serveur

- Creation d'un paire de clé avec ssh-keygen
- Envoie de la clé publique sur gitlab
- Mise a jour des config git git config --global core.sshCommand "ssh -i /ssh/private/key"
- Suppression de l'origin git remote rm origin
- Ajout de l'url ssh git remote add origin git@gitlab.com:lacapsule5051914/gitbegin2.git
- git push origin main