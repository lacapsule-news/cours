# Pretty logs

## HISTORIQUE DES COMMITS

L’affichage de l’historique est parfois assez peu lisible dans le terminal. Vous allez remédier à ce souci en améliorant l’apparence de vos git log. Aidez-vous de la documentation Git : https://git-scm.com/docs/git-log

👉 Clonez ce repository : https://gitlab.com/la-capsule-bootcamp/go-pulse

👉 Affichez uniquement les trois derniers commits de Felix Lange, avec une date dans le format suivant (format relatif) :

👉 Enregistrez les 20 derniers commits dans un fichier "last_commits.txt".

👉 Affichez les commits dans le format ci-dessous. Chaque information aura une couleur différente et l’affichage de la date est simplifié.

👉 En local, modifiez le commentaire du dernier commit de Shane Bammel.
Commit ID : 3749241ef6ed8e5ff3b8bb0c8e314789297125bd

👉 Affichez à nouveau commit et enregistrez-le dans votre fichier "last_commits.txt".

`git log -n 3`
`git log -n 20 > last_commits.txt`
`git rebase -i HEAD~3`
`git log --pretty=format:"%C(auto)%h %C(red)%an %C(blue)%ad %C(yellow)%s"`