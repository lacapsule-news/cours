# Démarrer avec Git

## Contexte

A mesure qu'un projet évolue, il devient difficile de s'y retrouver

Git est un outil de versioning.
Il permet de suivre l'évolution des différents fichiers d'un projet.

Il permet de savoir:
- Qui a modifié le fichier ?
- Quand a eu lieu la modification ?
- Qu'est ce qui a été modifié ?
- Pourquoi cette modification a été réalisée ?

Un outil très utilisé en entreprise facilitant le travail collaboratif.

Git permet de traquer chaque modification apportée à un fichier.

## Fonctionnement

Git est un outil en ligne de commande.
Il s'utilise dans le terminal intégré à VSCode.

Chaque évolution du projet est enregistrée sur l'ordinateur grâce à une commande qui s'exécute depuis le terminal.

Un enregistrement s'appelle un commit.

Les différents commit sont stockés sur l'ordinateur.
Le lieu de stockage s'appelle un dépôt.

Il nous donne les information suivantes:
- Qui a modifié le fichier ?
- Quand a eu lieu la modification ?
- Qu'est ce qui a été modifié ?
- Pourquoi cette modification a été réalisée ?

## Setup

### Vérifier que [Git](https://git-scm.com/downloads) est bien installé en vérifiant sa version.

`git --version`

### Initialiser un dépôt local

- Se potitioné a la racine du fichier

`pwd`

- Initialiser le dépôt local se fait par projet

`git init`

### Travailler sur un dépôt local

- Le commit est un principe en 2 étapes:
    - Sélectionner les fichier a photographier
    - Prendre la photo du projet

La sélection des fichier se fait grace a:

`git add [Fichier] ou git add .`

La commande **git commit** permet de photographier son projet et d'enregistrer cette photo dans son dépôt local.

`git commit -m "First commit"`

La commande **git status** permet d'avoir un état des lieux du dépôt.

La commande **git log** permet de consulter l'historique des commits.

```
commit 9c45e5e5335124613d42b90d3d08b0506efb5d09 (HEAD -> master, origin/master)
Author: sunburst <5unburst@proton.me>
Date:   Wed Apr 5 22:41:02 2023 +0200

    better pdf

commit 9ab3d33c9437a7bd372daa6d5b99facf4dea56bb
Author: sunburst <5unburst@proton.me>
Date:   Wed Apr 5 22:38:02 2023 +0200
```

On peut consulté le details d'un commit grace a **git show [id]**

```
commit 9c45e5e5335124613d42b90d3d08b0506efb5d09 (HEAD -> master, origin/master)
Author: sunburst <5unburst@proton.me>
Date:   Wed Apr 5 22:41:02 2023 +0200

    better pdf

diff --git a/readme.md b/readme.md
index f3876a3..55644d6 100644
--- a/readme.md
+++ b/readme.md
```

### Travailler sur un dépôt distant

En entreprise plusieurs développeurs collaborent sur un même projet mais sur différentes machines.

Le dépôt distant permet de rendre le projet accessible depuis internet.

Gitlab permet de créer très facilement un dépôt distant.

Pour connecter notre dépôt local au dépôt distant il faut utiliser la commande **git remote add origin [REPO_URL]**

Pour syncroniser notre dépôt local au dépôt distant il faut passer par la commande **git push origin main**

Git - est le logicielle qui permet de géré les mise a jours du projet(localement).
Gitlab - est la source commune qui sert a géré le projet(disponible).

Pour cloner un repository il suffit de copier le lien de celui-ci et fait **git clone [URL]**