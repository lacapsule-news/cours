# GIT BEGIN

## INSTALLATION DE GIT

Le contrôle de version permet de suivre et gérer efficacement les changements apportés à un code source afin de réduire le temps de développement et d’assurer le succès des déploiements.

Git va garder une trace de chaque changement apporté au code. Si une erreur est commise, les développeurs peuvent revenir en arrière et comparer les versions antérieures du code, ce qui leur permet de corriger l’erreur tout en minimisant les perturbations pour les autres membres de l’équipe.


Vous allez commencer par utiliser Git en local afin de versionner vos propres fichiers.


- 👉 Dans un premier temps, assurez-vous d’avoir installé Git sur votre machine.  


`git --version`

`git version 2.40.0`

- 👉 Si un message d’erreur apparaît, procédez à l’installation de Git grâce aux commandes suivantes.


`sudo apt update`

`sudo apt install git -y`


- 👉 Avant de commencer à utiliser Git, enregistrez votre nom d’utilisateur et votre email qui seront associés à vos commits grâce aux commandes suivantes.


git config --global user.name "John Doe"

git config --global user.email "johndoe@gmail.com"



## INITIALISER UN DÉPÔT LOCAL

Nous allons commencer par utiliser Git en local afin de versionner nos propres fichiers.
Créez un nouveau dossier nommé "gitbegin" puis copiez cet extrait code un premier fichier "hello.py".

`print("Hello world")`


- 👉 Au sein du dossier, initialiser un nouveau dépôt Git.


`git init`


Cela aura pour conséquence de créer un dossier caché ".git" qui agira comme le chef d’orchestre de la mécanique de Git au sein de votre projet.


- 👉 Vérifiez la présence du dossier ".git" en affichant les fichiers et dossiers cachés via ls.



Ce dossier caché ".git" est ce qui fait du dossier parent un repository Git. Sans ce dossier caché, les versions ne sont pas trackées.

## CRÉATION D’UN PREMIER COMMIT

- 👉 À ce stade, votre repository Git est créé mais aucun fichier n’est traqué. 

Une particularité de Git est son système de staging qui permet de sélectionner les fichiers à suivre lors du prochain commit. Vous pouvez imaginer ça comme une "zone d'attente" où l’on va lister les fichiers que l'on souhaite voir enregistrés (cf. documentation git).

Vous pouvez dire à Git de suivre votre premier fichier "hello.py".


`git add hello.py`

ou avec plusieurs fichiers :

`git add hello.py fichier_2.py`

ou pour ajouter tous les fichiers du répertoire :

`git add .`

- 👉 Vous pouvez à tout moment vérifier le statut de votre arbre de travail (environnement de staging, branche courante, commits en cours, etc) grâce à la commande suivante.


`git status`


- 👉 Une fois que la zone de staging est prête, vous allez pouvoir faire votre premier commit.
Un commit est une étape dans l'historique de votre projet, étape que l'on va pouvoir identifier avec un message particulier après l’option "-m".

`git commit -m "Project initialization"`

- 👉 Vous pouvez à tout moment consulter l’historique des commit grâce à la commande suivante.

`git log`

Par défaut, git a créé une branche principale nommée "master". Vous verrez le concept de branche dans le prochain challenge.

- 👉 Assurez-vous de renommer la branche principale en "main", qui est le nom couramment utilisé pour la branche en production.

`git branch -m main`

## CONNEXION À UN DÉPOT DISTANT SUR GITLAB

À chaque commit effectué, vous enregistrez vos modifications dans votre dépôt local.
Pour conserver ces versions sur le cloud ou tout simplement pour partager votre travail avec votre équipe, il est essentiel de créer un dépôt distant.

C’est ce que proposent des services web d'hébergement comme GitLab, GitHub ou BitBucket en mettant à disposition l’url de ce dépôt distant.

- 👉 Créez un compte sur GitLab et démarrez un nouveau projet que vous nommerez "gitbegin".
Veillez à créer un "blank project", sans initialiser le répertoire avec un README (case à décocher).

- 👉 Exécutez la commande suivante à la racine de votre projet afin de lier votre dépôt local et votre dépôt distant.

`git remote add origin https://gitlab.com:lacapsule5051914/gitbegin2.git`

👉 Déposez le contenu de votre repository local dans votre dépôt distant avec la commande git push.
Grâce à cette commande, vous envoyez les versions enregistrées localement vers le dépôt distant nommé "origin" sur sa branche principale nommée "main".

`git push origin main`

Une fois vos identifiants GitLab entrés, votre dépôt local est déposé dans le dépôt distant. Actualisez la page de votre repository sur GitLab pour retrouver votre code et toutes ses versions désormais en ligne.


## Conflit d'authetification avec mes serveur

- Creation d'un paire de clé avec ssh-keygen
- Envoie de la clé publique sur gitlab
- Mise a jour des config git git config --global core.sshCommand "ssh -i /ssh/private/key"
- Suppression de l'origin git remote rm origin
- Ajout de l'url ssh git remote add origin git@gitlab.com:lacapsule5051914/gitbegin2.git
- git push origin main