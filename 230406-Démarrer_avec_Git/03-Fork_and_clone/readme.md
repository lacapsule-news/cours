# Fork and clone

## FORK D’UN PROJET OPEN-SOURCE

Si un projet open-source est disponible et partagé sur une plateforme comme GitHub ou GitLab, il est possible d’en faire une copie sur votre compte. Cette copie contiendra l’ensemble des fichiers du projet et dupliquera également le dépôt, vous permettant d’avoir l’historique des versions du projet localement et de pouvoir charger ou restaurer l’une de ces versions.

Le principe de la contribution open-source étant de faire un fork du projet, modifier le code source et proposer par la suite l’intégration des modifications au projet d’origine.

- 👉 Le projet GitLab lui-même est open-source et s’appuie sur les contributions de milliers de développeurs. Le code source de GitLab se trouve sur GitLab (sans grande surprise) à l’adresse suivante : https://gitlab.com/gitlab-org/gitlab

- La page du projet GitLab présente le nombre de commits ayant été effectués sur la codebase, le nombre de branches, ainsi que la quantité de forks.

- 👉 Sélectionnez le projet open-source de votre choix et forkez-le sur votre compte GitLab.
Vous pouvez parcourir les projets open-source à l’adresse suivante : https://gitlab.com/explore/projects/starred

- Évitez les trop gros projets qui nécessiteraient par la suite beaucoup d’espace de stockage sur votre machine.

## CRÉATION D’UN CLONE LOCAL DE VOTRE FORK

Pour cloner un projet (via HTTPS), il faut connaître l’url du dépôt (le repository qui contient votre fork sur votre compte GitLab).


git clone https://gitlab.com/urldevotrerepository.git


⚠️ Attention : la commande git clone permet de récupérer une copie conforme au dépôt distant, vous permettant de tester et modifier le code localement. Pour modifier le code distant, vous devez posséder le rôle adéquat dans les options du repository sur GitLab. C’est bien évidemment le cas s’il s’agit de votre propre repository.

👉 Modifiez un fichier et faites un commit en local uniquement.

👉 Affichez les dépôts distants liés à votre dépôt local.


git remote -v


## TRAVAILLER AVEC PLUSIEURS DÉPÔTS DISTANTS

👉 Créez un nouveau repository sur GitLab, connectez votre dépôt local à ce nouveau dépôt distant auquel vous attribuerez l’alias "backup".

👉 Affichez à nouveau les dépôts distants liés à votre dépôt local.

Il est également possible de supprimer une liaison. Cette option peut être intéressante lorsque vous clonez un projet, mais souhaitez changer de dépôt distant.

👉 Supprimez la liaison avec le repository distant dont l’alias est origin.


git remote rm origin


👉 Affichez à nouveau la liste des liaisons avec des dépôts distants.

👉 En local, renommez votre dépôt distant "backup" en "sauvegarde".

👉 Affichez à nouveau la liste des liaisons avec des dépôts distants.


## TRAVAILLER À PLUSIEURS SUR DES DÉPÔTS MULTIPLES

Vous allez maintenant mettre votre buddy à contribution. Les plateformes telles que GitLab et GitHub prennent bien entendu tout leur sens quand il s’agit de travailler à plusieurs.

Pour envoyer vos modifications sur le dépôt distant, il vous faut avoir les droits pour le faire. Si vous n’avez pas les droits, il ne sera pas possible de pusher ces modifications. Les droits sont à configurer directement sur les dépôts distants sur GitLab, onglet "Settings".

- 👉 Ajoutez votre buddy en tant que collaborateur sur votre projet. Invitez-le à cloner votre projet,  à faire une modification et à la pusher sur votre repository.

- 👉 De la même manière, de votre côté, clonez le projet de votre buddy, faites une modification et pushez sur son repository.

- 👉 Vous décidez de faire machine arrière et souhaitez annuler le dernier commit que vous venez de pousser sur le dépôt distant de votre buddy. Trouvez et exécutez la (ou les) commande(s) pour annuler ce commit sur le repository de votre buddy.

- 👉 Créez chacun un nouveau repository sur GitLab nommé production, connectez-le à votre dépôt local et faites un git push sur ce nouveau repository.